#!/bin/bash

BGREEN="\e[1;32m"
BRED="\e[1;31m"
NORMAL="\e[0m"
BYELLOW="\e[1;33m"
ECHO_ME="/bin/echo -e"

if [ $SILENT -eq 1 ]; then
     ECHO_ME=$ECHO_ME" \c "
fi

NBSUCCESS=0
NBFAILURE=0
NBTESTCHE=0

PWPOLY_PATH="../"
TESTFI_PATH=$PWPOLY_PATH"test_files/"
EAROOTS_MP_CALL=$PWPOLY_PATH"bin/pw_earoots_mp"
EAROOTS_CALL=$PWPOLY_PATH"bin/pw_earoots"
COCO_CALL=$PWPOLY_PATH"bin/pw_solver_CoCo"
PWROOTS_CALL=$PWPOLY_PATH"bin/pwroots"
if [ $MEMMAM -eq 1 ]; then
     PWROOTS_CALL=$PWPOLY_PATH"bin/pwroots_mm"
fi

# EVALUATE_CALL=$PWPOLY_PATH"bin/pw_evaluate_verify"
PWEVAL_CALL=$PWPOLY_PATH"bin/pweval"

GENPOLFILE_CALL=$PWPOLY_PATH"bin/genPolFile"

GENPOIFILE_CALL=$PWPOLY_PATH"bin/genPointFile"

# VALGRIND_CALL="valgrind --quiet --leak-check=full"
VALGRIND_CALL="valgrind --leak-check=full"

FILENAME_IN="check_solver_in.pw"
FILENAME_OU="check.txt"

FILENAME_IN_POINT_FMPQ="check_eval_points_in_FMPQ.txt"
FILENAME_IN_POINT_DOUB="check_eval_points_in_DOUB.txt"
FILENAME_IN_POINT_ACBS="check_eval_points_in_ACBS.txt"

########################time
TIME_CALL="/usr/bin/time -f"
TIME_FORM="real\t%e\ncpu\t%U"
