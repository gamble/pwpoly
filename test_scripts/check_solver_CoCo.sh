#!/bin/bash

CHECK_NAME="solver_CoCo"

source params_check.sh
source globals_check.sh
source functions_check.sh

CHECK=1
GOAL="approximate"

if [ $MEMORY -eq 1 ]; then

    if [ $SHORT -eq 0 ]; then
        DEGREES="51 52"
        EPSVALS="-1000"
        for DEGR in $DEGREES; do
            for EPS in $EPSVALS; do
                $GENPOLFILE_CALL "Bernoulli" $DEGR -L 53 -o $FILENAME_IN
                $ECHO_ME "--- APPROXIMATE VALGRIND Bernoulli pol. of deg $DEGR with log2eps=$EPS---------------- "
                call_valgrind_checks_CoCo $DEGR $GOAL $EPS $DOMAIN $NBROOTS 0
                rm -rf $FILENAME_IN $FILENAME_OU
            done
        done

        EPS="-1000"
        DEGR="57"
        SEED="1"
        $GENPOLFILE_CALL "randomDenseComp" $DEGR -s $SEED -o $FILENAME_IN
        $ECHO_ME "--- APPROXIMATE VALGRIND $SEED-th randomDenseComp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_valgrind_checks_CoCo $DEGR $GOAL $EPS $DOMAIN $NBROOTS 0
        rm -rf $FILENAME_IN $FILENAME_OU
    fi

    EPS=-10
    DEGR="57"
    $GENPOLFILE_CALL "Mignotte" $DEGR -L 53 -o $FILENAME_IN
    $ECHO_ME "--- APPROXIMATE    VALGRIND  Mignotte pol. of deg $DEGR with log2eps=$EPS---------------- "
    call_valgrind_checks_CoCo $DEGR "a" $EPS $DOMAIN $NBROOTS 0
    $ECHO_ME "--- ISOLATE        VALGRIND  Mignotte pol. of deg $DEGR with log2eps=$EPS---------------- "
    call_valgrind_checks_CoCo $DEGR "i" $EPS $DOMAIN $NBROOTS 0
    $ECHO_ME "--- ISO AND APPROX VALGRIND  Mignotte pol. of deg $DEGR with log2eps=$EPS---------------- "
    call_valgrind_checks_CoCo $DEGR "ia" $EPS $DOMAIN $NBROOTS 0
    rm -rf $FILENAME_IN $FILENAME_OU

else

    EPS=-53
    DEGREES="51 102"
    SEEDS="1 2 3 4 5"
    
    if [ $SHORT -eq 1 ]; then
        DEGREES="102"
        SEEDS="1"
    fi

    for DEGR in $DEGREES; do
        for SEED in $SEEDS; do
            $GENPOLFILE_CALL "randomDenseHyp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
            $ECHO_ME "--- APPROXIMATE    $SEED-th randonDenseHyp pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_CoCo $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
            rm -rf $FILENAME_IN $FILENAME_OU

            $GENPOLFILE_CALL "randomDenseFlat" $DEGR -s $SEED -L 53 -o $FILENAME_IN
            $ECHO_ME "--- APPROXIMATE    $SEED-th randonDenseFlat pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_CoCo $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
            rm -rf $FILENAME_IN $FILENAME_OU

            $GENPOLFILE_CALL "randomDenseEll" $DEGR -s $SEED -L 53 -o $FILENAME_IN
            $ECHO_ME "--- APPROXIMATE    $SEED-th randonDenseEll pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_CoCo $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
            rm -rf $FILENAME_IN $FILENAME_OU
            
            $GENPOLFILE_CALL "randomDenseHypComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
            $ECHO_ME "--- APPROXIMATE    $SEED-th randonDenseHypComp pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_CoCo $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
            rm -rf $FILENAME_IN $FILENAME_OU

            $GENPOLFILE_CALL "randomDenseFlatComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
            $ECHO_ME "--- APPROXIMATE    $SEED-th randonDenseFlatComp pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_CoCo $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
            rm -rf $FILENAME_IN $FILENAME_OU

            $GENPOLFILE_CALL "randomDenseEllComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
            $ECHO_ME "--- APPROXIMATE    $SEED-th randonDenseEllComp pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_CoCo $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
            rm -rf $FILENAME_IN $FILENAME_OU
        done
    done 
    
    DEGREES="53 100 157"
    EPSVALS="-10 -100 -1000"
    
    if [ $SHORT -eq 1 ]; then
        DEGREES="100"
        EPSVALS="-100"
    fi
    
    for DEGR in $DEGREES; do
        $GENPOLFILE_CALL "Mignotte" $DEGR -L 53 -o $FILENAME_IN
        $ECHO_ME "--- ISOLATE        Mignotte pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_CoCo $DEGR "i" $EPS $DOMAIN $NBROOTS $CHECK
        for EPS in $EPSVALS; do
            $ECHO_ME "--- APPROXIMATE    Mignotte pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_CoCo $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
            $ECHO_ME "--- ISO AND APPROX Mignotte pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_CoCo $DEGR "ia" $EPS $DOMAIN $NBROOTS $CHECK
        done
        rm -rf $FILENAME_IN $FILENAME_OU
    done
    
    DEGREES="1 2 3 4 5 6 7 8 9 31 39 43 61 99"
    EPSVALS="-10 -100 -1000"
    
    if [ $SHORT -eq 1 ]; then
        DEGREES="9 99"
        EPSVALS="-100"
    fi
    
    for DEGR in $DEGREES; do
        for EPS in $EPSVALS; do
            $GENPOLFILE_CALL "Bernoulli" $DEGR -L 53 -o $FILENAME_IN
            $ECHO_ME "--- APPROXIMATE    Bernoulli pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_CoCo $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
            rm -rf $FILENAME_IN $FILENAME_OU
        done
    done

fi

rm -rf "roots.txt"
rm -rf "roots.plt"
#override SILENT
ECHO_ME="/bin/echo -e"
success_fail_print_exit
