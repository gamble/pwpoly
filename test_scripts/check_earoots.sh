#!/bin/bash

CHECK_NAME="earoots"

source params_check.sh
source globals_check.sh
source functions_check.sh

CHECK=1
EPS=-54
GOAL=a

if [ $MEMORY -eq 1 ]; then

    DEGREES="51 52"
    for DEGR in $DEGREES; do
        $GENPOLFILE_CALL "Bernoulli" $DEGR -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- VALGRIND Bernoulli pol. of deg $DEGR ---------------- "
        call_valgrind_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS 0
        rm -rf $FILENAME_IN $FILENAME_OU
    done

else

    EPS=-53
    DEGREES="51 102"
    SEEDS="1 2 3 4 5"

    for DEGR in $DEGREES; do
    for SEED in $SEEDS; do
        $GENPOLFILE_CALL "randomDenseHyp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseHyp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU

        $GENPOLFILE_CALL "randomDenseFlat" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseFlat pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU

        $GENPOLFILE_CALL "randomDenseEll" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseEll pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
        
        $GENPOLFILE_CALL "randomDenseHypComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseHypComp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU

        $GENPOLFILE_CALL "randomDenseFlatComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseFlatComp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU

        $GENPOLFILE_CALL "randomDenseEllComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseEllComp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
    done
    done 

    DEGREES="53 100 157"
    for DEGR in $DEGREES; do
        $GENPOLFILE_CALL "Mignotte" $DEGR -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- Mignotte pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
    done

    DEGREES="1 2 3 4 5 6 7 8 9 31 39 43 61 99"
    for DEGR in $DEGREES; do
        $GENPOLFILE_CALL "Bernoulli" $DEGR -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- Bernoulli pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
    done

    if [ $LONG -eq 1 ]; then
        DEGREES="512 767 1024 1513"
        SEEDS="1 2 3 4 5"

        for DEGR in $DEGREES; do
            for SEED in $SEEDS; do
                $GENPOLFILE_CALL "randomDenseHyp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
                $ECHO_ME "------------- $SEED-th randonDenseHyp pol. of deg $DEGR with log2eps=$EPS---------------- "
                call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
                rm -rf $FILENAME_IN $FILENAME_OU

                $GENPOLFILE_CALL "randomDenseFlat" $DEGR -s $SEED -L 53 -o $FILENAME_IN
                $ECHO_ME "------------- $SEED-th randonDenseFlat pol. of deg $DEGR with log2eps=$EPS---------------- "
                call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
                rm -rf $FILENAME_IN $FILENAME_OU

                $GENPOLFILE_CALL "randomDenseEll" $DEGR -s $SEED -L 53 -o $FILENAME_IN
                $ECHO_ME "------------- $SEED-th randonDenseEll pol. of deg $DEGR with log2eps=$EPS---------------- "
                call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
                rm -rf $FILENAME_IN $FILENAME_OU
                
                $GENPOLFILE_CALL "randomDenseHypComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
                $ECHO_ME "------------- $SEED-th randonDenseHypComp pol. of deg $DEGR with log2eps=$EPS---------------- "
                call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
                rm -rf $FILENAME_IN $FILENAME_OU

                $GENPOLFILE_CALL "randomDenseFlatComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
                $ECHO_ME "------------- $SEED-th randonDenseFlatComp pol. of deg $DEGR with log2eps=$EPS---------------- "
                call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
                rm -rf $FILENAME_IN $FILENAME_OU

                $GENPOLFILE_CALL "randomDenseEllComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
                $ECHO_ME "------------- $SEED-th randonDenseEllComp pol. of deg $DEGR with log2eps=$EPS---------------- "
                call_checks_earoots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
                rm -rf $FILENAME_IN $FILENAME_OU
            done
        done 
    
    fi

fi

rm -rf "roots.txt"
rm -rf "roots.plt"
#override SILENT
ECHO_ME="/bin/echo -e"
success_fail_print_exit
