usage()
{
   echo "Usage: ./check_$CHECK_NAME.sh <options> <args>"
   echo "args: "
   echo "--silent: display just a recap of tests"
   echo "--memory: perform memory use checks (not other checks)"
   echo "--short:  perform fast tests "
   echo "--long:   perform long tests "
   echo "--domain: search in domain "
   echo "--nbRoots: search only nbRoots in the domain"
   echo "--mm:     uses memory manager "
   if [ $CHECK_NAME = "pwroots" ]; then
   echo "--solving: for pwroots only; integer identifying solving options; default 63"
   fi
}

##########################get arguments
while [ "$1" != "" ]; do
   PARAM=`echo $1 | sed 's/=.*//'`
   VALUE=`echo $1 | sed 's/[^=]*//; s/=//'`
   case "$PARAM" in
      -h|--help)
         usage
         exit 0
         ;;
      --silent)
        SILENT=1
        ;;
      --memory)
        MEMORY=1
        ;;
      --short)
        SHORT=1
        ;;
      --long)
        LONG=1
        ;;
      --mm)
        MEMMAM=1
        ;;
      --solving)
        SOLVER=$VALUE
        ;;
      --domain)
        DOMAIN=$VALUE
        ;;
      --nbRoots)
        NBROOTS=$VALUE
        ;;
      *)
        usage
        exit 1
        ;;
    esac
    shift
done

#default values
if [ -z "$SILENT" ]; then
   SILENT=0
fi
if [ -z "$MEMORY" ]; then
   MEMORY=0
fi
if [ -z "$LONG" ]; then
   LONG=0
fi
if [ -z "$SHORT" ]; then
   SHORT=0
fi
if [ -z "$MEMMAM" ]; then
   MEMMAM=0
fi
if [ -z "$SOLVER" ]; then
   SOLVER=63
fi
if [ -z "$DOMAIN" ]; then
   DOMAIN="C"
fi
if [ -z "$NBROOTS" ]; then
   NBROOTS=-1
fi
