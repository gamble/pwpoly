#!/bin/bash

success_fail_update()
{
    LCHECK=$1
    LCHECKOK=$2
    if [ $LCHECK -eq 1 ]; then
        if [ $LCHECKOK -eq 1 ]; then
            NBSUCCESS=`echo "$NBSUCCESS+1" | bc`
        else
            NBFAILURE=`echo "$NBFAILURE+1" | bc`
        fi
        NBTESTCHE=`echo "$NBTESTCHE+1" | bc`
    fi
}

success_fail_update_from_succ()
{
    LCHECK=$1
    LCHECKOK=$2
    LSUCC=$3
    if [ $LCHECK -eq 1 ]; then
        if [ $LSUCC -ge 0 ]; then
            if [ $LCHECKOK -eq 1 ]; then
                NBSUCCESS=`echo "$NBSUCCESS+1" | bc`
            else
                NBFAILURE=`echo "$NBFAILURE+1" | bc`
            fi
        fi
        NBTESTCHE=`echo "$NBTESTCHE+1" | bc`
    fi
}

success_fail_print_exit()
{
    $ECHO_ME "\n-------------------------------------------------------------"
    if [ $NBFAILURE -eq 0 ]; then
        $ECHO_ME -n $BGREEN$NBFAILURE$NORMAL "failures, " $BGREEN$NBSUCCESS$NORMAL "success, " $BGREEN$NBTESTCHE$NORMAL "test checked \n"
        $ECHO_ME "-------------------------------------------------------------"
        exit 0
    else
        $ECHO_ME -n $BRED$NBFAILURE$NORMAL "failures, " $BRED$NBSUCCESS$NORMAL "success, " $BRED$NBTESTCHE$NORMAL "test checked \n"
        $ECHO_ME "-------------------------------------------------------------"
        exit 1
    fi
}

print_color_nbroots()
{
    ARG1=$1
    ARG2=$2
    ARG3=$3
    if [ "$ARG3" = "C" ] || [ -z $ARG3 ]; then
        if [ $ARG1 -ge $ARG2 ]; then
            $ECHO_ME -n $BGREEN$ARG1$NORMAL
        else
            $ECHO_ME -n $BRED$ARG1$NORMAL
        fi
    else
        if [ $ARG1 -ge $ARG2 ]; then
            $ECHO_ME -n $BGREEN$ARG1$NORMAL
        else 
            $ECHO_ME -n $BYELLOW$ARG1$NORMAL
        fi
    fi
}

print_color_success()
{
    ARG1=$1
    ARG2=$2
    ARG3=$3
    if [ $ARG1 -lt 0 ]; then
        $ECHO_ME -n $BRED$ARG1$NORMAL
    else
        if [ "$ARG3" = "C" ] || [ -z $ARG3 ]; then
            if [ $ARG1 -ge $ARG2 ]; then
                $ECHO_ME -n $BGREEN$ARG1$NORMAL
            else
                $ECHO_ME -n $BRED$ARG1$NORMAL
            fi
        else
            if [ $ARG1 -ge $ARG2 ]; then
                $ECHO_ME -n $BGREEN$ARG1$NORMAL
            else 
                $ECHO_ME -n $BYELLOW$ARG1$NORMAL
            fi
        fi
    fi
}

print_color_check()
{
    LCHECK=$1
    LCHECKOK=$2
    if [ $LCHECK -eq 1 ]; then
        if [ $LCHECKOK -eq 1 ]; then
            $ECHO_ME -n $BGREEN $LCHECKOK $NORMAL
        else
            $ECHO_ME -n $BRED $LCHECKOK $NORMAL
        fi
    else
        $ECHO_ME -n $BYELLOW "NOT CHECKED" $NORMAL
    fi
}

print_color_check_from_succ()
{
    LCHECK=$1
    LCHECKOK=$2
    LSUCC=$3
    if [ $LCHECK -eq 1 ]; then
        if [ $LSUCC -ge 0 ]; then
            if [ $LCHECKOK -eq 1 ]; then
                $ECHO_ME -n $BGREEN $LCHECKOK $NORMAL
            else
                $ECHO_ME -n $BRED $LCHECKOK $NORMAL
            fi
        else
            $ECHO_ME -n $BYELLOW "NOT CHECKED" $NORMAL
        fi
    else
        $ECHO_ME -n $BYELLOW "NOT CHECKED" $NORMAL
    fi
}

call_checks_earoots()
{
    LDEGR=$1
    LGOAL=$2
    LEPS=$3
    LDOM=$4
    LNBR=$5
    LCHECK=$6
    
    LCHECKARG=""
    if [ $LCHECK -eq 1 ]; then
        LCHECKARG="-c"
    fi
    
    $EAROOTS_CALL $FILENAME_IN -g $LGOAL -e $LEPS -d $LDOM -n $LNBR $LCHECKARG > $FILENAME_OU
    TIMEINI=$(grep "time for isolating" $FILENAME_OU | cut -f2 -d':')
    SUCCESS=$(grep "success" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    CHECKOK=$(grep "check output" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    $ECHO_ME -n " check OK: "
    print_color_check_from_succ $LCHECK $CHECKOK $SUCCESS
    $ECHO_ME -n " success: "
    if [ $LNBR -lt 0 ] || [ $LNBR -gt $LDEGR ]; then
        LNBR=$LDEGR
    fi
    print_color_success $SUCCESS $LNBR $LDOM
    $ECHO_ME -n " time: $TIMEINI"
    $ECHO_ME " "
    success_fail_update_from_succ $LCHECK $CHECKOK $SUCCESS
}

call_valgrind_checks_earoots()
{
    LDEGR=$1
    LGOAL=$2
    LEPS=$3
    LDOM=$4
    LNBR=$5
    LCHECK=$6
    
    LCHECKARG=""
    if [ $LCHECK -eq 1 ]; then
        LCHECKARG="-c"
    fi
    
    ($VALGRIND_CALL $EAROOTS_CALL $FILENAME_IN -g $LGOAL -e $LEPS -d $LDOM -n $LNBR $LCHECKARG > $FILENAME_OU ) &>> $FILENAME_OU
    TIMEINI=$(grep "time for isolating" $FILENAME_OU | cut -f2 -d':')
    SUCCESS=$(grep "success" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    CHECKOK=$(grep "check output" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    ERRORS=$(grep "ERROR SUMMARY"          $FILENAME_OU | cut -f2 -d':' | cut -f1 -d 'e' | tr -d ' ')
    if [ $ERRORS -ge 1 ]; then
        CHECKOK=0
    else
        CHECKOK=1
    fi
    $ECHO_ME -n " check OK: "
    print_color_check_from_succ $LCHECK $CHECKOK $SUCCESS
#     print_color_check $LCHECK $CHECKOK
    $ECHO_ME -n " success: "
    if [ $LNBR -lt 0 ] || [ $LNBR -gt $LDEGR ]; then
        LNBR=$LDEGR
    fi
    print_color_success $SUCCESS 1
    $ECHO_ME " "
    success_fail_update 1 $CHECKOK
}

call_checks_earoots_mp()
{
    LDEGR=$1
    LGOAL=$2
    LEPS=$3
    LDOM=$4
    LNBR=$5
    LCHECK=$6
    
    LCHECKARG=""
    if [ $LCHECK -eq 1 ]; then
        LCHECKARG="-c"
    fi
    
    $EAROOTS_MP_CALL $FILENAME_IN -i 1 -g $LGOAL -e $LEPS -d $LDOM -n $LNBR $LCHECKARG > $FILENAME_OU
    TIMEINI=$(grep "time for isolating" $FILENAME_OU | cut -f2 -d':')
    SUCCESS=$(grep "success" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    CHECKOK=$(grep "check output" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    $ECHO_ME -n " check OK: "
    print_color_check $LCHECK $CHECKOK
    $ECHO_ME -n " success: "
    if [ $LNBR -lt 0 ] || [ $LNBR -gt $LDEGR ]; then
        LNBR=$LDEGR
    fi
    print_color_success $SUCCESS $LNBR $LDOM
    $ECHO_ME -n " time: $TIMEINI"
    $ECHO_ME " "
    success_fail_update $LCHECK $CHECKOK
}

call_valgrind_checks_earoots_mp()
{
    LDEGR=$1
    LGOAL=$2
    LEPS=$3
    LDOM=$4
    LNBR=$5
    LCHECK=$6
    
    LCHECKARG=""
    if [ $LCHECK -eq 1 ]; then
        LCHECKARG="-c"
    fi
    
    ($VALGRIND_CALL $EAROOTS_MP_CALL $FILENAME_IN -i 1 -g $LGOAL -e $LEPS -d $LDOM -n $LNBR $LCHECKARG > $FILENAME_OU ) &>> $FILENAME_OU
    TIMEINI=$(grep "time for isolating" $FILENAME_OU | cut -f2 -d':')
    SUCCESS=$(grep "success" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    CHECKOK=$(grep "check output" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    ERRORS=$(grep "ERROR SUMMARY"          $FILENAME_OU | cut -f2 -d':' | cut -f1 -d 'e' | tr -d ' ')
    if [ $ERRORS -ge 1 ]; then
        CHECKOK=0
    else
        CHECKOK=1
    fi
    $ECHO_ME -n " check OK: "
    print_color_check 1 $CHECKOK
    $ECHO_ME -n " success: "
    if [ $LNBR -lt 0 ] || [ $LNBR -gt $LDEGR ]; then
        LNBR=$LDEGR
    fi
    print_color_success $SUCCESS $LNBR $LDOM
    $ECHO_ME -n " time: $TIMEINI"
    $ECHO_ME " "
    success_fail_update 1 $CHECKOK
}

call_checks_CoCo()
{
    LDEGR=$1
    LGOAL=$2
    LEPS=$3
    LDOM=$4
    LNBR=$5
    LCHECK=$6
    
    LCHECKARG=""
    if [ $LCHECK -eq 1 ]; then
        LCHECKARG="-c"
    fi
    
    $COCO_CALL $FILENAME_IN -g $LGOAL -e $LEPS -d $LDOM -n $LNBR $LCHECKARG > $FILENAME_OU
    TIMEINI=$(grep "time for approximating" $FILENAME_OU | cut -f2 -d':')
    NBROOTT=$(grep "number of roots" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    NBCLUST=$(grep "number of clusters" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    CHECKOK=$(grep "check output" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    $ECHO_ME -n " check OK: "
    print_color_check $LCHECK $CHECKOK
    $ECHO_ME -n " number of roots: "
    if [ $LNBR -lt 0 ] || [ $LNBR -gt $LDEGR ]; then
        LNBR=$LDEGR
    fi
    print_color_nbroots $NBROOTT $LNBR $LDOM
    $ECHO_ME -n " in $NBCLUST clusters, "
    $ECHO_ME -n " time: $TIMEINI"
    $ECHO_ME " "
    success_fail_update $LCHECK $CHECKOK
}

call_valgrind_checks_CoCo()
{
    LDEGR=$1
    LGOAL=$2
    LEPS=$3
    LDOM=$4
    LNBR=$5
    LCHECK=$6
    
    LCHECKARG=""
    if [ $LCHECK -eq 1 ]; then
        LCHECKARG="-c"
    fi
    
    ($VALGRIND_CALL $COCO_CALL $FILENAME_IN -g $LGOAL -e $LEPS -d $LDOM -n $LNBR $LCHECKARG > $FILENAME_OU) &>> $FILENAME_OU
#     cat $FILENAME_OU
    TIMEINI=$(grep "time for approximating" $FILENAME_OU | cut -f2 -d':')
    NBROOTT=$(grep "number of roots" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    NBCLUST=$(grep "number of clusters" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    CHECKOK=$(grep "check output" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    ERRORS=$(grep "ERROR SUMMARY"          $FILENAME_OU | cut -f2 -d':' | cut -f1 -d 'e' | tr -d ' ')
    if [ $ERRORS -ge 1 ]; then
        CHECKOK=0
    else
        CHECKOK=1
    fi
    $ECHO_ME -n " check OK: "
    print_color_check 1 $CHECKOK
    $ECHO_ME -n " number of roots: "
    if [ $LNBR -lt 0 ] || [ $LNBR -gt $LDEGR ]; then
        LNBR=$LDEGR
    fi
    print_color_nbroots $NBROOTT $LNBR $LDOM
    $ECHO_ME -n " in $NBCLUST clusters, "
    $ECHO_ME -n " time: $TIMEINI"
    $ECHO_ME " "
    success_fail_update 1 $CHECKOK
}

call_checks_pwroots()
{
    LDEGR=$1
    LGOAL=$2
    LEPS=$3
    LDOM=$4
    LNBR=$5
    LM=$6
    LINCREASE=$7
    LCHECK=$8
    LSOLVER=$9
    LROUNDINPUT=${10}
    LBFACT=${11}
    
    LCHECKARG=""
    if [ $LCHECK -eq 1 ]; then
        LCHECKARG="-c"
    fi
    
    LINCREASEARG=""
    if [ $LINCREASE -eq 0 ]; then
        LINCREASEARG="-f"
    fi
    
    CALL_ARGS="$FILENAME_IN -g $LGOAL -e $LEPS -d $LDOM -n $LNBR -m $LM $LINCREASEARG $LCHECKARG -s $LSOLVER -r $LROUNDINPUT -b $LBFACT"
    $PWROOTS_CALL $CALL_ARGS > $FILENAME_OU
#     ($TIME_CALL $TIME_FORM $PWROOTS_CALL $CALL_ARGS > $FILENAME_OU) &>> $FILENAME_OU
    
    TIMEINI=$(grep "time for isolating"    $FILENAME_OU | cut -f2 -d':')
#     TIMEREA=$(grep "real" $FILENAME_OU| cut -f2 -d'l' | tr -d ' ' | tr -d '\t')
#     TIMECPU=$(grep "cpu"  $FILENAME_OU| cut -f2 -d'u' | tr -d ' ' | tr -d '\t')
    NBROOTT=$(grep "total number of roots" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    CHECKOK=$(grep "check output"          $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    FINALM=$(grep "final m"                $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    
    $ECHO_ME -n " check OK: "
    print_color_check $LCHECK $CHECKOK
    $ECHO_ME -n " number of roots: "
    if [ $LNBR -lt 0 ] || [ $LNBR -gt $LDEGR ]; then
        LNBR=$LDEGR
    fi
    print_color_nbroots $NBROOTT $LNBR $LDOM
    $ECHO_ME -n " final m: $FINALM"
    $ECHO_ME -n " time: $TIMEINI"
#     $ECHO_ME -n " time: real: $TIMEREA cpu: $TIMECPU"
    $ECHO_ME " "
    success_fail_update $LCHECK $CHECKOK
}

call_valgrind_checks_pwroots()
{
    LDEGR=$1
    LGOAL=$2
    LEPS=$3
    LDOM=$4
    LNBR=$5
    LM=$6
    LINCREASE=$7
    LCHECK=$8
    LSOLVER=$9
    LROUNDINPUT=${10}
    LBFACT=${11}
    
    LCHECKARG=""
    if [ $LCHECK -eq 1 ]; then
        LCHECKARG="-c"
    fi
    
    LINCREASEARG=""
    if [ $LINCREASE -eq 0 ]; then
        LINCREASEARG="-f"
    fi
        
    ($VALGRIND_CALL $PWROOTS_CALL $FILENAME_IN -g $LGOAL -e $LEPS -d $LDOM -n $LNBR -m $LM $LINCREASEARG $LCHECKARG -s $LSOLVER -r $LROUNDINPUT -b $LBFACT > $FILENAME_OU ) &>> $FILENAME_OU
    TIMEINI=$(grep "time for isolating"    $FILENAME_OU | cut -f2 -d':')
    NBROOTT=$(grep "total number of roots" $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    CHECKOK=$(grep "check output"          $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    FINALM=$(grep "final m"                $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    ERRORS=$(grep "ERROR SUMMARY"          $FILENAME_OU | cut -f2 -d':' | cut -f1 -d 'e' | tr -d ' ')
    if [ $ERRORS -ge 1 ]; then
        CHECKOK=0
    else
        CHECKOK=1
    fi
    $ECHO_ME -n " check OK: "
    print_color_check 1 $CHECKOK
    $ECHO_ME -n " number of roots: "
    print_color_nbroots $NBROOTT $LDEGR $LDOM
    if [ $LNBR -lt 0 ] || [ $LNBR -gt $LDEGR ]; then
        LNBR=$LDEGR
    fi
    $ECHO_ME -n " final m: $FINALM"
    $ECHO_ME " "
    success_fail_update 1 $CHECKOK
}

call_checks_evaluate()
{
    LM=$1
    LCHECK=$2
    LPOINTS=$3
    
    LCHECKARG=""
    if [ $LCHECK -eq 1 ]; then
        LCHECKARG="-c"
    fi
    
    $PWEVAL_CALL $FILENAME_IN $LPOINTS -m $LM $LCHECKARG > $FILENAME_OU
    CHECKOK=$(grep "check output"          $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    $ECHO_ME -n " check OK: "
    print_color_check $LCHECK $CHECKOK
    $ECHO_ME " "
    success_fail_update $LCHECK $CHECKOK
}

call_valgrind_checks_evaluate()
{
    LM=$1
    LCHECK=$2
    LPOINTS=$3
    
    LCHECKARG=""
    if [ $LCHECK -eq 1 ]; then
        LCHECKARG="-c"
    fi
    
    ($VALGRIND_CALL $PWEVAL_CALL $FILENAME_IN $LPOINTS -m $LM $LCHECKARG > $FILENAME_OU ) &>> $FILENAME_OU
    CHECKOK=$(grep "check output"          $FILENAME_OU | cut -f2 -d':' | tr -d ' ')
    ERRORS=$(grep "ERROR SUMMARY"          $FILENAME_OU | cut -f2 -d':' | cut -f1 -d 'e' | tr -d ' ')
    if [ $ERRORS -ge 1 ]; then
        CHECKOK=0
    else
        CHECKOK=1
    fi
    $ECHO_ME -n " check OK: "
    print_color_check 1 $CHECKOK
    $ECHO_ME " "
    success_fail_update 1 $CHECKOK
}
