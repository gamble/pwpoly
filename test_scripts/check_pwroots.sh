#!/bin/bash

CHECK_NAME="pwroots"

source params_check.sh
source globals_check.sh
source functions_check.sh

CHECK=1
INCREASE=1

MVAL=10
# SOLVER=63 defined in params_check.sh
ROUNDINPUT=0
BFACT="2/5"
GOAL="a"
EPS="-53"
# EPS="-530"
BITSIZE=32

SEED=1

if [ $MEMORY -eq 1 ]; then

    $GENPOLFILE_CALL "randomDense" 512 -b $BITSIZE -s $SEED -f 1 -o $FILENAME_IN
    $ECHO_ME "------------- VALGRIND $SEED-th randomDenseHyp pol. of deg 512 with M=$MVAL---------------- "
    call_valgrind_checks_pwroots 512 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE 0 $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    if [ $LONG -eq 1 ]; then
        $ECHO_ME "------------- VALGRIND $SEED-th randomDenseRes pol. of deg 900 with M=$MVAL---------------- "
        cp $TESTFI_PATH"randomDenseRes_30_8.pw" $FILENAME_IN
        call_valgrind_checks_pwroots 900 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE 0 $SOLVER $ROUNDINPUT $BFACT
        rm -rf $FILENAME_IN $FILENAME_OU
    fi
    
    $ECHO_ME "------------- Mignotte polynomial of deg 129 and bitsize 8 with M=$MVAL---------------- "
    $GENPOLFILE_CALL "Mignotte" 129 -b 8 -f 1 -o $FILENAME_IN
    call_valgrind_checks_pwroots 129 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE 0 $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    $ECHO_ME "------------- Bernoulli polynomial of deg 64 with M=$MVAL---------------- "
    $GENPOLFILE_CALL "Bernoulli" 64 -f 1 -o $FILENAME_IN
    call_valgrind_checks_pwroots 64 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE 0 $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

else 

    $ECHO_ME "------------- randomDenseHyp pol. of deg 1000 with 0 trailing coeff with M=$MVAL---------------- "
    cp $TESTFI_PATH"randomDenseHyp0_1000_8.pw" $FILENAME_IN
    call_checks_pwroots 1000 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    $ECHO_ME "------------- 53 bits approx of randomDenseFlat pol. of deg 1000 with 0 trailing coeff with M=$MVAL---------------- "
    cp $TESTFI_PATH"randomDenseFlat0_1000_8_53.pw" $FILENAME_IN
    call_checks_pwroots 1000 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    $ECHO_ME "------------- 53 bits approx of randomDenseEll pol. of deg 1000 with 0 trailing coeff with M=$MVAL---------------- "
    cp $TESTFI_PATH"randomDenseEll0_1000_8_53.pw" $FILENAME_IN
    call_checks_pwroots 1000 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    DEGR=129
    MVAL=10
    $ECHO_ME "------------- Bernoulli polynomial of deg $DEGR with M=$MVAL---------------- "
    $GENPOLFILE_CALL "Bernoulli" $DEGR -f 1 -o $FILENAME_IN
    call_checks_pwroots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    MVAL=40
    $ECHO_ME "------------- Bernoulli polynomial of deg $DEGR with M=$MVAL---------------- "
    $GENPOLFILE_CALL "Bernoulli" $DEGR -f 1 -o $FILENAME_IN
    call_checks_pwroots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    BFACT="1/10"
    $ECHO_ME "------------- Bernoulli polynomial of deg $DEGR with M=$MVAL and B=$BFACT---------------- "
    $GENPOLFILE_CALL "Bernoulli" $DEGR -f 1 -o $FILENAME_IN
    call_checks_pwroots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    MVAL=10
    BFACT="2/5"
    $ECHO_ME "------------- Mignotte polynomial of deg $DEGR and bitsize $BITSIZE with M=$MVAL---------------- "
    $GENPOLFILE_CALL "Mignotte" $DEGR -b $BITSIZE -f 1 -o $FILENAME_IN
    call_checks_pwroots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    # cat $FILENAME_OU
    rm -rf $FILENAME_IN $FILENAME_OU

    DEGR=63
    $ECHO_ME "------------- Mandelbrot polynomial of deg $DEGR with M=$MVAL---------------- "
    $GENPOLFILE_CALL "Mandelbrot" 6 -f 1 -o $FILENAME_IN
    call_checks_pwroots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    BFACT="2/5"    
    MVAL=10
    DEGREES="1000 2001"
    # SEEDS="1 2 3 4 5"
    SEEDS="1 2"

    for DEGR in $DEGREES; do
    for SEED in $SEEDS; do
    #     $GENPOLFILE_CALL "randomDenseHyp" $DEGR -s $SEED -L 53 -f 1 -o $FILENAME_IN
        $GENPOLFILE_CALL "randomDense" $DEGR -b $BITSIZE -s $SEED -f 1 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randomDenseHyp pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_pwroots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
        rm -rf $FILENAME_IN $FILENAME_OU

        $GENPOLFILE_CALL "randomDenseFlat" $DEGR -b $BITSIZE -s $SEED -L 53 -f 1 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randomDenseFlat pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_pwroots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
        rm -rf $FILENAME_IN $FILENAME_OU

        $GENPOLFILE_CALL "randomDenseEll" $DEGR -b $BITSIZE -s $SEED -L 53 -f 1 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randomDenseEll pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_pwroots $DEGR $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
        rm -rf $FILENAME_IN $FILENAME_OU
    done
    done 

    MVAL=10
    cp $TESTFI_PATH"randomDenseRes_30_8.pw" $FILENAME_IN
    $ECHO_ME "------------- randomDenseRes pol. of deg 30^2 with M=$MVAL---------------- "
    call_checks_pwroots 900 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    CHECK=0
    MVAL=30
    cp $TESTFI_PATH"randomDenseRes_30_8.pw" $FILENAME_IN
    $ECHO_ME "------------- randomDenseRes pol. of deg 30^2 with M=$MVAL---------------- "
    call_checks_pwroots 900 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    MVAL=30
    cp $TESTFI_PATH"randomDenseRes_40_8.pw" $FILENAME_IN
    $ECHO_ME "------------- randomDenseRes pol. of deg 40^2 with M=$MVAL---------------- "
    call_checks_pwroots 1600 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

    MVAL=30
    cp $TESTFI_PATH"randomDenseRes_50_8.pw" $FILENAME_IN
    $ECHO_ME "------------- randomDenseRes pol. of deg 50^2 with M=$MVAL---------------- "
    call_checks_pwroots 2500 $GOAL $EPS $DOMAIN $NBROOTS $MVAL $INCREASE $CHECK $SOLVER $ROUNDINPUT $BFACT
    rm -rf $FILENAME_IN $FILENAME_OU

fi

rm -rf "roots.txt"
rm -rf "covering_with_roots.plt"
rm -rf "roots.plt"
#override SILENT
ECHO_ME="/bin/echo -e"
success_fail_print_exit
