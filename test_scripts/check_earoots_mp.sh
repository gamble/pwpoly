#!/bin/bash

CHECK_NAME="earoots_mp"

source params_check.sh
source globals_check.sh
source functions_check.sh

CHECK=1
EPS=-54
GOAL=a

if [ $MEMORY -eq 1 ]; then
    
    DEGREES="51 52"
    for DEGR in $DEGREES; do
        $GENPOLFILE_CALL "Bernoulli" $DEGR -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- VALGRIND Bernoulli pol. of deg $DEGR ---------------- "
        call_valgrind_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS 0
        rm -rf $FILENAME_IN $FILENAME_OU
    done

else

    EPS=-53
    DEGREES="51 102"
    SEEDS="1 2 3 4 5"
    
    if [ $SHORT -eq 1 ]; then
        SEEDS="3"
    fi

    for DEGR in $DEGREES; do
    for SEED in $SEEDS; do
        $GENPOLFILE_CALL "randomDenseHyp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseHyp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU

        $GENPOLFILE_CALL "randomDenseFlat" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseFlat pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU

        $GENPOLFILE_CALL "randomDenseEll" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseEll pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
        
        $GENPOLFILE_CALL "randomDenseHypComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseHypComp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU

        $GENPOLFILE_CALL "randomDenseFlatComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseFlatComp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU

        $GENPOLFILE_CALL "randomDenseEllComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseEllComp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
    done
    done 

    DEGREES="53 100 157"
    EPSVALS="-10 -100 -1000"
    if [ $SHORT -eq 1 ]; then
        EPSVALS="-10 -100"
    fi
    GOAL=a

    for DEGR in $DEGREES; do
        $GENPOLFILE_CALL "Mignotte" $DEGR -o $FILENAME_IN
        $ECHO_ME "-------------Isolate Mignotte pol. of deg $DEGR---------------- "
        call_checks_earoots_mp $DEGR i $EPS $DOMAIN $NBROOTS $CHECK
        for EPS in $EPSVALS; do
            $ECHO_ME "-------------Approximate (relative) Mignotte pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_earoots_mp $DEGR a $EPS $DOMAIN $NBROOTS $CHECK
            $ECHO_ME "-------------Approximate (absolute) Mignotte pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_earoots_mp $DEGR aa $EPS $DOMAIN $NBROOTS $CHECK
            $ECHO_ME "-------------Isolate and Approximate (relative) Mignotte pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_earoots_mp $DEGR ia $EPS $DOMAIN $NBROOTS $CHECK
        done
        $ECHO_ME " "
        rm -rf $FILENAME_IN $FILENAME_OU
    done

    DEGREES="1 2 3 4 5 6 7 8 9 31 39 43 61 99"
    if [ $SHORT -eq 1 ]; then
        DEGREES="9 99"
    fi
    for DEGR in $DEGREES; do
        $GENPOLFILE_CALL "Bernoulli" $DEGR -o $FILENAME_IN
        $ECHO_ME "-------------Isolate Bernoulli pol. of deg $DEGR---------------- "
        call_checks_earoots_mp $DEGR i $EPS $DOMAIN $NBROOTS $CHECK
        for EPS in $EPSVALS; do
            $ECHO_ME "-------------Approximate (relative) Bernoulli pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_earoots_mp $DEGR a $EPS $DOMAIN $NBROOTS $CHECK
            $ECHO_ME "-------------Approximate (absolute) Bernoulli pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_earoots_mp $DEGR aa $EPS $DOMAIN $NBROOTS $CHECK
            $ECHO_ME "-------------Isolate and Approximate (relative) Bernoulli pol. of deg $DEGR with log2eps=$EPS---------------- "
            call_checks_earoots_mp $DEGR ia $EPS $DOMAIN $NBROOTS $CHECK
        done
        $ECHO_ME " "
        rm -rf $FILENAME_IN $FILENAME_OU
    done

    DEGREES="3 4 5 6 7 8"
    if [ $SHORT -eq 1 ]; then
        DEGREES="5 6"
    fi
    
    for DEGR in $DEGREES; do
        DEGREE=`echo "2^$DEGR-1" | bc`
        $GENPOLFILE_CALL "Mandelbrot" $DEGR -o $FILENAME_IN
        $ECHO_ME "-------------Isolate Mandelbrot pol. of deg 2^$DEGR-1 = $DEGREE ---------------- "
        call_checks_earoots_mp $DEGREE i $EPS $DOMAIN $NBROOTS $CHECK
        for EPS in $EPSVALS; do
            $ECHO_ME "-------------Approximate (relative) Mandelbrot pol. of deg $DEGREE with log2eps=$EPS---------------- "
            call_checks_earoots_mp $DEGREE a $EPS $DOMAIN $NBROOTS $CHECK
            $ECHO_ME "-------------Approximate (absolute) Mandelbrot pol. of deg $DEGREE with log2eps=$EPS---------------- "
            call_checks_earoots_mp $DEGREE aa $EPS $DOMAIN $NBROOTS $CHECK
            $ECHO_ME "-------------Isolate and Approximate (relative) Mandelbrot pol. of deg $DEGREE with log2eps=$EPS---------------- "
            call_checks_earoots_mp $DEGREE ia $EPS $DOMAIN $NBROOTS $CHECK
        done
        $ECHO_ME " "
        rm -rf $FILENAME_IN $FILENAME_OU
    done

    DEGREES="256"
    SEEDS="1"
    
    if [ $LONG -eq 1 ]; then
    
        DEGREES="256 512 767 1024 1513"
        SEEDS="1 2 3 4 5"
    fi
        
    for DEGR in $DEGREES; do
    for SEED in $SEEDS; do
        $GENPOLFILE_CALL "randomDenseHyp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseHyp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
    
        $GENPOLFILE_CALL "randomDenseFlat" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseFlat pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
    
        $GENPOLFILE_CALL "randomDenseEll" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseEll pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
        
        $GENPOLFILE_CALL "randomDenseHypComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseHypComp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
    
        $GENPOLFILE_CALL "randomDenseFlatComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseFlatComp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
    
        $GENPOLFILE_CALL "randomDenseEllComp" $DEGR -s $SEED -L 53 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randonDenseEllComp pol. of deg $DEGR with log2eps=$EPS---------------- "
        call_checks_earoots_mp $DEGR $GOAL $EPS $DOMAIN $NBROOTS $CHECK
        rm -rf $FILENAME_IN $FILENAME_OU
    done
    done
    
fi

rm -rf "roots.txt"
rm -rf "roots.plt"
rm -rf "init_points.plt"
#override SILENT
ECHO_ME="/bin/echo -e"
success_fail_print_exit
