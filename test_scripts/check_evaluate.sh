#!/bin/bash

CHECK_NAME="pweval"

source params_check.sh
source globals_check.sh
source functions_check.sh

CHECK=1
M=53
SEED=1
BITSIZE=32

if [ $MEMORY -eq 1 ]; then
    DEGREE="250"
    SEED="1"
    MVAL="53"
    $GENPOIFILE_CALL "random" 200 -o $FILENAME_IN_POINT_FMPQ -f "fmpq"
    $GENPOIFILE_CALL "random" 200 -o $FILENAME_IN_POINT_DOUB -f "double"
    $GENPOIFILE_CALL "random" 200 -o $FILENAME_IN_POINT_ACBS -f "acb"
    $GENPOLFILE_CALL "randomDense" $DEGREE -b $BITSIZE -s $SEED -f 1 -o $FILENAME_IN
    $ECHO_ME "------------- VALGRIND $SEED-th randomDenseHyp pol. of deg $DEGREE with M=$MVAL, no input---------------- "
    call_valgrind_checks_evaluate $MVAL $CHECK
    $ECHO_ME "------------- VALGRIND $SEED-th randomDenseHyp pol. of deg $DEGREE with M=$MVAL, fmpq input-------------- "
    call_valgrind_checks_evaluate $MVAL $CHECK $FILENAME_IN_POINT_FMPQ
    $ECHO_ME "------------- VALGRIND $SEED-th randomDenseHyp pol. of deg $DEGREE with M=$MVAL, double input------------ "
    call_valgrind_checks_evaluate $MVAL $CHECK $FILENAME_IN_POINT_DOUB
    $ECHO_ME "------------- VALGRIND $SEED-th randomDenseHyp pol. of deg $DEGREE with M=$MVAL, acb input--------------- "
    call_valgrind_checks_evaluate $MVAL $CHECK $FILENAME_IN_POINT_ACBS
    
else

    DEGREE="500"
    SEED="1"
    MVAL="53"
    
    $GENPOIFILE_CALL "random" 200 -o $FILENAME_IN_POINT_FMPQ -f "fmpq"
    $GENPOIFILE_CALL "random" 200 -o $FILENAME_IN_POINT_DOUB -f "double"
    $GENPOIFILE_CALL "random" 200 -o $FILENAME_IN_POINT_ACBS -f "acb"
    $GENPOLFILE_CALL "randomDense" $DEGREE -b $BITSIZE -s $SEED -f 1 -o $FILENAME_IN
    $ECHO_ME "------------- $SEED-th randomDenseHyp pol. of deg $DEGREE with M=$MVAL, no input---------------- "
    call_checks_evaluate $MVAL $CHECK
    $ECHO_ME "------------- $SEED-th randomDenseHyp pol. of deg $DEGREE with M=$MVAL, fmpq input-------------- "
    call_checks_evaluate $MVAL $CHECK $FILENAME_IN_POINT_FMPQ
    $ECHO_ME "------------- $SEED-th randomDenseHyp pol. of deg $DEGREE with M=$MVAL, double input------------ "
    call_checks_evaluate $MVAL $CHECK $FILENAME_IN_POINT_DOUB
    $ECHO_ME "------------- $SEED-th randomDenseHyp pol. of deg $DEGREE with M=$MVAL, acb input--------------- "
    call_checks_evaluate $MVAL $CHECK $FILENAME_IN_POINT_ACBS
    
    DEGREES="250 503"
    SEEDS="2 3"
    MVALS="53 106"
    
    if [ $SHORT -eq 1 ]; then
        DEGREES="250"
        SEEDS="3"
        MVALS="106"
    fi
    
    for DEGR in $DEGREES; do
    for SEED in $SEEDS; do
    for MVAL in $MVALS; do
        
        $GENPOLFILE_CALL "randomDense" $DEGR -b $BITSIZE -s $SEED -f 1 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randomDenseHyp pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_evaluate $MVAL $CHECK
        
        $GENPOLFILE_CALL "randomDenseComp" $DEGR -b $BITSIZE -s $SEED -f 1 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randomDenseHypComp pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_evaluate $MVAL $CHECK
        
        $GENPOLFILE_CALL "randomDenseFlat" $DEGR -b $BITSIZE -s $SEED -f 1 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randomDenseFlat pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_evaluate $MVAL $CHECK
        
        $GENPOLFILE_CALL "randomDenseFlatComp" $DEGR -b $BITSIZE -s $SEED -f 1 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randomDenseFlatComp pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_evaluate $MVAL $CHECK
        
        $GENPOLFILE_CALL "randomDenseEll" $DEGR -b $BITSIZE -s $SEED -f 1 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randomDenseEll pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_evaluate $MVAL $CHECK
        
        $GENPOLFILE_CALL "randomDenseFlatComp" $DEGR -b $BITSIZE -s $SEED -f 1 -o $FILENAME_IN
        $ECHO_ME "------------- $SEED-th randomDenseEllComp pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_evaluate $MVAL $CHECK
        
    done
    done
    done
    
    DEGREE="251"
    MVALS="50 500"
    $GENPOLFILE_CALL "Bernoulli" $DEGREE -f 1 -o $FILENAME_IN
    for MVAL in $MVALS; do
        $ECHO_ME "------------- Bernoulli pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_evaluate $MVAL $CHECK
    done
    
    $GENPOLFILE_CALL "Wilkinson" $DEGREE -f 1 -o $FILENAME_IN
    for MVAL in $MVALS; do
        $ECHO_ME "------------- Wilkinson pol. of deg $DEGR with M=$MVAL---------------- "
        call_checks_evaluate $MVAL $CHECK
    done
    
fi

rm -rf $FILENAME_IN $FILENAME_OU
rm -rf $FILENAME_IN_POINT_FMPQ $FILENAME_IN_POINT_DOUB $FILENAME_IN_POINT_ACBS
rm -rf "values.txt" "covering_with_points.plt"

ECHO_ME="/bin/echo -e"
success_fail_print_exit

# $GENPOLFILE_CALL "randomDense" $DEGR -s $SEED -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseHyp pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# $GENPOLFILE_CALL "randomDenseFlat" $DEGR -s $SEED -L 53 -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseFlat pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# $GENPOLFILE_CALL "randomDenseEll" $DEGR -s $SEED -L 53 -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseEll pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# MVAL=32
# DEGR=1000
# $GENPOLFILE_CALL "randomDense" $DEGR -s $SEED -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseHyp pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# $GENPOLFILE_CALL "randomDenseFlat" $DEGR -s $SEED -L 53 -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseFlat pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# $GENPOLFILE_CALL "randomDenseEll" $DEGR -s $SEED -L 53 -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseEll pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# MVAL=16
# DEGR=2000
# $GENPOLFILE_CALL "randomDense" $DEGR -s $SEED -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseHyp pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# $GENPOLFILE_CALL "randomDenseFlat" $DEGR -s $SEED -L 53 -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseFlat pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# $GENPOLFILE_CALL "randomDenseEll" $DEGR -s $SEED -L 53 -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseEll pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# MVAL=32
# DEGR=2000
# $GENPOLFILE_CALL "randomDense" $DEGR -s $SEED -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseHyp pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# $GENPOLFILE_CALL "randomDenseFlat" $DEGR -s $SEED -L 53 -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseFlat pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# $GENPOLFILE_CALL "randomDenseEll" $DEGR -s $SEED -L 53 -o $FILENAME_IN
# $ECHO_ME "------------- randonDenseEll pol. of deg $DEGR with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# MVAL=16
# cp $TESTFI_PATH"randomDenseRes_30_8.pw" $FILENAME_IN
# $ECHO_ME "------------- randonDenseRes pol. of deg 30 with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# cp $TESTFI_PATH"randomDenseRes_40_8.pw" $FILENAME_IN
# $ECHO_ME "------------- randonDenseRes pol. of deg 40 with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# cp $TESTFI_PATH"randomDenseRes_50_8.pw" $FILENAME_IN
# $ECHO_ME "------------- randonDenseRes pol. of deg 50 with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# MVAL=32
# cp $TESTFI_PATH"randomDenseRes_30_8.pw" $FILENAME_IN
# $ECHO_ME "------------- randonDenseRes pol. of deg 30 with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# cp $TESTFI_PATH"randomDenseRes_40_8.pw" $FILENAME_IN
# $ECHO_ME "------------- randonDenseRes pol. of deg 40 with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
# 
# cp $TESTFI_PATH"randomDenseRes_50_8.pw" $FILENAME_IN
# $ECHO_ME "------------- randonDenseRes pol. of deg 50 with M=$MVAL---------------- "
# call_checks_evaluate $MVAL
# rm -rf $FILENAME_IN $FILENAME_OU
