/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

/*********************/
/* includes          */
/*********************/

#include <stdio.h>
#include <time.h>

/* from pwpoly */
#include "pw_base.h"
#include "common/pw_common.h"     /* for I/O             */
#include "solving/pw_solving.h"   /* for solving         */
#include "geometry/domain.h"      /* for parsing domains */

/*********************/
/* global variables  */
/*********************/
acb_poly_t pacb;
fmpq_poly_t pfmpq_re;
fmpq_poly_t pfmpq_im;
slong length;
int c;
fmpq_t scale, ratio;
int verbosity, increase, check, goal, solver;
slong m, log2eps, roundInput, goalNbRoots;
domain_t dom;

void init_global_variables();

void clear_global_variables();

/*********************/
/* main              */
/*********************/
int main(int argc, char* argv[]) {
    
    int level = 1; /* verbosity level */
    
    if ( (argc<2) || (strcmp( argv[1], "-h" ) == 0) 
                  || (strcmp( argv[1], "--help" ) == 0) ) {
        pwroots_fprint_options( stdout, argc, argv );
        exit(0);
    }
    
    init_global_variables();
    int parse = parseInputArgs( argc, argv, &verbosity, dom, &goalNbRoots, &m, &increase, &check, &log2eps, &goal, &solver, &roundInput, ratio );
    
    /* type is either PW_PARSE_FAILD, PW_PARSE_EXACT for integer/rational polynomials */
    /*             or PW_PARSE_BALLS for acb poly                                     */
    /* length is the degree+1 of the input poly                                       */
    /* fills pacb if PW_PARSE_BALLS, pfmpq_re and pfmpq_im if PW_PARSE_EXACT          */
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if ( (type==PW_PARSE_FAILD) || (parse==0) ){
        clear_global_variables();
        flint_cleanup();
        exit(0);
    }
    
    if (verbosity>=level) {
        printf("m          : %ld\n", m);
        printf("increase   : %d\n", increase);
        printf("goal       : "); pwroots_fprint_goal(stdout, goal); printf("\n");
        printf("ratio      : "); fmpq_print(ratio); printf("\n");
        printf("solver     : %d\n", solver);
        printf("roundInput : %ld\n", roundInput);
        printf("check      : %d\n", check);
        printf("verbosity  : %d\n", verbosity);
        printf("domain     : "); domain_print_short(dom); printf("\n"); 
        printf("goalNbRoots: %ld\n", goalNbRoots);
    }
    
    int realCoeffs = 0;
    if (type==PW_PARSE_EXACT)
        realCoeffs = fmpq_poly_is_zero(pfmpq_im);
    else
        realCoeffs = acb_poly_is_real(pacb);
    
    /* init profiling */
#ifdef PW_PROFILE
    PW_INIT_PROFILE
#endif

#ifdef PW_COCO_PROFILE
    PW_COCO_INIT_PROFILE
#endif

#ifdef PW_EA_PROFILE
    PW_EA_INIT_PROFILE
#endif
    
    /* round input */
    if ((type==PW_PARSE_EXACT)&&(roundInput>0)) {
        type = PW_PARSE_BALLS;
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, roundInput );
        for (slong i=0; i<pacb->length; i++) {
            acb_get_mid( (pacb->coeffs) + i, (pacb->coeffs) + i );
        }
    }
    
    /* files for output */
    FILE * covrFile = NULL;
    char covrFileName[] = "covering_with_roots.plt\0";
    FILE * rootsFile = NULL;
    char rootsFileName[] = "roots.plt\0";
    FILE * outFile = NULL;
    char outFileName[] = "roots.txt\0";
    if (verbosity!=-1) {
        covrFile = fopen (covrFileName,"w");
        rootsFile = fopen (rootsFileName,"w");
        outFile = fopen (outFileName,"w");
    }
    
    /* allocate memory for the output */
    acb_ptr roots = _acb_vec_init(length-1);
    slong * mults = (slong *) pwpoly_malloc ( (length-1)*sizeof(slong) );
    slong nbroots = 0, nbclusts = 0, nbreals = 0;
    int nbrealsExact = 1;
    ulong wm = (ulong)m;
    
    /* start solving */
    clock_t start_isolate = clock();
    
    if (!increase) { /* special case: ignore the goal and try to isolate the roots of input */
                     /* with a piecewise polynomial approximation at precision m            */
                     /* m IS NOT INCREASED in case of failure                               */
        if (type==PW_PARSE_EXACT)
            nbroots  = pwpoly_isolate_m_2fmpq_poly( roots, pfmpq_re, pfmpq_im, wm, (ulong)c, scale, ratio, dom, goalNbRoots, solver, verbosity, covrFile );
        else
            nbroots  = pwpoly_isolate_m_acb_poly( roots, pacb, wm, (ulong)c, scale, ratio, dom, goalNbRoots, solver, verbosity, covrFile );
    
        
    } else { /* usual case: solve according to the goal */

        if (PW_GOAL_MUST_REFINE(goal)) {
            if (type==PW_PARSE_EXACT)
                nbclusts = pwpoly_refine_2fmpq_poly( roots, mults, pfmpq_re, pfmpq_im, goal, log2eps, dom, goalNbRoots, verbosity );
            else
                nbclusts = pwpoly_refine_acb_poly( roots, mults, pacb, goal, log2eps, dom, goalNbRoots, verbosity );
        } else { /* goal is isolate, approximate or approximate and isolate */
            if (type==PW_PARSE_EXACT)
                nbclusts = pwpoly_solve_2fmpq_poly( roots, mults, &wm, pfmpq_re, pfmpq_im, goal, log2eps, dom, goalNbRoots, (ulong)c, scale, ratio, solver, verbosity, covrFile );
            else
                nbclusts = pwpoly_solve_acb_poly( roots, mults, &wm, pacb, goal, log2eps, dom, goalNbRoots, (ulong)c, scale, ratio, solver, verbosity, covrFile );
        }
    }
    
    double clicks_in_isolate = (clock() - start_isolate);
     
    if (!increase) {
        /* set multiplicities to 1, get number of real roots */
        nbclusts = nbroots;
        for (slong i=0; i<nbroots; i++) {
            mults[i] = 1;
            if ((realCoeffs)&&(arb_is_zero(acb_imagref(roots+i))))
                nbreals++;
        }
    } else {
        /* get sum of multiplicities of roots: always equal to length-1 when domain=C and goalNbRoots=length-1 */
        /*                                     otherwise can be <> length-1                                    */
        /* get number of real roots                                                                            */
        for (slong i=0; i<nbclusts; i++) {
            nbroots += mults[i];
            if (realCoeffs) {
                if (arb_is_zero(acb_imagref(roots+i)))
                    nbreals += mults[i];
                else if ( arb_contains_zero(acb_imagref(roots+i)) && (mults[i]>1) )
                    nbrealsExact = 0;
            }
        }
    }
    
    /* print solving info */
    if ((verbosity>=level) || (verbosity==-1)) {
        printf("\n");
        printf("&&&&&&&&&&&& PWROOTS OUTPUT &&&&&&&&&&&&&&&&\n");
        printf(" time for isolating the roots  : %.2lf seconds\n",
             clicks_in_isolate/CLOCKS_PER_SEC) ;
        printf("     total number of clusters  : %ld\n", nbclusts);
        printf("     total number of roots     : %ld\n", nbroots);
        if (realCoeffs&&nbrealsExact)
        printf("         number of real roots  = %ld\n", nbreals);
        if (realCoeffs&&!nbrealsExact) 
        printf("         number of real roots >= %ld\n", nbreals);
        printf(" final m                       : %lu\n", wm); 
        printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
    }

#ifdef PW_PROFILE
    PW_PRINT_PROFILE
#endif

#ifdef PW_COCO_PROFILE
    PW_COCO_PRINT_PROFILE
#endif

#ifdef PW_EA_PROFILE
    PW_EA_PRINT_PROFILE
#endif

    /* write the roots */
    if (outFile != NULL) {
        pwpoly_write_roots_mults( outFile, roots, mults, nbclusts, 10 );
        fclose (outFile);
    } 
    if (rootsFile != NULL) {
        pw_roots_domain_gnuplot( rootsFile, roots, nbclusts, dom );
        fclose (rootsFile);
    }
    if (covrFile != NULL)
        fclose (covrFile);

    /* check the output */
    if (check) {
        
        clock_t start_check = clock();
        int checkOK = 0;
          
        if (type==PW_PARSE_EXACT){
            checkOK = pwpoly_checkOutput_solve_2fmpq_poly ( roots, mults, nbclusts, pfmpq_re, pfmpq_im, log2eps, goal, dom, (slong) goalNbRoots );
        } else {
            checkOK = pwpoly_checkOutput_solve_exact_acb_poly ( roots, mults, nbclusts, pacb, log2eps, goal, dom, (slong) goalNbRoots );
        }

        double clicks_in_check = (clock() - start_check);
        printf(" check output                    : %d\n", checkOK);
        printf(" time for checking output        : %lf seconds\n",
                 clicks_in_check/CLOCKS_PER_SEC) ;
    } else {
        if ((verbosity>=level) || (verbosity==-1)) {
            printf(" check output                    : NOT CHECKED\n");
        }
    }
    
    pwpoly_free(mults);
    _acb_vec_clear(roots, length-1);
    clear_global_variables();
    flint_cleanup();
    
    return 0;
}

void init_global_variables(){
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    length = 0;
    c      = 2;
    fmpq_init(scale);
    fmpq_init(ratio);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(ratio, 2, 5);
//     fmpq_set_si(ratio, 1, 10);
    verbosity = 1;
    m = 10;
    increase = 1;
    check = 0;
    log2eps = -53;
    goal = PW_GOAL_ISOLATE;
    solver = 63;
    roundInput = 0;
    domain_init(dom); /* by default set to C */
    goalNbRoots = -1; /* find all the roots */
}

void clear_global_variables(){
    domain_clear(dom);
    fmpq_clear(ratio);
    fmpq_clear(scale);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(pfmpq_re);
    acb_poly_clear(pacb);
}

