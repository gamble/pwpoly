/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

/*********************/
/* includes          */
/*********************/

#include <stdio.h>
#include "pw_base.h"
#include "common/pw_common.h"     /* for I/O             */

#define FORMAT_DOUB 0
#define FORMAT_FMPQ 1
#define FORMAT_ACB  2

void generateRandomPoints( fmpq_ptr points_re, fmpq_ptr points_im,
                           slong nbPoints, slong bitsize, flint_rand_t state );

void write_fmpq_points( FILE * file, const fmpq_ptr points_re, const fmpq_ptr points_im, slong nbPoints );

void write_double_points( FILE * file, const fmpq_ptr points_re, const fmpq_ptr points_im, slong nbPoints );

int acb_dump_file( FILE * file, const acb_t src );

int write_acb_points( FILE * file, const fmpq_ptr points_re, const fmpq_ptr points_im, slong nbPoints, slong prec );
/*********************/
/* main              */
/*********************/
int main(int argc, char* argv[]) {
    
//     int level = 1; /* verbosity level */
    
    if ( (argc<3) || (strcmp( argv[1], "-h" ) == 0) 
                  || (strcmp( argv[1], "--help" ) == 0) ) {
        printf(" usage: %s random nbPoints [OPTIONS: bitsize filename format precision seed]\n", argv[0]);
        printf("       -o , --output: the output file                                  \n");
        printf("                      stdout [default]                                 \n");
        printf("       -f , --format: the format of the output                      \n");
        printf("                      double: [default] doubles complex             \n");
        printf("                      fmpq: gaussian rational                       \n");
        printf("                      acb:  acb balls                               \n");
        printf("       -b , --bitsize: the bitsize of the points                    \n");
        printf("                      32: [default] or a positive integer           \n");
        printf("       -L , --precision: for acb format                             \n");
        printf("                      if not specified and format is acb, generated rational \n");
        printf("                      are rounded in floats with 53 bits of precision with no error \n");
        printf("                      if positive integer p, generated rational are rounded \n");
        printf("                      in floats with p bits of precision with appropriated error \n");
        printf("       -s , --seed: the seed for the random generator \n" );
        printf("                      1: [default] or a positive integer \n");
        exit(0);
    }
    
    slong nbPoints=0;
    char typename[100];
    char filename[100];
    int toStdout = 1;
    char format[100];
    int  formatInt = FORMAT_DOUB;
    slong bitsize = 32;
    slong prec = 0;
    int seed = 1;
    
    /* parse input */
    int parse=1;
    parse = parse*sscanf(argv[1], "%s", typename);
    parse = parse*sscanf(argv[2], "%ld", &nbPoints);
    if (nbPoints<0) {
        printf("%s ERROR: NON-VALID NBPOINTS: %ld (should be >=0) \n", argv[0], nbPoints);
        parse = 0;
    }
    /* loop on arguments to figure out options */
    for (int arg = 3; arg< argc; arg++) {
        if ( (strcmp( argv[arg], "-o" ) == 0) || (strcmp( argv[arg], "--output" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%s", filename);
                toStdout = 0;
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-f" ) == 0) || (strcmp( argv[arg], "--format" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%s", format);
                
                if (strcmp( format, "double" ) == 0)
                    formatInt = FORMAT_DOUB;
                else if (strcmp( format, "fmpq" ) == 0)
                    formatInt = FORMAT_FMPQ;
                else if (strcmp( format, "acb" ) == 0)
                    formatInt = FORMAT_ACB;
                else {
                    printf("%s ERROR: NON-VALID FORMAT (should be in {double, fmpq, acb}) \n", argv[0]);
                    parse = 0;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-b" ) == 0) || (strcmp( argv[arg], "--bitsize" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%ld", &bitsize);
                if (bitsize<=0){
                    printf("%s ERROR: NON-VALID BITSIZE: %ld (should >0) \n", argv[0], bitsize);
                    parse = 0;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-L" ) == 0) || (strcmp( argv[arg], "--precision" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%ld", &prec);
                if (prec<=0){
                    printf("%s ERROR: NON-VALID PRECISION: %ld (should >0) \n", argv[0], prec);
                    parse = 0;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-s" ) == 0) || (strcmp( argv[arg], "--seed" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%d", &seed);
                if (seed<=0){
                    printf("%s ERROR: NON-VALID SEED: %d (should >0) \n", argv[0], seed);
                    parse = 0;
                }
                arg++;
            }
        }
        
    }
    
    if (parse==0)
        exit(EXIT_FAILURE);
    
//     printf("type     : %s\n", typename );
//     if (toStdout)
//     printf("output   : stdout\n" );
//     else 
//     printf("output   : %s\n", filename );   
//     printf("formatInt: %d\n", formatInt );
//     printf("bitsize  : %ld\n", bitsize );
//     printf("precision: %ld\n", prec );
//     printf("seed     : %d\n", seed );
    
    srand(seed);
    flint_rand_t state;
    flint_randinit(state);
    
    fmpq_ptr points_re = _fmpq_vec_init(nbPoints);
    fmpq_ptr points_im = _fmpq_vec_init(nbPoints);
    
    if (strcmp(typename, "random")==0) {
        for (int s = 1; s < seed; s++)
            generateRandomPoints( points_re, points_im, nbPoints, bitsize, state );
        generateRandomPoints( points_re, points_im, nbPoints, bitsize, state );
    } else {
        printf("%s ERROR: UNKNOWN TYPE %s \n", argv[0], typename);
        _fmpq_vec_clear(points_im, nbPoints);
        _fmpq_vec_clear(points_re, nbPoints);
        flint_randclear(state);
        exit(EXIT_FAILURE);
    }
    
    FILE * curFile;
    if (toStdout)
        curFile = stdout;
    else {
        curFile = fopen (filename,"w");
        
        if (curFile==NULL) {
            printf("%s ERROR: FILE %s CAN NOT BE OPEN FOR WRITE \n", argv[0], filename);
            _fmpq_vec_clear(points_im, nbPoints);
            _fmpq_vec_clear(points_re, nbPoints);
            flint_randclear(state);
            exit(EXIT_FAILURE);
        }
        
    }
    
    if (formatInt==FORMAT_FMPQ){
        write_fmpq_points( curFile, points_re, points_im, nbPoints);
    } else if (formatInt==FORMAT_DOUB){
        write_double_points( curFile, points_re, points_im, nbPoints);
    } else
        write_acb_points( curFile, points_re, points_im, nbPoints, prec );
    
    if (!toStdout)
        fclose (curFile);
    
    _fmpq_vec_clear(points_im, nbPoints);
    _fmpq_vec_clear(points_re, nbPoints);
    flint_randclear(state);
    
    return 0;
    
}

void generateRandomPoints( fmpq_ptr points_re, fmpq_ptr points_im,
                           slong nbPoints, slong bitsize, flint_rand_t state ) {
    fmpz_t bound, hb;
    fmpz_init(bound);
    fmpz_init(hb);
    fmpz_one(bound);
    fmpz_one(hb);
    fmpz_mul_2exp(bound, bound, (ulong)bitsize);
    fmpz_mul_2exp(hb, hb, (ulong)(bitsize-1));
    
    for (slong i=0; i<nbPoints; i++) {
        fmpz_randm( fmpq_numref(points_re + i), state, bound );
        fmpz_sub( fmpq_numref(points_re + i), fmpq_numref(points_re + i), hb );
        fmpz_randm( fmpq_denref(points_re + i), state, bound );
        while( fmpz_is_zero( fmpq_denref(points_re + i) ) )
            fmpz_randm( fmpq_denref(points_re + i), state, bound );
        
        fmpz_randm( fmpq_numref(points_im + i), state, bound );
        fmpz_sub( fmpq_numref(points_im + i), fmpq_numref(points_im + i), hb );
        fmpz_randm( fmpq_denref(points_im + i), state, bound );
        while( fmpz_is_zero( fmpq_denref(points_im + i) ) )
            fmpz_randm( fmpq_denref(points_im + i), state, bound );
    }
    
    fmpz_clear(hb);
    fmpz_clear(bound);
}

void write_fmpq_points( FILE * file, const fmpq_ptr points_re, const fmpq_ptr points_im, slong nbPoints ) {
    fprintf(file, "fmpq %ld\n", nbPoints);
    for (slong i=0; i<nbPoints; i++){
        fmpq_fprint( file, points_re+i );
        fprintf(file, " ");
        fmpq_fprint( file, points_im+i );
        fprintf(file, "\n");
    }
}

void write_double_points( FILE * file, const fmpq_ptr points_re, const fmpq_ptr points_im, slong nbPoints ) {
    fprintf(file, "double %ld\n", nbPoints);
    for (slong i=0; i<nbPoints; i++){
        fprintf(file, "%.16lf %.16lf\n", fmpq_get_d(points_re+i), fmpq_get_d(points_im+i) );
    }
}

int acb_dump_file( FILE * file, const acb_t src ) {
    int res = ( arb_dump_file(file, acb_realref(src))==0 );
    fprintf(file, " ");
    res = res && ( arb_dump_file(file, acb_imagref(src))==0 );
    return res;
}

int write_acb_points( FILE * file, const fmpq_ptr points_re, const fmpq_ptr points_im, slong nbPoints, slong prec ) {
    
    int res = 1;
    slong lprec = (prec==0? PWPOLY_DEFAULT_PREC : prec);
    fprintf(file, "acb %ld\n", nbPoints);
    acb_t temp;
    acb_init(temp);
    for (slong i=0; i<nbPoints; i++){
        arb_set_fmpq( acb_realref( temp ), points_re+i, lprec );
        arb_set_fmpq( acb_imagref( temp ), points_im+i, lprec );
        if (prec==0)
            acb_get_mid( temp, temp );
        res = res && acb_dump_file( file, temp );
        fprintf(file, "\n");
    }
    
    acb_clear(temp);
    return res;
}
