#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "pw_check_libs_versions.h"

#include "flint/flint.h"

#include "flint/fmpz.h"
#include "flint/fmpq_poly.h"
#include "flint/fmpq.h"
#include "flint/fmpq_mat.h"
#ifdef PW_GENPOLFILE_DO_RESULTANT
#include "flint/fmpq_mpoly.h"
#endif
#include "flint/arith.h"
#include "acb.h"
#include "acb_poly.h"
// #include <gmp.h>
#include <mpfr.h>

#define MALLOC malloc
#define FREE free

#define M_MAX(A,B) (A>=B? A : B)
#define M_MIN(A,B) (A<=B? A : B)
#define M_ABS(A) (A<=0? -A : A)

#ifndef acb_poly_ptr
typedef acb_poly_struct * acb_poly_ptr;
#endif

/* global variables */
flint_rand_t state;
fmpq_poly_t p, p_imag;
arb_poly_t p_realApprox, p_imagApprox;
acb_poly_t pApprox;

/* optional parameters */
int format   ;
int bitsize  ;
int nbterms  ;
int power    ;
slong precision;
int seed     ;
int c        ;
int a        ;
int complexCoeffs;

void init_global_variables(){ 
    flint_randinit(state);
    fmpq_poly_init(p);
    fmpq_poly_init(p_imag);
    arb_poly_init(p_realApprox);
    arb_poly_init(p_imagApprox);
    acb_poly_init(pApprox);
    fmpq_poly_zero(p);
    fmpq_poly_zero(p_imag);
    arb_poly_zero(p_realApprox);
    arb_poly_zero(p_imagApprox);
    acb_poly_zero(pApprox);
    format = 1;
    bitsize = 8;
    nbterms = 10;
    power = 2;
    precision = 0;
    seed = 1;
    c = 3;
    a = 16;
    complexCoeffs = 0;
}

void clear_global_variables(){  
    flint_randclear(state);
    fmpq_poly_clear(p);
    fmpq_poly_clear(p_imag);
    arb_poly_clear(p_realApprox);
    arb_poly_clear(p_imagApprox);
    acb_poly_clear(pApprox);
}

void arb_poly_add_error_2exp_si( arb_poly_t p, slong e ){
    for (slong i=0; i<p->length; i++)
        arb_add_error_2exp_si( p->coeffs + i, e);
}

/* intermediate functions for random generation */
int  isIntinListInt (int el, int * lis, int length);

void exIntro_polynomial(fmpq_poly_t poly, slong Halfdeg);

/* functions for polynomials generation */
void bernoulli_polynomial(fmpq_poly_t poly, slong deg);
void bernoulli_reverse_polynomial(fmpq_poly_t poly, slong deg);
void mignotte_polynomial(fmpq_poly_t poly, slong deg, slong bitsize);
void mignotte_reverse_polynomial(fmpq_poly_t poly, slong deg, slong bitsize);
/* assume bitsize has 8 as a factor */
/* assume deg has 4 as a factor */
void nested_mignotte_polynomial(fmpq_poly_t poly, slong deg, slong bitsize);
void mignotte_generalized(fmpq_poly_t poly, slong deg, ulong pow, slong bitsize);
void wilkinson_polynomial(fmpq_poly_t poly, slong degree);
void regularGrid_polynomial(fmpq_poly_t dest, int res);
void Chebyshev1_polynomial(fmpq_poly_t dest, int degree);
void Chebyshev2_polynomial(fmpq_poly_t dest, int degree);
void Legendre_polynomial(fmpq_poly_t dest, int degree);
void randomDense_polynomial( fmpq_poly_t dest, int degree, int bitsize, flint_rand_t state);
void randomDenseComp_polynomial( fmpq_poly_t dest_real, fmpq_poly_t dest_imag, int degree, int bitsize, flint_rand_t state);
void randomSparse_polynomial( fmpq_poly_t dest, int degree, int bitsize, int nbterms, flint_rand_t state);

void randomChar_polynomial( fmpq_poly_t dest, int degree, int bitsize, flint_rand_t state);

void randomDenseHyp_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec);
void randomDenseHypComp_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec);

void randomDenseFlat_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec);
void randomDenseFlatComp_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec);

void randomDenseEll_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec);
void randomDenseEllComp_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec);

#ifdef PW_GENPOLFILE_DO_RESULTANT
void randomDenseRes_polynomial( fmpq_poly_t dest, int degree, int bitsize, flint_rand_t state);
#endif

void Mandelbrot_polynomial( fmpq_poly_t dest, int iterations);
void Runnels_polynomial( fmpq_poly_t dest, int iterations);
void Wilkinson_polynomial(fmpq_poly_t dest, int degree);

void MignotteGen_polynomial( fmpq_poly_t dest, int degree, int bitsize, int power);
void MignotteMul_polynomial( fmpq_poly_t dest, int degree, int bitsize, int power);
void WilkRat_polynomial(  fmpq_poly_t dest, int degree);
void WilkMul_polynomial(  fmpq_poly_t dest, int degree);
void WilkMulF_polynomial(  fmpq_poly_t dest, int degree, int power);
void Laguerre_polynomial(  fmpq_poly_t dest, int degree);

void nestedClusters_polynomial(  fmpq_poly_t dest, int nbRoots, int relativeWidth, int iterations);

void testHefRoots_polynomial( fmpq_poly_t dest, int degree);

// void genSpiralPolFile( FILE * file, int degree, int prec);
void spiral_polynomial( arb_poly_t pr, arb_poly_t pi, int degree, slong prec);
void genClusterPolFile( FILE * file, int iteration, int prec);

void truncatedExp_polynomial( fmpq_poly_t dest, int degree); 

int arb_poly_dump_file( FILE * file, const arb_poly_t src ) {
    fprintf( file, "%ld ", src->length );
    int res = 0;
    for (slong i=0; i<src->length; i++){
        if ( arb_dump_file(file, src->coeffs + i) )
            res = 1;
        fprintf(file, " ");
    }
    return res;
}

int acb_poly_dump_file( FILE * file, const acb_poly_t src ) {
    fprintf( file, "%ld ", src->length );
    int res = 0;
    for (slong i=0; i<src->length; i++){
        if ( arb_dump_file(file, acb_realref(src->coeffs + i)) )
            res = 1;
        fprintf(file, " ");
        if ( arb_dump_file(file, acb_imagref(src->coeffs + i)) )
            res = 1;
        fprintf(file, " ");
    }
    return res;
}

void arb_poly_to_fmpq_poly( fmpq_poly_t dest, const arb_poly_t src ) {
    
    fmpq_poly_fit_length(dest, src->length);
    _fmpq_poly_set_length(dest, src->length);
    fmpq_t coeff;
    fmpq_init(coeff);
    for (slong i = 0; i<src->length; i++) {
        arf_get_fmpq(coeff, arb_midref( src->coeffs + i ));
        fmpq_poly_set_coeff_fmpq(dest, i, coeff);
    }
    fmpq_clear(coeff);
    fmpq_poly_canonicalise(dest);
}

void acb_poly_to_fmpq_poly( fmpq_poly_t dest_real, fmpq_poly_t dest_imag, const acb_poly_t src ) {
    
    fmpq_poly_fit_length(dest_real, src->length);
    _fmpq_poly_set_length(dest_real, src->length);
    fmpq_poly_fit_length(dest_imag, src->length);
    _fmpq_poly_set_length(dest_imag, src->length);
    fmpq_t coeff;
    fmpq_init(coeff);
    for (slong i = 0; i<src->length; i++) {
        arf_get_fmpq(coeff, arb_midref( acb_realref(src->coeffs + i) ));
        fmpq_poly_set_coeff_fmpq(dest_real, i, coeff);
        arf_get_fmpq(coeff, arb_midref( acb_imagref(src->coeffs + i) ));
        fmpq_poly_set_coeff_fmpq(dest_imag, i, coeff);
    }
    fmpq_clear(coeff);
    fmpq_poly_canonicalise(dest_real);
    fmpq_poly_canonicalise(dest_imag);
}

void acb_poly_to_arb_poly( arb_poly_t dest_real, arb_poly_t dest_imag, const acb_poly_t src ) {
    arb_poly_fit_length(dest_real, src->length);
    arb_poly_fit_length(dest_imag, src->length);
    _arb_poly_set_length(dest_real, src->length);
    _arb_poly_set_length(dest_imag, src->length);
    for (slong i = 0; i<src->length; i++) {
        arb_set( dest_real->coeffs + i, acb_realref( src->coeffs + i ) );
        arb_set( dest_imag->coeffs + i, acb_imagref( src->coeffs + i ) );
    }
    _arb_poly_normalise( dest_real ); 
    _arb_poly_normalise( dest_imag );
}

void write_hefroots_file( FILE * f ) {
    
    if (!(precision==0)) {
        acb_poly_to_fmpq_poly( p, p_imag, pApprox );
        precision = 0;
    }
    if (precision==0) {
        if (complexCoeffs==0) {
            fmpq_poly_fprint(f, p);
        } else {
            fprintf(f, "Complex\n");
            fmpq_poly_fprint(f, p);
            fprintf(f, "\n");
            fmpq_poly_fprint(f, p_imag);
        }
    } else {
        acb_poly_to_arb_poly( p_realApprox, p_imagApprox, pApprox );
        fprintf(f, "ApproximationARB\n");
        if (complexCoeffs==0) {
            arb_poly_dump_file(f, p_realApprox);
        } else {
            fprintf(f, "Complex\n");
            arb_poly_dump_file(f, p_realApprox);
            fprintf(f, "\n");
            arb_poly_dump_file(f, p_imagApprox);
        }
    }
    
}

void write_pwroots_file( FILE * f ) {
    if (precision==0) {
        if (complexCoeffs==0) {
            fprintf(f, "Exact Real\n");
            fmpq_poly_fprint(f, p);
        } else {
            fprintf(f, "Exact Complex\n");
            fmpq_poly_fprint(f, p);
            fprintf(f, "\n");
            fmpq_poly_fprint(f, p_imag);
        }
    } else {
            fprintf(f, "Ball Approximation Complex\n");
            acb_poly_dump_file(f, pApprox);
    }
}

void write_anewdsc_file( FILE * f ) {
    fmpq_poly_canonicalise(p);
    fprintf(f, "%ld\n", p->length -1);
    for (slong i=0; i<p->length; i++){
        fmpz_fprint(f, p->coeffs + i);
        fprintf(f, "\n");
    }
}

void write_maple_file( FILE * curFile ) {
    fmpq_poly_canonicalise(p);
    /* sets the denominator to one to get integer polynomial */
    fmpz_one( fmpq_poly_denref (p ) );
    if (complexCoeffs==0) {
        fprintf(curFile, "f:=");
        fmpq_poly_fprint_pretty(curFile, p, "x");
        fprintf(curFile, ":\n");
    } else {
        fmpq_poly_canonicalise(p_imag);
        /* sets the denominator to one to get integer plynomial */
        fmpz_one( fmpq_poly_denref (p_imag) );
        fprintf(curFile, "freal:=");
        fmpq_poly_fprint_pretty(curFile, p, "x");
        fprintf(curFile, ":\n");
        fprintf(curFile, "fimag:=");
        fmpq_poly_fprint_pretty(curFile, p_imag, "x");
        fprintf(curFile, ":\n");
        fprintf(curFile, "f:= expand(freal + I*fimag):\n");
    }
}

slong acb_poly_rel_accuracy_bits( acb_poly_t p ) {
    if (p->length <= 0)
        return -ARF_PREC_EXACT;
    slong acc = acb_rel_accuracy_bits( p->coeffs + 0 );
    for (slong i = 1; i<p->length; i++)
        acc = M_MIN( acc, acb_rel_accuracy_bits( p->coeffs + i ) );
    return acc;
}

void write_mpsolve_file ( FILE * f ) {
    fprintf(f, "Dense;\n");
    fprintf(f, "Monomial;\n");
    if (complexCoeffs==0)
        fprintf(f, "Real;\n");
    else
        fprintf(f, "Complex;\n");
    
    if (precision==0) {
        slong plen = M_MAX( p->length, p_imag->length );
        fprintf(f, "Degree = %ld;\n", plen - 1);
        fprintf(f, "Rational;\n");
        fprintf(f, "\n");
        
        fmpq_t coeff;
        fmpq_init(coeff);
        for(slong i = 0; i<plen; i++){
            fmpq_poly_get_coeff_fmpq(coeff, p, i);
            fmpq_fprint(f, coeff);
            if (complexCoeffs) {
                fmpq_poly_get_coeff_fmpq(coeff, p_imag, i);
                fprintf(f, " ");
                fmpq_fprint(f, coeff);
            }
            fprintf(f, "\n");
        }
        fmpq_clear(coeff);
        
    } else {
        
        slong plen = pApprox->length;
        slong accuracy = acb_poly_rel_accuracy_bits(pApprox);
        accuracy = M_MIN( precision, accuracy );
        double log2_10 = 3.32192809488736234787;
        int nbDig = (int) floor( (double)accuracy/log2_10 );
        
        fprintf(f, "Degree=%ld;\n", plen - 1);
        fprintf(f, "FloatingPoint;\n");
        fprintf(f, "Precision=%d;\n", nbDig);
        fprintf(f, "\n");
        
        mpfr_t coeff_mpfr;
        mpf_t coeff_mpf;
        mpf_init2(coeff_mpf, (mp_bitcnt_t)(precision+10));
        mpfr_init2( coeff_mpfr, (mpfr_prec_t)(precision+10));
        
        for(slong i = 0; i<plen; i++){
            arf_get_mpfr( coeff_mpfr, arb_midref(acb_realref(pApprox->coeffs + i)), MPFR_RNDN );
            mpfr_get_f(coeff_mpf, coeff_mpfr, MPFR_RNDN );
            gmp_fprintf( f, "%.Fe", coeff_mpf );
            if (complexCoeffs) {
                fprintf(f, " ");
                arf_get_mpfr( coeff_mpfr, arb_midref(acb_imagref(pApprox->coeffs + i)), MPFR_RNDN );
                mpfr_get_f(coeff_mpf, coeff_mpfr, MPFR_RNDN );
                gmp_fprintf( f, "%.Fe", coeff_mpf );
            }
            fprintf(f, "\n");
        }
        mpf_clear(coeff_mpf);
        mpfr_clear(coeff_mpfr);
    }
}

int parseOptions( int argc, char **argv, char *filename );
    
int main(int argc, char **argv){
    
    if ( (argc<3) || (strcmp( argv[1], "-h" ) == 0) 
                  || (strcmp( argv[1], "--help" ) == 0) ) {
        printf("usage: %s Bernoulli           degree    [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s BernoulliR          degree    [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s RegularGrid         degree    [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s Mignotte            degree    [OPTIONS: filename format bitsize precision] \n", argv[0]);
        printf("       %s MignotteR           degree    [OPTIONS: filename format bitsize precision] \n", argv[0]);
        printf("       %s MignotteGen         degree    [OPTIONS: filename format bitsize power precision]\n", argv[0]);
        printf("       %s MignotteMul         degree    [OPTIONS: filename format bitsize power precision]\n", argv[0]);
        printf("       %s MignotteNest        degree    [OPTIONS: filename format bitsize precision]\n", argv[0]);
        printf("       %s Chebyshev1          degree    [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s Chebyshev2          degree    [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s Legendre            degree    [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s randomDense         degree    [OPTIONS: filename format bitsize seed precision] \n", argv[0]);
        printf("       %s randomSparse        degree    [OPTIONS: filename format bitsize nbterms seed precision] \n", argv[0]);
        printf("       %s randomDenseComp     degree    [OPTIONS: filename format bitsize seed precision] \n", argv[0]);
        printf("       %s randomChar          degree    [OPTIONS: filename format bitsize seed precision] \n", argv[0]);
        printf("       %s randomDenseHyp      degree    [OPTIONS: filename format bitsize seed precision] \n", argv[0]);
        printf("       %s randomDenseHypComp  degree    [OPTIONS: filename format bitsize seed precision] \n", argv[0]);
        printf("       %s randomDenseFlat     degree    [OPTIONS: filename format bitsize seed precision] \n", argv[0]);
        printf("       %s randomDenseFlatComp degree    [OPTIONS: filename format bitsize seed precision] \n", argv[0]);
        printf("       %s randomDenseEll      degree    [OPTIONS: filename format bitsize seed precision] \n", argv[0]);
        printf("       %s randomDenseEllComp  degree    [OPTIONS: filename format bitsize seed precision] \n", argv[0]);
        printf("       %s truncatedExp        degree    [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s Wilkinson           degree    [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s WilkRat             degree    [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s WilkMul             nbOfRoots [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s WilkMulF            nbOfRoots [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s Mandelbrot          iteration [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s Runnels             iteration [OPTIONS: filename format precision] \n", argv[0]);
        printf("       %s Laguerre            degree    [OPTIONS: filename format precision] \n", argv[0]);
//         printf("       %s Spiral          degree    [OPTIONS: filename        precision]; only for MPsolve\n", argv[0]);
//         printf("       %s nestedClusters iteration filename [OPTIONS:        precision]; only for MPsolve\n", argv[0]);
//         printf("       %s nestedClusters iteration [OPTIONS: filename format c a]\n", argv[0]);
//         printf("       %s testHefroots    degree    [OPTIONS: filename format]       \n", argv[0]);
        printf("                                                                    \n");
        printf("       -o , --output: the output file                               \n");
        printf("                      stdout [default]                              \n");
        printf("       -f , --format: the format of the output file                 \n");
        printf("                      1: [default] PWRoots  (.pwr)                  \n");
        printf("                      2:           MPsolve  (.mps)                  \n");
        printf("                      4:           ANewDsc  (.dsc)                  \n");
        printf("                      8:           Maple    (.mpl)                  \n");
        printf("                     16:           hefroots (.hef)                  \n");
        printf("       -b , --bitsize: the bitsize of the coeffs (for Mignotte, randoms...)\n");
        printf("                      8: [default] or a positive integer            \n");
        printf("       -n , --nbterms: the number of non-zero coeffs (for randomSparse)\n");
        printf("                      10: [default] or a positive integer            \n");
        printf("       -p , --power: (for MignotteGen: nb of roots in the cluster, \n");
        printf("                     (for MignotteMul and WilkMulF: multiplicity of the roots, \n");
        printf("                      2: [default] or a positive integer            \n");
        printf("       -L , --precision: if set then compute a floating point approx with L-bits mantissa\n"); 
        printf("                      53: [default for randomDenseHyp, HypComp, Flat, FlatComp, Ell, EllComp] or a positive integer            \n");
        printf("       -s , --seed: (for randomDense, randomSparse, randomDenseComp, randomChar)            \n"); 
        printf("                       1: [default] or a positive integer            \n");
//         printf("       OPTIONS ONLY FOR nestedClusters:                                \n");
//         printf("       -c : nb of roots in the clusters, \n");
//         printf("                       3: [default] or a positive integer            \n");
//         printf("       -a : relative width of nested clusters, \n");
//         printf("                       16: [default] or an integer >=2            \n");
        return -1;
    }
    
    char poly[100];
    char filename[100];
    int firstArg  = 0;
    
    init_global_variables();
    /* parse arguments */
    
    int parse=1;
    
    parse = parse*sscanf(argv[1], "%s", poly);
    parse = parse*sscanf(argv[2], "%d", &firstArg);
    parse = parse*(firstArg>=0);
    if (firstArg<0) {
        printf("%s ERROR: NON-VALID DEGREE/ITERATION (should be positive) \n", argv[0]);
        return -1;
    }
    sprintf(filename, "stdout");
    parse = parse*parseOptions( argc, argv, filename );
    
    if (!parse) {
        printf("%s PARSING ERROR\n", argv[0] );
        return -1;
    }
    
    srand(seed);
    
    char exIntro[] = "exIntro\0";
    char bernoulli[] = "Bernoulli\0";
    char bernoulliR[] = "BernoulliR\0";
    char mignotte[] = "Mignotte\0";
    char mignotteR[] = "MignotteR\0";
    char mignotteMul[] = "MignotteMul\0";
    char mignotteGen[] = "MignotteGen\0";
    char mignotteNes[] = "MignotteNes\0";
    char regularGrid[] = "RegularGrid\0";
    char Chebyshev1[] = "Chebyshev1\0";
    char Chebyshev2[] = "Chebyshev2\0";
    char Legendre[] = "Legendre\0";
    char randomDense[] = "randomDense\0";
    char randomSparse[] = "randomSparse\0";
    char randomDenseComp[] = "randomDenseComp\0";
    char randomChar[] = "randomChar\0";
    char randomDenseHyp[] = "randomDenseHyp\0";
    char randomDenseHypComp[] = "randomDenseHypComp\0";
    char randomDenseFlat[] = "randomDenseFlat\0";
    char randomDenseFlatComp[] = "randomDenseFlatComp\0";
    char randomDenseEll[] = "randomDenseEll\0";
    char randomDenseEllComp[] = "randomDenseEllComp\0";
#ifdef PW_GENPOLFILE_DO_RESULTANT
    char randomDenseRes[] = "randomDenseRes\0";
#endif
    char truncatedExp[] = "truncatedExp\0";
    char Wilkinson[] = "Wilkinson\0";
    char WilkMul[] = "WilkMul\0";
    char WilkRat[] = "WilkRat\0";
    char WilkMulF[] = "WilkMulF\0";
    char Mandelbrot[] = "Mandelbrot\0";
    char Runnels[] = "Runnels\0";
    char Laguerre[] = "Laguerre\0";
//     char Spiral[] = "Spiral\0";
    char testHefroots[] = "testHefroots\0";
    char nestedClusters[] = "nestedClusters\0";
    
    if (strcmp(poly, exIntro)==0) {
        exIntro_polynomial( p, firstArg);
    } else
        
    if (strcmp(poly, bernoulli)==0) {
        bernoulli_polynomial( p, firstArg);
    } else
        
    if (strcmp(poly, bernoulliR)==0) {
        bernoulli_reverse_polynomial( p, firstArg);
    } else
    
    if (strcmp(poly, regularGrid)==0) {
        regularGrid_polynomial(p, firstArg);
    } else
    
    if (strcmp(poly, Chebyshev1)==0) {
        Chebyshev1_polynomial(p, firstArg);
    } else
    
    if (strcmp(poly, Chebyshev2)==0) {
        Chebyshev1_polynomial(p, firstArg);
    } else
    
    if (strcmp(poly, Legendre)==0) {
        Legendre_polynomial(p, firstArg);
    } else
    
    if (strcmp(poly, mignotte)==0) {
        mignotte_polynomial(p, firstArg, bitsize);
    } else
        
    if (strcmp(poly, mignotteR)==0) {
        mignotte_reverse_polynomial(p, firstArg, bitsize);
    } else
        
    if (strcmp(poly, mignotteMul)==0) {
        MignotteMul_polynomial(p, firstArg, bitsize, power);
    } else
        
    if (strcmp(poly, mignotteGen)==0) {
        MignotteGen_polynomial(p, firstArg, bitsize, power);
    } else
        
    if (strcmp(poly, mignotteNes)==0) {
        nested_mignotte_polynomial(p, firstArg, bitsize);
    } else
    
    if (strcmp(poly, randomDense)==0) {
        for (int i=1; i<seed; i++)
            randomDense_polynomial(p, firstArg, bitsize, state);
        randomDense_polynomial(p, firstArg, bitsize, state);
    } else
    
    if (strcmp(poly, randomDenseComp)==0) {
        complexCoeffs = 1;
        for (int i=1; i<seed; i++)
            randomDenseComp_polynomial(p, p_imag, firstArg, bitsize, state);
        randomDenseComp_polynomial(p, p_imag, firstArg, bitsize, state);
    } else
        
    if (strcmp(poly, randomSparse)==0) {
        nbterms = M_MIN( firstArg, nbterms );
        for (int i=1; i<seed; i++)
            randomSparse_polynomial(p, firstArg, bitsize, nbterms, state);
        randomSparse_polynomial(p, firstArg, bitsize, nbterms, state);
    } else
        
    if (strcmp(poly, randomChar)==0) {
        for (int i=1; i<seed; i++)
            randomChar_polynomial(p, firstArg, bitsize, state);
        randomChar_polynomial(p, firstArg, bitsize, state);
    } else
        
    if (strcmp(poly, randomDenseHyp)==0) {
        if (precision==0)
            precision=53;
        for (int i=1; i<seed; i++)
            randomDenseHyp_polynomial( pApprox, firstArg, bitsize, state, precision);
        randomDenseHyp_polynomial( pApprox, firstArg, bitsize, state, precision);
    } else
        
    if (strcmp(poly, randomDenseHypComp)==0) {
        complexCoeffs = 1;
        if (precision==0)
            precision=53;
        for (int i=1; i<seed; i++)
            randomDenseHypComp_polynomial( pApprox, firstArg, bitsize, state, precision);
        randomDenseHypComp_polynomial( pApprox, firstArg, bitsize, state, precision);
    } else
        
    if (strcmp(poly, randomDenseFlat)==0) {
        complexCoeffs = 0;
        if (precision==0)
            precision=53;
        for (int i=1; i<seed; i++)
            randomDenseFlat_polynomial( pApprox, firstArg, bitsize, state, precision);
        randomDenseFlat_polynomial( pApprox, firstArg, bitsize, state, precision);
    } else
        
    if (strcmp(poly, randomDenseFlatComp)==0) {
        complexCoeffs = 1;
        if (precision==0)
            precision=53;
        for (int i=1; i<seed; i++)
            randomDenseFlatComp_polynomial( pApprox, firstArg, bitsize, state, precision);
        randomDenseFlatComp_polynomial( pApprox, firstArg, bitsize, state, precision);
    } else
        
    if (strcmp(poly, randomDenseEll)==0) {
        complexCoeffs = 0;
        if (precision==0)
            precision=53;
        for (int i=1; i<seed; i++)
            randomDenseEll_polynomial( pApprox, firstArg, bitsize, state, precision);
        randomDenseEll_polynomial( pApprox, firstArg, bitsize, state, precision);
    } else
        
    if (strcmp(poly, randomDenseEllComp)==0) {
        complexCoeffs = 1;
        if (precision==0)
            precision=53;
        for (int i=1; i<seed; i++)
            randomDenseEllComp_polynomial( pApprox, firstArg, bitsize, state, precision);
        randomDenseEllComp_polynomial( pApprox, firstArg, bitsize, state, precision);
    } else
       
#ifdef PW_GENPOLFILE_DO_RESULTANT
    if (strcmp(poly, randomDenseRes)==0) {
        for (int i=1; i<seed; i++)
            randomDenseRes_polynomial(p, firstArg, bitsize, state);
        randomDenseRes_polynomial(p, firstArg, bitsize, state);
    } else
#endif
        
    if (strcmp(poly, truncatedExp)==0) {
        truncatedExp_polynomial(p, firstArg);
    } else
    
    if (strcmp(poly, Mandelbrot)==0) {
        Mandelbrot_polynomial(p, firstArg);
    } else
    
    if (strcmp(poly, Runnels)==0) {
        Runnels_polynomial(p, firstArg);
    } else
    
    if (strcmp(poly, Wilkinson)==0) {
        Wilkinson_polynomial( p, firstArg);
    } else
        
    if (strcmp(poly, WilkMul)==0) {
        WilkMul_polynomial( p, firstArg);
    } else
        
    if (strcmp(poly, WilkMulF)==0) {
        WilkMulF_polynomial( p, firstArg, power);
    } else
        
    if (strcmp(poly, WilkRat)==0) {
        WilkRat_polynomial( p, firstArg);
    } else
        
    if (strcmp(poly, Laguerre)==0) {
        Laguerre_polynomial( p, firstArg);
    } else
        
    if (strcmp(poly, nestedClusters)==0) {
        nestedClusters_polynomial( p, c, a, firstArg );
    } else
        
    if (strcmp(poly, testHefroots)==0) {
        testHefRoots_polynomial( p, firstArg );
    } else
        
//     if ((strcmp(poly, Spiral)==0)&&(parse==1)) {
// //         FILE * curFile;
// //         printf ("%s PARSING OK; output file: %s\n", argv[0], filename);
// //         curFile = fopen (filename,"w");
//         precision = M_MAX(precision, 1);
//         spiral_polynomial( pApprox, p_imagApprox, firstArg, (slong) precision);
// //         genSpiralPolFile( curFile, firstArg, precision );
// //         fclose (curFile);
// //         fmpq_poly_clear(p);
// //         fmpq_poly_clear(p_imag);
// //         flint_cleanup();
// //         return 1;
//     } else
        
//     if ((strcmp(poly, nestedClusters)==0)&&(parse==1)) {
//         FILE * curFile;
//         printf ("%s PARSING OK; output file: %s\n", argv[0], filename);
//         curFile = fopen (filename,"w");
//         genClusterPolFile( curFile, firstArg, precision );
//         fclose (curFile);
//         fmpq_poly_clear(p);
//         return 1;
//     } else
        
    {
        printf ("%s PARSING ERROR; INVALID POLYNOMIAL: %s\n", argv[0], poly);
        parse = 0;
        clear_global_variables();
        flint_cleanup();
        return -1;
    }
    
//     if ( (precision>0) && (strcmp(poly, Spiral)!=0) ) {
    if ( (precision>0) && 
         !( (strcmp(poly, randomDenseHyp)==0) || (strcmp(poly, randomDenseHypComp)==0)
         || (strcmp(poly, randomDenseFlat)==0)|| (strcmp(poly, randomDenseFlatComp)==0)
         || (strcmp(poly, randomDenseEll)==0)|| (strcmp(poly, randomDenseEllComp)==0)
        ) 
        ) {
        arb_poly_set_fmpq_poly( p_realApprox, p, (slong) precision );
        arb_poly_set_fmpq_poly( p_imagApprox, p_imag, (slong) precision );
        acb_poly_set2_fmpq_poly( pApprox, p, p_imag, (slong) precision );
//         arb_poly_add_error_2exp_si( pApprox, (slong) -precision );
//         arb_poly_add_error_2exp_si( p_imagApprox, (slong) -precision );
        
    } 
    for (slong i=0; i<pApprox->length; i++ ) {
        acb_get_mid( (pApprox->coeffs) + i, (pApprox->coeffs) + i );
    }
//     if ((precision>0) && ( format==1 ))
//         acb_poly_to_fmpq_poly(p, p_imag, pApprox);
    
    if ( ( format==1 ) || ( format==2 ) || ( format==4 ) || ( format==8 ) || ( format==16 )) {
        FILE * curFile;
        if (strcmp( filename, "stdout" ) == 0)
            curFile = stdout;
        else
            curFile = fopen (filename,"w");
        
        if (curFile==NULL) {
            printf("%s can not be open\n", filename);
            clear_global_variables();
            flint_cleanup();
            return -1;
        }
        
        if (format==1)
            write_pwroots_file(curFile);
        else if (format==2)
            write_mpsolve_file(curFile);    
        else if (format==4)
            write_anewdsc_file(curFile);
        else if (format==8)
            write_maple_file(curFile);
        else if (format==16)
            write_hefroots_file( curFile );
        fclose (curFile);
    }
    else {
        char filename2[104];
        
        if ( format & 0x1 ) {
            sprintf(filename2, "%s.pwr", filename);
            FILE * curFile;
            curFile = fopen (filename2,"w");
            if (curFile==NULL) {
                printf("%s can not be open\n", filename2);
                clear_global_variables();
                flint_cleanup();
                return -1;
            }
            write_pwroots_file(curFile);
            fclose (curFile);
        }
        if (format & (0x1<<1)) {    
            
            sprintf(filename2, "%s.mps", filename);
            FILE * curFile;
            curFile = fopen (filename2,"w");
            if (curFile==NULL) {
                printf("%s can not be open\n", filename2);
                clear_global_variables();
                flint_cleanup();
                return -1;
            }
            write_mpsolve_file(curFile);
            fclose (curFile);
            
        }
        if (format & (0x1<<2)) {
            
            sprintf(filename2, "%s.dsc", filename);
            FILE * curFile;
            curFile = fopen (filename2,"w");
            if (curFile==NULL) {
                printf("%s can not be open\n", filename2);
                clear_global_variables();
                flint_cleanup();
                return -1;
            }
            write_anewdsc_file(curFile);
            fclose (curFile);
            
        } 
        
        if (format & (0x1<<3)) {
            
            sprintf(filename2, "%s.mpl", filename);
            FILE * curFile;
            curFile = fopen (filename2,"w");
            if (curFile==NULL) {
                printf("%s can not be open\n", filename2);
                clear_global_variables();
                flint_cleanup();
                return -1;
            }
            write_maple_file(curFile);
            fclose (curFile);
            
        }
        
        if (format & (0x1<<4)) {
            sprintf(filename2, "%s.hef", filename);
            FILE * curFile;
            curFile = fopen (filename2,"w");
            if (curFile==NULL) {
                printf("%s can not be open\n", filename2);
                clear_global_variables();
                flint_cleanup();
                return -1;
            }
            write_hefroots_file(curFile);
            fclose (curFile);
        }
        
        
    }
//         printf("format %d not implemented\n", format);

    clear_global_variables();
    flint_cleanup();
    return 0;
}

int parseOptions( int argc, char **argv, char *filename ) {
    
    int parse = 1;
        /* loop on arguments to figure out options */
    for (int arg = 3; arg< argc; arg++) {
        
        if ( (strcmp( argv[arg], "-o" ) == 0) || (strcmp( argv[arg], "--output" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%s", filename);
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-f" ) == 0) || (strcmp( argv[arg], "--format" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%d", &format);
                if ((format<=0)||(format>31)) {
//                     1 2 4 3 5 6 7 8 9 10 11 12 13 14 15
                    printf("%s ERROR: NON-VALID FORMAT (should be in {1,...,31}) \n", argv[0]);
                    parse = 0;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-b" ) == 0) || (strcmp( argv[arg], "--bitsize" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%d", &bitsize);
                if (bitsize<=0){
                    printf("%s ERROR: NON-VALID BITSIZE (should >0) \n", argv[0]);
                    parse = 0;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-n" ) == 0) || (strcmp( argv[arg], "--nbterms" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%d", &nbterms);
                if (nbterms<=0){
                    printf("%s ERROR: NON-VALID NUMBER OF TERMS (should >0) \n", argv[0]);
                    parse = 0;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-p" ) == 0) || (strcmp( argv[arg], "--power" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%d", &power);
                if (power<=0){
                    printf("%s ERROR: NON-VALID POWER (should >0) \n", argv[0]);
                    parse = 0;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-L" ) == 0) || (strcmp( argv[arg], "--precision" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%ld", &precision);
                if (precision<=0){
                    printf("%s ERROR: NON-VALID PRECISION (should >0) \n", argv[0]);
                    parse = 0;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-s" ) == 0) || (strcmp( argv[arg], "--seed" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%d", &seed);
                if (seed<=0){
                    printf("%s ERROR: NON-VALID SEED (should >0) \n", argv[0]);
                    parse = 0;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-c" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%d", &c);
                if (c<=0){
                    printf("%s ERROR: NON-VALID c (should be >0) \n", argv[0]);
                    parse = 0;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-a" ) == 0) ) {
            if (argc>arg+1) {
                parse = parse*sscanf(argv[arg+1], "%d", &a);
                if (a<=1){
                    printf("%s ERROR: NON-VALID a (should be >=2) \n", argv[0]);
                    parse = 0;
                }
                arg++;
            }
        }
        
    }
    
    return parse;
}

typedef fmpq * fmpq_ptr;

void fmpq_div_ui(fmpq_t dest, const fmpq_t x, ulong y) { 
    fmpq_set(dest, x);
    fmpz_mul_si(fmpq_denref(dest), fmpq_denref(x), y);
    fmpq_canonicalise(dest);
}
    
void fmpq_poly_set_coeff_si_ui(fmpq_poly_t poly, slong n, slong num, ulong den) {
    fmpq_t temp; 
    fmpq_init( temp );
    fmpq_set_si(temp, num, den);
    fmpq_poly_set_coeff_fmpq( poly, n, temp);
    fmpq_clear(temp);
}

#ifndef acb_poly
typedef acb_poly_struct acb_poly;
#endif
// #ifndef acb_poly_ptr
// typedef acb_poly_struct * acb_poly_ptr;
// #endif

acb_srcptr acb_poly_getCoeff(const acb_poly_t poly, slong degree) { return poly->coeffs + degree; }

void exIntro_polynomial(fmpq_poly_t poly, slong Halfdeg) {
    
    slong len = 2*Halfdeg+1;
    fmpq_poly_fit_length(poly, len);
    _fmpq_poly_set_length(poly, len);
    
    fmpz_one( fmpq_poly_denref(poly) );
    
    for (slong i = 0; i<len; i++)
        fmpz_zero( fmpq_poly_numref(poly) + i );
    
    fmpz_one( fmpq_poly_numref(poly) + (len-1) );
    
    fmpz_one( fmpq_poly_numref(poly) + Halfdeg );
    fmpz_mul_2exp( fmpq_poly_numref(poly) + Halfdeg, fmpq_poly_numref(poly) + Halfdeg, (ulong) Halfdeg );
    fmpz_add_ui( fmpq_poly_numref(poly) + Halfdeg, fmpq_poly_numref(poly) + Halfdeg, 1);
    fmpz_neg( fmpq_poly_numref(poly) + Halfdeg, fmpq_poly_numref(poly) + Halfdeg );
    
    fmpz_one( fmpq_poly_numref(poly) + 0 );
    fmpz_mul_2exp( fmpq_poly_numref(poly) + 0, fmpq_poly_numref(poly) + 0, (ulong) Halfdeg );
}

void bernoulli_polynomial( fmpq_poly_t poly, slong deg) {
    arith_bernoulli_polynomial(poly , deg);
}

void bernoulli_reverse_polynomial( fmpq_poly_t poly, slong deg) {
    
    arith_bernoulli_polynomial(poly , deg);
    fmpq_poly_reverse(poly, poly, deg+1);
}

void mignotte_polynomial(fmpq_poly_t poly, slong deg, slong bitsize){
    
    fmpq_t coeff, two;
    fmpq_init(coeff);
    fmpq_init(two);
    
    fmpq_set_si(two, 2,1);
//     fmpq_pow_si(coeff, two, ((slong) bitsize/2)-1 );
    fmpq_pow_si(coeff, two, bitsize); /*coeff = 2^(bitsize) */
    
    fmpq_poly_fit_length(poly,deg+1);
    fmpq_poly_zero(poly);
    fmpq_poly_set_coeff_fmpq(poly, 1, coeff); /* poly = coeff*x */
    
    fmpq_set_si(coeff, -1,1);
    fmpq_poly_set_coeff_fmpq(poly, 0, coeff); /* poly = coeff*x -1 */
    fmpq_poly_pow(poly, poly, 2);                /* poly = (coeff*x -1)^2 */
    fmpq_poly_add(poly, poly, poly);             /* poly = 2*(coeff*x -1)^2 */
    fmpq_poly_neg(poly, poly);//                    /* poly = -2*(coeff*x -1)^2 */
    
    fmpq_set_si(coeff, 1,1);
    fmpq_poly_set_coeff_fmpq(poly, deg, coeff); /* poly = x^deg -2*(coeff*x -1)^2 */
    
    fmpq_clear(coeff);
    fmpq_clear(two);
}

void mignotte_reverse_polynomial(fmpq_poly_t poly, slong deg, slong bitsize) {
    mignotte_polynomial(poly, deg, bitsize);
    fmpq_poly_reverse(poly, poly, deg+1);
}

/* assume bitsize has 8 as a factor */
/* assume deg has 4 as a factor */
void nested_mignotte_polynomial(fmpq_poly_t poly, slong deg, slong bitsize){
    
    fmpq_t coeff;
    fmpq_poly_t templ, tempt, temp;
    fmpq_init(coeff);
    fmpq_poly_init(templ);
    fmpq_poly_fit_length(templ,(deg/4)+2);
    fmpq_poly_init(tempt);
    fmpq_poly_fit_length(tempt,3);
    fmpq_poly_init(temp);
    
    fmpq_poly_one(poly);
    fmpq_poly_one(templ);
    fmpq_poly_zero(tempt);
    fmpq_poly_shift_left(templ, templ, (deg/4)+1);
    
    fmpq_set_si(coeff, 2,1);
    fmpq_pow_si(coeff, coeff, bitsize/8); /*coeff = 2^(bitsize/8) */
    fmpq_sub_si(coeff, coeff, 1); /*coeff = 2^(bitsize/8)-1 */
    fmpq_poly_set_coeff_fmpq(temp, 2, coeff); /* tempt = coeff*z^2 */
    fmpq_poly_set_coeff_si_ui(temp, 0, -1, 1); /* tempt = coeff*z^2 - 1*/
    
    for (int i=1; i<=4; i++){
        fmpq_poly_pow(tempt, temp, 2*i);  /* tempt = (coeff*z^2 - 1)^2*/
        fmpq_poly_sub(tempt, templ, tempt);
        fmpq_poly_mul(poly, poly, tempt);
    }
    
    fmpq_clear(coeff);
    fmpq_poly_clear(temp);
    fmpq_poly_clear(tempt);
    fmpq_poly_clear(templ);
}

void mignotte_generalized(fmpq_poly_t poly, slong deg, ulong pow, slong bitsize){
    
    fmpq_t coeff, two;
    fmpq_init(coeff);
    fmpq_init(two);
    fmpq_poly_t p1, p2;
    fmpq_poly_init(p1);
    fmpq_poly_init(p2);
    fmpq_poly_fit_length(poly,deg+1);
    fmpq_poly_zero(poly);
    
    fmpq_set_si(two, 2,1);
    fmpq_pow_si(coeff, two, bitsize); /*coeff = 2^(bitsize) */
    fmpq_poly_set_coeff_fmpq(p1, 1, coeff); /* p1 = coeff*x */
    fmpq_poly_set_coeff_fmpq(p2, 1, coeff); /* p2 = coeff*x */
    fmpq_set_si(coeff, -1,1);
    fmpq_poly_set_coeff_fmpq(p1, 0, coeff); /* p1 = coeff*x -1 */
    fmpq_set_si(coeff, 1,1);
    fmpq_poly_set_coeff_fmpq(p2, 0, coeff); /* p2 = coeff*x +1 */
    fmpq_poly_pow(p1, p1, pow);                /* p1 = (coeff*x -1)^pow */
    fmpq_poly_pow(p2, p2, pow);                /* p2 = (coeff*x +1)^pow */
    fmpq_poly_mul(poly, p1, p2);               /* poly = (coeff*x -1)^pow*(coeff*x +1)^pow*/
    fmpq_poly_add(poly, poly, poly);           /* poly = 2*(coeff*x -1)^pow*(coeff*x +1)^pow*/
    fmpq_poly_neg(poly, poly);                    /* poly = -2*(coeff*x -1)^pow*(coeff*x +1)^pow*/
    
    fmpq_set_si(coeff, 1,1);
    fmpq_poly_set_coeff_fmpq(poly, deg, coeff); /* poly = x^deg -2*(coeff*x -1)^pow*(coeff*x +1)^pow*/
    
    fmpq_clear(coeff);
    fmpq_clear(two);
    fmpq_poly_clear(p1);
    fmpq_poly_clear(p2);
}

void wilkinson_polynomial(fmpq_poly_t poly, slong degree){
    fmpq_poly_t ptemp;
    fmpq_poly_init2(ptemp,2);
    
    fmpq_poly_one(poly);
    fmpq_poly_zero(ptemp);
    fmpq_poly_set_coeff_si_ui(ptemp, 1, 1, 1);
    
    for (int i=1; i<=degree; i++){
        fmpq_poly_set_coeff_si_ui(ptemp, 0, -i, 1);
        fmpq_poly_mul(poly, poly, ptemp);
    }
    
    fmpq_poly_clear(ptemp);
}

void truncatedExp_polynomial( fmpq_poly_t dest, int degree){
    fmpq_t coeff;
    fmpq_init(coeff);
    fmpq_poly_zero(dest);
    fmpq_poly_set_coeff_ui(dest, 0, 1);
    for (int i=1; i<=degree; i++){
        fmpq_one(coeff);
        fmpz_fac_ui(fmpq_denref(coeff), (ulong) i);
        fmpq_canonicalise(coeff);
        fmpq_poly_set_coeff_fmpq(dest, i, coeff);
    }
    fmpq_clear(coeff);
}

void regularGrid_polynomial(fmpq_poly_t preg, int res){
    
    fmpq_poly_t ptemp, ptemp2;
    fmpq_poly_init2(ptemp,2);
    fmpq_poly_init2(ptemp2,2);
    fmpq_poly_one(preg);
    fmpq_poly_zero(ptemp);
    fmpq_poly_zero(ptemp2);
    fmpq_poly_set_coeff_si_ui(ptemp, 1, 1, 1);
    fmpq_poly_set_coeff_si_ui(ptemp2, 2, 1, 1);
    
    for (int i=0; i<=res; i++){
        fmpq_poly_set_coeff_si_ui(ptemp, 0, -i, 1);
        fmpq_poly_mul(preg, preg, ptemp);
        
        for (int j=1; j<=res; j++){
            fmpq_poly_set_coeff_si_ui(ptemp2, 1, 2*i, 1);
            fmpq_poly_set_coeff_si_ui(ptemp2, 0, i*i+j*j, 1);
            fmpq_poly_mul(preg, preg, ptemp2);
        }
        
        if (i>0) {
            fmpq_poly_set_coeff_si_ui(ptemp, 0, i, 1);
            fmpq_poly_mul(preg, preg, ptemp);
            
            for (int j=1; j<=res; j++){
                fmpq_poly_set_coeff_si_ui(ptemp2, 1, -2*i, 1);
                fmpq_poly_set_coeff_si_ui(ptemp2, 0, i*i+j*j, 1);
                fmpq_poly_mul(preg, preg, ptemp2);
            }
        }
            
    }
    
    fmpq_poly_clear(ptemp);
    fmpq_poly_clear(ptemp2);
}

void Chebyshev1_polynomial(fmpq_poly_t pdest, int degree){
    
    fmpq_poly_t PNM1, PNM2;
    fmpq_poly_init2(PNM1,2);
    fmpq_poly_init2(PNM2,2);
    
    if (degree == 0) {
        fmpq_poly_one(pdest);
    }
    else if (degree == 1) {
        fmpq_poly_zero(pdest);
        fmpq_poly_set_coeff_si_ui(pdest, 1, 1, 1);
    }
    else {
        fmpq_poly_one(PNM2);
        fmpq_poly_zero(PNM1);
        fmpq_poly_set_coeff_si_ui(PNM1, 1, 1, 1);
        
        for (int i=2; i<=degree; i++) {
            fmpq_poly_shift_left(pdest, PNM1, 1);
            fmpq_poly_scalar_mul_si(pdest, pdest, 2);
            fmpq_poly_neg(PNM2, PNM2);
            fmpq_poly_add(pdest, pdest, PNM2);
            fmpq_poly_set(PNM2, PNM1);
            fmpq_poly_set(PNM1, pdest);
        }
    }
    
    fmpq_poly_clear(PNM1);
    fmpq_poly_clear(PNM2);
}

void Chebyshev2_polynomial(fmpq_poly_t pdest, int degree){
    
    fmpq_poly_t PNM1, PNM2;
    fmpq_poly_init2(PNM1,2);
    fmpq_poly_init2(PNM2,2);
    
    if (degree == 0) {
        fmpq_poly_one(pdest);
    }
    else if (degree == 1) {
        fmpq_poly_zero(pdest);
        fmpq_poly_set_coeff_si_ui(pdest, 1, 2, 1);
    }
    else {
        fmpq_poly_one(PNM2);
        fmpq_poly_zero(PNM1);
        fmpq_poly_set_coeff_si_ui(PNM1, 1, 2, 1);
        
        for (int i=2; i<=degree; i++) {
            fmpq_poly_shift_left(pdest, PNM1, 1);
            fmpq_poly_scalar_mul_si(pdest, pdest, 2);
            fmpq_poly_neg(PNM2, PNM2);
            fmpq_poly_add(pdest, pdest, PNM2);
            fmpq_poly_set(PNM2, PNM1);
            fmpq_poly_set(PNM1, pdest);
        }
    }
    
    fmpq_poly_clear(PNM1);
    fmpq_poly_clear(PNM2);
}

void Legendre_polynomial(fmpq_poly_t pdest, int degree){
    
    fmpq_poly_t PNM1, PNM2;
    fmpq_t q;
    fmpq_poly_init2(PNM1,2);
    fmpq_poly_init2(PNM2,2);
    fmpq_init(q);
    
    if (degree == 0) {
        fmpq_poly_one(pdest);
    }
    else if (degree == 1) {
        fmpq_poly_zero(pdest);
        fmpq_poly_set_coeff_si_ui(pdest, 1, 1, 1);
    }
    else {
        fmpq_poly_one(PNM2);
        fmpq_poly_zero(PNM1);
        fmpq_poly_set_coeff_si_ui(PNM1, 1, 1, 1);
        
        for (int i=2; i<=degree; i++) {
            fmpq_poly_shift_left(pdest, PNM1, 1);
            fmpq_poly_scalar_mul_si(pdest, pdest, (2*(i-1)+1));
            fmpq_poly_scalar_mul_si(PNM2, PNM2, i-1);
            fmpq_poly_neg(PNM2, PNM2);
            fmpq_poly_add(pdest, pdest, PNM2);
            fmpq_set_si(q, 1, i);
            fmpq_poly_scalar_mul_fmpq(pdest, pdest, q);
            fmpq_poly_set(PNM2, PNM1);
            fmpq_poly_set(PNM1, pdest);
        }
    }
    
    fmpq_poly_clear(PNM1);
    fmpq_poly_clear(PNM2);
    fmpq_clear(q);
}

void randomDense_polynomial( fmpq_poly_t dest, int degree, int bitsize, flint_rand_t state) {
    
    fmpq_poly_fit_length(dest, (slong) degree+1);
    fmpq_poly_zero(dest);
    
    fmpz_t bound, hb;
    fmpz_init(bound);
    fmpz_init(hb);
    fmpz_one(bound);
    fmpz_one(hb);
    fmpz_mul_2exp(bound, bound, (ulong)bitsize);
    fmpz_mul_2exp(hb, hb, (ulong)(bitsize-1));
    
    for (int i=0;i<degree; i++){
//         fmpz_randtest_not_zero(dest->coeffs + i,           state, bitsize);
        fmpz_randm( dest->coeffs + i, state, bound );
        fmpz_sub( dest->coeffs + i, dest->coeffs + i, hb );
    }
    fmpz_one(fmpq_poly_denref(dest));
    fmpz_mul_2exp(fmpq_poly_denref(dest), fmpq_poly_denref(dest), (ulong) (bitsize-1) );
    fmpz_one(dest->coeffs + degree);
    fmpz_mul_2exp(dest->coeffs + degree, dest->coeffs + degree, (ulong) (bitsize-1) );
    
    dest->length = degree +1;
    
    fmpz_clear(hb);
    fmpz_clear(bound);
    
}

void randomDenseComp_polynomial( fmpq_poly_t dest_real, fmpq_poly_t dest_imag, int degree, int bitsize, flint_rand_t state) {
    
    randomDense_polynomial(dest_real, degree, bitsize, state);
    randomDense_polynomial(dest_imag, degree, bitsize, state);
    
}

int  isIntinListInt (int el, int * lis, int length){
    for (int index=0;index<length;index++)
        if (el==lis[index])
            return 1;
    return 0;
}

void randomSparse_polynomial( fmpq_poly_t dest, int degree, int bitsize, int nbterms, flint_rand_t state){
    
    fmpq_poly_fit_length(dest, (slong) degree+1);
    fmpq_poly_zero(dest);
    fmpq_poly_set_coeff_si_ui(dest, (slong) degree, (slong) 1, (ulong) 1);
    fmpz_randtest_not_zero(dest->coeffs + 0,           state, bitsize);
    
    int * list_coeffs;
    int length = 2;
    int coeff;
    list_coeffs = (int *) malloc (nbterms*sizeof(int));
    list_coeffs[0]=degree;
    list_coeffs[1]=0;
    
    while (length < nbterms) {
        coeff = (rand() % (degree));
        if (!isIntinListInt (coeff, list_coeffs, length)){
            
            fmpz_randtest_not_zero(dest->coeffs + coeff,           state, bitsize);
            
            list_coeffs[length]=coeff;
            length++;
        }
    }
    
    fmpz_one(fmpq_poly_denref(dest));
    fmpz_mul_2exp(fmpq_poly_denref(dest), fmpq_poly_denref(dest), (ulong) bitsize );
    fmpz_one(dest->coeffs + degree);
    fmpz_mul_2exp(dest->coeffs + degree, dest->coeffs + degree, (ulong) bitsize );
    
    dest->length = degree +1;
    
}

void randomChar_polynomial( fmpq_poly_t dest, int degree, int bitsize, flint_rand_t state) {
    
    fmpq_mat_t m;
    fmpq_mat_init(m, degree, degree);
    fmpq_mat_randtest(m, state, bitsize);
    fmpq_mat_charpoly(dest, m);
    fmpq_mat_clear(m);
    
}

void randomDenseHyp_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec) {
 
    fmpq_poly_t a;
    fmpq_poly_init(a);
    
    slong lprec = 2*prec;
    
    randomDense_polynomial( a, degree, bitsize, state);
    acb_poly_set_fmpq_poly( dest, a, lprec );
    
    fmpq_poly_clear(a);
}

void randomDenseHypComp_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec) {
 
    fmpq_poly_t a, b;
    fmpq_poly_init(a);
    fmpq_poly_init(b);
    
    slong lprec = 2*prec;
    
    randomDenseComp_polynomial( a, b, degree, bitsize, state);
    acb_poly_set2_fmpq_poly( dest, a, b, lprec );
    
    fmpq_poly_clear(a);
    fmpq_poly_clear(b);
}

void randomDenseFlat_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec) {
 
    fmpq_poly_t a;
    fmpq_poly_init(a);
    fmpz_t fact;
    fmpz_init(fact);
    arb_t sqrtFact;
    arb_init(sqrtFact);
    
    slong lprec = 2*prec;
    
    randomDense_polynomial( a, degree, bitsize, state);
    acb_poly_set_fmpq_poly( dest, a, lprec );
    
    fmpz_one(fact);
    for (int i = 0; i<=degree; i++) {
        
        if (i>=2) {
            fmpz_mul_si( fact, fact, i );
            arb_set_fmpz(sqrtFact, fact);
            arb_sqrt(sqrtFact, sqrtFact, lprec);
            acb_div_arb( dest->coeffs + i, dest->coeffs + i, sqrtFact, lprec);
        }
        
    }
    
    arb_clear(sqrtFact);
    fmpz_clear(fact);
    fmpq_poly_clear(a);
}

void randomDenseFlatComp_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec) {
 
    fmpq_poly_t a, b;
    fmpq_poly_init(a);
    fmpq_poly_init(b);
    fmpz_t fact;
    fmpz_init(fact);
    arb_t sqrtFact;
    arb_init(sqrtFact);
    
    slong lprec = 2*prec;
    
    randomDenseComp_polynomial( a, b, degree, bitsize, state);
    acb_poly_set2_fmpq_poly( dest, a, b, lprec );
    
    fmpz_one(fact);
    for (int i = 2; i<=degree; i++) {
        fmpz_mul_si( fact, fact, i );
        arb_set_fmpz(sqrtFact, fact);
        arb_sqrt(sqrtFact, sqrtFact, lprec);
        acb_div_arb( dest->coeffs + i, dest->coeffs + i, sqrtFact, lprec);
    }
    
    arb_clear(sqrtFact);
    fmpz_clear(fact);
    fmpq_poly_clear(a);
    fmpq_poly_clear(b);
}

void randomDenseEll_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec) {
 
    fmpq_poly_t a;
    fmpq_poly_init(a);
    arb_t bin;
    arb_init(bin);
    
    slong lprec = 2*prec;
    
    randomDense_polynomial( a, degree, bitsize, state);
    acb_poly_set_fmpq_poly( dest, a, lprec );
    
    for (int i = 0; i<=degree; i++) {
        arb_bin_uiui(bin, (ulong)degree, (ulong)i, lprec);
        arb_sqrt(bin, bin, prec);
        acb_mul_arb( dest->coeffs + i, dest->coeffs + i, bin, lprec);
    }
    
    arb_clear(bin);
    fmpq_poly_clear(a);
}

void randomDenseEllComp_polynomial( acb_poly_t dest, int degree, int bitsize, flint_rand_t state, slong prec) {
 
    fmpq_poly_t a, b;
    fmpq_poly_init(a);
    fmpq_poly_init(b);
    arb_t bin;
    arb_init(bin);
    
    slong lprec = 2*prec;
    
    randomDenseComp_polynomial( a, b, degree, bitsize, state);
    acb_poly_set2_fmpq_poly( dest, a, b, lprec );
    
    for (int i = 2; i<=degree; i++) {
        arb_bin_uiui(bin, (ulong)degree, (ulong)i, prec);
        arb_sqrt(bin, bin, prec);
        acb_mul_arb( dest->coeffs + i, dest->coeffs + i, bin, lprec);
    }
    
    arb_clear(bin);
    fmpq_poly_clear(a);
    fmpq_poly_clear(b);
}

#ifdef PW_GENPOLFILE_DO_RESULTANT
void randomDenseRes_polynomial( fmpq_poly_t dest, int degree, int bitsize, flint_rand_t state){
    
    fmpq_mpoly_ctx_t biv;
    fmpq_mpoly_ctx_init(biv, 2, ORD_LEX);
    
    fmpq_mpoly_t A, B, C;
    fmpq_mpoly_init(A, biv);
    fmpq_mpoly_init(B, biv);
    fmpq_mpoly_init(C, biv);
    
    fmpz_t coeff;
    fmpz_init(coeff);
    
//     slong lensqr=((slong)degree+1)*((slong)degree+1);
//     char var1[] = "x";
//     char var2[] = "y";
//     const char * vars[2] = {var1,var2};
    
    ulong exps[2];
    for (ulong i =0; i<(ulong)degree + 1; i++ ) {
        for (ulong j =0; j<(ulong)degree + 1 -i; j++ ) {
            exps[0] = i;
            exps[1] = j;
            fmpz_randtest_not_zero(coeff, state, bitsize);
            fmpq_mpoly_push_term_fmpz_ui(A, coeff, exps, biv);
            fmpz_randtest_not_zero(coeff, state, bitsize);
            fmpq_mpoly_push_term_fmpz_ui(B, coeff, exps, biv);
        }
    }
    
    fmpz_clear(coeff);
    fmpq_mpoly_sort_terms(A, biv);
    fmpq_mpoly_sort_terms(B, biv);
//     fmpq_mpoly_randtest_bound(A, state, lensqr, (mp_limb_t)bitsize, (ulong)degree+1, biv);
//     fmpq_mpoly_randtest_bound(B, state, lensqr, (mp_limb_t)bitsize, (ulong)degree+1, biv);
//     printf("length of A: %ld\n", fmpq_mpoly_length(A, biv) );
//     printf("A: ");
//     fmpq_mpoly_fprint_pretty(stdout, A, vars, biv);
//     printf("\n");
//     printf("length of B: %ld\n", fmpq_mpoly_length(B, biv) );
//     printf("B: ");
//     fmpq_mpoly_fprint_pretty(stdout, B, vars, biv);
//     printf("\n");
    fmpq_mpoly_resultant(C, A, B, 1, biv);
//     printf("length of RES_x(A,B): %ld\n", fmpq_mpoly_length(C, biv) );
//     printf("RES_x(A,B): ");
//     fmpq_mpoly_fprint_pretty(stdout, C, vars, biv);
//     printf("\n");
    
    slong lendest = fmpq_mpoly_length(C, biv);
    int sgn = fmpq_sgn(fmpq_mpoly_content_ref(C,biv));
    fmpq_poly_fit_length( dest, lendest );
    _fmpq_poly_set_length( dest, lendest );
    fmpz_one(fmpq_poly_denref(dest));
    for (slong i=0; i<lendest; i++) {
        fmpz_set( dest->coeffs + (lendest-(i+1)), fmpq_mpoly_zpoly_term_coeff_ref(C, i, biv) );
        if (sgn<0)
            fmpz_neg( dest->coeffs + (lendest-(i+1)), dest->coeffs + (lendest-(i+1)) );
    }
    
    fmpq_t leading;
    fmpq_init(leading);
    fmpq_poly_get_coeff_fmpq(leading, dest, lendest-1);
    fmpq_poly_scalar_div_fmpq(dest, dest, leading );
    fmpq_clear(leading);
    
//     fmpq_poly_fprint_pretty( stdout, dest, "x" );
    
    fmpq_mpoly_clear(C, biv);
    fmpq_mpoly_clear(B, biv);
    fmpq_mpoly_clear(A, biv);
    fmpq_mpoly_ctx_clear(biv);
}
#endif

void Mandelbrot_polynomial( fmpq_poly_t pmand, int iterations){
    
    fmpq_poly_t pone, px;
    fmpq_poly_init(pone);
    fmpq_poly_init(px);
    
    fmpq_poly_one(pmand);
    fmpq_poly_one(pone);
    fmpq_poly_zero(px);
    fmpq_poly_set_coeff_si_ui(px, 1, 1, 1);
    
    for (int i = 1; i<=iterations; i++) {
        fmpq_poly_pow(pmand, pmand, 2);
        fmpq_poly_mul(pmand, pmand, px);
        fmpq_poly_add(pmand, pmand, pone);
    }
    
    fmpq_poly_clear(pone);
    fmpq_poly_clear(px);    
}

void testHefRoots_polynomial( fmpq_poly_t p, int degree){
    
    slong halfdeg = degree/2;
    fmpq_poly_init2(p, halfdeg+1);
    p->length = halfdeg+1;
    
    fmpq_poly_t temp;
    fmpq_poly_init2(temp, degree-halfdeg + 1);
    temp->length=degree-halfdeg + 1;
    
    fmpq_t ratio, ration;
    fmpq_init(ratio);
    fmpq_init(ration);
    fmpq_set_si(ratio, 1, 2);
    fmpq_set_si(ration, 1, 2);
    fmpq_poly_set_coeff_si_ui(p, 0, 1, 1);
    for(slong index = 1; index <=halfdeg; index++) {
        fmpq_poly_set_coeff_fmpq(p, index, ration);
        fmpq_mul(ration, ration, ratio);
    }
    
    fmpq_set_si(ratio, 2, 1);
    fmpq_set_si(ration, 2, 1);
    fmpq_poly_set_coeff_si_ui(temp, 0, 1, 1);
    for(slong index = 1; index <=degree-halfdeg; index++) {
        fmpq_poly_set_coeff_fmpq(temp, index, ration);
        fmpq_mul(ration, ration, ratio);
    }
    
    fmpq_poly_mul(p, p, temp);
        
    fmpq_clear(ratio);
    fmpq_clear(ration);
    fmpq_poly_clear(temp);
}

void Runnels_polynomial( fmpq_poly_t prun, int iterations){
    
    fmpq_poly_t prunm1, prunm2, pone, px;
    fmpq_poly_init(prunm1);
    fmpq_poly_init(prunm2);
    fmpq_poly_init(pone);
    fmpq_poly_init(px);
    
    fmpq_poly_zero(px);
    fmpq_poly_set_coeff_si_ui(px, 1, 1, 1);
    
    fmpq_poly_one(prunm2);
    fmpq_poly_set(prunm1, px);
    fmpq_poly_one(prun);
    
    for (int i = 2; i<=iterations; i++) {
        
        fmpq_poly_pow(prunm2, prunm2, 4);
        fmpq_poly_mul(prunm2, prunm2, px);
        
        fmpq_poly_pow(prun, prunm1, 2);
        fmpq_poly_add(prun, prun, prunm2);
        
        fmpq_poly_set(prunm2, prunm1);
        fmpq_poly_set(prunm1, prun);
    }
     
    fmpq_poly_clear(prunm1);
    fmpq_poly_clear(prunm2);
    fmpq_poly_clear(pone);
    fmpq_poly_clear(px);
}

void Wilkinson_polynomial(fmpq_poly_t pdest, int degree){
    
    fmpq_poly_t ptemp;
    fmpq_poly_init2(ptemp,2);
    
    fmpq_poly_one(pdest);
    fmpq_poly_zero(ptemp);
    fmpq_poly_set_coeff_si_ui(ptemp, 1, 1, 1);
    
    for (int i=1; i<=degree; i++){
        fmpq_poly_set_coeff_si_ui(ptemp, 0, -i, 1);
        fmpq_poly_mul(pdest, pdest, ptemp);
    }
    
    fmpq_poly_clear(ptemp);
}

void MignotteGen_polynomial( fmpq_poly_t dest, int degree, int bitsize, int power){
    mignotte_generalized(dest, (slong) degree, (ulong) power, (slong) bitsize);
}

void MignotteMul_polynomial( fmpq_poly_t dest, int degree, int bitsize, int power){
    mignotte_polynomial(dest, (slong) degree, (ulong) bitsize);
    fmpq_poly_pow(dest, dest, (ulong) power);
}

void WilkRat_polynomial(  fmpq_poly_t dest, int degree){
    
    fmpq_poly_t ptemp;
    fmpq_poly_init2(ptemp,2);
    fmpq_poly_one(dest);
    fmpq_poly_zero(ptemp);
    fmpq_poly_set_coeff_si_ui(ptemp, 1, 1, 1);
    
    for (int i=1; i<=degree; i++){
        fmpq_poly_set_coeff_si_ui(ptemp, 0, -i, ((ulong) degree)+1);
        fmpq_poly_mul(dest, dest, ptemp);
    }
    
    fmpq_poly_clear(ptemp);
    
}

void WilkMul_polynomial(  fmpq_poly_t dest, int degree){
    
    fmpq_poly_t ptemp;
    fmpq_poly_init2(ptemp,degree+1);
    fmpq_poly_one(dest);
    
    for (int i=1; i<=degree; i++){
        fmpq_poly_zero(ptemp);
        fmpq_poly_set_coeff_si_ui(ptemp, 1, 1, 1);
        fmpq_poly_set_coeff_si_ui(ptemp, 0, -i, 1);
        fmpq_poly_pow(ptemp, ptemp, (ulong) i);
//         fmpq_poly_pow(wilkptemp, ptemp, (ulong) (degree-i)+1);
        fmpq_poly_mul(dest, dest, ptemp);
    }
    
    fmpq_poly_clear(ptemp);
}

void WilkMulF_polynomial(  fmpq_poly_t dest, int degree, int power){
    fmpq_poly_t ptemp;
    fmpq_poly_init2(ptemp,degree+1);
    fmpq_poly_one(dest);
    
    for (int i=1; i<=degree; i++){
        fmpq_poly_zero(ptemp);
        fmpq_poly_set_coeff_si_ui(ptemp, 1, 1, 1);
        fmpq_poly_set_coeff_si_ui(ptemp, 0, -i, 1);
        fmpq_poly_pow(ptemp, ptemp, (ulong) power);
//         fmpq_poly_pow(wilkptemp, ptemp, (ulong) (degree-i)+1);
        fmpq_poly_mul(dest, dest, ptemp);
    }
    
    fmpq_poly_clear(ptemp);
}

void Laguerre_polynomial(  fmpq_poly_t dest, int degree){
    
    fmpq_poly_t pone, pzero, ptemp;
    fmpq_poly_init(pone);
    fmpq_poly_init(pzero);
    fmpq_poly_init(ptemp);
    fmpq_t coeff;
    fmpq_init(coeff);
    
    fmpq_poly_one(pzero);
    fmpq_poly_one(pone);
    fmpq_poly_set_coeff_si_ui(pone, 1, -1, 1);
    fmpq_poly_one(ptemp);
    fmpq_poly_set_coeff_si_ui(ptemp, 1, -1, 1);
    
    for (int i = 1; i<degree; i++) {
        
        fmpq_poly_set_coeff_si_ui(ptemp, 0, 2*i+1, 1);
        
        fmpq_poly_mul(dest, ptemp, pone);
        fmpq_set_si(coeff, (slong) i, 1);
        fmpq_mul_si(coeff, coeff, (slong) -i);
        fmpq_poly_scalar_mul_fmpq(pzero, pzero, coeff);
        fmpq_poly_add(dest, dest, pzero);
        
        fmpq_poly_set(pzero, pone);
        fmpq_poly_set(pone, dest);
    }
    
    fmpq_poly_clear(pone);
    fmpq_poly_clear(pzero);
    fmpq_poly_clear(ptemp);
}

void nestedClusters_polynomial(  fmpq_poly_t dest, int c, int a, int n) {
    
    /* compute degree = nbroots^n */
    slong degree = 1;
    for (int i = 1; i<= n; i++)
        degree = degree*c;
//     printf("degree: %ld\n", degree);
    /* compute nbroots^n power sums of resulting polynomial */
    fmpq_ptr pss = (fmpq_ptr) MALLOC ( (degree+1)*sizeof(fmpq) );
    for (slong i = 0; i<degree+1; i++) {
        fmpq_init(pss+i);
        fmpq_zero(pss+i);
    }
    fmpq_set_si(pss+0, 1, 1);
    
    for (int it = 1; it<=n; it++){
        
//         printf("---iteration: %d\n", it);
        
        fmpq_t ac, atc, nps, temp;
        fmpq_init(ac);
        fmpq_init(atc);
        fmpq_init(nps);
        fmpq_init(temp);
        fmpq_set_si(ac, (slong) a, 1);
        fmpq_pow_si(ac, ac, c);
        
        slong qc=degree;
        while ( qc>=0 ){
//             printf("------h=qc: %ld\n", qc);
            fmpq_zero(nps);
            fmpq_one(atc);
            for (slong tc = 0; tc<=qc; tc=tc+c ){
//                 printf("---------tc: %ld\n", tc);
                fmpq_one(temp);
                fmpz_bin_uiui( fmpq_numref(temp), qc, tc );
                fmpq_mul(temp, temp, pss+tc);
                fmpq_div(temp, temp, atc);
                fmpq_add(nps, nps, temp);
                /* atc = atc*ac */
                fmpq_mul(atc, atc, ac);
            }
            fmpq_mul_si(nps, nps, c);
            fmpq_set(pss+qc, nps);
            qc = qc - c;
        }
        
        
        fmpq_clear(ac);
        fmpq_clear(atc);
        fmpq_clear(nps);
        fmpq_clear(temp);
    }
    
//     printf("power sums: \n");
//     for (slong h = 0; h<=degree; h++){
//         printf("%ld-th ps: ",h); fmpq_print(pss+h); printf("\n");
//     }
    
    /* apply Newton identities to recover coefficients */
    fmpq_poly_init2(dest, degree+1);
    dest->length = degree+1;

    fmpq_t temp, coeff;
    fmpq_init(temp);
    fmpq_init(coeff);
    
    fmpq_poly_set_coeff_si_ui(dest, degree, 1, 1);
    for (slong i = 1; i<=degree; i++){
        
        if ((i%c)==0) {
            fmpq_set( coeff, pss+i );
            for(slong j = 1; j<=i-1; j++){
                fmpq_poly_get_coeff_fmpq( temp, dest, degree-j );
                fmpq_mul( temp, temp, pss+(i-j) );
                fmpq_add( coeff, coeff, temp);
            }
            fmpq_neg( coeff, coeff );
            fmpq_div_ui( coeff, coeff, (ulong) i );
        } else {
            fmpq_zero( coeff );
        }
        
        fmpq_poly_set_coeff_fmpq( dest, degree-i, coeff );
    }
    
    fmpq_clear(temp);
    fmpq_clear(coeff);
    
    for (slong i = 0; i<degree; i++) {
        fmpq_clear(pss+i);
    }
    FREE(pss);
}

void _spiral_polynomial( acb_poly_t dest, int degree, slong prec){
    
    fmpq_t modu;
    fmpq_t argu;
    acb_t a_modu;
    acb_t a_argu;
    acb_t coeff;
    
    fmpq_init(modu);
    fmpq_init(argu);
    acb_init(a_modu);
    acb_init(a_argu);
    acb_init(coeff);
    
    acb_poly_t temp;
    acb_poly_init2(temp,2);
    acb_poly_set_coeff_si(temp, 1, 1);
    
    acb_poly_one(dest);
    slong prectemp = degree*prec;
    
    for(int i=1; i<=degree; i++){
        fmpq_set_si(modu, -i, (ulong) degree);
        fmpq_set_si(argu, 4*i, (ulong) degree);
        acb_set_fmpq( a_modu, modu, prectemp);
        acb_set_fmpq( a_argu, argu, prectemp);
        acb_exp_pi_i( coeff, a_argu, prectemp);
        acb_mul( coeff, coeff, a_modu, prectemp);
        acb_poly_set_coeff_acb(temp, 0, coeff);
        acb_poly_mul(dest, dest, temp, prectemp);
//         printf("%s\n", arb_get_str(acb_realref(coeff), prec, 0));
    }
    
    fmpq_clear(modu);
    fmpq_clear(argu);
    acb_clear(a_modu);
    acb_clear(a_argu);
    acb_poly_clear(temp);
    
}

void spiral_polynomial( arb_poly_t pr, arb_poly_t pi, int degree, slong prec){
    
    acb_poly_t dest;
    acb_poly_init(dest);
    _spiral_polynomial( dest, degree, 16*prec);
    for (int i=0; i<=degree; i++) {
        arb_poly_set_coeff_arb( pr, (slong) i, acb_realref( dest->coeffs + (slong) i ) );
        arb_poly_set_coeff_arb( pi, (slong) i, acb_imagref( dest->coeffs + (slong) i ) );
        arb_poly_set_round(pr, pr, prec);
        arb_poly_set_round(pi, pi, prec);
    }
    acb_poly_clear(dest);
}

// void genSpiralPolFile( FILE * file, int degree, int prec){
//     
//     acb_poly_t dest;
//     acb_poly_init(dest);
//     
//     fmpq_t modu;
//     fmpq_t argu;
//     acb_t a_modu;
//     acb_t a_argu;
//     acb_t coeff;
//     
//     fmpq_init(modu);
//     fmpq_init(argu);
//     acb_init(a_modu);
//     acb_init(a_argu);
//     acb_init(coeff);
//     
//     acb_poly_t temp;
//     acb_poly_init2(temp,2);
//     acb_poly_set_coeff_si(temp, 1, 1);
//     
//     acb_poly_one(dest);
//     slong prectemp = degree*prec;
//     
//     for(int i=1; i<=degree; i++){
//         fmpq_set_si(modu, -i, (ulong) degree);
//         fmpq_set_si(argu, 4*i, (ulong) degree);
//         acb_set_fmpq( a_modu, modu, prectemp);
//         acb_set_fmpq( a_argu, argu, prectemp);
//         acb_exp_pi_i( coeff, a_argu, prectemp);
//         acb_mul( coeff, coeff, a_modu, prectemp);
//         acb_poly_set_coeff_acb(temp, 0, coeff);
//         acb_poly_mul(dest, dest, temp, prectemp);
// //         printf("%s\n", arb_get_str(acb_realref(coeff), prec, 0));
//     }
//     
//     fmpq_clear(modu);
//     fmpq_clear(argu);
//     acb_clear(a_modu);
//     acb_clear(a_argu);
//     acb_poly_clear(temp);
//     
//     
// //     fprintf(file, "Sparse;\n");
//     fprintf(file, "Monomial;\n");
//     fprintf(file, "Complex;\n");
//     fprintf(file, "FloatingPoint;\n");
//     fprintf(file, "Degree = %d;\n", (int) degree);
//     fprintf(file, "Precision = %d;\n", (int) prec);
//     fprintf(file, "\n");
//     
//     char tempstr[100*prec];
//     char * temp2;
//     
//     for(int i = 0; i<=degree; i++){
//         acb_set(coeff, acb_poly_getCoeff(dest, i));
//         
// //         printf("%s\n", arb_get_str(acb_realref(coeff), prec, 0));
//         
//         temp2 = arb_get_str(acb_realref(coeff), prec, ARB_STR_NO_RADIUS);
//         sprintf(tempstr, "%s", temp2);
//         free(temp2);
//         if (tempstr[0]=='[') sprintf(tempstr, "0.0");
//         fprintf(file, "%s ", tempstr);
//         temp2 = arb_get_str(acb_imagref(coeff), prec, ARB_STR_NO_RADIUS);
//         sprintf(tempstr, "%s", temp2);
//         free(temp2);
//         if (tempstr[0]=='[') sprintf(tempstr, "0.0");
//         fprintf(file, "%s\n", tempstr);
//         
//     }
// 
//     acb_clear(coeff);    
//     acb_poly_clear(dest);
// }

void clustersIterate( acb_poly_ptr tabres, acb_poly_ptr tabprec, int i, slong prec){
    // tabres is a table of 3^i acb_poly
    // tabres is a table of 3^(i-1) acb_poly
    fmpq_t modu;
    fmpq_t argu;
    acb_t a_modu;
    acb_t a_argu;
    acb_t coeff;
    
    fmpq_init(modu);
    fmpq_init(argu);
    acb_init(a_modu);
    acb_init(a_argu);
    acb_init(coeff);
    int indexInTabRes = 0;
//     printf("pow(3,i-1): %d\n", (int) pow(3,i-1));
    for (int j = 0; j<((int) pow(3,i-1)); j++){
//         printf("%d\n", j);
//         fmpq_set_si(modu, -1, (ulong) 0x1<<(4*(i-1)));
        fmpq_set_si(modu, -1, (ulong) pow(4, 2*(i-1)));
        acb_set_fmpq( a_modu, modu, prec);
        
        fmpq_set_si(argu, 2, 3);
        acb_set_fmpq( a_argu, argu, prec);
        acb_exp_pi_i( coeff, a_argu, prec);
        acb_mul( coeff, coeff, a_modu, prec);
        acb_add( coeff, coeff, acb_poly_getCoeff(tabprec + j, 0), prec);
        acb_poly_set( tabres + indexInTabRes, tabprec + j);
        acb_poly_set_coeff_acb(tabres + indexInTabRes, 0, coeff);
        indexInTabRes +=1;
        
        fmpq_set_si(argu, 4, 3);
        acb_set_fmpq( a_argu, argu, prec);
        acb_exp_pi_i( coeff, a_argu, prec);
        acb_mul( coeff, coeff, a_modu, prec);
        acb_add( coeff, coeff, acb_poly_getCoeff(tabprec + j, 0), prec);
        acb_poly_set( tabres + indexInTabRes, tabprec + j);
        acb_poly_set_coeff_acb(tabres + indexInTabRes, 0, coeff);
        indexInTabRes +=1;
        
        fmpq_set_si(argu, 6, 3);
        acb_set_fmpq( a_argu, argu, prec);
        acb_exp_pi_i( coeff, a_argu, prec);
        acb_mul( coeff, coeff, a_modu, prec);
        acb_add( coeff, coeff, acb_poly_getCoeff(tabprec + j, 0), prec);
        acb_poly_set( tabres + indexInTabRes, tabprec + j);
        acb_poly_set_coeff_acb(tabres + indexInTabRes, 0, coeff);
        indexInTabRes +=1;
        
    }
    fmpq_clear(modu);
    fmpq_clear(argu);
    acb_clear(a_modu);
    acb_clear(a_argu);
    acb_clear(coeff);    
}

void genClusterPolFile( FILE * file, int iterations, int prec){
    
    acb_poly_ptr tabprec;
    tabprec = (acb_poly_ptr) malloc (1*sizeof(acb_poly));
    acb_poly_init2( tabprec, 2);
    acb_poly_zero(tabprec);
    acb_poly_set_coeff_si(tabprec, 1, 1);
    int degree = 3;
    
    slong prectemp = iterations*10*prec;
    
    for (int i=1; i<=iterations; i++) {
        acb_poly_ptr tabres;
        tabres = (acb_poly_ptr) malloc (degree*sizeof(acb_poly));
        for(int j = 0; j<degree; j++) acb_poly_init2( tabres + j, 2);
        
        clustersIterate( tabres, tabprec, i, prectemp);
        for(int j = 0; j<((int) (degree/3)); j++) acb_poly_clear( tabprec + j);
        free(tabprec);
        tabprec = tabres;
        degree = degree*3;
    }
    
    degree = (int) degree/3;
    acb_poly_t dest;
    acb_poly_init2(dest, degree+1);
    acb_poly_one(dest);
    for(int j = 0; j<degree; j++) acb_poly_mul(dest, dest, tabprec +j, prectemp);
    for(int j = 0; j<degree; j++) acb_poly_clear( tabprec + j);
    free(tabprec);
    
    acb_t coeff;
    acb_init(coeff);
    
//     fprintf(file, "Sparse;\n");
    fprintf(file, "Monomial;\n");
    fprintf(file, "Complex;\n");
    fprintf(file, "FloatingPoint;\n");
    fprintf(file, "Degree = %d;\n", (int) degree);
    fprintf(file, "Precision = %d;\n", (int) prec);
    fprintf(file, "\n");
    
    char tempstr[100*prec];
    char * temp2;
    
    for(int i = 0; i<=degree; i++){
        acb_set(coeff, acb_poly_getCoeff(dest, i));
        
//         printf("%s\n", arb_get_str(acb_realref(coeff), prec, 0));
        temp2 = arb_get_str(acb_realref(coeff), prec, ARB_STR_NO_RADIUS);
        sprintf(tempstr, "%s", temp2);
        free(temp2);
        if (tempstr[0]=='[') sprintf(tempstr, "0.0");
        fprintf(file, "%s ", tempstr);
        temp2 = arb_get_str(acb_imagref(coeff), prec, ARB_STR_NO_RADIUS);
        sprintf(tempstr, "%s", temp2);
        free(temp2);
        if (tempstr[0]=='[') sprintf(tempstr, "0.0");
        fprintf(file, "%s\n", tempstr);
        
    }

    acb_clear(coeff);    
    acb_poly_clear(dest);
}
