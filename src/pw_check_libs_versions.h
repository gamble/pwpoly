/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_CHECK_VERSIONS_H
#define PW_CHECK_VERSIONS_H

#ifndef PW_NO_CHECK_LIBS_VERSIONS

#include "flint/flint.h"
#if __FLINT_VERSION < 2
#error FLINT 2.8.5 or later is required
#endif
#if (__FLINT_VERSION == 2) && (__FLINT_VERSION_MINOR < 8)
#error FLINT 2.8.5 or later is required
#endif
#if (__FLINT_VERSION == 2) && (__FLINT_VERSION_MINOR == 8) && (__FLINT_VERSION_PATCHLEVEL < 5)
#error FLINT 2.8.5 or later is required
#endif

#include "arb.h"
#if __ARB_VERSION < 2
#error ARB 2.22.1 or later is required
#endif
#if (__ARB_VERSION == 2) && (__ARB_VERSION_MINOR < 22)
#error ARB 2.22.1 or later is required
#endif
#if (__ARB_VERSION == 2) && (__ARB_VERSION_MINOR == 22) && (__ARB_VERSION_PATCHLEVEL < 1)
#error ARB 2.22.1 or later is required
#endif

#else

#include "arb.h"
#include "acb.h"
#ifndef acb_add_error_arb
ACB_INLINE void acb_add_error_arb(acb_t x, const arb_t err) {
    arb_add_error(acb_realref(x), err);
    arb_add_error(acb_imagref(x), err);
}
#endif

#endif

#endif
