/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_BASE_H
#define PW_BASE_H

/*********************/
/* compiling options */
/*********************/
// #define PW_SILENT               /* when defined at pre-compiling,                                */
                                   /* all sections of code containing prints to stdout are removed  */
// #define PW_NO_INTERFACE         /* when defined at pre-compiling,                                */
                                   /* all sections of code concerning i/o are removed               */ 
// #define PW_MEMORY               /* define this flag at pre-compiliing to define function for changing  */
                                   /* memory allocators                                             */
// #define PW_CHECK_INTERRUPT      /* when defined at pre-compiling,                                */
                                   /* users can define an interruption function which is regularly  polled */
                                   
#define PW_EA_PROFILE              /* profile multi-precision Ehrich Aberth iterations              */
#define PW_COCO_PROFILE            /* profile Graeffe-Pellet subdivision solver                     */
#define PW_PROFILE                 /* profile overall code                                          */

#ifdef PW_NO_INTERFACE
#ifndef PW_SILENT
#define PW_SILENT
#endif
#endif

#ifdef PW_SILENT
#undef PW_EA_PROFILE
#undef PW_COCO_PROFILE
#undef PW_PROFILE
#endif

#ifndef PW_SILENT
#define PW_VERBOSE_ARGU(a) ,int a
#define PW_VERBOSE_CALL(a) ,a
#else
#define PW_VERBOSE_ARGU(a)
#define PW_VERBOSE_CALL(a)
#endif

#ifndef PW_NO_INTERFACE
#define PW_FILE_ARGU(a) ,FILE * a
#define PW_FILE_CALL(a) ,a
#else
#define PW_FILE_ARGU(a)
#define PW_FILE_CALL(a)
#endif

// #define PWPOLY_HAS_BEFFT        /* when defined at pre-compiling,                                  */
                                   /* uses doubles with bounded error and FFT on doubles with bounded */
                                   /* error from befft library                                        */
// #define PWPOLY_HAS_EAROOTS      /* when defined at pre-compiling,                                  */
                                   /* uses Ehrich Aberth iterations with doubles from earoots library */
                                   /* DOES NOT WORK IF PWPOLY_HAS_BEFFT IS NOT DEFINED                */
#ifdef PWPOLY_HAS_BEFFT
#ifndef BEFFT_HAS_ARB
#define BEFFT_HAS_ARB
#endif
#endif

#ifdef PWPOLY_NO_INLINES              /* cancels inlining */
#define PWPOLY_INLINE static
#else
#define PWPOLY_INLINE static inline
#endif

/*********************/
/* MACROS            */
/*********************/

#define PWPOLY_MAX(A,B) (A>=B? A : B)
#define PWPOLY_MIN(A,B) (A<=B? A : B)
#define PWPOLY_ABS(A) (A<=0? -A : A)
#define PWPOLY_DEFAULT_PREC 53

/* for doubles */
#define PWPOLY_DOUBLE_PREC 53
/* if rounding mode is nearest, u = (1/2) 2^(1-PWPOLY_DOUBLE_PREC) = 2^-PWPOLY_DOUBLE_PREC */
/* below we assume rounding mode is nearest */
#define PWPOLY_LOG2U -PWPOLY_DOUBLE_PREC /* assume round to nearest */
#define PWPOLY_U (ldexp(0x1, PWPOLY_LOG2U))
#define PWPOLY_SQRTTWO  1.42             /* not used */

/*********************/
/* includes          */
/*********************/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "pw_check_libs_versions.h" /* check that flint >=2.8.5 and arb >=2.22.1 */
#include "flint/fmpz.h"
#include "flint/fmpq.h"
#include "flint/fmpq_poly.h"
#include "flint/fmpq_vec.h"
#include "flint/flint.h"
#include "flint/arith.h"

#include "acb.h"
#include "acb_poly.h"
#include "acb_mat.h"

#include <mpfr.h>

#ifdef PWPOLY_HAS_BEFFT
#include <fenv.h>
#endif

#include "windows.h"

#ifdef __cplusplus
extern "C" {
#endif
    
/**********************/
/* some new typedefs  */
/**********************/

#ifndef __compar_fn_t                              /* comparisons functions profiles for qsort */
#if defined(_MSC_VER)
typedef int(*__compar_fn_t) (const void *, const void *);
#else
typedef int(*__compar_fn_t) (__const void *, __const void *);
#endif
#endif

typedef void (*oracle)(acb_poly_t, slong);
    
#ifndef uint
typedef unsigned int uint;
#endif

#ifndef acb_poly_ptr
typedef acb_poly_struct * acb_poly_ptr;
#endif
#ifndef acb_poly_srcptr
typedef const acb_poly_struct * acb_poly_srcptr;
#endif

#ifndef acb_mat_ptr
typedef acb_mat_struct * acb_mat_ptr;
#endif

#ifndef fmpz_ptr
typedef fmpz * fmpz_ptr;
#endif

#ifndef fmpq_ptr
typedef fmpq * fmpq_ptr;
#endif

#ifndef fmpq_poly_ptr
typedef fmpq_poly_struct * fmpq_poly_ptr;
#endif

#ifndef fmpq_poly_srcptr
typedef const fmpq_poly_struct * fmpq_poly_srcptr;
#endif

/*********************/
/* MACROS for goals  */
/*********************/
#define PW_GOAL_ISO_B 0 /* first bit of goal says wether output roots must be isolated (1) or clustered (0) */
#define PW_GOAL_APP_B 1 /* second bit of goal says wether output roots must have precision required by user (1) or not (0) */
#define PW_GOAL_ABS_B 2 /* third bit of goal says wether precision is relative (0, default) of absolute (1) */
#define PW_GOAL_REF_B 3 /* fourth bit of goal says wether the goal is to refine an input domain             */
#define PW_GOAL_ISOLATE     1     /* isolate                                      */
#define PW_GOAL_APPROXIMATE 2     /* approximate with relative precision          */
#define PW_GOAL_ISO_AND_APP 3     /* isolate and refine with relative precision   */
#define PW_GOAL_APPROXIMATE_ABS 6 /* approximate with absolute precision          */
#define PW_GOAL_ISO_AND_APP_ABS 7 /* isolate and refine with absolute precision   */
#define PW_GOAL_REFINE      10    /* refine (and approximate), relative precision */
#define PW_GOAL_REFINE_ABS  14    /* refine (and approximate), absolute precision */
#define PW_GOAL_ISOLATE_STR     "isolate"
#define PW_GOAL_APPROXIMATE_STR "approximate"
#define PW_GOAL_ISO_AND_APP_STR "isolate_AND_approximate"
#define PW_GOAL_APPROXIMATE_ABS_STR "approximate_absolute"
#define PW_GOAL_ISO_AND_APP_ABS_STR "isolate_AND_approximate_absolute"
#define PW_GOAL_REFINE_STR "refine"
#define PW_GOAL_REFINE_ABS_STR "refine_absolute"
#define PW_GOAL_ISOLATE_STR_S     "i"
#define PW_GOAL_APPROXIMATE_STR_S "a"
#define PW_GOAL_ISO_AND_APP_STR_S "ia"
#define PW_GOAL_APPROXIMATE_ABS_STR_S "aa"
#define PW_GOAL_ISO_AND_APP_ABS_STR_S "iaa"
#define PW_GOAL_REFINE_STR_S "r"
#define PW_GOAL_REFINE_ABS_STR_S "ra"
#define PW_GOAL_MUST_ISOLATE(s)     ( (s>>PW_GOAL_ISO_B)%2 )
#define PW_GOAL_MUST_APPROXIMATE(s) ( (s>>PW_GOAL_APP_B)%2 )
#define PW_GOAL_MUST_REFINE(s)      ( (s>>PW_GOAL_REF_B)%2 )
#define PW_GOAL_PREC_RELATIVE(s)    ( ((s>>PW_GOAL_ABS_B)%2)==0 )
#define PW_GOAL_PREC_ABSOLUTE(s)    ( (s>>PW_GOAL_ABS_B)%2 )

#define PW_CAP_GOALNBROOTS(G,D) ( G<0? D : PWPOLY_MIN( G, D ) )

/********************************************/
/* functions for setting memory allocators  */
/********************************************/
#ifndef PW_MEMORY
PWPOLY_INLINE void * pwpoly_malloc  (size_t size)             { return malloc (size); }
/* not used but defined for standard... */
PWPOLY_INLINE void * pwpoly_calloc  (size_t num, size_t size) { return calloc (num, size); }
PWPOLY_INLINE void * pwpoly_realloc (void * ptr, size_t size) { return realloc(ptr, size); }
PWPOLY_INLINE void   pwpoly_free    (void * ptr)              { free(ptr); }
#else
void * pwpoly_malloc (size_t size);
void * pwpoly_calloc (size_t num, size_t size);
void * pwpoly_realloc(void * ptr, size_t newsize);
void   pwpoly_free   (void * ptr);

typedef void * (*malloc_func) (size_t size);
typedef void * (*calloc_func) (size_t num, size_t size);
typedef void * (*realloc_func)(void * ptr, size_t newsize);
typedef void   (*free_func)   (void * ptr);

EXT_DECL void __pwpoly_set_memory_functions( malloc_func mf, calloc_func cf, realloc_func rf, free_func ff);
EXT_DECL void __pwpoly_reset_memory_functions( );
#endif

/********************************************/
/* functions for checking interruptions     */
/********************************************/

#ifdef PW_CHECK_INTERRUPT
int pwpoly_interrupt (int a);
typedef int (*interrupt_func) ();
void __pwpoly_set_interrupt_function( interrupt_func ifu );
void __pwpoly_reset_interrupt_function( );
#endif

/********************************************************************************/
/* some debugging flags: do not uncomment unless you know what you are doing    */
/********************************************************************************/
// #define PWPOLY_TRACK_EXCEPT
// #define PWPOLY_DEBUG_EVALUATION

/**********************/
/* profiling code     */
/**********************/
#ifdef PW_PROFILE
#include <time.h>
extern double clicks_in_earoots_global;
extern double clicks_in_eaitts_global;
extern double clicks_in_checks_global;
extern double clicks_in_EA_mp;
extern double clicks_in_CoCo_global;
extern double clicks_in_covering;
extern double clicks_in_approx_annulii;
extern double clicks_in_expansion;
extern double clicks_in_compress;
extern double clicks_in_normalize;
extern double clicks_in_ffts;
extern double clicks_in_ffts_arb;
extern double clicks_in_ffts_be;
extern double clicks_in_ffts_conv_be;
extern double clicks_in_locate;
extern double clicks_in_evaluate;
extern double clicks_in_locate_eval;
extern double clicks_in_sort_eval;
extern double clicks_in_eval_eval;
extern double clicks_in_inclusion;
extern double clicks_in_conv_mps;
extern double clicks_in_mpsolve;
extern double clicks_in_conv_dbl;
extern double clicks_in_earoots;
extern double clicks_in_refineEA;
extern double clicks_in_CoCo;
extern double clicks_in_fewRoots[3];
extern double clicks_in_fewRoots_be[3];
extern double clicks_in_fewRoot_ffts;
extern double clicks_in_fewRoot_ffts_precomp;
extern double clicks_in_arbpellet;
extern double clicks_in_bepellet;
extern double clicks_in_arbDLG;
extern double clicks_in_beDLG;
extern double clicks_in_merge;
extern double clicks_in_solve_approx;
extern double clicks_in_refine_piecewise;
extern double clicks_in_refine_compApp;
extern double clicks_in_refine_locate;
extern double clicks_in_refine_CoCo;
extern double clicks_in_refine_disc;
extern double clicks_in_CoCo_refine;
extern ulong  nb_sectors_in_covering;
extern ulong  nb_call_fft_conv;
extern ulong  nb_succ_fft_conv;
extern ulong  nb_call_fft_be;
extern ulong  nb_succ_fft_be;
extern ulong  nb_call_fft_arb;
extern ulong  nb_dbl_conv;
extern ulong  nb_succ_dbl_conv;
extern ulong  nb_exce_dbl_conv;
extern ulong  nb_arbpellet_tests;
extern ulong  nb_bepellet_tests;
extern ulong  nb_succ_bepellet_tests;
extern ulong  nb_exce_bepellet_tests;
extern ulong  nb_som_root;
extern ulong  nb_und_root;
extern ulong  nb_one_root;
extern ulong  nb_two_root;
extern ulong  nb_thr_root;
extern ulong  nb_sector_solved;
extern ulong  nb_sector_solved_mpsolve;
extern ulong  nb_sector_solved_earoots;
extern ulong  nb_sector_solved_CoCo;
extern ulong  nb_succ_earoots;
extern ulong  nb_exce_earoots;
extern ulong  nb_succ_refineEA;
extern ulong  nb_call_refineEA;
extern ulong  nb_succ_fewRoots[3];
extern ulong  nb_call_fewRoots[3];
extern ulong  nb_succ_fewRoots_be[3];
extern ulong  nb_call_fewRoots_be[3];

#define PW_INIT_PROFILE {\
    clicks_in_earoots_global = 0.; \
    clicks_in_checks_global = 0.; \
    clicks_in_eaitts_global = 0.; \
    clicks_in_EA_mp = 0.; \
    clicks_in_CoCo_global = 0.; \
    clicks_in_covering = 0.; \
    clicks_in_approx_annulii = 0.; \
    clicks_in_expansion = 0.; \
    clicks_in_compress = 0.; \
    clicks_in_normalize = 0.; \
    clicks_in_ffts = 0.; \
    clicks_in_ffts_arb = 0.; \
    clicks_in_ffts_be = 0.; \
    clicks_in_ffts_conv_be = 0.; \
    clicks_in_locate = 0.; \
    clicks_in_evaluate = 0.; \
    clicks_in_locate_eval = 0.; \
    clicks_in_sort_eval = 0.; \
    clicks_in_eval_eval = 0.; \
    clicks_in_inclusion= 0.; \
    clicks_in_conv_mps=0.;\
    clicks_in_mpsolve=0.;\
    clicks_in_conv_dbl=0.;\
    clicks_in_earoots=0.;\
    clicks_in_refineEA=0.;\
    clicks_in_CoCo=0.;\
    for( int i=0; i<3; i++ ) clicks_in_fewRoots[i] = 0.;\
    for( int i=0; i<3; i++ ) clicks_in_fewRoots_be[i] = 0.;\
    clicks_in_fewRoot_ffts=0.;\
    clicks_in_fewRoot_ffts_precomp=0.;\
    clicks_in_arbpellet=0.;\
    clicks_in_bepellet=0.;\
    clicks_in_arbDLG=0.;\
    clicks_in_beDLG=0.;\
    clicks_in_merge=0.;\
    clicks_in_solve_approx=0.;\
    clicks_in_refine_piecewise=0.;\
    clicks_in_refine_compApp=0.;\
    clicks_in_refine_locate=0.;\
    clicks_in_refine_CoCo=0.;\
    clicks_in_refine_disc=0.;\
    clicks_in_CoCo_refine = 0.; \
    nb_sectors_in_covering=0;\
    nb_call_fft_conv=0;\
    nb_succ_fft_conv=0;\
    nb_call_fft_be=0;\
    nb_succ_fft_be=0;\
    nb_call_fft_arb=0;\
    nb_dbl_conv=0;\
    nb_succ_dbl_conv=0;\
    nb_exce_dbl_conv=0;\
    nb_arbpellet_tests=0;\
    nb_bepellet_tests=0;\
    nb_succ_bepellet_tests=0;\
    nb_exce_bepellet_tests=0;\
    nb_som_root=0;\
    nb_und_root=0;\
    nb_one_root=0;\
    nb_two_root=0;\
    nb_thr_root=0;\
    nb_sector_solved=0;\
    nb_sector_solved_mpsolve=0;\
    nb_sector_solved_earoots=0;\
    nb_sector_solved_CoCo=0;\
    nb_succ_earoots=0;\
    nb_exce_earoots=0;\
    nb_succ_refineEA=0;\
    nb_call_refineEA=0;\
    for( int i=0; i<3; i++ ) nb_succ_fewRoots[i]=0;\
    for( int i=0; i<3; i++ ) nb_call_fewRoots[i]=0;\
    for( int i=0; i<3; i++ ) nb_succ_fewRoots_be[i]=0;\
    for( int i=0; i<3; i++ ) nb_call_fewRoots_be[i]=0;\
}

#ifndef PW_NO_INTERFACE
#define PW_PRINT_PROFILE {\
    printf("\n"); \
    printf("&&&&&&&&&&&&&&&&&&&&&&& PW PROFILE &&&&&&&&&&&&&&&&&&&&&&&&\n"); \
    if (clicks_in_earoots_global!=0) { \
    printf("time in initial earoots:                     %lf\n", clicks_in_earoots_global/CLOCKS_PER_SEC ); \
    printf("     -->EA iterations:                          %lf\n", clicks_in_eaitts_global/CLOCKS_PER_SEC ); \
    printf("     -->checking separation:                    %lf\n", clicks_in_checks_global/CLOCKS_PER_SEC ); \
    printf("\n"); \
    } \
    if (clicks_in_covering!=0) { \
    printf("time in computing covering:                  %lf\n", clicks_in_covering/CLOCKS_PER_SEC ); \
    printf("\n"); \
    } \
    if (clicks_in_approx_annulii!=0) { \
    printf("time in computing approximations:            %lf\n", clicks_in_approx_annulii/CLOCKS_PER_SEC ); \
    printf("     -->computing expansions:                   %lf\n", clicks_in_expansion/CLOCKS_PER_SEC ); \
    printf("     -->compressing expansions:                 %lf\n", clicks_in_compress/CLOCKS_PER_SEC ); \
    if (clicks_in_ffts_be!=0) { \
    printf("     -->computing ffts with be:                 %lf\n", clicks_in_ffts_be/CLOCKS_PER_SEC ); \
    printf("        -->normalizing expansions:                 %lf\n", clicks_in_normalize/CLOCKS_PER_SEC ); \
    printf("        -->double<->arb conversions:               %lf\n", clicks_in_ffts_conv_be/CLOCKS_PER_SEC ); \
    printf("           -->nb succ. | nb calls:                 %lu | %lu \n", nb_succ_fft_conv, nb_call_fft_conv); \
    printf("        -->ffts:                                   %lf\n", (clicks_in_ffts_be-clicks_in_ffts_conv_be)/CLOCKS_PER_SEC ); \
    printf("           -->nb succ. | nb calls:                 %lu | %lu \n", nb_succ_fft_be, nb_call_fft_be); \
    } \
    if (clicks_in_ffts_arb!=0) { \
    printf("     -->computing ffts with arb:                %lf\n", clicks_in_ffts_arb/CLOCKS_PER_SEC ); \
    printf("        -->nb calls:                               %lu \n", nb_call_fft_arb); \
    } \
    printf("\n"); \
    } \
    if (clicks_in_solve_approx!=0) { \
    printf("time in solving approximations:              %lf\n", clicks_in_solve_approx/CLOCKS_PER_SEC ); \
    if (clicks_in_conv_dbl!=0) { \
    printf("     -->double<->arb conversions:               %lf\n", clicks_in_conv_dbl/CLOCKS_PER_SEC ); \
    printf("        -->nb succ. | nb except. | nb calls:    %lu | %lu | %lu\n", nb_succ_dbl_conv, nb_exce_dbl_conv, nb_dbl_conv ); \
    } \
    if (clicks_in_bepellet!=0) { \
    printf("     -->Pellet's test with be:                  %lf\n", clicks_in_bepellet/CLOCKS_PER_SEC ); \
    printf("        -->time in be  DLG's:                      %lf\n", clicks_in_beDLG/CLOCKS_PER_SEC ); \
    printf("        -->nb succ. | nb except. | nb calls:        %lu | %lu | %lu\n", nb_succ_bepellet_tests, nb_exce_bepellet_tests, nb_bepellet_tests ); \
    } \
    for(int i=0; i<3; i++) { \
    if (clicks_in_fewRoots_be[i]!=0) { \
    printf("     -->solver %d root(s) with be:               %lf\n",i+1, clicks_in_fewRoots_be[i]/CLOCKS_PER_SEC ); \
    printf("        --> nb success | nb calls:                 %lu | %lu\n", nb_succ_fewRoots_be[i], nb_call_fewRoots_be[i] ); \
    } \
    } \
    if (clicks_in_earoots!=0) { \
    printf("     -->EARoots:                                %lf\n", (clicks_in_earoots+clicks_in_refineEA)/CLOCKS_PER_SEC ); \
    printf("        -->nb succ. | nb except. | nb calls:    %lu | %lu | %lu\n", nb_succ_earoots, nb_exce_earoots, nb_sector_solved_earoots ); \
    if (clicks_in_earoots!=0) { \
    printf("        -->refineEA:                               %lf\n", clicks_in_refineEA/CLOCKS_PER_SEC ); \
    printf("           -->nb success | nb calls:               %lu | %lu\n", nb_succ_refineEA, nb_call_refineEA ); \
    } \
    } \
    if (clicks_in_arbpellet!=0) { \
    printf("     -->Pellet's test with ARB:                 %lf\n", clicks_in_arbpellet/CLOCKS_PER_SEC ); \
    printf("        -->time in acb DLG's:                      %lf\n", clicks_in_arbDLG/CLOCKS_PER_SEC ); \
    printf("        -->nb calls:                               %lu\n", nb_arbpellet_tests ); \
    } \
    for(int i=0; i<3; i++) { \
    if (clicks_in_fewRoots[i]!=0) { \
    printf("     -->solver %d root(s) with ARB:              %lf\n",i+1, clicks_in_fewRoots[i]/CLOCKS_PER_SEC ); \
    printf("        --> nb success | nb calls:                 %lu | %lu\n", nb_succ_fewRoots[i], nb_call_fewRoots[i] ); \
    } \
    } \
    if (clicks_in_fewRoot_ffts!=0) { \
    printf("     -->ffts in solver few roots with ARB:      %lf\n", (clicks_in_fewRoot_ffts + clicks_in_fewRoot_ffts_precomp)/CLOCKS_PER_SEC ); \
    printf("        --> precomp:                               %lf\n", clicks_in_fewRoot_ffts_precomp/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_mpsolve!=0) { \
    printf("     -->MPSolve:                                %lf\n", clicks_in_mpsolve/CLOCKS_PER_SEC ); \
    printf("        -->nb calls:                               %lu\n", nb_sector_solved_mpsolve ); \
    printf("        -->mpsolve<->arb conversions:              %lf\n", clicks_in_conv_mps/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_CoCo!=0) { \
    printf("     -->solver CoCo:                            %lf\n", clicks_in_CoCo/CLOCKS_PER_SEC ); \
    printf("        -->nb calls:                               %lu\n", nb_sector_solved_CoCo ); \
    } \
    printf("     -->computing inclusion disks:              %lf\n", clicks_in_inclusion/CLOCKS_PER_SEC ); \
    printf("        -->evaluations :                           %lf\n", clicks_in_evaluate/CLOCKS_PER_SEC ); \
    printf("        -->locate:                                 %lf\n", clicks_in_locate/CLOCKS_PER_SEC ); \
    printf("     -->eliminating doubles:                    %lf\n", clicks_in_merge/CLOCKS_PER_SEC ); \
    printf("%lu sectors in covering, %lu sectors solved\n", nb_sectors_in_covering, nb_sector_solved);\
    printf("%lu Pellet's tests\n", nb_succ_bepellet_tests + nb_arbpellet_tests);\
    printf("%lu sectors where Pellet's test failed, %lu sectors with roots\n", nb_und_root, nb_som_root);\
    printf("%lu sectors with 1 root, %lu sectors with 2 roots\n", nb_one_root, nb_two_root);\
    printf("%lu sectors with 3 roots\n", nb_thr_root);\
    printf("\n"); \
    } \
    if (clicks_in_evaluate!=0) { \
    printf("time in evaluations :                        %lf\n", clicks_in_evaluate/CLOCKS_PER_SEC ); \
    if (clicks_in_sort_eval!=0) { \
    printf("     --> time in sort:                          %lf\n", clicks_in_sort_eval/CLOCKS_PER_SEC ); \
    }\
    if (clicks_in_locate_eval!=0) { \
    printf("     --> time in locate:                        %lf\n", clicks_in_locate_eval/CLOCKS_PER_SEC ); \
    }\
    if (clicks_in_locate_eval!=0) { \
    printf("     --> time in eval sector:                   %lf\n", clicks_in_eval_eval/CLOCKS_PER_SEC ); \
    }\
    printf("\n"); \
    } \
    if (clicks_in_refine_piecewise!=0) { \
    printf("time in final refining with approx :         %lf\n", clicks_in_refine_piecewise/CLOCKS_PER_SEC ); \
    printf("     --> time in computing approxs.:            %lf\n", clicks_in_refine_compApp/CLOCKS_PER_SEC ); \
    printf("     --> time in locate roots:                  %lf\n", clicks_in_refine_locate/CLOCKS_PER_SEC ); \
    printf("     --> time in solver CoCo:                   %lf\n", clicks_in_refine_CoCo/CLOCKS_PER_SEC ); \
    printf("     --> time in comp. isolator:                %lf\n", clicks_in_refine_disc/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_CoCo_refine!=0) { \
    printf("time in final refining with solver CoCo :    %lf\n", clicks_in_CoCo_refine/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_EA_mp!=0) { \
    printf("time in final EA solver:                     %lf\n", clicks_in_EA_mp/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_CoCo_global!=0) { \
    printf("time in final solver CoCo :                  %lf\n", clicks_in_CoCo_global/CLOCKS_PER_SEC ); \
    } \
    printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"); \
    printf("\n\n"); \
}
#else
#define PW_PRINT_PROFILE
#endif
#endif

#ifdef __cplusplus
}
#endif

#endif
