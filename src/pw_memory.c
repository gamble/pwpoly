/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_base.h"

#ifdef PW_MEMORY
static void * _pwpoly_malloc (size_t);
static void * _pwpoly_calloc (size_t, size_t);
static void * _pwpoly_realloc(void *, size_t);
static void   _pwpoly_free   (void *);

static void * (*__pwpoly_malloc)  (size_t)         = _pwpoly_malloc;
static void * (*__pwpoly_calloc)  (size_t, size_t) = _pwpoly_calloc;
static void * (*__pwpoly_realloc) (void *, size_t) = _pwpoly_realloc;
static void   (*__pwpoly_free)    (void *)         = _pwpoly_free;

void __pwpoly_set_memory_functions( malloc_func mf, calloc_func cf, realloc_func rf, free_func ff) {
    __pwpoly_malloc = mf;
    __pwpoly_calloc = cf;
    __pwpoly_realloc= rf;
    __pwpoly_free   = ff;
}

void __pwpoly_reset_memory_functions( ) {
    __pwpoly_malloc = _pwpoly_malloc;
    __pwpoly_calloc = _pwpoly_calloc;
    __pwpoly_realloc= _pwpoly_realloc;
    __pwpoly_free   = _pwpoly_free;
}

void * _pwpoly_malloc (size_t size){
    return malloc(size);
}

void * _pwpoly_calloc  (size_t num, size_t size){
    return calloc(num, size);
}

void * _pwpoly_realloc (void * ptr, size_t newsize){
    return realloc(ptr, newsize);
}

void   _pwpoly_free   (void * ptr){
    free(ptr);
}

void * pwpoly_malloc (size_t size){
    return (*__pwpoly_malloc)(size);
}

void * pwpoly_calloc  (size_t num, size_t size){
    return (*__pwpoly_calloc)(num, size);
}

void * pwpoly_realloc (void * ptr, size_t newsize){
    return (*__pwpoly_realloc)(ptr, newsize);
}

void   pwpoly_free   (void * ptr){
           (*__pwpoly_free)(ptr);
}
#endif
