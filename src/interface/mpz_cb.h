/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef MPZ_CB_H
#define MPZ_CB_H

// #include "windows.h"

#include "pw_base.h"
// #include "common/pw_roots.h"
#include "flint/fmpz.h"
#include "flint/fmpq_poly.h"
#include <gmp.h>

#include "windows.h"
#ifndef EXT_DECL
#define EXT_DECL
#endif

#define mpz_struct __mpz_struct

#ifdef __cplusplus
extern "C" {
#endif
    
PWPOLY_INLINE double mpz_2exp_mpz_get_d ( const mpz_t m, const mpz_t e ) {
    return mpz_get_d(m)*pow(2, mpz_get_d(e));
}

/* structure to represent an arb real ball with mpz */
typedef struct {
    
    mpz_struct _cm; /* mantissa of the center */
    mpz_struct _ce; /* exponent of the center */
    mpz_struct _rm; /* mantissa of the radius */
    mpz_struct _re; /* exponent of the radius */
    
} mpz_rb_struct;

typedef mpz_rb_struct mpz_rb_t[1];
typedef mpz_rb_struct * mpz_rb_ptr;
typedef const mpz_rb_struct * mpz_rb_srcptr;

#define mpz_rb_cmref(X) ( &(X)->_cm )
#define mpz_rb_ceref(X) ( &(X)->_ce )
#define mpz_rb_rmref(X) ( &(X)->_rm )
#define mpz_rb_reref(X) ( &(X)->_re )

EXT_DECL void mpz_rb_init( mpz_rb_t x );
EXT_DECL void mpz_rb_clear( mpz_rb_t x );

/*******************************************************************************************/
/* pre  conditions: y is initialized          ,                                            */
/*                  the center of x is neither nan nor has an infinite value               */
/* post conditions: sets y._cm and y._ce so that y._cm*2^y._ce = center of x               */
/*                  and y._cm is either odd or 0, in which case y._ce=0 and center of x =0 */
/*                  sets y._rm and y._re so that y._rm*2^y._re = radius of x               */
/*                  and y._rm is either odd or 0, in which case y._re=0 and radius of x =0 */
/*******************************************************************************************/
void mpz_rb_set_arb ( mpz_rb_t y, const arb_t x );

/* this is used only for development */
void mpz_rb_get_arb ( arb_t y, const mpz_rb_t x  );

/* structure to represent an acb complex ball with mpz */
typedef struct {
    
    mpz_rb_struct _real; /* real part */
    mpz_rb_struct _imag; /* imaginary part */
    
} mpz_cb_struct;

typedef mpz_cb_struct mpz_cb_t[1];
typedef mpz_cb_struct * mpz_cb_ptr;
typedef const mpz_cb_struct * mpz_cb_srcptr;

#define mpz_cb_realref(X) ( &(X)->_real )
#define mpz_cb_imagref(X) ( &(X)->_imag )

#define mpz_cb_rcmref(X) ( &(&(X)->_real)->_cm )
#define mpz_cb_rceref(X) ( &(&(X)->_real)->_ce )
#define mpz_cb_rrmref(X) ( &(&(X)->_real)->_rm )
#define mpz_cb_rreref(X) ( &(&(X)->_real)->_re )
#define mpz_cb_icmref(X) ( &(&(X)->_imag)->_cm )
#define mpz_cb_iceref(X) ( &(&(X)->_imag)->_ce )
#define mpz_cb_irmref(X) ( &(&(X)->_imag)->_rm )
#define mpz_cb_ireref(X) ( &(&(X)->_imag)->_re )

PWPOLY_INLINE void mpz_cb_init( mpz_cb_t x ) { 
    mpz_rb_init( mpz_cb_realref(x) );
    mpz_rb_init( mpz_cb_imagref(x) );
}
PWPOLY_INLINE void mpz_cb_clear( mpz_cb_t x ){
    mpz_rb_clear( mpz_cb_realref(x) );
    mpz_rb_clear( mpz_cb_imagref(x) );
}

PWPOLY_INLINE void mpz_cb_set_acb ( mpz_cb_t y, const acb_t x ){
    mpz_rb_set_arb ( mpz_cb_realref(y), acb_realref(x) );
    mpz_rb_set_arb ( mpz_cb_imagref(y), acb_imagref(x) );
}

/* this is used only for development */
PWPOLY_INLINE void mpz_cb_get_acb ( acb_t y, const mpz_cb_t x  ){
    mpz_rb_get_arb ( acb_realref(y), mpz_cb_realref(x)  );
    mpz_rb_get_arb ( acb_imagref(y), mpz_cb_imagref(x)  );
}

// PWPOLY_INLINE void mpz_cb_set_root ( mpz_cb_t y, const root_t x ){
//     mpz_rb_set_arb ( mpz_cb_realref(y), acb_realref(root_enclosureref(x)) );
//     mpz_rb_set_arb ( mpz_cb_imagref(y), acb_imagref(root_enclosureref(x)) );
// }

PWPOLY_INLINE void mpz_cb_iReal_nReals_set_acb ( mpz_cb_t y, slong * iReal, slong * nReals, const acb_t x ){
    mpz_rb_set_arb ( mpz_cb_realref(y), acb_realref(x) );
    mpz_rb_set_arb ( mpz_cb_imagref(y), acb_imagref(x) );
    (*iReal) = arb_is_zero( acb_imagref( x ) );
    (*nReals) += (*iReal);
}
// 
// PWPOLY_INLINE void mpz_cb_sMult_set_root ( mpz_cb_t y, slong * sumOfMult, const root_t x ){
//     mpz_rb_set_arb ( mpz_cb_realref(y), acb_realref(root_enclosureref(x)) );
//     mpz_rb_set_arb ( mpz_cb_imagref(y), acb_imagref(root_enclosureref(x)) );
//     *sumOfMult = root_sumOfMultref(x);
// }
// 
// PWPOLY_INLINE void mpz_cb_sMult_iReal_nReals_set_root ( mpz_cb_t y, slong * sumOfMult, slong * iReal, slong * nReals, const root_t x ){
//     mpz_rb_set_arb ( mpz_cb_realref(y), acb_realref(root_enclosureref(x)) );
//     mpz_rb_set_arb ( mpz_cb_imagref(y), acb_imagref(root_enclosureref(x)) );
//     *sumOfMult = root_sumOfMultref(x);
//     *iReal     =root_isrealref(x);
//     *nReals += root_isrealref(x);
// }
#ifndef PW_NO_INTERFACE
void mpz_cb_fprint(FILE * file, const mpz_cb_t b);
PWPOLY_INLINE void mpz_cb_print(const mpz_cb_t b) { mpz_cb_fprint(stdout, b); }

void mpz_cb_gnuplot(FILE *file, const mpz_cb_t);

void mpz_cb_vec_gnuplot(FILE *fpreamb, FILE *fdata, mpz_cb_srcptr l, const slong len);
#endif

#ifdef __cplusplus
}
#endif

#endif
