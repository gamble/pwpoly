/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "mpz_cb.h"

void mpz_rb_init( mpz_rb_t x ){
    mpz_init(mpz_rb_cmref(x));
    mpz_init(mpz_rb_ceref(x));
    mpz_init(mpz_rb_rmref(x));
    mpz_init(mpz_rb_reref(x));
}

void mpz_rb_clear( mpz_rb_t x ){
    mpz_clear(mpz_rb_cmref(x));
    mpz_clear(mpz_rb_ceref(x));
    mpz_clear(mpz_rb_rmref(x));
    mpz_clear(mpz_rb_reref(x));
}

void mpz_rb_set_arb ( mpz_rb_t y, const arb_t x ){
    fmpz_t fm, fe;
    fmpz_init(fm);
    fmpz_init(fe);
    arf_get_fmpz_2exp(fm, fe, arb_midref(x) );
    fmpz_get_mpz(mpz_rb_cmref(y),fm);
    fmpz_get_mpz(mpz_rb_ceref(y),fe);
    arf_t rad;
    arf_init(rad);
    arf_set_mag(rad, arb_radref(x));
    arf_get_fmpz_2exp(fm, fe, rad );
    fmpz_get_mpz(mpz_rb_rmref(y),fm);
    fmpz_get_mpz(mpz_rb_reref(y),fe);
    arf_clear(rad);
    fmpz_clear(fm);
    fmpz_clear(fe);
}

void mpz_rb_get_arb ( arb_t y, const mpz_rb_t x  ){
    fmpz_t fm, fe;
    fmpz_init(fm);
    fmpz_init(fe);
    arf_t rad;
    arf_init(rad);
    
    arb_zero(y);
    
    fmpz_set_mpz(fm, mpz_rb_cmref(x));
    fmpz_set_mpz(fe, mpz_rb_ceref(x));
    arf_set_fmpz_2exp(arb_midref(y), fm, fe);
    
    fmpz_set_mpz(fm, mpz_rb_rmref(x));
    fmpz_set_mpz(fe, mpz_rb_reref(x));
    arf_set_fmpz_2exp(rad, fm, fe);
    arb_add_error_arf(y, rad);
    
    arf_clear(rad);
    fmpz_clear(fm);
    fmpz_clear(fe);
}

#ifndef PW_NO_INTERFACE
void mpz_cb_fprint(FILE * file, const mpz_cb_t b){
#ifndef PW_SILENT
    fprintf(file, "[( ");
    mpz_out_str( file, 10, mpz_cb_rcmref(b) ); fprintf(file, "x2^"); mpz_out_str( file, 10, mpz_cb_rceref(b) );
    fprintf(file, " + I ");
    mpz_out_str( file, 10, mpz_cb_icmref(b) ); fprintf(file, "x2^"); mpz_out_str( file, 10, mpz_cb_iceref(b) );
    fprintf(file, " ) +/- ( ");
    mpz_out_str( file, 10, mpz_cb_rrmref(b) ); fprintf(file, "x2^"); mpz_out_str( file, 10, mpz_cb_rreref(b) );
    fprintf(file, " + I ");
    mpz_out_str( file, 10, mpz_cb_irmref(b) ); fprintf(file, "x2^"); mpz_out_str( file, 10, mpz_cb_ireref(b) );
    fprintf(file, " )]");
#endif
}

void mpz_cb_gnuplot(FILE *file, const mpz_cb_t b){
    fprintf(file, "%lf", mpz_2exp_mpz_get_d ( mpz_cb_rcmref(b), mpz_cb_rceref(b) ));
    fprintf(file, "   ");
    fprintf(file, "%lf", mpz_2exp_mpz_get_d ( mpz_cb_icmref(b), mpz_cb_iceref(b) ));
    fprintf(file, "   ");
    fprintf(file, "%lf", mpz_2exp_mpz_get_d ( mpz_cb_rrmref(b), mpz_cb_rreref(b) ));
    fprintf(file, "   ");
    fprintf(file, "%lf", mpz_2exp_mpz_get_d ( mpz_cb_irmref(b), mpz_cb_ireref(b) ));
}

void mpz_cb_vec_gnuplot(FILE *fpreamb, FILE *fdata, mpz_cb_srcptr l, const slong len){
    fprintf(fpreamb, "plot '-' title 'approximated roots' with xyerrorbars lc rgb \"#008080\" \n");
    for(int i=0; i < len; i++) {
        mpz_cb_gnuplot(fdata, l + i);
        fprintf(fdata, "\n");
    }
    fprintf(fdata, "e\n");
    fprintf(fdata, "pause mouse close\n");
}
#endif
