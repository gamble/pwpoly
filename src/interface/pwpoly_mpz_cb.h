/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PWROOTS_MPZ_CB_H
#define PWROOTS_MPZ_CB_H

#include "pw_base.h"
#include "solving/pw_solving.h"
#include "flint/fmpz.h"
#include "flint/fmpq_poly.h"
#include "mpz_cb.h"
#include <gmp.h>

#include "windows.h"
#ifndef EXT_DECL
#define EXT_DECL
#endif

#ifdef __cplusplus
extern "C" {
#endif
    

/**************************************************************************************************************/
/* pre  conditions: mpz_cb_roots and roots are tables of at least len initialized mpz_cb_t and acb_t          */
/*                  for i=0,...,len-1, the cent. and rad. of roots[i] are neither nan nor +/-inf              */
/* post conditions: for i=0,...,len-1,                                                                        */
/*                      mpz_cb_roots + i = roots + i                                                          */
/*                      and of centers and radii of mpz_cb_roots + i                                          */
/*                      are either odd integers or 0, in which case corresponding exp. is 0 and roots + i = 0 */
/**************************************************************************************************************/
PWPOLY_INLINE void mpz_cb_vec_set_acb_vec ( mpz_cb_ptr mpz_cb_roots, acb_ptr roots, slong len ) {
    slong i;
    for (i = 0; i < len; i++)
        mpz_cb_set_acb ( mpz_cb_roots + i, roots + i );
}

/**************************************************************************************************************/
/* pre  conditions: mpz_cb_roots and roots are tables of at least len initialized mpz_cb_t and acb_t          */
/*                  reals is a table of at least len slongs                                                   */
/*                  nbreals is a reference to an integer                                                      */
/*                  for i=0,...,len-1, the cent. and rad. of roots[i] are neither nan nor +/-inf              */
/* post conditions: for i=0,...,len-1,                                                                        */
/*                      mpz_cb_roots + i = roots + i                                                          */
/*                      and of centers and radii of mpz_cb_roots + i                                          */
/*                      are either odd integers or 0, in which case corresponding exp. is 0 and roots + i = 0 */
/*                      reals[i] = 1 <=> the ind-th root of roots is certified to be real                     */
/*                   nbreals = nb of real roots                                                               */
/**************************************************************************************************************/
PWPOLY_INLINE void mpz_cb_reals_nbreals_vec_set_acb_vec ( mpz_cb_ptr mpz_cb_roots, slong * reals, slong * nbreals, acb_ptr roots, slong len ) {
    slong i;
    for (i = 0; i < len; i++)
        mpz_cb_iReal_nReals_set_acb ( mpz_cb_roots + i, reals + i, nbreals, roots + i );
}

/**************************************************************************************************************/
/* pre  conditions: mpz_cb_roots, roots and sumOfMults are tables of at least len initialized mpz_cb_t, root_t and slong */
/*                  for i=0,...,len-1, the cent. and rad. of roots[i] are neither nan nor +/-inf              */
/* post conditions: for i=0,...,len-1,                                                                        */
/*                      mpz_cb_roots + i = roots + i                                                          */
/*                      sumOfMults + i = root_sumOfMultref(roots + i)                                         */
/*                      and of centers and radii of mpz_cb_roots + i                                          */
/*                      are either odd integers or 0, in which case corresponding exp. is 0 and roots + i = 0 */
/**************************************************************************************************************/
// PWPOLY_INLINE void mpz_cb_sMult_vec_set_root_vec ( mpz_cb_ptr mpz_cb_roots, slong * sumOfMults, 
//                                                       root_ptr roots, signed long int len ) {
//     signed long int i;
//     for (i = 0; i < len; i++)
//         mpz_cb_sMult_set_root ( mpz_cb_roots + i, sumOfMults + i, roots + i );
// }

/**************************************************************************************************************/
/* pre  conditions: mpz_cb_roots, roots and sumOfMults are tables of at least len initialized mpz_cb_t, root_t and slong */
/*                  reals is a table of at least len slongs                                                   */
/*                  for i=0,...,len-1, the cent. and rad. of roots[i] are neither nan nor +/-inf              */
/* post conditions: for i=0,...,len-1,                                                                        */
/*                      mpz_cb_roots + i = roots + i                                                          */
/*                      sumOfMults + i = root_sumOfMultref(roots + i)                                         */
/*                      and of centers and radii of mpz_cb_roots + i                                          */
/*                      are either odd integers or 0, in which case corresponding exp. is 0 and roots + i = 0 */
/*                      reals[i] = 1 <=> the ind-th root of roots is certified to be real                     */
/**************************************************************************************************************/
// PWPOLY_INLINE void mpz_cb_sMult_reals_nbReals_vec_set_root_vec ( mpz_cb_ptr mpz_cb_roots, slong * sumOfMults, slong * reals, slong * nbReals,
//                                                                   root_ptr roots, signed long int len ) {
//     signed long int i;
//     for (i = 0; i < len; i++)
//         mpz_cb_sMult_iReal_nReals_set_root ( mpz_cb_roots + i, sumOfMults + i, reals + i, nbReals, roots + i );
// }

/****************************************************************************************/
/* pre  conditions: length is > 1 and is 1 + the degree of the input polynomial         */
/*                  coeffs is a table containing at least degree initialized mpz_t      */
/*                  coeffs[length] \neq 0                                               */
/*                  dest is initialized                                                 */
/* post conditions: sets dest to the polynomial of degree length -1                     */ 
/*                            p(z) = \sum_{i=0}^{length-1} coeffs[i]*z^i                */
/****************************************************************************************/
void fmpq_poly_set_mpz_vec ( fmpq_poly_t dest, mpz_ptr coeffs, slong length );

/****************************************************************************************************************/
/* pre  conditions: length is > 1 and is 1 + the degree of the input polynomial                                 */
/*                  res is a table containing at least degree initialized mpz_cb_t                              */
/*                  reals is a table containing at least degree slongs                                          */
/*                  nbreals is the reference of an slong                                                        */
/*                  coeffs is a table containing at least degree initialized mpz_t                              */
/*                  coeffs[length] \neq 0                                                                       */
/*                  the polynomial p(z) = \sum_{i=0}^{length-1} coeffs[i]*z^i is square free                    */
/*                  epsilon is an integer                                                                       */
/*                  nbMroots is an integer                                                                      */
/*                  domain_num and domain_den are tables of 4 mpz, let domain = [lre, ure] + I*[lim, uim]       */
/*                     lre = -inf if domain_den[0] = 0, domain_num[0]/domain_den[0] otherwise                   */
/*                     ure = +inf if domain_den[1] = 1, domain_num[1]/domain_den[1] otherwise                   */
/*                     lim = -inf if domain_den[2] = 2, domain_num[2]/domain_den[2] otherwise                   */
/*                     ure = +inf if domain_den[3] = 3, domain_num[3]/domain_den[3] otherwise                   */
/* post conditions: let n be length-1 if nbMroots<0, min(length-1, nbMroots) otherwise                          */
/*                  for i=0,...,n-1, let B_i be the box res + i                                                 */
/*                  returns integer m>=0 and <= length-1 satisfying                                             */
/*                     the boxes B_i for i=0,...,m-1 are pairwise disjoints                                     */
/*                     for i=0,...,m-1, the box B_i contains a unique root of p defined above                   */
/*                     for i=0,...,m-1, the max halfwidths of the B_i's <= 2^{min(-epsilon, 0}                  */
/*                     for i=0,...,m-1, B_i has non empty intersection with the domain                          */
/*                     if m<n then for any root in the domain, there is a B_i containing it                     */
/*                     for i=0,...,m-1, reals[i]=1 <=> the root in B_i is real                                  */
/*                     nbreals = nb of real roots in B_0U...UB_{m-1}                                            */
/****************************************************************************************************************/

EXT_DECL slong pwroots_isolate_mpz_poly_mpz_cb ( mpz_cb_ptr res, slong * reals, slong *nbreals,
                                                 mpz_ptr coeffs, slong length,
                                                 slong log2eps, 
                                                 mpz_struct domain_num[], mpz_struct domain_den[],
                                                 slong nbMroots
#ifdef PW_CHECK_INTERRUPT
                                                 , int (*check_interrupt)()
#endif
                                                );

EXT_DECL slong pwroots_isolate_comp_mpz_poly_mpz_cb ( mpz_cb_ptr res, 
                                                      mpz_ptr coeffs_real, slong length_real,
                                                      mpz_ptr coeffs_imag, slong length_imag,
                                                      slong log2eps, 
                                                      mpz_struct domain_num[], mpz_struct domain_den[],
                                                      slong nbMroots
#ifdef PW_CHECK_INTERRUPT                             
                                                      , int (*check_interrupt)()
#endif
                                                    );

/****************************************************************************************************************/
/* pre  conditions: length is > 1 and is 1 + the degree of the input polynomial p                               */
/*                  res is a table containing at least degree initialized mpz_cb_t                              */
/*                  sumOfMults is a table of at least degree slong                                              */
/*                  reals is a table containing at least degree slongs                                          */
/*                  nbreals is the reference of an slong                                                        */
/*                  coeffs is a table containing at least degree initialized mpz_t                              */
/*                  coeffs[length] \neq 0                                                                       */
/*                  the polynomial p(z) = \sum_{i=0}^{length-1} coeffs[i]*z^i is not nec. square free           */
/*                  epsilon is an integer                                                                       */
/*                  domain_num and domain_den are tables of 4 mpz, let domain = [lre, ure] + I*[lim, uim]       */
/*                     lre = -inf if domain_den[0] = 0, domain_num[0]/domain_den[0] otherwise                   */
/*                     ure = +inf if domain_den[1] = 1, domain_num[1]/domain_den[1] otherwise                   */
/*                     lim = -inf if domain_den[2] = 2, domain_num[2]/domain_den[2] otherwise                   */
/*                     ure = +inf if domain_den[3] = 3, domain_num[3]/domain_den[3] otherwise                   */
/*                  nbMroots is an integer                                                                      */
/* post conditions: let n be length-1 if nbMroots<0, min(length-1, nbMroots) otherwise                          */
/*                  for i=0,...,n-1, let B_i be the box res + i                                                 */
/*                  returns integer m>=0 and <= length-1 satisfying                                             */
/*                     the boxes B_i for i=0,...,m-1 are pairwise disjoints                                     */
/*                     for i=0,...,m-1, the boxes B_i and 3B_i contain                                          */
/*                                                         sumOfMults + i roots of p counted with multiplicity  */
/*                     for i=0,...,m-1, the max halfwidths of the B_i's <= 2^{min(-epsilon, 0}                  */
/*                     for i=0,...,m-1, B_i has non empty intersection with the domain                          */
/*                     let l be the sum of the sumOfMults + i for i=0, ... m-1;                                 */
/*                     if l<n then for any root in the domain, there is a B_i containing it                     */
/*                     for i=0,...,n-1, reals[i]=1  => the root in B_i is real                                  */
/*                     nbreals <= nb of real roots in B_0U...UB_{n-1}                                           */
/****************************************************************************************************************/

EXT_DECL slong pwroots_approximate_mpz_poly_mpz_cb ( mpz_cb_ptr res, slong * sumOfMults, slong * reals,  slong *nbreals,
                                                     mpz_ptr coeffs, slong length,
                                                     slong log2eps, 
                                                     mpz_struct domain_num[], mpz_struct domain_den[],
                                                     slong nbMroots
#ifdef PW_CHECK_INTERRUPT
                                                     , int (*check_interrupt)()
#endif
                                                    );

EXT_DECL slong pwroots_approximate_comp_mpz_poly_mpz_cb ( mpz_cb_ptr res, slong * sumOfMults,
                                                          mpz_ptr coeffs_real, slong length_real,
                                                          mpz_ptr coeffs_imag, slong length_imag,
                                                          slong log2eps, 
                                                          mpz_struct domain_num[], mpz_struct domain_den[],
                                                          slong nbMroots
#ifdef PW_CHECK_INTERRUPT                                     
                                                          , int (*check_interrupt)()
#endif
                                                         );
/****************************************************************************************************************/
/* pre  conditions: length is > 1 and is 1 + the degree of the input polynomial p                               */
/*                  res is a table containing at least degree initialized mpz_cb_t                              */
/*                  sumOfMults is a table of at least degree slong                                              */
/*                  reals is a table containing at least degree slongs                                          */
/*                  nbreals is the reference of an slong                                                        */
/*                  coeffs is a table containing at least degree initialized mpz_t                              */
/*                  coeffs[length] \neq 0                                                                       */
/*                  the polynomial p(z) = \sum_{i=0}^{length-1} coeffs[i]*z^i is not nec. square free           */
/*                  epsilon is an integer                                                                       */
/*                  domain_num and domain_den are tables of 4 mpz, let domain = [lre, ure] + I*[lim, uim]       */
/*                     s.t. domain_num's are >0                                                                 */
/*                     lre = domain_num[0]/domain_den[0]                                                        */
/*                     ure = domain_num[1]/domain_den[1]                                                        */
/*                     lim = domain_num[2]/domain_den[2]                                                        */
/*                     ure = domain_num[3]/domain_den[3]                                                        */
/*                  nbMroots is an integer >0 and <= length-1                                                   */
/*                  ASSUME that there are no roots on the boundary of domain and nbMroots (count with mults)    */
/*                         in the domain                                                                        */
/* post conditions: for i=0,...,n-1, let B_i be the box res + i                                                 */
/*                  returns integer m>=0 and <= nbMroots satisfying                                             */
/*                     the boxes B_i for i=0,...,m-1 are pairwise disjoints                                     */
/*                     for i=0,...,m-1, the boxes B_i and 3B_i contain                                          */
/*                                                         sumOfMults + i roots of p counted with multiplicity  */
/*                     for i=0,...,m-1, the max halfwidths of the B_i's <= 2^{min(-epsilon, 0}                  */
/*                     for i=0,...,m-1, B_i is strictly in the domain                                           */
/*                     let l be the sum of the sumOfMults + i for i=0, ... m-1; then l=nbMroots                 */
/*                     for i=0,...,n-1, reals[i]=1  => the root in B_i is real                                  */
/*                     nbreals <= nb of real roots in B_0U...UB_{n-1}                                           */
/****************************************************************************************************************/
EXT_DECL slong pwroots_refine_mpz_poly_mpz_cb ( mpz_cb_ptr res, slong * sumOfMults, slong * reals,  slong *nbreals,
                                                 mpz_ptr coeffs, slong length,
                                                 slong epsilon, 
                                                 mpz_struct domain_num[], mpz_struct domain_den[],
                                                 slong nbMroots
#ifdef PW_CHECK_INTERRUPT
                                                 , int (*check_interrupt)()
#endif
                                               );

EXT_DECL slong pwroots_refine_comp_mpz_poly_mpz_cb ( mpz_cb_ptr res, slong * sumOfMults,
                                                      mpz_ptr coeffs_real, slong length_real,
                                                      mpz_ptr coeffs_imag, slong length_imag,
                                                      slong epsilon, 
                                                      mpz_struct domain_num[], mpz_struct domain_den[],
                                                      slong nbMroots
#ifdef PW_CHECK_INTERRUPT                                
                                                      , int (*check_interrupt)()
#endif
                                                     );
#ifdef __cplusplus
}
#endif

#endif

/* DEPRECATED */

// /*******************************************************************************************/
// /* pre  conditions: m, e and x are initialized,                                            */
// /*                  the center of x is neither nan nor has an infinite value               */
// /* post conditions: sets m and e so that m*2^e = center of x                               */
// /*                  and m is either odd or 0, in which case e=0 and center of x =0         */
// /*******************************************************************************************/
// void hefroots_maple_arb_get_mid_mpz_2exp_mpz ( mpz_t m, mpz_t e, const arb_t x );
// /*******************************************************************************************/
// /* pre  conditions: m, e and x are initialized,                                            */
// /*                  the radius of x is neither nan nor has an infinite value               */
// /* post conditions: sets m and e so that m*2^e = radius of x                               */
// /*                  and m is either odd or 0, in which case e=0 and radius of x =0         */
// /*******************************************************************************************/
// void hefroots_maple_arb_get_rad_mpz_2exp_mpz ( mpz_t m, mpz_t e, const arb_t x );
// 
// /**************************************************************************************************/
// /* pre  conditions: mr, er, mi, ei and x are initialized,                                         */
// /*                  the center of x is neither nan nor has an infinite value                      */
// /* post conditions: sets mr, er, mi, ei so that mr*2^er + sqrt(-1)*(mi*2^ei) = center of x        */
// /*                  and mr is either odd or 0, in which case er=0 and real part of center of x =0 */
// /*                  and mi is either odd or 0, in which case ei=0 and imag part of center of x =0 */
// /**************************************************************************************************/
// PWPOLY_INLINE void hefroots_maple_acb_get_mid_mpz_2exp_mpz ( mpz_t mr, mpz_t er, mpz_t mi, mpz_t ei, const acb_t x ) {
//     hefroots_maple_arb_get_mid_mpz_2exp_mpz ( mr, er, acb_realref(x) );
//     hefroots_maple_arb_get_mid_mpz_2exp_mpz ( mi, ei, acb_imagref(x) );
// }
// /**************************************************************************************************/
// /* pre  conditions: mr, er, mi, ei and x are initialized,                                         */
// /*                  the radius of x is neither nan nor has an infinite value                      */
// /* post conditions: sets mr, er, mi, ei so that mr*2^er + sqrt(-1)*(mi*2^ei) = radius of x        */
// /*                  and mr is either odd or 0, in which case er=0 and real part of radius of x =0 */
// /*                  and mi is either odd or 0, in which case ei=0 and imag part of radius of x =0 */
// /**************************************************************************************************/
// PWPOLY_INLINE void hefroots_maple_acb_get_rad_mpz_2exp_mpz ( mpz_t mr, mpz_t er, mpz_t mi, mpz_t ei, const acb_t x ) {
//     hefroots_maple_arb_get_rad_mpz_2exp_mpz ( mr, er, acb_realref(x) );
//     hefroots_maple_arb_get_rad_mpz_2exp_mpz ( mi, ei, acb_imagref(x) );
// }
// /**************************************************************************************************/
// /* pre  conditions: cmr, cer, cmi, cei, rmr, rer, rmi, rei and x are initialized,                 */
// /*                  the center and radii of x are neither nan nor have an infinite value          */
// /* post conditions: sets cmr, cer, cmi, cei so that cmr*2^cer + sqrt(-1)*(cmi*2^cei) = center of x*/
// /*                  sets rmr, rer, rmi, rei so that rmr*2^rer + sqrt(-1)*(rmi*2^rei) = radius of x*/
// /*                  and rmr is either odd or 0, in which case rer=0 and real part of rad of x =0  */
// /*                  and rmi is either odd or 0, in which case rei=0 and imag part of rad of x =0  */
// /*                  and cmr is either odd or 0, in which case cer=0 and real part of cent of x =0 */
// /*                  and cmi is either odd or 0, in which case cei=0 and imag part of cent of x =0 */
// /**************************************************************************************************/
// PWPOLY_INLINE void hefroots_maple_acb_get_mpz_2exp_mpz ( mpz_t cmr, mpz_t cer, mpz_t cmi, mpz_t cei,
//                                                             mpz_t rmr, mpz_t rer, mpz_t rmi, mpz_t rei, const acb_t x ) {
//     hefroots_maple_acb_get_mid_mpz_2exp_mpz ( cmr, cer, cmi, cei, x );
//     hefroots_maple_acb_get_rad_mpz_2exp_mpz ( rmr, rer, rmi, rei, x );
// }
// 
// /***********************************************************************************************************/
// /* pre  conditions: cmr, cer, cmi, cei, rmr, rer, rmi, rei and r are                                       */
// /*                  tables of at least len initialized mpz_t (resp. acb_t)                                 */
// /*                  for i=0,...,len-1, the cent. and rad. of r[i] are neither nan nor +/-inf               */
// /* post conditions: for i=0,...,len-1, sets cells so that                                                  */ 
// /*                            cmr[i]*2^cer[i] + sqrt(-1)*(cmi[i]*2^cei[i]) = center of r[i]                */
// /*                            rmr[i]*2^rer[i] + sqrt(-1)*(rmi[i]*2^rei[i]) = radius of r[i]                */
// /*                  and cmr[i] is either odd or 0, in which case cer[i]=0 and real part of cent of r[i] =0 */
// /*                  and cmi[i] is either odd or 0, in which case cei[i]=0 and imag part of cent of r[i] =0 */
// /*                  and rmr[i] is either odd or 0, in which case rer[i]=0 and real part of rad of  r[i] =0 */
// /*                  and rmi[i] is either odd or 0, in which case rei[i]=0 and imag part of rad of  r[i] =0 */
// /***********************************************************************************************************/
// PWPOLY_INLINE void hefroots_maple_acb_vec_get_mpz_2exp_mpz ( mpz_ptr cmr, mpz_ptr cer, mpz_ptr cmi, mpz_ptr cei,
//                                                                 mpz_ptr rmr, mpz_ptr rer, mpz_ptr rmi, mpz_ptr rei,
//                                                                 acb_ptr r, signed long int len ) {
//     signed long int i;
//     for (i = 0; i < len; i++) {
//         hefroots_maple_acb_get_mpz_2exp_mpz( cmr + i, cer + i, cmi + i, cei + i, 
//                                              rmr + i, rer + i, rmi + i, rei + i,
//                                              r + i );
//     }
// }
/****************************************************************************************************************/
/* pre  conditions: length is > 1 and is 1 + the degree of the input polynomial                                 */
/*                  rr_man, rr_exp, ri_man, ri_exp are tables containing each at least degree initialized mpz_t */
/*                  radii is a table containing at least degree double                                          */
/*                  coeffs is a table containing at least degree initialized mpz_t                              */
/*                  coeffs[length] \neq 0                                                                       */
/*                  the polynomial p(z) = \sum_{i=0}^{length-1} coeffs[i]*z^i is square free                    */
/* post conditions: for i=0,...,length-1, let br[i] = crr_man[i]*2^{crr_exp[i]} +/- rrr_man[i]*2^{rrr_exp[i]}   */
/*                                        abd bi[i] = cri_man[i]*2^{cri_exp[i]} +/- rri_man[i]*2^{rri_exp[i]}   */
/*                  for i=0,...,length-1, let B_i be the box br[i] + \sqrt{-1}bi[i]                             */
/*                  the boxes B_i for i=0,...,length-1 are pairwise disjoints                                   */
/*                  for i=0,...,length-1, the box B_i contains a unique root of p defined above                 */
/*                  for i=0,...,length-1, the max rad of B_i <= 2^{-epsilon}                                    */
/****************************************************************************************************************/
// void hefroots_isolate_maple_mpz_poly ( mpz_ptr crr_man, mpz_ptr crr_exp, mpz_ptr cri_man, mpz_ptr cri_exp,
//                                        mpz_ptr rrr_man, mpz_ptr rrr_exp, mpz_ptr rri_man, mpz_ptr rri_exp,
//                                        mpz_ptr coeffs, signed long int length,
//                                        signed long int epsilon
// //                                        void *(*al) (size_t),
// //                                        void *(*real) (void *, size_t, size_t),
// //                                        void (*fr) (void *, size_t)
//                                      );
