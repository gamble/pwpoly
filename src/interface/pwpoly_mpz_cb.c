/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pwpoly_mpz_cb.h"
#include "geometry/domain.h"

void fmpq_poly_set_mpz_vec ( fmpq_poly_t dest, mpz_ptr coeffs, slong length ) {
    fmpq_poly_fit_length(dest, length);
    _fmpq_poly_set_length(dest, length);
    fmpz_one(fmpq_poly_denref(dest));
    for (slong i = 0; i < length; i++)
        fmpz_set_mpz( fmpq_poly_numref(dest) + i, coeffs + i );
}

slong pwroots_isolate_mpz_poly_mpz_cb ( mpz_cb_ptr mpz_cb_roots, slong * reals, slong *nbreals, 
                                         mpz_ptr coeffs, slong length,
                                         slong log2eps, 
                                         mpz_struct domain_num[], mpz_struct domain_den[], 
                                         slong nbMroots
#ifdef PW_CHECK_INTERRUPT
                                        , int (*check_interrupt)()
#endif
                                       ){
    /* input polynomial */
    fmpq_poly_t pR_exact, pI_exact;
    fmpq_poly_init(pR_exact);
    fmpq_poly_init(pI_exact);
    fmpq_poly_set_mpz_vec ( pR_exact, coeffs, length );
    fmpq_poly_zero(pI_exact);
    slong p_degree = fmpq_poly_degree(pR_exact);
    /* output isolating boxes and multiplicities */
    acb_ptr roots = _acb_vec_init(p_degree);
    slong * mults = (slong *) pwpoly_malloc ( (p_degree)*sizeof(slong) );
    /* solving options */
    ulong m=10;
    int goal=PW_GOAL_ISO_AND_APP;
    int solver=63;
//     int solver=31;
    nbMroots = PW_CAP_GOALNBROOTS(nbMroots,p_degree);
    domain_t dom;
    domain_init(dom);
    domain_set_mpz_tab( dom, domain_num, domain_den );
    ulong c = 2;
    fmpq_t scale, b;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
    
#ifdef PW_CHECK_INTERRUPT
    __pwpoly_set_interrupt_function( check_interrupt );
#endif
   
//     printf("&&&&&&&&&&&&&&&&&&&&&&&& call pwroots for degree %ld: &&&&&&&&&&&&&&&&&&&&&&&&&&\n", p_degree);
    
    slong res = pwpoly_solve_2fmpq_poly( roots, mults, &m, 
                                         pR_exact, pI_exact,
                                         goal, log2eps, dom, nbMroots,
                                         c, scale, b, solver PW_VERBOSE_CALL(0)
#ifndef PW_NO_INTERFACE                                         
                                         , NULL
#endif                                        
                                        );
    
//     printf("&&&&&&&&&&&&&&&&&&return from pwroots: nbroots %ld &&&&&&&&&&&&&&&&&&&&&&\n", res);
    
    
    *nbreals = 0;
    slong resNbRoots = PWPOLY_MIN( res, nbMroots);
    
#ifdef PW_CHECK_INTERRUPT
    if ( !pwpoly_interrupt(0) )
#endif    
    mpz_cb_reals_nbreals_vec_set_acb_vec ( mpz_cb_roots, reals, nbreals, roots, resNbRoots );

    
    fmpq_clear(b);
    fmpq_clear(scale);
    domain_clear(dom);
    pwpoly_free(mults);
    _acb_vec_clear(roots, p_degree);
    fmpq_poly_clear(pR_exact);
    fmpq_poly_clear(pI_exact);
    return resNbRoots;
}

slong pwroots_isolate_comp_mpz_poly_mpz_cb ( mpz_cb_ptr mpz_cb_roots, 
                                             mpz_ptr coeffs_real, slong length_real,
                                             mpz_ptr coeffs_imag, slong length_imag,
                                             slong log2eps, 
                                             mpz_struct domain_num[], mpz_struct domain_den[], 
                                             slong nbMroots
#ifdef PW_CHECK_INTERRUPT
                                             , int (*check_interrupt)()
#endif
                                            ){
    /* input polynomial */
    fmpq_poly_t pR_exact, pI_exact;
    fmpq_poly_init(pR_exact);
    fmpq_poly_init(pI_exact);
    fmpq_poly_set_mpz_vec ( pR_exact, coeffs_real, length_real );
    fmpq_poly_set_mpz_vec ( pI_exact, coeffs_imag, length_imag );
    slong p_degree = PWPOLY_MAX( fmpq_poly_degree(pR_exact), fmpq_poly_degree(pI_exact) );
    /* output isolating boxes and multiplicities */
    acb_ptr roots = _acb_vec_init(p_degree);
    slong * mults = (slong *) pwpoly_malloc ( (p_degree)*sizeof(slong) );
    /* solving options */
    ulong m=10;
    int goal=PW_GOAL_ISO_AND_APP;
    int solver=63;
    nbMroots = PW_CAP_GOALNBROOTS(nbMroots,p_degree);
    domain_t dom;
    domain_init(dom);
    domain_set_mpz_tab( dom, domain_num, domain_den );
    ulong c = 2;
    fmpq_t scale, b;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
    
#ifdef PW_CHECK_INTERRUPT
    __pwpoly_set_interrupt_function( check_interrupt );
#endif
    
//     printf("&&&&&&&&&&&&&&&&&&&&&&&& call pwroots for degree %ld: &&&&&&&&&&&&&&&&&&&&&&&&&&\n", p_degree);

    slong res = pwpoly_solve_2fmpq_poly( roots, mults, &m, 
                                         pR_exact, pI_exact,
                                         goal, log2eps, dom, nbMroots,
                                         c, scale, b, solver PW_VERBOSE_CALL(0)
#ifndef PW_NO_INTERFACE                                         
                                         , NULL
#endif                                        
                                        );
    
//     printf("&&&&&&&&&&&&&&&&&&return from pwroots: nbroots %ld &&&&&&&&&&&&&&&&&&&&&&\n", res);
    
    slong resNbRoots = PWPOLY_MIN( res, nbMroots);
    
#ifdef PW_CHECK_INTERRUPT
    if ( !pwpoly_interrupt(0) )
#endif    
    mpz_cb_vec_set_acb_vec ( mpz_cb_roots, roots, resNbRoots );
    
    fmpq_clear(b);
    fmpq_clear(scale);
    domain_clear(dom);
    pwpoly_free(mults);
    _acb_vec_clear(roots, p_degree);
    fmpq_poly_clear(pR_exact);
    fmpq_poly_clear(pI_exact);
    return resNbRoots;
}

slong pwroots_approximate_mpz_poly_mpz_cb ( mpz_cb_ptr mpz_cb_roots, slong * sumOfMults, slong * reals, slong * nbreals,
                                            mpz_ptr coeffs, slong length,
                                            slong log2eps, 
                                            mpz_struct domain_num[], mpz_struct domain_den[], 
                                            slong nbMroots
#ifdef PW_CHECK_INTERRUPT
                                           , int (*check_interrupt)()
#endif
                                           ){
    
    /* input polynomial */
    fmpq_poly_t pR_exact, pI_exact;
    fmpq_poly_init(pR_exact);
    fmpq_poly_init(pI_exact);
    fmpq_poly_set_mpz_vec ( pR_exact, coeffs, length );
    fmpq_poly_zero(pI_exact);
    slong p_degree = fmpq_poly_degree(pR_exact);
    /* output isolating boxes */
    acb_ptr roots = _acb_vec_init(p_degree);
    slong * mults = (slong *) pwpoly_malloc ( p_degree * sizeof(slong) );
    /* solving options */
    ulong m=10;
    int goal=PW_GOAL_APPROXIMATE;
    int solver=63;
    nbMroots = PW_CAP_GOALNBROOTS(nbMroots,p_degree);
    domain_t dom;
    domain_init(dom);
    domain_set_mpz_tab( dom, domain_num, domain_den );
    ulong c = 2;
    fmpq_t scale, b;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
    
#ifdef PW_CHECK_INTERRUPT
    __pwpoly_set_interrupt_function( check_interrupt );
#endif    
    
//     printf("&&&&&&&&&&&&&&&&&&&&&&&& call pwroots for degree %ld: &&&&&&&&&&&&&&&&&&&&&&&&&&\n", p_degree);
    
    slong resNbClust = pwpoly_solve_2fmpq_poly( roots, mults, &m, 
                                                pR_exact, pI_exact,
                                                goal, log2eps, dom, nbMroots,
                                                c, scale, b, solver PW_VERBOSE_CALL(0)
#ifndef PW_NO_INTERFACE                                         
                                               , NULL
#endif                                        
                                              );
    
//     printf("&&&&&&&&&&&&&&&&&&return from pwroots: nb clusts %ld &&&&&&&&&&&&&&&&&&&&&&\n", resNbClust);
    
    resNbClust = PWPOLY_MIN( resNbClust, nbMroots);
    *nbreals=0;
    
#ifdef PW_CHECK_INTERRUPT
    if ( !pwpoly_interrupt(0) )
#endif   
    mpz_cb_reals_nbreals_vec_set_acb_vec ( mpz_cb_roots, reals, nbreals, roots, resNbClust );
    for (slong i=0; i<resNbClust; i++)
        sumOfMults[i] = mults[i];
    
    fmpq_clear(b);
    fmpq_clear(scale);
    domain_clear(dom);
    pwpoly_free(mults);
    _acb_vec_clear(roots, p_degree);
    fmpq_poly_clear(pR_exact);
    fmpq_poly_clear(pI_exact); 
    return resNbClust;
}

slong pwroots_approximate_comp_mpz_poly_mpz_cb ( mpz_cb_ptr mpz_cb_roots, slong * sumOfMults,
                                                 mpz_ptr coeffs_real, slong length_real,
                                                 mpz_ptr coeffs_imag, slong length_imag,
                                                 slong log2eps, 
                                                 mpz_struct domain_num[], mpz_struct domain_den[], 
                                                 slong nbMroots
#ifdef PW_CHECK_INTERRUPT
                                                 , int (*check_interrupt)()
#endif
                                                ){
    /* input polynomial */
    fmpq_poly_t pR_exact, pI_exact;
    fmpq_poly_init(pR_exact);
    fmpq_poly_init(pI_exact);
    fmpq_poly_set_mpz_vec ( pR_exact, coeffs_real, length_real );
    fmpq_poly_set_mpz_vec ( pI_exact, coeffs_imag, length_imag );
    slong p_degree = PWPOLY_MAX( fmpq_poly_degree(pR_exact), fmpq_poly_degree(pI_exact) );
    /* output isolating boxes */
    acb_ptr roots = _acb_vec_init(p_degree);
    slong * mults = (slong *) pwpoly_malloc ( p_degree * sizeof(slong) );
    /* solving options */
    ulong m=10;
    int goal=PW_GOAL_APPROXIMATE;
    int solver=63;
    nbMroots = PW_CAP_GOALNBROOTS(nbMroots,p_degree);
    domain_t dom;
    domain_init(dom);
    domain_set_mpz_tab( dom, domain_num, domain_den );
    ulong c = 2;
    fmpq_t scale, b;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
    
#ifdef PW_CHECK_INTERRUPT
    __pwpoly_set_interrupt_function( check_interrupt );
#endif
    
//     printf("&&&&&&&&&&&&&&&&&&&&&&&& call pwroots for degree %ld: &&&&&&&&&&&&&&&&&&&&&&&&&&\n", p_degree);
    
    slong resNbClust = pwpoly_solve_2fmpq_poly( roots, mults, &m, 
                                                pR_exact, pI_exact,
                                                goal, log2eps, dom, nbMroots,
                                                c, scale, b, solver PW_VERBOSE_CALL(0)
#ifndef PW_NO_INTERFACE                                         
                                               , NULL
#endif                                        
                                              );
    
//     printf("&&&&&&&&&&&&&&&&&&return from pwroots: nb clusts %ld &&&&&&&&&&&&&&&&&&&&&&\n", resNbClust);
    
    resNbClust = PWPOLY_MIN( resNbClust, nbMroots);
    
#ifdef PW_CHECK_INTERRUPT
    if ( !pwpoly_interrupt(0) )
#endif    
    mpz_cb_vec_set_acb_vec ( mpz_cb_roots, roots, resNbClust );
    for (slong i=0; i<resNbClust; i++)
        sumOfMults[i] = mults[i];
    
    fmpq_clear(b);
    fmpq_clear(scale);
    domain_clear(dom);
    pwpoly_free(mults);
    _acb_vec_clear(roots, p_degree);
    fmpq_poly_clear(pR_exact);
    fmpq_poly_clear(pI_exact);
    return resNbClust;
    
}

slong pwroots_refine_mpz_poly_mpz_cb ( mpz_cb_ptr mpz_cb_roots, slong * sumOfMults, slong * reals, slong * nbreals,
                                        mpz_ptr coeffs, slong length,
                                        slong epsilon, 
                                        mpz_struct domain_num[], mpz_struct domain_den[], 
                                        slong nbMroots
#ifdef PW_CHECK_INTERRUPT
                                        , int (*check_interrupt)()
#endif
                                        ){
    
    /* input polynomial */
    fmpq_poly_t pR_exact, pI_exact;
    fmpq_poly_init(pR_exact);
    fmpq_poly_init(pI_exact);
    fmpq_poly_set_mpz_vec ( pR_exact, coeffs, length );
    fmpq_poly_zero(pI_exact);
    slong p_degree = fmpq_poly_degree(pR_exact);
    /* output isolating boxes */
    acb_ptr roots = _acb_vec_init(p_degree);
    slong * mults = (slong *) pwpoly_malloc ( p_degree * sizeof(slong) );
    /* solving options */
    int goal=PW_GOAL_REFINE;
    nbMroots = PW_CAP_GOALNBROOTS(nbMroots,p_degree);
    domain_t dom;
    domain_init(dom);
    domain_set_mpz_tab( dom, domain_num, domain_den );
    
#ifdef PW_CHECK_INTERRUPT
    __pwpoly_set_interrupt_function( check_interrupt );
#endif    
//     printf("&&&&&&&&&&&&&&&&&&&&&&&& call pwroots for degree %ld: &&&&&&&&&&&&&&&&&&&&&&&&&&\n", p_degree);
    
    slong resNbClust = pwpoly_refine_2fmpq_poly( roots, mults,
                                                 pR_exact, pI_exact,
                                                 goal, epsilon, dom, nbMroots PW_VERBOSE_CALL(0) );
    
//     printf("&&&&&&&&&&&&&&&&&&return from pwroots: nbroots %ld &&&&&&&&&&&&&&&&&&&&&&\n", res);
    
    resNbClust = PWPOLY_MIN( resNbClust, nbMroots);
    *nbreals=0;
    
#ifdef PW_CHECK_INTERRUPT
    if ( !pwpoly_interrupt(0) )
#endif   
    mpz_cb_reals_nbreals_vec_set_acb_vec ( mpz_cb_roots, reals, nbreals, roots, resNbClust );
    for (slong i=0; i<resNbClust; i++)
        sumOfMults[i] = mults[i];
    
    domain_clear(dom);
    pwpoly_free(mults);
    _acb_vec_clear(roots, p_degree);
    fmpq_poly_clear(pR_exact);
    fmpq_poly_clear(pI_exact); 
    return resNbClust;
}

slong pwroots_refine_comp_mpz_poly_mpz_cb ( mpz_cb_ptr mpz_cb_roots, slong * sumOfMults,
                                             mpz_ptr coeffs_real, slong length_real,
                                             mpz_ptr coeffs_imag, slong length_imag,
                                             slong epsilon, 
                                             mpz_struct domain_num[], mpz_struct domain_den[], 
                                             slong nbMroots
#ifdef PW_CHECK_INTERRUPT
                                             , int (*check_interrupt)()
#endif
                                             ){
    
        /* input polynomial */
    fmpq_poly_t pR_exact, pI_exact;
    fmpq_poly_init(pR_exact);
    fmpq_poly_init(pI_exact);
    fmpq_poly_set_mpz_vec ( pR_exact, coeffs_real, length_real );
    fmpq_poly_set_mpz_vec ( pI_exact, coeffs_imag, length_imag );
    slong p_degree = PWPOLY_MAX( fmpq_poly_degree(pR_exact), fmpq_poly_degree(pI_exact) );
    /* output isolating boxes */
    acb_ptr roots = _acb_vec_init(p_degree);
    slong * mults = (slong *) pwpoly_malloc ( p_degree * sizeof(slong) );
    /* solving options */
    int goal=PW_GOAL_REFINE;
    nbMroots = PW_CAP_GOALNBROOTS(nbMroots,p_degree);
    domain_t dom;
    domain_init(dom);
    domain_set_mpz_tab( dom, domain_num, domain_den );
    
#ifdef PW_CHECK_INTERRUPT
    __pwpoly_set_interrupt_function( check_interrupt );
#endif   
//     printf("&&&&&&&&&&&&&&&&&&&&&&&& call pwroots for degree %ld: &&&&&&&&&&&&&&&&&&&&&&&&&&\n", p_degree);

    slong resNbClust = pwpoly_refine_2fmpq_poly( roots, mults,
                                                 pR_exact, pI_exact,
                                                 goal, epsilon, dom, nbMroots PW_VERBOSE_CALL(0) );
    
//     printf("&&&&&&&&&&&&&&&&&&return from pwroots: nb clusts %ld &&&&&&&&&&&&&&&&&&&&&&\n", resNbClust);

    resNbClust = PWPOLY_MIN( resNbClust, nbMroots);
    
#ifdef PW_CHECK_INTERRUPT
    if ( !pwpoly_interrupt(0) )
#endif    
    mpz_cb_vec_set_acb_vec ( mpz_cb_roots, roots, resNbClust );
    for (slong i=0; i<resNbClust; i++)
        sumOfMults[i] = mults[i];
    
    domain_clear(dom);
    pwpoly_free(mults);
    _acb_vec_clear(roots, p_degree);
    fmpq_poly_clear(pR_exact);
    fmpq_poly_clear(pI_exact);
    return resNbClust;
    
}
