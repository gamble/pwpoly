/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_SOLVING_H
#define PW_SOLVING_H

#include "pw_base.h"

#ifdef PWPOLY_HAS_EAROOTS
#include "earoots.h"
#endif

#ifdef PWPOLY_HAS_BEFFT
#include <fenv.h>
#include "be.h"
#include "common/pw_conversion_be.h"
#endif

#ifndef PW_NO_INTERFACE
#include "common/pw_gnuplot.h"
#endif

#include "common/pw_common.h"
#include "common/pw_roots.h"
#include "covering/pw_covering.h"
#include "approximation/pw_approximation.h"
#include "solvers/tstar.h"
#include "solvers/solver_CoCo.h"
#include "solvers/solvers_few_roots.h"
#include "solvers/pw_EA.h"
#include "geometry/domain.h"

#ifdef __cplusplus
extern "C" {
#endif
    
#ifndef PWPOLY_HAS_BEFFT /* otherwise defined in pw_conversion_be.h */
#define PWPOLY_SET_ROUND_NEAREST_FAILED -7
#define PWPOLY_APPROX_DOUBLE_INFINITE -6
#define PWPOLY_DOUBLE_EXCEPTION -5
#endif
#define PWPOLY_NOT_FINITE -4
#define PWPOLY_NOT_ISOLATED -3
#define PWPOLY_NOT_NATURAL -2    
#define PWPOLY_NOT_APPROXIMATED -1

/* for 0<= i <len, sets the i-th coeff of poly dest                                        */
/*                 to 2^-m times the middle of the i-th element of coeffs, with zero error */
/* where m is the exponent of the leading coefficient                                      */
void  _pwpoly_middle_almost_monic_acb_poly( acb_poly_t dest, acb_srcptr coeffs, slong len, slong prec );

#ifdef PWPOLY_HAS_BEFFT
slong _pw_solving_tstar_unit_disc_be( double * poly, slong len, int * n, int nbMDLG, slong nbMsols PW_VERBOSE_ARGU(verbose));
int _be_no_intersection_with_unit_disc( double re, double im, double ae );
#ifdef PWPOLY_HAS_EAROOTS
slong _pw_solving_earoots( double * roots, double * poly, slong len, slong outputprec PW_VERBOSE_ARGU(verbose) );
int _pw_solving_earoots_global_dpoly( acb_ptr roots, 
                                     double * Pcoeffs, double * Pabers, slong len, 
                                     slong outputprec, int realCoeffs, int nbMaxItts PW_VERBOSE_ARGU(verbose) );
#endif
#endif

/* options for solving */
#define PW_SOLVING_DOUBLES 0
#define PW_SOLVING_EAROOTS 1
#define PW_SOLVING_FEWROOT 2
#define PW_SOLVING_COCO    3
#define PW_SOLVING_INIT_EA 4
#define PW_SOLVING_FINL_EA 5
#define PW_SOLVING_MPSOLVE 6

#define PW_SOLVING_USE_DOUBLES(s) ( (s>>PW_SOLVING_DOUBLES)%2 )
#define PW_SOLVING_USE_EAROOTS(s) ( (s>>PW_SOLVING_EAROOTS)%2 )
#define PW_SOLVING_USE_FEWROOT(s) ( (s>>PW_SOLVING_FEWROOT)%2 )
#define PW_SOLVING_USE_COCO(s)    ( (s>>PW_SOLVING_COCO)%2 )
#define PW_SOLVING_USE_INIT_EA(s) ( (s>>PW_SOLVING_INIT_EA)%2 )
#define PW_SOLVING_USE_FINL_EA(s) ( (s>>PW_SOLVING_FINL_EA)%2 )
#define PW_SOLVING_USE_MPSOLVE(s) ( (s>>PW_SOLVING_MPSOLVE)%2 )

// use coco:                                                         8         = 8
// use doubles + coco:                                         1+    8         = 9
// use doubles + earoots + coco:                               1+2+  8         = 11
// use coco    + fewroot:                                          4+8         = 12
// use doubles + fewroot + coco:                               1+  4+8         = 13
// use doubles + earoots + fewroot + coco:                     1+2+4+8         = 15
// use doubles + coco    + initEA:                             1+    8+16      = 25
// use doubles + earoots + coco + initEA:                      1+2+  8+16      = 27
// use doubles + fewroots + coco + initEA:                     1+  4+8+16      = 29
// use doubles + earoots + fewroot + coco + initEA:            1+2+4+8+16      = 31
// use doubles + earoots + fewroot + coco + finlEA:            1+2+4+8   +32   = 47
// use doubles + earoots + fewroot + coco + initEA + finlEA:   1+2+4+8+16+32   = 63


#ifdef PWPOLY_HAS_EAROOTS
/* Preconditions: poly is a polynomial of degree d                                                               */
/*                goal is an integer in 1, 2, 3, 6, 7 (see pw_base.h)                                            */
/*                log2eps is an integer <=0, dom a domain,                                                       */
/*                goalNbRoots an integer; if -1 let N = d, if >=0 let N = min( goalNbRoots, d )                  */
/*                nbMaxItts an integer > 0                                                                       */
/*                roots (resp. mults) contains room for at least d acb's (resp. slong)                           */
/* Postconditions: if return value is -1, failed                                                                 */
/*                 otherwise, let r be the returned value                                                        */
/*                    for all i=0...r-1, mults[i] = 1                                                            */
/*                    for all i=0...r-1, roots[i] = Bi satisfying:                                               */
/*                    (i)    the Bi's are pairwise disjoints                                                     */
/*                    (ii)   for all i=0...r-1, Bi and 3Bi contain a unique root of poly                         */
/*                    (iii)  for all i=0...r-1, Bi has non empty intersection with dom                           */
/*                    (iv)   if r < N, then poly has no more than r roots in dom                                 */
/*                           else any roots of poly in dom is in a Bi                                            */
/*                    (v)    if PW_GOAL_MUST_APPROXIMATE(goal) then                                              */
/*                             for all i=0...r-1, Bi has width <= 2^log2eps                                      */
/*                    (vi)   if poly has real coefficients, real roots of poly are in a Bi with 0 imaginary part */
/*                 where 3Bi holds for the factor 3 centered dilation of Bi                                      */
/*                 if PW_GOAL_PREC_RELATIVE(goal), width is relative                                             */
/*                 if PW_GOAL_PREC_ABSOLUTE(goal), width is absolute                                             */
/* Brief: performs at most nbMaxItts Ehrlich Aberth iterations in double precision                               */
/*        on success, apply multi precision QBR iterations to refine the roots to required precision             */
slong _pwpoly_solve_pw_polynomial_earoots_standalone( acb_ptr roots, slong * mults, pw_polynomial_t poly,
                                                      int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                                                      int nbMaxItts PW_VERBOSE_ARGU(verbose) );
/* for acb polys with no error */
slong pwpoly_solve_acb_poly_earoots( acb_ptr roots, slong * mults, const acb_poly_t pacb,
                                     int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                                     int nbMaxItts PW_VERBOSE_ARGU(verbose) );
/* for polys given as a pair (real, imaginary) of fmpq polys */
slong pwpoly_solve_2fmpq_poly_earoots( acb_ptr roots, slong * mults, const fmpq_poly_t p_re, const fmpq_poly_t p_im,
                                       int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                                       int nbMaxItts PW_VERBOSE_ARGU(verbose) );
#endif
  
/* Preconditions: poly is a polynomial of degree d                                                             */
/*                roots (resp. mults) contains room for at least d acb's (resp. slong)                         */
/*                m positive integer (precision to compute the piecewise polynomial approximation              */
/*                c, scale, b are positive                                                                     */
/*                dom is a domain, goalNbRoots an integer                                                      */
/*                solver an integer (see options for solving above); by default, it should be 63               */
/* Postconditions: returns r such that 0<=r<= min(goalNbRoots+1, d)                                            */
/*                    for all i=0...r-1, roots[i] = Bi where Bi is                                             */
/*                        either a square box of center ci and half-width hi                                   */
/*                            or a real interval of center ci and half-width hi                                */
/*                    (i)    for all i=0...r-1, D(ci,ri) and D(ci,3ri) contain a unique root of poly           */
/*                    (ii)   for all i=0...r-1, Bi has non empty intersection with dom                         */
/*                    (iii)  if poly has real coefficients, Bi is a real interval <=> the root in Bi is real   */
/*                 where D(ci,ri) holds for the complex disc centered in ci with radius ri                     */
/* Brief: computes a piecewise polynomial approximation of poly                                                */
/*        solve it on each disk of the partition intersecting the domain until goalNbRoots have been found     */
slong _pwpoly_isolate_m_pw_polynomial( acb_ptr roots, pw_polynomial_t poly, 
                                       const ulong m, const ulong c, const fmpq_t scale, const fmpq_t b,
                                       const domain_t dom, slong goalNbRoots,
                                       int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile) );
/* for acb polys with no error */
slong pwpoly_isolate_m_acb_poly( acb_ptr roots, const acb_poly_t pacb, 
                                 const ulong m, const ulong c, const fmpq_t scale, const fmpq_t b,
                                 const domain_t dom, slong goalNbRoots,
                                 int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile) );
/* for polys given as a pair (real, imaginary) of fmpq polys */
slong pwpoly_isolate_m_2fmpq_poly( acb_ptr roots, const fmpq_poly_t p_re, const fmpq_poly_t p_im,
                                   const ulong m, const ulong c, const fmpq_t scale, const fmpq_t b,
                                   const domain_t dom, slong goalNbRoots,
                                   int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile) );
/* Preconditions: poly is a polynomial of degree d                                                               */
/*                goal is an integer in 1, 2, 3, 6, 7 (see pw_base.h)                                            */
/*                log2eps is an integer <=0, dom a domain,                                                       */
/*                goalNbRoots an integer; if -1 let N = d, if >=0 let N = min( goalNbRoots, d )                  */
/*                c, scale, b are positive, m is positive                                                        */
/*                roots (resp. mults) contains room for at least d acb's (resp. slong)                           */
/* Postconditions: returns r such that 0 <= r <= N                                                               */
/*                    for all i=0...r-1, 1<= mults[i] <=d, let R = sum of the mults[i]                           */
/*                    for all i=0...r-1, roots[i] = Bi satisfying:                                               */
/*                    (i)    the Bi's are pairwise disjoints                                                     */
/*                    (ii)   for all i=0...r-1, Bi and 3Bi contain mult[i] roots of poly counted with mult.      */
/*                    (iii)  for all i=0...r-1, Bi has non empty intersection with dom                           */
/*                    (iv)   if R < N, then poly has no more than R roots in dom                                 */
/*                           else any roots of poly in dom is in a Bi                                            */
/*                    (v)    if PW_GOAL_MUST_APPROXIMATE(goal) then                                              */
/*                             for all i=0...r-1, Bi has width <= 2^log2eps                                      */
/*                    (vi)   if poly has real coefficients, real roots of poly are in a Bi with 0 imaginary part */
/*                 where 3Bi holds for the factor 3 centered dilation of Bi                                      */
/*                 if PW_GOAL_PREC_RELATIVE(goal), width is relative                                             */
/*                 if PW_GOAL_PREC_ABSOLUTE(goal), width is absolute                                             */
/* Brief: first applies _pwpoly_solve_pw_polynomial_earoots_standalone with nbMaxItts = 100;                     */ 
/*        if success returns result;                                                                             */
/*        else let max_m := c*(max (m init, sqrt(d))) + 20                                                       */
/*             compute a piecewise polynomial iteration at precision m                                           */
/*             (a) if domain is C, solve it until N roots are found;                                             */ 
/*                 increase m until it reaches max_m                                                             */
/*             (b) if domain is not C, solve it until d roots are found;                                         */ 
/*                 increase m until it reaches max_m                                                             */
/*          refine the found roots intersecting dom with a piecewise pol. approx. at precision <= max_m          */
/*          refine the found roots intersecting domwith QBR iterations applied to poly                           */
/*          if N roots are found and refined in domain, return result                                            */
/*          else apply at most 500 Ehrlich Aberth iterations in multi-precision                                  */
/*               if fail, apply subdivision solver which is guaranted to succeed                                 */
slong _pwpoly_solve_pw_polynomial( acb_ptr roots, slong * mults, ulong *m, pw_polynomial_t poly,
                                  int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                                  const ulong c, const fmpq_t scale, const fmpq_t b,
                                  int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile) );
/* for polys given as a pair (real, imaginary) of fmpq polys */
slong pwpoly_solve_2fmpq_poly( acb_ptr roots, slong * mults, ulong *m, 
                               const fmpq_poly_t p_re, const fmpq_poly_t p_im,
                               int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                               const ulong c, const fmpq_t scale, const fmpq_t b,
                               int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile) );
/* for acb polys with no error */
slong pwpoly_solve_acb_poly( acb_ptr roots, slong * mults, ulong *m, 
                             const acb_poly_t pacb, 
                             int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                             const ulong c, const fmpq_t scale, const fmpq_t b,
                             int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile) );

/* assume no roots on boundary of the domain                                           */
/* assume the roots strictly inside the domain have sum of multiplicities multInDomain */
/* Preconditions: poly is a polynomial of degree d                                                               */
/*                goal is an integer in 10, 14 (see pw_base.h)                                                   */
/*                log2eps is an integer <=0, dom a finite domain with no roots on its boundary                   */
/*                multInDomain = sum of multiplicities of roots of poly strictly in domain                       */
/*                roots (resp. mults) contains room for at least d acb's (resp. slong)                           */
/* Postconditions: returns r such that 0 <= r <= multInDomain                                                    */
/*                    for all i=0...r-1, 1<= mults[i] <=d, st sum of the mults[i] = multInDomain                 */
/*                    for all i=0...r-1, roots[i] = Bi satisfying:                                               */
/*                    (i)    the Bi's are pairwise disjoints                                                     */
/*                    (ii)   for all i=0...r-1, Bi and 3Bi contain mult[i] roots of poly counted with mult.      */
/*                    (iii)  for all i=0...r-1, Bi is strictly in dom                                            */
/*                    (iv)   for all i=0...r-1, Bi has width <= 2^log2eps                                        */
/*                    (vi)   if poly has real coefficients, real roots of poly are in a Bi with 0 imaginary part */
/*                 where 3Bi holds for the factor 3 centered dilation of Bi                                      */
/*                 if PW_GOAL_PREC_RELATIVE(goal), width is relative                                             */
/*                 if PW_GOAL_PREC_ABSOLUTE(goal), width is absolute                                             */
/* Brief: applies subdivision solver in dom, which is guaranteed to succeed                                      */
slong _pwpoly_refine_pw_polynomial( acb_ptr roots, slong * mults, pw_polynomial_t poly,
                                    int goal, slong log2eps, domain_t dom, slong multInDomain PW_VERBOSE_ARGU(verbose));
/* for acb polys with no error */
slong pwpoly_refine_acb_poly( acb_ptr roots, slong * mults,
                              const acb_poly_t pacb, 
                              int goal, slong log2eps, domain_t dom, slong multInDomain PW_VERBOSE_ARGU(verbose));

slong pwpoly_refine_2fmpq_poly( acb_ptr roots, slong * mults,
                                const fmpq_poly_t p_re, const fmpq_poly_t p_im, 
                                int goal, slong log2eps, domain_t dom, slong multInDomain PW_VERBOSE_ARGU(verbose));

/* python interfaces */
/* see pre/post conditions of _pwpoly_solve_pw_polynomial, with m = 10, c=2, scale = 3/2, b = 2/5, solver = 63 */
slong pwpoly_solve_2fmpq_poly_python( acb_ptr roots, /* the roots */
                                      slong * mults, /* the multiplicities of the roots */
                                      const fmpq_poly_t p_re, const fmpq_poly_t p_im, /* the poly */
                                      int goal, slong log2eps, char * domStr, slong goalNbRoots );
/* see pre/post conditions of _pwpoly_solve_pw_polynomial, with m = 10, c=2, scale = 3/2, b = 2/5, solver = 63 */
slong pwpoly_solve_exact_acb_poly_python( acb_ptr roots, /* the roots */
                                          slong * mults, /* the multiplicities of the roots */
                                          const acb_poly_t p, /* the poly */
                                          int goal, slong log2eps, char * domStr, slong goalNbRoots );
/* temporary */
void pwpoly_solve_2_double_poly( double * rr, double * ri, double *ab, /* the roots */
                                 double * pr, double * pi, int len /* the poly */ );

slong pwpoly_solve_2fmpq_poly_mp( acb_ptr roots, /* the roots */
                                  slong * mults, /* the multiplicities of the roots */
                                  const fmpq_poly_t p_re, const fmpq_poly_t p_im, /* the poly */ 
                                  int goal, slong log2eps, char * domStr, slong goalNbRoots );

slong pwpoly_solve_2fmpq_poly_double( double * rr, double * ri, double *ab, /* the roots */
                                      slong * mults, /* the multiplicities of the roots */
                                      const fmpq_poly_t p_re, const fmpq_poly_t p_im, /* the poly */ 
                                      int goal, slong log2eps );

slong pwpoly_solve_acb_poly_double( double * rr, double * ri, double *ab, /* the roots */
                                    slong * mults, /* the multiplicities of the roots */
                                    const acb_poly_t p, /* the poly */ 
                                    int goal, slong log2eps );

#ifdef __cplusplus
}
#endif

#endif
