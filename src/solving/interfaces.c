/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_solving.h"

slong pwpoly_isolate_m_acb_poly( acb_ptr roots, const acb_poly_t pacb, 
                                 const ulong m, const ulong c, const fmpq_t scale, const fmpq_t b,
                                 const domain_t dom, slong goalNbRoots,
                                 int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile) ) {
    
//     int level = 2; 
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, pacb );
    
    slong nbroots = _pwpoly_isolate_m_pw_polynomial( roots, poly, m, c, scale, b, dom, goalNbRoots, solver PW_VERBOSE_CALL(verbose) PW_FILE_CALL(covrFile));
    
    pw_polynomial_clear(poly);
    
    return nbroots;
}

slong pwpoly_isolate_m_2fmpq_poly( acb_ptr roots, const fmpq_poly_t p_re, const fmpq_poly_t p_im,
                                   const ulong m, const ulong c, const fmpq_t scale, const fmpq_t b,
                                   const domain_t dom, slong goalNbRoots,
                                   int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile) ) {
    
//     int level = 2; 
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    
    slong nbroots = _pwpoly_isolate_m_pw_polynomial( roots, poly, m, c, scale, b, dom, goalNbRoots, solver PW_VERBOSE_CALL(verbose) PW_FILE_CALL(covrFile));
    
    pw_polynomial_clear(poly);
    
    return nbroots;
}

#ifdef PWPOLY_HAS_EAROOTS
slong pwpoly_solve_acb_poly_earoots( acb_ptr roots, slong * mults, const acb_poly_t pacb,
                                     int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                                     int nbMaxItts PW_VERBOSE_ARGU(verbose)  ) {

    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, pacb );
    
    slong res = _pwpoly_solve_pw_polynomial_earoots_standalone( roots, mults, poly, goal, log2eps, dom, goalNbRoots, nbMaxItts PW_VERBOSE_CALL(verbose)  );
    
    pw_polynomial_clear(poly);
    
    return res;
}

slong pwpoly_solve_2fmpq_poly_earoots( acb_ptr roots, slong * mults, const fmpq_poly_t p_re, const fmpq_poly_t p_im,
                                           int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                                       int nbMaxItts PW_VERBOSE_ARGU(verbose)  ) {

    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    
    slong res = _pwpoly_solve_pw_polynomial_earoots_standalone( roots, mults, poly, goal, log2eps, dom, goalNbRoots, nbMaxItts PW_VERBOSE_CALL(verbose)  );
    
    pw_polynomial_clear(poly);
    
    return res;
}
#endif

slong pwpoly_solve_acb_poly( acb_ptr roots, slong * mults, ulong *m, 
                             const acb_poly_t pacb, 
                             int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                             const ulong c, const fmpq_t scale, const fmpq_t b,
                             int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile) ){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, pacb );
    
    slong nbClusts = _pwpoly_solve_pw_polynomial( roots, mults, m, poly, goal, log2eps, dom, goalNbRoots, c, scale, b, solver PW_VERBOSE_CALL(verbose) PW_FILE_CALL(covrFile));
    
    pw_polynomial_clear(poly);
    
    return nbClusts;
}

slong pwpoly_solve_2fmpq_poly( acb_ptr roots, slong * mults, ulong *m, 
                               const fmpq_poly_t p_re, const fmpq_poly_t p_im,
                               int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                               const ulong c, const fmpq_t scale, const fmpq_t b,
                               int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile) ){
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    
    slong nbClusts = _pwpoly_solve_pw_polynomial( roots, mults, m, poly, goal, log2eps, dom, goalNbRoots, c, scale, b, solver PW_VERBOSE_CALL(verbose) PW_FILE_CALL(covrFile));
    
    pw_polynomial_clear(poly);
    
    return nbClusts;
}

slong pwpoly_refine_acb_poly( acb_ptr roots, slong * mults,
                              const acb_poly_t pacb, 
                              int goal, slong log2eps, domain_t dom, slong multInDomain PW_VERBOSE_ARGU(verbose) ){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, pacb );
    
    slong nbClusts = _pwpoly_refine_pw_polynomial( roots, mults, poly, goal, log2eps, dom, multInDomain PW_VERBOSE_CALL(verbose));
    
    pw_polynomial_clear(poly);
    
    return nbClusts;
}

slong pwpoly_refine_2fmpq_poly( acb_ptr roots, slong * mults,
                                const fmpq_poly_t p_re, const fmpq_poly_t p_im, 
                                int goal, slong log2eps, domain_t dom, slong multInDomain PW_VERBOSE_ARGU(verbose)) {
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    
    slong nbClusts = _pwpoly_refine_pw_polynomial( roots, mults, poly, goal, log2eps, dom, multInDomain PW_VERBOSE_CALL(verbose) );
    
    pw_polynomial_clear(poly);
    
    return nbClusts;
}

slong _pwpoly_solve_pw_polynomial_python( acb_ptr roots, /* the roots */
                                          slong * mults, /* the multiplicities of the roots */
                                          pw_polynomial_t poly, /* the poly */
                                          int goal, slong log2eps, char * domStr, slong goalNbRoots ){ /* the solving options */
    ulong m = 10;
    ulong c = 2;
    fmpq_t scale, b;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
    int solver = 63;
    
    domain_t dom;
    domain_init(dom);
    slong InDom[8];
//     printf("input domainStr: %s\n", domStr);
    domain_set_str(InDom, domStr);
    domain_set_si( dom, InDom );
//     printf("input domain: "); domain_print(dom); printf("\n");
    
    slong nbClusts = _pwpoly_solve_pw_polynomial( roots, mults, &m, poly, goal, log2eps, dom, goalNbRoots, c, scale, b, solver PW_VERBOSE_CALL(0) PW_FILE_CALL(NULL));
    
    domain_clear(dom);
    fmpq_clear(b);
    fmpq_clear(scale);
    
    return nbClusts;
}

slong pwpoly_solve_2fmpq_poly_python( acb_ptr roots, /* the roots */
                                      slong * mults, /* the multiplicities of the roots */
                                      const fmpq_poly_t p_re, const fmpq_poly_t p_im, /* the poly */
                                      int goal, slong log2eps, char * domStr, slong goalNbRoots ){ /* the solving options */
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    
    slong nbClusts = _pwpoly_solve_pw_polynomial_python( roots, mults, poly, goal, log2eps, domStr, goalNbRoots);
    
    
     pw_polynomial_clear(poly);
    flint_cleanup();
    
    return nbClusts;
    
}

slong pwpoly_solve_exact_acb_poly_python( acb_ptr roots, /* the roots */
                                          slong * mults, /* the multiplicities of the roots */
                                          const acb_poly_t p, /* the poly */
                                          int goal, slong log2eps, char * domStr, slong goalNbRoots ){ /* the solving options */
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, p );
    
    slong nbClusts = _pwpoly_solve_pw_polynomial_python( roots, mults, poly, goal, log2eps, domStr, goalNbRoots);
    
    
     pw_polynomial_clear(poly);
    flint_cleanup();
    
    return nbClusts;
    
}

slong pwpoly_solve_2fmpq_poly_mp( acb_ptr roots, /* the roots */
                                  slong * mults, /* the multiplicities of the roots */
                                  const fmpq_poly_t p_re, const fmpq_poly_t p_im, /* the poly */
                                  int goal, slong log2eps, char * domStr, slong goalNbRoots ){ /* the solving options */
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
//     slong len = pw_polynomial_length(poly);
    
    ulong m = 10;
    ulong c = 2;
    fmpq_t scale, b;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
    int solver = 63;
    
    domain_t dom;
    domain_init(dom);
    slong InDom[8];
//     printf("input domainStr: %s\n", domStr);
    domain_set_str(InDom, domStr);
    domain_set_si( dom, InDom );
//     printf("input domain: "); domain_print(dom); printf("\n");
    
    slong nbClusts = _pwpoly_solve_pw_polynomial( roots, mults, &m, poly, goal, log2eps, dom, goalNbRoots, c, scale, b, solver PW_VERBOSE_CALL(0) PW_FILE_CALL(NULL));
    
    domain_clear(dom);
    
    fmpq_clear(b);
    fmpq_clear(scale);
     pw_polynomial_clear(poly);
    flint_cleanup();
    
    return nbClusts;
    
}

void pwpoly_solve_2_double_poly( double * rr, double * ri, double *ab,/* the roots */
                                 double * pr, double * pi, int len /* the poly */ ) {
    
    acb_poly_t p;
    acb_poly_init2(p, len);
    _acb_poly_set_length(p, len);
    for (int i=0; i<len; i++)
        acb_set_d_d(p->coeffs+i, pr[i], pi[i]);
    
    acb_ptr roots = _acb_vec_init(len-1);
    slong * mults = (slong *) pwpoly_malloc ( (len-1)*sizeof(slong) );
    ulong m = 10;
    int goal = PW_GOAL_ISO_AND_APP;
    slong log2eps = -PWPOLY_DEFAULT_PREC;
    ulong c = 2;
    fmpq_t scale, b;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
    int solver = 63;
    
    domain_t dom;
    domain_init(dom);
    
    pwpoly_solve_acb_poly( roots, mults, &m, p, goal, log2eps, dom, -1, c, scale, b, solver PW_VERBOSE_CALL(0) PW_FILE_CALL(NULL));
    
    domain_clear(dom);
    
    for (int i=0; i<len-1; i++) {
#ifdef PWPOLY_HAS_BEFFT
        _be_set_acb( rr+i, ri+i, ab+i, roots+i );
#else
        rr[i] = arf_get_d( arb_midref( acb_realref( roots+i ) ), ARF_RND_NEAR );
        ri[i] = arf_get_d( arb_midref( acb_imagref( roots+i ) ), ARF_RND_NEAR );
#endif        
    }
    
    fmpq_clear(b);
    fmpq_clear(scale);
    pwpoly_free(mults);
    _acb_vec_clear(roots, len-1);
    acb_poly_clear(p);
    flint_cleanup();
}

slong pwpoly_solve_2fmpq_poly_double( double * rr, double * ri, double *ab, /* the roots */
                                      slong * mults, /* the multiplicities of the roots */
                                      const fmpq_poly_t p_re, const fmpq_poly_t p_im, /* the poly */
                                      int goal, slong log2eps ){
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    slong len = pw_polynomial_length(poly);
    
    acb_ptr roots = _acb_vec_init(len-1);
    ulong m = 10;
    ulong c = 2;
    fmpq_t scale, b;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
    int solver = 63;
    
    domain_t dom;
    domain_init(dom);
    
    slong nbClusts = _pwpoly_solve_pw_polynomial( roots, mults, &m, poly, goal, log2eps, dom, -1, c, scale, b, solver PW_VERBOSE_CALL(0) PW_FILE_CALL(NULL));
    
    domain_clear(dom);
    
    for (slong i=0; i<nbClusts; i++) {
#ifdef PWPOLY_HAS_BEFFT
        _be_set_acb( rr+i, ri+i, ab+i, roots+i );
#else
        rr[i] = arf_get_d( arb_midref( acb_realref( roots+i ) ), ARF_RND_NEAR );
        ri[i] = arf_get_d( arb_midref( acb_imagref( roots+i ) ), ARF_RND_NEAR );
#endif        
    }
    
    fmpq_clear(b);
    fmpq_clear(scale);
    _acb_vec_clear(roots, len-1);
     pw_polynomial_clear(poly);
    flint_cleanup();
    
    return nbClusts;
    
}

slong pwpoly_solve_acb_poly_double( double * rr, double * ri, double *ab, /* the roots */
                                    slong * mults, /* the multiplicities of the roots */
                                    const acb_poly_t p, /* the poly */ 
                                    int goal, slong log2eps ){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, p );
    slong len = pw_polynomial_length(poly);
    
    acb_ptr roots = _acb_vec_init(len-1);
    ulong m = 10;
    ulong c = 2;
    fmpq_t scale, b;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
    int solver = 63;
    
    domain_t dom;
    domain_init(dom);
    
    slong nbClusts = _pwpoly_solve_pw_polynomial( roots, mults, &m, poly, goal, log2eps, dom, -1, c, scale, b, solver PW_VERBOSE_CALL(0) PW_FILE_CALL(NULL));
    
    domain_clear(dom);
    
    for (slong i=0; i<nbClusts; i++) {
#ifdef PWPOLY_HAS_BEFFT
        _be_set_acb( rr+i, ri+i, ab+i, roots+i );
#else
        rr[i] = arf_get_d( arb_midref( acb_realref( roots+i ) ), ARF_RND_NEAR );
        ri[i] = arf_get_d( arb_midref( acb_imagref( roots+i ) ), ARF_RND_NEAR );
#endif        
    }
    
    fmpq_clear(b);
    fmpq_clear(scale);
    _acb_vec_clear(roots, len-1);
     pw_polynomial_clear(poly);
    flint_cleanup();
    
    return nbClusts;
}
