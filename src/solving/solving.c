/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_solving.h"

#ifdef PW_PROFILE
double clicks_in_earoots_global;
double clicks_in_eaitts_global;
double clicks_in_checks_global;
double clicks_in_EA_mp;
double clicks_in_CoCo_global;
double clicks_in_inclusion;
double clicks_in_conv_mps;
double clicks_in_mpsolve;
double clicks_in_conv_dbl;
double clicks_in_earoots;
double clicks_in_refineEA;
double clicks_in_CoCo;
double clicks_in_fewRoots[3];
double clicks_in_fewRoots_be[3];
double clicks_in_merge;
double clicks_in_solve_approx;
double clicks_in_refine_piecewise;
double clicks_in_refine_compApp;
double clicks_in_refine_locate;
double clicks_in_refine_CoCo;
double clicks_in_refine_disc;
double clicks_in_CoCo_refine;
#ifndef PWPOLY_HAS_BEFFT
double clicks_in_beDLG;
double clicks_in_bepellet;
#endif
ulong  nb_sectors_in_covering;
ulong  nb_dbl_conv;
ulong  nb_succ_dbl_conv;
ulong  nb_exce_dbl_conv;
ulong  nb_arbpellet_tests;
ulong  nb_bepellet_tests;
ulong  nb_succ_bepellet_tests;
ulong  nb_exce_bepellet_tests;
ulong  nb_som_root;
ulong  nb_und_root;
ulong  nb_one_root;
ulong  nb_two_root;
ulong  nb_thr_root;
ulong  nb_sector_solved;
ulong  nb_sector_solved_mpsolve;
ulong  nb_sector_solved_earoots;
ulong  nb_sector_solved_CoCo;
ulong  nb_succ_earoots;
ulong  nb_exce_earoots;
ulong  nb_succ_refineEA;
ulong  nb_call_refineEA;
ulong  nb_succ_fewRoots[3];
ulong  nb_call_fewRoots[3];
ulong  nb_succ_fewRoots_be[3];
ulong  nb_call_fewRoots_be[3];
#endif

/* for 0<= i <len, sets the i-th coeff of poly dest                             */
/*                 to the middle of the i-th element of coeffs, with zero error */
void  _pwpoly_middle_acb_poly( acb_poly_t dest, acb_srcptr coeffs, slong len ) {
    
    acb_poly_fit_length(dest, len);
    _acb_poly_set_length(dest, len);
    for( slong i=0; i<len; i++){
        acb_get_mid( dest->coeffs+i, coeffs+i );
    }
    
}

/* for 0<= i <len, sets the i-th coeff of poly dest                                        */
/*                 to 2^-m times the middle of the i-th element of coeffs, with zero error */
/* where m is the exponent of the leading coefficient                                      */
void  _pwpoly_middle_almost_monic_acb_poly( acb_poly_t dest, acb_srcptr coeffs, slong len, slong prec ) {
    
    arb_t abs;
    arb_init(abs);
    arf_t mantissaleading;
    arf_init(mantissaleading);
    fmpz_t exponentleading;
    fmpz_init(exponentleading);
    
    acb_abs(abs, coeffs+(len-1), prec);
    arf_frexp( mantissaleading, exponentleading, arb_midref(abs));
    fmpz_neg(exponentleading, exponentleading);
    
    acb_poly_fit_length(dest, len);
    _acb_poly_set_length(dest, len);
    for( slong i=0; i<len; i++){
        acb_get_mid( dest->coeffs+i, coeffs+i );
        acb_mul_2exp_fmpz( dest->coeffs+i, dest->coeffs+i, exponentleading );
    }
    
    fmpz_clear(exponentleading);
    arf_clear(mantissaleading);
    arb_clear(abs);
}

#ifdef PWPOLY_HAS_EAROOTS
slong _pw_solving_solve_in_disc_earoots( acb_ptr roots, double * poly, acb_srcptr acbpoly, slong len, slong outputprec, int unitDisc, slong prec PW_VERBOSE_ARGU(verbose) ) {
    
    //     int level = 1;
    
    double * Droots = (double *) pwpoly_malloc ( 3*(len-1)*sizeof(double) );
    
#ifdef PW_PROFILE
    clock_t start = clock();
    nb_sector_solved_earoots++;
#endif
    
    slong res = _pw_solving_earoots( Droots, poly, len, outputprec PW_VERBOSE_CALL(verbose) );
    
#ifdef PW_PROFILE
    clicks_in_earoots += (clock() - start);
    if (res==PWPOLY_DOUBLE_EXCEPTION)
        nb_exce_earoots++;
#endif
    
    if (res<0) {
        pwpoly_free(Droots);
        return res;
    }
   
    int must_refine = 0;
    double max_error = ldexp(1, -outputprec);
    
    slong nbRoots = 0;
    /* check that roots intersecting the unit disc are well approximated */
    for (slong i=0; (i<(len-1))&&(must_refine==0); i++) {
    
        if ( (unitDisc)&&(_be_no_intersection_with_unit_disc( Droots[3*i], Droots[3*i+1], Droots[3*i+2] )) )
            continue;
        else {
            _be_get_acb( roots + nbRoots, Droots[3*i], Droots[3*i+1], Droots[3*i+2] );
            acb_get_mid( roots + nbRoots, roots + nbRoots);
            nbRoots++;
            if (Droots[3*i+2] > max_error)
                must_refine=1;
        }
    }
    
    if (must_refine) {
        nbRoots = PWPOLY_NOT_APPROXIMATED;
        
        if (acbpoly!=NULL) {
#ifdef PW_PROFILE
            clock_t start_refine = clock();
            nb_call_refineEA++;
#endif
            arb_t absroot, one;
            arb_init(absroot);
            arb_init(one);
            arb_one(one);
            /* recopy all the roots */
            for (slong i=0; i<(len-1); i++)
                _be_get_acb ( roots + i, Droots[3*i], Droots[3*i+1], Droots[3*i+2] );
            /* apply EA iterations to all the roots */
            slong resEA = _EA_its_on_l_roots( roots, len-1, -outputprec, 50,  acbpoly, len, prec, unitDisc PW_VERBOSE_CALL(verbose) );
//             /* check that roots are pairwise disjoints*/
//             for (slong i=0; (i<len-1)&&resEA; i++)
//                 for (slong k=0; (k<i)&&resEA; k++)
//                     resEA=resEA&&(acb_overlaps(roots + k, roots + i)==0);
            /* check that roots intersecting the unit disc are well approximated */
            if (resEA>0) {
                nbRoots = 0;
                for (slong i=0; i<(len-1); i++) {
                    acb_abs(absroot, roots+i, prec);
                    if (arb_gt( absroot, one) == 0) {
                        acb_get_mid( roots + nbRoots, roots + i );
                        nbRoots++;
                    }
                }
            } 
            arb_clear(one);
            arb_clear(absroot);
#ifdef PW_PROFILE
            clicks_in_refineEA += (clock() - start_refine);
            if (resEA>0)
                nb_succ_refineEA++;
#endif
        }
    }
#ifdef PW_PROFILE
    else
        nb_succ_earoots++;
#endif
    
    pwpoly_free(Droots);
    
    return nbRoots;
}
#endif

slong _pw_solving_solve_in_disc_CoCo( acb_ptr roots, slong outputprec, acb_srcptr coeffs, slong len, 
                                      const pw_approximation_t app, ulong n,
                                      slong prec, slong nbRootsInUnitDisc PW_VERBOSE_ARGU(verbose) ) {

#ifdef PW_PROFILE
    clock_t start = clock();
    nb_sector_solved_CoCo++;
#endif
    
    acb_poly_t exact;
    acb_poly_init2(exact, len);
    _acb_poly_set_length(exact, len);
    int realCoeffs = 1;
    for (slong i=0; i<len; i++){
        acb_set( exact->coeffs+i, coeffs+i );
        realCoeffs = realCoeffs && arb_is_zero( acb_imagref(exact->coeffs+i) );
    }
    
    slong noutputprec = -3*outputprec;
    ulong N=pw_approximation_Nref(app);
    ulong K = pw_approximation_nbsectref(app)[n];
    int scale = (K==1)&&( ( (n<N-1)  && ( fmpq_cmp_ui( pw_approximation_bigradref(app)+n, 0 ) > 0 ) )
                        ||( (n==N-1) && ( fmpq_cmp_ui( pw_approximation_bigradref(app)+(n-1), 0 ) < 0 ) ) );

    slong ceilLog2rad = 0;
    if (scale) {
        arb_t r;
        arb_init(r);
        if (n<N-1)
            arb_set(r, pw_approximation_annulii_radsref(app)+n);
        else {
            arb_set(r, pw_approximation_annulii_radsref(app)+(n-1));
            arb_inv(r, r, prec);
        }
        mag_t rad;
        mag_init(rad);
        arb_get_mag(rad, r);
        while ( mag_cmp_2exp_si(rad, ceilLog2rad) >= 0 )
            ceilLog2rad++;
        mag_clear(rad);
        arb_clear(r);
        /* scale input polynomial */
        fmpz_t logscale;
        fmpz_init(logscale);
        fmpz_zero(logscale);
        for (slong i=0; i<len; i++) {
            acb_mul_2exp_fmpz(exact->coeffs + i, exact->coeffs + i, logscale);
            fmpz_add_si(logscale, logscale, ceilLog2rad);
        }
        fmpz_clear(logscale);
        noutputprec = noutputprec - ceilLog2rad;
    }
    
    slong * mults = (slong *) pwpoly_malloc ( (len-1)*sizeof(slong) );
    
    slong nbClust = solver_CoCo_approximate_unit_disc_exact_acb_poly( roots, mults, exact, noutputprec, realCoeffs, nbRootsInUnitDisc PW_VERBOSE_CALL(verbose) );
    
#ifdef PW_PROFILE
    clicks_in_CoCo += (clock() - start);
#endif
    
    /* set roots to centers of output boxes */
    for (slong i=0; i<nbClust; i++){
        acb_get_mid( roots+i, roots+i );
        if (scale) {
            /* scale root */
            acb_mul_2exp_si(roots + i, roots + i, ceilLog2rad);
        }
    }
    
    pwpoly_free(mults);
    acb_poly_clear(exact);
    
    return nbClust;
}

int _pw_solving_get_isolating_disc( arb_t radius, const pw_approximation_t app, const ulong n, const acb_t x PW_VERBOSE_ARGU(verbose) ) {
    
#ifdef PW_PROFILE
    clock_t start = clock();
#endif
#ifndef PW_SILENT    
    int level = 3;
#endif    
    int res=PW_INDETERMI;
    slong prec = pw_approximation_precref(app);
    
    acb_t phi, phip, phipp;
    acb_init(phi);
    acb_init(phip);
    acb_init(phipp);
    arb_t errorpp, absphi, absphip, absphipp, absx, right, K, one;
    arb_init(errorpp);
    arb_init(absphi);
    arb_init(absphip);
    arb_init(absphipp);
    arb_init(absx);
    arb_init(right);
    arb_init(K);
    arb_init(one);
    fmpq_t oneOd;
    fmpq_init(oneOd);
    
    arb_one(one);
#ifndef PW_SILENT    
    if (verbose>=level) {
        pw_approximation_evaluate2_acb( phi, phip, app, x PW_VERBOSE_CALL(verbose)  );
        printf("--------------_pw_solving_get_isolating_disc-------------------\n");
        printf("x      : "); acb_printd(x, 10); printf("\n");
        printf("f(x)   : "); acb_printd(phi, 10); printf("\n");
        printf("f'(x)  : "); acb_printd(phip, 10); printf("\n");
    }
#endif    
    pw_approximation_evaluate_phi_3( phi, phip, phipp, errorpp, app, n, x PW_VERBOSE_CALL(verbose) );
    
    acb_abs(absphi, phi, prec);
    acb_abs(absphip, phip, prec);
    acb_abs(absphipp, phipp, prec);
    /* compute r=2|phi(x)|/|phi'(x)| */
    arb_div(radius, absphi, absphip, prec);
    arb_mul_2exp_si(radius, radius, 1);
    /* set absx to |x| */
    acb_abs(absx, x, prec);
    /* set right to (1/4)*|x|*(2^(1/d)-1) */
    fmpq_set_ui(oneOd, 1, pw_approximation_lenref(app)-1);
    arb_set_ui(right, 2);
    arb_pow_fmpq(right, right, oneOd, prec);
    arb_sub_si(right, right, 1, prec);
    arb_mul(right, right, absx, prec);
    arb_mul_2exp_si(right, right, -2);
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("--------------_pw_solving_get_isolating_disc-------------------\n");
        printf("x      : "); acb_printd(x, 10); printf("\n");
        printf("phi    : "); acb_printd(phi, 10); printf("\n");
        printf("phip   : "); acb_printd(phip, 10); printf("\n");
        printf("phipp  : "); acb_printd(phipp, 10); printf("\n");
        printf("errorpp: "); arb_printd(errorpp, 10); printf("\n");
        printf("radius : "); arb_printd(radius, 10); printf("\n");
        printf("right:=(1/4)|y|(2^(1/d)-1): "); arb_printd(right, 10); printf("\n");
        printf("radius<right?             : %d\n", arb_lt( radius, right ) );
    }
#endif    
    if (arb_lt( radius, right ) ) {
        /* compute K >= ( max_{z in D(x, radius} |f''(x)| )/|f'(x)| */
        arb_mul( K, radius, errorpp, prec);
        arb_add_error(absphipp, K);
        arb_div( K, absphipp, absphip, prec );
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("K      : "); arb_printd(K, 10); printf("\n");
        }
#endif
        /* compute 5rK */
        arb_mul(K, K, radius, prec);
        arb_mul_ui( K, K, 5, prec );
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("5rK    : "); arb_printd(K, 10); printf("\n");
            printf("5rK<1? : %d\n", arb_lt( K, one ) );
        }
#endif
        if ( !(arb_lt( K, one )) ) {
            arb_indeterminate( radius );
        } else {
            res=PW_ISOLATING;
            /* try to prove that the disc D(x, radius) is a natural isolator */
            /* i.e. D(x,3*radius) contains a unique root                     */
            arb_t threeRadius;
            arb_init(threeRadius);
            arb_mul_ui(threeRadius, radius, 3, prec);
            if (arb_lt( threeRadius, right ) ) {
                /* compute K >= ( max_{z in D(x, threeRadius} |f''(x)| )/|f'(x)| */
                arb_mul( K, threeRadius, errorpp, prec);
                acb_abs(absphipp, phipp, prec);
                arb_add_error(absphipp, K);
                arb_div( K, absphipp, absphip, prec );
                /* compute 5rK */
                arb_mul(K, K, radius, prec);
                arb_mul_ui( K, K, 5, prec );
                if (arb_lt( K, one )) {
                    res=PW_NATURAL_I;
                } else {
//                     printf(" isolator but not natural isolator !!! \n");
                }
            }
            arb_clear(threeRadius);
        }
    } else {
        arb_indeterminate( radius );
    }
    
    if (res==PW_INDETERMI) {
        /* try to get an inclusion disc */
        arb_div(radius, absphi, absphip, prec);
        arb_mul_ui(radius, radius, pw_approximation_lenref(app)-1, prec);
        if (arb_is_finite(radius)){
            res = PW_INCLUDING;
        } else {
            arb_indeterminate( radius );
        }
    }
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("-------------------result: %d-----------------------\n\n", res);
    }
#endif    
    fmpq_clear(oneOd);
    arb_clear(one);
    arb_clear(K);
    arb_clear(right);
    arb_clear(absx);
    arb_clear(absphipp);
    arb_clear(absphip);
    arb_clear(absphi);
    arb_clear(errorpp);
    acb_clear(phipp);
    acb_clear(phip);
    acb_clear(phi);
    
    
#ifdef PW_PROFILE
    clicks_in_inclusion += (clock() - start);
#endif
    
    return res;
}

/* let app be a piecewise approximation of f of degree d                                   */
/*     n,k be a sector of the approximation                                                */
/*     roots be a vector of len complex points (sought to be approximated roots of g_{n,k} */
/* for each y in roots, let x be y shifted in the global reference                         */
/* for each y such that x is in the containing disc of sector n,k                          */
/*     compute r s.t D(x,r) contains at least a root of f                                  */
/*     if D(x,r) intersects sector n,k, push D(x,r) in dest                                */
/* returns the number of elements in dest                                                  */
ulong  _pw_solving_get_roots_intersecting_nk_sector( root_ptr dest, acb_ptr *missed, ulong *nbmissed, const pw_approximation_t app,
                                                     const acb_ptr roots, ulong len, ulong n, ulong k PW_VERBOSE_ARGU(verbose) ){
#ifndef PW_SILENT    
    int level = 3;
#endif    
    acb_t center;
    acb_init(center);
    
    arb_t radius, abs, one;
    arb_init(radius);
    arb_init(abs);
    arb_init(one);
    arb_one(one);
    
    ulong N = pw_approximation_Nref(app);
    ulong K = pw_approximation_nbsectref(app)[n];
    slong prec = pw_approximation_precref(app);
    
    ulong nbdiscs=0;
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("_pw_solving_get_roots_intersecting_nk_sector, n: %lu/%lu, k: %lu/%lu, nb candidate roots: %ld \n", n, N-1, k, K-1, len);
        printf("---%lu-th annulus: r=", n);
        if (n==0)
            printf("0");
        else 
            arb_printd( pw_approximation_annulii_radsref(app)+(n-1), 10);
        printf(", R=");
        if (n==N-1)
            printf("+inf");
        else
            arb_printd( pw_approximation_annulii_radsref(app)+n, 10);
        printf("\n");
    }
#endif    
    for (ulong i = 0; i < len; i++){
        int isolation = PW_INDETERMI;
        _pw_covering_shift_back_point( center, pw_approximation_coveringref(app), n, k, roots+i, prec );
        /* make center square */
        mag_max( arb_radref( acb_realref( center ) ), arb_radref( acb_realref( center ) ), arb_radref( acb_imagref( center ) ) );
        mag_set( arb_radref( acb_imagref( center ) ), arb_radref( acb_realref( center ) ) );
        
        /* if the n-th annulus is divided in sectors, get rid of roots not in the unit disk */
        if ( K>1 ) { 
            acb_abs(abs, roots+i, prec);
            if (arb_gt(abs,one))
                continue;
        }
        /* get rid of roots not in a connected annulus */
        acb_abs(abs, center, prec);
        if ( (n>=2) && (arb_le(abs, pw_approximation_annulii_radsref(app)+(n-2))) ) {
            continue;
        }
        if ( ((slong)n<=((slong)N-3)) && (arb_ge(abs, pw_approximation_annulii_radsref(app)+(n+1))) ) {
            continue;
        }
        
        /* here roots+i is either in the unit disk of the nk-th approx, */
        /* or in a connected annulus                                    */ 
        isolation = _pw_solving_get_isolating_disc( radius, app, n, center PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("---isolating radius: "); arb_printd(radius, 10); printf(", inclusion: %d\n\n", isolation);
        }     

        if (verbose>=level) {
            if (isolation<PW_NATURAL_I){
                printf("_pw_solving_get_roots_intersecting_nk_sector: isolation %d, annulus: %lu\n", isolation, n);
            }
        }
#endif        
        if (isolation==PW_NATURAL_I){
            
            acb_add_error_arb( center, radius);
            /* ensures that B( center, radius ) does not contain zero */
            if ( acb_contains_zero(center) )
                continue;
                
            /* check if dest+nbdiscs intersects the angular sector */
            pw_sector_list_t sectors;
            pw_sector_list_init(sectors);
            
            pw_covering_locate_point( sectors, n, pw_approximation_coveringref(app), center, prec );
            
            int isIn = pw_sector_is_in_sorted_sector_list ( n, k, K, sectors );
            if (isIn) {
                root_set_acb_int ( dest+nbdiscs, center, isolation );
                nbdiscs++;
            } 
            pw_sector_list_clear(sectors);
        } else {
//             if (arb_is_finite(radius)){
//                 acb_add_error_arb( center, radius);
//             }
            acb_set( *missed + (*nbmissed), center );
            (*nbmissed) ++;
            
            /* increase the size of missed if necessary */
            if ( (*nbmissed)%pw_approximation_lenref(app) == 0 ) {
                ulong old_allocated_size = (*nbmissed);
                ulong new_allocated_size = (*nbmissed) + pw_approximation_lenref(app);
                acb_ptr nmissed = _acb_vec_init( new_allocated_size );
                for (i=0; i<(*nbmissed); i++)
                    acb_set( nmissed + i, *missed + i );
                _acb_vec_clear(*missed, old_allocated_size);
                *missed = nmissed;
            }
        }
#ifndef PW_SILENT        
        if (verbose>=level) 
            printf("\n");
#endif
    }
    
    arb_clear(radius);
    arb_clear(abs);
    arb_clear(one);
    acb_clear(center);
    
    return nbdiscs;
}

#ifdef PWPOLY_HAS_BEFFT
slong _pwpoly_isolate_in_nk_th_disc_double( slong *resPellet, acb_ptr roots, const pw_approximation_t app, ulong n, ulong k,
                                            int solver PW_VERBOSE_ARGU(verbose) ){

#ifndef PW_SILENT    
    int level = 3;
#endif
    slong res = 1;
    ulong N=pw_approximation_Nref(app);
    ulong K = pw_approximation_nbsectref(app)[n];
    slong prec = pw_approximation_precref(app);
    ulong m=pw_approximation_output__mref(app);
    
    acb_poly_t polyMonic;
    acb_poly_init(polyMonic);
    /* get approximation, and multiply it by a scalar so that the exponent of abs of the leading coeff is 1 */
    acb_srcptr coeffs;
    ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, k );
    _pwpoly_middle_almost_monic_acb_poly( polyMonic, coeffs, lenapprox, prec );
    /* try to convert acb poly in be poly */
    double * poly    = (double *) pwpoly_malloc ( 3*lenapprox*sizeof(double) );
    double * polyDLG = (double *) pwpoly_malloc ( 3*lenapprox*sizeof(double) );
#ifdef PW_PROFILE
    clock_t start_conv = clock();
    nb_dbl_conv++;
#endif
    res = pwpoly_acb_poly_to_be_poly( poly, polyMonic->coeffs, lenapprox PW_VERBOSE_CALL(verbose)  );
#ifdef PW_PROFILE
    clicks_in_conv_dbl += (clock() - start_conv);
    if (res==1)
        nb_succ_dbl_conv++;
    else
        nb_exce_dbl_conv++;
#endif    
    if (res!=1) {
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pwpoly_isolate_in_nk_th_disc_double: n=%lu, k=%lu, converting acb poly to be poly failed: %ld\n", n, k, res);
#endif
        *resPellet = TSTAR_CAN_NOT_DECIDE;
        pwpoly_free(polyDLG);
        pwpoly_free(poly);
        acb_poly_clear(polyMonic);
        return res;
    }
    
    for (ulong j=0; j<lenapprox; j++) {
        polyDLG[3*j]   = poly[3*j];
        polyDLG[3*j+1] = poly[3*j+1];
        polyDLG[3*j+2] = poly[3*j+2];
    }
    
    *resPellet = TSTAR_CAN_NOT_DECIDE;
    int NG = 0;
    if ((n>0) && (n<N-1) && (K>1)) {
        /* if the current annulus is divided in sectors, apply Pellet test to see if there are roots */
        *resPellet = _pw_solving_tstar_unit_disc_be( polyDLG, lenapprox, &NG, -1, lenapprox PW_VERBOSE_CALL(verbose) );
#ifndef PW_SILENT
        if (verbose>=level) { 
            if (*resPellet<=PWPOLY_DOUBLE_EXCEPTION)
                printf("_pwpoly_isolate_in_nk_th_disc_double: n=%lu, k=%lu, double exception while computing tstar!!!\n", n, k);
            else 
                printf("_pwpoly_isolate_in_nk_th_disc_double: n=%lu, k=%lu, res of tstar test in double: %ld, nb of DLG iterations: %d\n", n, k, *resPellet, NG);
        }
#endif
#ifdef PW_PROFILE
        nb_bepellet_tests++;
        if (*resPellet<=PWPOLY_DOUBLE_EXCEPTION) {
            nb_exce_bepellet_tests++;
        }
        if (*resPellet>PWPOLY_DOUBLE_EXCEPTION) {
            nb_succ_bepellet_tests++;
            if (*resPellet!=TSTAR_NO_ROOT)
                nb_som_root+=1;
            if (*resPellet<TSTAR_NO_ROOT)
                nb_und_root+=1;
            if (*resPellet==1)
                nb_one_root+=1;
            if (*resPellet==2)
                nb_two_root+=1;
            if (*resPellet==3)
                nb_thr_root+=1;
        }
#endif
    }
    if (*resPellet == TSTAR_NO_ROOT ) {
        res = TSTAR_NO_ROOT;
    } else {
        res = lenapprox-1;
        int resSolverFewRoots = 0;
        
        if ( PW_SOLVING_USE_FEWROOT(solver) && ( ( *resPellet == 1 )||( *resPellet == 2 )||( *resPellet == 3 ) ) ) {
//         if ( PW_SOLVING_USE_FEWROOT(solver) && ( ( *resPellet == 1 )||( *resPellet == 2 ) ) ) {
#ifdef PW_PROFILE
            clock_t start_fewRoot = clock();
#endif            
            resSolverFewRoots = pw_solver_123_roots_be( roots, *resPellet, -(slong)m, poly, lenapprox PW_VERBOSE_CALL(verbose)  );
#ifdef PW_PROFILE
            clicks_in_fewRoots_be[(*resPellet)-1]  += (clock() - start_fewRoot);
            nb_call_fewRoots_be[(*resPellet)-1]++;
            nb_succ_fewRoots_be[(*resPellet)-1]+=resSolverFewRoots;
#endif      
            if (resSolverFewRoots==1)
                res = *resPellet;
        }
        
        if (resSolverFewRoots == 0) {
            res = -1;
#ifdef PWPOLY_HAS_EAROOTS
            /* solve with earoots */
            if (PW_SOLVING_USE_EAROOTS(solver)) {
                res = _pw_solving_solve_in_disc_earoots( roots, poly, polyMonic->coeffs, lenapprox, (slong)m, (K>1), prec PW_VERBOSE_CALL(verbose)  );
            } 
            else {
                res = -1;
            }
#endif 
            
        }
    }
    
    pwpoly_free(polyDLG);
    pwpoly_free(poly);
    acb_poly_clear(polyMonic);
    
    return res;
}
#endif

slong _pwpoly_isolate_in_nk_th_disc_multiprec( slong * resPellet, acb_ptr roots, const pw_approximation_t app, ulong n, ulong k, 
                                               int solver PW_VERBOSE_ARGU(verbose) ){
#ifndef PW_SILENT    
    int level = 3;
#endif    
    slong res = -1;
    
    ulong N=pw_approximation_Nref(app);
    ulong K = pw_approximation_nbsectref(app)[n];
    slong prec = pw_approximation_precref(app);
    ulong m=pw_approximation_output__mref(app);
    
    acb_poly_t poly;
    acb_poly_init(poly);
    /* get approximation */
    acb_srcptr coeffs;
    ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, k );
    _pwpoly_middle_acb_poly( poly, coeffs, lenapprox );
    
    if ((n>0) && (n<N-1) && (K>1)) {
        if (*resPellet<=PWPOLY_DOUBLE_EXCEPTION) { /* resPellet failed in double or has not been run */
            acb_poly_t polyDLG;
            acb_poly_init(polyDLG);
            acb_poly_set(polyDLG, poly);
            int NG=0;
            slong lprec = PWPOLY_MAX( PWPOLY_DEFAULT_PREC, prec/4);
            *resPellet = _pwpoly_tstar_unit_disc_fixed_prec( polyDLG, &NG, -1, lenapprox, lprec);
#ifndef PW_SILENT
            if (verbose>=level)
                printf("_pwpoly_isolate_in_nk_th_disc_multiprec: n=%lu, k=%lu, res of tstar test: %ld, nb of DLG iterations: %d\n", n, k, *resPellet, NG);
#endif
            acb_poly_clear(polyDLG);
#ifdef PW_PROFILE
            nb_arbpellet_tests++;
            if (*resPellet!=TSTAR_NO_ROOT)
                nb_som_root+=1;
            if (*resPellet<TSTAR_NO_ROOT)
                nb_und_root+=1;
            if (*resPellet==1)
                nb_one_root+=1;
            if (*resPellet==2)
                nb_two_root+=1;
            if (*resPellet==3)
                nb_thr_root+=1;
#endif
        }        
    }

    if (*resPellet == TSTAR_NO_ROOT ) {
        res = TSTAR_NO_ROOT;
    } else {
        res = lenapprox-1;
        int resSolverFewRoots = 0;
        
        if ( PW_SOLVING_USE_FEWROOT(solver) && ( ( *resPellet == 1 )||( *resPellet == 2 )||( *resPellet == 3 ) ) ) {
//         if ( PW_SOLVING_USE_FEWROOT(solver) && ( ( *resPellet == 1 )||( *resPellet == 2 ) ) ) {
#ifdef PW_PROFILE
            clock_t start_fewRoot = clock();
#endif            
            resSolverFewRoots = pw_solver_123_roots( roots, *resPellet, -(slong)m, poly->coeffs, lenapprox, prec PW_VERBOSE_CALL(verbose)  );
#ifdef PW_PROFILE
            clicks_in_fewRoots[(*resPellet)-1]  += (clock() - start_fewRoot);
            nb_call_fewRoots[(*resPellet)-1]++;
            nb_succ_fewRoots[(*resPellet)-1]+=resSolverFewRoots;
#endif      
            if (resSolverFewRoots==1)
                res = *resPellet;
        }
        /* solve */
        if (resSolverFewRoots == 0) {
            res = _pw_solving_solve_in_disc_CoCo( roots, (slong)m, poly->coeffs, lenapprox, app, n, prec, *resPellet PW_VERBOSE_CALL(verbose)  );
        }
    }
    
    acb_poly_clear(poly);
    
    return res;
}
  
/* assume roots has enough room for 2*(lenapprox-1) new roots       */
/* where lenapprox is the length of the approx in the n,k-th sector */
ulong _pwpoly_isolate_m_app_sector( root_ptr roots, acb_ptr *missed, ulong *nbmissed, 
                                    ulong n, ulong k, pw_approximation_t app, int solver PW_VERBOSE_ARGU(verbose) ){
    
    ulong N=pw_approximation_Nref(app);
    ulong K=pw_approximation_nbsectref(app)[n];
    
    /* get the length of approximation */
    acb_srcptr coeffs;
    ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, k );
    
    /* initialize resPellet */
    slong resPellet;
    if ((n>0) && (n<N-1) && (K>1))
        resPellet = PWPOLY_DOUBLE_EXCEPTION;
    else
        resPellet = TSTAR_CAN_NOT_DECIDE;
            
    acb_ptr local_roots = _acb_vec_init(lenapprox-1);
    
    /* solve in the disk containing the sector */
    slong nbRoots = -1;
#ifdef PWPOLY_HAS_BEFFT
    /* try to solve with doubles */
    if ( PW_SOLVING_USE_DOUBLES(solver) ) 
        nbRoots = _pwpoly_isolate_in_nk_th_disc_double( &resPellet, local_roots, app, n, k, solver PW_VERBOSE_CALL(verbose)  );
#endif
    if (nbRoots < 0) /* solving with doubles failed -> solve in multiprec */
        nbRoots = _pwpoly_isolate_in_nk_th_disc_multiprec( &resPellet, local_roots, app, n, k, solver PW_VERBOSE_CALL(verbose)  );
    
    ulong nbIsolatedIn_nk = 0;
    if (nbRoots > 0) {
        nbIsolatedIn_nk = _pw_solving_get_roots_intersecting_nk_sector( roots, missed, nbmissed, app, local_roots, nbRoots, n, k PW_VERBOSE_CALL(verbose)  );
    }
    
    /* Free the allocated memory */
    _acb_vec_clear(local_roots, lenapprox-1);
    
    return nbIsolatedIn_nk;
}

/* assume roots has enough room for degree + 2*(lenapprox-1) roots */
/* where lenapprox is the max length of approxs in the sectors     */
ulong _pwpoly_isolate_m_app_global( root_ptr roots, ulong nbAlreadyIsolated, acb_ptr *missed, ulong *nbmissed, 
                                    pw_approximation_t app, 
                                    slong goalNbRoots,
                                    int solver PW_VERBOSE_ARGU(verbose) ){
    
#ifndef PW_SILENT
    int level = 2;
#endif
#ifdef PW_PROFILE
    clock_t start_solve = clock();
    nb_sectors_in_covering+=pw_approximation_Ksumref(app);
#endif
    
    slong nbIsolated = nbAlreadyIsolated;
    
    ulong N=pw_approximation_Nref(app);
    slong prec = pw_approximation_precref(app);
//     ulong lenInitial=pw_approximation_lenref(app);
    int realCoeffs = pw_approximation_realCoeffsref(app);
     
    /* assume N>=3 */
    ulong first_n=1, last_n=N-2;
        
    pw_approximation_compute_approx_annulus( app, first_n-1, 1 );
    pw_approximation_compute_approx_annulus( app, first_n, 1 );
        
    /* loop on annulii */
    /* first and last annuli do not contain roots! */
    for (ulong n=first_n; (n<= last_n)&&(nbIsolated<goalNbRoots); n++) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif
                
        /* compute approximations in next annulus */
        if ((n+1)<=(last_n+1))
            pw_approximation_compute_approx_annulus( app, n+1, 1 );
        /* purge approximations in (n-2)-th annulus */
        if (((slong)n-2)>=((slong)first_n-1))
            pw_approximation_clear_approx_annulus( app, n-2 );
        
        ulong K = pw_approximation_nbsectref(app)[n];
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_isolate_m_app_global, m=%lu: solving on %lu-th annulus: %lu sectors, %lu roots so far, approxs of degree %lu\n", 
                   pw_approximation_output__mref(app), n, K, nbIsolated, pw_approximation_approx_lengthsref(app)[n]);
        }
#endif
        
        /* loop on sectors */
        ulong lastk = K-1;
        if ( realCoeffs && (K>1) )
            lastk = K/2;
        for (ulong k=0; (k<= lastk)&&(nbIsolated<goalNbRoots); k++) {
            
#ifdef PW_CHECK_INTERRUPT
            if (pwpoly_interrupt(0))
                break;
#endif
            
            /* solve only if it has not already been solved */
            if (pw_approximation_already_solvedref(app)[n][k]==0) {

#ifdef PW_PROFILE
                nb_sector_solved++;
#endif                
                pw_approximation_already_solvedref(app)[n][k]=1;
                
                ulong nbIsolatedIn_nk = _pwpoly_isolate_m_app_sector( roots+nbIsolated, missed, nbmissed, n, k, app, solver PW_VERBOSE_CALL(verbose)  );
                /* if the initial polynomial has real coefficients, also push the complex conjugated */
                if ( realCoeffs && (K>1) && (k>0) && (k<lastk) && (nbIsolatedIn_nk>0) ) {
                    ulong begin=nbIsolated;
                    ulong end = nbIsolated+nbIsolatedIn_nk;
                    for ( ulong i = 0; i<nbIsolatedIn_nk; i++ )
                        root_conj( roots + (end + i), roots + (begin + i) );
                    nbIsolatedIn_nk*=2;
                }          
                nbIsolated+=nbIsolatedIn_nk;
            
                /* if the number of isolated roots is >= goalNbRoots or if it is the last sector, */
                /*    eliminate doubles in roots to check for termination                    */
#ifdef PW_CHECK_INTERRUPT
                if (!pwpoly_interrupt(0))
#endif
                if ( (nbIsolated>=goalNbRoots) || ((n==last_n)&&(k==lastk)) ) {
#ifndef PW_SILENT
                    if (verbose>=level) {
                        printf("_pwpoly_isolate_m_app_global, m=%lu: n:%lu/%lu, k:%lu/%lu, number of isolated roots: %lu, goalNbRoots: %ld \n", pw_approximation_output__mref(app), n, N, k, K, nbIsolated, goalNbRoots);
                        printf("_pwpoly_isolate_m_app_global, m=%lu: eliminate doubles \n", pw_approximation_output__mref(app));
                    }
#endif
#ifdef PW_PROFILE
                    clock_t start_merge = clock();
#endif
                    nbIsolated = _pw_roots_merge_overlaps(roots, nbIsolated, prec);
#ifdef PW_PROFILE
                    clicks_in_merge += (clock() - start_merge);
#endif  
#ifndef PW_SILENT
                    if (verbose>=level) {
                        printf("_pwpoly_isolate_m_app_global, m=%lu: %lu distinct isolated roots after eliminate doubles, n:%lu, k:%lu \n", pw_approximation_output__mref(app), nbIsolated, n, k);
                        printf("_pwpoly_isolate_m_app_global, m=%lu: %lu other candidates for roots\n", pw_approximation_output__mref(app), *nbmissed);
                    }
#endif
                }
            }
        }
        
    }

#ifdef PW_PROFILE
    clicks_in_solve_approx += (clock() - start_solve);
#endif
    
    return nbIsolated;
    
}

/* assume roots has enough room for degree + 2*(lenapprox-1) roots */
/* where lenapprox is the max length of approxs in the sectors     */
/* assume domain is not C */
ulong _pwpoly_isolate_m_app_domain( root_ptr roots, ulong nbAlreadyIsolated, ulong * nbAlreadyKeep,
                                    acb_ptr *missed, ulong *nbmissed, 
                                    pw_approximation_t app, 
                                    int goal, slong log2eps, const domain_t dom, slong goalNbRoots,
                                    int solver PW_VERBOSE_ARGU(verbose) ){
    
#ifndef PW_SILENT
    int level = 2;
#endif
#ifdef PW_PROFILE
    clock_t start_solve = clock();
    nb_sectors_in_covering+=pw_approximation_Ksumref(app);
#endif
    
    slong nbIsolated = nbAlreadyIsolated;
    slong nbKeep     = (*nbAlreadyKeep); 
    /* the number of roots already in roots that will be kept for sure:     */
    /* either they are strictly in the domain, or they intersect the domain */
    /* and have appropriated size */
    ulong N=pw_approximation_Nref(app);
    slong prec = pw_approximation_precref(app);
    slong deg=pw_approximation_lenref(app)-1;
    int realCoeffs = pw_approximation_realCoeffsref(app);
    realCoeffs = realCoeffs && domain_is_symetric_real_line(dom);
     
    /* assume N>=3 */
    ulong first_n=1, last_n=N-2;
    pw_covering_get_first_last_annuli_domain( &first_n, &last_n, pw_approximation_coveringref(app), dom, prec );
#ifndef PW_SILENT
    if (verbose>=level) {
        printf("_pwpoly_isolate_m_app_domain, m=%lu: first annulus: %lu, last annulus: %lu/%lu\n", 
                pw_approximation_output__mref(app), first_n, last_n, N-1);
    }
#endif
        
    if (first_n <= last_n) {
        pw_approximation_compute_approx_annulus( app, first_n-1, 1 );
        pw_approximation_compute_approx_annulus( app, first_n, 1 );
    }
        
    /* loop on annulii */
    /* first and last annuli do not contain roots! */
    for (ulong n=first_n; (n<= last_n)&&(nbKeep<goalNbRoots)&&(nbIsolated<deg); n++) {
     
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif
        
        /* compute approximations in next annulus */
        if ((n+1)<=(last_n+1))
            pw_approximation_compute_approx_annulus( app, n+1, 1 );
        /* purge approximations in (n-2)-th annulus */
        if (((slong)n-2)>=((slong)first_n-1))
            pw_approximation_clear_approx_annulus( app, n-2 );
        
        ulong K = pw_approximation_nbsectref(app)[n];
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_isolate_m_app_domain, m=%lu: solving on %lu-th annulus: %lu sectors, %lu roots so far, approxs of degree %lu\n", 
                   pw_approximation_output__mref(app), n, K, nbIsolated, pw_approximation_approx_lengthsref(app)[n]);
        }
#endif
        
        /* loop on sectors */
        ulong lastk = K-1;
        if ( realCoeffs && (K>1) )
            lastk = K/2;
        for (ulong k=0; (k<= lastk)&&(nbKeep<goalNbRoots)&&(nbIsolated<deg); k++) {
            
#ifdef PW_CHECK_INTERRUPT
            if (pwpoly_interrupt(0))
                break;
#endif
            
            /* check if disk containing the (n,k)-th sector intersects the domain */
            if ( (K>1) && (pw_covering_disk_domain_empty_intersection( pw_approximation_coveringref(app), n, k, dom, prec )) )
                continue;
            
            /* solve only if it has not already been solved */
            if (pw_approximation_already_solvedref(app)[n][k]==0) {
#ifdef PW_PROFILE
                nb_sector_solved++;
#endif                
                pw_approximation_already_solvedref(app)[n][k]=1;
                
                ulong nbIsolatedIn_nk = _pwpoly_isolate_m_app_sector( roots+nbIsolated, missed, nbmissed, n, k, app, solver PW_VERBOSE_CALL(verbose)  );
                if (nbIsolatedIn_nk>0){
//                     printf("nbIsolated: %lu, nbIsolatedIn_nk: %lu\n", nbIsolated, nbIsolatedIn_nk);
                    /* keep only the roots intersecting the domain */
                    ulong begin=nbIsolated;
                    ulong end = nbIsolated+nbIsolatedIn_nk;
                    while (begin < end) {
                        if (domain_acb_intersect_domain(dom, root_enclosureref(roots+begin)))
                            begin++;
                        else {
                            root_swap( roots+begin, roots+(end-1) );
                            end--;
                        }
                    }
                    nbIsolatedIn_nk = begin-nbIsolated;
                }
                /* if the initial polynomial has real coefficients, also push the complex conjugated */
                if ( realCoeffs && (K>1) && (k>0) && (k<lastk) && (nbIsolatedIn_nk>0) ) {
                    ulong begin=nbIsolated;
                    ulong end = nbIsolated+nbIsolatedIn_nk;
                    for ( ulong i = 0; i<nbIsolatedIn_nk; i++ )
                        root_conj( roots + (end + i), roots + (begin + i) );
                    nbIsolatedIn_nk*=2;
                }
                
                /* actualize nbKeep */
                ulong nbKeepIn_nk=0;
                for ( ulong i = 0; i<nbIsolatedIn_nk; i++ ) {
                    if ( domain_acb_is_strictly_in_domain( dom, root_enclosureref(roots+(nbIsolated+i)) ) )
                        nbKeepIn_nk++;
                    else if ( (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                           || pwpoly_width_less_eps( root_enclosureref(roots+(nbIsolated+i)), log2eps, PW_GOAL_PREC_RELATIVE(goal) ) )
                            nbKeepIn_nk++;
                }
                nbKeep+=nbKeepIn_nk;
                /* actualize nbIsolated */
                nbIsolated+=nbIsolatedIn_nk;
                
            
                /* if the number of isolated roots is >= goalNbRoots or if it is the last sector, */
                /*    eliminate doubles in roots to check for termination                    */
#ifdef PW_CHECK_INTERRUPT
                if (!pwpoly_interrupt(0))
#endif
                if ( (nbKeep>=goalNbRoots) || (nbIsolated>=deg) || ((n==last_n)&&(k==lastk)) ) {
#ifndef PW_SILENT
                    if (verbose>=level) {
                        printf("_pwpoly_isolate_m_app_domain, m=%lu: n:%lu/%lu, k:%lu/%lu\n",
                               pw_approximation_output__mref(app), n, N, k, K);
                        printf("                                     number of isolated roots: %lu/%lu\n", nbIsolated, deg);
                        printf("                                     number of surely   roots: %lu/%lu\n", nbKeep, goalNbRoots);
                        printf("                                     eliminate doubles \n");
                    }
#endif
#ifdef PW_PROFILE
                    clock_t start_merge = clock();
#endif
                    nbIsolated = _pw_roots_merge_overlaps(roots, nbIsolated, prec);
#ifdef PW_PROFILE
                    clicks_in_merge += (clock() - start_merge);
#endif  
                    /* actualize nbKeep */
                    nbKeep=0;
                    for ( slong i = 0; i<nbIsolated; i++ ) {
                        if ( domain_acb_is_strictly_in_domain( dom, root_enclosureref(roots+i) ) )
                            nbKeep++;
                        else if ( (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                           || pwpoly_width_less_eps( root_enclosureref(roots+i), log2eps, PW_GOAL_PREC_RELATIVE(goal) ) )
                            nbKeep++;
                    }
#ifndef PW_SILENT
                    if (verbose>=level) {
                        printf("_pwpoly_isolate_m_app_domain, m=%lu: after eliminate doubles\n", pw_approximation_output__mref(app));
                        printf("                                     number of isolated roots: %lu/%lu\n", nbIsolated, deg);
                        printf("                                     number of surely   roots: %lu/%lu\n", nbKeep, goalNbRoots);
                        printf("                                     eliminate doubles \n");
                    }
#endif
                }
            }
        }
        
    }

#ifdef PW_PROFILE
    clicks_in_solve_approx += (clock() - start_solve);
#endif
    
    *nbAlreadyKeep = nbKeep;
    
    return nbIsolated;
    
}

/* assume roots has enough room for degree + 2*(lenapprox-1) roots */
/* where lenapprox is the max length of approxs in the sectors     */
ulong _pwpoly_isolate_m_app_in_sectors_global( root_ptr roots, ulong nbAlreadyIsolated, 
                                               acb_ptr *missed, ulong *nbmissed, 
                                               pw_approximation_t app, pw_sector_list_t sectors, 
                                               slong goalNbRoots,
                                               int solver PW_VERBOSE_ARGU(verbose) ){

#ifndef PW_SILENT    
    int level = 2;
#endif
    
#ifdef PW_PROFILE
    clock_t start_solve = clock();
#endif
    
    slong nbIsolated = nbAlreadyIsolated;
    ulong N=pw_approximation_Nref(app);
    slong prec = pw_approximation_precref(app);
    
    int realCoeffs = pw_approximation_realCoeffsref(app);
    
    pw_sector_list_iterator sector = pw_sector_list_begin(sectors);
    while ( (sector != pw_sector_list_end()) && (nbIsolated<goalNbRoots) ) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif
        
        ulong n = pw_sector_nref(pw_sector_list_elmt(sector));
        ulong k = pw_sector_kref(pw_sector_list_elmt(sector));
        
        /* solve only if it has not already been solved */
        if (pw_approximation_already_solvedref(app)[n][k]==0) {
        
            pw_approximation_already_solvedref(app)[n][k] = 1;
            
            /* compute approximations in n-1, n and n+1-th annulii */
            if (n>0)
                pw_approximation_compute_approx_annulus( app, n-1, 1 );
            pw_approximation_compute_approx_annulus( app, n, 1 );
            if (n<N-1)
                pw_approximation_compute_approx_annulus( app, n+1, 1 );
        
            /* find roots in n,k-th sector */
            ulong nbIsolatedIn_nk = _pwpoly_isolate_m_app_sector( roots+nbIsolated, missed, nbmissed, n, k, app, solver PW_VERBOSE_CALL(verbose)  );
 
            /* if the initial polynomial has real coefficients, also push the complex conjugated */
            ulong K = pw_approximation_nbsectref(app)[n];
            ulong lastk = (realCoeffs? K/2 : K-1);
            if ( realCoeffs && (K>1) && (k>0) && (k<lastk) && (nbIsolatedIn_nk>0) ) {
                ulong begin=nbIsolated;
                ulong end = nbIsolated+nbIsolatedIn_nk;
                for ( ulong i = 0; i<nbIsolatedIn_nk; i++ )
                    root_conj( roots + (end + i), roots + (begin + i) );
                nbIsolatedIn_nk*=2;
            }
            
            nbIsolated+=nbIsolatedIn_nk;
        
            /* if the number of isolated roots is >= degree or if it is the last sector, */
            /*    eliminate doubles in roots to check for termination                    */
            if ( (nbIsolated>=goalNbRoots) || ( pw_sector_list_next(sector)==pw_sector_list_end() ) ) {
#ifndef PW_SILENT
                if (verbose>=level) {
                    ulong m=pw_approximation_output__mref(app);
                    printf("_pwpoly_isolate_m_app_in_sectors_global, m=%lu: number of isolated roots: %lu, goalNbRoots: %ld \n", m, nbIsolated, goalNbRoots);
                    printf("_pwpoly_isolate_m_app_in_sectors_global, m=%lu: eliminate doubles \n", m);
                }
#endif
#ifdef PW_PROFILE
                clock_t start_merge = clock();
#endif
                nbIsolated = _pw_roots_merge_overlaps(roots, nbIsolated, prec);
#ifdef PW_PROFILE
                clicks_in_merge += (clock() - start_merge);
#endif  
#ifndef PW_SILENT
                if (verbose>=level) {
                    ulong m=pw_approximation_output__mref(app);
                    printf("_pwpoly_isolate_m_app_in_sectors_global, m=%lu: %lu distinct isolated roots after eliminate doubles, n:%lu, k:%lu \n", m, nbIsolated, n, k);
                    printf("_pwpoly_isolate_m_app_in_sectors_global, m=%lu: %lu other candidates for roots\n", m, *nbmissed);
                }
#endif
            }
        }
        sector=pw_sector_list_next(sector);
    }

#ifdef PW_PROFILE
    clicks_in_solve_approx += (clock() - start_solve);
#endif
    
    return nbIsolated;
}

/* assume roots has enough room for degree + 2*(lenapprox-1) roots */
/* where lenapprox is the max length of approxs in the sectors     */
ulong _pwpoly_isolate_m_app_in_sectors_domain( root_ptr roots, ulong nbAlreadyIsolated, ulong * nbAlreadyKeep, 
                                               acb_ptr *missed, ulong *nbmissed, 
                                               pw_approximation_t app, pw_sector_list_t sectors, 
                                               int goal, slong log2eps, const domain_t dom, slong goalNbRoots,
                                               int solver PW_VERBOSE_ARGU(verbose) ){

#ifndef PW_SILENT    
    int level = 2;
#endif
    
#ifdef PW_PROFILE
    clock_t start_solve = clock();
#endif
    
    slong nbIsolated = nbAlreadyIsolated;
    slong nbKeep     = (*nbAlreadyKeep); 
    /* the number of roots already in roots that will be kept for sure:     */
    /* either they are strictly in the domain, or they intersect the domain */
    /* and have appropriated size */
    ulong N=pw_approximation_Nref(app);
    slong prec = pw_approximation_precref(app);
    slong deg=pw_approximation_lenref(app)-1;
    int realCoeffs = pw_approximation_realCoeffsref(app);
    realCoeffs = realCoeffs && domain_is_symetric_real_line(dom);
    
    pw_sector_list_iterator sector = pw_sector_list_begin(sectors);
    while ( (sector != pw_sector_list_end())&&(nbKeep<goalNbRoots)&&(nbIsolated<deg) ) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif
        
        ulong n = pw_sector_nref(pw_sector_list_elmt(sector));
        ulong k = pw_sector_kref(pw_sector_list_elmt(sector));
        ulong K = pw_approximation_nbsectref(app)[n];
        ulong lastk = (realCoeffs? K/2 : K-1);
        
        /* check if disk containing the (n,k)-th sector intersects the domain */
        if ( (K>1) && (pw_covering_disk_domain_empty_intersection( pw_approximation_coveringref(app), n, k, dom, prec )) )
            continue;
        
        /* solve only if it has not already been solved */
        if (pw_approximation_already_solvedref(app)[n][k]==0) {
#ifdef PW_PROFILE
            nb_sector_solved++;
#endif
            pw_approximation_already_solvedref(app)[n][k] = 1;
            
            /* compute approximations in n-1, n and n+1-th annulii */
            if (n>0)
                pw_approximation_compute_approx_annulus( app, n-1, 1 );
            pw_approximation_compute_approx_annulus( app, n, 1 );
            if (n<N-1)
                pw_approximation_compute_approx_annulus( app, n+1, 1 );
        
            /* find roots in n,k-th sector */
            ulong nbIsolatedIn_nk = _pwpoly_isolate_m_app_sector( roots+nbIsolated, missed, nbmissed, n, k, app, solver PW_VERBOSE_CALL(verbose)  );
 
            if (nbIsolatedIn_nk>0){
//                 printf("nbIsolated: %lu, nbIsolatedIn_nk: %lu\n", nbIsolated, nbIsolatedIn_nk);
                /* keep only the roots intersecting the domain */
                ulong begin=nbIsolated;
                ulong end = nbIsolated+nbIsolatedIn_nk;
                while (begin < end) {
                    if (domain_acb_intersect_domain(dom, root_enclosureref(roots+begin)))
                        begin++;
                    else {
                        root_swap( roots+begin, roots+(end-1) );
                        end--;
                    }
                }
                nbIsolatedIn_nk = begin-nbIsolated;
            }
            /* if the initial polynomial has real coefficients, also push the complex conjugated */
            if ( realCoeffs && (K>1) && (k>0) && (k<lastk) && (nbIsolatedIn_nk>0) ) {
                ulong begin=nbIsolated;
                ulong end = nbIsolated+nbIsolatedIn_nk;
                for ( ulong i = 0; i<nbIsolatedIn_nk; i++ )
                    root_conj( roots + (end + i), roots + (begin + i) );
                nbIsolatedIn_nk*=2;
            }
            
            /* actualize nbKeep */
            ulong nbKeepIn_nk=0;
            for ( ulong i = 0; i<nbIsolatedIn_nk; i++ ) {
                if ( domain_acb_is_strictly_in_domain( dom, root_enclosureref(roots+(nbIsolated+i)) ) )
                    nbKeepIn_nk++;
                else if ( (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                    || pwpoly_width_less_eps( root_enclosureref(roots+(nbIsolated+i)), log2eps, PW_GOAL_PREC_RELATIVE(goal) ) )
                        nbKeepIn_nk++;
            }
            nbKeep+=nbKeepIn_nk;
            /* actualize nbIsolated */
            nbIsolated+=nbIsolatedIn_nk;
        
            /* if the number of isolated roots is >= degree or if it is the last sector, */
            /*    eliminate doubles in roots to check for termination                    */
            if ( (nbKeep>=goalNbRoots) || (nbIsolated>=deg) || ( pw_sector_list_next(sector)==pw_sector_list_end() ) ) {
#ifndef PW_SILENT
                if (verbose>=level) {
                    printf("_pwpoly_isolate_m_app_in_sectors_domain, m=%lu: n:%lu/%lu, k:%lu/%lu\n",
                               pw_approximation_output__mref(app), n, N, k, K);
                    printf("                                                number of isolated roots: %lu/%lu\n", nbIsolated, deg);
                    printf("                                                number of surely   roots: %lu/%lu\n", nbKeep, goalNbRoots);
                    printf("                                                eliminate doubles \n");
                }
#endif
#ifdef PW_PROFILE
                clock_t start_merge = clock();
#endif
                nbIsolated = _pw_roots_merge_overlaps(roots, nbIsolated, prec);
#ifdef PW_PROFILE
                clicks_in_merge += (clock() - start_merge);
#endif  
                /* actualize nbKeep */
                nbKeep=0;
                for ( slong i = 0; i<nbIsolated; i++ ) {
                    if ( domain_acb_is_strictly_in_domain( dom, root_enclosureref(roots+i) ) )
                        nbKeep++;
                    else if ( (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                       || pwpoly_width_less_eps( root_enclosureref(roots+i), log2eps, PW_GOAL_PREC_RELATIVE(goal) ) )
                        nbKeep++;
                }
#ifndef PW_SILENT
                if (verbose>=level) {
                    printf("_pwpoly_isolate_m_app_in_sectors_domain, m=%lu: after eliminate doubles\n", 
                            pw_approximation_output__mref(app));
                    printf("                                                number of isolated roots: %lu/%lu\n", nbIsolated, deg);
                    printf("                                                number of surely   roots: %lu/%lu\n", nbKeep, goalNbRoots);
                    printf("                                                eliminate doubles \n");
                }
#endif
            }
        }
        sector=pw_sector_list_next(sector);
    }

#ifdef PW_PROFILE
    clicks_in_solve_approx += (clock() - start_solve);
#endif
    
    return nbIsolated;
}

slong _pwpoly_isolate_m_pw_polynomial( acb_ptr roots, pw_polynomial_t poly, 
                                       const ulong m, const ulong c, const fmpq_t scale, const fmpq_t b,
                                       const domain_t dom, slong goalNbRoots,
                                       int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile)) {
#ifndef PW_SILENT    
    int level = 1; 
#endif
    slong len = pw_polynomial_length(poly);
    /* cap goalNbRoots */
    goalNbRoots = PW_CAP_GOALNBROOTS(goalNbRoots,(len-1));
    
    /* remove root zero and its multiplicity */
    slong multOfZero = pw_polynomial_remove_root_zero(poly);
    slong nlen = pw_polynomial_length(poly);
    slong offsetInRoots = 0;
    if ( (multOfZero>0) && (domain_contains_zero(dom)) ) {
        acb_zero( roots + 0 );
        offsetInRoots = 1;
        goalNbRoots = PWPOLY_MAX( 0, goalNbRoots - multOfZero );
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_isolate_m_pw_polynomial: 0 is a root of multiplicity %ld in domain, %ld roots still to find \n", multOfZero, goalNbRoots );
        }
#endif
    }
    goalNbRoots = PW_CAP_GOALNBROOTS(goalNbRoots,(nlen-1));
    
    if (goalNbRoots==0) {
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_isolate_m_pw_polynomial: goalNbRoots is 0: return \n" );
        }
#endif
        return offsetInRoots;
    }
    
    pw_approximation_t app;
    pw_approximation_init_pw_polynomial( app, poly, m, c, scale, b);
    
    acb_ptr missed = _acb_vec_init( nlen );
    ulong nbmissed = 0;
    
/*#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("pwpoly_isolate_m_pw_polynomial: m: %lu\n", m);
        printf("pwpoly_isolate_m_pw_polynomial: approximation: \n");
        pw_approximation_print_short( app );       
    }
#endif */   
    ulong cm   = pw_approximation_cref(app)*pw_approximation_working_mref(app);
    ulong allocsize = nlen + 2*cm;
    root_ptr allroots = root_vec_init(allocsize);
    ulong nbroots = 0;
    if (domain_is_C(dom))
        nbroots = _pwpoly_isolate_m_app_global( allroots, 0, &missed, &nbmissed, app, goalNbRoots, solver PW_VERBOSE_CALL(verbose)  );
    else {
        ulong nbAlreadyKeep = 0;
        int goal = PW_GOAL_ISOLATE;
        slong log2eps = 0;
        nbroots = _pwpoly_isolate_m_app_domain( allroots, 0, &nbAlreadyKeep, &missed, &nbmissed, app, 
                                                goal, log2eps, dom, goalNbRoots, solver PW_VERBOSE_CALL(verbose)  );
    }
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("pwpoly_isolate_m_pw_polynomial: m: %lu, %lu isolated roots, %lu other candidates\n", m, nbroots, nbmissed);
    }
#endif    
    int realCoeffs = pw_approximation_realCoeffsref(app);
    for (ulong i=0; i<nbroots; i++) {
        acb_set( roots+offsetInRoots+i, root_enclosureref( allroots+i ) );
        /* since all enclosures in roots are natural isolator */
        /* if coeffs are real, enclosure contains real axis => the root in the enclosure is real */
        /* in this case, set imaginary part to zero */
        if ( realCoeffs && ( arb_contains_zero( acb_imagref(roots+offsetInRoots+i) ) ) )
            arb_zero( acb_imagref(roots+offsetInRoots+i) );
    }
    
    root_vec_clear( allroots, allocsize );
    
    nbroots += (ulong)offsetInRoots;
    
#ifndef PW_NO_INTERFACE
    if (covrFile != NULL) {
//         _pw_covering_with_points2_gnuplot( covrFile, pw_approximation_coveringref(app), roots, (slong)nbroots, missed, nbmissed );
        if (domain_is_C(dom))
            _pw_covering_with_points_gnuplot( covrFile, pw_approximation_coveringref(app), roots, (slong)nbroots );
        else
            _pw_covering_with_domain_gnuplot( covrFile, pw_approximation_coveringref(app), dom );
    }
#endif
    
    ulong missed_size = nlen*(nbmissed/nlen + 1);
//     printf("nbmissed: %lu, missed_size: %lu\n", nbmissed, missed_size);
    _acb_vec_clear( missed, missed_size );
    
    pw_approximation_clear(app);
    
#ifdef PW_PROFILE
    clicks_in_solve_approx -= clicks_in_approx_annulii;
#endif
    
    return nbroots;
}

ulong _pwpoly_isolate_piecewise_get_next_m( ulong m, slong nbroots, slong len, slong max_m, ulong c ) {
    ulong next_m=m;
    uint log2factor = (uint) ceil( log2( ((double)(len-1))/((double)nbroots) ) );
    log2factor = PWPOLY_MAX(log2factor, 1);
    next_m*=2;
    log2factor--;
    while ( (log2factor>0) && ( 2*c*next_m < (ulong)max_m ) ) {
        next_m*=2;
        log2factor--;
    }
    return next_m;
}

ulong _pwpoly_isolate_piecewise_get_next_m_NEW( ulong m, slong nbMissingRoots, slong nbNewRoots) {
    nbNewRoots = PWPOLY_MAX(1, nbNewRoots);
    uint log2factor = (uint) ceil( log2( ((double)(nbMissingRoots))/((double)nbNewRoots) ) );
    log2factor = PWPOLY_MAX(log2factor, 1);
//     ulong next_m = m << log2factor;
    ulong next_m = m;
    while ((log2factor>0) && ( (next_m<<1) > next_m )) {
        next_m = next_m<<1;
        log2factor--;
    }
    return next_m;
}

int _pwpoly_refine_acb_app( acb_ptr nroot, const acb_ptr root, pw_approximation_t app PW_VERBOSE_ARGU(verbose) ) {
#ifndef PW_SILENT    
    int level = 2;
#endif    
    int refined=0;
    slong prec = pw_approximation_precref(app);
    
    arb_t radius;
    arb_init(radius);
    acb_t shifted_root;
    acb_init(shifted_root);
    
    pw_sector_list_t sectors;
    pw_sector_list_init(sectors);
    
    /* locate root in sectors of app */
#ifdef PW_PROFILE
    clock_t start_locate = clock();
#endif
    pw_covering_locate_point( sectors, 0, pw_approximation_coveringref(app), root, prec );
#ifdef PW_PROFILE
    clicks_in_refine_locate += (clock() - start_locate);
#endif
    pw_sector_list_iterator sector = pw_sector_list_begin(sectors);
    
    while ( (sector!=pw_sector_list_end()) && (refined==0) ){
        /* use current sector */
        ulong n = pw_sector_nref(pw_sector_list_elmt(sector));
        ulong k = pw_sector_kref(pw_sector_list_elmt(sector));
    
        /* translate root in the reference of the sector */
        _pw_covering_shift_point( shifted_root, pw_approximation_coveringref(app), n, k, root, prec );
#ifndef PW_SILENT
        if (verbose>=level) {
            ulong N = pw_covering_Nref(pw_approximation_coveringref(app));
            ulong K = pw_covering_nbsectref(pw_approximation_coveringref(app))[n];
            printf("_pwpoly_refine_acb_app: (n/N, k/K): (%lu/%lu, %lu/%lu)\n", n, N, k, K );
            printf("_pwpoly_refine_acb_app: root in global ref:"); acb_printd(root, 10); printf("\n");
            printf("_pwpoly_refine_acb_app: root in local  ref:"); acb_printd(shifted_root, 10); printf("\n");
        }
#endif
        /* compute approximations on n-th annulus */
#ifdef PW_PROFILE
        clock_t start_app = clock();
#endif
        pw_approximation_compute_approx_annulus( app, n, 1 );
#ifdef PW_PROFILE
        clicks_in_refine_compApp += (clock() - start_app);
#endif
        /* get approximation on (n,k)-th sector */
        acb_poly_t poly;
        acb_poly_init(poly);
        /* get approximation */
        acb_srcptr coeffs;
        ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, k );
        _pwpoly_middle_acb_poly( poly, coeffs, lenapprox );
        /* find roots of approximated polynomial in the box with solver_CoCo */
        /* if no root => fail */
        /* otherwise compute isolating discs for roots until it works */
        /* if it does not work, fail */
        acb_ptr nroots = _acb_vec_init( lenapprox-1 );
        slong * mults = (slong *) pwpoly_malloc ( (lenapprox-1)*sizeof(slong) );
        acb_set( nroots + 0, shifted_root );
        mults[0] = 1;
    
        slong outputprec = -pw_approximation_precref(app);
#ifdef PW_PROFILE
        clock_t start_CoCo = clock();
#endif
        slong nbClusts = solver_CoCo_refine_exact_acb_poly( nroots, mults, 1, poly, outputprec, PW_GOAL_APPROXIMATE_ABS, 0 PW_VERBOSE_CALL(verbose)  );
#ifdef PW_PROFILE
        clicks_in_refine_CoCo += (clock() - start_CoCo);
#endif  
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pwpoly_refine_acb_app: nbClusts: %ld\n", nbClusts );
#endif    
        for (slong i=0; (i<nbClusts)&&(refined==0); i++) {
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pwpoly_refine_acb_app: %ld-th approximated root in local reference: ", i );
                acb_printd(nroots + i, 10); printf("\n");
            }
#endif
            int isolation = PW_INDETERMI;
            _pw_covering_shift_back_point( shifted_root, pw_approximation_coveringref(app), n, k, nroots + i, prec );
            /* make shifted_root square */
            mag_max( arb_radref( acb_realref( shifted_root ) ), arb_radref( acb_realref( shifted_root ) ), arb_radref( acb_imagref( shifted_root ) ) );
            mag_set( arb_radref( acb_imagref( shifted_root ) ), arb_radref( acb_realref( shifted_root ) ) );
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pwpoly_refine_acb_app: %ld-th approximated root in global reference: ", i );
                acb_printd(shifted_root, 10); printf("\n");
            }
#endif
#ifdef PW_PROFILE
            clock_t start_disc = clock();
#endif
            isolation = _pw_solving_get_isolating_disc( radius, app, n, shifted_root PW_VERBOSE_CALL(verbose)  );
#ifdef PW_PROFILE
            clicks_in_refine_disc += (clock() - start_disc);
#endif
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pwpoly_refine_acb_app: radius:"); arb_printd(radius, 10); printf(", inclusion: %d\n\n", isolation);
            }
#endif
            if (isolation==PW_NATURAL_I) {
                acb_add_error_arb( shifted_root, radius);
                /* check that shifted_root intersects nroot */
                if (acb_overlaps(shifted_root, root)) {
                    refined=1;
                    acb_set(nroot, shifted_root);
                }
            }
        }
        
        pwpoly_free(mults);
        _acb_vec_clear(nroots, lenapprox-1);
        acb_poly_clear(poly);
        sector = pw_sector_list_next(sector);
    }
    
    pw_sector_list_clear(sectors);
    acb_clear(shifted_root);
    arb_clear(radius);
    
    return refined;
}

int _pwpoly_refine_root_app( root_ptr nroot, const root_ptr root, pw_approximation_t app PW_VERBOSE_ARGU(verbose)  ) {
    
    int refined = _pwpoly_refine_acb_app( root_enclosureref(nroot), root_enclosureref(root), app PW_VERBOSE_CALL(verbose)  );
    if (refined)
        root_isolationref(nroot) = PW_NATURAL_I;
    return refined;
}

slong _pwpoly_refine_piecewise_app_global( root_ptr roots, slong len, pw_approximation_t app PW_VERBOSE_ARGU(verbose) ) {
#ifndef PW_SILENT    
    int level = 1;
#endif
    int realCoeffs = pw_approximation_realCoeffsref(app);
    /* assume the roots are sorted by increasing width, and len is the number */
    /* of roots to be refined */
#ifndef PW_SILENT
    if (verbose>=level)
        printf("_pwpoly_refine_piecewise_app_global: m: %lu, nb of roots to refine: %ld,\n", 
               pw_approximation_output__mref(app), len );
#endif    
    root_t nroot;
    root_init(nroot);
    
    root_ptr refined = root_vec_init(len);
    slong nbRefined = 0;
    
    root_ptr missed = root_vec_init(len);
    slong nbMissed = 0;
  
    for (slong i=0; i<len; i++) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif 
        
        root_set(nroot, roots+i);
        int resRefine = 0;
            
        if ( (realCoeffs==0) || (arb_is_negative(acb_imagref(root_enclosureref(roots+i)))==0) ) {
            
            resRefine = _pwpoly_refine_root_app( nroot, nroot, app PW_VERBOSE_CALL(verbose)  );
                
            if (resRefine) {
                
                root_set(refined+nbRefined, nroot);
                nbRefined++;
                
                /* if the initial polynomial has real coefficients, also push the complex conjugated */
                if ( realCoeffs && arb_is_positive( acb_imagref( root_enclosureref(nroot) ) ) ){
                    root_conj( refined+nbRefined, nroot );
                    nbRefined++;
                }
                
            } else {
                root_set(missed+nbMissed, roots+i);
                nbMissed++;
                /* if the initial polynomial has real coefficients, also push the complex conjugated */
                if ( realCoeffs && arb_is_positive( acb_imagref( root_enclosureref(roots+i) ) ) ){
                    root_conj( missed+nbMissed, roots+i );
                    nbMissed++;
                }
            }
        }
    }
        
    /* copy refined roots in roots */
    slong indInRoots = 0;
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    for (slong i=0; i<nbRefined; i++) {
        root_set( roots+indInRoots, refined+i );
        indInRoots++;
    }
    /* copy non-refined roots in roots */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    for (slong i=0; i<nbMissed; i++) {
        root_set( roots+indInRoots, missed+i);
        indInRoots++;
    }
#ifndef PW_SILENT    
    if (verbose>=level)
        printf("_pwpoly_refine_piecewise_app_global: m: %lu, nb of roots refined: %ld\n", pw_approximation_output__mref(app), nbRefined );
#endif 
    
    root_vec_clear(missed, len);
    root_vec_clear(refined, len);
    root_clear(nroot);   

    return indInRoots;                                      
}

slong _pwpoly_refine_piecewise_app_domain( root_ptr roots, slong len, pw_approximation_t app,
                                           const domain_t dom PW_VERBOSE_ARGU(verbose) ) {
#ifndef PW_SILENT    
    int level = 1;
#endif
    int realCoeffs = pw_approximation_realCoeffsref(app);
    realCoeffs = realCoeffs && domain_is_symetric_real_line(dom);
    /* assume the roots are sorted by increasing width, and len is the number */
    /* of roots to be refined */
#ifndef PW_SILENT
    if (verbose>=level)
        printf("_pwpoly_refine_piecewise_app_domain: m: %lu, nb of roots to refine: %ld,\n", 
               pw_approximation_output__mref(app), len );
#endif    
    root_t nroot;
    root_init(nroot);
    
    root_ptr refinedInDom = root_vec_init(len);
    slong nbRefinedInDom = 0;
    
    root_ptr refinedOuDom = root_vec_init(len);
    slong nbRefinedOuDom = 0;
    
    root_ptr missed = root_vec_init(len);
    slong nbMissed = 0;

    for (slong i=0; i<len; i++) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif 
        
        root_set(nroot, roots+i);
        int resRefine = 0;
            
        if ( (realCoeffs==0) || (arb_is_negative(acb_imagref(root_enclosureref(roots+i)))==0) ) {
            
            resRefine = _pwpoly_refine_root_app( nroot, nroot, app PW_VERBOSE_CALL(verbose)  );
                
            if (resRefine) {
                
                if ( (domain_is_C(dom)) || domain_acb_intersect_domain(dom, root_enclosureref(nroot)) ) {
                    root_set(refinedInDom+nbRefinedInDom, nroot);
                    nbRefinedInDom++;
                    /* if the initial polynomial has real coefficients, also push the complex conjugated */
                    if ( realCoeffs && arb_is_positive( acb_imagref( root_enclosureref(nroot) ) ) ){
                        root_conj( refinedInDom+nbRefinedInDom, nroot );
                        nbRefinedInDom++;
                    }
                } else {
                    root_set(refinedOuDom+nbRefinedOuDom, nroot);
                    nbRefinedOuDom++;
                    /* if the initial polynomial has real coefficients, also push the complex conjugated */
                    if ( realCoeffs && arb_is_positive( acb_imagref( root_enclosureref(nroot) ) ) ){
                        root_conj( refinedOuDom+nbRefinedOuDom, nroot );
                        nbRefinedOuDom++;
                    }
                }
                
            } else {
                root_set(missed+nbMissed, roots+i);
                nbMissed++;
                /* if the initial polynomial has real coefficients, also push the complex conjugated */
                if ( realCoeffs && arb_is_positive( acb_imagref( root_enclosureref(roots+i) ) ) ){
                    root_conj( missed+nbMissed, roots+i );
                    nbMissed++;
                }
            }
        }
    }
        
    /* copy refined roots in domain in roots */
    slong indInRoots = 0;
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif 
    for (slong i=0; i<nbRefinedInDom; i++) {
        root_set( roots+indInRoots, refinedInDom+i );
        indInRoots++;
    }
    /* copy non-refined roots in roots */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    for (slong i=0; i<nbMissed; i++) {
        root_set( roots+indInRoots, missed+i);
        indInRoots++;
    }
    slong nbInDom = indInRoots;
    /* copy refined roots outside domain in roots */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    for (slong i=0; i<nbRefinedOuDom; i++) {
        root_set( roots+indInRoots, refinedOuDom+i );
        indInRoots++;
    }
#ifndef PW_SILENT    
    if (verbose>=level)
        printf("_pwpoly_refine_piecewise_app_domain: m: %lu, nb of roots in domain: %ld\n", pw_approximation_output__mref(app), nbInDom );
#endif 

    root_vec_clear(missed, len);
    root_vec_clear(refinedOuDom, len);
    root_vec_clear(refinedInDom, len);
    root_clear(nroot);   

    return nbInDom;                                        
}

/* assume dom is C */
slong _pwpoly_isolate_piecewise_depth_pw_polynomial_global( acb_ptr roots, slong nbAlreadyIn, acb_ptr *missed, ulong *nbmissed,
                                                            ulong * m, slong max_m, pw_polynomial_t poly, 
                                                            slong log2eps, int goal, slong goalNbRoots,
                                                            const ulong c, const fmpq_t scale, const fmpq_t b,
                                                            int solver PW_VERBOSE_ARGU(verbose)  PW_FILE_ARGU(covrFile)) {
#ifndef PW_SILENT    
    int level = 1;
#endif
    slong len = pw_polynomial_length(poly);
    int realCoeffs = pw_polynomial_is_real(poly);

#ifndef PW_SILENT
#ifdef PW_PROFILE
    clock_t start_solve, start_solve_missed;
#endif
#endif

    slong nbroots = 0;
    slong nbMissingRoots = len-1;
    slong nbRootsSave = nbroots;
    slong nbNewRoots = 0;
    
    root_ptr newroots=NULL, oldroots=NULL;
    ulong newallocsize=0, oldallocsize=0;
    max_m*=2;
    ulong old_m = *m;
    
    while ( (nbroots<goalNbRoots) && (c*(*m) < (ulong)max_m)
#ifdef PW_CHECK_INTERRUPT
            && (!pwpoly_interrupt(0))
#endif
           ) {
        
        int last_m = (2*c*(*m)) >= (ulong)max_m;
        old_m = *m;
        
        if ( ( (*m) > 30 )&& PW_SOLVING_USE_EAROOTS(solver) ) {
            int earoots_val = 0x1<<PW_SOLVING_EAROOTS;
            solver-=earoots_val;
        }
        
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, last m? %d, max m: %ld, nbroots: %ld, nbmissed: %lu, solver: %d, goalNbRoots: %ld\n", *m, last_m, max_m, nbroots, *nbmissed, solver, goalNbRoots);
        }
#endif        
        pw_approximation_t app;
        pw_approximation_init_pw_polynomial( app, poly, *m, c, scale, b);
        
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif
        if ( PW_GOAL_MUST_APPROXIMATE(goal) && (last_m || ( (slong)(*m) >= -log2eps/2 ) ) && (nbroots>0) ) {
#ifndef PW_SILENT
            if (verbose>=level)
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, refine oldroots?\n", *m);
#endif       
            slong indToRefine, lenToRefine;
            root_get_bounds_to_refine( &indToRefine, &lenToRefine, oldroots, nbroots, goal, log2eps, realCoeffs );

#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            if ( lenToRefine>0 ) {
#ifdef PW_PROFILE
                clock_t start_refine = clock();
#endif
                _pwpoly_refine_piecewise_app_global( oldroots+indToRefine, lenToRefine, app PW_VERBOSE_CALL(verbose)  );
                
#ifdef PW_PROFILE
                clicks_in_refine_piecewise += (clock() - start_refine);
#ifndef PW_SILENT
                if (verbose>=level) {
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu time in refine with approximations: %lf s\n", *m, clicks_in_refine_piecewise/CLOCKS_PER_SEC);
                }
#endif
#endif
            }
        }
   
#ifndef PW_SILENT
#ifdef PW_PROFILE
        start_solve = clock();
#endif
#endif
        
        if ( (newroots!=NULL) && (newallocsize>0) )
            root_vec_clear( newroots, newallocsize );
        newallocsize=0;
        newroots=NULL;
        /* allocate newroots */
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0)) {
#endif
        ulong cworm  = pw_approximation_cref(app)*pw_approximation_working_mref(app);
        newallocsize = PWPOLY_MAX( (ulong)(2*len), (ulong)(len + 2*cworm));
        newroots     = root_vec_init(newallocsize);
#ifdef PW_CHECK_INTERRUPT
        }
#endif
        ulong nbNroots = 0;
        
        /* re-use previous result */
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif
        if ((*nbmissed)>0) {
            pw_sector_list_t sectors;
            pw_sector_list_init(sectors);
            
            /* locate missed roots of old app in sectors of new covering */
#ifndef PW_SILENT
#ifdef PW_PROFILE
            clock_t start_locate_in_sectors = clock();
#endif
#endif
#ifdef PW_SILENT
                                    pw_covering_locate_points( sectors, pw_approximation_coveringref(app), *missed, *nbmissed, 
                                                               pw_approximation_precref(app) );
#else         
            ulong nbmissedsectors = pw_covering_locate_points( sectors, pw_approximation_coveringref(app), *missed, *nbmissed, 
                                                               pw_approximation_precref(app) );
            if (verbose>=level) {
#ifdef PW_PROFILE
                double clicks_in_locate_in_sectors = (clock() - start_locate_in_sectors);
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, nb sector missed: %lu/%lu, time in locate: %lf s\n", 
                       *m, nbmissedsectors, pw_approximation_Ksumref(app), clicks_in_locate_in_sectors/CLOCKS_PER_SEC );
#else
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, nb sector missed: %lu/%lu\n", 
                       *m, pw_approximation_Ksumref(app), nbmissedsectors);
#endif
            }
#endif
            
            /* clear and re-init missed */
            ulong missed_size = len*((*nbmissed)/len + 1);
//          printf("nbmissed: %lu, missed_size: %lu\n", nbmissed, missed_size);
            _acb_vec_clear( *missed, missed_size );
            *missed = _acb_vec_init( len );
            *nbmissed = 0;
        
            /* solve in missed sectors */
#ifndef PW_SILENT
#ifdef PW_PROFILE
            start_solve_missed = clock();
#endif
#endif
            nbNroots = _pwpoly_isolate_m_app_in_sectors_global( newroots, 0, missed, nbmissed, app, sectors, goalNbRoots, solver PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT
            if (verbose>=level) {
#ifdef PW_PROFILE
                double clicks_in_solve_missed = (clock() - start_solve_missed);
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, %lu/%lu isolated roots in missed sectors, time: %lf s\n", *m, nbNroots, len-1, clicks_in_solve_missed/CLOCKS_PER_SEC );
#else
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, %lu/%lu isolated roots in missed sectors\n", *m, nbNroots, len-1 );
#endif        
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, nb of missed roots: %lu\n", *m, *nbmissed );
            }
#endif
            
            /* copy the new roots in old roots and eliminate doubles */
            /* both nbroots and nbNroots are < nlen        */
            /* and oldroots contains at least 2*nlen slots */
#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            for (ulong i=0; i<nbNroots; i++)
                root_set( oldroots + (nbroots + i), newroots + i );
#ifdef PW_PROFILE
            clock_t start_merge = clock();
#endif 
#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            nbroots = _pw_roots_merge_overlaps(oldroots, nbNroots + nbroots, pw_approximation_precref(app));
#ifdef PW_PROFILE
            clicks_in_merge += (clock() - start_merge);
#endif
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, %lu distinct isolated roots after eliminate doubles\n", *m, nbroots);
        }
#endif
#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            if (nbroots < goalNbRoots) {
                /* get the neighbors of missed sectors */
#ifndef PW_SILENT
#ifdef PW_PROFILE
                clock_t start_neighbors = clock();
#endif
#endif
                pw_covering_add_neighbors( sectors, pw_approximation_coveringref(app), realCoeffs );
                
#ifndef PW_SILENT
                nbmissedsectors = pw_sector_list_get_size(sectors);
                if (verbose>=level) {
#ifdef PW_PROFILE
                    double clicks_in_neighbors = (clock() - start_neighbors);
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, nb sector missed after adding neighborhood: %lu/%lu, time: %lf s\n", 
                       *m, nbmissedsectors, pw_approximation_Ksumref(app), clicks_in_neighbors/CLOCKS_PER_SEC );
#else
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, nb sector missed after adding neighborhood: %lu/%lu\n", 
                       *m, pw_approximation_Ksumref(app), nbmissedsectors);
#endif
                }
#endif
                /* solve in neighbors of missed sectors */
#ifndef PW_SILENT
#ifdef PW_PROFILE
                start_solve_missed = clock();
#endif
#endif
                ulong nbNroots_save = nbNroots;
                nbNroots = _pwpoly_isolate_m_app_in_sectors_global( newroots, nbNroots, missed, nbmissed, app, sectors, goalNbRoots, solver PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT
                if (verbose>=level) {
#ifdef PW_PROFILE
                    double clicks_in_solve_missed = (clock() - start_solve_missed);
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, %lu/%lu isolated roots in missed sectors, time: %lf s\n", *m, nbNroots, len-1, clicks_in_solve_missed/CLOCKS_PER_SEC );
#else
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, %lu/%lu isolated roots in missed sectors\n", *m, nbNroots, nlen-1 );
#endif        
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, nb of missed roots: %lu\n", *m, *nbmissed );
                }
#endif

#ifdef PW_CHECK_INTERRUPT
                if (!pwpoly_interrupt(0))
#endif
                if (nbNroots>nbNroots_save) {
                    /* copy the new roots in old roots and eliminate doubles */
                    /* both nbroots and nbNroots are < nlen        */
                    /* and oldroots contains at least 2*nlen slots */
                    for (ulong i=0; i<nbNroots; i++)
                        root_set( oldroots + (nbroots + i), newroots + i );
#ifdef PW_PROFILE
                    clock_t start_merge = clock();
#endif                
                    nbroots = _pw_roots_merge_overlaps(oldroots, nbNroots + nbroots, pw_approximation_precref(app));
#ifdef PW_PROFILE
                    clicks_in_merge += (clock() - start_merge);
#endif 
#ifndef PW_SILENT
                    if (verbose>=level) {
                        printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, %lu distinct isolated roots after eliminate doubles\n", *m, nbroots);
                    }
#endif
                }
            }
#ifndef PW_SILENT
            if (verbose>=level) {
                if (PW_GOAL_MUST_APPROXIMATE(goal)) {
                    slong min, max;
                    root_log2width_minmax( &min, &max, newroots, nbNroots, PW_GOAL_PREC_RELATIVE(goal) );
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, log2widths of newroots: min: %ld, max: %ld (target: %ld)\n", *m, min, max, log2eps);
                }
            }
#endif            
            pw_sector_list_clear(sectors);
        }

#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif
        if (nbroots < goalNbRoots) {
            
            if ((*nbmissed) > 0) { /* increase m and continue solving missed sectors */
                nbMissingRoots = (len-1) - nbRootsSave;
                nbNewRoots     = nbroots - nbRootsSave;
#ifndef PW_SILENT
                if (verbose>=level)
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, nb roots missing before: %lu, nb new roots after: %lu\n", 
                                                 *m, nbMissingRoots, nbNewRoots);
#endif
//                 (*m)*=2;
                (*m) = _pwpoly_isolate_piecewise_get_next_m_NEW( *m, nbMissingRoots, nbNewRoots );
#ifndef PW_SILENT
                if (verbose>=level)
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, next m: %lu\n", old_m, *m );
#endif
            } else { /*solve in not already solved sectors */
                nbroots = _pwpoly_isolate_m_app_global( newroots, nbNroots, missed, nbmissed, app, goalNbRoots, solver PW_VERBOSE_CALL(verbose)  );
                
                nbMissingRoots = (len-1) - nbRootsSave;
                nbNewRoots     = nbroots - nbRootsSave;
#ifndef PW_SILENT
                if (verbose>=level)
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, nb roots missing before: %lu, nb new roots after: %lu\n", 
                                                 *m, nbMissingRoots, nbNewRoots);
#endif                
                if (nbroots < goalNbRoots) { 
                    /* increase m and continue solving */
//                     (*m)*=2;
//                     ulong old_m = *m;
//                     *m = _pwpoly_isolate_piecewise_get_next_m( *m, nbroots, len, max_m, c );
                    (*m) = _pwpoly_isolate_piecewise_get_next_m_NEW( *m, nbMissingRoots, nbNewRoots );
#ifndef PW_SILENT
                    if (verbose>=level)
                        printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, next m: %lu\n", old_m, *m );
#endif
                }
                /* move newroots in oldroots */
                if ( (oldroots!=NULL) && (oldallocsize>0) ) /* should not happen? except if roots are missing whitout any missed root... */
                    root_vec_clear(oldroots, oldallocsize);
                oldroots=newroots;
                oldallocsize=newallocsize;
                newroots=NULL;
                newallocsize=0;
                
            }
            
            nbRootsSave=nbroots;
        }
#ifndef PW_SILENT        
        if (verbose>=level) {
#ifdef PW_PROFILE
            double clicks_in_solve = (clock() - start_solve);
            printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, %lu/%lu isolated roots, time: %lf s\n", old_m, nbroots, len-1, clicks_in_solve/CLOCKS_PER_SEC );
#else
            printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, %lu/%lu isolated roots\n", old_m, nbroots, len-1 );
#endif
            printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, nb of missed roots: %lu\n", old_m, *nbmissed );
            
            if (PW_GOAL_MUST_APPROXIMATE(goal)) {
                slong min, max;
                root_log2width_minmax( &min, &max, oldroots, nbroots, PW_GOAL_PREC_RELATIVE(goal) );
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_global: m: %lu, log2widths of oldroots: min: %ld, max: %ld (target: %ld)\n", old_m, min, max, log2eps);
            }
        }
#endif        
        
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif
        /* copy the natural clusters of oldroots in roots */
        for (slong i=0; i<nbroots; i++) {
            acb_set( roots+nbAlreadyIn+i, root_enclosureref( oldroots+i ) );
            /* since all enclosures in roots are natural isolator */
            /* if coeffs are real, enclosure contains real axis => the root in the enclosure is real */
            if ( realCoeffs && ( arb_contains_zero( acb_imagref(roots+nbAlreadyIn+i) ) ) )
                arb_zero( acb_imagref(roots+nbAlreadyIn+i) );
        }
#ifndef PW_NO_INTERFACE       
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif
        if (covrFile != NULL) {
            int write = !((nbroots<goalNbRoots) && (c*(*m) < (ulong)max_m));
            if (write) {
                _pw_covering_with_points_gnuplot( covrFile, pw_approximation_coveringref(app), roots, (slong)(nbroots+nbAlreadyIn) );
            }
        }
#endif        
        pw_approximation_clear(app);
    
    }
    
    /* clear newroots and oldroots */
    if ( (newroots!=NULL) && (newallocsize>0) )
        root_vec_clear( newroots, newallocsize );
    newallocsize=0;
    if ( (oldroots!=NULL) && (oldallocsize>0) )
        root_vec_clear( oldroots, oldallocsize );
    oldallocsize=0;
    
    if (nbroots<(len-1))
        *m = old_m;
    
    return nbroots;
}

void _pwpoly_refine_piecewise_global( acb_ptr roots, slong lenToRefineIn, ulong *m, slong max_m, 
                                      pw_polynomial_t poly,
                                      int goal, slong log2eps,
                                      const ulong c, const fmpq_t scale, const fmpq_t b PW_VERBOSE_ARGU(verbose) ) {
#ifndef PW_SILENT    
    int level = 1;
#endif    
    if (lenToRefineIn==0)
        return;
#ifndef PW_SILENT    
    if (verbose>=level)
        printf("_pwpoly_refine_piecewise_global begin: m: %lu, nb of roots to refine: %ld\n", *m, lenToRefineIn );
#endif    
    /* try to increase m to reach asked precision */
    ulong msave = *m;
    if (log2eps<0) {
        if ( (slong)(*m) >= -log2eps/2 )
            (*m)*=2;
        else
            (*m) = (ulong)(-log2eps/2);
    }
#ifndef PW_SILENT    
    if (verbose>=level)
        printf("_pwpoly_refine_piecewise_global: new m: %lu, new c*m: %lu, max c*m: %lu\n", *m, c*(*m), (ulong)max_m );
#endif    
    if (c*(*m) >= (ulong)max_m) {
        *m = msave;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pwpoly_refine_piecewise_global end  : m: %lu, nb of roots to refine: %ld\n", *m, lenToRefineIn );
#endif
        return;
    }
    
    int realCoeffs = pw_polynomial_is_real(poly);
    slong lenToRefine = lenToRefineIn;
    slong indToRefine = 0;
    
    acb_t nroot;
    acb_init(nroot);
    
    slong nbTodo = 0;
    acb_ptr todo = _acb_vec_init(lenToRefineIn);
    /* push the roots to refine in missed */
    /* if realCoeffs, keep only non imaginary-negative roots */
    for (slong i=0; i<lenToRefine; i++) {
        acb_set( todo + nbTodo, roots + i );
        /* if imaginary part is zero, transform it into a square box */
        if ( arb_is_zero( acb_imagref( todo + nbTodo ) ) )
            mag_set( arb_radref(acb_imagref( todo + nbTodo )), arb_radref(acb_realref( todo + nbTodo )) );
        /* if non imaginary-negative, keep it */
        if ((realCoeffs==0) || (arb_is_negative(acb_imagref(todo + nbTodo))==0))
            nbTodo++;
    }
    
    while ( (c*(*m) < (ulong)max_m) && (nbTodo>0) ) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif
            
#ifndef PW_SILENT        
        if (verbose>=level)
            printf("_pwpoly_refine_piecewise_global: m: %lu, nb of roots to refine: %ld\n", *m, nbTodo );
#endif        
        pw_approximation_t app;
        pw_approximation_init_pw_polynomial( app, poly, *m, c, scale, b);
        
        slong nbMissed = 0;
        for (slong i=0; i<nbTodo; i++) {
            
            #ifdef PW_CHECK_INTERRUPT
            if (pwpoly_interrupt(0))
                break;
            #endif
            
            int resRefine = _pwpoly_refine_acb_app( nroot, todo+i, app PW_VERBOSE_CALL(verbose)  );
            if (resRefine) {
                if (pwpoly_width_less_eps( nroot, log2eps, PW_GOAL_PREC_RELATIVE(goal) )) {
                    /* write refined root in roots, decide realness */
                    acb_set( roots+indToRefine, nroot );
                    if ( realCoeffs && ( arb_contains_zero( acb_imagref(nroot) ) ) )
                        arb_zero( acb_imagref(roots+indToRefine) );
                    indToRefine++;
                    if ( realCoeffs && arb_is_positive( acb_imagref( nroot ) ) ) {
                        acb_conj( roots+indToRefine, nroot );
                        indToRefine++;
                    }
                } else {
                    /* write it in todo */
                    acb_set( todo + nbMissed, nroot );
                    nbMissed++;
                }
            } else {
                /* write it in todo */
                acb_set( todo + nbMissed, todo + i );
                nbMissed++;
            }
        }
        pw_approximation_clear(app);
        nbTodo = nbMissed;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pwpoly_refine_piecewise_global: m: %lu, nbMissed: %ld\n", *m, nbMissed );
#endif
        /* increase m if necessary */
        msave = *m;
        if (nbTodo>0) {
            (*m)*=2;
        }
    }
    
    lenToRefine = 0;
    /* copy the roots still to refine in roots */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    for (slong i=0; i<nbTodo; i++) {
        acb_set( roots+(indToRefine+lenToRefine), todo+i);
        lenToRefine++;
        if ( realCoeffs && arb_is_positive( acb_imagref( todo+i ) ) ) {
            acb_conj( roots+(indToRefine+lenToRefine), todo+i );
            lenToRefine++;
        }
    }
    
    _acb_vec_clear(todo, lenToRefineIn);
    acb_clear(nroot);
    
    *m = msave;
#ifndef PW_SILENT    
    if (verbose>=level)
        printf("_pwpoly_refine_piecewise_global end  : m: %lu, nb of roots to refine: %ld\n", *m, lenToRefine );
#endif    
    return;
}

slong _pwpoly_refine_piecewise_domain( acb_ptr roots, slong lenToRefineIn, ulong *m, slong max_m, 
                                       pw_polynomial_t poly,
                                       int goal, slong log2eps, const domain_t dom, slong goalNbRoots,
                                       const ulong c, const fmpq_t scale, const fmpq_t b PW_VERBOSE_ARGU(verbose)) {
#ifndef PW_SILENT    
    int level = 1;
#endif    
    if ( (lenToRefineIn==0) || (goalNbRoots==0) )
        return 0;
#ifndef PW_SILENT    
    if (verbose>=level)
        printf("_pwpoly_refine_piecewise_domain begin: m: %lu, nb of roots to refine: %ld, goalNbRoots: %ld\n", *m, lenToRefineIn, goalNbRoots );
#endif    
    /* try to increase m to reach asked precision */
    ulong msave = *m;
    if (log2eps<0) {
        if ( (slong)(*m) >= -log2eps/2 )
            (*m)*=2;
        else
            (*m) = (ulong)(-log2eps/2);
    }

//     max_m = (ulong)pw_polynomial_length(poly);
#ifndef PW_SILENT    
    if (verbose>=level)
        printf("_pwpoly_refine_piecewise_domain: new m: %lu, new c*m: %lu, max c*m: %lu\n", *m, c*(*m), (ulong)max_m );
#endif    
    if (c*(*m) >= (ulong)max_m) {
        *m = msave;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pwpoly_refine_piecewise_domain end  : m: %lu, nb of roots intersecting domain: %ld\n", *m, lenToRefineIn );
#endif
        return lenToRefineIn;
    }
    
    int realCoeffs = pw_polynomial_is_real(poly);
    realCoeffs = realCoeffs&&domain_is_symetric_real_line(dom);
    int decideRealness = pw_polynomial_is_real(poly);
    slong lenToRefine = lenToRefineIn;
    slong indToRefine = 0;
    
    acb_t nroot;
    acb_init(nroot);
    
    slong nbTodo = 0;
    acb_ptr todo = _acb_vec_init(lenToRefineIn);
    /* push the roots to refine in missed */
    /* if realCoeffs, keep only non imaginary-negative roots */
    for (slong i=0; i<lenToRefine; i++) {
        acb_set( todo + nbTodo, roots + i );
        /* if imaginary part is zero, transform it into a square box */
        if ( arb_is_zero( acb_imagref( todo + nbTodo ) ) )
            mag_set( arb_radref(acb_imagref( todo + nbTodo )), arb_radref(acb_realref( todo + nbTodo )) );
        /* if non imaginary-negative, keep it */
        if ((realCoeffs==0) || (arb_is_negative(acb_imagref(todo + nbTodo))==0))
            nbTodo++;
    }
    
    slong nbOutsDomain = 0;
    acb_ptr outsDomain = _acb_vec_init(lenToRefineIn);
    
    while ( (c*(*m) < (ulong)max_m) && (nbTodo>0) && (indToRefine<goalNbRoots) ) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif
        
#ifndef PW_SILENT        
        if (verbose>=level)
            printf("_pwpoly_refine_piecewise_domain: m: %lu, nb refined: %ld, nbTodo: %ld\n",
                   *m, indToRefine, nbTodo );
#endif        
        pw_approximation_t app;
        pw_approximation_init_pw_polynomial( app, poly, *m, c, scale, b);
        
        slong nbMissed = 0;
        for (slong i=0; i<nbTodo; i++) {
            
#ifdef PW_CHECK_INTERRUPT
            if (pwpoly_interrupt(0))
                break;
#endif
            
            if (indToRefine >= goalNbRoots){
                acb_set( todo + nbMissed, todo + i );
                nbMissed++;
                continue;
            }
            int resRefine = _pwpoly_refine_acb_app( nroot, todo+i, app PW_VERBOSE_CALL(verbose)  );
            if (resRefine) {
                
                if ( !domain_acb_intersect_domain(dom, nroot) ){
                    /* write refined root in outsDomain, decide realness */
                    acb_set( outsDomain+nbOutsDomain, nroot );
                    if ( decideRealness && ( arb_contains_zero( acb_imagref(nroot) ) ) )
                        arb_zero( acb_imagref(outsDomain+nbOutsDomain) );
                    nbOutsDomain++;
                    if ( realCoeffs && arb_is_positive( acb_imagref( nroot ) ) ) {
                        acb_conj( outsDomain+nbOutsDomain, nroot );
                        nbOutsDomain++;
                    }
                } else {
                
                    if (pwpoly_width_less_eps( nroot, log2eps, PW_GOAL_PREC_RELATIVE(goal) )) {
                        /* write refined root in roots, decide realness */
                        acb_set( roots+indToRefine, nroot );
                        if ( decideRealness && ( arb_contains_zero( acb_imagref(nroot) ) ) )
                            arb_zero( acb_imagref(roots+indToRefine) );
                        indToRefine++;
                        if ( realCoeffs && arb_is_positive( acb_imagref( nroot ) ) ) {
                            acb_conj( roots+indToRefine, nroot );
                            indToRefine++;
                        }
                    } else {
                        /* write it in todo */
                        acb_set( todo + nbMissed, nroot );
                        nbMissed++;
                    }
                }
            } else {
                /* write it in todo */
                acb_set( todo + nbMissed, todo + i );
                nbMissed++;
            }
        }
        pw_approximation_clear(app);
        nbTodo = nbMissed;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pwpoly_refine_piecewise_domain: m: %lu, nbRefined: %ld, nbMissed: %ld, nbOutsDomain: %ld\n",
                   *m, indToRefine, nbMissed, nbOutsDomain );
#endif
        /* increase m if necessary */
        msave = *m;
        if (nbTodo>0) {
            (*m)*=2;
        }
    }
    
    lenToRefine = 0;
    /* copy the roots still to refine in roots -> they all intersect the domain */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    for (slong i=0; i<nbTodo; i++) {
        acb_set( roots+(indToRefine+lenToRefine), todo+i);
        if ( decideRealness && ( arb_contains_zero( acb_imagref(todo+i) ) ) )
            arb_zero( acb_imagref(roots+(indToRefine+lenToRefine)) );
        lenToRefine++;
        if ( realCoeffs && arb_is_positive( acb_imagref( todo+i ) ) ) {
            acb_conj( roots+(indToRefine+lenToRefine), todo+i );
            lenToRefine++;
        }
    }
    slong nbRootsInDomain = indToRefine + lenToRefine;
    /* copy the roots outside domain in roots */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    for (slong i=0; i<nbOutsDomain; i++) {
        acb_set( roots+(indToRefine+lenToRefine), outsDomain+i);
        lenToRefine++;
    }
    
    _acb_vec_clear(outsDomain, lenToRefineIn);
    _acb_vec_clear(todo, lenToRefineIn);
    acb_clear(nroot);
    
    *m = msave;
#ifndef PW_SILENT    
    if (verbose>=level)
        printf("_pwpoly_refine_piecewise_domain end  : m: %lu, nb of roots intersecting domain: %ld\n", *m, nbRootsInDomain );
#endif    
    return nbRootsInDomain;
}

slong _pwpoly_isolate_piecewise_depth_pw_polynomial_domain( acb_ptr roots, slong nbAlreadyIn, acb_ptr *missed, ulong *nbmissed,
                                                            ulong * m, slong max_m, pw_polynomial_t poly, 
                                                            slong log2eps, int goal, const domain_t dom, slong goalNbRoots,
                                                            const ulong c, const fmpq_t scale, const fmpq_t b,
                                                            int solver PW_VERBOSE_ARGU(verbose)  PW_FILE_ARGU(covrFile)) {
#ifndef PW_SILENT    
    int level = 1;
#endif
    slong len = pw_polynomial_length(poly);
    slong deg = len-1;
    int realCoeffs = pw_polynomial_is_real(poly);

#ifndef PW_SILENT
#ifdef PW_PROFILE
    clock_t start_solve, start_solve_missed;
#endif
#endif

    slong nbRoots = 0;             /* the total number of roots so far  */
    slong nbMissingRoots = len-1;  /* the total number of roots to find */
    slong nbRootsSave = nbRoots;
    slong nbNewRoots = 0;
    
    slong nbRootsInDomain = 0;     /* the number of roots in roots intersecting the domain    */
    slong nbRootsInResult = 0;     /* the number of roots that will be part of the output:    */
                                   /* they are either strictly in the domain, or intersect it */
                                   /* and have appropriated size                              */
                                   
    /* always assume that the nbRootsInDomain first elements of roots are roots intersecting the domain */
    root_ptr newroots=NULL, oldroots=NULL;
    ulong newallocsize=0, oldallocsize=0;
    max_m*=2;
    ulong old_m = *m;
    
    while ( (nbRoots<deg) && (nbRootsInResult<goalNbRoots) && (c*(*m) < (ulong)max_m)
#ifdef PW_CHECK_INTERRUPT
            && (!pwpoly_interrupt(0))
#endif
          ) {
        
        int last_m = (2*c*(*m)) >= (ulong)max_m;
        old_m = *m;
        
        if ( ( (*m) > 30 )&& PW_SOLVING_USE_EAROOTS(solver) ) {
            int earoots_val = 0x1<<PW_SOLVING_EAROOTS;
            solver-=earoots_val;
        }
        
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, last m? %d, max m: %ld\n",
                   *m, last_m, max_m );
            printf("                                                            , nbRoots: %ld, nbmissed: %lu, solver: %d\n", 
                    nbRoots, *nbmissed, solver);
            printf("                                                            , nbRootsInDomain: %ld, nbRootsInResult: %lu, goalNbRoots: %ld\n", 
                    nbRootsInDomain, nbRootsInResult, goalNbRoots);
        }
#endif        
        pw_approximation_t app;
        pw_approximation_init_pw_polynomial( app, poly, *m, c, scale, b);

#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif        
        if ( PW_GOAL_MUST_APPROXIMATE(goal) && (last_m || ( (slong)(*m) >= -log2eps/2 ) ) && (nbRootsInDomain>0) ) {
#ifndef PW_SILENT
            if (verbose>=level)
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, refine oldroots?\n", *m);
#endif       
            slong indToRefine, lenToRefine;
            root_get_bounds_to_refine( &indToRefine, &lenToRefine, oldroots, nbRootsInDomain, goal, log2eps, 
                                       realCoeffs&&domain_is_symetric_real_line(dom) );
#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            if ( lenToRefine>0 ) {
#ifdef PW_PROFILE
                clock_t start_refine = clock();
#endif
                /* try to refine all the actual roots in domain */
                slong res = _pwpoly_refine_piecewise_app_domain( oldroots+indToRefine, lenToRefine, app, dom PW_VERBOSE_CALL(verbose)  );
                /* actualize nbRootsInDomain */
                nbRootsInDomain = indToRefine+res;
                /* actualize nbRootsInResult */
                nbRootsInResult=0;
                for (slong i=0; i<nbRootsInDomain; i++)
                    if ( domain_acb_is_strictly_in_domain( dom, root_enclosureref(oldroots+i) ) )
                        nbRootsInResult++;
                    else if ( (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                           || pwpoly_width_less_eps( root_enclosureref(oldroots+i), log2eps, PW_GOAL_PREC_RELATIVE(goal) ) )
                        nbRootsInResult++;
#ifndef PW_SILENT                
                if (verbose>=level) {
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, after refine:\n", *m);
                    printf("                                                      nbRootsInDomain: %ld, nbRootsInResult: %ld\n", nbRootsInDomain, nbRootsInResult);
                }
#endif
#ifdef PW_PROFILE
                clicks_in_refine_piecewise += (clock() - start_refine);
#ifndef PW_SILENT
                if (verbose>=level) {
                    printf("                                                      time in refine with approximations: %lf s\n", clicks_in_refine_piecewise/CLOCKS_PER_SEC);
                }
#endif
#endif
            }

        }
   
#ifndef PW_SILENT
#ifdef PW_PROFILE
        start_solve = clock();
#endif
#endif
        
        if ( (newroots!=NULL) && (newallocsize>0) )
            root_vec_clear( newroots, newallocsize );
        newallocsize=0;
        newroots=NULL;
        /* allocate newroots */
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0)) {
#endif
        ulong cworm  = pw_approximation_cref(app)*pw_approximation_working_mref(app);
        newallocsize = PWPOLY_MAX( (ulong)(2*len), (ulong)(len + 2*cworm));
        newroots     = root_vec_init(newallocsize);
#ifdef PW_CHECK_INTERRUPT
        }
#endif
        ulong nbNroots = 0;

#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif        
        /* re-use previous result */
        if ((*nbmissed)>0) {
            pw_sector_list_t sectors;
            pw_sector_list_init(sectors);
            
            /* locate missed roots of old app in sectors of new covering */
#ifndef PW_SILENT
#ifdef PW_PROFILE
            clock_t start_locate_in_sectors = clock();
#endif
#endif
#ifdef PW_SILENT
                                    pw_covering_locate_points( sectors, pw_approximation_coveringref(app), *missed, *nbmissed, 
                                                               pw_approximation_precref(app) );
#else         
            ulong nbmissedsectors = pw_covering_locate_points( sectors, pw_approximation_coveringref(app), *missed, *nbmissed, 
                                                               pw_approximation_precref(app) );
            if (verbose>=level) {
#ifdef PW_PROFILE
                double clicks_in_locate_in_sectors = (clock() - start_locate_in_sectors);
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, nb sector missed: %lu/%lu, time in locate: %lf s\n", 
                       *m, nbmissedsectors, pw_approximation_Ksumref(app), clicks_in_locate_in_sectors/CLOCKS_PER_SEC );
#else
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, nb sector missed: %lu/%lu\n", 
                       *m, pw_approximation_Ksumref(app), nbmissedsectors);
#endif
            }
#endif
            
            /* clear and re-init missed */
            ulong missed_size = len*((*nbmissed)/len + 1);
//          printf("nbmissed: %lu, missed_size: %lu\n", nbmissed, missed_size);
            _acb_vec_clear( *missed, missed_size );
            *missed = _acb_vec_init( len );
            *nbmissed = 0;
        
            /* solve in missed sectors */
#ifndef PW_SILENT
#ifdef PW_PROFILE
            start_solve_missed = clock();
#endif
#endif
            nbNroots = _pwpoly_isolate_m_app_in_sectors_global( newroots, 0, missed, nbmissed, app, sectors, deg, solver PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT
            if (verbose>=level) {
#ifdef PW_PROFILE
                double clicks_in_solve_missed = (clock() - start_solve_missed);
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, %lu/%lu isolated roots in missed sectors, time: %lf s\n", *m, nbNroots, len-1, clicks_in_solve_missed/CLOCKS_PER_SEC );
#else
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, %lu/%lu isolated roots in missed sectors\n", *m, nbNroots, len-1 );
#endif        
                printf("                                                              nb of missed roots: %lu\n", *nbmissed );
            }
#endif
            /* copy the new roots in old roots and eliminate doubles */
            /* both nbRoots and nbNroots are < nlen                  */
            /* and oldroots contains at least 2*nlen slots           */
#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            for (ulong i=0; i<nbNroots; i++)
                root_set( oldroots + (nbRoots + i), newroots + i );
#ifdef PW_PROFILE
            clock_t start_merge = clock();
#endif 
#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            nbRoots = _pw_roots_merge_overlaps(oldroots, nbNroots + nbRoots, pw_approximation_precref(app));
#ifdef PW_PROFILE
            clicks_in_merge += (clock() - start_merge);
#endif
            /* actualize nbRootsInDomain and nbRootsInResult */
            nbRootsInDomain=0;
            nbRootsInResult=0;
            for (slong i=0; i<nbRoots; i++)
                if (domain_acb_intersect_domain(dom, root_enclosureref(oldroots+i) ) ) {
                    nbRootsInDomain++;
                    if ( domain_acb_is_strictly_in_domain( dom, root_enclosureref(oldroots+i) ) )
                        nbRootsInResult++;
                    else if ( (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                            || pwpoly_width_less_eps( root_enclosureref(oldroots+i), log2eps, PW_GOAL_PREC_RELATIVE(goal) ) )
                        nbRootsInResult++;
                }
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, %lu distinct isolated roots after eliminate doubles\n", *m, nbRoots);
                printf("                                                      nbRootsInDomain: %ld, nbRootsInResult: %ld\n", nbRootsInDomain, nbRootsInResult);
            }
#endif
#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            if ( (nbRoots < deg)&&(nbRootsInResult<goalNbRoots) ) {
                /* get the neighbors of missed sectors */
#ifndef PW_SILENT
#ifdef PW_PROFILE
                clock_t start_neighbors = clock();
#endif
#endif
                pw_covering_add_neighbors( sectors, pw_approximation_coveringref(app), realCoeffs );
                
#ifndef PW_SILENT
                nbmissedsectors = pw_sector_list_get_size(sectors);
                if (verbose>=level) {
#ifdef PW_PROFILE
                    double clicks_in_neighbors = (clock() - start_neighbors);
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, nb sector missed after adding neighborhood: %lu/%lu, time: %lf s\n", 
                       *m, nbmissedsectors, pw_approximation_Ksumref(app), clicks_in_neighbors/CLOCKS_PER_SEC );
#else
                    printf("                                                         nb sector missed after adding neighborhood: %lu/%lu\n", 
                       *m, pw_approximation_Ksumref(app), nbmissedsectors);
#endif
                }
#endif
                /* solve in neighbors of missed sectors */
#ifndef PW_SILENT
#ifdef PW_PROFILE
                start_solve_missed = clock();
#endif
#endif
                ulong nbNroots_save = nbNroots;
                nbNroots = _pwpoly_isolate_m_app_in_sectors_global( newroots, nbNroots, missed, nbmissed, app, sectors, deg, solver PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT
                if (verbose>=level) {
#ifdef PW_PROFILE
                    double clicks_in_solve_missed = (clock() - start_solve_missed);
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, %lu/%lu isolated roots in missed sectors, time: %lf s\n", *m, nbNroots, len-1, clicks_in_solve_missed/CLOCKS_PER_SEC );
#else
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, %lu/%lu isolated roots in missed sectors\n", *m, nbNroots, nlen-1 );
#endif        
                    printf("                                                      nb of missed roots: %lu\n", *nbmissed );
                }
#endif
#ifdef PW_CHECK_INTERRUPT
                if (!pwpoly_interrupt(0))
#endif                
                if (nbNroots>nbNroots_save) {
                    /* copy the new roots in old roots and eliminate doubles */
                    /* both nbroots and nbNroots are < nlen        */
                    /* and oldroots contains at least 2*nlen slots */
                    for (ulong i=0; i<nbNroots; i++)
                        root_set( oldroots + (nbRoots + i), newroots + i );
#ifdef PW_PROFILE
                    clock_t start_merge = clock();
#endif                
                    nbRoots = _pw_roots_merge_overlaps(oldroots, nbNroots + nbRoots, pw_approximation_precref(app));
#ifdef PW_PROFILE
                    clicks_in_merge += (clock() - start_merge);
#endif             
                    /* actualize nbRootsInDomain and nbRootsInResult */
                    nbRootsInDomain=0;
                    nbRootsInResult=0;
                    for (slong i=0; i<nbRoots; i++)
                        if (domain_acb_intersect_domain(dom, root_enclosureref(oldroots+i) ) ) {
                            nbRootsInDomain++;
                            if ( domain_acb_is_strictly_in_domain( dom, root_enclosureref(oldroots+i) ) )
                                nbRootsInResult++;
                            else if ( (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                                    || pwpoly_width_less_eps( root_enclosureref(oldroots+i), log2eps, PW_GOAL_PREC_RELATIVE(goal) ) )
                                nbRootsInResult++;
                        }
#ifndef PW_SILENT
                    if (verbose>=level) {
                        printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, %lu distinct isolated roots after eliminate doubles\n", *m, nbRoots);
                        printf("                                                      nbRootsInDomain: %ld, nbRootsInResult: %ld\n", nbRootsInDomain, nbRootsInResult);
                    }
#endif
                }
            }
            
            /* puts roots intersecting the domain at the beginning of oldroots */
#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            root_get_roots_intersecting_domain( oldroots, nbRoots, dom );
            
#ifndef PW_SILENT
            if (verbose>=level) {
                if (PW_GOAL_MUST_APPROXIMATE(goal)) {
                    slong min, max;
                    root_log2width_minmax( &min, &max, newroots, nbNroots, PW_GOAL_PREC_RELATIVE(goal) );
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, log2widths of newroots: min: %ld, max: %ld (target: %ld)\n", *m, min, max, log2eps);
                }
            }
#endif            
            pw_sector_list_clear(sectors);
        }
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif        
        if ( (nbRoots < deg)&&(nbRootsInResult<goalNbRoots) ) {
            
            if ((*nbmissed) > 0) { /* increase m and continue solving missed sectors */
                nbMissingRoots = deg - nbRootsSave;
                nbNewRoots     = nbRoots - nbRootsSave;
#ifndef PW_SILENT
                if (verbose>=level)
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, nb roots missing before: %lu, nb new roots after: %lu\n", 
                                                 *m, nbMissingRoots, nbNewRoots);
#endif
                *m = _pwpoly_isolate_piecewise_get_next_m_NEW( *m, nbMissingRoots, nbNewRoots );
#ifndef PW_SILENT
                if (verbose>=level)
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, next m: %lu\n", old_m, *m );
#endif
            } else { /*solve in not already solved sectors */
                nbRoots = _pwpoly_isolate_m_app_global( newroots, nbNroots, missed, nbmissed, app, deg, solver PW_VERBOSE_CALL(verbose)  );
                
                nbMissingRoots = deg - nbRootsSave;
                nbNewRoots     = nbRoots - nbRootsSave;
                
                /* actualize nbRootsInDomain and nbRootsInResult */
                nbRootsInDomain=0;
                nbRootsInResult=0;
                for (slong i=0; i<nbRoots; i++)
                    if (domain_acb_intersect_domain(dom, root_enclosureref(newroots+i) ) ) {
                        nbRootsInDomain++;
                        if ( domain_acb_is_strictly_in_domain( dom, root_enclosureref(newroots+i) ) )
                            nbRootsInResult++;
                        else if ( (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                                || pwpoly_width_less_eps( root_enclosureref(newroots+i), log2eps, PW_GOAL_PREC_RELATIVE(goal) ) )
                            nbRootsInResult++;
                    }
                /* puts roots intersecting the domain at the beginning of newroots */
#ifdef PW_CHECK_INTERRUPT
                if (!pwpoly_interrupt(0))
#endif
                root_get_roots_intersecting_domain( newroots, nbRoots, dom );
#ifndef PW_SILENT
                if (verbose>=level)
                    printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, nb roots missing before: %lu, nb new roots after: %lu\n", 
                                                 *m, nbMissingRoots, nbNewRoots);
#endif                
                if ( (nbRoots < deg) && (nbRootsInResult<goalNbRoots) ) { 
                    /* increase m and continue solving */
                    *m = _pwpoly_isolate_piecewise_get_next_m_NEW( *m, nbMissingRoots, nbNewRoots );
#ifndef PW_SILENT
                    if (verbose>=level)
                        printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, next m: %lu\n", old_m, *m );
#endif
                }
                /* move newroots in oldroots */
                if ( (oldroots!=NULL) && (oldallocsize>0) ) /* should not happen? except if roots are missing whitout any missed root... */
                    root_vec_clear(oldroots, oldallocsize);
                oldroots=newroots;
                oldallocsize=newallocsize;
                newroots=NULL;
                newallocsize=0;
                
            }
            
            nbRootsSave=nbRoots;
        }
#ifndef PW_SILENT        
        if (verbose>=level) {
#ifdef PW_PROFILE
            double clicks_in_solve = (clock() - start_solve);
            printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, %lu/%lu isolated roots, time: %lf s\n", old_m, nbRoots, deg, clicks_in_solve/CLOCKS_PER_SEC );
#else
            printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, %lu/%lu isolated roots\n", old_m, nbRoots, deg );
#endif
            printf("                                                      nbRootsInDomain: %ld, nbRootsInResult: %ld, goalNbRoots: %ld\n", nbRootsInDomain, nbRootsInResult, goalNbRoots );
            printf("                                                      nb of missed roots: %lu\n", *nbmissed );
            
            if (PW_GOAL_MUST_APPROXIMATE(goal)) {
                slong min, max;
                root_log2width_minmax( &min, &max, oldroots, nbRoots, PW_GOAL_PREC_RELATIVE(goal) );
                printf("_pwpoly_isolate_piecewise_depth_pw_polynomial_domain: m: %lu, log2widths of oldroots: min: %ld, max: %ld (target: %ld)\n", old_m, min, max, log2eps);
            }
        }
#endif 
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif
        /* copy the natural clusters of oldroots in roots */
        for (slong i=0; i<nbRoots; i++) {
            acb_set( roots+nbAlreadyIn+i, root_enclosureref( oldroots+i ) );
            /* since all enclosures in roots are natural isolator */
            /* if coeffs are real, enclosure contains real axis => the root in the enclosure is real */
            if ( realCoeffs && ( arb_contains_zero( acb_imagref(roots+nbAlreadyIn+i) ) ) )
                arb_zero( acb_imagref(roots+nbAlreadyIn+i) );
        }
#ifndef PW_NO_INTERFACE 
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif
        if (covrFile != NULL) {
            int write = !((nbRoots<deg) && (nbRootsInResult<goalNbRoots) && (c*(*m) < (ulong)max_m));
            if (write) {
                _pw_covering_with_points_domain_gnuplot( covrFile, pw_approximation_coveringref(app), roots, (slong)(nbRoots+nbAlreadyIn), dom );
            }
        }
#endif        
        pw_approximation_clear(app);
    
    }
    
    /* clear newroots and oldroots */
    if ( (newroots!=NULL) && (newallocsize>0) )
        root_vec_clear( newroots, newallocsize );
    newallocsize=0;
    if ( (oldroots!=NULL) && (oldallocsize>0) )
        root_vec_clear( oldroots, oldallocsize );
    oldallocsize=0;
    
    if (nbRoots<deg)
        *m = old_m;
    
    return nbRoots;
}

#ifdef PWPOLY_HAS_EAROOTS
int pwpoly_earoots_isolate_pw_polynomial( acb_ptr roots, pw_polynomial_t poly,
                                          int nbMaxItts PW_VERBOSE_ARGU(verbose)   ) {
#ifndef PW_SILENT    
    int level = 1;
#endif
    
    int res = 1;
    
#ifdef PW_PROFILE
    clock_t start_solve = clock();
#endif
    
    res = pw_polynomial_compute_double_approximation( poly );
//     res = pw_polynomial_compute_double_approximation_balanced( poly );
    
    if (res<1) {
#ifndef PW_SILENT
        if (verbose>=level) {
            if (res==PWPOLY_SET_ROUND_NEAREST_FAILED)
                printf("pwpoly_earoots_isolate_pw_polynomial: setting rounding to nearest failed!!!\n");
            if (res==PWPOLY_DOUBLE_EXCEPTION)
                printf("pwpoly_earoots_isolate_pw_polynomial: double exception!!!\n");
            else
                printf("pwpoly_earoots_isolate_pw_polynomial: input poly does not fit in double\n");
        }
#endif
        res = PWPOLY_APPROX_DOUBLE_INFINITE;
        return res;
    }
    
    slong len = pw_polynomial_length(poly);
    int realCoeffs = pw_polynomial_is_real(poly);
    res = _pw_solving_earoots_global_dpoly( roots,
                                           pw_polynomial_Dcoeffref(poly), pw_polynomial_Dabersref(poly), len, 
                                           -PWPOLY_DEFAULT_PREC, realCoeffs, nbMaxItts PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT    
    if (verbose>=level) {
            printf("pwpoly_earoots_isolate_pw_polynomial: res = %d\n", res);
    }
#endif    
#ifdef PW_PROFILE
    clicks_in_earoots_global+=(clock() - start_solve);
#endif
    
    return res;
}

/* returns either -1: failed       */
/* or the number of roots in roots */
/* assume 0 is not a root of poly  */
/* assume goalNbRoots > 0          */
slong _pwpoly_solve_pw_polynomial_earoots( acb_ptr roots, slong * mults, pw_polynomial_t poly,
                                           int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                                           int nbMaxItts PW_VERBOSE_ARGU(verbose)  ) {
    
#ifndef PW_SILENT    
    int level = 1; 
#endif
    slong len = pw_polynomial_length(poly);
    slong nbroots = 0;
    int resEaroots = pwpoly_earoots_isolate_pw_polynomial( roots, poly, nbMaxItts PW_VERBOSE_CALL(verbose)   );
    
    if (resEaroots==1)
        nbroots = len-1;
#ifndef PW_SILENT        
    if (verbose>=level) {
        printf("_pwpoly_solve_pw_polynomial_earoots: %lu/%lu isolated roots (res: %d)\n", nbroots, len-1, resEaroots );
    }
#endif    
    
    if (nbroots==0)
        return -1;
    
    /* set all the mults to 1 */
    for (slong i=0; i<len-1; i++)
        mults[i]=1;

#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif    
    if (domain_is_C(dom)==0) {
        /* filter the roots intersecting the domain */
        nbroots = pwpoly_get_roots_intersecting_domain( roots, mults, nbroots, dom );
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial_earoots: number of roots in domain: %ld, goalNbRoots: %ld\n", nbroots, goalNbRoots);
        }
#endif
    }
    
    slong first=0, lenToRefine=0;
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    if ( PW_GOAL_MUST_APPROXIMATE(goal) ) {
        /* sort roots in domain by increasing width */
        pwpoly_sort_increasing_width( roots, nbroots, PW_GOAL_PREC_RELATIVE(goal) );
        /* get the first index of root to refine */
        while ( (first < nbroots) 
             && (pwpoly_width_less_eps( roots + first, log2eps, PW_GOAL_PREC_RELATIVE(goal) ))) {
                first++;
        }
        lenToRefine = nbroots - first;
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial_earoots: length to refine: %lu, first index to refine: %ld\n", lenToRefine, first);
        } 
#endif
    }
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    if (lenToRefine>0) {
#ifdef PW_PROFILE
        clock_t start_CoCo = clock();
#endif       
        /* should never fail, ie, nbRootsRefined should be equal the number of roots refined in the domain */         
        slong nbRootsInDomain = solver_CoCo_refine_pw_polynomial( roots + first, mults + first, lenToRefine, 
                                                                  poly, log2eps, goal, dom, goalNbRoots, 1 PW_VERBOSE_CALL(verbose)  );
        nbroots = first + nbRootsInDomain;
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial_earoots: nb of roots refined in domain: %ld, nbroots: %ld, goalNbRoots: %ld\n", nbRootsInDomain, nbroots, goalNbRoots);
        }
#endif
#ifdef PW_PROFILE
        clicks_in_CoCo_refine = (clock() - start_CoCo);
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial_earoots: time in refine with subdivision solver: %lf s\n", clicks_in_CoCo_refine/CLOCKS_PER_SEC);
        }
#endif
#endif           
    }
    
    return nbroots;
}

slong _pwpoly_solve_pw_polynomial_earoots_standalone( acb_ptr roots, slong * mults, pw_polynomial_t poly,
                                                      int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                                                      int nbMaxItts PW_VERBOSE_ARGU(verbose)  ) {
    
#ifndef PW_SILENT    
    int level = 1; 
#endif
    
    slong len = pw_polynomial_length(poly);
    /* cap goalNbRoots */
    goalNbRoots = PW_CAP_GOALNBROOTS(goalNbRoots,(len-1));
    
    /* remove root zero and its multiplicity */
    slong multOfZero = pw_polynomial_remove_root_zero(poly);
    slong nlen = pw_polynomial_length(poly);
    slong offsetInRoots = 0;
    if ( (multOfZero>0) && (domain_contains_zero(dom)) ) {
        acb_zero( roots + 0 );
        mults[0] = multOfZero;
        offsetInRoots = 1;
        goalNbRoots = PWPOLY_MAX( 0, goalNbRoots - multOfZero );
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial_earoots_standalone: 0 is a root of multiplicity %ld in domain, %ld roots still to find \n", multOfZero, goalNbRoots );
        }
#endif
    }
    goalNbRoots = PW_CAP_GOALNBROOTS(goalNbRoots,(nlen-1));
    
    if (goalNbRoots==0) {
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial_earoots_standalone: goalNbRoots is 0: return \n" );
        }
#endif
        return offsetInRoots;
    }
    
    /* initialize multiplicities */
    for (slong i=offsetInRoots; i< len-1; i++)
        mults[i]=1;
    
    slong nbroots = -1;
    slong resEaroots = _pwpoly_solve_pw_polynomial_earoots( roots + offsetInRoots, mults + offsetInRoots, poly,
                                                            goal, log2eps, dom, goalNbRoots, nbMaxItts PW_VERBOSE_CALL(verbose)  );
    
    /* if resEaroots is -1, go ahead */
    /* if >=0, solving succeed and there is resEaroots roots in the domain */
    if (resEaroots>=0) {
        nbroots = resEaroots + offsetInRoots;
    }
    
    return nbroots;
}
#endif

slong _pwpoly_solve_pw_polynomial( acb_ptr roots, slong * mults, ulong *m, pw_polynomial_t poly,
                                  int goal, slong log2eps, domain_t dom, slong goalNbRoots,
                                  const ulong c, const fmpq_t scale, const fmpq_t b,
                                  int solver PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(covrFile)) {
#ifndef PW_SILENT    
    int level = 1; 
#endif
    slong len = pw_polynomial_length(poly);
    /* cap goalNbRoots */
    goalNbRoots = PW_CAP_GOALNBROOTS(goalNbRoots,(len-1));
    slong goalNbRootsSave = goalNbRoots;
    
    /* remove root zero and its multiplicity */
    slong multOfZero = pw_polynomial_remove_root_zero(poly);
    slong nlen = pw_polynomial_length(poly);
    slong offsetInRoots = 0;
    if ( (multOfZero>0) && (domain_contains_zero(dom)) ) {
        acb_zero( roots + 0 );
        mults[0] = multOfZero;
        offsetInRoots = 1;
        goalNbRoots = PWPOLY_MAX( 0, goalNbRoots - multOfZero );
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: 0 is a root of multiplicity %ld in domain, %ld roots still to find \n", multOfZero, goalNbRoots );
        }
#endif
    }
    goalNbRoots = PW_CAP_GOALNBROOTS(goalNbRoots,(nlen-1));
    
    if (goalNbRoots==0) {
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: goalNbRoots is 0: return \n" );
        }
#endif
        return offsetInRoots;
    }
    
    /* initialize multiplicities */
    for (slong i=offsetInRoots; i< len-1; i++)
        mults[i]=1;
    
    
    int realCoeffs = pw_polynomial_is_real(poly);
    ulong sqrt_len = (ulong) ceil( sqrt( (double)nlen ) );
    slong max_m = c*PWPOLY_MAX( *m, sqrt_len ) + 20;
    
    slong nbRoots = 0;
    slong nbRootsInDomain=0; 
    
#ifdef PWPOLY_HAS_EAROOTS
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    /* try solving with earoots */
    if ( nlen<=3500 && PW_SOLVING_USE_DOUBLES(solver) && PW_SOLVING_USE_INIT_EA(solver) ) {
        
        slong resEaroots = _pwpoly_solve_pw_polynomial_earoots( roots + offsetInRoots, mults + offsetInRoots, poly,
                                                                goal, log2eps, dom, goalNbRoots, 100 PW_VERBOSE_CALL(verbose)  );
        
        /* if resEaroots is -1, go ahead */
        /* if >=0, solving succeed and there is resEaroots roots in the domain */
        if (resEaroots>=0) {
            nbRoots = resEaroots + offsetInRoots;
            return nbRoots;
        }
        
    }
#endif
    
    /* if fail, use piecewise approximation */
    acb_ptr missed=NULL;
    ulong nbmissed = 0;
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    if (nbRoots < goalNbRoots) {
        missed = _acb_vec_init( nlen );

        if (domain_is_C(dom)) {
            nbRoots = _pwpoly_isolate_piecewise_depth_pw_polynomial_global( roots, offsetInRoots, &missed, &nbmissed,
                                                                            m, max_m, poly, log2eps, goal, goalNbRoots,
                                                                            c, scale, b, solver PW_VERBOSE_CALL(verbose) PW_FILE_CALL(covrFile));
            nbRootsInDomain=nbRoots;
        } else {
            nbRoots = _pwpoly_isolate_piecewise_depth_pw_polynomial_domain( roots, offsetInRoots, &missed, &nbmissed,
                                                                            m, max_m, poly, log2eps, goal, dom, goalNbRoots,
                                                                            c, scale, b, solver PW_VERBOSE_CALL(verbose) PW_FILE_CALL(covrFile));
            /* assume the isolating boxes having non empty intersection with the domain are in the beginning */
            /* get nbRootsInDomain */
            nbRootsInDomain=0;
            for (slong i=0; i<nbRoots; i++)
                if (domain_acb_intersect_domain(dom, roots+(offsetInRoots+i) ) ) {
                    nbRootsInDomain++;
                }
        }
            
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: final value for m: %lu, max_m: %ld, %lu/%lu isolated roots, %lu candidates\n", *m, max_m, nbRoots, nlen-1, nbmissed );
            if (!domain_is_C(dom)) {
                printf("                             nbRootsInDomain: %ld, goalNbRoots: %ld\n", nbRootsInDomain, goalNbRoots );
            }
        }
#endif
#ifdef PW_PROFILE
        clicks_in_solve_approx = clicks_in_solve_approx - clicks_in_approx_annulii - clicks_in_covering
                                 + clicks_in_refine_compApp;
#endif
    }
    /* get the number of missed roots in domain */
    slong nbMissedInDomain = 0;
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    nbMissedInDomain = pwpoly_get_points_intersecting_domain( missed, nbmissed, dom );
//     printf("nb missed in domain: %ld, nb missed: %ld\n", nbMissedInDomain, nbmissed);
    /* clear missed */
    if (missed!=NULL) {
        ulong missed_size = nlen*(nbmissed/nlen + 1);
        _acb_vec_clear( missed, missed_size );
        nbmissed=0;
    }

#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    if ( PW_GOAL_MUST_APPROXIMATE(goal) ) {
#ifndef PW_SILENT
        if (verbose>=level) {
            slong min, max;
            pwpoly_log2width_minmax_domain( &min, &max, roots + offsetInRoots, nbRoots, goal, dom );
            printf("_pwpoly_solve_pw_polynomial: log2widths of roots in  domain: min: %ld, max: %ld (target: %ld)\n",
                    min, max, log2eps);
        }
#endif
        slong lenToRefine, indToRefine;
        pwpoly_get_bounds_to_refine( &indToRefine, &lenToRefine, roots+offsetInRoots, nbRootsInDomain, goal, log2eps, realCoeffs&&domain_is_symetric_real_line(dom) );
        slong goalNbRootsToRefine = PWPOLY_MAX(0, goalNbRoots - indToRefine);
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: first index to refine: %ld, length to refine: %lu, goalNbRootsToRefine: %ld\n", 
                   indToRefine, lenToRefine, goalNbRootsToRefine);
        } 
#endif      
        if (goalNbRootsToRefine>0) {
#ifdef PW_PROFILE
            clock_t start_refine = clock();
#endif
            if (domain_is_C(dom))
                _pwpoly_refine_piecewise_global( roots+(offsetInRoots+indToRefine), goalNbRootsToRefine, m, max_m, poly, goal, log2eps, c, scale, b PW_VERBOSE_CALL(verbose)  );
            else {
                slong nbRInDom = _pwpoly_refine_piecewise_domain( roots+(offsetInRoots+indToRefine), lenToRefine, m, max_m, poly, goal, log2eps, dom, goalNbRootsToRefine, c, scale, b PW_VERBOSE_CALL(verbose)  );
                nbRootsInDomain = indToRefine + nbRInDom;
            }
#ifdef PW_PROFILE
            clicks_in_refine_piecewise += (clock() - start_refine);
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pwpoly_solve_pw_polynomial: time in refine with approximations: %lf s\n", clicks_in_refine_piecewise/CLOCKS_PER_SEC);
            }
#endif
#endif
#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif            
            pwpoly_get_bounds_to_refine( &indToRefine, &lenToRefine, roots+offsetInRoots, nbRootsInDomain, goal, log2eps, realCoeffs&&domain_is_symetric_real_line(dom) );
            goalNbRootsToRefine = PWPOLY_MAX(0, goalNbRoots - indToRefine);
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pwpoly_solve_pw_polynomial: first index to refine: %ld, length to refine: %lu, goalNbRootsToRefine: %ld\n", indToRefine, lenToRefine, goalNbRootsToRefine);
            } 
#endif
        }
        
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif
        if (goalNbRootsToRefine>0) {
#ifdef PW_PROFILE
            clock_t start_CoCo = clock();
#endif
            /* should never fail, ie, nbRootsRefined should be equal the number of roots refined in the domain */         
            slong nbRootsRefined = solver_CoCo_refine_pw_polynomial( roots + (offsetInRoots+indToRefine), 
                                                                     mults + (offsetInRoots+indToRefine), lenToRefine, 
                                                                     poly, log2eps, goal, dom, goalNbRootsToRefine, 1 PW_VERBOSE_CALL(verbose)  );
            nbRootsInDomain = indToRefine + nbRootsRefined;
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pwpoly_solve_pw_polynomial: nb of roots refined: %ld, nb of roots intersecting domain: %ld\n",
                       nbRootsRefined, nbRootsInDomain);
            }
#endif
#ifdef PW_PROFILE
            clicks_in_CoCo_refine = (clock() - start_CoCo);
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pwpoly_solve_pw_polynomial: time in refine with subdivision solver: %lf s\n", clicks_in_CoCo_refine/CLOCKS_PER_SEC);
            }
#endif
#endif
        }
    }
    
    /* filter roots in domain */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    if ( !domain_is_C(dom) ) {
        /* filter roots in domain */
        nbRootsInDomain = pwpoly_get_roots_intersecting_domain( roots+offsetInRoots, mults+offsetInRoots, nbRoots, dom );
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: number of roots in domain after filtering: %ld, goalNbRoots: %ld\n", nbRootsInDomain, goalNbRoots);
        }
#endif
    }
    
    slong nbClusts = nbRoots;
    
    /* in case where a domain is given, choose between EA iterations and subdivision */
    int useEA = (nbRootsInDomain + nbMissedInDomain) > (nlen-1)/10;
    
    
    /* if fail, or if solving domain is C, use multiprecision EA solver */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    if ( PW_SOLVING_USE_FINL_EA(solver) && (
         (domain_is_C(dom) && (nbRoots < goalNbRoots))
        || ((nbRoots < (nlen-1))&&(nbRootsInDomain < goalNbRoots)&&useEA)  ) ) {
        
#ifdef PW_PROFILE
        clock_t start_EAmp = clock();
#endif
        
        slong nbMaxItts = 500;
        slong nbIttsBeforeIncreasePrec = 50;
        
        slong prec = PWPOLY_DEFAULT_PREC;
        slong working_m = (slong) pw_approximation_get_working_m(*m, nlen);
        while (prec<2*working_m)
            prec*=2;
//         prec = PWPOLY_DEFAULT_PREC;
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: call mp EA solver with %ld isolated roots and max %ld itts, working m: %lu, %ld first itts at prec: %lu\n",
                nbRoots, nbMaxItts, working_m, nbIttsBeforeIncreasePrec, prec);
        }
#endif
        slong res = pw_EA_solve_pw_polynomial ( roots+offsetInRoots, mults+offsetInRoots, &nbClusts, nbRoots, poly, goal, log2eps, dom, goalNbRoots, &prec, nbMaxItts, nbIttsBeforeIncreasePrec PW_VERBOSE_CALL(verbose)  PW_FILE_CALL(NULL));
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: res of mp EA solver: %ld, nbOfClusters: %ld, prec: %ld\n",
               res, nbClusts, prec);
        }
#endif
#ifdef PW_PROFILE
        clicks_in_EA_mp = (clock() - start_EAmp);
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: time in mp EA solver: %lf s\n", clicks_in_EA_mp/CLOCKS_PER_SEC);
        }
#endif
#endif

        if ( (res>=nbMaxItts)&&(nbClusts>1) ) {
//             /* for 0<=i<nbClusts, letting B(ci,ri) = roots+i, D(ci,ri) is a natural clusters of mults[i] roots */
//             /* any root of poly is included in a D(ci,ri) */
//             /* -> call solver CoCo refine to finish the solving */
// #ifndef PW_SILENT
//             if (verbose>=level) {
//                 printf("_pwpoly_solve_pw_polynomial: call subdivision solver with output of mp EA solver\n");
//             }
// #endif
// #ifdef PW_PROFILE
//             clock_t start_CoCo = clock();
// #endif
//             nbClusts = solver_CoCo_refine_pw_polynomial( roots + offsetInRoots, mults + offsetInRoots, nbClusts, 
//                                                          poly, log2eps, goal, 1 PW_VERBOSE_CALL(verbose)  );
//             
//             nbroots=nlen-1;
//             
// #ifdef PW_PROFILE
//             clicks_in_CoCo_global = (clock() - start_CoCo);
// #ifndef PW_SILENT
//             if (verbose>=level) {
//                 printf("_pwpoly_solve_pw_polynomial: time in subdivision solver: %lf s\n", clicks_in_CoCo_global/CLOCKS_PER_SEC);
//             }
// #endif
// #endif
            nbRoots = 0;
        } else if ( res < 0 ) {
            /* the D(ci,ri) are pairwise disjoints but not natural */
            /* call solver CoCo over C */
            nbRoots = 0;
        } else {
            /* solver succeeded ! */
            nbRoots=nlen-1;
        }
        
    }
    
    /* if fail, use solver CoCo */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    if (  (domain_is_C(dom)&&(nbRoots < goalNbRoots))
       || ((nbRoots < (nlen-1))&&(nbRootsInDomain < goalNbRoots)) ){

#ifdef PW_PROFILE
        clock_t start_CoCo = clock();
#endif         
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: call subdivision solver\n");
        }
#endif        
        nbClusts = solver_CoCo_solve_pw_polynomial( roots+offsetInRoots, mults+offsetInRoots, nbRoots, poly, log2eps, goal, dom, goalNbRoots PW_VERBOSE_CALL(verbose)  );
        
        
#ifdef PW_PROFILE
        clicks_in_CoCo_global = (clock() - start_CoCo);
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: time in subdivision solver: %lf s\n", clicks_in_CoCo_global/CLOCKS_PER_SEC);
        }
#endif
#endif
    } 
    
    nbClusts += offsetInRoots;
    
    /* if domain dom is not C, keep only the roots intersecting the domain */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    nbClusts = pwpoly_get_roots_intersecting_domain( roots, mults, nbClusts, dom );
    
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif    
    if (goalNbRootsSave < len-1) {
        slong sMults=0;
        slong retNbC=0;
        while ((sMults<goalNbRootsSave)&&(retNbC<nbClusts)) {
            sMults += mults[retNbC];
            retNbC++;
        }
        nbClusts=retNbC;
    }
    
    return nbClusts;
    
}

slong _pwpoly_refine_pw_polynomial( acb_ptr roots, slong * mults, pw_polynomial_t poly,
                                    int goal, slong log2eps, domain_t dom, slong multInDomain PW_VERBOSE_ARGU(verbose)) {

#ifndef PW_SILENT    
    int level = 1; 
#endif 
    slong len = pw_polynomial_length(poly);
    /* filter domain and multInDomain arguments */
    if ( !domain_is_finite(dom) || domain_is_point(dom) || (multInDomain<=0) || (multInDomain>=len) ){
        return -1;
    }
//     slong multInDomainSave = multInDomain;
    /* remove root zero and its multiplicity */
    slong multOfZero = pw_polynomial_remove_root_zero(poly);
//     slong nlen = pw_polynomial_length(poly);
    slong offsetInRoots = 0;
    if ( (multOfZero>0) && (domain_contains_zero_in_interior(dom)) ) {
        acb_zero( roots + 0 );
        mults[0] = multOfZero;
        offsetInRoots = 1;
        multInDomain = PWPOLY_MAX( 0, multInDomain - multOfZero );
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_refine_pw_polynomial: 0 is a root of multiplicity %ld in domain, %ld roots still to find \n", multOfZero, multInDomain );
        }
#endif
    }
    
    slong nbClusts = 0;
    
    if (multInDomain==0) {
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pwpoly_solve_pw_polynomial: multInDomain is 0: return \n" );
        }
#endif
        return offsetInRoots;        
    }
    
    /* call solver_CoCo in the domain */
    nbClusts = solver_CoCo_refine_domain_pw_polynomial( roots + offsetInRoots, mults + offsetInRoots,
                                                        poly, goal, log2eps, dom, multInDomain PW_VERBOSE_CALL(verbose)  );
    
    return nbClusts + offsetInRoots;
}

/* NOT USED */
/* let app be a piecewise approximation of f of degree d       */
/* let x   be a complex point                                  */
/* sets radius so that the disc centered in x of radius radius */
/* is guaranteed to contain at least one root of f             */
/* uses the formula d*|f(x)|/|f'(x)|                           */
/* where f,f' are evaluated with app (possibly appprime)       */
// int  _pw_solving_get_inclusion_disc( arb_t radius, const pw_approximation_t app, 
//                                      const acb_t x PW_VERBOSE_ARGU(verbose)  ) {
//     
// #ifdef PW_PROFILE
//     clock_t start = clock();
// #endif
//     
//     int res = PW_INDETERMI;
//     
//     int level = 3;
//     
//     acb_t fx, fpx;
//     acb_init(fx);
//     acb_init(fpx);
//     arb_t absfx, absfpx;
//     arb_init(absfx);
//     arb_init(absfpx);
//     
//     slong prec = pw_approximation_precref(app);
// 
//     pw_approximation_evaluate2_acb( fx, fpx, app, x PW_VERBOSE_CALL(verbose)  );
//     
// #ifdef PWPOLY_DEBUG
//     pw_approximation_evaluate2_acb( fx, fpx, app, x PW_VERBOSE_CALL(verbose)  );
// #endif
//     /* compute d|f(x)|/|f'(x)| */
//     acb_abs(absfx, fx, prec);
//     acb_abs(absfpx, fpx, prec);
//     arb_div(radius, absfx, absfpx, prec);
//     arb_mul_ui(radius, radius, pw_approximation_lenref(app)-1, prec);
//     
//     if (verbose>=level) {
//         printf("--------------_pw_solving_get_inclusion_disc-------------------\n");
//         printf("x     : "); acb_printd(x, 10); printf("\n");
//         printf("fx    : "); acb_printd(fx, 10); printf("\n");
//         printf("fpx   : "); acb_printd(fpx, 10); printf("\n");
//         printf("radius: "); arb_printd(radius, 10); printf("\n");
//         printf("---------------------------------------------------------------\n");
//     }
//     
//     if (arb_is_finite(radius)){
//         res = PW_INCLUDING;
//     } else {
//         arb_indeterminate( radius );
//     }
//     
//     acb_clear(fx);
//     acb_clear(fpx);
//     arb_clear(absfx);
//     arb_clear(absfpx);
//     
// #ifdef PW_PROFILE
//     clicks_in_inclusion += (clock() - start);
// #endif
//     
//     return res;
// }
// 
// /* sets dest to fptr rounded up to 2^-prec:                           */
// /* let e:=[-2^{-prec+1}, 2^{-prec+1}] + I*[-2^{-prec+1}, 2^{-prec+1}] */
// /* each coeff contained in e is set to e                              */
// void _pwpoly_round_acb_poly( acb_poly_t dest, acb_srcptr fptr, slong len, slong prec ){
//     
//     arb_t error;
//     arb_init(error);
//     arb_one(error);
//     arb_mul_2exp_si(error, error, - (prec+1) );
//     acb_t ball;
//     acb_init(ball);
//     acb_zero(ball);
//     acb_add_error_arb( ball, error );
//     
//     acb_poly_fit_length(dest, len);
//     _acb_poly_set_length(dest, len);
//     
//     for (slong i=0; i<len; i++)
//         if (acb_contains(ball, fptr+i))
//             acb_set( dest->coeffs+i, ball );
//         else
//             acb_set( dest->coeffs+i, fptr+i );
//         
//     acb_clear(ball);
//     arb_clear(error);
//     
// }
// 
// slong _pwpoly_isolate_piecewise_breadth_pw_polynomial( acb_ptr roots, slong nbAlreadyIn, acb_ptr *missed, ulong *nbmissed,
//                                                        ulong * m, slong max_m, pw_polynomial_t poly, 
//                                                        const ulong c, const fmpq_t scale, const fmpq_t b,
//                                                        int solver PW_VERBOSE_ARGU(verbose) , FILE * covrFile ) {
//     
//     int level = 1;
//     slong len = pw_polynomial_length(poly);
//     int realCoeffs = pw_polynomial_is_real(poly);
//     
// #ifdef PW_PROFILE
//     clock_t start_solve, start_solve_missed;
// #endif
//     
//     slong nbroots = 0;
//     
//     root_ptr newroots=NULL, oldroots=NULL;
//     ulong newallocsize=0, oldallocsize=0;
//     
//     while ( (nbroots<(len-1)) && (c*(*m) < (ulong)len) ) {
//     
//         int last_m = (2*c*(*m)) >= (ulong)len;
//         
//         if ( ( (*m) > 30 )&& PW_SOLVING_USE_EAROOTS(solver) ) {
//             int earoots_val = 0x1<<PW_SOLVING_EAROOTS;
//             solver-=earoots_val;
//         }
//         
//         if (verbose>=level)
//             printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, last m? %d, nbroots: %ld, nbmissed: %lu, solver: %d\n", 
//                                                  *m, last_m, nbroots, *nbmissed, solver);   
//         
//         pw_approximation_t app;
//         pw_approximation_init_pw_polynomial( app, poly, *m, c, scale, b);
//         
// #ifdef PW_PROFILE
//         start_solve = clock();
// #endif
//         
//         /* allocate newroots */
//         ulong cworm  = pw_approximation_cref(app)*pw_approximation_working_mref(app);
//         newallocsize = PWPOLY_MAX( (ulong)(2*len), (ulong)(len + 2*cworm));
//         newroots     = root_vec_init(newallocsize);
//         
//         /* re-use previous result */
//         if (*nbmissed>0) {
// #ifdef PWPOLY_REUSE_PREVIOUS
//             pw_sector_list_t sectors;
//             pw_sector_list_init(sectors);
//             /* locate missed roots of old app in sectors of new covering */
// #ifdef PW_PROFILE
//             clock_t start_locate_in_sectors = clock();
// #endif
//             ulong nbmissedsectors = pw_covering_locate_points( sectors, pw_approximation_coveringref(app), *missed, *nbmissed, 
//                                                                pw_approximation_precref(app) );
//             if (verbose>=level) {
// #ifdef PW_PROFILE
//                 double clicks_in_locate_in_sectors = (clock() - start_locate_in_sectors);
//                 printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, nb sector missed: %lu/%lu, time in locate: %lf s\n", 
//                        *m, nbmissedsectors, pw_approximation_Ksumref(app), clicks_in_locate_in_sectors/CLOCKS_PER_SEC );
// #else
//                 printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, nb sector missed: %lu/%lu\n", 
//                        *m, nbmissedsectors, pw_approximation_Ksumref(app));
// #endif
//             }
//             
//             /* clear and re-init missed */
//             ulong missed_size = len*((*nbmissed)/len + 1);
// //          printf("nbmissed: %lu, missed_size: %lu\n", nbmissed, missed_size);
//             _acb_vec_clear( *missed, missed_size );
//             *missed = _acb_vec_init( len );
//             *nbmissed = 0;
//         
//             /* solve in missed sectors */
// #ifdef PW_PROFILE
//             start_solve_missed = clock();
// #endif
//             ulong nbNroots = _pwpoly_isolate_m_app_in_sectors( newroots, 0, missed, nbmissed, app, sectors, solver PW_VERBOSE_CALL(verbose)  );
//             if (verbose>=level) {
// #ifdef PW_PROFILE
//                 double clicks_in_solve_missed = (clock() - start_solve_missed);
//                 printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, %lu/%lu isolated roots in missed sectors, time: %lf s\n", *m, nbNroots, len-1, clicks_in_solve_missed/CLOCKS_PER_SEC );
// #else
//                 printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, %lu/%lu isolated roots in missed sectors\n", *m, nbNroots, len-1 );
// #endif        
//                 printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, nb of missed roots: %lu\n", *m, *nbmissed );
//             }
//             
//             if ( (nbNroots<(ulong)(len-1))&&((nbNroots + nbroots) >= (ulong)(len-1)) ) {
//                 /* copy the new roots in old roots and eliminate doubles */
//                 /* both nbroots and nbNroots are < nlen        */
//                 /* and oldroots contains at least 2*nlen slots */
//                 for (ulong i=0; i<nbNroots; i++)
//                     root_set( oldroots + (nbroots + i), newroots + i );
// #ifdef PW_PROFILE
//                 clock_t start_merge = clock();
// #endif                
//                 nbroots = _pw_roots_merge_overlaps(oldroots, nbNroots + nbroots, pw_approximation_precref(app));
// #ifdef PW_PROFILE
//                 clicks_in_merge += (clock() - start_merge);
// #endif              
//                 if (verbose>=level) {
//                     printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, %lu distinct isolated roots after eliminate doubles\n", *m, nbroots);
//                 }
//                 
//                 if (nbroots == len-1) {
//                     /* clear newroots */
//                     root_vec_clear(newroots, newallocsize);
//                     /* set newroots to oldroots */
//                     newroots = oldroots;
//                     newallocsize = oldallocsize;
//                     oldroots=NULL;
//                     oldallocsize=0;
//                     nbNroots=nbroots;
//                 }
//             }
//             
//             /* clear oldroots */
//             if ( (oldroots!=NULL) && (oldallocsize>0) )
//                 root_vec_clear(oldroots, oldallocsize);
//             oldroots=NULL;
//             oldallocsize = 0;
//             /* continue with newroots*/
//             nbroots = nbNroots;
//             
//             pw_sector_list_clear(sectors);
// #else
//             /* clear and re-init missed */
//             ulong missed_size = nlen*((*nbmissed)/len + 1);
// //          printf("nbmissed: %lu, missed_size: %lu\n", nbmissed, missed_size);
//             _acb_vec_clear( *missed, missed_size );
//             *missed = _acb_vec_init( len );
//             *nbmissed = 0;
// #endif
//         }
// #ifndef PWPOLY_REUSE_PREVIOUS
//         nbroots = 0;
// #endif
//         
//         if (nbroots < len-1)
//             nbroots = _pwpoly_isolate_m_app( newroots, nbroots, missed, nbmissed, app, solver PW_VERBOSE_CALL(verbose)  );
//     
//         if (verbose>=level) {
// #ifdef PW_PROFILE
//             double clicks_in_solve = (clock() - start_solve);
//             printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, %lu/%lu isolated roots, time: %lf s\n", *m, nbroots, len-1, clicks_in_solve/CLOCKS_PER_SEC );
// #else
//             printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, %lu/%lu isolated roots\n", *m, nbroots, len-1 );
// #endif
//             printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, nb of missed roots: %lu\n", *m, *nbmissed );
//         }
//         
// //         if ( (nbroots==nlen-1) && (covrFile != NULL) )
// //             _pw_covering_with_points_gnuplot( covrFile, pw_approximation_coveringref(app), roots, nbroots + multOfZero );
//         
//         last_m = last_m || (nbroots==len-1) ;
// //         if ( last_m ) {
//         if (nbroots==len-1) {
//             /* copy the natural clusters of newroots in roots */
//             for (slong i=0; i<nbroots; i++) {
//                 acb_set( roots+nbAlreadyIn+i, root_enclosureref( newroots+i ) );
//                 /* since all enclosures in roots are natural isolator */
//                 /* if coeffs are real, enclosure contains real axis => the root in the enclosure is real */
//                 if ( realCoeffs && ( arb_contains_zero( acb_imagref(roots+nbAlreadyIn+i) ) ) )
//                     arb_zero( acb_imagref(roots+nbAlreadyIn+i) );
//             }
//             
//             if (covrFile != NULL) {
//                 _pw_covering_with_points2_gnuplot( covrFile, pw_approximation_coveringref(app), roots, (slong)(nbroots+nbAlreadyIn), *missed, *nbmissed );
//             }
//         }
//     
//         if (nbroots < len-1) {
//             if ( (oldroots!=NULL) && (oldallocsize>0) ) /* should not happen? except if roots are missing whitout any missed root... */
//                 root_vec_clear(oldroots, oldallocsize);
//             oldroots=newroots;
//             oldallocsize=newallocsize;
//             newroots=NULL;
//             newallocsize=0;
//         }
//         
//         pw_approximation_clear(app);
//     
//         if (nbroots<(len-1)) {
// //             (*m)*=2;
//             ulong old_m = *m;
//             *m = _pwpoly_isolate_piecewise_get_next_m( *m, nbroots, len, max_m, c );
//             if (verbose>=level)
//                 printf("_pwpoly_isolate_piecewise_breadth_pw_polynomial: m: %lu, next m: %lu\n", old_m, *m );
//         }
//     }
//     
//     /* clear newroots and oldroots */
//     if ( (newroots!=NULL) && (newallocsize>0) )
//         root_vec_clear( newroots, newallocsize );
//     newallocsize=0;
//     if ( (oldroots!=NULL) && (oldallocsize>0) )
//         root_vec_clear( oldroots, oldallocsize );
//     oldallocsize=0;
//     
//     return nbroots;
// }

// slong _pwpoly_refine_piecewise( acb_ptr roots, slong lenToRefineIn, slong nbroots, ulong *m, slong max_m, pw_polynomial_t poly,
//                                 int goal, slong log2eps, const domain_t dom,
//                                 const ulong c, const fmpq_t scale, const fmpq_t b,
//                                 int solver PW_VERBOSE_ARGU(verbose)  ) {
//     
//     int level = 1;
// //     slong len = pw_polynomial_length(poly);
//     int realCoeffs = pw_polynomial_is_real(poly);
//     slong nbRefined = 0;
//     slong nbMissed = 0;
//     /* assume the roots are sorted by increasing width, and lenToRefine is the number */
//     /* of trailing roots in roots to be refined */
//     slong lenToRefine = lenToRefineIn;
//     slong indToRefine = nbroots-lenToRefine;
//     
//     if (verbose>=level)
//         printf("_pwpoly_refine_piecewise begin: m: %lu, nb of roots to refine: %ld\n", *m, lenToRefine );
//         
//     if (lenToRefine==0)
//         return 0;
//     
//     acb_t nroot;
//     acb_init(nroot);
//     
//     /* try to increase m to reach asked precision */
//     if (log2eps<0) {
//         if ( (slong)(*m) >= -log2eps/2 )
//             (*m)*=2;
//         else
//             (*m) = (ulong)(-log2eps/2);
//     }
//     
//     while ( (c*(*m) < (ulong)max_m) && (lenToRefine>0) ) {
//     
//         if (verbose>=level)
//             printf("_pwpoly_refine_piecewise: m: %lu, nb of roots to refine: %ld\n", *m, lenToRefine );
//         
//         acb_ptr refined = _acb_vec_init(lenToRefine);
//         nbRefined = 0;
//     
//         acb_ptr missed = _acb_vec_init(lenToRefine);
//         nbMissed = 0;
//         
//         pw_approximation_t app;
//         pw_approximation_init_pw_polynomial( app, poly, *m, c, scale, b);
//         
//         for (slong i=indToRefine; i<nbroots; i++) {
//             acb_set(nroot, roots+i);
//             int resRefine = 0;
//             /* if imaginary part is zero, transform it into a square box */
//             if ( arb_is_zero( acb_imagref( roots+i ) ) ){
//                 mag_set( arb_radref(acb_imagref( nroot )), arb_radref(acb_realref( nroot )) );
//             }
//             if ( (realCoeffs==0) || (arb_is_negative(acb_imagref(roots+i))==0) ) {
//                 resRefine = _pwpoly_refine_acb_app( nroot, nroot, app PW_VERBOSE_CALL(verbose)  );
//                 if (resRefine) {
//                     acb_set(refined+nbRefined, nroot);
//                     nbRefined++;
//                 } else {
//                     acb_set(missed+nbMissed, roots+i);
//                     nbMissed++;
//                 }
//             }
//         }
//         pw_approximation_clear(app);
//         
//         if (verbose>=level)
//             printf("_pwpoly_refine_piecewise: m: %lu, nbRefined: %ld, nbMissed: %ld\n", *m, nbRefined, nbMissed );
//         
//         /* copy refined roots in roots, decide realness */
//         for (slong i=0; i<nbRefined; i++) {
// //             printf("_pwpoly_refine_piecewise: i: %ld, indToRefine: %ld\n", i, indToRefine );
//             acb_set( roots+indToRefine, refined+i );
//             if ( realCoeffs && ( arb_contains_zero( acb_imagref(roots+indToRefine) ) ) )
//                         arb_zero( acb_imagref(roots+indToRefine) );
//             indToRefine++;
//             if ( realCoeffs && arb_is_positive( acb_imagref( refined+i ) ) ) {
// //                 printf("_pwpoly_refine_piecewise: i: %ld, indToRefine: %ld\n", i, indToRefine );
//                 acb_conj( roots+indToRefine, refined+i );
//                 indToRefine++;
//             }
//         }
//         /* copy non-refined roots in roots, conjugate if necessary */
//         for (slong i=0; i<nbMissed; i++) {
//             acb_set( roots+indToRefine, missed+i);
//             indToRefine++;
//             if ( realCoeffs && arb_is_positive( acb_imagref( missed+i ) ) ) {
//                 acb_conj( roots+indToRefine, missed+i );
//                 indToRefine++;
//             }
//         }
//         _acb_vec_clear(missed, lenToRefine);
//         _acb_vec_clear(refined, lenToRefine);
//         /* sort roots by increasing width and get lenToRefine */
//         lenToRefine = pwpoly_sort_increasing_width_and_get_lenToRefine ( roots, nbroots, goal, log2eps, realCoeffs );
//         indToRefine = (nbroots)-lenToRefine;
//         /* increase m if necessary */
//         if (lenToRefine>0)
//             (*m)*=2;
//     }
//     acb_clear(nroot);
//     
//     if (lenToRefine>0)
//         *m = (*m)/2;
//     
//     if (verbose>=level)
//         printf("_pwpoly_refine_piecewise end  : m: %lu, nb of roots to refine: %ld\n", *m, lenToRefine );
//     
//     return lenToRefine;
// }
