/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_solving.h"

slong _pw_solving_tstar_unit_disc_be( double * poly, slong len, int * n, int nbMDLG, slong nbMsols PW_VERBOSE_ARGU(verbose) ){

#ifndef PW_SILENT    
    int level = 4;
#endif
    slong res = -1;
    /* save exception flags and re-init exceptions */
    fexcept_t except_save;
    fegetexceptflag (&except_save, FE_ALL_EXCEPT);
    feclearexcept (FE_ALL_EXCEPT);
    
    /* save rounding mode and set double rounding mode to NEAREST */
    int rounding_save = fegetround();
    int ressetnearest = fesetround (FE_TONEAREST);
    if (ressetnearest!=0) {
        res = PWPOLY_SET_ROUND_NEAREST_FAILED;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pw_solving_tstar_unit_disc_be: setting rounding to nearest failed!!!\n");
#endif
    }
    
    res = tstar_unit_disc_be( poly, len, n, nbMDLG, nbMsols );
    
    int double_exception = _pwpoly_test_and_print_exception(PW_PREAMB_CALL("_pw_solving_be_poly_oneDLGIteration_naive: ") PW_VERBOSE_CALL(verbose) );
    if (double_exception) {
        res = PWPOLY_DOUBLE_EXCEPTION;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pw_solving_tstar_unit_disc_be: double exception!!!\n");
#endif
    }
    
    /* restore exception flags and rounding mode */
    fesetexceptflag (&except_save, FE_ALL_EXCEPT);
    fesetround (rounding_save);
    
    return res;
}

/* if returns 1 then B(re, im, ae) does not intersect the unit disc */
int _be_no_intersection_with_unit_disc( double re, double im, double ae ) {
    
    double abs, abs_aber;
    _be_abs_aber( &abs, &abs_aber, re, im, ae );
    
    int res = _be_real_pos_gt( abs, abs_aber, 1.0, .0 );
    
    return res;
}


#ifdef PWPOLY_HAS_EAROOTS
slong _pw_solving_earoots( double * roots, double * poly, slong len, slong outputprec PW_VERBOSE_ARGU(verbose)  ) {
#ifndef PW_SILENT    
    int level = 3;
#endif
    slong res = 1;
    
    /* save exception flags and re-init exceptions */
    fexcept_t except_save;
    fegetexceptflag (&except_save, FE_ALL_EXCEPT);
    feclearexcept (FE_ALL_EXCEPT);
    
    /* save rounding mode and set double rounding mode to NEAREST */
    int rounding_save = fegetround();
    int ressetnearest = fesetround (FE_TONEAREST);
    if (ressetnearest!=0) {
        res = PWPOLY_SET_ROUND_NEAREST_FAILED;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pw_solving_earoots: setting rounding to nearest failed!!!\n");
#endif        
        /* restore exception flags and rounding mode */
        fesetexceptflag (&except_save, FE_ALL_EXCEPT);
        fesetround (rounding_save);
        
        return res;
    }
    
        
    double * Dcoeff = (double *) pwpoly_malloc ( 2*(len)*sizeof(double) );
    double * Dcaber = (double *) pwpoly_malloc (     len*sizeof(double) );
    double * Droots = (double *) pwpoly_malloc ( 2*(len-1)*sizeof(double) );
    double * Dabers = (double *) pwpoly_malloc (   (len-1)*sizeof(double) );
    
    for (slong i=0; i<len; i++) {
        Dcoeff[2*i]   = poly[3*i];
        Dcoeff[2*i+1] = poly[3*i+1];
        Dcaber[i]     = poly[3*i+2];
//         if (verbose>=level)
//             printf("%.10e + I%.10e +/ %.10e\n", Dcoeff[2*i], Dcoeff[2*i+1], Dcaber[i] );
    }
    
    double max_error = ldexp(1, -2*outputprec);
    int verb = 0;
    int maxits = 150;
    
//     int double_exception0 = _pwpoly_test_and_print_exception("_pw_solving_earoots 0: " PW_VERBOSE_CALL(verbose) );
    
    int res_earoots = ea_roots(Dcoeff, Dcaber, (int) len, maxits, max_error, verb, Droots, Dabers);
    
    /* see what Guillaume answers */
//     int double_exception = _pwpoly_test_and_print_exception("_pw_solving_earoots: " PW_VERBOSE_CALL(verbose) );
    int double_exception = 0;
    
//     if (verbose>=level)
//         printf("_pw_solving_earoots: res_earoots: %d\n", res_earoots);
    
    if ( (res_earoots<=0)||(double_exception) ) {
        res = PWPOLY_DOUBLE_EXCEPTION;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pw_solving_earoots: double exception while solving with earoots\n");
#endif
    }
    
    /* check if all the roots are finite */
    for (slong i=0; (i<len-1)&&(res==1); i++)
        if ( !(isfinite(Droots[2*i])&&isfinite(Droots[2*i+1])&&isfinite(Dabers[i])) ) {
            res = PWPOLY_NOT_FINITE;
#ifndef PW_SILENT
            if (verbose>=level)
                printf("_pw_solving_earoots: some infinite roots\n");
#endif
        }
        
    /* copy the roots in output table */
    for (slong i=0; i<len-1; i++) {
        roots[3*i]   = Droots[2*i];
        roots[3*i+1] = Droots[2*i+1];
        roots[3*i+2] = Dabers[i];
    }
    
    pwpoly_free(Dcoeff);
    pwpoly_free(Dcaber);
    pwpoly_free(Droots);
    pwpoly_free(Dabers);
    
    /* restore exception flags and rounding mode */
    fesetexceptflag (&except_save, FE_ALL_EXCEPT);
    fesetround (rounding_save);
    
    return res;
}

double _pw_compute_upper( double center, double radius, double corr, int infl ) {
    double res = center + infl*radius;
    if (res > 0)
        res = res/corr;
    else
        res = res*corr;
    return res;
}

double _pw_compute_lower( double center, double radius, double corr, int infl ) {
    double res = center - infl*radius;
    if (res > 0)
        res = res*corr;
    else
        res = res/corr;
    return res;
}
int _pw_boxes_overlaps ( const double b1[4], const double b2[4] ) {
    return ( (b1[0] <= b2[1]) /* rl_i <= ru_j */ 
          && (b1[1] >= b2[0])   /* ru_i >= rl_j */ 
          && (b1[2] <= b2[3]) /* il_i <= iu_j */
          && (b1[3] >= b2[2]) );
}
int _pw_box_intersect_real_axis( const double b[4] ) {
    return ( (b[2]<=0)&&(b[3]>=0) );
}
int _pw_box_is_non_imaginary_negative( const double b[4] ) {
    return (b[3]>=0);
}
/* does not support aliasing */
void _pw_box_conjugate ( double b1[4], const double b2[4] ) {
    b1[0] = b2[0];
    b1[1] = b2[1];
    b1[2] = -b2[3];
    b1[3] = -b2[2];
}

int _pw_roots_check_separation ( const double * Roots, slong len, int realCoeffs ) {
    
    double corrInfl = 1-4*PWPOLY_U; /* is exact and < 1-3*PWPOLY_U */
    double contBoxInfl[4];
    double contBoxConj[4];
    int isolators = 1;
    int naturalIs = 1;
    
    /* check separation */
    for(slong i = 0; (i< len-1)&&(isolators==1); i++){
        contBoxInfl[0] = _pw_compute_lower( Roots[ 8*i ],   Roots[ 8*i+2 ], corrInfl, 3);
        contBoxInfl[1] = _pw_compute_upper( Roots[ 8*i ],   Roots[ 8*i+2 ], corrInfl, 3);
        contBoxInfl[2] = _pw_compute_lower( Roots[ 8*i+1 ], Roots[ 8*i+2 ], corrInfl, 3);
        contBoxInfl[3] = _pw_compute_upper( Roots[ 8*i+1 ], Roots[ 8*i+2 ], corrInfl, 3);
        /* verify that containing boxes are pairwise disjoints */
        /* and natural isolators */
        for(slong j = i+1; (j<len) && (isolators==1); j++) {
            if ( _pw_boxes_overlaps ( Roots + (8*i+3), Roots + (8*j+3) ) )
                isolators = 0;
            if ( _pw_boxes_overlaps ( contBoxInfl,     Roots + (8*j+3) ) )
                naturalIs=0;
            if ( (realCoeffs) && _pw_box_intersect_real_axis(Roots+(8*i+3)) ) { 
                /* containing box intersects the real axis */
                /* check with complex conjugated */
                _pw_box_conjugate ( contBoxConj, Roots + (8*j+3) );
                if ( _pw_boxes_overlaps ( Roots + (8*i+3), contBoxConj ) )
                    isolators = 0;
                if ( _pw_boxes_overlaps ( contBoxInfl, contBoxConj ) )
                    naturalIs=0;
            }
        }
        if ( (realCoeffs) && ( !_pw_box_intersect_real_axis(Roots+(8*i+3)) )
                          && (  _pw_box_intersect_real_axis(contBoxInfl) ) )
            naturalIs=0;
    }
    
    if (isolators==0)
        return PWPOLY_NOT_ISOLATED;
    if (naturalIs==0)
        return PWPOLY_NOT_NATURAL;
    
    return 1;
    
}

int _pw_discs_compare_width(const double * x, const double * y){
//     return (x[2] >= y[2]);
    if (x[2]>y[2])
        return -1;
    if (x[2]==y[2])
        return 0;
    return 1;
}

int _pw_solving_earoots_global_dpoly( acb_ptr roots, 
                                     double * Pcoeffs, double * Pabers, slong len, 
                                     slong outputprec, int realCoeffs, int nbMaxItts PW_VERBOSE_ARGU(verbose)  ) {
#ifndef PW_SILENT    
    int level = 1;
#endif
    int res = 1;
    
    /* save exception flags and re-init exceptions */
    fexcept_t except_save;
    fegetexceptflag (&except_save, FE_ALL_EXCEPT);
    feclearexcept (FE_ALL_EXCEPT);
    
    /* save rounding mode and set double rounding mode to NEAREST */
    int rounding_save = fegetround();
    int ressetnearest = fesetround (FE_TONEAREST);
    if (ressetnearest!=0) {
        res = PWPOLY_SET_ROUND_NEAREST_FAILED;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pw_solving_earoots_global: setting rounding to nearest failed!!!\n");
#endif        
        /* restore exception flags and rounding mode */
        fesetexceptflag (&except_save, FE_ALL_EXCEPT);
        fesetround (rounding_save);
        
        return res;
    }
    
    double * Rmidds = (double *) pwpoly_malloc ( 2*(len-1)*sizeof(double) );
    double * Rabers = (double *) pwpoly_malloc (   (len-1)*sizeof(double) );
    double * Rroots = (double *) pwpoly_malloc ( 8*(len-1)*sizeof(double) );
    
    double max_error = ldexp(1, outputprec);
    int verb = 0;
//     int maxits = 100;
    
#ifdef PW_PROFILE
    clock_t start_eaitts = clock();
#endif
    int res_earoots = ea_roots(Pcoeffs, Pabers, (int) len, nbMaxItts, max_error, verb, Rmidds, Rabers);
#ifdef PW_PROFILE
    clicks_in_eaitts_global+=(clock() - start_eaitts);
#endif
    /* see what Guillaume answers */
//     int double_exception = _pwpoly_test_and_print_exception("_pw_solving_earoots_global: ", 1);
    feclearexcept (FE_ALL_EXCEPT);
    int double_exception = 0;
#ifndef PW_SILENT    
    if (verbose>=level)
        printf("_pw_solving_earoots: res_earoots: %d\n", res_earoots);
#endif    
    if ( (res_earoots<=0)||(double_exception) ) {
        res = PWPOLY_DOUBLE_EXCEPTION;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pw_solving_earoots_global: double exception while solving with earoots\n");
#endif
    }
    
#ifndef PW_SILENT
    if (verbose>=level) {
        slong nbFinite = len-1;
        slong nbFiniteApp = len-1;
        for (slong i=0; (i<(len-1))&&(res==1); i++) {
            if ( !(isfinite(Rmidds[2*i])&&isfinite(Rmidds[2*i+1])&&isfinite(Rabers[i])) )
                nbFinite--;
            if ( !(isfinite(Rmidds[2*i])&&isfinite(Rmidds[2*i+1])) )
                nbFiniteApp--;
        }
        
        printf("_pw_solving_earoots_global: %ld/%ld roots are finite, %ld/%ld roots have finite app\n", nbFinite, len-1, nbFiniteApp, len-1);
    }
#endif

    /* check if all the roots are finite */
    /* if (realCoeffs), copy the non negative roots in Roots */
    /* otherwise copy all the roots */
    slong nbFilteredRoots = 0;
    double corr = 1-2*PWPOLY_U; /* is exact */
    
    for (slong i=0; (i<(len-1))&&(res==1); i++) {
        
        if ( !(isfinite(Rmidds[2*i])&&isfinite(Rmidds[2*i+1])&&isfinite(Rabers[i])) ) {
            res = PWPOLY_NOT_FINITE;
#ifndef PW_SILENT
            if (verbose>=level)
                printf("_pw_solving_earoots_global: some infinite roots\n");
#endif
        }
        
        if ( res==1 ) {
            Rroots[8*nbFilteredRoots]   = Rmidds[2*i];
            Rroots[8*nbFilteredRoots+1] = Rmidds[2*i+1];
            Rroots[8*nbFilteredRoots+2] = Rabers[i];
            Rroots[8*nbFilteredRoots+3] = _pw_compute_lower( Rmidds[2*i],   Rabers[i], corr, 1);
            Rroots[8*nbFilteredRoots+4] = _pw_compute_upper( Rmidds[2*i],   Rabers[i], corr, 1);
            Rroots[8*nbFilteredRoots+5] = _pw_compute_lower( Rmidds[2*i+1], Rabers[i], corr, 1);
            Rroots[8*nbFilteredRoots+6] = _pw_compute_upper( Rmidds[2*i+1], Rabers[i], corr, 1);
            
            if ( (realCoeffs==0) || _pw_box_is_non_imaginary_negative( Rroots + (8*nbFilteredRoots+3) ) )
                nbFilteredRoots++;
        }
    }
    
    double_exception = _pwpoly_test_and_print_exception(PW_PREAMB_CALL("_pw_solving_earoots_global: ") PW_VERBOSE_CALL(verbose) );
    if (double_exception) {
        res = PWPOLY_DOUBLE_EXCEPTION;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pw_solving_earoots_global: double exception while computing containing boxes\n");
#endif
    }
    
#ifdef PW_PROFILE
    clock_t start_checks = clock();
#endif
    
    /* sort Roots by decreasing radius */
    if (res==1)
        qsort(Rroots, nbFilteredRoots, 8*sizeof(double), (__compar_fn_t) _pw_discs_compare_width);
    
    /* check if containing boxes in Roots are isolators and natural isolators */
    if (res==1)
        res = _pw_roots_check_separation ( Rroots, nbFilteredRoots, realCoeffs );
    
    double_exception = _pwpoly_test_and_print_exception(PW_PREAMB_CALL("_pw_solving_earoots_global: ") PW_VERBOSE_CALL(verbose) );
    if (double_exception) {
        res = PWPOLY_DOUBLE_EXCEPTION;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_pw_solving_earoots_global: double exception while checking separation\n");
#endif
    }
#ifdef PW_PROFILE
    clicks_in_checks_global+=(clock() - start_checks);
#endif
    /* write Roots in output */
    if (res==1) {
        slong nbRoots = 0;
        for(slong i = 0; i<nbFilteredRoots; i++){
            _be_get_acb( roots + nbRoots, Rroots[8*i], Rroots[8*i+1], Rroots[8*i+2] );
            if (realCoeffs) {
                if (_pw_box_intersect_real_axis(Rroots + (8*i+3) ))
                    arb_zero( acb_imagref( roots + nbRoots ) );
                else {
                    nbRoots++;
                    _be_get_acb( roots + nbRoots, Rroots[8*i], -Rroots[8*i+1], Rroots[8*i+2] );
                }
            }
            nbRoots++;
        }
    }
    
    pwpoly_free(Rroots);
    pwpoly_free(Rmidds);
    pwpoly_free(Rabers);
    
    /* restore exception flags and rounding mode */
    fesetexceptflag (&except_save, FE_ALL_EXCEPT);
    fesetround (rounding_save);
    
    return res;
}

// int earoots_global_dpoly( acb_ptr roots, 
//                           double * Pcoeffs, double * Pabers, slong len, 
//                           slong outputprec, int realCoeffs, slong nbItts PW_VERBOSE_ARGU(verbose)  ) {
// #ifndef PW_SILENT    
//     int level = 1;
// #endif
//     int res = 1;
//     
//     /* save exception flags and re-init exceptions */
//     fexcept_t except_save;
//     fegetexceptflag (&except_save, FE_ALL_EXCEPT);
//     feclearexcept (FE_ALL_EXCEPT);
//     
//     /* save rounding mode and set double rounding mode to NEAREST */
//     int rounding_save = fegetround();
//     int ressetnearest = fesetround (FE_TONEAREST);
//     if (ressetnearest!=0) {
//         res = PWPOLY_SET_ROUND_NEAREST_FAILED;
// #ifndef PW_SILENT
//         if (verbose>=level)
//             printf("earoots_global_dpoly: setting rounding to nearest failed!!!\n");
// #endif        
//         /* restore exception flags and rounding mode */
//         fesetexceptflag (&except_save, FE_ALL_EXCEPT);
//         fesetround (rounding_save);
//         
//         return res;
//     }
//     
//     double * Rmidds = (double *) pwpoly_malloc ( 2*(len-1)*sizeof(double) );
//     double * Rabers = (double *) pwpoly_malloc (   (len-1)*sizeof(double) );
//     double * Rroots = (double *) pwpoly_malloc ( 8*(len-1)*sizeof(double) );
//     
//     double max_error = ldexp(1, outputprec);
//     int verb = 0;
//     int maxits = nbItts;
//     
// // #ifdef PW_PROFILE
// //     clock_t start_eaitts = clock();
// // #endif
//     int res_earoots = ea_roots(Pcoeffs, Pabers, (int) len, maxits, max_error, verb, Rmidds, Rabers);
// // #ifdef PW_PROFILE
// //     clicks_in_eaitts_global+=(clock() - start_eaitts);
// // #endif
//     /* see what Guillaume answers */
// //     int double_exception = _pwpoly_test_and_print_exception("_pw_solving_earoots_global: ", 1);
//     feclearexcept (FE_ALL_EXCEPT);
//     int double_exception = 0;
// #ifndef PW_SILENT    
//     if (verbose>=level)
//         printf("earoots_global_dpoly: res_earoots: %d\n", res_earoots);
// #endif    
//     if ( (res_earoots<=0)||(double_exception) ) {
//         res = PWPOLY_DOUBLE_EXCEPTION;
// #ifndef PW_SILENT
//         if (verbose>=level)
//             printf("earoots_global_dpoly: double exception while solving with earoots\n");
// #endif
//     }
//     
// #ifndef PW_SILENT
//     if (verbose>=level) {
//         slong nbFinite = len-1;
//         slong nbFiniteApp = len-1;
//         for (slong i=0; (i<(len-1)); i++) {
//             if ( !(isfinite(Rmidds[2*i])&&isfinite(Rmidds[2*i+1])&&isfinite(Rabers[i])) )
//                 nbFinite--;
//             if ( !(isfinite(Rmidds[2*i])&&isfinite(Rmidds[2*i+1])) )
//                 nbFiniteApp--;
//         }
//         
//         printf("earoots_global_dpoly: %ld/%ld roots are finite, %ld/%ld roots have finite app\n", nbFinite, len-1, nbFiniteApp, len-1);
//     }
// #endif
// 
//     /* check if all the roots are finite */
//     /* if (realCoeffs), copy the non negative roots in Roots */
//     /* otherwise copy all the roots */
//     slong nbFilteredRoots = 0;
//     double corr = 1-2*PWPOLY_U; /* is exact */
//     
//     for (slong i=0; (i<(len-1))&&(res==1); i++) {
//         
//         if ( !(isfinite(Rmidds[2*i])&&isfinite(Rmidds[2*i+1])&&isfinite(Rabers[i])) ) {
//             res = PWPOLY_NOT_FINITE;
// #ifndef PW_SILENT
//             if (verbose>=level)
//                 printf("_earoots_global_dpoly: some infinite roots\n");
// #endif
//         }
//         
//         if ( res==1 ) {
//             Rroots[8*nbFilteredRoots]   = Rmidds[2*i];
//             Rroots[8*nbFilteredRoots+1] = Rmidds[2*i+1];
//             Rroots[8*nbFilteredRoots+2] = Rabers[i];
//             Rroots[8*nbFilteredRoots+3] = _pw_compute_lower( Rmidds[2*i],   Rabers[i], corr, 1);
//             Rroots[8*nbFilteredRoots+4] = _pw_compute_upper( Rmidds[2*i],   Rabers[i], corr, 1);
//             Rroots[8*nbFilteredRoots+5] = _pw_compute_lower( Rmidds[2*i+1], Rabers[i], corr, 1);
//             Rroots[8*nbFilteredRoots+6] = _pw_compute_upper( Rmidds[2*i+1], Rabers[i], corr, 1);
//             
//             if ( (realCoeffs==0) || _pw_box_is_non_imaginary_negative( Rroots + (8*nbFilteredRoots+3) ) )
//                 nbFilteredRoots++;
//         }
//     }
//     
//     double_exception = _pwpoly_test_and_print_exception("earoots_global_dpoly: " PW_VERBOSE_CALL(verbose) );
//     if (double_exception) {
//         res = PWPOLY_DOUBLE_EXCEPTION;
// #ifndef PW_SILENT
//         if (verbose>=level)
//             printf("earoots_global_dpoly: double exception while computing containing boxes\n");
// #endif
//     }
//     
// #ifdef PW_PROFILE
//     clock_t start_checks = clock();
// #endif
//     
//     /* sort Roots by decreasing radius */
//     if (res==1)
//         qsort(Rroots, nbFilteredRoots, 8*sizeof(double), (__compar_fn_t) _pw_discs_compare_width);
//     
//     /* check if containing boxes in Roots are isolators and natural isolators */
//     if (res==1)
//         res = _pw_roots_check_separation ( Rroots, nbFilteredRoots, realCoeffs );
//     
//     double_exception = _pwpoly_test_and_print_exception("earoots_global_dpoly: " PW_VERBOSE_CALL(verbose) );
//     if (double_exception) {
//         res = PWPOLY_DOUBLE_EXCEPTION;
// #ifndef PW_SILENT
//         if (verbose>=level)
//             printf("earoots_global_dpoly: double exception while checking separation\n");
// #endif
//     }
// #ifdef PW_PROFILE
//     clicks_in_checks_global+=(clock() - start_checks);
// #endif
//     
//     if (res==1) {
//         /* write Roots in output */
//         slong nbRoots = 0;
//         for(slong i = 0; i<nbFilteredRoots; i++){
//             _be_get_acb( roots + nbRoots, Rroots[8*i], Rroots[8*i+1], Rroots[8*i+2] );
//             if (realCoeffs) {
//                 if (_pw_box_intersect_real_axis(Rroots + (8*i+3) ))
//                     arb_zero( acb_imagref( roots + nbRoots ) );
//                 else {
//                     nbRoots++;
//                     _be_get_acb( roots + nbRoots, Rroots[8*i], -Rroots[8*i+1], Rroots[8*i+2] );
//                 }
//             }
//             nbRoots++;
//         }
//     } else {
//         /* check if all the centers are finite */
//         res = 1;
//         for (slong i=0; (i<(len-1))&&(res==1); i++) {
//             if ( !(isfinite(Rmidds[2*i])&&isfinite(Rmidds[2*i+1])) )
//                 res = 0;
//         }
//         printf("earoots_global_dpoly: res: %d\n", res);
//         if (res==1) {
//             /* write Roots in output */
//             for(slong i = 0; i<(len-1); i++) {
//                 _be_get_acb( roots + i, Rmidds[2*i], Rmidds[2*i+1], 0 );
//             }
//             res = 2;
//         }
//         printf("earoots_global_dpoly: res: %d\n", res);
//     }
//     
//     pwpoly_free(Rroots);
//     pwpoly_free(Rmidds);
//     pwpoly_free(Rabers);
//     
//     /* restore exception flags and rounding mode */
//     fesetexceptflag (&except_save, FE_ALL_EXCEPT);
//     fesetround (rounding_save);
//     
//     return res;
// }

#endif
