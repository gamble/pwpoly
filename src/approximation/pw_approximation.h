/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_APPROXIMATION_H
#define PW_APPROXIMATION_H

#include "pw_base.h"
#include "covering/pw_covering.h"
#include "polynomial/pw_polynomial.h"
#include <acb_dft.h>
#ifdef PWPOLY_HAS_BEFFT
#include "befft.h"
#endif
#ifdef __cplusplus
extern "C" {
#endif

/* Preconditions:  coeffs is a table of len arb                                            */
/*                 mantissas is a table of len arbs                                        */
/*                 exponents is a table of len fmpqs                                       */
/* Postconditions: for 0 <= i < len, computes m and e s.t. mid(coeffs[i]) = m*2^e          */
/*                                   if mid(coeffs) is a normal value: 0.5 <= |m| < 1      */
/*                                   else m=mid(coeffs[i]) and e = 0                       */
/*                                   let mantissas[i]=m, exponents[i]=e                    */
/* exported for tests                                                                      */
void _pw_approximation_compute_mantissas_exponents_arb_ptr( arb_ptr mantissas, fmpq_ptr exponents, const arb_ptr coeffs, ulong len );

PWPOLY_INLINE ulong pw_approximation_get_working_m(const ulong m, const slong length) {
    return 2*(m + (ulong)ceil(log2( (double)length ))); /* what if len is so big that the difference between 2 doubles is > 2 */
}

PWPOLY_INLINE ulong pw_approximation_get_working_prec(const ulong working_m) {
    return 4*working_m;
}

#ifndef acb_dft_rad2_ptr
typedef acb_dft_rad2_struct * acb_dft_rad2_ptr;
#endif

typedef struct {
    
    /* initial polynomial */
    ulong           _len;            /* the number of coeffs */
    acb_poly_srcptr _poly;           /* the poly as vector of coefficients */
    arb_ptr         _abs_coeffs;     /* the vector of absolute values of coefficients */
    arb_ptr         _mantissas;      /* the mantissas of the coeffs */
    fmpq_ptr        _exponents;      /* the exponents of the coeffs */
    int             _realCoeffs;     /* 1 <=> _poly has real coefficients */
    int             _isExact;        /* 1 if _poly has no error, 0 otherwise */
    slong           _maxRelErrorBits;/* is _isExact=0, the log2 of the max of the relative error */
    
    /* the covering of the complex plane */
    ulong           _output__m;      /* the required output prec relative to _poly^+ <- given by user                                          */
    slong           _prec;           /* the working precision: should be 2*_working_m                                                          */
    ulong           _c;              /* used internally and given by user:                                                                     */
                                     /* should be high enough so that log2( e*_ratio/_c )*_c*_working_m + _ratio*_working_m + 1 <= -_working_m */
    pw_covering     _covering;       /* the covering of the plane                                                                              */ 
                                     /* computed with _working_m, _c=1, _scale and _ratio                                                      */ 
    
    /* the pre-computation for the fft's */
    acb_dft_rad2_struct _rad2_log2_nbsect;  /* initialized only when needed */
    uint                _log2_nb_sect_comp; /* initialized to zero; */ 
                                            /* if non-zero, the value for which _rad2_log2_nbsect has been initialized */
#ifdef PWPOLY_HAS_BEFFT
    befft_fft_rad2_struct _rad2_log2_nbsect_be;  /* initialized only when needed */
    uint                  _log2_nb_sect_comp_be; /* initialized to zero; */ 
                                                 /* if non-zero, the value for which _rad2_log2_nbsect_be has been initialized */
    double *              _yx_conv;              /* initialized only when needed */
                                                 /* size 6*pw_approximation_nbsectref(app) */
#endif
    /* the piecewise approximation */
    ulong *         _approx_lengths; /* _approx_lengths[i]:= the lengths of the approximations in i-th annulus, 0 if not already computed */
    acb_mat_ptr     _approx_coeffs;  /* _approx_coeffs[i]:= the coeffs of the approximations in i-th annulus */
                                     /* !!! approximations are indexed in anti-clockwise order               */
                                     /* _approx_coeffs[i] is of size nbsect[i]*_c*_working_m                 */
    
    uint **         _already_solved;
} pw_approximation;

typedef pw_approximation pw_approximation_t[1];
typedef pw_approximation * pw_approximation_ptr;

#define pw_approximation_lenref(X)         ( X->_len )
#define pw_approximation_polyref(X)        ( (X)->_poly )
#ifdef PWPOLY_DEBUG
#define pw_approximation_polyInitref(X)    ( &(X)->_polyInit )
#endif
#define pw_approximation_abs_coeffsref(X)  ( X->_abs_coeffs )
#define pw_approximation_mantissasref(X)   ( X->_mantissas )
#define pw_approximation_exponentsref(X)   ( X->_exponents )
#define pw_approximation_realCoeffsref(X)  ( X->_realCoeffs )
#define pw_approximation_isExactref(X)     ( X->_isExact )
#define pw_approximation_maxRelErrorBitsref(X)     ( X->_maxRelErrorBits )

#define pw_approximation_output__mref(X)   ( X->_output__m)
#define pw_approximation_working_mref(X)   ( (&(X)->_covering)->_m)
#define pw_approximation_precref(X)        ( X->_prec )
#define pw_approximation_scaleref(X)       (&(&(X)->_covering)->_scale )
#define pw_approximation_ratioref(X)       (&(&(X)->_covering)->_ratio )
#define pw_approximation_cref(X)           ( X->_c )
#define pw_approximation_working_cref(X)   ( (&(X)->_covering)->_c )
#define pw_approximation_coveringref(X)    ( &(X)->_covering )

#define pw_approximation_rad2_log2_nbsectref(X)    ( &(X)->_rad2_log2_nbsect )
#define pw_approximation_log2_nb_sect_compref(X)   ( X->_log2_nb_sect_comp )
#ifdef PWPOLY_HAS_BEFFT
#define pw_approximation_rad2_log2_nbsect_beref(X)    ( &(X)->_rad2_log2_nbsect_be )
#define pw_approximation_log2_nb_sect_comp_beref(X)   ( X->_log2_nb_sect_comp_be )
#define pw_approximation_yx_convref(X)                ( X->_yx_conv )
#endif

#define pw_approximation_degreeref(X)      ( (&(X)->_covering)->_degree )
#define pw_approximation_Nref(X)           ( (&(X)->_covering)->_N )
#define pw_approximation_Ksumref(X)           ( (&(X)->_covering)->_Ksum )
#define pw_approximation_indlowref(X)      ( (&(X)->_covering)->_indlow )
#define pw_approximation_indhigref(X)      ( (&(X)->_covering)->_indhig )
#define pw_approximation_bigradref(X)      ( (&(X)->_covering)->_bigrad )
#define pw_approximation_inddomref(X)      ( (&(X)->_covering)->_inddom )
#define pw_approximation_sl_at_edref(X)      ( (&(X)->_covering)->_sl_at_ed )
#define pw_approximation_annulii_radsref(X)  ( (&(X)->_covering)->_app_bigrad )
#define pw_approximation_app_gammasref(X)    ( (&(X)->_covering)->_app_gammas )
#define pw_approximation_app_rhosref(X)      ( (&(X)->_covering)->_app_rhos )
#define pw_approximation_nbsectref(X)      ( (&(X)->_covering)->_nbsect )
#define pw_approximation_log2_nbsectref(X) ( (&(X)->_covering)->_log2_nbsect )
#define pw_approximation_approx_coeffsref(X)  ( X->_approx_coeffs )
#define pw_approximation_approx_lengthsref(X) ( X->_approx_lengths )
#define pw_approximation_already_solvedref(X) ( X->_already_solved )

/* this just set the len field to 0 */
/* this is to give to python an initialization method with no argument */
/* do not use it in the C library                */
void pw_approximation_init_dummy( pw_approximation_t app );
  
void pw_approximation_init_acb_poly( pw_approximation_t app, const acb_poly_t p, 
                                     const ulong m, const ulong c, const fmpq_t scale, const fmpq_t ratio );
void pw_approximation_init_pw_polynomial( pw_approximation_t app, pw_polynomial_t poly, 
                                          const ulong m, const ulong c, const fmpq_t scale, const fmpq_t ratio );

void pw_approximation_clear( pw_approximation_t app );

void pw_approximation_compute_approx_annulus( pw_approximation_t app, ulong n, int usebefft );

void pw_approximation_compute_approx_annulii( pw_approximation_t app, int usebefft );

void pw_approximation_clear_approx_annulus( pw_approximation_t app, ulong n );

ulong pw_approximation_get_approx(acb_srcptr * coeffs, const pw_approximation_t app, ulong n, slong k );

void _pw_approximation_evaluation_error( arb_t error, const pw_approximation_t app, const arb_t abspoint, slong prec PW_VERBOSE_ARGU(verbose)  );
void pw_approximation_evaluate_acb( acb_t value, const pw_approximation_t app, const acb_t point PW_VERBOSE_ARGU(verbose)  );

void pw_approximation_evaluate2_acb( acb_t value, acb_t valueder, const pw_approximation_t app, const acb_t point PW_VERBOSE_ARGU(verbose)  );
void _pw_approximation_evaluate2_acb_nk( acb_t value, acb_t valueder, const pw_approximation_t app, ulong n, ulong k, const acb_t point PW_VERBOSE_ARGU(verbose)  );
void pw_approximation_evaluate_phi_3( acb_t phi, acb_t phip, acb_t phipp, arb_t errorpp, 
                                       const pw_approximation_t app, const ulong n, const acb_t z PW_VERBOSE_ARGU(verbose)  );

// void _pw_approximation_ubound_hatf( arb_t error, const pw_approximation_t app, const acb_t point, slong prec PW_VERBOSE_ARGU(verbose)  );

#ifndef PW_SILENT
void pw_approximation_fprint( FILE * f, const pw_approximation_t app );
PWPOLY_INLINE void pw_approximation_print( const pw_approximation_t app ){
  pw_approximation_fprint( stdout, app );
}

void pw_approximation_fprint_short( FILE * f, const pw_approximation_t app );
PWPOLY_INLINE void pw_approximation_print_short( const pw_approximation_t app ){
  pw_approximation_fprint_short( stdout, app );
}
#endif

#ifdef __cplusplus
}
#endif

#endif
