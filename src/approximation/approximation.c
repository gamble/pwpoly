/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_approximation.h"

#ifdef PW_PROFILE
double clicks_in_covering;
double clicks_in_approx_annulii;
double clicks_in_expansion;
double clicks_in_compress;
double clicks_in_normalize;
double clicks_in_ffts;
double clicks_in_ffts_arb;
double clicks_in_ffts_be;
double clicks_in_ffts_conv_be;
double clicks_in_evaluate;
ulong  nb_call_fft_conv;
ulong  nb_succ_fft_conv;
ulong  nb_call_fft_be;
ulong  nb_succ_fft_be;
ulong  nb_call_fft_arb;
#endif

/* Preconditions:  coeffs is a table of len arb, being either 0 or normal values           */
/*                 mantissas is a table of len arbs                                        */
/*                 exponents is a table of len fmpqs                                       */
/* Postconditions: for 0 <= i < len, computes m and e s.t. mid(coeffs[i]) = m*2^e          */
/*                                   if mid(coeffs) is a normal value: 0.5 <= |m| < 1      */
/*                                   else m=mid(coeffs[i]) and e = 0                       */
/*                                   let mantissas[i]=m, exponents[i]=e                    */
void _pw_approximation_compute_mantissas_exponents_arb_ptr( arb_ptr mantissas, fmpq_ptr exponents, const arb_ptr coeffs, ulong len ){
    fmpz_t temp;
    fmpz_init(temp);
    for (ulong i = 0; i<len; i++)
        if (arf_is_zero(arb_midref(coeffs+i))) {
            fmpq_zero(exponents+i);
            arf_zero( arb_midref(mantissas+i) );
        } else {
            fmpz_one( fmpq_denref(exponents+i) );
            arf_frexp( arb_midref(mantissas+i), fmpq_numref(exponents+i), arb_midref(coeffs+i));
            /* set the error to error(coeffs+i)*2^(-(exponents+i)) */
            fmpz_neg(temp, fmpq_numref(exponents+i));
            mag_mul_2exp_fmpz(arb_radref(mantissas+i), arb_radref(coeffs+i), temp);
        }
    fmpz_clear(temp);
}

/* this just set the len field to 0 */
/* this is to give to python an initialization method with no argument */
/* do not use it in the C library                */
void pw_approximation_init_dummy( pw_approximation_t app ){
    pw_approximation_lenref(app) = 0;
}

void pw_approximation_init_acb_poly( pw_approximation_t app, const acb_poly_t p, const ulong m, const ulong c, const fmpq_t scale, const fmpq_t ratio ){
    
    ulong len                           = (ulong) p->length;
    pw_approximation_lenref(app)        = len;
    pw_approximation_polyref(app)       = p;
    pw_approximation_abs_coeffsref(app) = _arb_vec_init( (slong) len );
    pw_approximation_mantissasref(app)  = _arb_vec_init( (slong) len );
    pw_approximation_exponentsref(app)  = _fmpq_vec_init( (slong) len );
    
    ulong working_m                    = pw_approximation_get_working_m( m, (slong)len );
    slong prec                         = pw_approximation_get_working_prec(working_m);
    int realCoeffs = 1;
    slong maxRelErrorBits = LONG_MIN;
    int isExact = 1;
    /* compute abs of coeffs of poly to prec _prec */
    /* compute maximum relative error on coeffs */
    for (ulong i = 0; i<len; i++) {
        realCoeffs = realCoeffs && arb_is_zero( acb_imagref( (p->coeffs)+i ) );
        if (!acb_is_exact((p->coeffs)+i)) {
            isExact = 0;
            slong relErrorBits = 1 + PWPOLY_MAX( arb_rel_error_bits( acb_realref( (p->coeffs)+i ) ),
                                                 arb_rel_error_bits( acb_imagref( (p->coeffs)+i ) ) );
            maxRelErrorBits = PWPOLY_MAX( maxRelErrorBits, relErrorBits );
        }
        acb_abs(pw_approximation_abs_coeffsref(app)+i, (pw_approximation_polyref( app )->coeffs) + i, prec);
        arb_get_mid_arb( pw_approximation_abs_coeffsref(app)+i, pw_approximation_abs_coeffsref(app)+i );
        
    }
    
    /* ensure that the center of the abs of the leading coeff is non-zero, */
    /* assuming the leading coeff is non-zero                              */
    slong prectemp = prec;
    while (arb_is_zero(pw_approximation_abs_coeffsref(app)+(len-1))){
        prectemp=2*prec;
        acb_abs(pw_approximation_abs_coeffsref(app)+(len-1), (pw_approximation_polyref( app )->coeffs) + (len-1), prectemp);
        arb_get_mid_arb( pw_approximation_abs_coeffsref(app)+(len-1), pw_approximation_abs_coeffsref(app)+(len-1) );
    }
//     printf("isExact: %d, maxRelErrorBits: %ld\n", isExact, maxRelErrorBits);
    pw_approximation_realCoeffsref(app) = realCoeffs;
    pw_approximation_isExactref(app) = isExact;
    pw_approximation_maxRelErrorBitsref(app) = maxRelErrorBits;
    
    /* compute mantissas and exponents */
    _pw_approximation_compute_mantissas_exponents_arb_ptr( pw_approximation_mantissasref(app), 
                                                           pw_approximation_exponentsref(app), 
                                                           pw_approximation_abs_coeffsref(app), len );
    
    pw_approximation_output__mref(app) = m;
    pw_approximation_precref(app)      = prec;
    
    /* initialize and compute covering */
    pw_approximation_cref(app) = c;
    ulong working_c = 1;
    
    pw_covering_init( pw_approximation_coveringref(app), working_m, len, working_c, scale, ratio );
    
#ifdef PW_PROFILE
    clock_t start = clock();
#endif    
    _pw_covering_compute_annulii_from_fmpq_weights( pw_approximation_coveringref(app), pw_approximation_exponentsref(app) );
    
    /* compute approximations of annulii and roots of unit at precision prec */
    _pw_covering_compute_app_rads( pw_approximation_coveringref(app), prec );
#ifdef PW_PROFILE
    clicks_in_covering += (clock() - start);
#endif 
    
    pw_approximation_log2_nb_sect_compref(app) = 0;
#ifdef PWPOLY_HAS_BEFFT
    pw_approximation_log2_nb_sect_comp_beref(app) = 0;
#endif
    /* initialize piecewise approximation */
    ulong N    = pw_approximation_Nref(app);
//     ulong cmp1 = pw_approximation_cref(app)*pw_approximation_working_mref(app)+1;
    pw_approximation_approx_lengthsref(app)= (ulong *) pwpoly_malloc (N*sizeof(ulong));
    pw_approximation_approx_coeffsref(app) = (acb_mat_ptr) pwpoly_malloc (N*sizeof(acb_mat_struct));
    
    for (ulong n=0; n<N; n++) {
        pw_approximation_approx_lengthsref(app)[n]= 0;
    }
    
    /* initialized already_solved */
    pw_approximation_already_solvedref(app) = (uint **) pwpoly_malloc ( N*sizeof(uint *) );
    for (ulong n=0; n<N; n++) {
        ulong K = pw_approximation_nbsectref(app)[n];
        pw_approximation_already_solvedref(app)[n] = (uint *) pwpoly_malloc ( K*sizeof(uint) );
        for (ulong k=0; k<K; k++)
            pw_approximation_already_solvedref(app)[n][k] = 0;
    }
}

void pw_approximation_init_pw_polynomial( pw_approximation_t app, pw_polynomial_t poly, 
                                          const ulong m, const ulong c, const fmpq_t scale, const fmpq_t ratio ) {
    
    slong len            = pw_polynomial_length(poly);
    ulong working_m      = pw_approximation_get_working_m( m, len );
    slong prec           = pw_approximation_get_working_prec(working_m);
    acb_poly_srcptr pacb = pw_polynomial_getApproximation( poly, prec );
    pw_approximation_init_acb_poly( app, pacb, m, c, scale, ratio );
    
}

void pw_approximation_clear( pw_approximation_t app ) {
    
    if (pw_approximation_lenref(app)==0)
        return;
    
//     printf("pw_approximation_clear: 0\n");
    ulong len = pw_approximation_lenref(app);
    _arb_vec_clear( pw_approximation_abs_coeffsref(app), (slong)len);
    _arb_vec_clear( pw_approximation_mantissasref(app), (slong)len);
    _fmpq_vec_clear(pw_approximation_exponentsref(app), (slong)len);
    
//     printf("pw_approximation_clear: 1\n");
    
    ulong N = pw_approximation_Nref(app);
    
//     printf("pw_approximation_clear: N: %lu\n", N);
    
    for (ulong i=0; i<N; i++) {
//         printf("pw_approximation_clear: i: %lu, length: %lu\n", i, pw_approximation_approx_lengthsref(app)[i]);
        if ( pw_approximation_approx_lengthsref(app)[i] >0 )
            acb_mat_clear( pw_approximation_approx_coeffsref(app) + i );
    }
    
//     printf("pw_approximation_clear: 10\n");
    
    pwpoly_free(pw_approximation_approx_coeffsref(app));
    
//     printf("pw_approximation_clear: 11\n");
    
    pwpoly_free(pw_approximation_approx_lengthsref(app));
    
//     printf("pw_approximation_clear: 12\n");
    
    pw_covering_clear(pw_approximation_coveringref(app));
    
//     printf("pw_approximation_clear: 2\n");
    
    if (pw_approximation_log2_nb_sect_compref(app) != 0) {
        acb_dft_rad2_clear(pw_approximation_rad2_log2_nbsectref(app));
        pw_approximation_log2_nb_sect_compref(app) = 0;
    }
    
//     printf("pw_approximation_clear: 3\n");
    
#ifdef PWPOLY_HAS_BEFFT
    if (pw_approximation_log2_nb_sect_comp_beref(app) != 0) {
        befft_fft_rad2_clear(pw_approximation_rad2_log2_nbsect_beref(app));
        pw_approximation_log2_nb_sect_comp_beref(app) = 0;
        pwpoly_free( pw_approximation_yx_convref(app) );
    }
#endif 

//     printf("pw_approximation_clear: 4\n");
    
    /* clear already_solved */
    for (ulong n=0; n<N; n++)
        pwpoly_free( pw_approximation_already_solvedref(app)[n] );
    pwpoly_free( pw_approximation_already_solvedref(app) );
    
//     printf("pw_approximation_clear: 5\n");
    
    pw_approximation_lenref(app)=0;
}

void pw_approximation_clear_approx_annulus( pw_approximation_t app, ulong n ) {
    
    ulong N = pw_approximation_Nref(app);
    if (n<N) {
        acb_mat_clear( pw_approximation_approx_coeffsref(app) + n );
        pw_approximation_approx_lengthsref(app)[n] = 0;
    }
}

void arb_pow_si( arb_t dest, arb_t src, slong e, slong prec){
    ulong abse = (e<0? (ulong)-e: (ulong)e );
    if (e<0)
        arb_inv(dest, src, prec);
    else
        arb_set(dest, src);
    arb_pow_ui(dest, dest, abse, prec);
}
/* sets stop_crite to (2^-m * w^max( r ))        */
/*                   where r = 1-scale*rho/gamma */
/*                         m = pw_approximation_working_mref(app) */
void _pw_approximation_stopping_criterion( arb_t stop_crite, pw_approximation_t app, ulong n, slong prec ) {
    
    arb_ptr gamma = pw_approximation_app_gammasref(app)+n;
    arb_ptr rho   = pw_approximation_app_rhosref(app)+n;
    fmpq_ptr scale= pw_approximation_scaleref(app);
    ulong km   = pw_approximation_indlowref(app)[n];
    
    arb_t r, logr, twommwmax;
    arb_init(r);
    arb_init(logr);
    arb_init(twommwmax);
    fmpz_t exp;
    fmpz_init(exp);
    /* sets r to gamma-scale*rho */
    arb_set_fmpq(r, scale, prec);
    arb_mul( r, r, rho, prec);
    arb_sub( r, gamma, r, prec);
    arb_log_base_ui(logr, r, 2, prec);
    
//     printf("_pw_approximation_stopping_criterion: n: %lu\n", n);
//     printf("--- gamma              : "); arb_printd(gamma, 10); printf("\n");
//     printf("--- rho                : "); arb_printd(rho, 10); printf("\n");
//     printf("--- scale              : "); fmpq_print(scale); printf("\n");
//     printf("--- r (gamma-scale*rho): "); arb_printd(r,10); printf("\n");
//     printf("--- log2 r             : "); arb_printd(logr,10); printf("\n");
    
    /* compute the index of monomial supporting the slope gamma-scale*rho */
    ulong min, max;
    if (arb_is_positive(r))
        _pw_covering_arb_searchsorted( &min, &max, logr, pw_approximation_sl_at_edref(app), pw_approximation_lenref(app)-1, prec );
    /*else if (arb_contains_zero(r)){
        
    }*/ 
    else {
        min=0;
        max=0;
    }
        
    ulong kp   = pw_approximation_indhigref(app)[n];
//     printf("----------- _pw_approximation_stopping_criterion, n: %lu, km: %lu, kp:= %lu -------------\n", n, km, kp);
//     printf(" logr: "); arb_printd(logr, 10); printf("\n");
//     printf(" found pos in table: %lu, %lu\n", min, max);
//     printf(" values in tab between %lu, %lu: [ ", min-1, max+1);
//     for (ulong i = min-1; i<=max+1; i++) {
//         arb_t temp;
//         arb_init(temp);
//         arb_set_fmpq(temp, pw_approximation_sl_at_edref(app) + i, prec);
// //         fmpq_print( pw_approximation_sl_at_edref(app) + i);
//         arb_printd(temp, 10);
//         printf(", ");
//         arb_clear(temp);
//     }
//     printf(" ]\n");
//     printf("-----------------------------------------------------------------------------------\n\n");
    /* cap min,max between km and kp */
    min = PWPOLY_MAX(min, km);
    min = PWPOLY_MIN(min, kp);
    max = PWPOLY_MAX(max, km);
    max = PWPOLY_MIN(max, kp);
    
//     printf("n: %lu, km: %lu, kp = %lu, min: %lu, max: %lu\n", n, km, kp, min, max);
    arb_pow_si( twommwmax, r, (slong)min-(slong)km, prec );
    fmpz_sub_ui(exp, fmpq_numref(pw_approximation_exponentsref(app)+min), pw_approximation_working_mref(app)+1 );
    arb_mul_2exp_fmpz(twommwmax, twommwmax, exp);
    arb_mul_2exp_si(twommwmax, twommwmax, -1);
    for (ulong i = min+1; i<=max; i++) {
        arb_pow_si( twommwmax, r, (slong)i-(slong)km, prec );
        fmpz_sub_ui(exp, fmpq_numref(pw_approximation_exponentsref(app)+i), pw_approximation_working_mref(app)+1 );
        arb_mul_2exp_fmpz(stop_crite, stop_crite, exp);
        arb_mul_2exp_si(stop_crite, stop_crite, -1);
        arb_union( twommwmax, twommwmax, stop_crite, prec );
    }
    arb_set( stop_crite, twommwmax);
    
    fmpz_clear(exp);
    arb_clear(twommwmax);
    arb_clear(logr);
    arb_clear(r);
}

// /* Preconditions:  0<= km <= kp                                                               */
// /*                 rho, gamma and scale are positive real numbers                             */
// /*                 coeffs is a table of at least kp-km+1 acb                                  */
// /*                 expansion is an initialized table of at least kp-km+1 acb                  */
// /*                 stop_crit is an arb                                                        */
// /*                 if line>0, expansion has been obtained as:                                 */
// /*                           _pw_approximation_Taylor_expansion called with the same arguments*/
// /* Postconditions: for 0 <= i < kp-km+1,                                                      */
// /*                     sets expansion+i to                                                    */
// /*                           (binom(i, line)*(scale*rho/gamma)^line)*(coeffs+km+i)*gamma^i    */
// /*                 where binom(i, line) = choose line among i                                 */
// /*                 returns 1 if for all 0 <= i < kp-km+1,                                     */
// /*                            (scale*rho/gamma)*(i-line)/(line+1) <= 1/2                      */
// /*                        and |expansion+i| <= stop_crite                                     */
// /*                 otherwise returns 0                                                        */
int  _pw_approximation_Taylor_expansion( acb_ptr expansion, const arb_t stop_crit, 
                                         ulong line, acb_srcptr coeffs,
                                         ulong km, ulong kp, const arb_t rho, const arb_t gamma, const fmpq_t scale, slong prec ) {

#ifdef PW_PROFILE
    clock_t start = clock();
#endif

    int res = 1;    
//     printf("km: %lu, kp: %lu, kp-km+1: %lu\n", km, kp, kp-km+1 );
    if (line==0) {
        arb_t gamma_pows;
        arb_init(gamma_pows);
        arb_one(gamma_pows);
        for (ulong i=0; i < kp-km+1; i++){
//             printf("i: %lu\n", i );
            acb_mul_arb( expansion+i, coeffs + (km+i), gamma_pows, prec);
            arb_mul( gamma_pows, gamma_pows, gamma, prec );
        }
        arb_clear(gamma_pows);
        res=0;
    } else {
        arb_t scaleTrhoOgamma, fact, ratio, half;
        arb_init(scaleTrhoOgamma);
        arb_init(fact);
        arb_init(ratio);
        arb_init(half);
        arb_set_d(half, .5);
        arb_set_fmpq(scaleTrhoOgamma, scale, prec);
        arb_mul( scaleTrhoOgamma, scaleTrhoOgamma, rho, prec);
        arb_div( scaleTrhoOgamma, scaleTrhoOgamma, gamma, prec);
        arb_div_ui( ratio, scaleTrhoOgamma, line+1, prec );
        arb_div_ui( scaleTrhoOgamma, scaleTrhoOgamma, line, prec );
        for (ulong i=line-1; i < kp-km+1; i++){
            arb_mul_si( fact, scaleTrhoOgamma, i-line+1, prec );
            acb_mul_arb( expansion+i, expansion+i, fact, prec );
//             arb_mul( expansion_without_co + i, expansion_without_co + i, fact, prec );

            if ( (i>=line)&&(res) ) {
                /* sets fact to (scale*rho/gamma)*(i-line)/(line+1) */
                arb_mul_si( fact, ratio, (slong)i -line, prec ); 
                res = res && arb_le( fact, half );
                if (res) {
                    acb_abs(fact, expansion+i, prec);
                }
                res = res && arb_le( fact, stop_crit );
            }
        }
        arb_clear(half);
        arb_clear(ratio);
        arb_clear(fact);
        arb_clear(scaleTrhoOgamma);
    }
#ifdef PW_PROFILE
    clicks_in_expansion += (clock() - start);
#endif
    return res;
}

void _pw_approximation_modulo( acb_ptr compressed_expansion, 
                               acb_ptr expansion, ulong M, ulong K, slong prec ) {

#ifdef PW_PROFILE
    clock_t start = clock();
#endif     
    /* fill compressed_expansion with zeros */
    _acb_vec_zero( compressed_expansion, K );
    
    /* make modulo K */
    for (ulong i=0; i<M; i++){
        slong ind_in_comp = i%K;
        acb_add( compressed_expansion + ind_in_comp, compressed_expansion + ind_in_comp, expansion + i, prec );
    }
#ifdef PW_PROFILE
    clicks_in_compress += (clock() - start);
#endif
}

void _pw_approximation_normalize_fft_input( fmpz_t exp, acb_ptr y, acb_ptr x, slong len, slong prec ) {
   
#ifdef PW_PROFILE
    clock_t start = clock();
#endif
    
    arf_t min, max;
    arf_init(min);
    arf_init(max);
    fmpz_t mexp;
    fmpz_init(mexp);
    
    arf_pos_inf(min);
    arf_zero(max);
    for (slong i=0; i<len; i++) {
        if (!arf_is_zero( arb_midref( acb_realref( x+i ) ) ) )
            arf_min( min, min, arb_midref( acb_realref( x+i ) ) );
        if (!arf_is_zero( arb_midref( acb_imagref( x+i ) ) ) )
            arf_min( min, min, arb_midref( acb_imagref( x+i ) ) );
        arf_max( max, max, arb_midref( acb_realref( x+i ) ) );
        arf_max( max, max, arb_midref( acb_imagref( x+i ) ) );
    }
    arf_add( min, min, max, prec, ARF_RND_NEAR );
    arf_mul_2exp_si(min, min, -1);
    arf_frexp(max, exp, min);
    fmpz_neg(mexp, exp);
    for (slong i=0; i<len; i++)
        acb_mul_2exp_fmpz( y+i, x+i, mexp );
        
    fmpz_clear(mexp);
    arf_clear(max);
    arf_clear(min);
    
#ifdef PW_PROFILE
    clicks_in_normalize += (clock() - start);
#endif
}

#ifdef PWPOLY_HAS_BEFFT
int _pw_approximation_befft( pw_approximation_t app, ulong n, ulong line, acb_srcptr compressed_expansion, const fmpz_t exp ) {
    
#ifdef PW_PROFILE
    nb_call_fft_conv++;
    clock_t start_befft = clock();
    clock_t start_becon = clock();
#endif
            
    int success = 1;
    ulong nbsect = pw_approximation_nbsectref(app)[n];
    
    /* save exception flags and re-init exceptions */
    fexcept_t except_save;
    fegetexceptflag (&except_save, FE_ALL_EXCEPT);
    feclearexcept (FE_ALL_EXCEPT);
    
    /* save rounding mode and set double rounding mode to NEAREST */
    int rounding_save = fegetround();
    int ressetnearest = fesetround (FE_TONEAREST);
    if (ressetnearest!=0) {
        success = 0;
//         if (verbose>=level)
//             printf("_pw_approximation_befft: setting rounding to nearest failed!!!\n");
    }
    
//     double max_error = 0.;
//     double max_rel_error = 0.;
    double * y_real = pw_approximation_yx_convref(app); 
    double * y_imag = pw_approximation_yx_convref(app) + nbsect; 
    double * y_aber = pw_approximation_yx_convref(app) + 2*nbsect; 
    double * x_real = pw_approximation_yx_convref(app) + 3*nbsect;
    double * x_imag = pw_approximation_yx_convref(app) + 4*nbsect;
    double * x_aber = pw_approximation_yx_convref(app) + 5*nbsect;
    
    /* convert compressed_expansion to doubles */
    for (ulong k=0; (k<nbsect)&&(success); k++) {
        success = success && _be_set_acb( x_real + k, x_imag + k, x_aber + k, compressed_expansion+k );
        if ( !acb_contains_zero(compressed_expansion+k) && 
              _be_contains_zero( x_real[k], x_imag[k], x_aber[k] ) )
            success=0;
    }
    /* check double exception */
    int double_exception = success && _pwpoly_test_and_print_exception(PW_PREAMB_CALL("_pw_approximation_befft conversion: ") PW_VERBOSE_CALL(0));
    if (double_exception) {
        success = 0;
//         if (verbose>=level)
//             printf("_pw_approximation_befft: double exception while converting!!!\n");
    }
#ifdef PW_PROFILE
    clicks_in_ffts_conv_be += clock()-start_becon;
    if (success) 
        nb_succ_fft_conv++;
#endif
    if (success) {
        success = !befft_fft_rad2_dynamic_precomp( y_real, y_imag, y_aber, x_real, x_imag, x_aber, 
                                                   pw_approximation_rad2_log2_nbsect_beref(app) );
        
        /* check double exception */
        double_exception = success && _pwpoly_test_and_print_exception(PW_PREAMB_CALL("_pw_approximation_befft fft: ") PW_VERBOSE_CALL(0));
        if (double_exception) {
            success = 0;
            //         if (verbose>=level)
//             printf("_pw_approximation_befft: double exception while computing fft!!!\n");
//             printf("double_exception: %d\n", double_exception);
        }
//         printf("success fft: %d\n", success);
#ifdef PW_PROFILE
        nb_call_fft_be++;
        if (success)
            nb_succ_fft_be++;
#endif
    }
#ifdef PW_PROFILE
    start_becon = clock();
#endif 
    /* convert back y to acb's */
    if (success) {
        for (ulong k=0; k<nbsect; k++) {
            
            _be_get_acb( acb_mat_entry( pw_approximation_approx_coeffsref(app)+n, k, line ), y_real[k], y_imag[k], y_aber[k] );
            acb_mul_2exp_fmpz( acb_mat_entry( pw_approximation_approx_coeffsref(app)+n, k, line ), 
                               acb_mat_entry( pw_approximation_approx_coeffsref(app)+n, k, line ), exp );
        }
    }
    
    /* restore exception flags and rounding mode */
    fesetexceptflag (&except_save, FE_ALL_EXCEPT);
    fesetround (rounding_save);
                
#ifdef PW_PROFILE
    clock_t end_befft = clock();
    clicks_in_ffts_conv_be += end_befft - start_becon;
    clicks_in_ffts_be += end_befft - start_befft;
    clicks_in_ffts += end_befft - start_befft;
#endif
    
    return success;
}
#endif

void pw_approximation_compute_approx_annulus( pw_approximation_t app, ulong n, int usebefft ) {
   
#ifdef PW_PROFILE
    clock_t start = clock();
#endif
    
//     printf("pw_approximation_compute_approx_annulus: n: %lu, already computed: %ld\n", n, pw_approximation_approx_lengthsref(app)[n]>0);
    
    if (pw_approximation_approx_lengthsref(app)[n]>0)
        return;
    
    slong lprec = pw_approximation_precref(app);
    ulong nbsect = pw_approximation_nbsectref(app)[n];
    
//     ulong d    = pw_approximation_degreeref(app);
    ulong N    = pw_approximation_Nref(app);
    ulong cm   = pw_approximation_cref(app)*pw_approximation_working_mref(app);
    ulong km   = pw_approximation_indlowref(app)[n];
    ulong kp   = pw_approximation_indhigref(app)[n];
    ulong nbmono = kp-km+1;
    
    usebefft = usebefft&&(pw_approximation_working_mref(app) <= PWPOLY_DEFAULT_PREC);
    
    /* init matrix for storing coefficients */
    acb_mat_init( pw_approximation_approx_coeffsref(app) + n, (slong)nbsect, (slong)cm+1 );
    
    /* case where the polynomial is just truncated */
//     if ( kp - km <=cm ) {
    if (nbsect==1){
        if (n<N-1)
            for (ulong i=km; i<=kp; i++)
                acb_get_mid( acb_mat_entry( pw_approximation_approx_coeffsref(app)+n, 0, i-km ), (pw_approximation_polyref(app)->coeffs) + i );
        else /* last annulus: reverse the coefficients */
            for (ulong i=km; i<=kp; i++)
                acb_get_mid( acb_mat_entry( pw_approximation_approx_coeffsref(app)+n, 0, i-km ), (pw_approximation_polyref(app)->coeffs) + (kp-(i-km)) );

        pw_approximation_approx_lengthsref(app)[n]=nbmono;
        return;
    }
    
    /* case where the polynomial is truncated and shifted */
    arb_ptr expansion_stop_crite = _arb_vec_init( nbmono );
    acb_ptr expansion            = _acb_vec_init( nbmono );
    acb_ptr compressed_expansion = _acb_vec_init( nbsect );
    
    arb_t stop_crit;
    arb_init(stop_crit);
    
    arb_ptr gamma = pw_approximation_app_gammasref(app)+n;
    arb_ptr rho   = pw_approximation_app_rhosref(app)+n;
    
    slong lprec2=lprec;
    
    if ( pw_approximation_log2_nb_sect_compref(app) != pw_approximation_log2_nbsectref(app)[n] ) {
//         printf("re-compute acb_dft structure\n");
        if ( pw_approximation_log2_nb_sect_compref(app)!=0 )
            acb_dft_rad2_clear(pw_approximation_rad2_log2_nbsectref(app));
        pw_approximation_log2_nb_sect_compref(app) = pw_approximation_log2_nbsectref(app)[n];
        acb_dft_rad2_init(pw_approximation_rad2_log2_nbsectref(app), (slong)pw_approximation_log2_nb_sect_compref(app), lprec2);
    }
#ifdef PWPOLY_HAS_BEFFT
    acb_ptr n_compressed_expansion;
    fmpz_t exp;
    if (usebefft) {
        if ( pw_approximation_log2_nb_sect_comp_beref(app) != pw_approximation_log2_nbsectref(app)[n] ) {
//             printf("re-compute befft_fft structure\n");
            if ( pw_approximation_log2_nb_sect_comp_beref(app)!=0 ) {
                befft_fft_rad2_clear(pw_approximation_rad2_log2_nbsect_beref(app));
                pwpoly_free(pw_approximation_yx_convref(app));
            }
            pw_approximation_log2_nb_sect_comp_beref(app) = pw_approximation_log2_nbsectref(app)[n];
            befft_fft_rad2_init( pw_approximation_rad2_log2_nbsect_beref(app), (slong)pw_approximation_log2_nb_sect_comp_beref(app) );
            pw_approximation_yx_convref(app) = (double *) pwpoly_malloc ( 6*nbsect*sizeof(double) );
        }
        n_compressed_expansion = _acb_vec_init( nbsect );
        fmpz_init(exp);
    }
#endif
    
    /* sets stop_crite to (2^-m * w^max( r ))      */
    /*                   where r = 1-scale*rho/gamma */
    /*                         m = pw_approximation_working_mref(app) */
    _pw_approximation_stopping_criterion( stop_crit, app, n, lprec);
    
    int can_stop = 0;
    ulong line = 0;
//     printf("output m: %lu, working m: %lu, prec: %ld, n: %lu/%lu, K: %lu\n", 
//                pw_approximation_output__mref(app), pw_approximation_working_mref(app), lprec, n, N, nbsect);
    for ( ; (line <= cm)&&(can_stop==0); line++) {
        
        can_stop = _pw_approximation_Taylor_expansion( expansion, stop_crit, 
                                                       line, pw_approximation_polyref(app)->coeffs,
                                                       km, kp, rho, gamma, pw_approximation_scaleref(app), lprec );

        _pw_approximation_modulo( compressed_expansion, expansion, nbmono, nbsect, lprec ); 
    
        int success_fft_be = 0;
#ifdef PWPOLY_HAS_BEFFT
        if (usebefft) {
            /* normalize: set average of non zero values to about 1 */
            _pw_approximation_normalize_fft_input( exp, n_compressed_expansion, compressed_expansion, nbsect, lprec );
            success_fft_be = _pw_approximation_befft( app, n, line, n_compressed_expansion, exp );
        }
#endif
        if (success_fft_be==0) {
#ifdef PW_PROFILE
            clock_t start_fft = clock();
#endif        
            acb_dft_rad2_precomp_inplace(compressed_expansion, pw_approximation_rad2_log2_nbsectref(app), lprec2); 
#ifdef PW_PROFILE
            clock_t end_fft = clock();
            clicks_in_ffts_arb += end_fft - start_fft;
            clicks_in_ffts += end_fft - start_fft;
            nb_call_fft_arb++;
#endif
            
            for (ulong k=0; k<nbsect; k++) {
                acb_set( acb_mat_entry( pw_approximation_approx_coeffsref(app)+n, k, line ), compressed_expansion + k );
            }
        }
    }
    
    pw_approximation_approx_lengthsref(app)[n]=line;
    
#ifdef PWPOLY_HAS_BEFFT
    if (usebefft) {
        fmpz_clear(exp);
        _acb_vec_clear( n_compressed_expansion, nbsect );
    }
#endif
    
    arb_clear(stop_crit);
    
    _arb_vec_clear(expansion_stop_crite, nbmono );
    _acb_vec_clear( compressed_expansion, nbsect );
    _acb_vec_clear( expansion, nbmono );
    
#ifdef PW_PROFILE
    clicks_in_approx_annulii += (clock() - start);
#endif
}

void pw_approximation_compute_approx_annulii( pw_approximation_t app, int usebefft ){
    
    ulong N = pw_approximation_Nref(app);
    for (ulong n=0; n<N; n++) {
        pw_approximation_compute_approx_annulus( app, n, usebefft );
    }
}

ulong pw_approximation_get_approx(acb_srcptr * coeffs, const pw_approximation_t app, ulong n, slong k ){
    ulong K = pw_approximation_nbsectref(app)[n];
    *coeffs = acb_mat_entry( pw_approximation_approx_coeffsref(app)+n, (-k)%K, 0 );
    return pw_approximation_approx_lengthsref(app)[n];
}

/* compute an upper bound to max_i |f_i| |point|^i where f = initial polynomial without error */
/*                         = max_i |a_i| 2^(w_i) |point|^i where a = mantissas, w = exponents */
/*                        <= max_i 2^(w_i) |point|^i since |a_i|<1 for all i                  */
// void _pw_approximation_ubound_hatf( arb_t error, const pw_approximation_t app, const acb_t point, slong prec PW_VERBOSE_ARGU(verbose)  ) {
void _pw_approximation_ubound_hatf( arb_t error, const pw_approximation_t app, const arb_t abspoint, slong prec PW_VERBOSE_ARGU(verbose)  ) {
#ifndef PW_SILENT    
    int level = 4;
#endif    
    if (arb_is_zero(abspoint)) {
        arb_zero(error); 
        return;
    }
    
    arb_t logabspoint, temp;
    arb_init(logabspoint);
    arb_init(temp);
    
    /* compute wmax(point):= max_i 2^(w_i) |point|^i */
    arb_log_base_ui(logabspoint, abspoint, 2, prec);
#ifndef PW_SILENT     
    if (verbose>=level) {
        printf("--------------_pw_approximation_ubound_hatf-------------------\n");
        printf("abspoint   : "); arb_printd(abspoint, 10); printf("\n");
    }
#endif    
    ulong min, max;
    _pw_covering_arb_searchsorted( &min, &max, logabspoint, pw_approximation_sl_at_edref(app), pw_approximation_lenref(app)-1, prec );
    arb_pow_ui( error, abspoint, min, prec );
    arb_mul_2exp_fmpz(error, error, fmpq_numref(pw_approximation_exponentsref(app)+min));
    for (ulong i = min+1; i<=max; i++) {
        arb_pow_ui( temp, abspoint, i, prec );
        arb_mul_2exp_fmpz(temp, temp, fmpq_numref(pw_approximation_exponentsref(app)+i));
        arb_union( error, error, temp, prec );
    }
#ifndef PW_SILENT     
    if (verbose>=level) {
        printf("error      : "); arb_printd(error, 10); printf("\n");
    }
#endif
    arb_clear(logabspoint);
    arb_clear(temp);
}

/* compute an upper bound to (2^-m + 2^(errormax))*ftilde(point) */
/* where ftilde(point) = |f_0| + |f_1||point| + ... + |f_d||point|^d where f is the initial polynomial without error */
/* where errormax is the max of the relative errors of the coeffs of the input polynomial */
/* ftilde(point) <=  (d+1) * fmax(point) where fmax(point) = max_i |f_i| |point|^i */
// void _pw_approximation_evaluation_error( arb_t error, const pw_approximation_t app, const acb_t point, slong prec PW_VERBOSE_ARGU(verbose)  ){
void _pw_approximation_evaluation_error( arb_t error, const pw_approximation_t app, const arb_t abspoint, slong prec PW_VERBOSE_ARGU(verbose)  ){

#ifndef PW_SILENT     
    int level = 4;
#endif
    if (arb_is_zero(abspoint)) {
        arb_zero(error); 
        return;
    }
    
    /* compute error = wmax(point) >= max_i |f_i| |point|^i where f = initial polynomial without error */
    _pw_approximation_ubound_hatf( error, app, abspoint, prec PW_VERBOSE_CALL(verbose)  );
      
    arb_mul_ui(error, error, pw_approximation_lenref(app), prec);
    
    if (pw_approximation_isExactref(app)) {
        /* compute 2^(-m)(d+1)wmax(point) */
        arb_mul_2exp_si(error, error, -pw_approximation_working_mref(app));
    } else {
        /* compute (2^(-m)+2^errormax)*(d+1)wmax(point) */
        arb_t error2;
        arb_init(error2);
        arb_mul_2exp_si(error2, error, pw_approximation_maxRelErrorBitsref(app));
        arb_mul_2exp_si(error, error, -pw_approximation_working_mref(app));
        arb_add(error, error, error2, prec);
        arb_clear(error2);
    }
    
#ifndef PW_SILENT     
    if (verbose>=level) {
        printf("--------------_pw_approximation_evaluation_error-------------------\n");
        printf("error      : "); arb_printd(error, 10); printf("\n");
    }
#endif
}

void pw_approximation_evaluate_acb( acb_t value, const pw_approximation_t app, const acb_t point PW_VERBOSE_ARGU(verbose)  ){
    
#ifdef PW_PROFILE
    clock_t start = clock();
#endif 
#ifndef PW_SILENT 
    int level = 4;
#endif 
    slong lprec = pw_approximation_precref(app);
    
    pw_sector_list_t sectors;
    pw_sector_list_init(sectors);
    
    ulong N    = pw_approximation_Nref(app);
    
    pw_covering_locate_point( sectors, 0, pw_approximation_coveringref(app), point, lprec );
#ifndef PW_SILENT     
    if (verbose>=level) {
        printf("_pw_approximation_evaluate_acb: indices: ");
        pw_sector_list_print(sectors);
    }
#endif    
    arb_t error, abspoint;
    arb_init(error);
    arb_init(abspoint);
    acb_abs(abspoint, point, lprec);
    arb_get_ubound_arf( arb_midref(abspoint), abspoint, lprec );
    mag_zero( arb_radref(abspoint) );
    _pw_approximation_evaluation_error( error, app, abspoint, lprec PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT
    if (verbose>=level) {
        printf("_pw_approximation_evaluate_acb: error: "); arb_printd(error, 10); printf("\n");
    }
#endif    
    acb_t powpoint, shipoint, valuetemp;
    acb_init(powpoint);
    acb_init(shipoint);
    acb_init(valuetemp);
    ulong nprev = 0;
    pw_sector_list_iterator it = pw_sector_list_begin( sectors );
    ulong i=0;
    while( it!=pw_sector_list_end() ) {
        pw_sector_ptr sector = pw_sector_list_elmt(it);
        ulong n = pw_sector_nref(sector);
        ulong k = pw_sector_kref(sector);
        ulong K = pw_approximation_nbsectref(app)[n];
        
        if ( (i==0) || (n!=nprev) ) {
            if (n<N-1)
                acb_pow_ui( powpoint, point, pw_approximation_indlowref(app)[n], lprec );
            else
                acb_pow_ui( powpoint, point, pw_approximation_indhigref(app)[n-1], lprec );
        }
        nprev = n;
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pw_approximation_evaluate_acb: \n");
            printf(" index        : (%lu/%lu, %ld/%ld) \n", n, N-1,  k, K-1 );
            printf(" point        : "); acb_printd( point, 10); printf("\n");
            printf(" indlow       : %lu\n", pw_approximation_indlowref(app)[n]);
            printf(" powered point: "); acb_printd( powpoint, 10); printf("\n");
        }
#endif        
        acb_srcptr coeffs;
        ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, k );
        if (K==1) {
            if (n<N-1)
                _acb_poly_evaluate(valuetemp, coeffs, lenapprox, point, lprec);
            else {
                acb_inv(shipoint, point, lprec);
                _acb_poly_evaluate(valuetemp, coeffs, lenapprox, shipoint, lprec);
            }
        } else {
            /* shift point: z = (gamma + scale*rho*x)e^(i2Pik/K) */
            /*          <=> x = (1/(scale*rho))*(z*e^(-i2Pik/K)-gamma) */
            _pw_covering_shift_point( shipoint, pw_approximation_coveringref(app), n, k, point, lprec );
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pw_approximation_evaluate_acb: shifted point: "); acb_printd( shipoint, 10); printf("\n");
            }
#endif
            /* evaluate point */
            _acb_poly_evaluate(valuetemp, coeffs, lenapprox, shipoint, lprec);
        }       
        acb_mul(valuetemp, valuetemp, powpoint, lprec);
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pw_approximation_evaluate_acb: evaluation before error: "); acb_printd(valuetemp, 10); printf("\n");
        }
#endif        
        acb_add_error_arb( valuetemp, error );
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pw_approximation_evaluate_acb: evaluation after error: "); acb_printd(valuetemp, 10); printf("\n");
        }
#endif        
        if (i==0) {
            acb_set(value, valuetemp);
        } else {
            acb_union(value, value, valuetemp, lprec);
        }
        
#ifdef PWPOLY_DEBUG_EVALUATION
        acb_t valn;
        acb_init(valn);
        acb_poly_evaluate_rectangular(valn, pw_approximation_polyInitref(app), point, pw_approximation_precref(app));
        int overlaps    = acb_overlaps(valn, value);
        if ( !overlaps ) {
            printf("DEBUG !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! _pw_approximation_evaluate_acb \n");
            printf("      EVALUATION NOT CORRECT: overlaps: %d\n", overlaps );
            printf("      index                    : (%lu/%lu, %ld/%ld) \n", n, N-1, k, K-1 );
            printf("      point                    : "); acb_printd(point, 10); printf("\n");                
            printf("      acb evaluation           : "); acb_printd(valn, 10); printf("\n");
            printf("      pw  evaluation           : "); acb_printd(value, 10); printf("\n");
            printf("\n");
        }
        acb_clear(valn);
#endif
        i++;
        it = pw_sector_list_next(it);
    }
    
    acb_clear(powpoint);
    acb_clear(shipoint);
    acb_clear(valuetemp);
    
    arb_clear(abspoint);
    arb_clear(error);
    
    pw_sector_list_clear(sectors);
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("\n");
    }
#endif    
    
#ifdef PW_PROFILE
    clicks_in_evaluate += (clock() - start);
#endif 
    
}

void _pw_approximation_evaluate2_acb_non_shifted_annulus( acb_t value, acb_t valueder, const pw_approximation_t app, ulong n, const acb_t point PW_VERBOSE_ARGU(verbose)  ) {
    slong lprec = pw_approximation_precref(app);
    slong km = pw_approximation_indlowref(app)[n];
    
    arb_t error, abspoint;
    arb_init(error);
    arb_init(abspoint);
    
    acb_srcptr coeffs;
    ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, 0 );
    _acb_poly_evaluate2(value, valueder, coeffs, lenapprox, point, lprec);
    
// #ifndef PW_SILENT
//     printf("_pw_approximation_evaluate2_acb_non_shifted_annulus:\n");
//     printf(" point: "); acb_printd(point, 10); printf("\n");
//     printf(" value: "); acb_printd(value, 10); printf("\n");
//     printf(" valueder: "); acb_printd(valueder, 10); printf("\n");
// #endif
    
    if (km>0) {
        acb_t powpoint;
        acb_init(powpoint);
        acb_pow_ui(powpoint, point, km, lprec);
        acb_mul(value, value, powpoint, lprec);       /* value    = z^km g(z) */
        acb_mul(valueder, valueder, powpoint, lprec); /* valueder = z^km g'(z) */
        acb_div(powpoint, value, point, lprec);       /* powpoint = z^(km-1) g(z) */
        acb_mul_ui(powpoint, powpoint, km, lprec);     /* powpoint = kmz^(km-1) g(z) */
        acb_add(valueder, valueder, powpoint, lprec); /* valueder = kmz^(km-1) g(z) + z^km g'(z) */
        acb_clear(powpoint);
    }
    
    /* add evaluation error to value                                       */
    /* evaluation error <= (2^-m + 2^(errormax))*ftilde(point)             */
    /*                  <= error:= (2^-m + 2^(errormax))*(d+1)*wmax(point) */ 
    acb_abs(abspoint, point, lprec);
    arb_get_ubound_arf( arb_midref(abspoint), abspoint, lprec );
    mag_zero( arb_radref(abspoint) );
    _pw_approximation_evaluation_error( error, app, abspoint, lprec PW_VERBOSE_CALL(verbose)  );
    acb_add_error_arb( value, error );
    /* add evaluation error to valueder                                        */
    /* evaluation error <= (2^-m + 2^(errormax))*((d+1)/|point|)*ftilde(point) */
    /*                  <= (2^-m + 2^(errormax))*((d+1)^2/|point|)*wmax(point) */
    if (!acb_is_zero(point)){
        arb_div(error, error, abspoint, lprec);
        arb_mul_ui(error, error, pw_approximation_lenref(app), lprec);
    };
    acb_add_error_arb( valueder, error );
    
    arb_clear(abspoint);
    arb_clear(error);
}

void _pw_approximation_evaluate2_acb_last_annulus( acb_t value, acb_t valueder, const pw_approximation_t app, const acb_t point PW_VERBOSE_ARGU(verbose)  ) {
    
    ulong N    = pw_approximation_Nref(app);
    ulong d    = pw_approximation_lenref(app)-1;
    slong lprec = pw_approximation_precref(app);
    
    acb_t invpoint, powpoint;
    acb_init(invpoint);
    acb_init(powpoint);
    arb_t error, abspoint;
    arb_init(error);
    arb_init(abspoint);
    
    acb_inv(invpoint, point, lprec);
    acb_pow_ui(powpoint, point, d-2, lprec);
    
    acb_srcptr coeffs;
    ulong lenapprox = pw_approximation_get_approx(&coeffs, app, N-1, 0 );
    _acb_poly_evaluate2(value, valueder, coeffs, lenapprox, invpoint, lprec);
//     printf("value   : "); acb_printd(value, 10); printf("\n");
//     printf("valueder: "); acb_printd(valueder, 10); printf("\n");
    
    /* set valueder to dz^(d-1)g(1/z) - z^(d-2)g'(1/z) */
    acb_mul( valueder, valueder, powpoint, lprec );
    acb_mul( powpoint, powpoint, point, lprec );
    acb_mul( invpoint, value, powpoint, lprec );
    acb_mul_ui( invpoint, invpoint, d, lprec );
    acb_sub(valueder, invpoint, valueder, lprec );
    /* set value to z^d * g(1/z) */
    acb_mul( powpoint, powpoint, point, lprec );
    acb_mul(value, value, powpoint, lprec );
    
    /* add evaluation error to value                                       */
    /* evaluation error <= (2^-m + 2^(errormax))*ftilde(point)             */
    /*                  <= error:= (2^-m + 2^(errormax))*(d+1)*wmax(point) */
    acb_abs(abspoint, point, lprec);
    arb_get_ubound_arf( arb_midref(abspoint), abspoint, lprec );
    mag_zero( arb_radref(abspoint) );
    _pw_approximation_evaluation_error( error, app, abspoint, lprec PW_VERBOSE_CALL(verbose)  );
    acb_add_error_arb( value, error );
    /* add evaluation error to valueder                                        */
    /* evaluation error <= (2^-m + 2^(errormax))*((d+1)/|point|)*ftilde(point) */
    /*                  <= (2^-m + 2^(errormax))*((d+1)^2/|point|)*wmax(point) */
    arb_div(error, error, abspoint, lprec);
    arb_mul_ui(error, error, pw_approximation_lenref(app), lprec);
    acb_add_error_arb( valueder, error );
    
    arb_clear(abspoint);
    arb_clear(error);
    acb_clear(invpoint);
    acb_clear(powpoint);
}

void _pw_approximation_evaluate2_acb_nk( acb_t value, acb_t valueder, const pw_approximation_t app, ulong n, ulong k, const acb_t point PW_VERBOSE_ARGU(verbose)  ) {
#ifndef PW_SILENT    
    int level=4;
#endif
    ulong K = pw_approximation_nbsectref(app)[n];
    ulong N    = pw_approximation_Nref(app);
    if ( (n<N-1) && (K==1) )
        return _pw_approximation_evaluate2_acb_non_shifted_annulus( value, valueder, app, n, point PW_VERBOSE_CALL(verbose)  );
    if (n==N-1)
        return _pw_approximation_evaluate2_acb_last_annulus( value, valueder, app, point PW_VERBOSE_CALL(verbose)  );
    
    slong lprec = pw_approximation_precref(app);
    slong km = pw_approximation_indlowref(app)[n];
    
    acb_t shipoint, powpoint, unitroot;
    acb_init(shipoint);
    acb_init(powpoint);
    acb_init(unitroot);
    
    arb_t rinv, error, abspoint;
    arb_init(rinv);
    arb_init(error);
    arb_init(abspoint);
    
    _pw_covering_get_root_of_unit( unitroot, pw_approximation_coveringref(app), -k, K );
    arb_set_fmpq( rinv, pw_approximation_scaleref(app), lprec );
    arb_mul( rinv, rinv, pw_approximation_app_rhosref(app)+n, lprec );
    arb_inv( rinv, rinv, lprec);
    
    acb_srcptr coeffs;
    ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, k );
    _pw_covering_shift_point( shipoint, pw_approximation_coveringref(app), n, k, point, lprec );
    _acb_poly_evaluate2(value, valueder, coeffs, lenapprox, shipoint, lprec);
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf(" pw evaluation before mult: "); acb_printd(value, 10); printf("\n");
    }
#endif    
    if (km>0) {
        acb_pow_ui(powpoint, point, km-1, lprec);
    
        acb_mul(valueder, valueder, point, lprec);
        acb_mul(valueder, valueder, unitroot, lprec);
        acb_mul_arb(valueder, valueder, rinv, lprec);
        acb_mul_ui(unitroot, value, km, lprec);
        acb_add(valueder, valueder, unitroot, lprec);
        acb_mul(valueder, valueder, powpoint, lprec);
    
        acb_mul(value, value, point, lprec);
//         acb_pow_ui(powpoint, point, km, lprec);
        acb_mul(value, value, powpoint, lprec);
    } else {
        acb_mul(valueder, valueder, unitroot, lprec);
        acb_mul_arb(valueder, valueder, rinv, lprec);
    }
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf(" pw evaluation before error: "); acb_printd(value, 10); printf("\n");
    }
#endif    
    /* add evaluation error to value                                       */
    /* evaluation error <= (2^-m + 2^(errormax))*ftilde(point)             */
    /*                  <= error:= (2^-m + 2^(errormax))*(d+1)*wmax(point) */
    acb_abs(abspoint, point, lprec);
    arb_get_ubound_arf( arb_midref(abspoint), abspoint, lprec );
    mag_zero( arb_radref(abspoint) );
    _pw_approximation_evaluation_error( error, app, abspoint, lprec PW_VERBOSE_CALL(verbose)  );
    acb_add_error_arb( value, error );
    /* add evaluation error to valueder                                                         */
    /* evaluation error <= (2^-m + 2^(errormax))*((d+1)/|point|)*ftilde(point)                  */
    /*                  <= (2^-m + 2^(errormax))*((d+1)^2(1/|point|+1/(scale*rho)))*wmax(point) */
    arb_inv(abspoint, abspoint, lprec);
    arb_add(abspoint, abspoint, rinv, lprec);
    arb_mul(error, error, abspoint, lprec);
    arb_mul_ui(error, error, pw_approximation_lenref(app), lprec);
    acb_add_error_arb( valueder, error );
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf(" pw evaluation after error: "); acb_printd(value, 10); printf("\n");
        printf("                devivative: "); acb_printd(valueder, 10); printf("\n");
    }
#endif    
    acb_clear(shipoint);
    acb_clear(powpoint);
    acb_clear(unitroot);
    arb_clear(rinv);
    arb_clear(error);
    arb_clear(abspoint);
    
}

void pw_approximation_evaluate2_acb( acb_t value, acb_t valueder, const pw_approximation_t app, const acb_t point PW_VERBOSE_ARGU(verbose)  ) {

#ifdef PW_PROFILE
    clock_t start = clock();
#endif
#ifndef PW_SILENT    
    int level=4;
#endif
    /* locate the point in a list of sectors */
    slong lprec = pw_approximation_precref(app);
    
    pw_sector_list_t sectors;
    pw_sector_list_init(sectors);
    
    pw_covering_locate_point( sectors, 0, pw_approximation_coveringref(app), point, lprec );
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("_pw_approximation_evaluate_acb: indices: ");
        pw_sector_list_print(sectors);
    }
#endif    
    /* for each sector, use evaluate2 in sector */
    acb_t vt, vtder;
    acb_init(vt);
    acb_init(vtder);
    pw_sector_list_iterator it = pw_sector_list_begin( sectors );
    ulong i=0;
    while( it!=pw_sector_list_end() ) {
        pw_sector_ptr sector = pw_sector_list_elmt(it);
        ulong n = pw_sector_nref(sector);
        ulong k = pw_sector_kref(sector);        
        _pw_approximation_evaluate2_acb_nk( vt, vtder, app, n, k, point PW_VERBOSE_CALL(verbose)  );
        if (i==0) {
            acb_set(value, vt);
            acb_set(valueder, vtder);
        } else {
            acb_union(value, value, vt, lprec);
            acb_union(valueder, valueder, vtder, lprec);
        }
#ifndef PW_SILENT
        if (verbose>=level) {
            ulong N    = pw_approximation_Nref(app);
            printf(" index        : (%lu/%lu, %ld/%ld) \n", n, N-1,  k, pw_approximation_nbsectref(app)[n]-1 );
            printf(" point        : "); acb_printd(point, 10); printf("\n");
            printf(" evaluation   : "); acb_printd(vt, 10); printf("\n");
            printf(" derivative   : "); acb_printd(vtder, 10); printf("\n");
        }
#endif        
#ifdef PWPOLY_DEBUG_EVALUATION
        ulong N    = pw_approximation_Nref(app);
        acb_t valn, valdern;
        acb_init(valn);
        acb_init(valdern);
        acb_poly_evaluate2_rectangular(valn, valdern, pw_approximation_polyInitref(app), point, pw_approximation_working_mref(app));
        
        int overlaps    = acb_overlaps(valn, value);
        int overlapsder = acb_overlaps(valdern, valueder); 
        
        if ( (!overlaps) || (!overlapsder) ) {
            printf("DEBUG !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! _pw_approximation_evaluate2_acb \n");
            printf("      EVALUATION NOT CORRECT: overlaps: %d, overlapsder: %d\n", overlaps, overlapsder );
            printf("      index                    : (%lu/%lu, %ld/%ld) \n", n, N-1,  k, pw_approximation_nbsectref(app)[n]-1 );
            printf("      point                    : "); acb_printd(point, 10); printf("\n");                
            printf("      acb evaluation           : "); acb_printd(valn, 10); printf("\n");
            printf("          derivative           : "); acb_printd(valdern, 10); printf("\n");
            printf("      pw  evaluation   : "); acb_printd(value, 10); printf("\n");
            printf("          derivative   : "); acb_printd(valueder, 10); printf("\n");
            printf("\n");
        }
        acb_clear(valn);
        acb_clear(valdern);
#endif
        i++;
        it = pw_sector_list_next(it); 
    }
    acb_clear(vt);
    acb_clear(vtder);
    
    pw_sector_list_clear(sectors);
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("\n");
    }
#endif    
#ifdef PW_PROFILE
    clicks_in_evaluate += (clock() - start);
#endif
}

void _pw_approximation_evaluate_phi_3_errors_non_shifted( arb_t error, arb_t errorp, arb_t errorpp, arb_t eaddpp, 
                                                          const arb_t twommhatfOzl, 
                                                          const pw_approximation_t app, ulong n, const acb_t z  ) {
    slong prec  = pw_approximation_precref(app);
    ulong N     = pw_approximation_Nref(app);
    slong d     = pw_approximation_lenref(app)-1;
    ulong l     = pw_approximation_indlowref(app)[n];
    ulong u     = (n<N-1 ? pw_approximation_indlowref(app)[n+1] : (ulong)d);
    ulong delta = u-l;
    
    arb_t absz, absz2, falld, falldelta;
    arb_init(absz);
    arb_init(absz2);
    arb_init(falld);
    arb_init(falldelta);
    
    /* compute |z| */
    acb_abs(absz, z, prec);
    /* compute error of phi = (d-delta) * 2^(-m)hatf(|z|)/(|z|^l) */
    arb_mul_ui( error, twommhatfOzl, d-delta, prec);
    /* compute error of phip = ( d(d+1)-delta(delta+1) )/2 * 2^(-m)hatf(|z|)/(|z|^l+1)  */
    arb_set_ui(falld, d+1);
    arb_mul_ui(falld, falld, d, prec);
    arb_set_ui(falldelta, delta+1);
    arb_mul_ui(falldelta, falldelta, delta, prec);
    arb_sub(errorp, falld, falldelta, prec);
    arb_div_ui(errorp, errorp, 2, prec);
    arb_mul(errorp, errorp, twommhatfOzl, prec);
    arb_div(errorp, errorp, absz, prec);
    /* compute error of phipp = ( (d-1)d(d+1)-(delta-1)delta(delta+1) )/3 * 2^(-m)hatf(|z|)/(|z|^l+2) */
    arb_mul_ui(falld, falld, d-1, prec);
    arb_mul_ui(falldelta, falldelta, delta-1, prec);
    arb_sub(errorpp, falld, falldelta, prec);
    arb_div_ui(errorpp, errorpp, 3, prec);
    arb_mul(errorpp, errorpp, twommhatfOzl, prec);
    arb_sqr(absz2, absz, prec);
    arb_div(errorpp, errorpp, absz2, prec);
    /* compute additionnal error on phipp = 2*( (delta+1)...(delta-2)/4 */
    /*                                    + 2^(-m) ( (d+1)...(d-2) - (delta+1)...(delta-2) )/4 )hatf(|z|)/(|z|^l+3) */
    arb_mul_ui(falld, falld, d-2, prec);
    arb_mul_ui(falldelta, falldelta, delta-2, prec);
    arb_sub( falld, falld, falldelta, prec);
    arb_mul_2exp_si( falld, falld, -2);
    arb_mul_2exp_si( falldelta, falldelta, -2);
    arb_mul_2exp_si( falldelta, falldelta, -2);
    arb_mul_2exp_si( falldelta, falldelta, pw_approximation_working_mref(app));
    arb_add(eaddpp, falldelta, falld, prec);
    arb_mul(eaddpp, eaddpp, twommhatfOzl, prec);
    arb_mul(absz, absz2, absz, prec);
    arb_div(eaddpp, eaddpp, absz, prec);
    arb_mul_2exp_si( eaddpp, eaddpp, 1);
    
    
    arb_clear(falldelta);
    arb_clear(falld);
    arb_clear(absz2);
    arb_clear(absz);
    
}

void _pw_approximation_evaluate_phi_3_errors_shifted( arb_t error, arb_t errorp, arb_t errorpp, 
                                                      const arb_t twommhatfOzl, const arb_t rinv,
                                                      const pw_approximation_t app, ulong n, ulong lenapprox  ) {
    slong prec  = pw_approximation_precref(app);
    ulong N     = pw_approximation_Nref(app);
    slong d     = pw_approximation_lenref(app)-1;
    ulong l     = pw_approximation_indlowref(app)[n];
    ulong u     = (n<N-1 ? pw_approximation_indlowref(app)[n+1] : (ulong)d);
    ulong delta = u-l;
    
    
    arb_t qdeltap1, temp;
    arb_init(qdeltap1);
    arb_init(temp);
    /* compute qdeltap1 = q*(delta+1) <= (delta+1)*2^(-m)hatf(|z|)/(|z|^l) */
    arb_mul_ui( qdeltap1, twommhatfOzl, delta + 1, prec);
    /* add q*(delta+1) to error of phi */
    arb_add( error, error, qdeltap1, prec);
    /* add ((dg+2)rinv)q*(delta+1) to error of phi' */
    arb_mul_ui(temp, rinv, (lenapprox-1) + 2, prec);
    arb_mul(temp, temp, qdeltap1, prec);
    arb_add(errorp, errorp, temp, prec);
    /* add ((dg^2+3dg+4)rinv)q*(delta+1) to error of phi'' */
    arb_set_ui(temp, lenapprox -1);
    arb_sqr(temp, temp, prec);
    arb_add_ui(temp, temp, 3*(lenapprox-1) + 4, prec);
    arb_mul(temp, temp, rinv, prec);
    arb_mul(temp, temp, qdeltap1, prec);
    arb_add(errorpp, errorpp, temp, prec);
    
    arb_clear(temp);
    arb_clear(qdeltap1);
}

/* assume n<N-1 */
void _pw_approximation_evaluate_phi_3_non_shifted_annulus( acb_t phi, acb_t phip, acb_t phipp, arb_t eaddpp, 
                                                           const arb_t twommhatfOzl, 
                                                           const pw_approximation_t app, ulong n, const acb_t z  ) {
    slong prec = pw_approximation_precref(app);
    
    arb_t error, errorp, errorpp;
    arb_init(error);
    arb_init(errorp);
    arb_init(errorpp);
    
    /* evaluate g, g', g'' at  z */
    acb_srcptr coeffs;
    ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, 0 );
    acb_ptr coeffsder = _acb_vec_init(lenapprox-1);
    _acb_poly_derivative(coeffsder, coeffs, lenapprox, prec);
    _acb_poly_evaluate(phi, coeffs, lenapprox, z, prec);
    _acb_poly_evaluate2(phip, phipp, coeffsder, lenapprox-1, z, prec);
    /* get and add errors */
    _pw_approximation_evaluate_phi_3_errors_non_shifted( error, errorp, errorpp, eaddpp, twommhatfOzl, app, n, z );
    acb_add_error_arb(phi, error);
    acb_add_error_arb(phip, errorp);
    acb_add_error_arb(phipp, errorpp);
    
    /* free allocated memory */
    _acb_vec_clear(coeffsder, lenapprox-1);
    arb_clear(errorpp);
    arb_clear(errorp);
    arb_clear(error);
    
}

void _pw_approximation_evaluate_phi_3_last_annulus( acb_t phi, acb_t phip, acb_t phipp, arb_t eaddpp, 
                                                    const arb_t twommhatfOzl, 
                                                    const pw_approximation_t app, const acb_t z  ) {
    
    slong prec = pw_approximation_precref(app);
    ulong N    = pw_approximation_Nref(app);
    ulong n    = N-1;
    slong d = pw_approximation_lenref(app)-1;
    
    acb_t invpoint, zd, zdm1, zdm2, zdm3, zdm4;
    acb_init(invpoint);
    acb_init(zd);
    acb_init(zdm1);
    acb_init(zdm2);
    acb_init(zdm3);
    acb_init(zdm4);
    arb_t error, errorp, errorpp;
    arb_init(error);
    arb_init(errorp);
    arb_init(errorpp);
    
    /* compute 1/z */
    acb_inv(invpoint, z, prec);
    /* compute z^d, z^(d-1), z^(d-2), z^(d-3), z^(d-4) */
    acb_pow_ui(zdm4, z, d-4, prec);
    acb_pow_ui(zdm3, z, d-3, prec);
    acb_pow_ui(zdm2, z, d-2, prec);
    acb_pow_ui(zdm1, z, d-1, prec);
    acb_pow_ui(zd,   z, d  , prec);
//     acb_mul(zdm3, zdm4, z, prec);
//     acb_sqr(zd, z, prec);
//     acb_mul(zdm2, zdm4, zd, prec);
//     acb_mul(zdm1, zdm3, zd, prec);
//     acb_mul(zd, zdm2, zd, prec);
    /* evaluate g, g', g'' at inv z */
    acb_srcptr coeffs;
    ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, 0 );
    acb_ptr coeffsder = _acb_vec_init(lenapprox-1);
    _acb_poly_derivative(coeffsder, coeffs, lenapprox, prec);
    _acb_poly_evaluate(phi, coeffs, lenapprox, invpoint, prec);
    _acb_poly_evaluate2(phip, phipp, coeffsder, lenapprox-1, invpoint, prec);
//     _acb_poly_evaluate2(phi, phip, coeffs, lenapprox, invpoint, prec);
//     printf("phi     : "); acb_printd(phi, 10); printf("\n");
//     printf("phip    : "); acb_printd(phip, 10); printf("\n");
    /* compute phi''(z) \simeq d(d-1)z^(d-2) g(inv z) + 2(1-d)z^(d-3)g'(inv z) + z^(d-4)g''(inv z) */
    acb_mul(phipp, phipp, zdm4, prec);
    acb_mul(zdm4, phip, zdm3, prec);
    acb_mul_si(zdm4, zdm4, -2*(d-1), prec);
    acb_add(phipp, phipp, zdm4, prec);
    acb_mul(zdm4, phi, zdm2, prec);
    acb_mul_ui(zdm4, zdm4, d, prec);
    acb_mul_ui(zdm4, zdm4, d-1, prec);
    acb_add(phipp, phipp, zdm4, prec);
    /* compute phi'(z) \simeq dz^(d-1) g(inv z) - z^(d-2)g'(inv z) */
    acb_mul(phip, phip, zdm2, prec);
    acb_mul(zdm2, phi, zdm1, prec);
    acb_mul_ui(zdm2, zdm2, d, prec);
    acb_sub(phip, zdm2, phip, prec);
    /* compute phi(z) \simeq z^d g(inv z) */
    acb_mul( phi, phi, zd, prec);
    /* get and add errors */
    _pw_approximation_evaluate_phi_3_errors_non_shifted( error, errorp, errorpp, eaddpp, twommhatfOzl, app, n, z );
    acb_add_error_arb(phi, error);
    acb_add_error_arb(phip, errorp);
    acb_add_error_arb(phipp, errorpp);
    
    /* free allocated memory */
    _acb_vec_clear(coeffsder, lenapprox-1);
    arb_clear(errorpp);
    arb_clear(errorp);
    arb_clear(error);
    acb_clear(zdm4);
    acb_clear(zdm3);
    acb_clear(zdm2);
    acb_clear(zdm1);
    acb_clear(zd);
    acb_clear(invpoint);
    
}

void _pw_approximation_evaluate_phi_3_n_k( acb_t phi, acb_t phip, acb_t phipp, arb_t eaddpp,
                                           const arb_t twommhatfOzl, 
                                           const pw_approximation_t app, ulong n, ulong k, const acb_t z  ) {
    
    ulong K = pw_approximation_nbsectref(app)[n];
    ulong N    = pw_approximation_Nref(app);
    if ( (n<N-1) && (K==1) ) /* encloses case n==0 */
        return _pw_approximation_evaluate_phi_3_non_shifted_annulus( phi, phip, phipp, eaddpp, twommhatfOzl, app, n, z  );
    if (n==N-1)
        return _pw_approximation_evaluate_phi_3_last_annulus( phi, phip, phipp, eaddpp, twommhatfOzl, app, z   );
    
    slong prec = pw_approximation_precref(app);
    
    acb_t shipoint, unitroot;
    acb_init(shipoint);
    acb_init(unitroot);
    arb_t rinv, error, errorp, errorpp;
    arb_init(rinv);
    arb_init(error);
    arb_init(errorp);
    arb_init(errorpp);
    /* compute rinv = 1/(scale*rho) */
    arb_set_fmpq( rinv, pw_approximation_scaleref(app), prec );
    arb_mul( rinv, rinv, pw_approximation_app_rhosref(app)+n, prec );
    arb_inv( rinv, rinv, prec);
    /* get unit root = e^(-i2Pik/K) */
    _pw_covering_get_root_of_unit( unitroot, pw_approximation_coveringref(app), -k, K );
    /* shift z */
    _pw_covering_shift_point( shipoint, pw_approximation_coveringref(app), n, k, z, prec );
    /* evaluate g, g', g'' at shifted z */
    acb_srcptr coeffs;
    ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, k );
    acb_ptr coeffsder = _acb_vec_init(lenapprox-1);
    _acb_poly_derivative(coeffsder, coeffs, lenapprox, prec);
    _acb_poly_evaluate(phi, coeffs, lenapprox, shipoint, prec);
    _acb_poly_evaluate2(phip, phipp, coeffsder, lenapprox-1, shipoint, prec);
    /* compute (1/(scale*rho))*e^(-i2Pik/K)*g'(shifted z) */
    acb_mul_arb( phip, phip, rinv, prec);
    acb_mul(phip, phip, unitroot, prec);
    /* compute (1/(scale*rho))^2(e^(-i2Pik/K))^2 * g''(shifted z) */
    acb_mul_arb( phipp, phipp, rinv, prec);
    acb_mul_arb( phipp, phipp, rinv, prec);
    acb_mul(phipp, phipp, unitroot, prec);
    acb_mul(phipp, phipp, unitroot, prec);
    /* get and add errors from truncation in annulus */
    _pw_approximation_evaluate_phi_3_errors_non_shifted( error, errorp, errorpp, eaddpp, twommhatfOzl, app, n, z );
    acb_add_error_arb(phi, error);
    acb_add_error_arb(phip, errorp);
    acb_add_error_arb(phipp, errorpp);
    /* get and add errors from shift in annulus */
    _pw_approximation_evaluate_phi_3_errors_shifted( error, errorp, errorpp, twommhatfOzl, rinv, app, n, lenapprox );
    acb_add_error_arb(phi, error);
    acb_add_error_arb(phip, errorp);
    acb_add_error_arb(phipp, errorpp);
    
    /* free allocated memory */
    _acb_vec_clear(coeffsder, lenapprox-1);
    arb_clear(errorpp);
    arb_clear(errorp);
    arb_clear(error);
    arb_clear(rinv);
    acb_clear(unitroot);
    acb_clear(shipoint);
    
}

void pw_approximation_evaluate_phi_3( acb_t phi, acb_t phip, acb_t phipp, arb_t errorpp, 
                                       const pw_approximation_t app, const ulong n, const acb_t z PW_VERBOSE_ARGU(verbose)  ) {
#ifdef PW_PROFILE
    clock_t start = clock();
#endif
#ifndef PW_SILENT
    int level=3;
#endif
    slong prec = pw_approximation_precref(app);
    ulong N    = pw_approximation_Nref(app);
    /* locate the point in a list of sectors */
    pw_sector_list_t sectors;
    pw_sector_list_init(sectors);
    pw_covering_locate_point( sectors, n, pw_approximation_coveringref(app), z, prec );
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("_pw_approximation_evaluate_acb: indices: ");
        pw_sector_list_print(sectors);
    }
#endif    
    arb_t absz, abszl, twommhatf, twommhatfOzl, epp;
    arb_init(absz);
    arb_init(abszl);
    arb_init(twommhatf);
    arb_init(twommhatfOzl);
    arb_init(epp);
    acb_t v, vp, vpp;
    acb_init(v);
    acb_init(vp);
    acb_init(vpp);
    /* compute |z| */
    acb_abs(absz, z, prec);
    /* compute an upper bound to twommhatf = (2^(-m)+2^errormax)hatf(|z|) */
    _pw_approximation_ubound_hatf( twommhatf, app, absz, prec PW_VERBOSE_CALL(verbose)  );
    if (pw_approximation_isExactref(app)) {
        arb_mul_2exp_si(twommhatf, twommhatf, -(slong)pw_approximation_working_mref(app));
    } else {
        arb_t twommhatf2;
        arb_init(twommhatf2);
        arb_mul_2exp_si(twommhatf2, twommhatf, pw_approximation_maxRelErrorBitsref(app));
        arb_mul_2exp_si(twommhatf, twommhatf, -pw_approximation_working_mref(app));
        arb_add(twommhatf, twommhatf, twommhatf2, prec);
        arb_clear(twommhatf2);
    }
#ifndef PW_SILENT
    if (verbose>=level) {
        printf(" upper bound to (2^(-m) +2^(maxerr)) hat f: "); arb_printd(twommhatf, 10); printf("\n");
    }
#endif
    /* for each sector, use evaluate3 in sector */
    pw_sector_list_iterator it = pw_sector_list_begin( sectors );
    ulong i=0;
    while( it!=pw_sector_list_end() ) {
        pw_sector_ptr sector = pw_sector_list_elmt(it);
        ulong n = pw_sector_nref(sector);
        ulong k = pw_sector_kref(sector);        
        slong l     = pw_approximation_indlowref(app)[n];
        /* compute twommhatfOzl = (2^(-m)+2^errormax)hatf(|z|)/(|z|^l) */
        arb_pow_ui(abszl, absz, (ulong)l, prec);
        if (  n < N-1 )
            arb_div(twommhatfOzl, twommhatf, abszl, prec);
        else
            arb_set(twommhatfOzl, twommhatf);
#ifndef PW_SILENT
        if (verbose>=level) {
            printf(" upper bound to (2^(-m) +2^(maxerr)) hat f/|x|^l: "); arb_printd(twommhatfOzl, 10); printf("\n");
        }
#endif
        /* evaluate */
        _pw_approximation_evaluate_phi_3_n_k( v, vp, vpp, epp, twommhatfOzl, app, n, k, z );
        if (i==0) {
            acb_set(phi, v);
            acb_set(phip, vp);
            acb_set(phipp, vpp);
            arb_set(errorpp, epp);
        } else {
            acb_union(phi, phi, v, prec);
            acb_union(phip, phip, vp, prec);
            acb_union(phipp, phipp, vpp, prec);
            arb_union(errorpp, errorpp, epp, prec);
        }
        
#ifdef PWPOLY_DEBUG_EVALUATION
        acb_t valn, valdern, valderdern;
        acb_init(valn);
        acb_init(valdern);
        acb_init(valderdern);
        acb_poly_evaluate2(valn, valdern, pw_approximation_polyInitref(app), z, prec);
        
        acb_poly_t der;
        acb_poly_init(der);
        acb_poly_derivative(der, pw_approximation_polyInitref(app), prec);
        acb_poly_evaluate2( valdern, valderdern, der, z, prec );
        acb_poly_clear(der);
        
        if (  (n < N-1)&&(l>0) ) {
                acb_t zl, temp;
                acb_init(zl);
                acb_init(temp);
                acb_pow_ui(zl, z, (ulong) l, prec);
                acb_mul(v, phi, zl, prec);   /* v = z^l phi(z) */
//                 printf("f(z)   : "); acb_printd(v, 10); printf("\n");
                acb_mul(vp, phip, zl, prec); /* vp = z^l phi'(z) */
                acb_div(v, v, z, prec);
                acb_mul_ui(v, v, (ulong) l, prec); /* v = lz^(l-1) phi(z) */
                acb_add(temp, vp, v, prec); /* temp = lz^(l-1) phi(z) + z^l phi'(z) */
//                 printf("f'(z)  : "); acb_printd(temp, 10); printf("\n");
                acb_mul(vpp, phipp, zl, prec); /* vpp = z^l phi''(z) */
                acb_div(vp, vp, z, prec);    /* vp = z^(l-1) phi'(z) */
                acb_mul_ui(vp, vp, (ulong) 2*l, prec); /* vp = 2lz^(l-1) phi'(z) */
                acb_add(vpp, vpp, vp, prec);
                acb_div(v, v, z, prec); /* v = lz^(l-2) phi(z) */
                acb_mul_ui(v, v, (ulong) l-1, prec); /* v = (l-1)lz^(l-2) phi(z) */
                acb_add(vpp, vpp, v, prec);
//                 printf("f''(z) : "); acb_printd(vpp, 10); printf("\n");
                acb_set(vp, temp);
                acb_clear(temp);
                acb_clear(zl);
            }
                
        int overlaps    = acb_overlaps(valn, v);
        int overlapsder = acb_overlaps(valdern, vp); 
        int overlapsderder = acb_overlaps(valderdern, vpp);
        
        if ( (!overlaps) || (!overlapsder) ) {
            printf("DEBUG !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! _pw_approximation_evaluate_phi_3 \n");
            printf("      EVALUATION NOT CORRECT: overlaps: %d, overlapsder: %d, overlapsderder: %d\n", overlaps, overlapsder, overlapsderder );
            printf("      index                    : (%lu/%lu, %ld/%ld) \n", n, N-1, k, pw_approximation_nbsectref(app)[n]-1 );
            printf("      point                    : "); acb_printd(z, 10); printf("\n");                
            printf("      acb evaluation           : "); acb_printd(valn, 10); printf("\n");
            printf("          derivative           : "); acb_printd(valdern, 10); printf("\n");
            printf("          derivative           : "); acb_printd(valderdern, 10); printf("\n");
            printf("      pw  evaluation   : "); acb_printd(v, 10); printf("\n");
            printf("          derivative   : "); acb_printd(vp, 10); printf("\n");
            printf("          derivative   : "); acb_printd(vpp, 10); printf("\n");
            printf("\n");
        }
        acb_clear(valn);
        acb_clear(valdern);
        acb_clear(valderdern);
#endif        
        i++;
        it = pw_sector_list_next(it);
    }
    
    /* free allocated memory */
    acb_clear(vpp);
    acb_clear(vp);
    acb_clear(v);
    arb_clear(epp);
    arb_clear(twommhatfOzl);
    arb_clear(twommhatf);
    arb_clear(abszl);
    arb_clear(absz);
    pw_sector_list_clear(sectors);
    
#ifdef PW_PROFILE
    clicks_in_evaluate += (clock() - start);
#endif
}

#ifndef PW_SILENT
void pw_approximation_fprint( FILE * f, const pw_approximation_t app ){
    
    ulong len = pw_approximation_lenref(app);
    fprintf(f, "pw_approximation: length: %lu, input m: %lu, working m: %lu, working prec: %ld, realCoeffs: %d\n", 
            len, pw_approximation_output__mref(app), pw_approximation_working_mref(app), pw_approximation_precref(app),
            pw_approximation_realCoeffsref(app));
    fprintf(f, "pw_approximation: exponents of absolute values of approximated input polynomial\n");
    fprintf(f,"=================================================================\n");
    fprintf(f,"[ ");
    for (ulong i=0; i<len; i++) {
        fmpq_fprint(f, pw_approximation_exponentsref(app)+i); fprintf(f," ");
    }
    fprintf(f,"]\n");
    fprintf(f,"=================================================================\n");
    fprintf(f, "pw_approximation: covering of the complex plane:\n");
    fprintf(f,"=================================================================\n");
    _pw_covering_fprint( f, pw_approximation_coveringref(app) );
    fprintf(f,"=================================================================\n");
    
    ulong N = pw_approximation_Nref(app);
    for(ulong n=0; n<N; n++) {
        ulong app_len = pw_approximation_approx_lengthsref(app)[n];
        if ( app_len>0 ) {
            fprintf(f,"=================================================================\n");
            fprintf(f, "pw_approximation: approximations on %lu-th annulus:\n", n);
            fprintf(f,"=================================================================\n");
            ulong K = pw_approximation_nbsectref(app)[n];
            for(ulong k=0; k<K; k++) {
                fprintf(f,"[ ");
                for (ulong j=0; j<app_len; j++){
                    acb_fprintd(f, acb_mat_entry(pw_approximation_approx_coeffsref(app)+n, k, j), 5 );
                    fprintf(f," ");
                }
                fprintf(f,"]\n");
            }
            fprintf(f,"=================================================================\n");
        }
    }
    
}

void pw_approximation_fprint_short( FILE * f, const pw_approximation_t app ){
    
    ulong len = pw_approximation_lenref(app);
    fprintf(f, "pw_approximation: length: %lu, input m: %lu, working m: %lu, working prec: %ld, realCoeffs: %d\n", 
            len, pw_approximation_output__mref(app), pw_approximation_working_mref(app), pw_approximation_precref(app),
            pw_approximation_realCoeffsref(app));
//     fprintf(f, "pw_approximation: exponents of absolute values of approximated input polynomial\n");
//     fprintf(f,"=================================================================\n");
//     fprintf(f,"[ ");
//     for (ulong i=0; i<len; i++) {
//         fmpq_fprint(f, pw_approximation_exponentsref(app)+i); fprintf(f," ");
//     }
//     fprintf(f,"]\n");
    fprintf(f,"=================================================================\n");
    fprintf(f, "pw_approximation: covering of the complex plane:\n");
    fprintf(f,"=================================================================\n");
    _pw_covering_fprint_short( f, pw_approximation_coveringref(app) );
    fprintf(f,"=================================================================\n");
    
    ulong N = pw_approximation_Nref(app);
    ulong cmp1=pw_approximation_cref(app)*pw_approximation_working_mref(app)+1;
    fprintf(f,"=================================================================\n");
    for(ulong n=0; n<N; n++) {
        ulong K   = pw_approximation_nbsectref(app)[n];
        ulong len = pw_approximation_approx_lengthsref(app)[n];
        if (len>0){
            fprintf(f, "pw_approximation: %lu approximations on %lu-th annulus: computed: %d, lengths: %lu / %lu\n", K, n, len>0, len, cmp1);
        }
    }
    fprintf(f,"=================================================================\n");
}
#endif

/* DEPRECATED */

// /* Preconditions:  0<= km <= kp                                                            */
// /*                 rho, gamma and scale are positive real numbers                          */
// /*                 stop_crite is a initialized table of len kp-km+1,                       */
// /* Postconditions: for 0 <= i < kp-km+1, sets stop_crite+i to                              */
// /*                   (2^(-m-1))*(gamma-scale*rho)^i*|coeffs+km+i|                          */
// void _pw_approximation_stopping_criterionOLD( arb_ptr stop_crite, arb_srcptr abscoeffs,
//                                               ulong m, ulong km, ulong kp, const arb_t rho, const arb_t gamma, const fmpq_t scale, slong prec ) {
//     
//     slong len = kp-km+1;
//     arb_t temp;
//     arb_init(temp);
//     /* sets temp to gamma-scale*rho */
//     arb_set_fmpq(temp, scale, prec);
//     arb_mul( temp, temp, rho, prec);
//     arb_sub( temp, gamma, temp, prec);
//     /* compute the powers of gamma-scale*rho */
//     _arb_vec_set_powers(stop_crite, temp, len, prec);
//     /* multiply each by 2^(-m-1)*|coeffs+km+i| */
//     for (slong i=0; i<len; i++){ 
//         arb_mul_2exp_si(stop_crite+i, stop_crite+i, -(slong)m -1 );
//         arb_mul(stop_crite+i, stop_crite+i, abscoeffs + (km+i), prec);
//     }
//     arb_clear(temp);
// }
// 
// /* Preconditions:  0<= km <= kp                                                               */
// /*                 rho, gamma and scale are positive real numbers                             */
// /*                 coeffs is a table of at least kp-km+1 acb                                  */
// /*                 expansion is an initialized table of at least kp-km+1 acb                  */
// /*                 stop_crite is an initialized table of at least kp-km+1 arb                 */
// /*                 if line>0, expansion and expansion_without_co have been obtained as:       */
// /*                           _pw_approximation_Taylor_expansion called with the same arguments*/
// /* Postconditions: for 0 <= i < kp-km+1,                                                      */
// /*                     sets expansion+i to                                                    */
// /*                           (binom(i, line)*(scale*rho/gamma)^line)*(coeffs+km+i)*gamma^i    */
// /*                 where binom(i, line) = choose line among i                                 */
// /*                 returns 1 if for all 0 <= i < kp-km+1,                                     */
// /*                            (scale*rho/gamma)*(i-line)/(line+1) <= 1/2                      */
// /*                        and |expansion+i| <= stop_crite+i                                   */
// /* <=> (binom(i, line)*(scale*rho/gamma)^line)*(coeffs+km+i)*gamma^i <= (2^(-m-1))*(gamma-scale*rho)^i*(coeffs+km+i) */
// /*                 otherwise returns 0                                                        */
// int  _pw_approximation_Taylor_expansionOLD(  acb_ptr expansion, arb_ptr stop_crite, 
//                                              ulong line, acb_srcptr coeffs,
//                                              ulong km, ulong kp, const arb_t rho, const arb_t gamma, const fmpq_t scale, slong prec ) {
// 
// #ifdef PW_PROFILE
//     clock_t start = clock();
// #endif
// 
//     int res = 1;    
// //     printf("km: %lu, kp: %lu, kp-km+1: %lu\n", km, kp, kp-km+1 );
//     if (line==0) {
//         arb_t gamma_pows;
//         arb_init(gamma_pows);
//         arb_one(gamma_pows);
//         for (ulong i=0; i < kp-km+1; i++){
// //             printf("i: %lu\n", i );
//             acb_mul_arb( expansion+i, coeffs + (km+i), gamma_pows, prec);
//             arb_mul( gamma_pows, gamma_pows, gamma, prec );
//         }
//         arb_clear(gamma_pows);
// //         arb_set_si(max, 2);
//         res=0;
//     } else {
//         arb_t scaleTrhoOgamma, fact, ratio, half, temp;
//         arb_init(scaleTrhoOgamma);
//         arb_init(fact);
//         arb_init(ratio);
//         arb_init(half);
//         arb_init(temp);
//         arb_set_d(half, .5);
//         arb_set_fmpq(scaleTrhoOgamma, scale, prec);
//         arb_mul( scaleTrhoOgamma, scaleTrhoOgamma, rho, prec);
//         arb_div( scaleTrhoOgamma, scaleTrhoOgamma, gamma, prec);
//         arb_div_ui( ratio, scaleTrhoOgamma, line+1, prec );
//         arb_div_ui( scaleTrhoOgamma, scaleTrhoOgamma, line, prec );
//         for (ulong i=line-1; i < kp-km+1; i++){
//             arb_mul_si( fact, scaleTrhoOgamma, i-line+1, prec );
//             acb_mul_arb( expansion+i, expansion+i, fact, prec );
// //             arb_mul( expansion_without_co + i, expansion_without_co + i, fact, prec );
// 
//             if ( (i>=line)&&(res) ) {
//                 /* sets fact to (scale*rho/gamma)*(i-line)/(line+1) */
//                 arb_mul_si( fact, ratio, (slong)i -line, prec ); 
//                 res = res && arb_le( fact, half );
//                 if (res) {
//                     acb_abs(fact, expansion+i, prec);
// //                     arb_sqr(fact, acb_realref(expansion+i), prec);
// //                     arb_sqr(temp, acb_imagref(expansion+i), prec);
// //                     arb_add(fact, fact, temp, prec);
//                 }
//                 res = res && arb_le( fact, stop_crite + i );
//             }
//         }
//         arb_clear(temp);
//         arb_clear(half);
//         arb_clear(ratio);
//         arb_clear(fact);
//         arb_clear(scaleTrhoOgamma);
//     }
// #ifdef PW_PROFILE
//     clicks_in_expansion += (clock() - start);
// #endif
//     return res;
// }

// /* Preconditions:  0<= km <= kp                                                            */
// /*                 rho, gamma and scale are positive real numbers                          */
// /*                 stop_crite is a initialized table of len kp-km+1,                       */
// /* Postconditions: for 0 <= i < kp-km+1, sets stop_crite+i to                              */
// /*                   (2^(-m-1))*(gamma-scale*rho)^i*|coeffs+km+i|                          */
// void _pw_approximation_stopping_criterionOLD( arb_ptr stop_crite, arb_srcptr abscoeffs,
//                                               ulong m, ulong km, ulong kp, const arb_t rho, const arb_t gamma, const fmpq_t scale, slong prec ) {
//     
//     slong len = kp-km+1;
//     arb_t temp;
//     arb_init(temp);
//     /* sets temp to gamma-scale*rho */
//     arb_set_fmpq(temp, scale, prec);
//     arb_mul( temp, temp, rho, prec);
//     arb_sub( temp, gamma, temp, prec);
//     /* compute the powers of gamma-scale*rho */
//     _arb_vec_set_powers(stop_crite, temp, len, prec);
//     /* multiply each by 2^(-m-1)*|coeffs+km+i| */
//     for (slong i=0; i<len; i++){ 
// //         acb_abs( temp, coeffs + (km+i), prec );
//         arb_mul_2exp_si(stop_crite+i, stop_crite+i, -(slong)m -1 );
//         arb_mul(stop_crite+i, stop_crite+i, abscoeffs + (km+i), prec);
// //         arb_sqr(stop_crite+i, stop_crite+i, prec);
//     }
//     arb_clear(temp);
// }

// /* Preconditions:  0<= km <= kp                                                               */
// /*                 rho, gamma and scale are positive real numbers                             */
// /*                 coeffs is a table of at least kp-km+1 acb                                  */
// /*                 expansion is an initialized table of at least kp-km+1 acb                  */
// /*                 stop_crite is an initialized table of at least kp-km+1 arb                 */
// /*                 if line>0, expansion and expansion_without_co have been obtained as:       */
// /*                           _pw_approximation_Taylor_expansion called with the same arguments*/
// /* Postconditions: for 0 <= i < kp-km+1,                                                      */
// /*                     sets expansion+i to                                                    */
// /*                           (binom(i, line)*(scale*rho/gamma)^line)*(coeffs+km+i)*gamma^i    */
// /*                 where binom(i, line) = choose line among i                                 */
// /*                 returns 1 if for all 0 <= i < kp-km+1,                                     */
// /*                            (scale*rho/gamma)*(i-line)/(line+1) <= 1/2                      */
// /*                        and |expansion+i| <= stop_crite+i                                   */
// /* <=> (binom(i, line)*(scale*rho/gamma)^line)*(coeffs+km+i)*gamma^i <= (2^(-m-1))*(gamma-scale*rho)^i*(coeffs+km+i) */
// /*                 otherwise returns 0                                                        */
// int  _pw_approximation_Taylor_expansionOLD(  acb_ptr expansion, arb_ptr stop_crite, 
//                                              ulong line, acb_srcptr coeffs,
//                                              ulong km, ulong kp, const arb_t rho, const arb_t gamma, const fmpq_t scale, slong prec ) {
// 
// #ifdef PW_PROFILE
//     clock_t start = clock();
// #endif
// 
//     int res = 1;    
// //     printf("km: %lu, kp: %lu, kp-km+1: %lu\n", km, kp, kp-km+1 );
//     if (line==0) {
//         arb_t gamma_pows;
//         arb_init(gamma_pows);
//         arb_one(gamma_pows);
//         for (ulong i=0; i < kp-km+1; i++){
// //             printf("i: %lu\n", i );
//             acb_mul_arb( expansion+i, coeffs + (km+i), gamma_pows, prec);
//             arb_mul( gamma_pows, gamma_pows, gamma, prec );
//         }
//         arb_clear(gamma_pows);
// //         arb_set_si(max, 2);
//         res=0;
//     } else {
//         arb_t scaleTrhoOgamma, fact, ratio, half, temp;
//         arb_init(scaleTrhoOgamma);
//         arb_init(fact);
//         arb_init(ratio);
//         arb_init(half);
//         arb_init(temp);
//         arb_set_d(half, .5);
//         arb_set_fmpq(scaleTrhoOgamma, scale, prec);
//         arb_mul( scaleTrhoOgamma, scaleTrhoOgamma, rho, prec);
//         arb_div( scaleTrhoOgamma, scaleTrhoOgamma, gamma, prec);
//         arb_div_ui( ratio, scaleTrhoOgamma, line+1, prec );
//         arb_div_ui( scaleTrhoOgamma, scaleTrhoOgamma, line, prec );
//         for (ulong i=line-1; i < kp-km+1; i++){
//             arb_mul_si( fact, scaleTrhoOgamma, i-line+1, prec );
//             acb_mul_arb( expansion+i, expansion+i, fact, prec );
// //             arb_mul( expansion_without_co + i, expansion_without_co + i, fact, prec );
// 
//             if ( (i>=line)&&(res) ) {
//                 /* sets fact to (scale*rho/gamma)*(i-line)/(line+1) */
//                 arb_mul_si( fact, ratio, (slong)i -line, prec ); 
//                 res = res && arb_le( fact, half );
//                 if (res) {
//                     acb_abs(fact, expansion+i, prec);
// //                     arb_sqr(fact, acb_realref(expansion+i), prec);
// //                     arb_sqr(temp, acb_imagref(expansion+i), prec);
// //                     arb_add(fact, fact, temp, prec);
//                 }
//                 res = res && arb_le( fact, stop_crite + i );
//             }
//         }
//         arb_clear(temp);
//         arb_clear(half);
//         arb_clear(ratio);
//         arb_clear(fact);
//         arb_clear(scaleTrhoOgamma);
//     }
// #ifdef PW_PROFILE
//     clicks_in_expansion += (clock() - start);
// #endif
//     return res;
// }

// int  _pw_approximation_Taylor_expansion_0( acb_ptr expansion, 
//                                            acb_srcptr coeffs,
//                                            ulong km, ulong kp, const arb_t gamma, slong prec ) {
// 
// #ifdef PW_PROFILE
//     clock_t start = clock();
// #endif
// 
//     arb_t gamma_pows;
//     arb_init(gamma_pows);
//     arb_one(gamma_pows);
//     for (ulong i=0; i < kp-km+1; i++){
// //         printf("i: %lu\n", i );
//         acb_mul_arb( expansion+i, coeffs + (km+i), gamma_pows, prec);
//         arb_mul( gamma_pows, gamma_pows, gamma, prec );
//     }
//     arb_clear(gamma_pows);
// #ifdef PW_PROFILE
//     clicks_in_expansion_1 += (clock() - start);
// #endif
//        
//     return 0;
// }

// int  _pw_approximation_Taylor_expansion_other( acb_ptr expansion, arb_ptr expansion_without_co, 
//                                                acb_ptr expansion_0, 
//                                                const arb_ptr stop_crite, 
//                                                ulong line,
//                                                const arb_mat_t bins,
//                                                ulong km, ulong kp, const arb_t rho, const arb_t gamma, const fmpq_t scale, slong prec ) {
// 
// #ifdef PW_PROFILE
//     clock_t start = clock();
// #endif
// 
//     int res = 1;   
//     arb_t scaleTrhoOgamma, half, fact, ratio;
//     fmpz_t bin;
//     fmpz_init(bin);
//     arb_init(scaleTrhoOgamma);
//     arb_init(half);
//     arb_init(fact);
//     arb_init(ratio);
//     arb_set_d(half, .5);
//     arb_set_fmpq(scaleTrhoOgamma, scale, prec);
//     arb_mul( scaleTrhoOgamma, scaleTrhoOgamma, rho, prec);
//     arb_div( scaleTrhoOgamma, scaleTrhoOgamma, gamma, prec);
//     arb_div_si( fact, scaleTrhoOgamma, line+1, prec );
//     arb_pow_ui( scaleTrhoOgamma, scaleTrhoOgamma, line, prec);
//     
//     acb_zero( expansion+(line-1) );
//     arb_zero( expansion_without_co+(line-1) );
//     for (ulong i=line; i < kp-km+1; i++){
//         
// //         arb_bin_uiui(expansion_without_co+i, i, line, prec);
// //         arb_mul( expansion_without_co+i, expansion_without_co+i, scaleTrhoOgamma, prec);
//         
// //         fmpz_bin_uiui( bin, i, line);
// //         printf(" i-1: %lu/%ld, line-1: %lu/%ld\n", i-1, bins->r,  line-1, bins->c );
//         arb_mul( expansion_without_co+i, scaleTrhoOgamma, 
//                  arb_mat_entry( bins, line-1, i-1),
//                  prec);
//         
//         acb_mul_arb( expansion+i, expansion_0 + i, expansion_without_co+i, prec );
//         arb_mul_2exp_si( expansion_without_co+i, expansion_without_co+i, 1 );
//         if (res) {
//             /* sets ratio to (scale*rho/gamma)*(i-line)/(line+1) */
//             arb_mul_si( ratio, fact, (slong)i -line, prec );
//             res = res && arb_le( ratio, half );
//             res = res && arb_le( expansion_without_co + i, stop_crite );
//         }
//     }
//     
//     arb_clear(ratio);
//     arb_clear(fact);
//     arb_clear(half);
//     arb_clear(scaleTrhoOgamma);
//     fmpz_clear(bin);
//     
// #ifdef PW_PROFILE
//     clicks_in_expansion_2 += (clock() - start);
// #endif
//     return res;
// }

// /* assume 0 < n < N-1 */
// void _pw_approximation_compute_error_annulus( arb_t error, const pw_approximation_t app, ulong n PW_VERBOSE_ARGU(verbose)  ) {
//     
//     slong lprec = pw_approximation_precref(app);
//     
//     /* compute the error: |mantissas|_1*(2^-m)*(w^max(z))/z^indlow                   */
//     /*                   <= (degree+1)*(2^-m)*(w^max(z))/z^indlow                    */
//     /*       use w^max(z) <= 2^weidom*(2^S)^inddom                                   */
//     /*       and z^indlow >= (2^s)^indlow                                            */
//     /*       then  error <= (degree+1)*(2^-m)*(2^weidom*(2^S)^inddom)/(2^(s*indlow)) */
//     /*                    = (degree+1)*(2^-m)*(2^weidom*2^(S*inddom)*2^(-s*indlow))  */
//     /*                    = (degree+1)*2^(weidom+S*inddom-s*indlow-m)                */
//     /* where: S      = pw_approximation_bigradref(cov)+n                             */
//     /*        s      = pw_approximation_bigradref(cov)+n-1                           */
//     /*        inddom = pw_approximation_inddomref(app)[n]                            */
//     /*        weidom = pw_approximation_exponentsref(app) + inddom                   */
//     /*             m = pw_approximation_working_mref(app)                            */
//     /*        indlow = pw_approximation_indlowref(app)[n]                            */
//     fmpq_t exp, tmp;
//     fmpq_init(exp);
//     fmpq_init(tmp);
//     /* index of dominant monomial: */
//     ulong inddom = pw_approximation_inddomref(app)[n];
//     ulong indlow = pw_approximation_indlowref(app)[n];
//     fmpq_ptr weidom = pw_approximation_exponentsref(app) + inddom;
//     fmpq_ptr      S = pw_approximation_bigradref(app)+n;
//     fmpq_ptr      s = pw_approximation_bigradref(app)+(n-1);
//     fmpq_mul_ui(exp, S, inddom);
//     fmpq_mul_ui(tmp, s, indlow);
//     fmpq_add   (exp, exp, weidom);
//     if (verbose) {
//         arb_set_si(error, 2);
//         arb_pow_fmpq(error, error, S, lprec);
//         printf("S: "); fmpq_print(S); printf(" 2^S: "); arb_printd(error, 10); printf("\n");
//         printf("inddom: %lu, indlow: %lu\n", inddom, indlow);
//         arb_set_si(error, 2);
//         arb_pow_fmpq(error, error, exp, lprec);
//         printf("upper bound for wmax(z) on %lu-th annulus: ", n); arb_printd(error, 10); printf("\n");
//     }
//     fmpq_sub   (exp, exp, tmp);
//     if (verbose) {
//         arb_set_si(error, 2);
//         arb_pow_fmpq(error, error, exp, lprec);
//         printf("upper bound for wmax(z)/z^indlow on %lu-th annulus: ", n); arb_printd(error, 10); printf("\n");
//     }
//     fmpq_sub_ui(exp, exp, pw_approximation_working_mref(app));
//     arb_set_si(error, 2);
//     arb_pow_fmpq(error, error, exp, lprec);
//     arb_mul_ui(error, error, pw_approximation_lenref(app), lprec);
//     
//     fmpq_clear(tmp);
//     fmpq_clear(exp);
// }
// 
// void _pw_approximation_compute_error_first_annulus( arb_t error, const pw_approximation_t app PW_VERBOSE_ARGU(verbose)  ) {
//     
//     slong lprec = pw_approximation_precref(app);
//     
//     /* compute the error: 2^(-m)*2^weihig*(2^S)^indhig*(degree - indhig)  */
//     /*                   <= (degree - indhig)*2^(weihig + S*indhig - m)   */
//     /* where: S      = pw_approximation_bigradref(cov)+0                  */
//     /*        indhig = pw_approximation_indhigref(app)[n]                 */
//     /*        weihig = pw_approximation_exponentsref(app) + indhig        */
//     /*             m = pw_approximation_working_mref(app)                 */
//     fmpq_t exp;
//     fmpq_init(exp);
//     /* index of dominant monomial: */
//     ulong indhig = pw_approximation_indhigref(app)[0];
//     fmpq_ptr weihig = pw_approximation_exponentsref(app) + indhig;
//     fmpq_ptr      S = pw_approximation_bigradref(app)+0;
//     fmpq_mul_ui(exp, S, indhig);
//     fmpq_add   (exp, exp, weihig);
//     if (verbose) {
//         arb_set_si(error, 2);
//         arb_pow_fmpq(error, error, exp, lprec);
//         printf("upper bound for wmax(z) on %lu-th annulus: ", (ulong)0); arb_printd(error, 10); printf("\n");
//     }
//     fmpq_sub_ui(exp, exp, pw_approximation_working_mref(app));
//     arb_set_si(error, 2);
//     arb_pow_fmpq(error, error, exp, lprec);
//     arb_mul_ui(error, error, pw_approximation_degreeref(app), lprec);
//     
//     fmpq_clear(exp);
// }
// 
// void _pw_approximation_compute_error_last_annulus( arb_t error, const pw_approximation_t app PW_VERBOSE_ARGU(verbose)  ) {
//     
//     slong lprec = pw_approximation_precref(app);
//     ulong N     = pw_approximation_Nref(app);
//     
//     /* compute the error: 2^(-m)*2^weilow*(2^-s)^indlow*(degree - indlow) */
//     /*                   <= (degree - indlow)*2^(weilow -s*indlow - m)    */
//     /* where: s      = pw_approximation_bigradref(cov)+(N-1)              */
//     /*        indlow = pw_approximation_indlowref(app)[N-1]               */
//     /*        weilow = pw_approximation_exponentsref(app) + indlow        */
//     /*             m = pw_approximation_working_mref(app)                 */
//     fmpq_t exp;
//     fmpq_init(exp);
//     /* index of dominant monomial: */
//     ulong indlow = pw_approximation_indlowref(app)[N-1];
//     fmpq_ptr weilow = pw_approximation_exponentsref(app) + indlow;
//     fmpq_ptr      s = pw_approximation_bigradref(app)+(N-2);
//     fmpq_mul_si(exp, s, -(slong)indlow);
//     fmpq_add   (exp, exp, weilow);
//     if (verbose) {
//         arb_set_si(error, 2);
//         arb_pow_fmpq(error, error, exp, lprec);
//         printf("upper bound for wmax(z) on %lu-th annulus: ", N-1); arb_printd(error, 10); printf("\n");
//     }
//     fmpq_sub_ui(exp, exp, pw_approximation_working_mref(app));
//     arb_set_si(error, 2);
//     arb_pow_fmpq(error, error, exp, lprec);
//     arb_mul_ui(error, error, pw_approximation_degreeref(app)-indlow, lprec);
//     
//     fmpq_clear(exp);
// }

// void _pw_get_inclusion_disc_first_annulus( arb_t radius, const pw_approximation_t app, const acb_t y, const acb_t x PW_VERBOSE_ARGU(verbose)  ) {
//     slong lprec = pw_approximation_precref(app);
// 
//     if (verbose) {
//         printf("x: "); acb_printd(x, 10); printf("\n");
//         printf("y: "); acb_printd(y, 10); printf("\n");
//     }
//     /* y = x                       */
//     /* f(y) = g(x)                 */
//     /* f'(y)= g'(x)                */
//     /* radius = d*|f(y)| / |f'(y)| */
//     /*        = d*|g(y)| / |g'(y)| */
//     acb_srcptr coeffs;
//     ulong lenapprox = pw_approximation_get_approx(&coeffs, app, 0, 0 );
//     
//     acb_t gval, gpval;
//     acb_init(gval);
//     acb_init(gpval);
//     arb_t error, errorder;
//     arb_init(error);
//     arb_init(errorder);
//     /* evaluate approximation polynomial and its derivative at x      */
//     _acb_poly_evaluate2(gval, gpval, coeffs, lenapprox, x, lprec);
//     if (verbose){
//         printf("gval: "); acb_printd(gval, 10); printf(" gpval: "); acb_printd(gpval, 10); printf("\n");
//     }
//     /* get evaluation error */
//     _pw_approximation_evaluation_error( error, errorder, app, y, lprec PW_VERBOSE_CALL(verbose)  );
//     if (verbose) {
//         printf("error: "); arb_printd(error, 10); printf(" errorder: "); arb_printd(errorder, 10); printf("\n");
//     }
//     arb_add_error( acb_realref(gval), error);
//     arb_add_error( acb_imagref(gval), error);
//     arb_add_error( acb_realref(gpval), errorder);
//     arb_add_error( acb_imagref(gpval), errorder);
//     if (verbose) {
//         printf("gval: "); acb_printd(gval, 10); printf(" gpval: "); acb_printd(gpval, 10); printf("\n");
//     }
//     acb_abs(error, gpval, lprec);
//     acb_abs(radius, gval, lprec);
//     arb_div(radius, radius, error, lprec);
//     arb_mul_ui( radius, radius, pw_approximation_lenref(app), lprec );
//     if (verbose) {
//         printf("radius: "); arb_printd(radius, 10); printf("\n"); printf("\n");
//     }
//     
//     acb_clear(gval);
//     acb_clear(gpval);
//     arb_clear(error);
//     arb_clear(errorder);
// }
// 
// void _pw_get_inclusion_disc_last_annulus( arb_t radius, const pw_approximation_t app, const acb_t y, const acb_t x PW_VERBOSE_ARGU(verbose)  ) {
//     slong lprec = pw_approximation_precref(app);
//     ulong N = pw_approximation_Nref(app);
//     if (verbose) {
//         printf("x: "); acb_printd(x, 10); printf("\n");
//         printf("y: "); acb_printd(y, 10); printf("\n");
//     }
//     /* y=1/x <=> x=1/y                                                     */
//     /* x' = -y^(-2)                                                        */
//     /* f(y) =y^d*g(1/y)                                                    */
//     /* f'(y)=y^d*(-y^(-2))*g'(1/y) + dy^(d-1)g(1/y)                        */
//     /*      = y^(d-2) * ( -g'(1/y) + d*y*g(1/y) )                          */
//     /* radius = d*|f(y)| / |f'(y)|                                         */
//     /*        = d*|y|^d*|g(1/y)| / ( |y|^(d-2) * |-g'(1/y) + d*y*g(1/y)| ) */
//     /*        = d*|y|^2*|g(1/y)| / |-g'(1/y) + d*y*g(1/y)|                 */
//     acb_srcptr coeffs;
//     ulong lenapprox = pw_approximation_get_approx(&coeffs, app, N-1, 0 );
//     
//     acb_t gval, gpval;
//     acb_init(gval);
//     acb_init(gpval);
//     arb_t error, errorder;
//     arb_init(error);
//     arb_init(errorder);
//     /* evaluate approximation polynomial and its derivative at x      */
//     _acb_poly_evaluate2(gval, gpval, coeffs, lenapprox, x, lprec);
//     if (verbose){
//         printf("gval: "); acb_printd(gval, 10); printf(" gpval: "); acb_printd(gpval, 10); printf("\n");
//     }
//     /* get evaluation error */
//     _pw_approximation_evaluation_error( error, errorder, app, y, lprec PW_VERBOSE_CALL(verbose)  );
//     if (verbose) {
//         printf("error: "); arb_printd(error, 10); printf(" errorder: "); arb_printd(errorder, 10); printf("\n");
//     }
//     arb_add_error( acb_realref(gval), error);
//     arb_add_error( acb_imagref(gval), error);
//     arb_add_error( acb_realref(gpval), errorder);
//     arb_add_error( acb_imagref(gpval), errorder);
//     if (verbose) {
//         printf("gval: "); acb_printd(gval, 10); printf(" gpval: "); acb_printd(gpval, 10); printf("\n");
//     }
//     acb_abs(radius, gval, lprec);
//     acb_abs(error, y, lprec);
//     arb_sqr(error, error, lprec);
//     arb_mul(radius, radius, error, lprec); /* radius = |y|^2*|g(1/y)| */
//     
//     acb_mul(gval, gval, y, lprec);
//     acb_mul_ui(gval, gval, pw_approximation_lenref(app), lprec);
//     acb_neg(gpval, gpval);
//     acb_add(gval, gval, gpval, lprec);
//     acb_abs(error, gval, lprec);           /* error = |-g'(1/y) + d*y*g(1/y)| */
//     
//     arb_div(radius, radius, error, lprec);
//     arb_mul_ui(radius, radius, pw_approximation_lenref(app), lprec);
//     
//     if (verbose) {
//         printf("radius: "); arb_printd(radius, 10); printf("\n"); printf("\n");
//     }
//     
//     acb_clear(gval);
//     acb_clear(gpval);
//     arb_clear(error);
//     arb_clear(errorder);
// }
// 
// 
// void _pw_get_inclusion_disc_non_shifted_annulus( arb_t radius, const pw_approximation_t app, const acb_t y, const acb_t x, ulong n PW_VERBOSE_ARGU(verbose)  ) {
//     slong lprec = pw_approximation_precref(app);
//     if (verbose) {
//         printf("x: "); acb_printd(x, 10); printf("\n");
//         printf("y: "); acb_printd(y, 10); printf("\n");
//     }
//     /* y = x                                                           */
//     /* f(y) = (y^km)*g(x)                                              */
//     /* f'(y)= (y^km)*g'(x) + km*y^(km-1)g(x)                           */
//     /* radius = d*|f(y)| / |f'(y)|                                     */
//     /*        = d*|y|^km * |g(y)| / |(y^km)*g'(x) + km*y^(km-1)g(x)|   */
//     /*        = d*|y|^km * |g(y)| / (|y|^(km-1) * |y*g'(x) + km*g(x)|) */
//     /*        = d*|y|*|g(x)| / |y*g'(x) + km*g(x)|                     */
//     acb_srcptr coeffs;
//     ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, 0 );
//     ulong km = pw_approximation_indlowref(app)[n];
//     
//     acb_t gval, gpval;
//     acb_init(gval);
//     acb_init(gpval);
//     arb_t error, errorder;
//     arb_init(error);
//     arb_init(errorder);
//     /* evaluate approximation polynomial and its derivative at x      */
//     _acb_poly_evaluate2(gval, gpval, coeffs, lenapprox, x, lprec);
//     if (verbose){
//         printf("gval: "); acb_printd(gval, 10); printf(" gpval: "); acb_printd(gpval, 10); printf("\n");
//     }
//     /* get evaluation error */
//     _pw_approximation_evaluation_error( error, errorder, app, y, lprec PW_VERBOSE_CALL(verbose)  );
//     if (verbose) {
//         printf("error: "); arb_printd(error, 10); printf(" errorder: "); arb_printd(errorder, 10); printf("\n");
//     }
//     arb_add_error( acb_realref(gval), error);
//     arb_add_error( acb_imagref(gval), error);
//     arb_add_error( acb_realref(gpval), errorder);
//     arb_add_error( acb_imagref(gpval), errorder);
//     if (verbose) {
//         printf("gval: "); acb_printd(gval, 10); printf(" gpval: "); acb_printd(gpval, 10); printf("\n");
//     }
//     acb_abs(radius, gval, lprec);
//     acb_abs(error, y, lprec);
//     arb_mul(radius, radius, error, lprec); /* radius = |y|*|g(x)| */
//     
//     acb_mul(gpval, gpval, y, lprec);
//     acb_mul_ui(gval, gval, km, lprec);
//     acb_add(gval, gval, gpval, lprec);
//     acb_abs(error, gval, lprec);           /* error = |y*g'(x) + km*g(x)| */
//     
//     arb_div(radius, radius, error, lprec);
//     arb_mul_ui( radius, radius, pw_approximation_lenref(app), lprec );
//     
//     if (verbose) {
//         printf("radius: "); arb_printd(radius, 10); printf("\n"); printf("\n");
//     }
//     
//     acb_clear(gval);
//     acb_clear(gpval);
//     arb_clear(error);
//     arb_clear(errorder);
// }
// 
// /* Preconditions:  x is an approximated root of an approximation (n',k') in app                  */
// /*                 y is x shifted back in the global reference                                   */
// /*                 0<=n<N, 0<=k<K[n]                                                             */
// /* Postconditions: computes a disc centered in y with radius radius;                             */
// /*                 if the following are satisfied:                                               */
// /*                 (1) if n==0, then |x|<=R where R= outer radius of the 0-th annulus            */
// /*                 (2) if n<N-1 then r<=|x|<=R where r,R = inner,outer radii of the n-th annulus */
// /*                 (3) if n==N-1 then 1/|x| >= R where R = outer radius of the (N-1)-th annulus  */
// /*                 (4) if (0<n<N-1) and K[n]>1, |x|<=1                                           */
// /*                 then the disk is guaranteed to contain a root of the initial polynomial f     */
// /*                 with formula d|f(y)|/|f'(y)|                                                  */
// void _pw_get_inclusion_disc_with_nkth_approx( arb_t radius, const pw_approximation_t app, const acb_t y, const acb_t x, ulong n, ulong k PW_VERBOSE_ARGU(verbose)  ) {
//     
//     ulong N = pw_approximation_Nref(app);
//     ulong K = pw_approximation_nbsectref(app)[n];
//     /* corner cases */
//     if (n==0) {
//         _pw_get_inclusion_disc_first_annulus( radius, app, y, x PW_VERBOSE_CALL(verbose)  );
//         return;
//     } else if (n==N-1) {
//         _pw_get_inclusion_disc_last_annulus( radius, app, y, x PW_VERBOSE_CALL(verbose)  );
//         return;
//     } else if (K==1) {
//         _pw_get_inclusion_disc_non_shifted_annulus( radius, app, y, x, n PW_VERBOSE_CALL(verbose)  );
//         return;
//     }
//     
//     
//     slong lprec = pw_approximation_precref(app);
//     
//     acb_t gval, gpval, temp;
//     acb_init(gval);
//     acb_init(gpval);
//     acb_init(temp);
//     
//     arb_t absy, error, errorder;
//     arb_init(absy);
//     arb_init(error);
//     arb_init(errorder);
//     
//     if (verbose) {
//         printf("x: "); acb_printd(x, 10); printf("\n");
//         printf("y: "); acb_printd(y, 10); printf("\n");
//     }
//     
//     acb_abs( absy, y, lprec );
//     ulong km = pw_approximation_indlowref(app)[n];
//     acb_srcptr coeffs;
//     ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, k );
//     /*                  y=(gamma + r*x)e^(2Pik/K) where r = scale*rho           */
//     /*                  x=(1/r)*(x*e^(-2Pik/K) - gamma )                        */
//     /*                  y'= r*e^(2Pik/K)                                        */
//     /*                  x'=(1/r)*e^(-2Pik/K)                                    */
//     /*                  f(y) = y^km*g(x)                                        */
//     /*                  f'(y) = y^km*g'(x)*(1/r)*e^(-2Pik/K) + km*y^(km-1)*g(x) */
//     /* radius = d*|f(y)| / |f'(y)|                                              */
//     /*        = d*|y|^km*|g(x)| / |y^km*g'(x)*(1/r)*e^(-2Pik/K) + km*y^(km-1)*g(x)|  */
//     /*        = d*|y|^km*|g(x)| / |y|^(km-1) * |y*g'(x)*(1/r)*e^(-2Pik/K) + km*g(x)| */
//     /*        =    d*|y|*|g(x)| / |y*g'(x)*(1/r)*e^(-2Pik/K) + km*g(x)|  */
//     /* set temp = r*e^(2Pik/K) */
//     acb_zero(temp);
//     arb_set_fmpq( acb_realref(temp), pw_approximation_scaleref(app), lprec );
//     arb_mul( acb_realref(temp), acb_realref(temp), pw_approximation_app_rhosref(app)+n, lprec );
//     _pw_covering_get_root_of_unit( gval, pw_approximation_coveringref(app), k, K );
//     acb_mul( temp, temp, gval, lprec );
//     /* evaluate approximation polynomial and its derivative at x      */
//     _acb_poly_evaluate2(gval, gpval, coeffs, lenapprox, x, lprec);
//     if (verbose){
//         printf("gval: "); acb_printd(gval, 10); printf(" gpval: "); acb_printd(gpval, 10); printf("\n");
//     }
//     /* get evaluation error */
//     _pw_approximation_evaluation_error( error, errorder, app, y, lprec PW_VERBOSE_CALL(verbose)  );
//     if (verbose) {
//         printf("error: "); arb_printd(error, 10); printf(" errorder: "); arb_printd(errorder, 10); printf("\n");
//     }
//     arb_add_error( acb_realref(gval), error);
//     arb_add_error( acb_imagref(gval), error);
//     arb_add_error( acb_realref(gpval), errorder);
//     arb_add_error( acb_imagref(gpval), errorder);
//     if (verbose) {
//         _pw_approximation_compute_error_annulus( error, app, n, 1 );
//         printf("error2: "); arb_printd(error, 10); printf("\n");
//         printf("gval: "); acb_printd(gval, 10); printf(" gpval: "); acb_printd(gpval, 10); printf("\n");
//     }
//     acb_abs(radius, gval, lprec);
//     arb_mul(radius, radius, absy, lprec);          /* radius = |y|*|g(x)| */
//     acb_mul_ui( gval, gval, km, lprec );           /* gval = km*g(x)       */
//     acb_mul( gpval, gpval, y, lprec );             /* gpval = y*g'(x)      */
//     acb_div( gpval, gpval, temp, lprec );          /* gpval = y*g'(x)/(r*e^(2Pik/K)) */
//     acb_add( gpval, gpval, gval, lprec );          /* gpval = y*g'(x)/(r*e^(2Pik/K)) +  km*g(x) */
//     acb_abs( acb_realref(temp), gpval, lprec ); 
//     arb_div( radius, radius, acb_realref(temp), lprec ); /* radius = |y|*|g(x)| / |y*g'(x)*(1/r)*e^(-2Pik/K) + km*g(x)|  */
//     arb_mul_ui( radius, radius, pw_approximation_lenref(app), lprec );
//     if (verbose) {
//         printf("radius: "); arb_printd(radius, 10); printf("\n"); printf("\n");
//     }
//     
//     
//     acb_clear(gval);
//     acb_clear(gpval);
//     acb_clear(temp);
//     arb_clear(absy);
//     arb_clear(error);
//     arb_clear(errorder);
// }

// /* Preconditions:  0<= km <= kp                                                            */
// /*                 rho, gamma and scale are positive real numbers                          */
// /*                 stop_crite is a initialized table of len kp-km+1,                       */
// /* Postconditions: for 0 <= i < kp-km+1, sets stop_crite+i to (2^-m)*(1-scale*rho/gamma)^i */
// void _pw_approximation_stopping_criterion( arb_ptr stop_crite, 
//                                            ulong m, ulong km, ulong kp, const arb_t rho, const arb_t gamma, const fmpq_t scale, slong prec ) {
//     
//     slong len = kp-km+1;
//     arb_t temp;
//     arb_init(temp);
//     /* sets temp to 1-scale*rho/gamma */
//     arb_set_fmpq(temp, scale, prec);
//     arb_mul( temp, temp, rho, prec);
//     arb_div( temp, temp, gamma, prec);
//     arb_sub_si( temp, temp, 1, prec );
//     arb_neg( temp, temp );
//     /* compute the powers of 1-scale*rho/gamma */
//     _arb_vec_set_powers(stop_crite, temp, len, prec);
//     /* multiply each by 2^-m */
//     for (slong i=0; i<len; i++)
//         arb_mul_2exp_si(stop_crite+i, stop_crite+i, -(slong)m );
//     arb_clear(temp);
// }

/* Preconditions:  0<= km <= kp                                                               */
/*                 rho, gamma and scale are positive real numbers                             */
/*                 coeffs is a table of at least kp-km+1 acb                                  */
/*                 expansion is an initialized table of at least kp-km+1 acb                  */
/*                 expansion_without_co is an initialized table of at least kp-km+1 arb       */
/*                 stop_crite is an initialized table of at least kp-km+1 arb                 */
/*                 if line>0, expansion and expansion_without_co have been obtained as:       */
/*                           _pw_approximation_Taylor_expansion called with the same arguments*/
/* Postconditions: for 0 <= i < kp-km+1,                                                      */
/*                     sets expansion_without_co+i to 2*binom(i, line)*(scale*rho/gamma)^line */
/*                     sets expansion+i to                                                    */
/*                           (binom(i, line)*(scale*rho/gamma)^line)*(coeffs+km+i)*gamma^i    */
/*                 where binom(i, line) = choose line among i                                 */
/*                 returns 1 if for all 0 <= i < kp-km+1,                                     */
/*                            (scale*rho/gamma)*(i-line)/(line+1) <= 1/2                      */
/*                        and expansion_without_co+i <= stop_crite+i                          */
/*                        i.e. 2*binom(i, line)*(scale*rho/gamma)^line <= (2^-m)*(1-scale*rho/gamma)^i */
/* <=> (binom(i, line)*(scale*rho/gamma)^line)*(coeffs+km+i)*gamma^i <= (2^(-m-1))*(1-scale*rho/gamma)^i*(coeffs+km+i)*gamma^i */
/* <=> (binom(i, line)*(scale*rho/gamma)^line)*(coeffs+km+i)*gamma^i <= (2^(-m-1))*(gamma-scale*rho)^i*(coeffs+km+i) */
/*                 otherwise returns 0                                                        */
// int  _pw_approximation_Taylor_expansion( acb_ptr expansion, arb_ptr expansion_without_co, arb_ptr stop_crite, 
//                                          ulong line, acb_srcptr coeffs,
//                                          ulong km, ulong kp, const arb_t rho, const arb_t gamma, const fmpq_t scale, slong prec ) {
// 
// #ifdef PW_PROFILE
//     clock_t start = clock();
// #endif
// 
//     int res = 1;    
// //     printf("km: %lu, kp: %lu, kp-km+1: %lu\n", km, kp, kp-km+1 );
//     if (line==0) {
//         arb_t gamma_pows;
//         arb_init(gamma_pows);
//         arb_one(gamma_pows);
//         for (ulong i=0; i < kp-km+1; i++){
// //             printf("i: %lu\n", i );
//             acb_mul_arb( expansion+i, coeffs + (km+i), gamma_pows, prec);
//             arb_mul( gamma_pows, gamma_pows, gamma, prec );
//             
//             arb_set_si( expansion_without_co + i, 2 );
//         }
//         arb_clear(gamma_pows);
// //         arb_set_si(max, 2);
//         res=0;
//     } else {
//         arb_t scaleTrhoOgamma, fact, ratio, half;
//         arb_init(scaleTrhoOgamma);
//         arb_init(fact);
//         arb_init(ratio);
//         arb_init(half);
//         arb_set_d(half, .5);
//         arb_set_fmpq(scaleTrhoOgamma, scale, prec);
//         arb_mul( scaleTrhoOgamma, scaleTrhoOgamma, rho, prec);
//         arb_div( scaleTrhoOgamma, scaleTrhoOgamma, gamma, prec);
//         arb_div_ui( ratio, scaleTrhoOgamma, line+1, prec );
//         arb_div_ui( scaleTrhoOgamma, scaleTrhoOgamma, line, prec );
//         for (ulong i=line-1; i < kp-km+1; i++){
//             arb_mul_si( fact, scaleTrhoOgamma, i-line+1, prec );
//             acb_mul_arb( expansion+i, expansion+i, fact, prec );
//             arb_mul( expansion_without_co + i, expansion_without_co + i, fact, prec );
// 
//             if ( (i>=line)&&(res) ) {
//                 /* sets fact to (scale*rho/gamma)*(i-line)/(line+1) */
//                 arb_mul_si( fact, ratio, (slong)i -line, prec ); 
//                 res = res && arb_le( fact, half );
//                 res = res && arb_le( expansion_without_co + i, stop_crite + i );
//             }
//         }
//         arb_clear(half);
//         arb_clear(ratio);
//         arb_clear(fact);
//         arb_clear(scaleTrhoOgamma);
//     }
// #ifdef PW_PROFILE
//     clicks_in_expansion += (clock() - start);
// #endif
//     return res;
// }
