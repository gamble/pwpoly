/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_covering.h"

#ifdef PW_PROFILE
double clicks_in_locate;
#endif

/* Preconditions:  0<=i<=j<=k */
/*                 yi, yj, yk are finite rationals */
/* Specifications: returns 1 if and only if (j,yj) lies above or on line ( (i,yi),   (k,yk) ) */
int  _pw_covering_fmpq_lies_above( const ulong i, const fmpq_t yi,
                                   const ulong j, const fmpq_t yj,
                                   const ulong k, const fmpq_t yk ) {
    /* decide if pj:=(j,yj) lies above the line passing trough pi:=(i, yi) and pk:=(k, yk) */
    /* <=> decide if the slope of (pi,pj) >= the slope of (pi,pk)                          */
    /* <=> decide if (yj-yi)/(j-i) >= (yk-yi)/(k-i)                                        */
    /* <=> decide if (yj-yi)*(k-i) >= (yk-yi)*(j-i)                                        */
    
//     if (fmpq_is_zero(yi))
//         return 0;
//     
//     if (fmpq_is_zero(yj))
//         return 1;
    
    ulong diff;
    fmpq_t left, right;
    fmpq_init(left);
    fmpq_init(right);
    fmpq_sub(left, yj, yi);
    diff = k-i;
    fmpq_mul_ui(left, left, diff);
    fmpq_sub(right, yk, yi);
    diff = j-i;
    fmpq_mul_ui(right, right, diff);
    
    int res = fmpq_cmp( left, right );
    fmpq_clear(left);
    fmpq_clear(right);
    
    return res>=0;
}

/* Preconditions:  i<>j            */
/*                 yi, yj are finite integers*/
/* Postconditions: slope is the slope of the line ( (i,yi), (j,yj) ) */
void _pw_covering_fmpq_lineslope( fmpq_t slope, const ulong i, const fmpq_t yi, const ulong j, const fmpq_t yj){
    fmpz_t den;
    fmpq_sub( slope, yj, yi );
    fmpz_init(den);
    fmpz_set_ui(den, j);
    fmpz_sub_ui(den, den, i);
    fmpq_div_fmpz( slope, slope, den );
    fmpz_clear(den);
    fmpq_canonicalise(slope);
}

/* Preconditions:  ys is a table of len fmpq                                               */
/*                 ys[0] and ys[len-1] are non zero                                        */
/*                 CH is an allocated table of len slong                                   */
/* Postconditions: computes the LOWER part of the convex hull of the set of (2D) points:   */
/*                     { (0, ys+0), (1, ys+1), ..., (len-1, ys + (len-1)) }                */
/*                 let { (h1, ys+h1), ..., (hl, ys+hl) }       be this convex hull         */
/*                 computes: CH:=[h1, h2, ..., hl ]                                        */
/*                 returns : l                                                             */
/* Implements Graham's algorithm                                                           */
ulong _pw_covering_fmpq_convex_hull( ulong * CH, const fmpq_ptr ys, const ulong len ) {
    if (len==0)
        return 0;
    if (len==1) {
        CH[0] = 0;
        return 1;
    }
    ulong res = 2;
    ulong i;
    /* push first point */
    CH[0]=0;
    i = 1;
    while ( (i<len-1) && fmpq_is_zero(ys+i) )
        i++;
    CH[1]=i;
    i++;
    /* loop on other points */
    for (; i<len; i++){
        if (fmpq_is_zero(ys+i))
            continue;
        
        int liesAbove = 1;
        while ((res >= 2) && (liesAbove==1) ) {
            liesAbove = _pw_covering_fmpq_lies_above( CH[res-2], ys + CH[res-2],
                                                      CH[res-1], ys + CH[res-1],
                                                      i        , ys + i );
            if (liesAbove==1)
                res--;
        }
        CH[res] = i;
//         printf("CH so far: ");
//         for (slong m=0; m<=res; m++) printf("%ld, ", CH[m]);
//         printf("\n");
        res++;
    }
    return res;
}
// ulong _pw_covering_fmpq_convex_hull( ulong * CH, const fmpq_ptr ys, const ulong len ) {
//     if (len==0)
//         return 0;
//     if (len==1) {
//         CH[0] = 0;
//         return 1;
//     }
//     ulong res = 2;
//     ulong i;
//     /* push two first points */
//     CH[0]=0;
//     CH[1]=1;
//     /* loop on other points */
//     for (i = 2; i<len; i++){
//         int liesAbove = 1;
//         while ((res >= 2) && (liesAbove==1) ) {
//             liesAbove = _pw_covering_fmpq_lies_above( CH[res-2], ys + CH[res-2],
//                                                       CH[res-1], ys + CH[res-1],
//                                                       i        , ys + i );
//             if (liesAbove==1)
//                 res--;
//         }
//         CH[res] = i;
// //         printf("CH so far: ");
// //         for (slong m=0; m<=res; m++) printf("%ld, ", CH[m]);
// //         printf("\n");
//         res++;
//     }
//     return res;
// }

/* Pre conditions: i<j<k, xi, xk are finite integers */
/*                 y is initialized                  */
/* Post conditions: compute y s.t. the points (j,y) lies                   */
/*                  on the segment [ (i, xi), (k, xk) ]                    */
/* i.e. (xj-xi)/(j-i) = (xk-xi)/(k-i)   */
/*      xj = xi + ((xk-xi)/(k-i))*(j-i) */
void _pw_covering_fmpq_snap_segment( fmpq_t y, ulong j, ulong i, const fmpq_t xi,
                                                        ulong k, const fmpq_t xk){
    
    fmpz_t diff;
    fmpz_init(diff);
    fmpq_sub(y, xk, xi);
    fmpz_set_ui(diff, k);
    fmpz_sub_ui(diff, diff, i);
    fmpq_div_fmpz(y, y, diff);
    fmpz_set_ui(diff, j);
    fmpz_sub_ui(diff, diff, i);
    fmpq_mul_fmpz(y, y, diff);
    fmpq_add(y, y, xi);
    fmpz_clear(diff);
}

/* Pre conditions:  zs, ys are tables of leny fmpq                                                            */
/*                  CH=[h1, h2, ..., hlenCH] and { (h1, ys+h1), ..., (hl, ys+hlenCH) } is the             */
/*                  lower part of the convex hull of { (0, ys+0), (1, ys+1), ..., (len-1, ys + (leny-1) } */
/* Post conditions: for all 0<= i < leny, if i is not in CH, let j st hj < i < hj+1                    */
/*                  computes zs[i] as ys[i] moved on the segment [ (hj, ys+hj)), (hj+1, ys +(hj+1)) ]   */
/*                  otherwise (i is in CH) lets zs[i]=ys[i]                                             */
void _pw_covering_fmpq_snap_convex_hull( fmpq_ptr zs, const fmpq_ptr ys, const ulong * CH, const ulong lenCH){
    
    ulong iCH, i,j,k;
    for ( iCH = 0; iCH < lenCH-1; iCH++ ){
        i = CH[iCH];
        fmpq_set( zs+CH[iCH], ys+CH[iCH] );
        k = CH[iCH+1];
        for (j = i+ 1; j < k; j++) {
            _pw_covering_fmpq_snap_segment( zs+j, j, i, ys+i, k, ys+k);
        }
    }
    fmpq_set( zs+CH[lenCH-1], ys+CH[lenCH-1] );
}

/* Pre conditions:  slopes, spoints, points are tables of lenpoints fmpq's                                              */
/*                  CH=[h1, h2, ..., hlenCH] and { (h1, points+h1), ..., (hl, points+hlenCH) } is the                   */
/*                    lower part of the convex hull of { (0, points+0), (1, points+1), ..., (len-1, points + (leny-1) } */
/* Post conditions: for all 0<= i < lenpoints, let high(i) := min_{k} { k s.t. 0<=k and                                 */
/*                         (points[CH[k+1]] - points[CH[k]])/(CH[k+1]-CH[k]) >= (spoints[i] - points[CH[k]])/(i-CH[k]) }*/
/*                         and High[i]:= (spoints[i] - points[CH[high(i)]])/(i-CH[high(i)])                             */
/*                  computes for all 1 <= i < lenpoints slopes[i]= High[i]                                              */
/*                  lets slopes[0] = -1/0                                                                               */ 
void _pw_covering_fmpq_highslopes( fmpq_ptr slopes, fmpq_ptr spoints, fmpq_ptr points, ulong lenpoints, ulong * CH ){
    ulong j=0;
    fmpq_t slope1, slope2;
    fmpq_init(slope1);
    fmpq_init(slope2);
    
    fmpz_set_si( fmpq_numref(slopes + 0), -1 );
    fmpz_set_si( fmpq_denref(slopes + 0),  0 );
    
    for (ulong i=1; i<lenpoints; i++) {
        _pw_covering_fmpq_lineslope( slope1, CH[j], points + CH[j], i, spoints + i);
        _pw_covering_fmpq_lineslope( slope2, CH[j], points + CH[j], CH[j+1], points + CH[j+1]);
        while ( fmpq_cmp( slope1, slope2 )>0 ) {
            j++;
            _pw_covering_fmpq_lineslope( slope1, CH[j], points + CH[j], i, spoints + i);
            _pw_covering_fmpq_lineslope( slope2, CH[j], points + CH[j], CH[j+1], points + CH[j+1]);
        }
//         printf("i: %lu, CH[j]: %lu\n", i, CH[j]);
        fmpq_set( slopes + i, slope1 );
    }
    
    fmpq_clear(slope1);
    fmpq_clear(slope2);
}

/* Pre conditions:  slopes, spoints, points are tables of lenpoints fmpq's                                              */
/*                  CH=[h1, h2, ..., hlenCH] and { (h1, points+h1), ..., (hl, points+hlenCH) } is the                   */
/*                    lower part of the convex hull of { (0, points+0), (1, points+1), ..., (len-1, points + (leny-1) } */
/* Post conditions: for all 0<= i < lenpoints, let low(i) := max_{k} { k s.t. k<=d and                                  */
/*                         (points[CH[k]] - points[CH[k-1]])/(CH[k]-CH[k-1]) >= (points[CH[k]] - spoints[i] )/(CH[k]-i) }*/
/*                         and Low[i]:= (points[low(i)] - spoints[i] )/(low(i)-i)                                        */
/*                  computes for all 0 <= i < lenpoints-1 slopes[i]= Low[i]                                              */
/*                  lets slopes[lenpoints-1] = 1/0                                                                       */ 
void _pw_covering_fmpq_lowslopes( fmpq_ptr slopes, fmpq_ptr spoints, fmpq_ptr points, ulong lenpoints, ulong * CH, ulong lenCH ){
    ulong j=lenCH-1;
    fmpq_t slope1, slope2;
    fmpq_init(slope1);
    fmpq_init(slope2);
    
    fmpz_set_si( fmpq_numref(slopes + (lenpoints-1)),  1 );
    fmpz_set_si( fmpq_denref(slopes + (lenpoints-1)),  0 );
    
    for (slong i=((slong) lenpoints-2); i>=0; i--) {
        _pw_covering_fmpq_lineslope( slope1, CH[j], points + CH[j], i, spoints + i);
        _pw_covering_fmpq_lineslope( slope2, CH[j], points + CH[j], CH[j-1], points + CH[j-1]);
        while ( fmpq_cmp( slope1, slope2 )<0 ) {
            j--;
            _pw_covering_fmpq_lineslope( slope1, CH[j], points + CH[j], i, spoints + i);
            _pw_covering_fmpq_lineslope( slope2, CH[j], points + CH[j], CH[j-1], points + CH[j-1]);
        }
//         printf("i: %lu, CH[j]: %lu\n", i, CH[j]);
        fmpq_set( slopes + i, slope1 );
    }
    
    fmpq_clear(slope1);
    fmpq_clear(slope2);
}


/* Pre conditions:  slopes, spoints are tables of lenpoints fmpq's                                                       */
/* Post conditions: for all 0<= i < lenpoints-1, sets slopes[i] to the slope of the line                                 */
/*                         ( (i,spoints[i]), (i+1, spoints[i+1]) )                                                       */
/*                  let slopes[lenpoints-1] = 1/0 = +inf                                                                 */
/*                  if the points (i,spoints[i]) are in convex position, slopes is increasing                            */
void _pw_covering_fmpq_middleslopes( fmpq_ptr slopes, const fmpq_ptr spoints, const ulong lenpoints){
    for (ulong i=0; i<lenpoints-1; i++)
        _pw_covering_fmpq_lineslope( slopes+i, i, spoints + i, i+1, spoints + (i+1) );
    fmpq_one( slopes+(lenpoints-1) );
    fmpz_zero( fmpq_denref( slopes+(lenpoints-1) ) );
}

/* Pre conditions:  values is table of len (non-necessarily strictly) increasing fmpq's  */
/* Post conditions: returns the index l s.t. if l=0,   then value <= value[0]            */
/*                                           if l=len, then value[len-1] < value         */                                             
/*                                           if 0 < l < len, then                        */
/*                                               values[l-1] < value <= values[l]        */
ulong _pw_covering_fmpq_searchsorted( const fmpq_t value, const fmpq_ptr values, const ulong len ){
    ulong l = 0;
    ulong r = len-1;
    if (fmpq_cmp(value, values + l)<=0)
        r=l;
    else if (fmpq_cmp(value, values + r )>0)
        l=r+1;
    /* here and below, l<r => values[l] < value <= values[r] */
    while (l<r) {
        ulong m = (l+r)/2; /* l<=m<r */
        if (fmpq_cmp( value, values + m ) <= 0) /* values[l] < value <= values[m] */
            r=m;
        else                                    /* values[m] < value <= values[r] */       
            l=m;
        if ( (r-l)==1 ) /* values[r-1] < value <= values[r] */
            l=r;
    }
    return l;
}

slong _pw_covering_fmpq_searchsorted_min_max( ulong min, ulong max, const fmpq_t value, const fmpq_ptr values ){
    ulong l = min;
    ulong r = max-1;
    if (fmpq_cmp(value, values + l)<=0)
        r=l;
    else if (fmpq_cmp(value, values + r )>0)
        l=r+1;
    /* here and below, l<r => values[l] < value <= values[r] */
    while (l<r) {
        ulong m = (l+r)/2; /* l<=m<r */
        if (fmpq_cmp( value, values + m ) <= 0) /* values[l] < value <= values[m] */
            r=m;
        else                                    /* values[m] < value <= values[r] */       
            l=m;
        if ( (r-l)==1 ) /* values[r-1] < value <= values[r] */
            l=r;
    }
    return l;
}
/* Pre conditions:  values is table of len exact (non-necessarily strictly) increasing arb's  */
/*                  value=[lv,uv] is an arb                                                   */ 
/*                  0<= min < max <= len                                                      */
/* Post conditions: sets min to the highest index s.t. values[min-1] < lv <= values[min]      */
/*                       max to the highest index s.t. values[max-1] < uv <= values[max]      */
/*                       and min <= max                                                       */
/*                    if min = 0     then lv <= values[0]                                     */
/*                    if min = len   then values[len-1] < lv                                  */
/*                    if max = 0     then uv <= values[0]                                     */
/*                    if max = len   then values[len-1] < uv                                  */
void _pw_covering_arb_searchsorted( ulong *min, ulong *max, const arb_t value, const fmpq_ptr values, const ulong len, slong prec ){
    
    arf_t l, u;
    arf_init(l);
    arf_init(u);
    arb_get_interval_arf(l, u, value, prec);
    
    fmpq_t lrat, urat;
    fmpq_init(lrat);
    fmpq_init(urat);
    arf_get_fmpq(lrat, l);
    arf_get_fmpq(urat, u);
    
    /* first find min */
    *min = _pw_covering_fmpq_searchsorted_min_max( 0, len, lrat, values );
    /* then  find max */
    *max = _pw_covering_fmpq_searchsorted_min_max( 0, len, urat, values );
    
    fmpq_clear(lrat);
    fmpq_clear(urat);
    
    arf_clear(l);
    arf_clear(u);
}

void pw_covering_init( pw_covering_t cov, const ulong m, const ulong len, const ulong c, const fmpq_t scale, const fmpq_t ratio ){
    
    cov->_size_allocated = PW_ALLOC_SIZE;
    
    pw_covering_degreeref(cov) = len-1;
    
    pw_covering_mref(cov) = m;
    pw_covering_cref(cov) = c;
    fmpq_init(pw_covering_scaleref(cov));
    fmpq_init(pw_covering_ratioref(cov));
    fmpq_init(pw_covering_factoref(cov));
    fmpq_set(pw_covering_scaleref(cov), scale);
    fmpq_set(pw_covering_ratioref(cov), ratio);
    fmpq_div(pw_covering_factoref(cov), ratio, scale);
    
    pw_covering_Nref(cov) = 0;
    pw_covering_Ksumref(cov) = 0;
    
    
    pw_covering_indlowref(cov) = (ulong *) pwpoly_malloc ( (cov->_size_allocated)*sizeof(ulong) );
    pw_covering_indhigref(cov) = (ulong *) pwpoly_malloc ( (cov->_size_allocated)*sizeof(ulong) );
    pw_covering_nbsectref(cov) = (ulong *) pwpoly_malloc ( (cov->_size_allocated)*sizeof(ulong) );
    pw_covering_log2_nbsectref(cov) = (uint *) pwpoly_malloc ( (cov->_size_allocated)*sizeof(uint) );
    pw_covering_max_nbsectref(cov) = 0;
    pw_covering_bigradref(cov) = _fmpq_vec_init( cov->_size_allocated );
    pw_covering_inddomref(cov) = (ulong *) pwpoly_malloc ( (cov->_size_allocated)*sizeof(ulong) );
    pw_covering_sl_at_edref(cov) = _fmpq_vec_init( len );
    
    pw_covering_app_precref(cov) = -1;
    
}

// void pw_covering_init_set( pw_covering_t dest, pw_covering_t src ) {
//     
//     dest->_size_allocated = src->_size_allocated;
//     pw_covering_degreeref(dest) = pw_covering_degreeref(src);
//     pw_covering_mref(dest) = pw_covering_mref(src);
//     pw_covering_cref(dest) = pw_covering_cref(src);
//     fmpq_init(pw_covering_scaleref(dest));
//     fmpq_init(pw_covering_ratioref(dest));
//     fmpq_init(pw_covering_factoref(dest));
//     fmpq_set(pw_covering_scaleref(dest), pw_covering_scaleref(src));
//     fmpq_set(pw_covering_ratioref(dest), pw_covering_ratioref(src));
//     fmpq_set(pw_covering_factoref(dest), pw_covering_factoref(src));
//     
//     pw_covering_Nref(dest) = pw_covering_Nref(src);
//     pw_covering_Ksumref(dest) = pw_covering_Ksumref(src);
//     
//     
//     pw_covering_indlowref(dest) = (ulong *) pwpoly_malloc ( (dest->_size_allocated)*sizeof(ulong) );
//     pw_covering_indhigref(dest) = (ulong *) pwpoly_malloc ( (dest->_size_allocated)*sizeof(ulong) );
//     pw_covering_nbsectref(dest) = (ulong *) pwpoly_malloc ( (dest->_size_allocated)*sizeof(ulong) );
//     pw_covering_log2_nbsectref(dest) = (uint *) pwpoly_malloc ( (dest->_size_allocated)*sizeof(uint) );
//     pw_covering_max_nbsectref(dest) = 0;
//     pw_covering_bigradref(dest) = _fmpq_vec_init( dest->_size_allocated );
//     pw_covering_inddomref(dest) = (ulong *) pwpoly_malloc ( (dest->_size_allocated)*sizeof(ulong) );
//     pw_covering_sl_at_edref(dest) = _fmpq_vec_init( len );
//     
//     pw_covering_app_precref(dest) = -1;
// }

void pw_covering_clear( pw_covering_t cov ){
    fmpq_clear(pw_covering_scaleref(cov));
    fmpq_clear(pw_covering_ratioref(cov));
    fmpq_clear(pw_covering_factoref(cov));
    
    pwpoly_free(pw_covering_indlowref(cov));
    pwpoly_free(pw_covering_indhigref(cov));
    pwpoly_free(pw_covering_log2_nbsectref(cov));
    _fmpq_vec_clear(pw_covering_bigradref(cov), cov->_size_allocated);
    pwpoly_free(pw_covering_inddomref(cov));
    _fmpq_vec_clear(pw_covering_sl_at_edref(cov), pw_covering_degreeref(cov)+1 );
    
    if (pw_covering_app_precref(cov)>=0) {
        _arb_vec_clear(pw_covering_app_bigradref(cov), cov->_size_allocated);
        _arb_vec_clear(pw_covering_app_gammasref(cov), cov->_size_allocated);
        _arb_vec_clear(pw_covering_app_rhosref(cov), cov->_size_allocated);
        _acb_vec_clear(pw_covering_app_unit_rref(cov), pw_covering_max_nbsectref(cov));
    }
    pwpoly_free(pw_covering_nbsectref(cov));
}

#ifndef PW_SILENT
void _pw_covering_fprint( FILE * f, const pw_covering_t cov ) {
    
    arb_t appsl;
    arb_init(appsl);
    
    fprintf(f, "pw_covering: degree: %lu, m: %lu, c:%lu, scale: ", pw_covering_degreeref(cov), pw_covering_mref(cov), pw_covering_cref(cov) );
    fmpq_fprint(f, pw_covering_scaleref(cov));
    fprintf(f, " ratio: ");
    fmpq_fprint(f, pw_covering_ratioref(cov));
    fprintf(f, "\npw_covering: %lu annulii: \n[ ", pw_covering_Nref(cov));
    
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        arb_set_si(appsl, 2);
        arb_pow_fmpq(appsl, appsl, pw_covering_bigradref(cov)+i, PWPOLY_DEFAULT_PREC);
        fmpq_fprint( f, pw_covering_bigradref(cov)+i );
        fprintf(f, " ("); arf_fprintd( f, arb_midref( appsl ), 5 ); fprintf(f, ") ");
    }
    fprintf(f, " ]\npw_covering: indlows: \n[");
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        fprintf(f, "%lu ", pw_covering_indlowref(cov)[i]);
    }
    fprintf(f, " ]\npw_covering: indhighs: \n[");
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        fprintf(f, "%lu ", pw_covering_indhigref(cov)[i]);
    }
    fprintf(f, " ]\npw_covering: inddom: \n[");
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        fprintf(f, "%lu ", pw_covering_inddomref(cov)[i]);
    }
    fprintf(f, " ]\npw_covering: nbsectors: \n[");
    
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        fprintf(f, "%lu ", pw_covering_nbsectref(cov)[i]);
    }
    fprintf(f, " ]\n");
    
    arb_clear(appsl);
}

void _pw_covering_fprint_short( FILE * f, const pw_covering_t cov ) {
    
    arb_t appsl;
    arb_init(appsl);
    
    fprintf(f, "pw_covering: degree: %lu, m: %lu, c:%lu, scale: ", pw_covering_degreeref(cov), pw_covering_mref(cov), pw_covering_cref(cov) );
    fmpq_fprint(f, pw_covering_scaleref(cov));
    fprintf(f, " ratio: ");
    fmpq_fprint(f, pw_covering_ratioref(cov));
    fprintf(f, "\npw_covering: %lu annulii: \n[ ", pw_covering_Nref(cov));
    
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        arb_set_si(appsl, 2);
        arb_pow_fmpq(appsl, appsl, pw_covering_bigradref(cov)+i, PWPOLY_DEFAULT_PREC);
        arf_fprintd( f, arb_midref( appsl ), 5 ); fprintf(f, " ");
    }
    
    fprintf(f, " ]\npw_covering: indlows: \n[");
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        fprintf(f, "%lu ", pw_covering_indlowref(cov)[i]);
    }
    fprintf(f, " ]\npw_covering: indhighs: \n[");
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        fprintf(f, "%lu ", pw_covering_indhigref(cov)[i]);
    }
    fprintf(f, " ]\npw_covering: inddom: \n[");
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        fprintf(f, "%lu ", pw_covering_inddomref(cov)[i]);
    }
    fprintf(f, " ]\npw_covering: nbsectors: \n[");
    
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        fprintf(f, "%lu ", pw_covering_nbsectref(cov)[i]);
    }
    fprintf(f, " ]\n");
    
    arb_clear(appsl);
}
#endif

/* not used */
// int _pw_covering_first_annulus( pw_covering_t cov, const fmpq_ptr lowslopes, const fmpq_ptr highslopes ){
//     
//     int res = 1;
//     ulong cm =   pw_covering_cref(cov)*pw_covering_mref(cov);
//     ulong dmcm = pw_covering_degreeref(cov) - cm;
//     /* assume cm<=degree */
//     pw_covering_Nref(cov) = 1;
//     fmpq_set( pw_covering_bigradref(cov)+0, highslopes+(cm));
//     pw_covering_indlowref(cov)[0] = 0;
//     pw_covering_indhigref(cov)[0] = cm;
//     /* it is not the one before the last if */
//     res = fmpq_cmp( pw_covering_bigradref(cov)+0, lowslopes+dmcm)<0;
//     
//     return res;
// }

void _pw_covering_extend_tables( pw_covering_t cov ){
    ulong nsize = 2*(cov->_size_allocated);
    pw_covering_indlowref(cov) = (ulong *) pwpoly_realloc( pw_covering_indlowref(cov), nsize*sizeof(ulong) );
    pw_covering_indhigref(cov) = (ulong *) pwpoly_realloc( pw_covering_indhigref(cov), nsize*sizeof(ulong) );
    pw_covering_nbsectref(cov) = (ulong *) pwpoly_realloc( pw_covering_nbsectref(cov), nsize*sizeof(ulong) );
    pw_covering_log2_nbsectref(cov) = (uint *) pwpoly_realloc( pw_covering_log2_nbsectref(cov), nsize*sizeof(uint) );
    fmpq_ptr bigrads = pw_covering_bigradref(cov);
    pw_covering_inddomref(cov) = (ulong *) pwpoly_realloc( pw_covering_inddomref(cov), nsize*sizeof(ulong) );
    
    pw_covering_bigradref(cov) = _fmpq_vec_init( nsize );
    for (ulong i = 0; i<cov->_size_allocated; i++) {
        fmpq_set( pw_covering_bigradref(cov)+i, bigrads+i );
    }
    
    _fmpq_vec_clear(bigrads, cov->_size_allocated);
    cov->_size_allocated=nsize;
}

/* not used */
// int _pw_covering_next_annulus( pw_covering_t cov, const fmpq_ptr lowslopes, const fmpq_ptr highslopes ){
//     
//     /* reallocate tables if necessary */
//     if ( pw_covering_Nref(cov) >= (cov->_size_allocated) ) {
//         _pw_covering_extend_tables( cov );
//     }
//     
//     fmpz_t k;
//     fmpq_t ratiok;
//     fmpz_init(k);
//     fmpq_init(ratiok);
//     
//     /* compute next annulus */
//     pw_covering_Nref(cov) += 1;
//     ulong   deg = pw_covering_degreeref(cov);
//     ulong     N = pw_covering_Nref(cov);
//     ulong     m = pw_covering_mref(cov);
//     ulong   km  = pw_covering_indlowref(cov)[N-2];
//     ulong   kp  = pw_covering_indhigref(cov)[N-2];
//     ulong   kms = km;
//     ulong   kps = kp;
//     fmpq_ptr s  = pw_covering_bigradref(cov) + N-2;
//     fmpq_ptr ss = pw_covering_bigradref(cov) + N-1;
//     
//     /* find kms: it is the first index i s.t. s < lowslopes[i] */
//     /* since lowslopes[deg]=+inf, kms < deg                    */
//     while( (kms<deg) && ( fmpq_cmp( s, lowslopes + kms) >=0) )
//         kms++;
//     /* set the number of monomials in the support k = *kms - *kps + 1*/
//     fmpz_set_si(k, kps + 1);
//     fmpz_sub_si(k, k, kms);
//     /* set ratiok = (ratio/scale)*(m/k) */
//     fmpq_mul_ui( ratiok, pw_covering_factoref(cov), m );
//     fmpq_div_fmpz(ratiok, ratiok, k);
//     /* increase kps until kps = deg or highslopes[kps] >= s + (ratio/scale)*(m/k) */
//     fmpq_sub(ss, highslopes+kps, s);
//     while( ( kps < deg ) && (fmpq_cmp(ss, ratiok)<0) ){
//         kps++;
//         /* set the number of monomials in the support k = *kms - *kps + 1*/
//         fmpz_set_si(k, kps + 1);
//         fmpz_sub_si(k, k, kms);
//         /* set ratiok = (ratio/scale)*(m/k) */
//         fmpq_mul_ui( ratiok, pw_covering_factoref(cov), m );
//         fmpq_div_fmpz(ratiok, ratiok, k);
//         
//         fmpq_sub(ss, highslopes+kps, s);
//     }
//     /* sets ss to s + (ratio/scale)*(m/k) */
//     fmpq_add(ss, s, ratiok);
//     /* it is not the one before the last if */
//     ulong cm =   pw_covering_cref(cov)*pw_covering_mref(cov);
//     ulong dmcm = deg - cm;
//     int res = fmpq_cmp( ss, lowslopes+dmcm)<0;
//     
//     pw_covering_indlowref(cov)[N-1]=kms;
//     pw_covering_indhigref(cov)[N-1]=kps;
//     
//     fmpz_clear(k);
//     fmpq_clear(ratiok);
//     
//     return res;
// }

/* not used */
// void _pw_covering_last_annulus( pw_covering_t cov, const fmpq_ptr lowslopes, const fmpq_ptr highslopes ) {
//     
//     /* reallocate tables if necessary */
//     if ( pw_covering_Nref(cov) >= (cov->_size_allocated) ) {
//         _pw_covering_extend_tables( cov );
//     }
//     
//     ulong cm =   pw_covering_cref(cov)*pw_covering_mref(cov);
//     ulong dmcm = pw_covering_degreeref(cov) - cm;
//     /* set last annulus */
//     pw_covering_Nref(cov) += 1;
//     ulong N = pw_covering_Nref(cov);
//     pw_covering_indlowref(cov)[N-1] = dmcm;
//     pw_covering_indhigref(cov)[N-1] = pw_covering_degreeref(cov);
// //     fmpq_set(pw_covering_bigradref(cov) + N-1, lowslopes+dmcm);
//     fmpq_one( pw_covering_bigradref(cov) + N-1 );
//     fmpz_zero( fmpq_denref( pw_covering_bigradref(cov) + N-1 ) );
// }

/* assume i < pw_covering_Nref(cov) */
void _pw_nb_sectors_in_ith_annulus( fmpz_t nbsectors, pw_covering_t cov, ulong i ){
    
    ulong m    = pw_covering_mref(cov);
    ulong cm   = pw_covering_cref(cov)*m;
    
    if ( pw_covering_indhigref(cov)[i] - pw_covering_indlowref(cov)[i] <=cm ) {
        fmpz_one(nbsectors);
        return; 
    }
    
    slong prec = PWPOLY_DEFAULT_PREC;
    arb_t R,r, gamma, rho;
    arb_init(R);
    arb_init(r);
    arb_init(gamma);
    arb_init(rho);
    arb_set_si(R, 2);
    arb_pow_fmpq(R, R, pw_covering_bigradref(cov)+i, prec);
    arb_set_si(r, 2);
    arb_pow_fmpq(r, r, pw_covering_bigradref(cov)+(i-1), prec);
    arb_add(gamma, R, r, prec);
    arb_div_si(gamma, gamma, 2, prec);
    arb_sub(rho, R, r, prec);
    arb_div_si(rho, rho, 2, prec);
    arb_const_pi(R, prec);
    arb_mul(R, R, gamma, prec);
    arb_mul_si(R, R, 2, prec); /* R:= 2*Pi*gamma */
    arb_mul_fmpz(r, rho, fmpq_numref(pw_covering_scaleref(cov)), prec); /*r:=scale*rho*/
    arb_div_fmpz(r, r,   fmpq_denref(pw_covering_scaleref(cov)), prec); /*r:=scale*rho*/
    arb_div(R, R, r, prec); /* R:=2*Pi*gamma/scale*rho */
    arb_ceil(R, R, prec);   /* R:=ceil(2*Pi*gamma/scale*rho) */
    arb_get_unique_fmpz(nbsectors, R);
    
    arb_clear(R);
    arb_clear(r);
    arb_clear(gamma);
    arb_clear(rho);
}

ulong _pw_nb_sectors_in_ith_annulus_sharp( pw_covering_t cov, ulong i ){
    
    ulong N=pw_covering_Nref(cov);
    if ( (i==0) || (i==N-1) )
        return 1;
    
    /* I don't know why the following is commented... should test */
//     ulong m    = pw_covering_mref(cov);
//     ulong cm   = pw_covering_cref(cov)*m;
//     if ( pw_covering_indhigref(cov)[i] - pw_covering_indlowref(cov)[i] <=cm ) {
//         return 1; 
//     }
    
    slong prec = PWPOLY_DEFAULT_PREC;
    arb_t R,r,rho;
    arb_init(R);
    arb_init(r);
    arb_init(rho);
    
    arf_t ub;
    arf_init(ub);
    
    arb_set_si(R, 2);
    arb_pow_fmpq(R, R, pw_covering_bigradref(cov)+i, prec);
    arb_set_si(r, 2);
    arb_pow_fmpq(r, r, pw_covering_bigradref(cov)+(i-1), prec);
    arb_sub(rho, R, r, prec);
    arb_div_si(rho, rho, 2, prec);
    
    arb_const_pi(r, prec);
    arb_mul( r, r, R, prec );
    arb_set_fmpq( R, pw_covering_scaleref(cov), prec );
    arb_sqr(R, R, prec);
    arb_sub_si(R, R, 1, prec);
    arb_sqrt(R, R, prec);
    arb_mul(R, R, rho, prec);
    arb_div(r, r, R, prec);
    
    arb_get_ubound_arf( ub, r, prec );
    ulong u = (ulong) arf_get_si(ub, ARF_RND_CEIL);
    
    arf_clear(ub);
    arb_clear(R);
    arb_clear(r);
    arb_clear(rho);
    
    return u;
    
}

/* assume that 0-th and last coeff of the initial polynomial are not zero */
/* compute log2 of lower bound of the modulus of the roots and  */
/*         log2 of upper bound of the modulus of the roots with */
/* Fujiwara's bounds:                                           */
/* binf = min_{i=1,...,len-1} (weights[0]       - weights[i])/i    */
/* bsup = max_{i=1,...,len-1} (weights[len-1-i] - weights[len-1])/i*/
void _pw_covering_log2_fujiwara_bounds ( fmpq_t binf, fmpq_t bsup, const fmpq_ptr weights, ulong len ) {
    
    fmpq_t ti, ts, den;
    fmpq_init(ti);
    fmpq_init(ts);
    fmpq_init(den);
    fmpq_sub( binf, weights+0,       weights+1);
    fmpq_sub( bsup, weights+(len-2), weights+(len-1));
    for( ulong i=2; i<len; i++) {
        fmpq_set_ui(den, i, 1);
        fmpq_sub( ti, weights+0,         weights+i);
        fmpq_div( ti, ti, den);
        fmpq_sub( ts, weights+(len-1-i), weights+(len-1));
        fmpq_div( ts, ts, den);
        if (fmpq_cmp( ti, binf )<0)
            fmpq_set(binf, ti);
        if (fmpq_cmp( ts, bsup )>0)
            fmpq_set(bsup, ts);
    }
    fmpq_add_si(bsup, bsup, 1);
    fmpq_sub_si(binf, binf, 1);
    fmpq_clear(den);
    fmpq_clear(ts);
    fmpq_clear(ti);
}

int _pw_covering_first_annulus_with_root_bounds( pw_covering_t cov, 
                                                 const fmpq_t binf, const fmpq_t bsup,
                                                 const fmpq_ptr highslopes ){
    
    int res = 1;
    ulong  deg = pw_covering_degreeref(cov);
    pw_covering_Nref(cov) = 1;
    fmpq_set( pw_covering_bigradref(cov)+0, binf);
    pw_covering_indlowref(cov)[0] = 0;
    ulong kps=1;
//     fmpq_print(highslopes + kps); printf("\n");
    /* find kps: it is the last index i s.t. binf > highslopes[i] */
    /* since highslopes[0]=-inf, kps > 0                    */
//     printf("binf: "); fmpq_print(binf); 
//     printf(" kps: %lu, highslopes + kps: ", kps); fmpq_print(highslopes + kps); printf("\n");
    while( (kps<=deg) && ( fmpq_cmp( binf, highslopes + kps) >0) ) {
        kps++;
//         printf("binf: "); fmpq_print(binf); 
//         printf(" kps: %lu, highslopes + kps: ", kps); fmpq_print(highslopes + kps); printf("\n");
    }
    if (kps>deg)
        kps=deg;
    pw_covering_indhigref(cov)[0] = kps;
    /* it is not the one before the last if (should never happen!)*/
    res = fmpq_cmp( binf, bsup)<0;
    
    return res;
}

int _pw_covering_next_annulus_with_root_bounds( pw_covering_t cov, 
                                                const fmpq_t bsup,
                                                const fmpq_ptr lowslopes, const fmpq_ptr highslopes ){
    
    /* reallocate tables if necessary */
    if ( pw_covering_Nref(cov) >= (cov->_size_allocated) ) {
        _pw_covering_extend_tables( cov );
    }
    
    fmpz_t k;
    fmpq_t ratiok;
    fmpz_init(k);
    fmpq_init(ratiok);
    
    /* compute next annulus */
    pw_covering_Nref(cov) += 1;
    ulong   deg = pw_covering_degreeref(cov);
    ulong     N = pw_covering_Nref(cov);
    ulong     m = pw_covering_mref(cov);
    ulong   km  = pw_covering_indlowref(cov)[N-2];
    ulong   kp  = pw_covering_indhigref(cov)[N-2];
    ulong   kms = km;
    ulong   kps = kp;
    fmpq_ptr s  = pw_covering_bigradref(cov) + N-2;
    fmpq_ptr ss = pw_covering_bigradref(cov) + N-1;
    
    /* find kms: it is the first index i s.t. s < lowslopes[i] */
    /* since lowslopes[deg]=+inf, kms < deg                    */
    while( (kms<deg) && ( fmpq_cmp( s, lowslopes + kms) >=0) )
        kms++;
    /* set the number of monomials in the support k = *kms - *kps + 1*/
    fmpz_set_si(k, kps + 1);
    fmpz_sub_si(k, k, kms);
    /* set ratiok = (ratio/scale)*(m/k) */
    fmpq_mul_ui( ratiok, pw_covering_factoref(cov), m );
    fmpq_div_fmpz(ratiok, ratiok, k);
    /* increase kps until kps = deg or highslopes[kps] >= s + (ratio/scale)*(m/k) */
    fmpq_sub(ss, highslopes+kps, s);
    while( ( kps < deg ) && (fmpq_cmp(ss, ratiok)<0) ){
        kps++;
        /* set the number of monomials in the support k = *kms - *kps + 1*/
        fmpz_set_si(k, kps + 1);
        fmpz_sub_si(k, k, kms);
        /* set ratiok = (ratio/scale)*(m/k) */
        fmpq_mul_ui( ratiok, pw_covering_factoref(cov), m );
        fmpq_div_fmpz(ratiok, ratiok, k);
        
        fmpq_sub(ss, highslopes+kps, s);
    }
    /* sets ss to s + (ratio/scale)*(m/k) */
    fmpq_add(ss, s, ratiok);
    /* it is not the one before the last if */
    int res = fmpq_cmp( ss, bsup)<0;
    
    pw_covering_indlowref(cov)[N-1]=kms;
    pw_covering_indhigref(cov)[N-1]=kps;
    
    fmpz_clear(k);
    fmpq_clear(ratiok);
    
    return res;
}

void _pw_covering_last_annulus_with_root_bounds( pw_covering_t cov, 
                                                 const fmpq_ptr lowslopes ) {
    
    /* reallocate tables if necessary */
    if ( pw_covering_Nref(cov) >= (cov->_size_allocated) ) {
        _pw_covering_extend_tables( cov );
    }
    ulong   deg = pw_covering_degreeref(cov);
    /* set last annulus */
    pw_covering_Nref(cov) += 1;
    ulong N = pw_covering_Nref(cov);
    ulong   km  = pw_covering_indlowref(cov)[N-2];
    ulong   kms = km;
    fmpq_ptr s  = pw_covering_bigradref(cov) + N-2;
    /* find kms: it is the first index i s.t. s < lowslopes[i] */
    /* since lowslopes[deg]=+inf, kms < deg                    */
    while( (kms<deg) && ( fmpq_cmp( s, lowslopes + kms) >=0) )
        kms++;
    pw_covering_indlowref(cov)[N-1] = kms;
    pw_covering_indhigref(cov)[N-1] = pw_covering_degreeref(cov);
    fmpq_one( pw_covering_bigradref(cov) + N-1 );
    fmpz_zero( fmpq_denref( pw_covering_bigradref(cov) + N-1 ) );
}

void _pw_covering_compute_annulii_from_fmpq_weights( pw_covering_t cov, const fmpq_ptr weights ){
    
    ulong len  = pw_covering_degreeref(cov)+1;
    ulong m    = pw_covering_mref(cov);
    
    fmpq_ptr mweights      = _fmpq_vec_init( len );
    fmpq_ptr sweights      = _fmpq_vec_init( len );
    fmpq_ptr highslopes    = _fmpq_vec_init( len );
    fmpq_ptr lowslopes     = _fmpq_vec_init( len );
//     fmpq_ptr middleslopes  = _fmpq_vec_init( len );
    /* compute -weights */
    for (ulong i = 0; i<len; i++)
        fmpq_neg(mweights+i, weights+i );
        
    /* compute the lower part of the convex hull of -weights */
    ulong *CH = (ulong *) pwpoly_malloc ( len*sizeof(ulong) );
    ulong lenCH = _pw_covering_fmpq_convex_hull( CH, mweights, len );
//     printf("indexes of the edges of the convex: \n[");
//     for (ulong i=0; i<lenCH; i++)
//         printf("%lu, ", CH[i]);
//     printf("]\n");
    /* let sweights be the point of mweights in convex position */
    _pw_covering_fmpq_snap_convex_hull( sweights, mweights, CH, lenCH);
    /* shift the snapped convex hull by -m towards -inf */
    for (ulong i=0; i<len; i++) {
        fmpq_sub_si(sweights+i, sweights+i, m);
    }
    /* compute the vectors of highslopes, lowslopes and middleslopes */
    _pw_covering_fmpq_highslopes(     highslopes, sweights, mweights, len, CH);
    _pw_covering_fmpq_lowslopes(       lowslopes, sweights, mweights, len, CH, lenCH );
    _pw_covering_fmpq_middleslopes( pw_covering_sl_at_edref(cov), sweights, len );
    
    fmpq_t binf, bsup;
    fmpq_init(binf);
    fmpq_init(bsup);
    _pw_covering_log2_fujiwara_bounds ( binf, bsup, weights, len );
//     printf("_pw_covering_compute_annulii_from_fmpq_weights: log2 of root bounds: ");
//     fmpq_print(binf);
//     printf(", ");
//     fmpq_print(bsup);
//     printf("\n");
    
    int cont = _pw_covering_first_annulus_with_root_bounds( cov, binf, bsup, highslopes );
    while (cont)
        cont = _pw_covering_next_annulus_with_root_bounds( cov, bsup, lowslopes, highslopes );
    _pw_covering_last_annulus_with_root_bounds( cov, lowslopes );
    
    fmpq_clear(bsup);
    fmpq_clear(binf);  
    
    for (ulong n=0; n< pw_covering_Nref(cov); n++) {
        /* compute the index of monomial supporting the slope _bigrad[n] */
        pw_covering_inddomref(cov)[n] = _pw_covering_fmpq_searchsorted( pw_covering_bigradref(cov)+n, 
                                                                        pw_covering_sl_at_edref(cov), len-1 );
        /* compute the number of sectors in n-th annulus */
        fmpz_t nbsectors;
        fmpz_init(nbsectors);
//         _pw_nb_sectors_in_ith_annulus( nbsectors, cov, n );
        ulong nbs = _pw_nb_sectors_in_ith_annulus_sharp( cov, n );
        fmpz_set_ui(nbsectors, nbs);
        /* get the power of 2 just above */
        pw_covering_log2_nbsectref(cov)[n] = (uint) fmpz_clog_ui( nbsectors, 2 );
        pw_covering_nbsectref(cov)[n]=0x1<<(pw_covering_log2_nbsectref(cov)[n]);
        pw_covering_Ksumref(cov) += pw_covering_nbsectref(cov)[n];
        pw_covering_max_nbsectref(cov) = PWPOLY_MAX( pw_covering_max_nbsectref(cov), pw_covering_nbsectref(cov)[n] );
//         printf("nbsectors: %lu, %lu\n", nbs, pw_covering_nbsectref(cov)[n] ); 
        fmpz_clear(nbsectors);
    }
    
    pwpoly_free(CH);
    _fmpq_vec_clear( mweights, len );
    _fmpq_vec_clear( sweights, len );
    _fmpq_vec_clear( highslopes, len );
    _fmpq_vec_clear( lowslopes, len );
}

void _pw_covering_compute_app_rads( pw_covering_t cov, slong prec ) {
    
    if (pw_covering_app_precref(cov) >= prec) {
        return;
    }
    
    if ( pw_covering_app_precref(cov) >=0 ) {
        _arb_vec_clear(pw_covering_app_bigradref(cov), cov->_size_allocated);
        _arb_vec_clear(pw_covering_app_gammasref(cov), cov->_size_allocated);
        _arb_vec_clear(pw_covering_app_rhosref(cov), cov->_size_allocated);
        _acb_vec_clear(pw_covering_app_unit_rref(cov), pw_covering_max_nbsectref(cov));
    }
    
    ulong N = pw_covering_Nref(cov);
    
    pw_covering_app_precref(cov)   = prec;
    pw_covering_app_bigradref(cov) = _arb_vec_init( cov->_size_allocated );
    pw_covering_app_gammasref(cov) = _arb_vec_init( cov->_size_allocated );
    pw_covering_app_rhosref(cov)   = _arb_vec_init( cov->_size_allocated );
    
    ulong maxNbSect = pw_covering_max_nbsectref(cov);
    pw_covering_app_unit_rref(cov) = _acb_vec_init(maxNbSect);
    
    for (ulong n=0; n< N; n++) {
        arb_set_si(pw_covering_app_bigradref(cov)+n, 2);
        arb_pow_fmpq(pw_covering_app_bigradref(cov)+n, 
                     pw_covering_app_bigradref(cov)+n, 
                     pw_covering_bigradref(cov)+n, prec);
        if (n==0) {
            arb_div_si( pw_covering_app_gammasref(cov)+n, pw_covering_app_bigradref(cov)+n, 2, prec );
            arb_set( pw_covering_app_rhosref(cov)+n, pw_covering_app_gammasref(cov)+n );
        } else if (n==N-1) {
            arb_pos_inf( pw_covering_app_gammasref(cov)+n );
            arb_pos_inf( pw_covering_app_rhosref(cov)+n );
        } else {
            arb_add( pw_covering_app_gammasref(cov)+n, 
                     pw_covering_app_bigradref(cov)+n, pw_covering_app_bigradref(cov)+(n-1), prec);
            arb_sub( pw_covering_app_rhosref(cov)+n, 
                     pw_covering_app_bigradref(cov)+n, pw_covering_app_bigradref(cov)+(n-1), prec);
            arb_div_si( pw_covering_app_gammasref(cov)+n, pw_covering_app_gammasref(cov)+n, 2, prec );
            arb_div_si( pw_covering_app_rhosref(cov)+n, pw_covering_app_rhosref(cov)+n, 2, prec );
//             printf("n: %lu, gamma: ", n);
//             arb_printd(pw_covering_app_gammasref(cov)+n, 10);
//             printf(" rho: ");
//             arb_printd(pw_covering_app_rhosref(cov)+n, 10);
//             printf("\n");
        }
        
        
    }   
    _acb_vec_unit_roots( pw_covering_app_unit_rref(cov), (slong)maxNbSect, (slong)maxNbSect, prec );
}

void _pw_covering_get_inner_outer_radii_annulus( arb_t inner, arb_t outer, pw_covering_t cov, ulong n, slong prec ) {
    ulong N = pw_covering_Nref(cov);
    if (n>=N) {
        arb_indeterminate(inner);
        arb_indeterminate(outer);
        return;
    }
    /* this does something only if not already computed of computed with a precision < prec */
    _pw_covering_compute_app_rads( cov, prec );
    arb_t sr;
    arb_init(sr);
    arb_set_fmpq( sr, pw_covering_scaleref(cov), prec );
    arb_mul( sr, pw_covering_app_rhosref(cov)+n, sr, prec );
    /* inner radius of current annulus */
    arb_sub( inner, pw_covering_app_gammasref(cov)+n, sr, prec );
    if (n==0)
        arb_zero(inner);
    /* outer radius of current annulus */
    arb_add( outer, pw_covering_app_gammasref(cov)+n, sr, prec );
    arb_clear(sr);
}

/* Pre conditions: */
/* Post conditions returns 1 => sector's disks of n-th annulus have empty intersection with dom */
int _pw_covering_annulus_domain_empty_intersection( pw_covering_t cov, ulong n, const domain_t dom, slong prec ){
    
    ulong N = pw_covering_Nref(cov);
    
    if (n>=N)
        return 1;
    
    /* this does something only if not already computed of computed with a precision < prec */
    _pw_covering_compute_app_rads( cov, prec );
    arb_t r, R;
    arb_init(r);
    arb_init(R);
    
    _pw_covering_get_inner_outer_radii_annulus( r, R, cov, n, prec );
    int res = domain_acb_annulus_empty_intersection(dom, r, R, prec );
//     printf("n: %lu, r: ", n); arb_printd(r, 10); printf(", R: "); arb_printd(R,10); printf(", res: %d\n", res);
    
    arb_clear(R);
    arb_clear(r);
    
    return res;
    
}

void pw_covering_get_first_last_annuli_domain( ulong * first, ulong * last,
                                               pw_covering_t cov, const domain_t dom, slong prec ) {
//     ulong N = pw_covering_Nref(cov);
//     (*first)=0;
//     (*last)=N-2;
    while ( ((*first) <= (*last)) && _pw_covering_annulus_domain_empty_intersection( cov, (*first), dom, prec ) )
        (*first)++;
    while ( ((*last) > (*first)) && _pw_covering_annulus_domain_empty_intersection( cov, (*last), dom, prec ) )
        (*last)--;    
}
          
int pw_covering_disk_domain_empty_intersection( pw_covering_t cov, ulong n, ulong k, const domain_t dom, slong prec ) {
    
    /* this does something only if not already computed of computed with a precision < prec */
    _pw_covering_compute_app_rads( cov, prec );
    
//     ulong N = pw_covering_Nref(cov);
    ulong K = pw_covering_nbsectref(cov)[n];
    
    acb_t center, rot;
    arb_t radius;
    acb_init(center);
    acb_init(rot);
    arb_init(radius);
    
    arb_set_fmpq( radius, pw_covering_scaleref(cov), prec );
    arb_mul( radius, pw_covering_app_rhosref(cov)+n, radius, prec );
    acb_set_arb( center, pw_covering_app_gammasref(cov)+n );
    _pw_covering_get_root_of_unit( rot, cov, k, K );
    acb_mul(center, center, rot, prec);
    
    int res = domain_is_acb_disc_strictly_outside_domain( dom, center, radius, prec );
    
    arb_clear(radius);
    acb_clear(rot);
    acb_clear(center);
    
    return res;
}

/* Pre  conditions:                                                              */
/* Post conditions: if (_app_prec==1) or (K > _max_nbsect) sets x to NaN + I*NaN */
/*                  else sets x to e^(I*2Pi*k/K)                                 */
void _pw_covering_get_root_of_unit( acb_t x, const pw_covering_t cov, slong k, ulong K ){
    
    if ( (pw_covering_app_precref(cov)==-1)||(K>pw_covering_max_nbsectref(cov)) ) {
        arb_indeterminate( acb_realref(x) );
        arb_indeterminate( acb_imagref(x) );
    } else {
        ulong km    = k%K;
        ulong shift = pw_covering_max_nbsectref(cov)/K;
        acb_set( x, pw_covering_app_unit_rref(cov) + km*shift );
    }
}

ulong pw_covering_locate_point( pw_sector_list_t sectors, ulong around,
                                const pw_covering_t cov, const acb_t point, slong prec ) {
#ifdef PW_PROFILE
    clock_t start = clock();
#endif
    
    arb_t abs_point, ang_point, Pi, temp;
    arf_t l, u;
    arb_init(abs_point);
    arb_init(ang_point);
    arb_init(Pi);
    arb_init(temp);
    arf_init(l);
    arf_init(u);
    
    arb_const_pi(Pi, prec);
    acb_abs( abs_point, point, prec );
    
    acb_arg( ang_point, point, prec );
    if ( arb_contains_zero(acb_imagref(point)) ) {
        if ( arb_is_negative( acb_realref(point) ) ){
            acb_t mpoint;
            acb_init(mpoint);
            acb_neg(mpoint, point);
            acb_arg( ang_point, mpoint, prec );
            arb_add( ang_point, ang_point, Pi, prec );
            acb_clear(mpoint);
        }
    } 
    
    ulong N = pw_covering_Nref(cov);
    /* let pw_covering_app_bigradref(cov)+(-1) = -inf */
    /*     pw_covering_app_bigradref(cov)+(N-1) = +inf */
    /* find the highest index  0<=low<=N-1 s.t. pw_covering_app_bigradref(cov)+(low-1) < abs_point */
    /*  and the lowest  index  0<=hig<=N-1 s.t. abs_point < pw_covering_app_bigradref(cov)+hig */
    ulong low = around;
    low = PWPOLY_MIN( low, N-1);
    low = ( low > 0 ? low-1: 0 );
    /* find the highest index  0<=low<=N-1 s.t. pw_covering_app_bigradref(cov)+(low-1) < abs_point */
    while ( ( low>0 )&&(!arb_lt ( pw_covering_app_bigradref(cov) + (low-1), abs_point)) )
        low--;
    while ( ( low<(N-1) )&&(arb_lt ( pw_covering_app_bigradref(cov) + low, abs_point)) )
        low++;
    /*  and the lowest  index  0<=hig<=N-1 s.t. abs_point < pw_covering_app_bigradref(cov)+hig */
    /*  note hig >=low */
    ulong hig = low;
    while ( ( hig<N-1 )&&(!arb_gt ( pw_covering_app_bigradref(cov) + hig, abs_point)) )
        hig++;
    
    for (ulong n=low; n<=hig; n++) {
        ulong K = pw_covering_nbsectref(cov)[n];
        if (K==1) {
            pw_sector_list_insert_sorted_unique( sectors, n, 0, K );
        } else {
            /* find the highest k between 0 and K-1 such that 2Pi(2k-1)/(2K) < ang_point */
            /*                                       <=> k < ang_point*K/(2Pi) + 1/2     */
            arb_mul_ui( temp, ang_point, K, prec );
            arb_div( temp, temp, Pi, prec );
            arb_add_si (temp, temp, 1, prec);
            arb_div_si (temp, temp, 2, prec);
            arb_get_interval_arf(l, u, temp, prec);
            slong lk = arf_get_si(l, ARF_RND_FLOOR);
            slong uk = arf_get_si(u, ARF_RND_CEIL);
            for (slong k = lk; k<uk; k++){
                pw_sector_list_insert_sorted_unique( sectors, n, k, K );
            }
        }
    }
    
    arb_clear(abs_point);
    arb_clear(ang_point);
    arb_clear(Pi);
    arb_clear(temp);
    arf_clear(l);
    arf_clear(u);
    
#ifdef PW_PROFILE
    clicks_in_locate += (clock() - start);
#endif 
    return low;
}

slong _pw_prec_for_compare;

int _pw_acb_compare_abs_of_midpoint ( const acb_t u, const acb_t v ) {
    acb_t mu, mv;
    int result;
    acb_init(mu);
    acb_init(mv);
    acb_get_mid(mu, u);
    acb_get_mid(mv, v);
    arf_mul( arb_midref( acb_realref( mu ) ), arb_midref( acb_realref( mu ) ), arb_midref( acb_realref( mu ) ), _pw_prec_for_compare, ARF_RND_NEAR );
    arf_mul( arb_midref( acb_imagref( mu ) ), arb_midref( acb_imagref( mu ) ), arb_midref( acb_imagref( mu ) ), _pw_prec_for_compare, ARF_RND_NEAR );
    arf_add( arb_midref( acb_realref( mu ) ), arb_midref( acb_realref( mu ) ), arb_midref( acb_imagref( mu ) ), _pw_prec_for_compare, ARF_RND_NEAR );
    arf_mul( arb_midref( acb_realref( mv ) ), arb_midref( acb_realref( mv ) ), arb_midref( acb_realref( mv ) ), _pw_prec_for_compare, ARF_RND_NEAR );
    arf_mul( arb_midref( acb_imagref( mv ) ), arb_midref( acb_imagref( mv ) ), arb_midref( acb_imagref( mv ) ), _pw_prec_for_compare, ARF_RND_NEAR );
    arf_add( arb_midref( acb_realref( mv ) ), arb_midref( acb_realref( mv ) ), arb_midref( acb_imagref( mv ) ), _pw_prec_for_compare, ARF_RND_NEAR );
    result = arf_cmp(arb_midref( acb_realref( mu ) ), arb_midref( acb_realref( mv ) ));
    acb_clear(mv);
    acb_clear(mu);
    return result;
}

ulong pw_covering_locate_points( pw_sector_list_t sectors, const pw_covering_t cov, acb_ptr points, slong len, slong prec ) {
    
// #ifdef PW_PROFILE
//     clock_t start = clock();
// #endif
    ulong size_save = pw_sector_list_get_size(sectors);
    ulong n=0;
//     int level = 4;
    /* sort points by increasing modulus of midpoint using precision prec */
    _pw_prec_for_compare = prec;
    qsort(points, len, sizeof(acb_struct), (__compar_fn_t) _pw_acb_compare_abs_of_midpoint);
// #ifdef PW_PROFILE
//     printf("pw_covering_locate_points: time in sort: %f\n", (double)(clock() - start)/CLOCKS_PER_SEC );
// #endif
    
    for (slong i=0; i<len; i++) {
        n = pw_covering_locate_point( sectors, n, cov, points+i, prec );
    }
// #ifdef PW_PROFILE
//     printf("pw_covering_locate_points: time in locate points: %f\n", (double)(clock() - start)/CLOCKS_PER_SEC );
// #endif
    return pw_sector_list_get_size(sectors) - size_save;
}

/* assume np is the index of an existing annulus */
void _pw_covering_add_neighbors_on_connected_annulus( pw_sector_list_t neighbors, 
                                                      const pw_covering_t cov, ulong np, ulong n, ulong k, int realCoeffs ) {
    ulong K = pw_covering_nbsectref(cov)[n];
    ulong Kp = pw_covering_nbsectref(cov)[np];
    slong lastk = ( ( realCoeffs && (Kp>1) ) ? Kp/2 : Kp-1);
    
    fmpq_t angle, nangle;
    fmpq_init (angle);
    fmpq_init (nangle);
    
    if ((K==1)||(Kp==1)) { /* add the whole annulii np */
        for (slong knei = 0; knei <= lastk; knei++)
            pw_sector_list_insert_sorted_unique( neighbors, np, knei, Kp );
    } else {
        slong knei = (k*Kp/K) %Kp; /* (np, knei) is the neighbor with either same center angle than (n, k) */
                                   /* or which upper angle is the center angle of (n, k) */
        if ( (knei<=lastk) && ((!realCoeffs) || (knei>=0)) )
        pw_sector_list_insert_sorted_unique( neighbors, np, knei, Kp );
        ulong added=1;
        fmpq_set_si(angle,  2*k-1,    2*K);    /* lower angle of (n, k) */
        fmpq_set_si(nangle, 2*knei-1, 2*Kp);   /* lower angle of (np, knei) */
        while ( ((!realCoeffs) || (knei>0))&&(fmpq_cmp( nangle, angle ) >= 0)&&(added<Kp) ) {
            knei--;
            fmpq_set_si(nangle, 2*knei-1, 2*Kp); /* lower angle of (np, knei) */
            pw_sector_list_insert_sorted_unique( neighbors, np, knei, Kp );
            added++;
        }
        fmpq_set_si(angle,  2*k+1,    2*K);    /* upper angle of (n, k) */
        fmpq_set_si(nangle, 2*knei+1, 2*Kp);   /* upper angle of (np, knei) */
        while ( (knei<lastk)&&(fmpq_cmp( nangle, angle ) <= 0)&&(added<Kp) ) {
            knei++;
            fmpq_set_si(nangle, 2*knei+1, 2*Kp); /* upper angle of (n-1, knei) */
            pw_sector_list_insert_sorted_unique( neighbors, np, knei, Kp );
            added++;
        }
    }
    
    fmpq_clear (nangle);
    fmpq_clear (angle);
}

ulong pw_covering_add_neighbors( pw_sector_list_t sectors, const pw_covering_t cov, int realCoeffs ) {
    
    ulong size_save = pw_sector_list_get_size(sectors);
    pw_sector_list_t neighbors;
    pw_sector_list_init(neighbors);
    
    ulong N = pw_covering_Nref(cov);
    
    /* fill neighboors with the neighbors of sectors */
    pw_sector_list_iterator it = pw_sector_list_begin(sectors);
    while ( it!=pw_sector_list_end() ) {
        pw_sector_ptr scur = pw_sector_list_elmt(it);
        ulong n = pw_sector_nref(scur);
        slong k = (slong)pw_sector_kref(scur); /* is >=0 */
        ulong K = pw_covering_nbsectref(cov)[n];
        slong lastk = ( ( realCoeffs && (K>1) ) ? K/2 : K-1);
        
        if (n>0) { /* add neighbors on the previous annulus */
            _pw_covering_add_neighbors_on_connected_annulus( neighbors, cov, n-1, n, k, realCoeffs );
        }
        if (n<N-1) { /* add neighbors on the next annulus */
            _pw_covering_add_neighbors_on_connected_annulus( neighbors, cov, n+1, n, k, realCoeffs );
        }
        if (K>1) { /* add neighbors on this annulus */
            if ( (!realCoeffs) || (k>0) )
                pw_sector_list_insert_sorted_unique( neighbors, n, (slong)k-1, K );
            if (k<lastk)
                pw_sector_list_insert_sorted_unique( neighbors, n, (slong)k+1, K );
        }
        it = pw_sector_list_next(it);
    }
    /* merge sectors and its neighbors in sectors */
    while (!pw_sector_list_is_empty(neighbors)) {
        pw_sector_ptr scur = pw_sector_list_pop(neighbors);
        int res = list_insert_sorted_unique(sectors, scur, _pw_sector_isless_for_list); 
        if (!res)
            pwpoly_free(scur);
    }
    
    pw_sector_list_clear(neighbors);
    return pw_sector_list_get_size(sectors) - size_save;
}

/* does not support aliazing */
/* Pre  conditions:                                                               */
/* Post conditions: if (_app_prec==-1) or (n >= N) sets y to NaN + I*NaN          */
/*                  if ( (n==0) || ( (n<N-1) && (K[n]==1) ) sets x to y           */
/*                  if (n==N-1) sets x to 1/y                                     */
/*                  else sets x to (1/(scale*rho))*(y*e^(-i2Pik/K)-gamma)         */
void _pw_covering_shift_point( acb_t x, const pw_covering_t cov, const ulong n, const slong k, const acb_t y, slong prec ){
    
    ulong N = pw_covering_Nref(cov);
    ulong K = pw_covering_nbsectref(cov)[n];
    
    if ( (n==0) || ( (n<N-1) && (K==1) ) ) {
        acb_set(x, y);
    } else if (n==N-1) {
        acb_inv(x, y, prec);
    } else if (n>=N) {
        arb_indeterminate( acb_realref(x) );
        arb_indeterminate( acb_imagref(x) );
    } else {
//         printf("ICI\n");
        arb_t temp;
        arb_init(temp);
        arb_ptr gamma = pw_covering_app_gammasref(cov)+n;
        arb_ptr rho   = pw_covering_app_rhosref(cov)+n;
        /* shift y: y = (gamma + scale*rho*x)e^(i2Pik/K)       */
        /*      <=> x = (1/(scale*rho))*(y*e^(-i2Pik/K)-gamma) */ 
        _pw_covering_get_root_of_unit( x, cov, -k, K );
        arb_zero(temp);
        arb_set_fmpq( temp, pw_covering_scaleref(cov), prec );
        arb_mul( temp, rho, temp, prec );
        arb_inv( temp, temp, prec );
        acb_mul( x, y, x, prec );
        arb_sub( acb_realref(x), acb_realref(x), gamma, prec );
        acb_mul_arb( x, x, temp, prec );
        arb_clear(temp);
    }
    
}

/* does not support aliazing */
/* Pre  conditions:                                                               */
/* Post conditions: if (_app_prec==-1) or (n >= N) sets y to NaN + I*NaN          */
/*                  if ( (n==0) || ( (n<N-1) && (K[n]==1) ) sets y to x           */
/*                  if (n==N-1) sets y to 1/x                                     */
/*                  else sets y to (gamma + scale*rho*x)e^(i2Pik/K)               */
void _pw_covering_shift_back_point( acb_t y, const pw_covering_t cov, const ulong n, const slong k, const acb_t x, slong prec ){
    
    ulong N = pw_covering_Nref(cov);
    ulong K = pw_covering_nbsectref(cov)[n];
    
    if ( (n==0) || ( (n<N-1) && (K==1) ) ) {
        acb_set(y, x);
    } else if (n==N-1) {
        acb_inv(y, x, prec);
    } else if (n>=N) {
        arb_indeterminate( acb_realref(y) );
        arb_indeterminate( acb_imagref(y) );
    } else {
        acb_t rot;
        acb_init(rot);
        arb_ptr gamma = pw_covering_app_gammasref(cov)+n;
        arb_ptr rho   = pw_covering_app_rhosref(cov)+n;
        /* shift x: y = (gamma + scale*rho*x)e^(i2Pik/K)       */ 
        ulong K = pw_covering_nbsectref(cov)[n];
        _pw_covering_get_root_of_unit( rot, cov, k, K );
//         arb_zero(temp);
        acb_zero(y);
        arb_set_fmpq( acb_realref(y), pw_covering_scaleref(cov), prec );
        arb_mul( acb_realref(y), rho, acb_realref(y), prec );
        acb_mul( y, y, x, prec);
        arb_add( acb_realref(y), acb_realref(y), gamma, prec );
        acb_mul( y, y, rot, prec);
        acb_clear(rot);
    }
}

#ifndef PW_NO_INTERFACE
void _pw_covering_sectors_gnuplot(  FILE * f, pw_covering_t cov, ulong i ) {
    
    ulong nbdisks = pw_covering_nbsectref(cov)[i];

    if ( nbdisks==1 ) {
        return; 
    }
    
    slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits=16;
    
    acb_ptr ro = _acb_vec_init( 2*nbdisks );
    _acb_vec_unit_roots(ro, 2*nbdisks, 2*nbdisks, prec);
    
    acb_t point1, point2;
    acb_init(point1);
    acb_init(point2);
    arb_t r, R;
    arb_init(r);
    arb_init(R);
    arb_set_si(R, 2);
    arb_pow_fmpq(R, R, pw_covering_bigradref(cov)+i, prec);
    arb_set_si(r, 2);
    arb_pow_fmpq(r, r, pw_covering_bigradref(cov)+(i-1), prec);
    for (ulong j = 0; j<nbdisks; j++) {
        acb_mul_arb(point1, ro+(2*j+1), r, prec);
        acb_mul_arb(point2, ro+(2*j+1), R, prec);
        acb_sub(point2, point2, point1, prec);
        arb_fprintn(f, acb_realref(point1), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_imagref(point1), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_realref(point2), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_imagref(point2), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    
    arb_clear(R);
    arb_clear(r);
    acb_clear(point2);
    acb_clear(point1);
    _acb_vec_clear( ro, 2*nbdisks );
}

void _pw_covering_sectors_domain_gnuplot(  FILE * f, pw_covering_t cov, ulong i, const domain_t dom ) {
    
    ulong nbdisks = pw_covering_nbsectref(cov)[i];

    if ( nbdisks==1 ) {
        return; 
    }
    
    slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits=16;
    
    acb_ptr ro = _acb_vec_init( 2*nbdisks );
    _acb_vec_unit_roots(ro, 2*nbdisks, 2*nbdisks, prec);
    
    acb_t point1, point2;
    acb_init(point1);
    acb_init(point2);
    arb_t r, R;
    arb_init(r);
    arb_init(R);
    arb_set_si(R, 2);
    arb_pow_fmpq(R, R, pw_covering_bigradref(cov)+i, prec);
    arb_set_si(r, 2);
    arb_pow_fmpq(r, r, pw_covering_bigradref(cov)+(i-1), prec);
    for (ulong j = 0; j<nbdisks; j++) {
        
        if (pw_covering_disk_domain_empty_intersection( cov, i, j, dom, prec ))
            continue;
        
        if (j==0) {
            acb_mul_arb(point1, ro+(2*nbdisks-1), r, prec);
            acb_mul_arb(point2, ro+(2*nbdisks-1), R, prec);
        } else {
            acb_mul_arb(point1, ro+(2*j-1), r, prec);
            acb_mul_arb(point2, ro+(2*j-1), R, prec);
        }
        acb_sub(point2, point2, point1, prec);
        arb_fprintn(f, acb_realref(point1), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_imagref(point1), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_realref(point2), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_imagref(point2), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
        
        acb_mul_arb(point1, ro+(2*j+1), r, prec);
        acb_mul_arb(point2, ro+(2*j+1), R, prec);
        acb_sub(point2, point2, point1, prec);
        arb_fprintn(f, acb_realref(point1), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_imagref(point1), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_realref(point2), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_imagref(point2), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    
    arb_clear(R);
    arb_clear(r);
    acb_clear(point2);
    acb_clear(point1);
    _acb_vec_clear( ro, 2*nbdisks );
}

void _pw_covering_sectors_minmax_gnuplot(  FILE * f, pw_covering_t cov, ulong i, slong min, slong max ) {
    
    ulong nbdisks = pw_covering_nbsectref(cov)[i];

    if ( nbdisks==1 ) {
        return; 
    }
    
    slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits=16;
    
    acb_ptr ro = _acb_vec_init( 2*nbdisks );
    _acb_vec_unit_roots(ro, 2*nbdisks, 2*nbdisks, prec);
    
    acb_t point1, point2;
    acb_init(point1);
    acb_init(point2);
    arb_t r, R;
    arb_init(r);
    arb_init(R);
    arb_set_si(R, 2);
    arb_pow_fmpq(R, R, pw_covering_bigradref(cov)+i, prec);
    arb_set_si(r, 2);
    arb_pow_fmpq(r, r, pw_covering_bigradref(cov)+(i-1), prec);
//     for (ulong j = 0; j<nbdisks; j++) {
    for (slong j = min; j<=max; j++) {
        slong tjp1 = (2*j+1)%(2*nbdisks);
        acb_mul_arb(point1, ro+tjp1 , r, prec);
        acb_mul_arb(point2, ro+tjp1 , R, prec);
        acb_sub(point2, point2, point1, prec);
        arb_fprintn(f, acb_realref(point1), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_imagref(point1), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_realref(point2), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_imagref(point2), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
        slong tjm1 = (2*j-1)%(2*nbdisks);
        acb_mul_arb(point1, ro+tjm1, r, prec);
        acb_mul_arb(point2, ro+tjm1, R, prec);
        acb_sub(point2, point2, point1, prec);
        arb_fprintn(f, acb_realref(point1), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_imagref(point1), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_realref(point2), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "   ");
        arb_fprintn(f, acb_imagref(point2), nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    
    arb_clear(R);
    arb_clear(r);
    acb_clear(point2);
    acb_clear(point1);
    _acb_vec_clear( ro, 2*nbdisks );
    
}

void _pw_covering_gnuplot( FILE * f, pw_covering_t cov ){
    
    slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits = 16;
    ulong     N = pw_covering_Nref(cov);
    arb_t radius;
    arb_init(radius);
     
    fprintf(f, "set size square\n");
    fprintf(f, "set xrange[-10 : 10]\n");
    fprintf(f, "set yrange[-10 : 10]\n");
    fprintf(f, "plot '-' title 'anulii circles' with circles lc rgb \"#008080\" fs transparent solid 0.0, \\\n");
//     fprintf(f, "     '-' title 'last circle   ' with circles lc rgb \"#ff0000\" fs transparent solid 0.0,  \\\n");
//     fprintf(f, "     '-' title 'covering circle' with circles lc rgb \"#000000\" fs transparent solid 0.0  \n");
    fprintf(f, "     '-' title 'angular sectors' with vectors nohead lc rgb \"#000000\" \n");
     
    for (ulong i=0; i< N-1; i++) {
        arb_set_si(radius, 2);
        arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+i, prec);
        fprintf(f, "0  0  ");
        arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
//     arb_set_si(radius, 2);
//     arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+(N-1), prec);
//     fprintf(f, "0  0  ");
//     arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
//     fprintf(f, "\n");
//     fprintf(f, "e\n");
    
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        _pw_covering_sectors_gnuplot( f, cov, i );
    }
    fprintf(f, "e\n");
    
    fprintf(f, "pause mouse close\n");
    arb_clear(radius);
}

void _pw_covering_with_domain_gnuplot( FILE * f, pw_covering_t cov, const domain_t dom ){
    
        slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits = 16;
    ulong     N = pw_covering_Nref(cov);
    arb_t radius;
    arb_init(radius);
     
    fprintf(f, "set size square\n");
//     fprintf(f, "set xrange[-10 : 10]\n");
//     fprintf(f, "set yrange[-10 : 10]\n");
    
    double minre=+INFINITY, maxre=-INFINITY, minim=+INFINITY, maxim=-INFINITY;
    pw_domain_gnuplot(f, &minre, &maxre, &minim, &maxim, dom);
    
    fprintf(f, "plot '-' title 'anulii circles' with circles lc rgb \"#008080\" fs transparent solid 0.0, \\\n");
    fprintf(f, "     '-' title 'first circle  ' with circles lc rgb \"#ff0000\" fs transparent solid 0.0,  \\\n");
    fprintf(f, "     '-' title 'last circle   ' with circles lc rgb \"#ff0000\" fs transparent solid 0.0,  \\\n");
    fprintf(f, "     '-' title 'angular sectors' with vectors nohead lc rgb \"#000000\" \n");
     
    for (ulong i=0; i< N-1; i++) {
        arb_set_si(radius, 2);
        arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+i, prec);
        fprintf(f, "0  0  ");
        arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
    ulong first=1, last=pw_covering_Nref(cov)-2;
    arb_t r, R, t;
    arb_init(r);
    arb_init(R);
    arb_init(t);
    pw_covering_get_first_last_annuli_domain( &first, &last, cov, dom, prec );
    if (first>0) {
        _pw_covering_get_inner_outer_radii_annulus( t, r, cov, first-1, prec );
    } else {
        arb_zero(r);
    }
    if (last < pw_covering_Nref(cov)-2) {
        _pw_covering_get_inner_outer_radii_annulus( R, t, cov, last+1, prec );
    } else {
        arb_zero(R);
    }
    
    fprintf(f, "0  0  ");
    arb_fprintn(f, r, nbdigits, ARB_STR_NO_RADIUS);
    fprintf(f, "\n");
    fprintf(f, "e\n");
    fprintf(f, "0  0  ");
    arb_fprintn(f, R, nbdigits, ARB_STR_NO_RADIUS);
    fprintf(f, "\n");
    fprintf(f, "e\n");
    
    arb_clear(t);
    arb_clear(R);
    arb_clear(r);
    
    for (ulong i=0; i< pw_covering_Nref(cov); i++) {
        _pw_covering_sectors_domain_gnuplot( f, cov, i, dom );
    }
    fprintf(f, "e\n");
    
    fprintf(f, "pause mouse close\n");
    arb_clear(radius);
    
}

void _pw_covering_with_point_gnuplot( FILE * f, pw_covering_t cov, const acb_t point ){
    
    slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits = 16;
    ulong     N = pw_covering_Nref(cov);
    arb_t radius;
    arb_init(radius);
    
    arb_set_si(radius, 2);
    arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+(N-2), prec);
    double maxre = arf_get_d( arb_midref(radius), ARF_RND_NEAR );
    double minre=-maxre, minim=-maxre, maxim=maxre; 
    double offsetre = PWPOLY_MAX((maxre-minre)*0.1, .1);
    double offsetim = PWPOLY_MAX((maxim-minim)*0.1, .1);
    fprintf(f, "set xrange[%f:%f]\n", minre-offsetre, maxre+offsetre);
    fprintf(f, "set yrange[%f:%f]\n", minim-offsetim, maxim+offsetim);
    fprintf(f, "set size ratio -1\n");
     
    fprintf(f, "plot '-' title 'anulii circles' with circles lc rgb \"#008080\" fs transparent solid 0.0, \\\n");
//     fprintf(f, "     '-' title 'last circle   ' with circles lc rgb \"#ff0000\" fs transparent solid 0.0,  \\\n");
    fprintf(f, "     '-' title 'point' with xyerrorbars lc rgb \"#000000\", \\\n");
//     fprintf(f, "     '-' title 'sectors' with circles lc rgb \"#000000\" fs transparent solid 0.0  \n");
    fprintf(f, "     '-' title 'sectors' with vectors nohead lc rgb \"#000000\" \n");
     
    for (ulong i=0; i< N-1; i++) {
        arb_set_si(radius, 2);
        arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+i, prec);
        fprintf(f, "0  0  ");
        arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
//     arb_set_si(radius, 2);
//     arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+(N-1), prec);
//     fprintf(f, "0  0  ");
//     arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
//     fprintf(f, "\n");
//     fprintf(f, "e\n");
    
    pw_acb_gnuplot(f, point, 16);
    fprintf(f, "\n");
    fprintf(f, "e\n");   
    
    pw_sector_list_t sectors;
    pw_sector_list_init(sectors);
    pw_covering_locate_point( sectors, 0, cov, point, prec );
    
    pw_sector_list_iterator it = pw_sector_list_begin( sectors );
    pw_sector_list_iterator itn;
    while( it!=pw_sector_list_end() ) {
        pw_sector_ptr sector = pw_sector_list_elmt(it);
        ulong n = pw_sector_nref(sector);
        ulong k = pw_sector_kref(sector);
        slong min=k;
        slong max=k;
        itn=pw_sector_list_next(it);
        while ( (itn!=pw_sector_list_end()) && (pw_sector_nref(pw_sector_list_elmt(itn))==n) ) {
            it = pw_sector_list_next(it);
            itn= pw_sector_list_next(it);
            max = pw_sector_kref(pw_sector_list_elmt(it));
        }
        _pw_covering_sectors_minmax_gnuplot( f, cov, n, min, max );
        it = pw_sector_list_next(it);
    }
    fprintf(f, "e\n");
    
    pw_sector_list_clear(sectors);
    
    fprintf(f, "pause mouse close\n");
    arb_clear(radius);
}

void _pw_covering_with_points_gnuplot( FILE * f, pw_covering_t cov, const acb_ptr points, slong nbPoints ){
    slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits = 16;
    ulong     N = pw_covering_Nref(cov);
    arb_t radius;
    arb_init(radius);
     
    arb_set_si(radius, 2);
    arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+(N-2), prec);
    double maxre = arf_get_d( arb_midref(radius), ARF_RND_NEAR );
    double minre=-maxre, minim=-maxre, maxim=maxre; 
    double offsetre = PWPOLY_MAX((maxre-minre)*0.1, .1);
    double offsetim = PWPOLY_MAX((maxim-minim)*0.1, .1);
    fprintf(f, "set xrange[%f:%f]\n", minre-offsetre, maxre+offsetre);
    fprintf(f, "set yrange[%f:%f]\n", minim-offsetim, maxim+offsetim);
    fprintf(f, "set size ratio -1\n");
    
    fprintf(f, "plot '-' title 'anulii circles' with circles lc rgb \"#008080\" fs transparent solid 0.0, \\\n");
//     fprintf(f, "     '-' title 'last circle   ' with circles lc rgb \"#ff0000\" fs transparent solid 0.0,  \\\n");
    fprintf(f, "     '-' title 'points' with xyerrorbars lc rgb \"#000000\"\n");
//     fprintf(f, "     '-' title 'sectors' with circles lc rgb \"#000000\" fs transparent solid 0.0  \n");
     
    for (ulong i=0; i< N-1; i++) {
        arb_set_si(radius, 2);
        arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+i, prec);
        fprintf(f, "0  0  ");
        arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
//     arb_set_si(radius, 2);
//     arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+(N-1), prec);
//     fprintf(f, "0  0  ");
//     arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
//     fprintf(f, "\n");
//     fprintf(f, "e\n");
    
    for (slong i=0; i<nbPoints; i++) {
        pw_acb_gnuplot(f, points+i, 16);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");   
    
//     ulong alloc_size=8;
//     ulong * ns = (ulong *) pwpoly_malloc( alloc_size*sizeof(ulong) );
//     slong * ks = (slong *) pwpoly_malloc( alloc_size*sizeof(slong) );
//     
//     ulong nbsect = _pw_covering_locate_point( &ns, &ks, alloc_size, cov, point, prec );
//     
//     for (ulong i=0; i< nbsect; i++) {
//         slong min=ks[i];
//         slong max=ks[i];
//         while ( (i<(nbsect-1))&&(ns[i+1]==ns[i]) ){
//             i++;
//             max = ks[i];
//         }
//         _pw_covering_sectors_minmax_gnuplot( f, cov, ns[i], min, max );
//     }
//     fprintf(f, "e\n");
//     
//     pwpoly_free(ns);
//     pwpoly_free(ks);
    
    fprintf(f, "pause mouse close\n");
    arb_clear(radius);
}

void _pw_covering_with_points_domain_gnuplot( FILE * f, pw_covering_t cov, const acb_ptr points, slong nbPoints, const domain_t dom ){
    slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits = 16;
    ulong     N = pw_covering_Nref(cov);
    arb_t radius;
    arb_init(radius);
     
    arb_set_si(radius, 2);
    arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+(N-2), prec);
    double maxre = arf_get_d( arb_midref(radius), ARF_RND_NEAR );
    double minre=-maxre, minim=-maxre, maxim=maxre; 
    pw_domain_gnuplot( f, &minre, &maxre, &minim, &maxim, dom );
    double offsetre = PWPOLY_MAX((maxre-minre)*0.1, .1);
    double offsetim = PWPOLY_MAX((maxim-minim)*0.1, .1);
    fprintf(f, "set xrange[%f:%f]\n", minre-offsetre, maxre+offsetre);
    fprintf(f, "set yrange[%f:%f]\n", minim-offsetim, maxim+offsetim);
    fprintf(f, "set size ratio -1\n");
    
    fprintf(f, "plot '-' title 'anulii circles' with circles lc rgb \"#008080\" fs transparent solid 0.0, \\\n");
//     fprintf(f, "     '-' title 'last circle   ' with circles lc rgb \"#ff0000\" fs transparent solid 0.0,  \\\n");
    fprintf(f, "     '-' title 'points' with xyerrorbars lc rgb \"#000000\"\n");
//     fprintf(f, "     '-' title 'sectors' with circles lc rgb \"#000000\" fs transparent solid 0.0  \n");
     
    for (ulong i=0; i< N-1; i++) {
        arb_set_si(radius, 2);
        arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+i, prec);
        fprintf(f, "0  0  ");
        arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
//     arb_set_si(radius, 2);
//     arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+(N-1), prec);
//     fprintf(f, "0  0  ");
//     arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
//     fprintf(f, "\n");
//     fprintf(f, "e\n");
    
    for (slong i=0; i<nbPoints; i++) {
        pw_acb_gnuplot(f, points+i, 16);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");   
    
//     ulong alloc_size=8;
//     ulong * ns = (ulong *) pwpoly_malloc( alloc_size*sizeof(ulong) );
//     slong * ks = (slong *) pwpoly_malloc( alloc_size*sizeof(slong) );
//     
//     ulong nbsect = _pw_covering_locate_point( &ns, &ks, alloc_size, cov, point, prec );
//     
//     for (ulong i=0; i< nbsect; i++) {
//         slong min=ks[i];
//         slong max=ks[i];
//         while ( (i<(nbsect-1))&&(ns[i+1]==ns[i]) ){
//             i++;
//             max = ks[i];
//         }
//         _pw_covering_sectors_minmax_gnuplot( f, cov, ns[i], min, max );
//     }
//     fprintf(f, "e\n");
//     
//     pwpoly_free(ns);
//     pwpoly_free(ks);
    
    fprintf(f, "pause mouse close\n");
    arb_clear(radius);
}

void _pw_covering_with_points_sectors_gnuplot( FILE * f, pw_covering_t cov, const acb_ptr points, slong nbPoints ){
    slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits = 16;
    ulong     N = pw_covering_Nref(cov);
    arb_t radius;
    arb_init(radius);
     
    fprintf(f, "set size square\n");
    fprintf(f, "set xrange[-10 : 10]\n");
    fprintf(f, "set yrange[-10 : 10]\n");
    fprintf(f, "plot '-' title 'anulii circles' with circles lc rgb \"#008080\" fs transparent solid 0.0, \\\n");
    fprintf(f, "     '-' title 'sectors' with vectors nohead lc rgb \"#000000\", \\\n");
    fprintf(f, "     '-' title 'points' with xyerrorbars lc rgb \"#000000\"\n");
     
    for (ulong i=0; i< N-1; i++) {
        arb_set_si(radius, 2);
        arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+i, prec);
        fprintf(f, "0  0  ");
        arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
    for (ulong i=0; i< N-1; i++) {
        _pw_covering_sectors_minmax_gnuplot( f, cov, i, 0, (pw_covering_nbsectref(cov)[i])-1 );
    }
    fprintf(f, "e\n");
    
    for (slong i=0; i<nbPoints; i++) {
        pw_acb_gnuplot(f, points+i, 16);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");   
    
//     ulong alloc_size=8;
//     ulong * ns = (ulong *) pwpoly_malloc( alloc_size*sizeof(ulong) );
//     slong * ks = (slong *) pwpoly_malloc( alloc_size*sizeof(slong) );
//     
//     ulong nbsect = _pw_covering_locate_point( &ns, &ks, alloc_size, cov, point, prec );
//     
//     for (ulong i=0; i< nbsect; i++) {
//         slong min=ks[i];
//         slong max=ks[i];
//         while ( (i<(nbsect-1))&&(ns[i+1]==ns[i]) ){
//             i++;
//             max = ks[i];
//         }
//         _pw_covering_sectors_minmax_gnuplot( f, cov, ns[i], min, max );
//     }
//     fprintf(f, "e\n");
//     
//     pwpoly_free(ns);
//     pwpoly_free(ks);
    
    fprintf(f, "pause mouse close\n");
    arb_clear(radius);
}

void _pw_covering_with_points2_gnuplot( FILE * f, pw_covering_t cov, const acb_ptr points, slong nbPoints
                                                                   , const acb_ptr points2, slong nbPoints2
){
    slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits = 16;
    ulong     N = pw_covering_Nref(cov);
    arb_t radius;
    arb_init(radius);
     
    fprintf(f, "set size square\n");
    fprintf(f, "set xrange[-10 : 10]\n");
    fprintf(f, "set yrange[-10 : 10]\n");
    fprintf(f, "plot '-' title 'anulii circles' with circles lc rgb \"#008080\" fs transparent solid 0.0, \\\n");
    fprintf(f, "     '-' title 'points' with xyerrorbars lc rgb \"#000000\", \\\n");
    fprintf(f, "     '-' title 'cadidates' with xyerrorbars lc rgb \"#ff0000\"\n");
     
    for (ulong i=0; i< N-1; i++) {
        arb_set_si(radius, 2);
        arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+i, prec);
        fprintf(f, "0  0  ");
        arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
//     arb_set_si(radius, 2);
//     arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+(N-1), prec);
//     fprintf(f, "0  0  ");
//     arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
//     fprintf(f, "\n");
//     fprintf(f, "e\n");
    
    for (slong i=0; i<nbPoints; i++) {
        pw_acb_gnuplot(f, points+i, 16);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n"); 
    
    for (slong i=0; i<nbPoints2; i++) {
        pw_acb_gnuplot(f, points2+i, 16);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
//     ulong alloc_size=8;
//     ulong * ns = (ulong *) pwpoly_malloc( alloc_size*sizeof(ulong) );
//     slong * ks = (slong *) pwpoly_malloc( alloc_size*sizeof(slong) );
//     
//     ulong nbsect = _pw_covering_locate_point( &ns, &ks, alloc_size, cov, point, prec );
//     
//     for (ulong i=0; i< nbsect; i++) {
//         slong min=ks[i];
//         slong max=ks[i];
//         while ( (i<(nbsect-1))&&(ns[i+1]==ns[i]) ){
//             i++;
//             max = ks[i];
//         }
//         _pw_covering_sectors_minmax_gnuplot( f, cov, ns[i], min, max );
//     }
//     fprintf(f, "e\n");
//     
//     pwpoly_free(ns);
//     pwpoly_free(ks);
    
    fprintf(f, "pause mouse close\n");
    arb_clear(radius);
}

void _pw_covering_with_roots_gnuplot( FILE * f, pw_covering_t cov, double rre[], double rim[], double err[] ){
    slong prec = PWPOLY_DEFAULT_PREC;
    int nbdigits = 16;
    ulong     N = pw_covering_Nref(cov);
    ulong   deg = pw_covering_degreeref(cov);
    arb_t radius;
    arb_init(radius);
     
    fprintf(f, "set size square\n");
    fprintf(f, "set xrange[-10 : 10]\n");
    fprintf(f, "set yrange[-10 : 10]\n");
    fprintf(f, "plot '-' title 'anulii circles' with circles lc rgb \"#008080\" fs transparent solid 0.0, \\\n");
    fprintf(f, "     '-' title 'last circle   ' with circles lc rgb \"#ff0000\" fs transparent solid 0.0, \\\n");
    fprintf(f, "     '-' title 'approximated roots' with xyerrorbars lc rgb \"#000000\" \n");
     
    for (ulong i=0; i< N-1; i++) {
        arb_set_si(radius, 2);
        arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+i, prec);
        fprintf(f, "0  0  ");
        arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
    arb_set_si(radius, 2);
    arb_pow_fmpq(radius, radius, pw_covering_bigradref(cov)+(N-1), prec);
    fprintf(f, "0  0  ");
    arb_fprintn(f, radius, nbdigits, ARB_STR_NO_RADIUS);
    fprintf(f, "\n");
    fprintf(f, "e\n");
    
    for (ulong i=0; i< deg; i++) {
        fprintf(f, "%lf  %lf  %lf  %lf\n", rre[i], rim[i], err[i], err[i]);
    }
    
    fprintf(f, "e\n");
    fprintf(f, "pause mouse close\n");
    arb_clear(radius);
}

void slong_fmpq_gnuplot(FILE * f, const ulong x, const fmpq_t y) {
    fprintf(f, "%lu   %lf", x, fmpq_get_d(y));
}

void CH_draw_edge( FILE* f, const ulong i, const fmpq_ptr ys, const ulong CH[]){
    fprintf(f, "%lu   %lf", CH[i-1], fmpq_get_d(ys + CH[i-1]) );
    fprintf(f, "   %lu   %lf", CH[i], fmpq_get_d(ys + CH[i]));
}

void _pw_covering_draw_line_from_slope( FILE* f, const fmpq_t slope, const fmpq_t ys, const fmpq_ptr slopes, const ulong len ){
    
    /* find the point of the CH the line passes through */
    ulong ind = _pw_covering_fmpq_searchsorted( slope, slopes, len-1 );
//     printf("ind: %lu\n", ind);
    fmpq_t left, right;
    fmpq_init(left);
    fmpq_init(right);
    /* leftmost point */
    fmpq_mul_si( left, slope, -(slong)ind);
    fmpq_add(left, left, ys+ind);
    /* rightmost point */
    fmpq_mul_si( right, slope, len-(slong)ind);
    fmpq_add(right, right, ys+ind);
    
    fprintf(f, "%lu   %lf",    (ulong) 0, fmpq_get_d(left) );
    fprintf(f, "   %lu   %lf", len,       fmpq_get_d(right));
    
    fmpq_clear(left);
    fmpq_clear(right);
}

void _pw_covering_fmpq_convex_hull_gnuplot( FILE * f, const ulong CH[], const ulong lenCH, const fmpq_ptr ys, const ulong len ) {
    fprintf(f, "# output for GNUPLOT\n#open it with gnuplot!\n");
    fprintf(f, "set pointsize 1\n");
    
    fprintf(f, "plot '-' title '-log(weights)' pt 2 lc rgb \"#008080\", \\\n");
    fprintf(f, "     '-' using 1:2:($3-$1):($4-$2) title 'CH' with vectors nohead lc rgb \"#ff0000\"\n");
    
    for(ulong i=0; i < len; i++) {
        slong_fmpq_gnuplot(f, i, ys+i);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    for(ulong i=1; i < lenCH; i++) {
        CH_draw_edge( f, i, ys, CH);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
    fprintf(f, "pause mouse close\n");
}

void _pw_covering_fmpq_convex_hull_annulii_gnuplot( FILE * f, const ulong CH[], const ulong lenCH, 
                                                    const fmpq_ptr ss, 
                                                    const fmpq_ptr ys, const ulong len,
                                                    const fmpq_t s1, const fmpq_t s2, ulong k1, ulong k2,
                                                    const fmpq_ptr highslopes, const fmpq_ptr lowslopes) {
    fprintf(f, "# output for GNUPLOT\n#open it with gnuplot!\n");
    fprintf(f, "set pointsize 1\n");
    
    fprintf(f, "plot '-' title '-log(weights)' pt 2 lc rgb \"#008080\", \\\n");
    fprintf(f, "     '-' using 1:2:($3-$1):($4-$2) title 'CH' with vectors nohead lc rgb \"#ff0000\", \\\n");
    fprintf(f, "     '-' title '-log(weights)-m' pt 2 lc rgb \"#ff0000\", \\\n");
    fprintf(f, "     '-' using 1:2:($3-$1):($4-$2) title 'annulus slopes' with vectors nohead lc rgb \"#008080\", \\\n");
    fprintf(f, "     '-' using 1:2:($3-$1):($4-$2) title 'highlow slopes' with vectors nohead lc rgb \"#000000\"\n");
    
    for(ulong i=0; i < len; i++) {
        slong_fmpq_gnuplot(f, i, ys+i);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    for(ulong i=1; i < lenCH; i++) {
        CH_draw_edge( f, i, ys, CH);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    for(ulong i=0; i < len; i++) {
        slong_fmpq_gnuplot(f, i, ss+i);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
    /*compute the middleslopes*/
    fmpq_ptr slopes = _fmpq_vec_init( len );
    _pw_covering_fmpq_middleslopes( slopes, ss, len);
    _pw_covering_draw_line_from_slope( f, s1, ys, slopes, len );
    fprintf(f, "\n");
    _pw_covering_draw_line_from_slope( f, s2, ys, slopes, len );
    fprintf(f, "\n");
    fprintf(f, "e\n");
    
    _pw_covering_draw_line_from_slope( f, lowslopes+k1, ys, slopes, len );
    fprintf(f, "\n");
    _pw_covering_draw_line_from_slope( f, highslopes+k2, ys, slopes, len );
    fprintf(f, "\n");
    fprintf(f, "e\n");
    
    
    fprintf(f, "pause mouse close\n");
    
    _fmpq_vec_clear(slopes, len);
}

void _pw_covering_fmpq_convex_hull_firstannulus_gnuplot( FILE * f, const ulong CH[], const ulong lenCH, 
                                                         const fmpq_ptr ss, 
                                                         const fmpq_ptr ys, const ulong len,
                                                         const fmpq_t s, ulong k,
                                                         const fmpq_ptr highslopes, const fmpq_ptr lowslopes) {
    fprintf(f, "# output for GNUPLOT\n#open it with gnuplot!\n");
    fprintf(f, "set pointsize 1\n");
    
    fprintf(f, "plot '-' title '-log(weights)' pt 2 lc rgb \"#008080\", \\\n");
    fprintf(f, "     '-' using 1:2:($3-$1):($4-$2) title 'CH' with vectors nohead lc rgb \"#ff0000\", \\\n");
    fprintf(f, "     '-' title '-log(weights)-m' pt 2 lc rgb \"#ff0000\", \\\n");
    fprintf(f, "     '-' using 1:2:($3-$1):($4-$2) title 'annulus slope' with vectors nohead lc rgb \"#008080\", \\\n");
    fprintf(f, "     '-' using 1:2:($3-$1):($4-$2) title 'highlow slope' with vectors nohead lc rgb \"#000000\"\n");
    
    for(ulong i=0; i < len; i++) {
        slong_fmpq_gnuplot(f, i, ys+i);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    for(ulong i=1; i < lenCH; i++) {
        CH_draw_edge( f, i, ys, CH);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    for(ulong i=0; i < len; i++) {
        slong_fmpq_gnuplot(f, i, ss+i);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
    /*compute the middleslopes*/
    fmpq_ptr slopes = _fmpq_vec_init( len );
    _pw_covering_fmpq_middleslopes( slopes, ss, len);
    _pw_covering_draw_line_from_slope( f, s, ys, slopes, len );
    fprintf(f, "\n");
    fprintf(f, "e\n");
    
    _pw_covering_draw_line_from_slope( f, highslopes+k, ys, slopes, len );
    fprintf(f, "\n");
    fprintf(f, "e\n");
    
    
    fprintf(f, "pause mouse close\n");
    
    _fmpq_vec_clear(slopes, len);
}

void _pw_covering_fmpq_convex_hull_lastannulus_gnuplot( FILE * f, const ulong CH[], const ulong lenCH, 
                                                         const fmpq_ptr ss, 
                                                         const fmpq_ptr ys, const ulong len,
                                                         const fmpq_t s, ulong k,
                                                         const fmpq_ptr highslopes, const fmpq_ptr lowslopes) {
    fprintf(f, "# output for GNUPLOT\n#open it with gnuplot!\n");
    fprintf(f, "set pointsize 1\n");
    
    fprintf(f, "plot '-' title '-log(weights)' pt 2 lc rgb \"#008080\", \\\n");
    fprintf(f, "     '-' using 1:2:($3-$1):($4-$2) title 'CH' with vectors nohead lc rgb \"#ff0000\", \\\n");
    fprintf(f, "     '-' title '-log(weights)-m' pt 2 lc rgb \"#ff0000\" \n");
//     fprintf(f, "     '-' using 1:2:($3-$1):($4-$2) title 'annulus slope' with vectors nohead lc rgb \"#008080\", \\\n");
//     fprintf(f, "     '-' using 1:2:($3-$1):($4-$2) title 'highlow slope' with vectors nohead lc rgb \"#000000\"\n");
    
    for(ulong i=0; i < len; i++) {
        slong_fmpq_gnuplot(f, i, ys+i);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    for(ulong i=1; i < lenCH; i++) {
        CH_draw_edge( f, i, ys, CH);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    for(ulong i=0; i < len; i++) {
        slong_fmpq_gnuplot(f, i, ss+i);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n");
    
    /*compute the middleslopes*/
//     fmpq_ptr slopes = _fmpq_vec_init( len );
//     _pw_covering_fmpq_middleslopes( slopes, ss, len);
//     _pw_covering_draw_line_from_slope( f, s, ys, slopes, len );
//     fprintf(f, "\n");
//     fprintf(f, "e\n");
//     
//     _pw_covering_draw_line_from_slope( f, lowslopes+k, ys, slopes, len );
//     fprintf(f, "\n");
//     fprintf(f, "e\n");
    
    
    fprintf(f, "pause mouse close\n");
    
//     _fmpq_vec_clear(slopes, len);
}

/* assume cov is initialized and computed */
void _pw_covering_compute_annulii_from_fmpq_weights_gnuplot( FILE * f, ulong annulus_ind, const pw_covering_t cov, const fmpq_ptr weights ){
    
    ulong len  = pw_covering_degreeref(cov)+1;
    ulong m    = pw_covering_mref(cov);
    ulong cm   = pw_covering_cref(cov)*m;
    
    if ((cm > pw_covering_degreeref(cov))||(annulus_ind>=pw_covering_Nref(cov)-1)) {
        
    }
    
    fmpq_ptr mweights   = _fmpq_vec_init( len );
    fmpq_ptr sweights   = _fmpq_vec_init( len );
    fmpq_ptr highslopes = _fmpq_vec_init( len );
    fmpq_ptr lowslopes  = _fmpq_vec_init( len );
    /* compute -weights */
    for (ulong i = 0; i<len; i++)
        fmpq_neg(mweights+i, weights+i );
        
    /* compute the lower part convex hull of -weights */
    ulong *CH = (ulong *) pwpoly_malloc ( len*sizeof(ulong) );
    ulong lenCH = _pw_covering_fmpq_convex_hull( CH, mweights, len );
    /* let sweights be the point of mweights in convex position */
    _pw_covering_fmpq_snap_convex_hull( sweights, mweights, CH, lenCH);
    /* shift the snapped convex hull by -m towards -inf */
    for (ulong i=0; i<len; i++) {
        fmpq_sub_si(sweights+i, sweights+i, m);
    }
    /* compute the vectors of highslopes and lowslopes */
    _pw_covering_fmpq_highslopes( highslopes, sweights, mweights, len, CH );
    _pw_covering_fmpq_lowslopes(   lowslopes, sweights, mweights, len, CH, lenCH );
    
    if ((cm > pw_covering_degreeref(cov))||(annulus_ind>pw_covering_Nref(cov)-1)) {
        _pw_covering_fmpq_convex_hull_gnuplot( f, CH, lenCH, mweights, len );
    } else {
        if (annulus_ind==0)
            _pw_covering_fmpq_convex_hull_firstannulus_gnuplot( f, CH, lenCH, sweights, mweights, len,
                                                                pw_covering_bigradref(cov)+annulus_ind, pw_covering_indhigref(cov)[annulus_ind],
                                                                highslopes, lowslopes);
        else if (annulus_ind==pw_covering_Nref(cov)-1)
            _pw_covering_fmpq_convex_hull_lastannulus_gnuplot( f, CH, lenCH, sweights, mweights, len,
                                                               pw_covering_bigradref(cov)+annulus_ind, pw_covering_indlowref(cov)[annulus_ind],
                                                               highslopes, lowslopes);
        else
            _pw_covering_fmpq_convex_hull_annulii_gnuplot( f, CH, lenCH, sweights, mweights, len,
                                                           pw_covering_bigradref(cov)+(annulus_ind-1), pw_covering_bigradref(cov) +annulus_ind,
                                                           pw_covering_indlowref(cov)[annulus_ind], pw_covering_indhigref(cov)[annulus_ind],
                                                           highslopes, lowslopes);
    }
//     int cont = _pw_covering_first_annulus( cov, lowslopes, highslopes );
//     while (cont)
//         cont = _pw_covering_next_annulus( cov, lowslopes, highslopes );
//     _pw_covering_last_annulus( cov, lowslopes, highslopes );
    
    pwpoly_free(CH);
    _fmpq_vec_clear( mweights, len );
    _fmpq_vec_clear( sweights, len );
    _fmpq_vec_clear( highslopes, len );
    _fmpq_vec_clear( lowslopes, len );
}

#endif

// void _pw_covering_sectors_gnuplot(  FILE * f, pw_covering_t cov, ulong i ) {
//     
//     ulong m    = pw_covering_mref(cov);
//     ulong cm   = pw_covering_cref(cov)*m;
//     
// //     printf("i: %lu, k-: %lu, k+: %lu\n", i, pw_covering_indlowref(cov)[i], pw_covering_indhigref(cov)[i] );
//     if ( pw_covering_indhigref(cov)[i] - pw_covering_indlowref(cov)[i] <= cm ) {
//         return; 
//     }
//     
//     slong prec = PWPOLY_DEFAULT_PREC;
//     int nbdigits=16;
//     
//     ulong nbdisks = pw_covering_nbsectref(cov)[i];
//     acb_ptr ro = _acb_vec_init( nbdisks );
//     _acb_vec_unit_roots(ro, nbdisks, nbdisks, prec);
//     
//     arb_t R,r, gamma, rho;
//     arb_init(R);
//     arb_init(r);
//     arb_init(gamma);
//     arb_init(rho);
//     arb_set_si(R, 2);
//     arb_pow_fmpq(R, R, pw_covering_bigradref(cov)+i, prec);
//     arb_set_si(r, 2);
//     arb_pow_fmpq(r, r, pw_covering_bigradref(cov)+(i-1), prec);
//     arb_add(gamma, R, r, prec);
//     arb_div_si(gamma, gamma, 2, prec);
//     arb_sub(rho, R, r, prec);
//     arb_div_si(rho, rho, 2, prec);
//     arb_mul_fmpz(r, rho, fmpq_numref(pw_covering_scaleref(cov)), prec); /*r:=scale*rho*/
//     arb_div_fmpz(r, r,   fmpq_denref(pw_covering_scaleref(cov)), prec); /*r:=scale*rho*/
//     
//     for (ulong j = 0; j<nbdisks; j++) {
//         acb_mul_arb(ro+j, ro+j, gamma, prec);
//         arb_fprintn(f, acb_realref(ro+j), nbdigits, ARB_STR_NO_RADIUS);
//         fprintf(f, "   ");
//         arb_fprintn(f, acb_imagref(ro+j), nbdigits, ARB_STR_NO_RADIUS);
//         fprintf(f, "   ");
//         arb_fprintn(f, r, nbdigits, ARB_STR_NO_RADIUS);
//         fprintf(f, "\n");
//     }
//     
//     arb_clear(R);
//     arb_clear(r);
//     arb_clear(gamma);
//     arb_clear(rho);
//     _acb_vec_clear( ro, nbdisks );
// }

// void _pw_covering_sectors_minmax_gnuplot(  FILE * f, pw_covering_t cov, ulong i, slong min, slong max ) {
//     
//     ulong m    = pw_covering_mref(cov);
//     ulong cm   = pw_covering_cref(cov)*m;
//     
//     if ( pw_covering_indhigref(cov)[i] - pw_covering_indlowref(cov)[i] <=cm ) {
//         return; 
//     }
//     
//     slong prec = PWPOLY_DEFAULT_PREC;
//     int nbdigits=16;
//     
//     ulong nbdisks = pw_covering_nbsectref(cov)[i];
//     acb_ptr ro = _acb_vec_init( nbdisks );
//     _acb_vec_unit_roots(ro, nbdisks, nbdisks, prec);
//     
//     arb_t R,r, gamma, rho;
//     arb_init(R);
//     arb_init(r);
//     arb_init(gamma);
//     arb_init(rho);
//     arb_set_si(R, 2);
//     arb_pow_fmpq(R, R, pw_covering_bigradref(cov)+i, prec);
//     arb_set_si(r, 2);
//     arb_pow_fmpq(r, r, pw_covering_bigradref(cov)+(i-1), prec);
//     arb_add(gamma, R, r, prec);
//     arb_div_si(gamma, gamma, 2, prec);
//     arb_sub(rho, R, r, prec);
//     arb_div_si(rho, rho, 2, prec);
//     arb_mul_fmpz(r, rho, fmpq_numref(pw_covering_scaleref(cov)), prec); /*r:=scale*rho*/
//     arb_div_fmpz(r, r,   fmpq_denref(pw_covering_scaleref(cov)), prec); /*r:=scale*rho*/
//     
//     for (slong j = min; j<=max; j++) {
//         slong jm = j%nbdisks;
//         acb_mul_arb(ro+jm, ro+jm, gamma, prec);
//         arb_fprintn(f, acb_realref(ro+jm), nbdigits, ARB_STR_NO_RADIUS);
//         fprintf(f, "   ");
//         arb_fprintn(f, acb_imagref(ro+jm), nbdigits, ARB_STR_NO_RADIUS);
//         fprintf(f, "   ");
//         arb_fprintn(f, r, nbdigits, ARB_STR_NO_RADIUS);
//         fprintf(f, "\n");
//     }
//     
//     arb_clear(R);
//     arb_clear(r);
//     arb_clear(gamma);
//     arb_clear(rho);
//     _acb_vec_clear( ro, nbdisks );
// }

/* OLD */
// ulong _pw_covering_locate_point_old( ulong** ns, slong** ks, ulong * alloc_size, const pw_covering_t cov, const acb_t point, slong prec, int verbose ){
//     
// #ifdef PW_PROFILE
//     clock_t start = clock();
// #endif
//     
//     int level = 5;
//     ulong res = 0;
//     
//     arb_t abs_point, ang_point, Pi, temp;
//     arf_t l, u;
//     arb_init(abs_point);
//     arb_init(ang_point);
//     arb_init(Pi);
//     arb_init(temp);
//     arf_init(l);
//     arf_init(u);
//     
//     arb_const_pi(Pi, prec);
//     acb_abs( abs_point, point, prec );
//     
//     acb_arg( ang_point, point, prec );
//     if ( arb_contains_zero(acb_imagref(point)) ) {
//         if ( arb_is_negative( acb_realref(point) ) ){
//             acb_t mpoint;
//             acb_init(mpoint);
//             acb_neg(mpoint, point);
//             acb_arg( ang_point, mpoint, prec );
//             arb_add( ang_point, ang_point, Pi, prec );
//             acb_clear(mpoint);
//         }
//     } 
//     
//     ulong N = pw_covering_Nref(cov);
//     /* let pw_covering_app_bigradref(cov)+(-1) = -inf */
//     /*     pw_covering_app_bigradref(cov)+(N-1) = +inf */
//     /* find the highest index  0<=low<=N-1 s.t. pw_covering_app_bigradref(cov)+(low-1) < abs_point */
//     /*  and the lowest  index  0<=hig<=N-1 s.t. abs_point < pw_covering_app_bigradref(cov)+hig */
//     ulong low = 0, hig = N-1;
//     while ( ( low<(N-1) )&&(arb_lt ( pw_covering_app_bigradref(cov) + low, abs_point)) )
//         low++;
//     while ( ( hig>0 )    &&(arb_gt ( pw_covering_app_bigradref(cov) + (hig-1), abs_point)) )
//         hig--;
//     
//     if (verbose>=level) {
//         printf(" point (modulus: "); arb_printd(abs_point, 10); 
//         printf(", argument: "); arb_printd(ang_point, 10); printf("), low: %lu, hig %lu \n", low, hig);
//     }
//     
//     for (ulong n=low; n<=hig; n++) {
//         ulong K = pw_covering_nbsectref(cov)[n];
//         if (verbose>=level)
//             printf("sectors in %lu-th annulus (%lu sectors): ", n, K);
//         if (K==1) {
//             if (verbose>=level)
//                 printf("0");
//             if (res>=(*alloc_size)){
//                 (*alloc_size)*=2;
//                 *ns = (ulong *) pwpoly_realloc( *ns, (*alloc_size)*sizeof(ulong) );
//                 *ks = (slong *) pwpoly_realloc( *ks, (*alloc_size)*sizeof(slong) );
//             }
//             (*ns)[res]=n;
//             (*ks)[res]=0;
//             res++;
//         } else {
//             /* find the highest k between 0 and K-1 such that 2Pi(2k-1)/(2K) < ang_point */
//             /*                                       <=> k < ang_point*K/(2Pi) + 1/2     */
//             arb_mul_ui( temp, ang_point, K, prec );
//             arb_div( temp, temp, Pi, prec );
//             arb_add_si (temp, temp, 1, prec);
//             arb_div_si (temp, temp, 2, prec);
//             arb_get_interval_arf(l, u, temp, prec);
//             slong lk = arf_get_si(l, ARF_RND_FLOOR);
//             slong uk = arf_get_si(u, ARF_RND_CEIL);
//             if (verbose>=level) {
//                 arb_printd(temp, 10); 
//                 printf(" ( "); arf_printd(l, 10); printf(", %ld, ", lk); arf_printd(u, 10); printf(", %ld)", uk);
//                 printf("\n");
//             }
//             for (slong k = lk; k<uk; k++){
//                 if (res>=(*alloc_size)){
//                     (*alloc_size)*=2;
//                     *ns = (ulong *) pwpoly_realloc( *ns, (*alloc_size)*sizeof(ulong) );
//                     *ks = (slong *) pwpoly_realloc( *ks, (*alloc_size)*sizeof(slong) );
//                 }
//                 (*ns)[res]=n;
//                 (*ks)[res]=k;
//                 res++;
//             }
//         }
// //         printf("\n");
//     }
//     
//     arb_clear(abs_point);
//     arb_clear(ang_point);
//     arb_clear(Pi);
//     arb_clear(temp);
//     arf_clear(l);
//     arf_clear(u);
//     
// #ifdef PW_PROFILE
//     clicks_in_locate += (clock() - start);
// #endif 
//     return res;
// }
