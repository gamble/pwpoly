/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_SECTOR_LIST_H
#define PW_SECTOR_LIST_H

#include "pw_base.h"
#include "list/pw_list.h"

#ifdef __cplusplus
extern "C" {
#endif
    
struct pw_sector{
    ulong _n;
    ulong _k;
};

typedef struct pw_sector pw_sector_t[1];
typedef struct pw_sector * pw_sector_ptr;

#define pw_sector_nref(X) ( (X)->_n )
#define pw_sector_kref(X) ( (X)->_k )

PWPOLY_INLINE void _pw_sector_set( pw_sector_t res, ulong n, slong k, ulong K ) {
    pw_sector_nref(res) = n;
    k = k%K;
    if (k<0)
        k+=K;
    pw_sector_kref(res) = k;
}
/* assume 0<=n<N, 0<K */
/* create the sector (n,k') where 0<=k'<K and k' = k%K */
PWPOLY_INLINE pw_sector_ptr _pw_sector_create( ulong n, slong k, ulong K ) {
    pw_sector_ptr res = (pw_sector_ptr) pwpoly_malloc ( sizeof(struct pw_sector) );
    _pw_sector_set( res, n, k, K );
    return res;
}

PWPOLY_INLINE int _pw_sector_isless(const pw_sector_t s1, const pw_sector_t s2) {
    if ( pw_sector_nref(s1) == pw_sector_nref(s2) )
        return pw_sector_kref(s1) < pw_sector_kref(s2);
    else
        return pw_sector_nref(s1) < pw_sector_nref(s2);
}
#ifndef PW_SILENT
PWPOLY_INLINE void  _pw_sector_fprint(FILE * file, const pw_sector_t s){
    fprintf(file, "< %lu, %lu >", pw_sector_nref(s), pw_sector_kref(s) );
}
#endif
PWPOLY_INLINE void _pw_sector_clear_for_list(void * b){
}

PWPOLY_INLINE int _pw_sector_isless_for_list(const void * b1, const void * b2){
    return _pw_sector_isless( (pw_sector_ptr) b1, (pw_sector_ptr) b2 );
}
#ifndef PW_SILENT
PWPOLY_INLINE void _pw_sector_fprint_for_list(FILE * file, const void * b){
    _pw_sector_fprint(file, (pw_sector_ptr) b);
}
#endif
typedef struct list   pw_sector_list;
typedef struct list   pw_sector_list_t[1];
typedef struct list * pw_sector_list_ptr;

PWPOLY_INLINE void          pw_sector_list_init ( pw_sector_list_t l )                       { list_init(l); }
PWPOLY_INLINE void          pw_sector_list_swap ( pw_sector_list_t l1, pw_sector_list_t l2 ) { list_swap(l1, l2); }
PWPOLY_INLINE void          pw_sector_list_clear( pw_sector_list_t l )                       { list_clear(l, _pw_sector_clear_for_list); }
PWPOLY_INLINE pw_sector_ptr pw_sector_list_first( pw_sector_list_t l )                       { return (pw_sector_ptr) list_first(l); }
PWPOLY_INLINE pw_sector_ptr pw_sector_list_last ( pw_sector_list_t l )                       { return (pw_sector_ptr) list_last(l); }
PWPOLY_INLINE pw_sector_ptr pw_sector_list_pop  ( pw_sector_list_t l )                       { return (pw_sector_ptr) list_pop(l); }
PWPOLY_INLINE int           pw_sector_list_insert_sorted_unique ( pw_sector_list_t l, ulong n, slong k, ulong K ) { 
    pw_sector_ptr s = _pw_sector_create( n, k, K );
    int res = list_insert_sorted_unique(l, s, _pw_sector_isless_for_list); 
    if (!res)
        pwpoly_free(s);
    return res;
}
PWPOLY_INLINE int           pw_sector_is_in_sorted_sector_list ( ulong n, slong k, ulong K, pw_sector_list_t l ) { 
    pw_sector_t s;
    _pw_sector_set( s, n, k, K );
    return list_sorted_unique_contains_data(l, s, _pw_sector_isless_for_list);
}
#ifndef PW_SILENT
PWPOLY_INLINE void    pw_sector_list_fprint( FILE * file, pw_sector_list_t l )   { list_fprint(file, l, _pw_sector_fprint_for_list); }
PWPOLY_INLINE void    pw_sector_list_print (              pw_sector_list_t l )   { pw_sector_list_fprint(stdout, l); }
#endif
PWPOLY_INLINE int     pw_sector_list_is_empty  ( pw_sector_list_t l )            { return list_is_empty(l); }
PWPOLY_INLINE slong   pw_sector_list_get_size  ( const pw_sector_list_t l )      { return list_get_size(l); }

/*iterator */
typedef list_iterator pw_sector_list_iterator;
PWPOLY_INLINE pw_sector_list_iterator pw_sector_list_begin(const pw_sector_list_t l)   { return list_begin(l); }
PWPOLY_INLINE pw_sector_list_iterator pw_sector_list_endEl(const pw_sector_list_t l)   { return list_endEl(l); }
PWPOLY_INLINE pw_sector_list_iterator pw_sector_list_next(pw_sector_list_iterator it)  { return list_next(it); }
PWPOLY_INLINE pw_sector_list_iterator pw_sector_list_prev(pw_sector_list_iterator it)  { return list_prev(it); }
PWPOLY_INLINE pw_sector_list_iterator pw_sector_list_end()                             { return list_end(); }
PWPOLY_INLINE pw_sector_ptr           pw_sector_list_elmt(pw_sector_list_iterator it)  { return (pw_sector_ptr) list_elmt(it); }

#ifdef __cplusplus
}
#endif

#endif
