/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_COVERING_H
#define PW_COVERING_H

#include "pw_base.h"

#ifndef PW_NO_INTERFACE
#include "common/pw_gnuplot.h"
#endif

#include "pw_sector_list.h"
#include "geometry/domain.h"

#ifdef __cplusplus
extern "C" {
#endif
    
/* Preconditions:  ys is a table of len fmpq                                               */
/*                 CH is an allocated table of len slong                                   */
/* Postconditions: computes the LOWER part of the convex hull of the set of (2D) points:   */
/*                     { (0, ys+0), (1, ys+1), ..., (len-1, ys + (len-1)) }                */
/*                 let { (h1, ys+h1), ..., (hl, ys+hl) }       be this convex hull         */
/*                 computes: CH:=[h1, h2, ..., hl ]                                        */
/*                 returns : l                                                             */
/* Implements Graham's algorithm                                                           */
ulong _pw_covering_fmpq_convex_hull( ulong * CH, const fmpq_ptr ys, const ulong len );

/* Pre conditions:  zs, ys are tables of leny fmpq                                                            */
/*                  CH=[h1, h2, ..., hlenCH] and { (h1, ys+h1), ..., (hl, ys+hlenCH) } is the             */
/*                  lower part of the convex hull of { (0, ys+0), (1, ys+1), ..., (len-1, ys + (leny-1) } */
/* Post conditions: for all 0<= i < leny, if i is not in CH, let j st hj < i < hj+1                    */
/*                  computes zs[i] as ys[i] moved on the segment [ (hj, ys+hj)), (hj+1, ys +(hj+1)) ]   */
/*                  otherwise (i is in CH) lets zs[i]=ys[i]                                             */
void _pw_covering_fmpq_snap_convex_hull( fmpq_ptr zs, const fmpq_ptr ys, const ulong * CH, const ulong lenCH);

/* Pre conditions:  slopes, spoints, points are tables of lenpoints fmpq's                                              */
/*                  CH=[h1, h2, ..., hlenCH] and { (h1, points+h1), ..., (hl, points+hlenCH) } is the                   */
/*                    lower part of the convex hull of { (0, points+0), (1, points+1), ..., (len-1, points + (leny-1) } */
/* Post conditions: for all 0<= i < lenpoints, let high(i) := min_{k} { k s.t. 0<=k and                                 */
/*                         (points[CH[k+1]] - points[CH[k]])/(CH[k+1]-CH[k]) >= (spoints[i] - points[CH[k]])/(i-CH[k]) }*/
/*                         and High[i]:= (spoints[i] - points[CH[high(i)]])/(i-CH[high(i)])                             */
/*                  computes for all 1 <= i < lenpoints slopes[i]= High[i]                                              */
/*                  lets slopes[0] = -1/0                                                                               */ 
void _pw_covering_fmpq_highslopes( fmpq_ptr slopes, fmpq_ptr spoints, fmpq_ptr points, ulong lenpoints, ulong * CH );

/* Pre conditions:  slopes, spoints, points are tables of lenpoints fmpq's                                               */
/*                  CH=[h1, h2, ..., hlenCH] and { (h1, points+h1), ..., (hl, points+hlenCH) } is the                    */
/*                    lower part of the convex hull of { (0, points+0), (1, points+1), ..., (len-1, points + (leny-1) }  */
/* Post conditions: for all 0<= i < lenpoints, let low(i) := max_{k} { k s.t. k<=d and                                   */
/*                         (points[CH[k]] - points[CH[k-1]])/(CH[k]-CH[k-1]) >= (points[CH[k]] - spoints[i] )/(CH[k]-i) }*/
/*                         and Low[i]:= (points[low(i)] - spoints[i] )/(low(i)-i)                                        */
/*                  computes for all 0 <= i < lenpoints-1 slopes[i]= Low[i]                                              */
/*                  lets slopes[lenpoints-1] = 1/0                                                                       */ 
void _pw_covering_fmpq_lowslopes( fmpq_ptr slopes, fmpq_ptr spoints, fmpq_ptr points, ulong lenpoints, ulong * CH, ulong lenCH );

/* Pre conditions:  slopes, spoints are tables of lenpoints fmpq's                                                       */
/* Post conditions: for all 0<= i < lenpoints-1, sets slopes[i] to the slope of the line                                 */
/*                         ( (i,spoints[i]), (i+1, spoints[i+1]) )                                                       */
/*                  let slopes[lenpoints-1] = 1/0 = +inf                                                                 */
/*                  if the points (i,spoints[i]) are in convex position, slopes is increasing                            */
void _pw_covering_fmpq_middleslopes( fmpq_ptr slopes, const fmpq_ptr spoints, const ulong lenpoints);

/* Pre conditions:  values is table of len (non-necessarily strictly) increasing fmpq's  */
/* Post conditions: returns the index l s.t. if l=0,   then value <= value[0]            */
/*                                           if l=len, then value[len-1] < value         */                                             
/*                                           if 0 < l < len, then                        */
/*                                               values[l-1] < value <= values[l]        */
ulong _pw_covering_fmpq_searchsorted( const fmpq_t value, const fmpq_ptr values, const ulong len );

/* Pre conditions:  values is table of len exact (non-necessarily strictly) increasing arb's  */
/*                  value=[lv,uv] is an arb                                                   */ 
/*                  0<= min < max <= len                                                      */
/* Post conditions: sets min to the highest index s.t. values[min-1] < lv <= values[min]      */
/*                       max to the highest index s.t. values[max-1] < uv <= values[max]      */
/*                       and min <= max                                                       */
/*                    if min = 0     then lv <= values[0]                                     */
/*                    if min = len   then values[len-1] < lv                                  */
/*                    if max = 0     then uv <= values[0]                                     */
/*                    if max = len   then values[len-1] < uv                                  */
void _pw_covering_arb_searchsorted( ulong *min, ulong *max, const arb_t value, const fmpq_ptr values, const ulong len, slong prec );

/* data structure for encoding the covering of the complex plane with a sequence of nested annulii */
#define PW_ALLOC_SIZE 128
typedef struct {
    
    ulong     _size_allocated;
    
    ulong     _degree;    /* the degree of the polynomial for which the covering  is computed */
    
    /* Parameters */
    ulong     _m;        /* the m   */
    ulong     _c;        /* first and last annulii are obtained by truncating input pol at length _c*_m */
    fmpq      _scale;    /* the scale factor */
    fmpq      _ratio;    /* the ratio factor */
    fmpq      _facto;    /* ratio/scale      */
    
    /* The covering */
    ulong     _N;         /* the number of annulii, including the first one and the last one   */
    ulong     _Ksum;      /* the total number of sectors */ 
    
    ulong *   _indlow;      /* inds of lowest degree monomials supporting the annulii                                   */
    ulong *   _indhig;      /* inds of highest degree monomials supporting the annulii                                  */
    ulong *   _nbsect;      /* the number of angular sectors in annulii                                                 */ 
    uint  *   _log2_nbsect; /* the log2 of the former                                                                   */
    ulong     _max_nbsect;  /* the max nb of sectors overall annulii, i.e. the max of _nbsect                           */
    fmpq_ptr  _bigrad;      /* log2 of outer radii of the N-1 first annulii                                             */
    ulong *   _inddom;      /* for annulus n, index of monomial supporting the slope _bigrad[n]                         */
    fmpq_ptr  _sl_at_ed;    /* table of _degree + 1 increasing fmpq's, the slopes at edges of the convex hull */
    slong     _app_prec;    /* either -1, in which case the tables below are not initialized,                           */
                            /* or the precision to which the approximations below are computed                          */
    arb_ptr   _app_bigrad;  /* approx of outer radii of the N-1 first annulii                                           */
    arb_ptr   _app_gammas;  /* approx of centers of the N-1 first annulii                                               */
    arb_ptr   _app_rhos;    /* approx of midradii of the N-1 first annulii                                              */
    acb_ptr   _app_unit_r;  /* approx of the _max_nbsect _max_nbsect-th order roots of unit, indexed in clockwise order */
} pw_covering;

typedef pw_covering pw_covering_t[1];
typedef pw_covering * pw_covering_ptr;

#define pw_covering_mref(X)      ( X->_m )
#define pw_covering_cref(X)      ( X->_c )
#define pw_covering_degreeref(X) ( X->_degree )
#define pw_covering_scaleref(X)  (&(X)->_scale )
#define pw_covering_ratioref(X)  (&(X)->_ratio )
#define pw_covering_factoref(X)  (&(X)->_facto )
#define pw_covering_Nref(X)      ( X->_N )
#define pw_covering_Ksumref(X)      ( X->_Ksum )
#define pw_covering_indlowref(X) ( X->_indlow )
#define pw_covering_indhigref(X) ( X->_indhig )
#define pw_covering_nbsectref(X) ( X->_nbsect )
#define pw_covering_log2_nbsectref(X) ( X->_log2_nbsect )
#define pw_covering_max_nbsectref(X) ( X->_max_nbsect )
#define pw_covering_app_precref(X) ( X->_app_prec )
#define pw_covering_bigradref(X) ( X->_bigrad )
#define pw_covering_inddomref(X) ( X->_inddom )
#define pw_covering_sl_at_edref(X) ( X->_sl_at_ed )
#define pw_covering_app_bigradref(X) ( X->_app_bigrad )
#define pw_covering_app_gammasref(X) ( X->_app_gammas )
#define pw_covering_app_rhosref(X)   ( X->_app_rhos )
#define pw_covering_app_unit_rref(X) ( X->_app_unit_r )

void pw_covering_init( pw_covering_t cov, const ulong m, const ulong len, const ulong c, const fmpq_t scale, const fmpq_t ratio );
void pw_covering_clear( pw_covering_t cov );

// void pw_covering_init_set( pw_covering_t dest, pw_covering_t src );

#ifndef PW_SILENT
void _pw_covering_fprint( FILE * f, const pw_covering_t cov );
PWPOLY_INLINE void _pw_covering_print( const pw_covering_t cov ){
  _pw_covering_fprint( stdout, cov );
}

void _pw_covering_fprint_short( FILE * f, const pw_covering_t cov );
PWPOLY_INLINE void _pw_covering_print_short( const pw_covering_t cov ){
  _pw_covering_fprint_short( stdout, cov );
}
#endif

int _pw_covering_first_annulus( pw_covering_t cov, const fmpq_ptr lowslopes, const fmpq_ptr highslopes );
int _pw_covering_next_annulus( pw_covering_t cov, const fmpq_ptr lowslopes, const fmpq_ptr highslopes );
void _pw_covering_last_annulus( pw_covering_t cov, const fmpq_ptr lowslopes, const fmpq_ptr highslopes );

void _pw_covering_compute_annulii_from_fmpq_weights( pw_covering_t cov, const fmpq_ptr weights );

void _pw_covering_compute_app_rads( pw_covering_t cov, slong prec );

// /* Pre conditions: */
// /* Post conditions returns 1 => sector's disks of n-th annulus have empty intersection with dom */
// int pw_covering_annulus_domain_empty_intersection( pw_covering_t cov, ulong n, const domain_t dom, slong prec );
void pw_covering_get_first_last_annuli_domain( ulong * first, ulong * last,
                                               pw_covering_t cov, const domain_t dom, slong prec );
int pw_covering_disk_domain_empty_intersection( pw_covering_t cov, ulong n, ulong k, const domain_t dom, slong prec );

/* Pre  conditions:                                                               */
/* Post conditions: if (_app_prec==-1) or (K > _max_nbsect) sets x to NaN + I*NaN */
/*                  else sets x to e^(I*2Pi*k/K)                                  */
void _pw_covering_get_root_of_unit( acb_t x, const pw_covering_t cov, slong k, ulong K );

/* Pre  conditions: around is the index of an annulus of cov                   */
/* Post conditions: sectors is a sorted list of sectors of cov with no doubles */
/*                  sectors contains the sectors intersected by point          */
/*                  returns the highest index low of annulus of cov            */
/*                          s.t. pw_covering_app_bigradref(cov)+(low-1) < |point| */
/*                  around is used as an indication for location of point in annulus */
/*                  the result is correct whatever correctness of around             */
ulong pw_covering_locate_point( pw_sector_list_t sectors, ulong around,
                                const pw_covering_t cov, const acb_t point, slong prec );

extern slong _pw_prec_for_compare;
/* Pre  conditions: points is an array of len acb's                            */
/* Post conditions: points is sorted by increasing modulii of midpoints        */
/*                  sectors is a sorted list of sectors of cov with no doubles */
/*                  sectors contains the sectors intersected by points         */
/*                  returns the number of sectors inserted in sectors          */
ulong pw_covering_locate_points( pw_sector_list_t sectors, const pw_covering_t cov, acb_ptr points, slong len, slong prec );

ulong pw_covering_add_neighbors( pw_sector_list_t sectors, const pw_covering_t cov, int realCoeffs );

/* Pre  conditions: point is finite (not nan nor infinite value), n<N                */
/* Post conditions: if returns 0 then point has no intersection with */
// int   _pw_covering_intersects_nk_sector( const pw_covering_t cov, const acb_t point, ulong n, ulong k, slong prec );

/* does not support aliazing */
/* Pre  conditions:                                                               */
/* Post conditions: if (_app_prec==-1) or (n >= N) sets y to NaN + I*NaN          */
/*                  if ( (n==0) || ( (n<N-1) && (K[n]==1) ) sets x to y           */
/*                  if (n==N-1) sets x to 1/y                                     */
/*                  else sets x to (1/(scale*rho))*(y*e^(-i2Pik/K)-gamma)         */
void _pw_covering_shift_point( acb_t x, const pw_covering_t cov, const ulong n, const slong k, const acb_t y, slong prec );

/* does not support aliazing */
/* Pre  conditions:                                                               */
/* Post conditions: if (_app_prec==-1) or (n >= N) sets y to NaN + I*NaN          */
/*                  if ( (n==0) || ( (n<N-1) && (K[n]==1) ) sets y to x           */
/*                  if (n==N-1) sets y to 1/x                                     */
/*                  else sets y to (gamma + scale*rho*x)e^(i2Pik/K)               */
void _pw_covering_shift_back_point( acb_t y, const pw_covering_t cov, const ulong n, const slong k, const acb_t x, slong prec );

#ifndef PW_NO_INTERFACE
void _pw_covering_fmpq_convex_hull_gnuplot( FILE * f, const ulong CH[], const ulong lenCH, const fmpq_ptr ys, const ulong len );
void _pw_covering_gnuplot( FILE * f, pw_covering_t cov );
void _pw_covering_with_roots_gnuplot( FILE * f, pw_covering_t cov, double rre[], double rim[], double err[] );
void _pw_covering_with_point_gnuplot( FILE * f, pw_covering_t cov, const acb_t point );
void _pw_covering_with_points_gnuplot( FILE * f, pw_covering_t cov, const acb_ptr points, slong nbPoints );
void _pw_covering_with_points2_gnuplot( FILE * f, pw_covering_t cov, const acb_ptr points, slong nbPoints
                                                                   , const acb_ptr points2, slong nbPoints2 );
void _pw_covering_with_points_sectors_gnuplot( FILE * f, pw_covering_t cov, const acb_ptr points, slong nbPoints );
void _pw_covering_compute_annulii_from_fmpq_weights_gnuplot( FILE * f, ulong annulus_ind, const pw_covering_t cov, const fmpq_ptr weights );
void _pw_covering_with_domain_gnuplot( FILE * f, pw_covering_t cov, const domain_t dom );
void _pw_covering_with_points_domain_gnuplot( FILE * f, pw_covering_t cov, const acb_ptr points, slong nbPoints, const domain_t dom );
#endif

/* OLD */
// ulong _pw_covering_locate_point_old( ulong** ns, slong** ks, ulong * alloc_size, const pw_covering_t cov, const acb_t point, slong prec, int verbose );

#ifdef __cplusplus
}
#endif

#endif
