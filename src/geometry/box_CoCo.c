/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "box_CoCo.h"

void box_CoCo_init(box_CoCo_t x) {
    box_list_init(box_CoCo_boxesref(x));
    fmpz_init(box_CoCo_depthref(x));
    fmpz_init(box_CoCo_infReref(x));
    fmpz_init(box_CoCo_supReref(x));
    fmpz_init(box_CoCo_infImref(x));
    fmpz_init(box_CoCo_supImref(x));
    fmpz_init(box_CoCo_QBRSpref(x));
    fmpz_set_si(box_CoCo_QBRSpref(x), 2);
    
    box_CoCo_Mprecref(x) = PWPOLY_DEFAULT_PREC;
    box_CoCo_nbRooref(x) = -1;
    box_CoCo_2_Sepref(x) = 0;
    box_CoCo_6_Sepref(x) = 0;
    box_CoCo_QBRSuref(x) = 0;
    
    box_CoCo_w_encref(x) = 0;
    box_CoCo_1_Sepref(x) = 1;
    box_CoCo_nbRIEref(x) = -1;
    acb_init(box_CoCo_encloref(x));
}

void box_CoCo_init_box(box_CoCo_t x, box_t b){
    box_CoCo_init(x);
    box_list_insert_sorted( box_CoCo_boxesref(x), b);
    fmpz_set( box_CoCo_depthref(x), box_depthref(b) );
    fmpz_set( box_CoCo_infReref(x), box_crealref(b) );
    fmpz_set( box_CoCo_supReref(x), box_crealref(b) );
    fmpz_set( box_CoCo_infImref(x), box_cimagref(b) );
    fmpz_set( box_CoCo_supImref(x), box_cimagref(b) );
    
    box_CoCo_Mprecref(x) = box_pprecref(b);
}

void box_CoCo_clear(box_CoCo_t x) {
    box_list_clear(box_CoCo_boxesref(x));
    fmpz_clear(box_CoCo_depthref(x));
    fmpz_clear(box_CoCo_infReref(x));
    fmpz_clear(box_CoCo_supReref(x));
    fmpz_clear(box_CoCo_infImref(x));
    fmpz_clear(box_CoCo_supImref(x));
    fmpz_clear(box_CoCo_QBRSpref(x));
    
    acb_clear(box_CoCo_encloref(x));
}

void box_CoCo_clear_light(box_CoCo_t x) {
    while ( ! box_list_is_empty(box_CoCo_boxesref(x)) )
        box_list_pop(box_CoCo_boxesref(x));
    box_list_clear(box_CoCo_boxesref(x));
    fmpz_clear(box_CoCo_depthref(x));
    fmpz_clear(box_CoCo_infReref(x));
    fmpz_clear(box_CoCo_supReref(x));
    fmpz_clear(box_CoCo_infImref(x));
    fmpz_clear(box_CoCo_supImref(x));
    fmpz_clear(box_CoCo_QBRSpref(x));
    
    acb_clear(box_CoCo_encloref(x));
}

void box_CoCo_init_set_from_acb_slong_int( box_CoCo_t cc, 
                                           const acb_ptr enclosure, slong nbRootsInEnclosure, 
                                           int is_1_Sep, int is_2_Sep, int is_6_Sep, slong prec ) {
    box_CoCo_init(cc);
    box_list_t boxes;
    box_list_init(boxes);
    
    if (acb_is_exact (enclosure) ) {
        /* create a cc with empty list of component boxes */
        fmpz_set_si( box_CoCo_depthref(cc), COEFF_MIN );
        box_CoCo_nbRooref(cc) = nbRootsInEnclosure;
        box_CoCo_Mprecref(cc) = prec;
        box_CoCo_2_Sepref(cc) = 1;
        box_CoCo_6_Sepref(cc) = 1;
        
        box_CoCo_w_encref(cc) = 1;
        box_CoCo_1_Sepref(cc) = 1;
        box_CoCo_nbRIEref(cc) = nbRootsInEnclosure;
        acb_set(box_CoCo_encloref(cc), enclosure);
    } else {
        box_get_box_list_acb( boxes, enclosure );
        while ( !box_list_is_empty(boxes) )
            box_CoCo_insert_box(cc, box_list_pop(boxes));
        if (is_1_Sep)
            box_CoCo_nbRooref(cc) = nbRootsInEnclosure;
        box_CoCo_Mprecref(cc) = prec;
//         box_CoCo_Qprecref(cc) = PWPOLY_DEFAULT_PREC;
        box_CoCo_2_Sepref(cc) = is_2_Sep;
        box_CoCo_6_Sepref(cc) = is_6_Sep;
        
        box_CoCo_w_encref(cc) = 1;
//         box_CoCo_w_encref(cc) = 0;
        box_CoCo_1_Sepref(cc) = is_1_Sep;
        box_CoCo_nbRIEref(cc) = nbRootsInEnclosure;
        acb_set(box_CoCo_encloref(cc), enclosure);
        
    }
    box_list_clear(boxes);
}

void box_CoCo_init_set_from_enclosure ( box_CoCo_t cc, 
                                        const acb_ptr enclosure, slong nbRootsInEnclosure, 
                                        int is_1_Sep, int is_2_Sep, int is_6_Sep, slong prec ) {
    box_CoCo_init(cc);
    
    /* create a cc with empty list of component boxes */
    fmpz_set_si( box_CoCo_depthref(cc), COEFF_MIN );
    box_CoCo_nbRooref(cc) = nbRootsInEnclosure;
    box_CoCo_Mprecref(cc) = prec;
    box_CoCo_2_Sepref(cc) = is_2_Sep;
    box_CoCo_6_Sepref(cc) = is_6_Sep;
    
    box_CoCo_w_encref(cc) = 1;
    box_CoCo_1_Sepref(cc) = is_1_Sep;
    box_CoCo_nbRIEref(cc) = nbRootsInEnclosure;
    acb_set(box_CoCo_encloref(cc), enclosure);

}

void box_CoCo_init_set_from_domainfmpq_int( box_CoCo_t cc, 
                                            const fmpq_t lre, const fmpq_t ure,  const fmpq_t lim,  const fmpq_t uim,   
                                            slong nbRoots, 
                                            int is_2_Sep, int is_6_Sep, slong prec ){
    box_CoCo_init(cc);
    box_list_t boxes;
    box_list_init(boxes);
    
    
    slong prec2 = box_get_box_list_domainfmpq( boxes, lre, ure,  lim,  uim );
    while ( !box_list_is_empty(boxes) )
        box_CoCo_insert_box(cc, box_list_pop(boxes));
    box_CoCo_nbRooref(cc) = nbRoots;
    box_CoCo_Mprecref(cc) = PWPOLY_MAX(prec, prec2);
    box_CoCo_2_Sepref(cc) = is_2_Sep;
    box_CoCo_6_Sepref(cc) = is_6_Sep;
    
    box_CoCo_w_encref(cc) = 0;
        
    
    box_list_clear(boxes);
}

void box_CoCo_initiali_QBRSp(box_CoCo_t x){
    fmpz_set_si(box_CoCo_QBRSpref(x), 2);
}

void box_CoCo_increase_QBRSp(box_CoCo_t x){
    fmpz_mul_si( box_CoCo_QBRSpref(x), box_CoCo_QBRSpref(x), 2);
}

int  box_CoCo_isminima_QBRSp(box_CoCo_t x){
    if (fmpz_cmp_si(box_CoCo_QBRSpref(x), 2)>0)
        return 0;
    else
        return 1;
}

void box_CoCo_decrease_QBRSp(box_CoCo_t x){
    
    if (fmpz_cmp_si(box_CoCo_QBRSpref(x), 2)>0){ /* means that connCmp_nwSpdref(x) > 2*/
        fmpz_divexact_si(box_CoCo_QBRSpref(x), box_CoCo_QBRSpref(x), 2);
    }
    else
        fmpz_set_si(box_CoCo_QBRSpref(x), 2);
}

void box_CoCo_get_infRe_arf(arf_t r, const box_CoCo_t x){
    fmpz_t temp;
    fmpz_init(temp);
    fmpz_mul_ui(temp, box_CoCo_infReref(x), 2);
    /* set to 2*box_CoCo_infReref(x) * 2^(depth) */
    arf_set_fmpz_2exp(r, temp, box_CoCo_depthref(x));
    fmpz_clear(temp);
}

void box_CoCo_get_supRe_arf(arf_t r, const box_CoCo_t x){
    fmpz_t temp;
    fmpz_init(temp);
    fmpz_mul_ui(temp, box_CoCo_supReref(x), 2);
    fmpz_add_ui(temp, temp, 2);
    /* set to (2*box_CoCo_supReref(x)+2) * 2^(depth) */
    arf_set_fmpz_2exp(r, temp, box_CoCo_depthref(x));
    fmpz_clear(temp);
}

void box_CoCo_get_infIm_arf(arf_t r, const box_CoCo_t x){
    fmpz_t temp;
    fmpz_init(temp);
    fmpz_mul_ui(temp, box_CoCo_infImref(x), 2);
    /* set to 2*box_CoCo_infImref(x) * 2^(depth) */
    arf_set_fmpz_2exp(r, temp, box_CoCo_depthref(x));
    fmpz_clear(temp);
}

void box_CoCo_get_supIm_arf(arf_t r, const box_CoCo_t x){
    fmpz_t temp;
    fmpz_init(temp);
    fmpz_mul_ui(temp, box_CoCo_supImref(x), 2);
    fmpz_add_ui(temp, temp, 2);
    /* set to (2*box_CoCo_supImref(x)+2) * 2^(depth) */
    arf_set_fmpz_2exp(r, temp, box_CoCo_depthref(x));
    fmpz_clear(temp);
}

int  box_CoCo_is_strictly_in_2_unit(const box_CoCo_t x){
    
    int res = 0;
    arf_t bound;
    arf_init(bound);
    
    box_CoCo_get_infRe_arf(bound, x);
    if ( arf_cmp_si(bound, -2) > 0 ) {
        box_CoCo_get_supRe_arf(bound, x);
        if ( arf_cmp_si(bound,  2) < 0 ) {
            box_CoCo_get_infIm_arf(bound, x);
            if ( arf_cmp_si(bound, -2) > 0 ) {
                box_CoCo_get_supIm_arf(bound, x);
                if ( arf_cmp_si(bound,  2) < 0 )
                    res = 1;
            }
        }
    }
    
    arf_clear(bound);
    return res;
}

int box_CoCo_is_strictly_in_inflated_unit ( box_CoCo_t x, double inflation ){
        
    int res = 0;
    arf_t bound;
    arf_init(bound);
    
    box_CoCo_get_infRe_arf(bound, x);
    if ( arf_cmp_d(bound, -inflation) > 0 ) {
        box_CoCo_get_supRe_arf(bound, x);
        if ( arf_cmp_d(bound,  inflation) < 0 ) {
            box_CoCo_get_infIm_arf(bound, x);
            if ( arf_cmp_d(bound, -inflation) > 0 ) {
                box_CoCo_get_supIm_arf(bound, x);
                if ( arf_cmp_d(bound,  inflation) < 0 )
                    res = 1;
            }
        }
    }
    
    arf_clear(bound);
    return res;
}

void box_CoCo_diameter(arb_t r, const box_CoCo_t x){
    fmpz_t nbRe, nbIm;
    fmpz_init(nbRe);
    fmpz_init(nbIm);
    
    fmpz_sub(nbRe, box_CoCo_supReref(x), box_CoCo_infReref(x) );
    fmpz_sub(nbIm, box_CoCo_supImref(x), box_CoCo_infImref(x) );
    if (fmpz_cmp( nbIm, nbRe ) > 0)
        fmpz_set(nbRe, nbIm);
    
    arb_zero(r);
    arf_set_fmpz_2exp( arb_midref(r), nbRe, box_CoCo_depthref(x) );
    
    fmpz_clear(nbRe);
    fmpz_clear(nbIm);
}

int box_CoCo_is_compact(const box_CoCo_t x){
    int res = 0;
    fmpz_t nbRe, nbIm;
    fmpz_init(nbRe);
    fmpz_init(nbIm);
    
    fmpz_sub(nbRe, box_CoCo_supReref(x), box_CoCo_infReref(x) );
    fmpz_sub(nbIm, box_CoCo_supImref(x), box_CoCo_infImref(x) );
    
    res = (fmpz_cmp_ui(nbRe, 3) <= 0) && (fmpz_cmp_ui(nbIm, 3) <= 0);
    
    fmpz_clear(nbRe);
    fmpz_clear(nbIm);
    return res;
}

void box_CoCo_get_contBox(acb_t r, const box_CoCo_t x){
    
    fmpz_t temp;
    fmpz_init(temp);
    /* real part */
    fmpz_add(temp, box_CoCo_infReref(x), box_CoCo_supReref(x));
    fmpz_add_ui( temp, temp, 1);
    arf_set_fmpz_2exp(arb_midref(acb_realref(r)), temp, box_CoCo_depthref(x));
    fmpz_sub(temp, box_CoCo_supReref(x), box_CoCo_infReref(x));
    fmpz_add_ui(temp, temp, 1);
    mag_set_fmpz_2exp_fmpz(arb_radref(acb_realref(r)), temp, box_CoCo_depthref(x));
    
    /* imaginary part */
    fmpz_add(temp, box_CoCo_infImref(x), box_CoCo_supImref(x));
    fmpz_add_ui( temp, temp, 1);
    arf_set_fmpz_2exp(arb_midref(acb_imagref(r)), temp, box_CoCo_depthref(x));
    fmpz_sub(temp, box_CoCo_supImref(x), box_CoCo_infImref(x));
    fmpz_add_ui(temp, temp, 1);
    mag_set_fmpz_2exp_fmpz(arb_radref(acb_imagref(r)), temp, box_CoCo_depthref(x));
    
    mag_max( arb_radref(acb_realref(r)), arb_radref(acb_realref(r)), arb_radref(acb_imagref(r)) );
    mag_set( arb_radref(acb_imagref(r)), arb_radref(acb_realref(r)) );
    
    fmpz_clear(temp);
}

void box_CoCo_get_center_halfwidth_fmpq( fmpq_t c_re, fmpq_t c_im, fmpq_t hw, const box_CoCo_t x ){
    
    /* ((2*infRe+1)*2^d + (2*supRe+1)*2^d)/2 = ( (infRe+supRe)*2*2^d + 2*2^d )/2 = (infRe+supRe + 1)*2^d */
    /* ((2*infIm+1)*2^d + (2*supIm+1)*2^d)/2 = ( (infIm+supIm)*2*2^d + 2*2^d )/2 = (infIm+supIm + 1)*2^d */
    /* nb of boxes re: (supRe - infRe + 1), halfwidth re: (nb of boxes re)*2^d */
    /* same for im */
    
    fmpz_t temp;
    fmpz_init(temp);
    /* max( nb boxes hor, nb boxes ver ) */
    fmpz_sub( fmpq_numref(c_re), box_CoCo_supReref(x), box_CoCo_infReref(x));
    fmpz_sub( fmpq_numref(c_im), box_CoCo_supImref(x), box_CoCo_infImref(x));
    fmpz_max( temp, fmpq_numref(c_re), fmpq_numref(c_im) );
    fmpz_add_ui( temp, temp, 1 );
    /* set hw to 2^box_CoCo_depthref(x) */
    fmpz_set_si( fmpq_numref(c_re), 2 );
    fmpz_abs( fmpq_denref(c_re), box_CoCo_depthref(x) );
    fmpz_pow_fmpz( fmpq_numref(c_re), fmpq_numref(c_re), fmpq_denref(c_re) );
    if ( fmpz_cmp_si( box_CoCo_depthref(x), 0 ) >= 0 ) {
        fmpz_set( fmpq_numref( hw ), fmpq_numref(c_re) );
        fmpz_one( fmpq_denref( hw ) );
    } else {
        fmpz_set( fmpq_denref( hw ), fmpq_numref(c_re) );
        fmpz_one( fmpq_numref( hw ) );
    }
    /* center of the real part */
    fmpz_one( fmpq_denref(c_re) );
    fmpz_add( fmpq_numref(c_re), box_CoCo_infReref(x), box_CoCo_supReref(x));
    fmpz_add_ui( fmpq_numref(c_re), fmpq_numref(c_re), 1 );
    fmpq_mul( c_re, c_re, hw );
    /* center of the imag part */
    fmpz_one( fmpq_denref(c_im) );
    fmpz_add( fmpq_numref(c_im), box_CoCo_infImref(x), box_CoCo_supImref(x));
    fmpz_add_ui( fmpq_numref(c_im), fmpq_numref(c_im), 1 );
    fmpq_mul( c_im, c_im, hw );
    /* hw */
    fmpq_mul_fmpz( hw, hw, temp );
    
    fmpz_clear(temp);
}

#ifndef PW_SILENT
void box_CoCo_fprint(FILE * file, box_CoCo_t x) {
    fprintf(file, "Connected component at depth "); fmpz_fprint(file, box_CoCo_depthref(x));
    fprintf(file, " with %ld boxes\n", box_list_get_size( box_CoCo_boxesref(x) ) );
//     box_list_fprint(file, box_CoCo_boxesref(x) ); fprintf(file, "\n");
    if ( box_list_get_size( box_CoCo_boxesref(x) ) > 0 ) {
        arf_t t;
        arf_init(t);
        box_CoCo_get_infRe_arf(t, x);
        fprintf(file, "   infRe: "); arf_fprintd(file, t, 10); fprintf(file, "\n");
        box_CoCo_get_supRe_arf(t, x);
        fprintf(file, "   supRe: "); arf_fprintd(file, t, 10); fprintf(file, "\n");
        box_CoCo_get_infIm_arf(t, x);
        fprintf(file, "   infIm: "); arf_fprintd(file, t, 10); fprintf(file, "\n");
        box_CoCo_get_supIm_arf(t, x);
        fprintf(file, "   supIm: "); arf_fprintd(file, t, 10); fprintf(file, "\n");
        arf_clear(t);
        acb_t contBox;
        acb_init(contBox);
        box_CoCo_get_contBox(contBox, x);
        fprintf(file, "   containing box: "); acb_fprintd(file, contBox, 10); fprintf(file, "\n");
        fprintf(file, "   QBR speed: "); fmpz_print(box_CoCo_QBRSpref(x)); fprintf(file, "\n");
        fprintf(file, "   max prec: %ld, nb of roots, %ld, 2 separated: %d, 6 separated: %d, last QBR success: %d\n", 
                                box_CoCo_Mprecref(x), box_CoCo_nbRooref(x), 
                                box_CoCo_2_Sepref(x), box_CoCo_6_Sepref(x),
                                box_CoCo_QBRSuref(x)
               );
        acb_clear(contBox);
        if (box_CoCo_w_encref(x)) {
            fprintf(file, "   enclosure box: "); acb_fprintd(file, box_CoCo_encloref(x), 10); fprintf(file, "\n");
            fprintf(file, "   nb of roots in enclosure: %ld, 1 separated: %d\n", 
                              box_CoCo_nbRIEref(x), box_CoCo_1_Sepref(x) );
        }
    } else {
        if (box_CoCo_w_encref(x)) {
            fprintf(file, "   enclosure box: "); acb_fprintd(file, box_CoCo_encloref(x), 10); fprintf(file, "\n");
            fprintf(file, "   nb of roots in enclosure: %ld, 1 separated: %d\n", 
                              box_CoCo_nbRIEref(x), box_CoCo_1_Sepref(x) );
        }
        fprintf(file, "   max prec: %ld, nb of roots, %ld, 2 separated: %d, 8 separated: %d, last QBR success: %d\n", 
                                box_CoCo_Mprecref(x), box_CoCo_nbRooref(x), 
                                box_CoCo_2_Sepref(x), box_CoCo_6_Sepref(x),
                                box_CoCo_QBRSuref(x)
               );
    }
}
#endif

/*Precondition:  nbSubd>=0 */
/*Specification: allocate and initializes a cc, and its constituent boxes, so that: */
/*               the re-union of the constituent boxes is:                          */
/*                           2*B(0,1) if nbSubd = 0,                                */
/*                       (1 + 2^(-(nbSubd-1))*B(0,1) otherwise                      */
/*               contains:                                                          */
/*                       4 boxes if nbSubd = 0,                                     */
/*                       (2^nbSubd + 2)^2 boxes otherwise                           */
box_CoCo_ptr box_CoCo_initial_CoCo(int nbSubd){
    
    box_CoCo_ptr res = (box_CoCo_ptr) pwpoly_malloc ( sizeof(box_CoCo) );
    
    box_list_t boxes, subboxes;
    box_list_init(boxes);
    box_list_init(subboxes);
    
    box_ptr NE   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_ptr SE   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_ptr SW   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_ptr NW   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_init(NE);
    box_init(SE);
    box_init(SW);
    box_init(NW);
    box_set_si_si_si(NE, 0,  0,  0);
    box_set_si_si_si(SE, 0,  0, -1);
    box_set_si_si_si(SW, 0, -1, -1);
    box_set_si_si_si(NW, 0, -1,  0);
    box_list_insert_sorted(boxes, SW  );
    box_list_insert_sorted(boxes, NW  );
    box_list_insert_sorted(boxes, SE  );
    box_list_insert_sorted(boxes, NE  );
    
    acb_t u, v;
    acb_init(u);
    acb_init(v);
    acb_zero(u);
    mag_one( arb_radref( acb_realref( u ) ) );
    mag_one( arb_radref( acb_imagref( u ) ) );
    box_ptr bcur;
    
    for (int s = 0; s<nbSubd; s++){
        /* quadrisect boxes of cc, push it into subboxes */
        /* subboxes is now a sorted list of boxes */
        box_list_quadrisect( subboxes, boxes );
        /* keep only the boxes having non-empty intersection with B(0,1) */
        while (! box_list_is_empty(subboxes) ) {
            bcur = box_list_pop(subboxes);
            box_get_acb(v, bcur);
            if (acb_overlaps(u, v))
                box_list_insert_sorted(boxes, bcur);
            else {
                box_clear(bcur);
                pwpoly_free(bcur);
            }
        }
    }
    
    box_CoCo_init(res);
    /*insert boxes of boxes in cc */
    while (! box_list_is_empty(boxes) )
        box_CoCo_insert_box(res, box_list_pop(boxes));
    
    box_list_clear(boxes);
    box_list_clear(subboxes);
    acb_clear(u);
    acb_clear(v);
    
    return res;
}

box_CoCo_ptr box_CoCo_unit_box(){
    
    box_CoCo_ptr res = (box_CoCo_ptr) pwpoly_malloc ( sizeof(box_CoCo) );
    box_CoCo_init(res);
    
    box_ptr NE   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_ptr SE   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_ptr SW   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_ptr NW   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_init(NE);
    box_init(SE);
    box_init(SW);
    box_init(NW);
    box_set_si_si_si(NE, -1,  0,  0);
    box_set_si_si_si(SE, -1,  0, -1);
    box_set_si_si_si(SW, -1, -1, -1);
    box_set_si_si_si(NW, -1, -1,  0);
    box_CoCo_insert_box(res, SW);
    box_CoCo_insert_box(res, NW);
    box_CoCo_insert_box(res, SE);
    box_CoCo_insert_box(res, NE);
    
    return res;
}

/* get four child of the box centered in zero with halfwidth 2^scale */ 
box_CoCo_ptr box_CoCo_unit_box_scaled(slong scale, const acb_ptr InRoots, slong nbInRoots){
    
    box_CoCo_ptr res = (box_CoCo_ptr) pwpoly_malloc ( sizeof(box_CoCo) );
    box_CoCo_init(res);
    
    box_ptr NE   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_ptr SE   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_ptr SW   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_ptr NW   = (box_ptr) pwpoly_malloc (sizeof(box));
    box_init(NE);
    box_init(SE);
    box_init(SW);
    box_init(NW);
    box_set_si_si_si(NE, scale,  0,  0);
    box_set_si_si_si(SE, scale,  0, -1);
    box_set_si_si_si(SW, scale, -1, -1);
    box_set_si_si_si(NW, scale, -1,  0);
    
    if (nbInRoots > 0) {
        acb_t acbSW, acbNW, acbSE, acbNE;
        acb_init(acbSW);
        acb_init(acbNW);
        acb_init(acbSE);
        acb_init(acbNE);
        
        box_roIntref(SW) = _acb_vec_init( nbInRoots );
        box_lenRIref(SW) = nbInRoots;
        box_nbRinref(SW) = 0;
        box_get_acb(acbSW, SW);
        mag_mul_2exp_si( arb_radref( acb_realref( acbSW ) ), arb_radref( acb_realref( acbSW ) ), 1 );
        mag_mul_2exp_si( arb_radref( acb_imagref( acbSW ) ), arb_radref( acb_imagref( acbSW ) ), 1 );
        
        box_roIntref(NW) = _acb_vec_init( nbInRoots );
        box_lenRIref(NW) = nbInRoots;
        box_nbRinref(NW) = 0;
        box_get_acb(acbNW, NW);
        mag_mul_2exp_si( arb_radref( acb_realref( acbNW ) ), arb_radref( acb_realref( acbNW ) ), 1 );
        mag_mul_2exp_si( arb_radref( acb_imagref( acbNW ) ), arb_radref( acb_imagref( acbNW ) ), 1 );
        
        box_roIntref(SE) = _acb_vec_init( nbInRoots );
        box_lenRIref(SE) = nbInRoots;
        box_nbRinref(SE) = 0;
        box_get_acb(acbSE, SE);
        mag_mul_2exp_si( arb_radref( acb_realref( acbSE ) ), arb_radref( acb_realref( acbSE ) ), 1 );
        mag_mul_2exp_si( arb_radref( acb_imagref( acbSE ) ), arb_radref( acb_imagref( acbSE ) ), 1 );
        
        box_roIntref(NE) = _acb_vec_init( nbInRoots );
        box_lenRIref(NE) = nbInRoots;
        box_nbRinref(NE) = 0;
        box_get_acb(acbNE, NE);
        mag_mul_2exp_si( arb_radref( acb_realref( acbNE ) ), arb_radref( acb_realref( acbNE ) ), 1 );
        mag_mul_2exp_si( arb_radref( acb_imagref( acbNE ) ), arb_radref( acb_imagref( acbNE ) ), 1 );
        
        for (slong i=0; i< nbInRoots; i++) {
            if ( acb_contains( acbSW, InRoots+i ) ) {
                acb_set( box_roIntref(SW) + box_nbRinref(SW), InRoots+i );
                box_nbRinref(SW)++;
            }
            if ( acb_contains( acbNW, InRoots+i ) ) {
                acb_set( box_roIntref(NW) + box_nbRinref(NW), InRoots+i );
                box_nbRinref(NW)++;
            }
            if ( acb_contains( acbSE, InRoots+i ) ) {
                acb_set( box_roIntref(SE) + box_nbRinref(SE), InRoots+i );
                box_nbRinref(SE)++;
            }
            if ( acb_contains( acbNE, InRoots+i ) ) {
                acb_set( box_roIntref(NE) + box_nbRinref(NE), InRoots+i );
                box_nbRinref(NE)++;
            }
        }
        
        if ( box_nbRinref(NE)==0 ) {
            _acb_vec_clear( box_roIntref(NE), box_lenRIref(NE) );
            box_lenRIref(NE) = 0;
        }
        
        if ( box_nbRinref(SE)==0 ) {
            _acb_vec_clear( box_roIntref(SE), box_lenRIref(SE) );
            box_lenRIref(SE) = 0;
        }
        
        if ( box_nbRinref(NW)==0 ) {
            _acb_vec_clear( box_roIntref(NW), box_lenRIref(NW) );
            box_lenRIref(NW) = 0;
        }
        
        if ( box_nbRinref(SW)==0 ) {
            _acb_vec_clear( box_roIntref(SW), box_lenRIref(SW) );
            box_lenRIref(SW) = 0;
        }
        
        acb_clear(acbNE);
        acb_clear(acbSE);
        acb_clear(acbNW);
        acb_clear(acbSW);
    }
    
    box_CoCo_insert_box(res, SW);
    box_CoCo_insert_box(res, NW);
    box_CoCo_insert_box(res, SE);
    box_CoCo_insert_box(res, NE);
    
    return res;
}

/*Precondition:  boxes of cc and b have the same width  */
/*               b > boxes of cc => crealref(b) > supReref(cc) */
/*                                  or (crealref(b) = supReref(cc)) and (cimagref(b) > supImref(cc)) */
/*Specification: returns true if (cc \cap b)\neq \emptyset */
/*                       false otherwise                   */
int box_CoCo_are_8connected( box_CoCo_t cc, box_t b ){
    
    int res=0;
    fmpz_t dist, temp;
    fmpz_init(dist);
    fmpz_init(temp);
    
    fmpz_sub(dist, box_crealref(b), box_CoCo_supReref(cc));
    /* check if crealref(b) - supReref(cc) <=1 */
    res = fmpz_cmp_ui( dist, 1 ) <= 0;
    
    if (res) {
        /* check if cimagref(b) >= infImref(cc) -1 */
        /*      and cimagref(b) <= supImref(cc) +1 */
        fmpz_add_ui( dist, box_cimagref(b), 1 );
        fmpz_add_ui( temp, box_CoCo_supImref(cc), 1 );
        res = (fmpz_cmp( dist, box_CoCo_infImref(cc) ) >=0)
            &&(fmpz_cmp( temp, box_cimagref(b) ) >=0);
    }
    
    if (res) {
        res = 0;
        /* if a box b2 of cc with crealref(b) - crealref(b2) <=1  */
        /* is 8 connected with b */
        box_list_iterator it = box_list_endEl( box_CoCo_boxesref (cc) );
        while ( it != NULL ) {
            fmpz_sub(dist, box_crealref(b), box_crealref(box_list_elmt(it)));
            if (fmpz_cmp_ui( dist, 1 ) <= 0){
                if ( box_are_8connected ( b, box_list_elmt(it) ) ) {
                    res = 1;
                    it = NULL;
                } else
                    it = box_list_prev(it);
            }
            else
                it = NULL;
        }
    }
    
    fmpz_clear(dist);
    fmpz_clear(temp);
    
    return res;
}

/*Specification: returns true iff boxes of cc are all outside unit disc */
int box_CoCo_is_outside_unit_disc ( const box_CoCo_t cc ) {
    
    acb_t contBox;
    acb_init(contBox);
    arb_t abs, one;
    arb_init(abs);
    arb_init(one);
    arb_one(one);
    box_CoCo_get_contBox(contBox, cc);
    acb_abs(abs, contBox, PWPOLY_DEFAULT_PREC);
    
    int res;
    if ( arb_gt (abs, one) )
        res = 1;
    else if (arb_lt (abs, one))
        res = 0;
    else {
        res = 1;
        box_list_iterator it = box_list_begin( box_CoCo_boxesref(cc) );
        while ( (it!=box_list_end())&&(res==1) ) {
            res = box_is_outside_unit_disc ( box_list_elmt(it) );
            it = box_list_next(it);
        }
    }
    
    acb_clear(contBox);
    arb_clear(abs);
    arb_clear(one);
    return res;
}

/*Specification: returns true iff boxes of cc are all strictly inside unit disc */
int  box_CoCo_is_inside_unit_disc(const box_CoCo_t cc){
    acb_t contBox;
    acb_init(contBox);
    arb_t abs, one;
    arb_init(abs);
    arb_init(one);
    arb_one(one);
    box_CoCo_get_contBox(contBox, cc);
    acb_abs(abs, contBox, PWPOLY_DEFAULT_PREC);
    
    int res;
    if ( arb_lt (abs, one) )
        res = 1;
    else if (arb_gt (abs, one))
        res = 0;
    else {
        res = 1;
        box_list_iterator it = box_list_begin( box_CoCo_boxesref(cc) );
        while ( (it!=box_list_end())&&(res==1) ) {
            res = box_is_inside_unit_disc ( box_list_elmt(it) );
            it = box_list_next(it);
        }
    }
    
    acb_clear(contBox);
    arb_clear(abs);
    arb_clear(one);
    return res;
}

int box_CoCo_has_box( const box_CoCo_t cc, const box_t b ){
    int isIn = 0;
    box_list_iterator it = box_list_begin( box_CoCo_boxesref( cc ) );
    while ( (it!=box_list_end()) && (isIn==0) ){
        if (   (fmpz_cmp( box_depthref( box_list_elmt(it) ), box_depthref( b ) ) == 0)
            && (fmpz_cmp( box_crealref( box_list_elmt(it) ), box_crealref( b ) ) == 0)
            && (fmpz_cmp( box_cimagref( box_list_elmt(it) ), box_cimagref( b ) ) == 0) )
            isIn = 1;
        it = box_list_next(it);
    }
    return isIn;
}

/* assume dest is an empty, initialized, CoCo */
void box_CoCo_set_conjugate_closure( box_CoCo_t dest, const box_CoCo_t src ) {
    box_ptr bcur, bcurConj;
    box_list_iterator it = box_list_begin( box_CoCo_boxesref( src ) );
    while (it!=box_list_end()){
        bcur     = (box_ptr) pwpoly_malloc ( sizeof(box) );
        bcurConj = (box_ptr) pwpoly_malloc ( sizeof(box) );
        box_init(bcur);
        box_init(bcurConj);
        box_set(bcur, box_list_elmt(it) );
        box_conjugate( bcurConj, bcur );
        box_CoCo_insert_box(dest, bcur);
        if ( box_CoCo_has_box(src, bcurConj ) ) {
            box_clear(bcurConj);
            pwpoly_free(bcurConj);
        } else {
            box_CoCo_insert_box(dest, bcurConj);
        }
        it = box_list_next(it);
    }
    fmpz_set(box_CoCo_QBRSpref(dest), box_CoCo_QBRSpref(src));
    box_CoCo_Mprecref(dest) = box_CoCo_Mprecref(src);
    box_CoCo_nbRooref(dest) = box_CoCo_nbRooref(src);
    box_CoCo_2_Sepref(dest) = box_CoCo_2_Sepref(src);
    box_CoCo_6_Sepref(dest) = box_CoCo_6_Sepref(src);
    box_CoCo_QBRSuref(dest) = box_CoCo_QBRSuref(src);
    
    fmpz_set( box_CoCo_depthref(dest), box_CoCo_depthref(src) );
    box_CoCo_w_encref(dest) = box_CoCo_w_encref(src);
    box_CoCo_1_Sepref(dest) = box_CoCo_1_Sepref(src);
    box_CoCo_nbRIEref(dest) = box_CoCo_nbRIEref(src);
    if(box_CoCo_w_encref(src))
        acb_set(box_CoCo_encloref(dest), box_CoCo_encloref(src));
}

/* assume dest is an empty, initialized, CoCo */
/* assume src is imaginary positive */
void box_CoCo_set_conjugate( box_CoCo_t dest, const box_CoCo_t src ) {
    box_ptr bcur, bcurConj;
    box_list_iterator it = box_list_begin( box_CoCo_boxesref( src ) );
    while (it!=box_list_end()){
        bcur     = box_list_elmt(it);
        bcurConj = (box_ptr) pwpoly_malloc ( sizeof(box) );
        box_init(bcurConj);
        box_conjugate( bcurConj, bcur );
        box_CoCo_insert_box(dest, bcurConj);
        it = box_list_next(it);
    }
    fmpz_set(box_CoCo_QBRSpref(dest), box_CoCo_QBRSpref(src));
    box_CoCo_Mprecref(dest) = box_CoCo_Mprecref(src);
    box_CoCo_nbRooref(dest) = box_CoCo_nbRooref(src);
    box_CoCo_2_Sepref(dest) = box_CoCo_2_Sepref(src);
    box_CoCo_6_Sepref(dest) = box_CoCo_6_Sepref(src);
    box_CoCo_QBRSuref(dest) = box_CoCo_QBRSuref(src);
    
    fmpz_set( box_CoCo_depthref(dest), box_CoCo_depthref(src) );
    box_CoCo_w_encref(dest) = box_CoCo_w_encref(src);
    box_CoCo_1_Sepref(dest) = box_CoCo_1_Sepref(src);
    box_CoCo_nbRIEref(dest) = box_CoCo_nbRIEref(src);
    if(box_CoCo_w_encref(src))
        acb_conj(box_CoCo_encloref(dest), box_CoCo_encloref(src));
}

/* Preconditions: cc1, cc2 are not empty, have the same depth and are connected */
/* Specifs      : set cc1 to the union of cc1 and cc2 */
/*              : let cc2 empty but do not clear it   */
void box_CoCo_merge_2_box_CoCo( box_CoCo_t cc1, box_CoCo_t cc2 ){
    
    /* sets infRe, supRe, infIm, supIm */
    if ( fmpz_cmp( box_CoCo_infReref(cc2), box_CoCo_infReref(cc1) ) < 0 )
        fmpz_set(box_CoCo_infReref(cc1), box_CoCo_infReref(cc2));
    if ( fmpz_cmp( box_CoCo_supReref(cc2), box_CoCo_supReref(cc1) ) > 0 )
        fmpz_set(box_CoCo_supReref(cc1), box_CoCo_supReref(cc2));
    if ( fmpz_cmp( box_CoCo_infImref(cc2), box_CoCo_infImref(cc1) ) < 0 )
        fmpz_set(box_CoCo_infImref(cc1), box_CoCo_infImref(cc2));
    if ( fmpz_cmp( box_CoCo_supImref(cc2), box_CoCo_supImref(cc1) ) > 0 )
        fmpz_set(box_CoCo_supImref(cc1), box_CoCo_supImref(cc2));
    
    /* case where cc1 contains only one box and this box 
     * is greater than the boxes of cc2 */
    if ( (box_CoCo_list_get_size(box_CoCo_boxesref(cc1))==1) && 
         (box_isless( box_list_last(box_CoCo_boxesref(cc2)), box_list_first(box_CoCo_boxesref(cc1)) ) ) ) {
        box_list_push( box_CoCo_boxesref(cc2), box_list_pop( box_CoCo_boxesref(cc1) ) );
        box_list_swap(box_CoCo_boxesref(cc2), box_CoCo_boxesref(cc1));
        return;
    }
    
    /* save cc1.boxes in temp and sets cc1.boxes to an empty list */
    box_list_t temp;
    box_list_init(temp);
    box_list_swap(temp, box_CoCo_boxesref(cc1));
    
    /* fill cc1.boxes while preserving order */
    box_ptr b1, b2;
    
    while ( (!box_list_is_empty(temp)) && (!box_list_is_empty( box_CoCo_boxesref(cc2) )) ) {
        b1 = box_list_first( temp );
        b2 = box_list_first( box_CoCo_boxesref(cc2) );
        if (box_isless(b1, b2))
            box_list_push( box_CoCo_boxesref(cc1), box_list_pop( temp ));
        else
            box_list_push( box_CoCo_boxesref(cc1), box_list_pop( box_CoCo_boxesref(cc2) ));
    }
    /* now either temp or cc2.boxes are empty: fill cc1.boxes with the remaining boxes */
    while (!box_list_is_empty(temp))
        box_list_push( box_CoCo_boxesref(cc1), box_list_pop( temp ));
    
    while (!box_list_is_empty(box_CoCo_boxesref(cc2)))
        box_list_push( box_CoCo_boxesref(cc1), box_list_pop( box_CoCo_boxesref(cc2) ));
    
    box_list_clear(temp);
    
    box_CoCo_Mprecref(cc1) = PWPOLY_MAX( box_CoCo_Mprecref(cc1), box_CoCo_Mprecref(cc2) );
    box_CoCo_nbRooref(cc1) = -1;
    box_CoCo_2_Sepref(cc1) = 0;
    box_CoCo_6_Sepref(cc1) = 0;
}

/* maintains order in cc and hull */
/* if a box is already in there, assume depths are the same */
void box_CoCo_insert_box(box_CoCo_t x, box_t b){
    
    if (box_list_is_empty( box_CoCo_boxesref(x) )) {
        box_list_insert_sorted( box_CoCo_boxesref(x), b);
        fmpz_set( box_CoCo_depthref(x), box_depthref(b) );
        fmpz_set( box_CoCo_infReref(x), box_crealref(b) );
        fmpz_set( box_CoCo_supReref(x), box_crealref(b) );
        fmpz_set( box_CoCo_infImref(x), box_cimagref(b) );
        fmpz_set( box_CoCo_supImref(x), box_cimagref(b) );
    } else {
        box_list_insert_sorted( box_CoCo_boxesref(x), b);
        if ( fmpz_cmp (box_crealref(b), box_CoCo_infReref(x) ) < 0 )
            fmpz_set( box_CoCo_infReref(x), box_crealref(b) );
        if ( fmpz_cmp (box_crealref(b), box_CoCo_supReref(x) ) > 0 )
            fmpz_set( box_CoCo_supReref(x), box_crealref(b) );
        if ( fmpz_cmp (box_cimagref(b), box_CoCo_infImref(x) ) < 0 )
            fmpz_set( box_CoCo_infImref(x), box_cimagref(b) );
        if ( fmpz_cmp (box_cimagref(b), box_CoCo_supImref(x) ) > 0 )
            fmpz_set( box_CoCo_supImref(x), box_cimagref(b) );
    }
    
    box_CoCo_Mprecref(x) = PWPOLY_MAX( box_CoCo_Mprecref(x), box_pprecref(b) );
    box_CoCo_nbRooref(x) = -1;
    box_CoCo_2_Sepref(x) = 0;
    box_CoCo_6_Sepref(x) = 0;
}

/*Precondition:  cc in ccs are disjoint connected components of the same width */
/*               boxes of ccs and b are distinct and have the same depth  */
/*               b > boxes of ccs                        */
/*Specification: modifies inplace the list of ccs so that b is in boxes of ccs */
/*               and cc in ccs are disjoint connected components */
void box_CoCo_union_box( box_CoCo_list_t ccs, box_t b){
    
    box_CoCo_ptr cb;
    cb = ( box_CoCo_ptr ) pwpoly_malloc (sizeof(box_CoCo));
    box_CoCo_init_box(cb, b);
    
    box_CoCo_list_t ltemp;
    box_CoCo_list_init(ltemp);
    
    box_CoCo_ptr cctemp;
    
    while (!box_CoCo_list_is_empty(ccs)){
        cctemp = box_CoCo_list_pop(ccs);
        /* optimization: the box is more probably connected to the last cc in the list */
//         cctemp = box_CoCo_list_pop_back(ccs);
        if (box_CoCo_are_8connected(cctemp, b)){
            box_CoCo_merge_2_box_CoCo(cb, cctemp);
            
            box_CoCo_clear(cctemp);
            pwpoly_free(cctemp);
        }
        else 
            box_CoCo_list_push(ltemp, cctemp);
    }
    
    box_CoCo_list_push(ltemp, cb);
    box_CoCo_list_swap(ltemp, ccs);
    box_CoCo_list_clear(ltemp);
    
}
