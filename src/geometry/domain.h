/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef DOMAIN_H
#define DOMAIN_H

#include "pw_base.h"
#include "box.h"
#include "box_CoCo.h"
#include <string.h>

#include <gmp.h>
#ifndef mpz_struct
#define mpz_struct __mpz_struct
#endif

#ifdef __cplusplus
extern "C" {
#endif
    
#define DOMAIN_INFLATION 5 /* set inf_ure = ((x+1)/x)*ure, inf_uim = ((x+1)/x)*uim */
                           /* set inf_lre = ((x-1)/x)*lre, inf_lim = ((x-1)/x)*lim */

typedef struct { 
  int            _boundsAreFinite; /* _boundsAreFinite == 0 -> initial domain is C, default */
                                   /* _boundsAreFinite&1 >0 -> _lre is finite */
                                   /* _boundsAreFinite&2 >0 -> _ure is finite */
                                   /* _boundsAreFinite&4 >0 -> _lim is finite */
                                   /* _boundsAreFinite&8 >0 -> _uim is finite */
  /* initial domain */
  fmpq           _lre;
  fmpq           _ure;
  fmpq           _lim;
  fmpq           _uim;
  /* inflated initial domain -> only used in solver_CoCo? */
  fmpq           _inf_lre;
  fmpq           _inf_ure;
  fmpq           _inf_lim;
  fmpq           _inf_uim;
} domain;

typedef domain domain_t[1];
typedef domain * domain_ptr;

#define domain_finiteref(X)  ( (X)->_boundsAreFinite )
#define domain_lreref(X)     ( &(X)->_lre )
#define domain_ureref(X)     ( &(X)->_ure )
#define domain_limref(X)     ( &(X)->_lim )
#define domain_uimref(X)     ( &(X)->_uim )
#define domain_inf_lreref(X) ( &(X)->_inf_lre )
#define domain_inf_ureref(X) ( &(X)->_inf_ure )
#define domain_inf_limref(X) ( &(X)->_inf_lim )
#define domain_inf_uimref(X) ( &(X)->_inf_uim )

void domain_init( domain_t d );

void domain_clear( domain_t d );

void domain_set_fmpq( domain_t d, const int lre_finite, const fmpq_t lre, 
                                  const int ure_finite, const fmpq_t ure, 
                                  const int lim_finite, const fmpq_t lim, 
                                  const int uim_finite, const fmpq_t uim );

void domain_set_mpz_tab( domain_t d, const mpz_struct domain_num[], const mpz_struct domain_den[] );

void domain_set_si(   domain_t d, const slong bounds[] );
             
PWPOLY_INLINE int domain_is_lre_finite( const domain_t d ) { return ( !(!(domain_finiteref(d) & 0x1)) ); }
PWPOLY_INLINE int domain_is_ure_finite( const domain_t d ) { return ( !(!(domain_finiteref(d) & 0x2)) ); }
PWPOLY_INLINE int domain_is_lim_finite( const domain_t d ) { return ( !(!(domain_finiteref(d) & 0x4)) ); }
PWPOLY_INLINE int domain_is_uim_finite( const domain_t d ) { return ( !(!(domain_finiteref(d) & 0x8)) ); }

/* printing */
#ifndef PW_NO_INTERFACE
void domain_fprint(FILE * file, const domain_t d);

PWPOLY_INLINE void domain_print (const domain_t d) {
    domain_fprint(stdout, d);
}

/* printing short */
void domain_fprint_short(FILE * file, const domain_t d);

PWPOLY_INLINE void domain_print_short (const domain_t d) {
    domain_fprint_short(stdout, d);
}
#endif

PWPOLY_INLINE int domain_is_C (const domain_t d) {
  return (domain_finiteref(d)==0);
}

PWPOLY_INLINE int domain_is_finite (const domain_t d) {
  return (domain_is_lre_finite(d) && domain_is_ure_finite(d) && domain_is_lim_finite(d) && domain_is_uim_finite(d));
}

int domain_is_point( const domain_t d );
int domain_contains_zero( const domain_t d );
int domain_contains_zero_in_interior( const domain_t d );

/* scanning a str into a table of slong*/
void domain_set_str( slong domain[], char * str );

/* is symmetric with respect to real line */
int domain_is_symetric_real_line ( const domain_t d );

int _box_has_real_part_lt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a );
int _box_has_real_part_gt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a );
int _box_has_imag_part_lt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a );
int _box_has_imag_part_gt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a );

void domain_get_max_width( fmpq_t max_width, const domain_t dom ); 

// /* assume box_depthref(b) < 0                                                                   */
// /* For a rational, check if for any alpha in b, Re(1/alpha) < a                                 */
// /* if a=0 <=> check if for any alpha in b, Re(alpha) < 0                                        */
// /* else   <=> check if b is fully outside the disc of center (1/(2a)) + 0*I and radius (1/(2a)) */
// int _box_inv_has_real_part_lt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a );
// int box_inv_has_real_part_lt_fmpq( const box_t b, const fmpq_t a );
// /* assume box_depthref(b) < 0                                                                   */
// /* For a rational, check if for any alpha in b, Re(1/alpha) > a                                 */
// /* if a=0 <=> check if for any alpha in b, Re(alpha) > 0                                        */
// /* else   <=> check if b is fully  inside the disc of center (1/(2a)) + 0*I and radius (1/(2a)) */
// int _box_inv_has_real_part_gt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a );
// int box_inv_has_real_part_gt_fmpq( const box_t b, const fmpq_t a );
// /* assume box_depthref(b) < 0                                                                   */
// /* For a rational, check if for any alpha in b, Im(1/alpha) < a                                 */
// /* if a=0 <=> check if for any alpha in b, -Im(alpha) < 0 <=> Im(alpha)>0                       */
// /* else   <=> check if b is fully outside the disc of center 0 - (1/(2a))*I and radius (1/(2a)) */
// int _box_inv_has_imag_part_lt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a );
// int box_inv_has_imag_part_lt_fmpq( const box_t b, const fmpq_t a );
// /* assume box_depthref(b) < 0                                                                   */
// /* For a rational, check if for any alpha in b, Im(1/alpha) > a                                 */
// /* if a=0 <=> check if for any alpha in b, -Im(alpha) > 0 <=> Im(alpha)<0                       */
// /* else   <=> check if b is fully inside  the disc of center 0 - (1/(2a))*I and radius (1/(2a)) */
// int _box_inv_has_imag_part_gt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a );
// int box_inv_has_imag_part_gt_fmpq( const box_t b, const fmpq_t a );

int domain_is_box_outside_domain( const domain_t d, const box_t b );
int domain_is_box_outside_inflated_domain( const domain_t d, const box_t b );

// int domain_is_box_inside_domain( const domain_t d, const box_t b, const int reverse );
int domain_is_box_inside_inflated_domain( const domain_t d, const box_t b );

// int domain_is_box_CoCo_contBox_outside_domain( const domain_t d, const box_CoCo_t cc, const int reverse );
// int domain_is_box_CoCo_contBox_outside_inflated_domain( const domain_t d, const box_CoCo_t cc, const int reverse );

// int domain_is_box_CoCo_contBox_inside_domain( const domain_t d, const box_CoCo_t cc, const int reverse );
// int domain_is_box_CoCo_contBox_inside_inflated_domain( const domain_t d, const box_CoCo_t cc, const int reverse );
// int domain_is_box_CoCo_inflated_contBox_inside_inflated_domain( const domain_t d, const box_CoCo_t cc, const ulong factor, const int reverse );

int domain_is_box_CoCo_outside_domain( const domain_t d, const box_CoCo_t cc );
// int domain_is_box_CoCo_outside_inflated_domain( const domain_t d, const box_CoCo_t cc, const int reverse );

int domain_is_box_CoCo_inside_domain( const domain_t d, const box_CoCo_t cc );
int domain_is_box_CoCo_inside_inflated_domain( const domain_t d, const box_CoCo_t cc );

int domain_acb_intersect_domain( const domain_t d, const acb_t b );

int domain_acb_is_strictly_in_domain( const domain_t d, const acb_t b );

// int _acb_disc_inv_has_real_part_lt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec );
// int _acb_disc_inv_has_real_part_gt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec );
// int _acb_disc_inv_has_imag_part_lt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec );
// int _acb_disc_inv_has_imag_part_gt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec );

/* assume center does not contain zero */
int domain_is_acb_disc_strictly_outside_domain( const domain_t d, const acb_t center, const arb_t radius, slong prec );
/* assume r,R are >0 */
/* returns 1 => A(0,r,R) has empty intersection with the domain */
int domain_acb_annulus_empty_intersection(const domain_t d, const arb_t r, const arb_t R, slong prec );
#ifdef __cplusplus
}
#endif

#endif
