/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "domain.h"

void domain_init( domain_t d ){
    domain_finiteref(d) = 0; /* by default, the domain is C */
    /* initial domain */
    fmpq_init(domain_lreref(d));
    fmpq_init(domain_ureref(d));
    fmpq_init(domain_limref(d));
    fmpq_init(domain_uimref(d));
    /* inflated initial domain */
    fmpq_init(domain_inf_lreref(d));
    fmpq_init(domain_inf_ureref(d));
    fmpq_init(domain_inf_limref(d));
    fmpq_init(domain_inf_uimref(d));
}

void domain_clear( domain_t d ){
    /* initial domain */
    fmpq_clear(domain_lreref(d));
    fmpq_clear(domain_ureref(d));
    fmpq_clear(domain_limref(d));
    fmpq_clear(domain_uimref(d));
    /* inflated initial domain */
    fmpq_clear(domain_inf_lreref(d));
    fmpq_clear(domain_inf_ureref(d));
    fmpq_clear(domain_inf_limref(d));
    fmpq_clear(domain_inf_uimref(d));
}

void domain_set_fmpq( domain_t d, const int lre_finite, const fmpq_t lre, 
                                  const int ure_finite, const fmpq_t ure, 
                                  const int lim_finite, const fmpq_t lim, 
                                  const int uim_finite, const fmpq_t uim ){
    domain_finiteref(d) = 0;
    if (lre_finite) {
        domain_finiteref(d) += 1;
        fmpq_set( domain_lreref(d), lre );
    }
    if (ure_finite) {
        domain_finiteref(d) += 2;
        fmpq_set( domain_ureref(d), ure );
    }
    if (lim_finite) {
        domain_finiteref(d) += 4;
        fmpq_set( domain_limref(d), lim );
    }
    if (uim_finite) {
        domain_finiteref(d) += 8;
        fmpq_set( domain_uimref(d), uim );
    }
    /* set inflated domain */
    fmpq_t inflp, inflm, middle, halfwidth;
    fmpq_init(inflp);
    fmpq_init(inflm);
    fmpq_init(middle);
    fmpq_init(halfwidth);
    fmpq_set_si( inflp, DOMAIN_INFLATION+1, DOMAIN_INFLATION );
    fmpq_set_si( inflm, 1, DOMAIN_INFLATION );
    if ( (lre_finite) && (ure_finite) && (fmpq_cmp( lre, ure)!=0) ) {
        fmpq_set_si(halfwidth, 1, 2);
        fmpq_sub(middle, ure, lre);
        fmpq_mul(halfwidth, halfwidth, middle);
        fmpq_add(middle, lre, halfwidth);
        fmpq_mul(halfwidth, halfwidth, inflp);
        fmpq_sub(domain_inf_lreref(d), middle, halfwidth);
        fmpq_add(domain_inf_ureref(d), middle, halfwidth);
    } else {
        if (lre_finite) {
            fmpq_sub(domain_inf_lreref(d), lre, inflm);
        }
        if (ure_finite) {
            fmpq_add(domain_inf_ureref(d), ure, inflm);
        }
    }
    if ( (lim_finite) && (uim_finite) && (fmpq_cmp( lim, uim)!=0) ) {
        fmpq_set_si(halfwidth, 1, 2);
        fmpq_sub(middle, uim, lim);
        fmpq_mul(halfwidth, halfwidth, middle);
        fmpq_add(middle, lim, halfwidth);
        fmpq_mul(halfwidth, halfwidth, inflp);
        fmpq_sub(domain_inf_limref(d), middle, halfwidth);
        fmpq_add(domain_inf_uimref(d), middle, halfwidth);
    } else {
        if (lim_finite) {
            fmpq_sub(domain_inf_limref(d), lim, inflm);
        }
        if (uim_finite) {
            fmpq_add(domain_inf_uimref(d), uim, inflm);
        }
    }
    fmpq_clear(inflp);
    fmpq_clear(inflm);
    fmpq_clear(middle);
    fmpq_clear(halfwidth);
}

void domain_set_mpz_tab( domain_t d, const mpz_struct domain_num[], const mpz_struct domain_den[] ) {
    int lre_finite, ure_finite, lim_finite, uim_finite;
    fmpq_t lre, ure, lim, uim;
    fmpq_init(lre);
    fmpq_init(ure);
    fmpq_init(lim);
    fmpq_init(uim);
    if (mpz_cmp_ui( domain_den+0 , 0 )==0) {
        lre_finite=0;
//         printf("domain_set_mpz_tab, lre = -infinity\n");
    } else {
        lre_finite=1;
//         printf("domain_set_mpz_tab, lre = ");
//         mpz_out_str( stdout, 10, domain_num+0 );
//         printf(" / "); mpz_out_str( stdout, 10, domain_den+0 );
//         printf("\n");
        fmpz_set_mpz( fmpq_numref(lre), domain_num+0 );
        fmpz_set_mpz( fmpq_denref(lre), domain_den+0 );
        fmpq_canonicalise(lre);
    }
    if (mpz_cmp_ui( domain_den+1 , 0 )==0) {
        ure_finite=0;
    } else {
        ure_finite=1;
        fmpz_set_mpz( fmpq_numref(ure), domain_num+1 );
        fmpz_set_mpz( fmpq_denref(ure), domain_den+1 );
        fmpq_canonicalise(ure);
    }
    if (mpz_cmp_ui( domain_den+2 , 0 )==0) {
        lim_finite=0;
    } else {
        lim_finite=1;
        fmpz_set_mpz( fmpq_numref(lim), domain_num+2 );
        fmpz_set_mpz( fmpq_denref(lim), domain_den+2 );
        fmpq_canonicalise(lim);
    }
    if (mpz_cmp_ui( domain_den+3 , 0 )==0) {
        uim_finite=0;
    } else {
        uim_finite=1;
        fmpz_set_mpz( fmpq_numref(uim), domain_num+3 );
        fmpz_set_mpz( fmpq_denref(uim), domain_den+3 );
        fmpq_canonicalise(uim);
    }
    domain_set_fmpq( d, lre_finite, lre, 
                        ure_finite, ure, 
                        lim_finite, lim, 
                        uim_finite, uim );
    fmpq_clear(lre);
    fmpq_clear(ure);
    fmpq_clear(lim);
    fmpq_clear(uim);
}

void domain_set_si(   domain_t d, const slong bounds[] ){
    fmpq_t lre, ure, lim, uim;
    fmpq_init(lre);
    fmpq_init(ure);
    fmpq_init(lim);
    fmpq_init(uim);
    int lre_finite = (bounds[1]!=0);
    int ure_finite = (bounds[3]!=0);
    int lim_finite = (bounds[5]!=0);
    int uim_finite = (bounds[7]!=0);
    fmpq_set_si(lre, bounds[0], bounds[1]);
    fmpq_set_si(ure, bounds[2], bounds[3]);
    fmpq_set_si(lim, bounds[4], bounds[5]);
    fmpq_set_si(uim, bounds[6], bounds[7]);
    domain_set_fmpq( d, lre_finite, lre, ure_finite, ure, lim_finite, lim, uim_finite, uim );
    fmpq_clear(lre);
    fmpq_clear(ure);
    fmpq_clear(lim);
    fmpq_clear(uim);
}

#ifndef PW_NO_INTERFACE
void domain_fprint(FILE * file, const domain_t d){
    if (domain_finiteref(d)==0) {
        fprintf(file, "the complex plain");
        return;
    }
    
    fprintf(file, "[");
    if (domain_is_lre_finite(d))
        fmpq_fprint(file, domain_lreref(d));
    else
        fprintf(file, "-inf");
        
    fprintf(file, ", ");
    if (domain_is_ure_finite(d))
        fmpq_fprint(file, domain_ureref(d));
    else
        fprintf(file, "+inf");
    
    fprintf(file, "] + I*[");
    if (domain_is_lim_finite(d))
        fmpq_fprint(file, domain_limref(d));
    else
        fprintf(file, "-inf");
        
    fprintf(file, ", ");
    if (domain_is_uim_finite(d))
        fmpq_fprint(file, domain_uimref(d));
    else
        fprintf(file, "+inf");
    fprintf(file, "]");   
}

void domain_fprint_short(FILE * file, const domain_t d){
    
    if (domain_finiteref(d)==0) {
        fprintf(file, "the complex plain");
        return;
    }
    
    fprintf(file, "[");
    if (domain_is_lre_finite(d))
        fmpq_fprint(file, domain_lreref(d));
    else
        fprintf(file, "-inf");
        
    fprintf(file, ", ");
    if (domain_is_ure_finite(d))
        fmpq_fprint(file, domain_ureref(d));
    else
        fprintf(file, "+inf");
    
    fprintf(file, "] + I*[");
    if (domain_is_lim_finite(d))
        fmpq_fprint(file, domain_limref(d));
    else
        fprintf(file, "-inf");
        
    fprintf(file, ", ");
    if (domain_is_uim_finite(d))
        fmpq_fprint(file, domain_uimref(d));
    else
        fprintf(file, "+inf");
    fprintf(file, "]");
}
#endif

/* scanning a str into a table of 8 slong's*/
void domain_set_str( slong d[], char * str ) {
    char *toklre=NULL, *tokure=NULL, *toklim=NULL, *tokuim=NULL;
    char *toklrenum=NULL, *toklreden=NULL;
    char *tokurenum=NULL, *tokureden=NULL;
    char *toklimnum=NULL, *toklimden=NULL;
    char *tokuimnum=NULL, *tokuimden=NULL;
    
    if ( (strcmp( str, "Global" )==0) || (strcmp( str, "global" )==0) || (strcmp( str, "C" )==0)) {
        d[0] = 1; d[1] = 0; d[2] = 1; d[3] = 0;
        d[4] = 1; d[5] = 0; d[6] = 1; d[7] = 0;
        return;
    }
    
    toklre = strtok (str,",");
    tokure = strtok (NULL,",");
    toklim = strtok (NULL,",");
    tokuim = strtok (NULL,",");
    
    if ( (toklre==NULL)||(tokure==NULL)||(toklim==NULL)||(toklre==NULL) ) {
        printf("error when parsing domain! set domain to the whole complex plain\n");
        d[0] = 1; d[1] = 0; d[2] = 1; d[3] = 0;
        d[4] = 1; d[5] = 0; d[6] = 1; d[7] = 0;
        return;
    }
    
    if ( ( strcmp( toklre, "-inf" )==0 ) || (strcmp( toklre, "inf" )==0) ) {
        d[0] = 1; d[1] = 0;
    } else {
        toklrenum = strtok (toklre,"/");
        toklreden = strtok (NULL,"/");
//         sscanf( toklrenum, "%ld", d + 0 );
        d[0] = strtol(toklrenum, NULL, 10);
        if (toklreden!=NULL)
//             sscanf( toklreden, "%ld", d + 1 );
            d[1] = strtol(toklreden, NULL, 10);
        else
            d[1] = 1;
    }
    
    if ( ( strcmp( tokure, "+inf" )==0 ) || (strcmp( tokure, "inf" )==0) ) {
        d[2] = 1; d[3] = 0;
    } else {
        tokurenum = strtok (tokure,"/");
        tokureden = strtok (NULL,"/");
//         sscanf( tokurenum, "%ld", d + 2 );
        d[2] = strtol(tokurenum, NULL, 10);
        if (tokureden!=NULL)
//             sscanf( tokureden, "%ld", d + 3 );
            d[3] = strtol(tokureden, NULL, 10);
        else
            d[3] = 1;
    }
    
    if ( ( strcmp( toklim, "-inf" )==0 ) || (strcmp( toklim, "inf" )==0) ) {
        d[4] = 1; d[5] = 0;
    } else {
        toklimnum = strtok (toklim,"/");
        toklimden = strtok (NULL,"/");
//         sscanf( toklimnum, "%ld", d + 4 );
        d[4] = strtol(toklimnum, NULL, 10);
        if (toklimden!=NULL)
//             sscanf( toklimden, "%ld", d + 5 );
            d[5] = strtol(toklimden, NULL, 10);
        else
            d[5] = 1;
    }
    
    if ( ( strcmp( tokuim, "+inf" )==0 ) || (strcmp( tokuim, "inf" )==0) ) {
        d[6] = 1; d[7] = 0;
    } else {
        tokuimnum = strtok (tokuim,"/");
        tokuimden = strtok (NULL,"/");
//         sscanf( tokuimnum, "%ld", d + 6 );
        d[6] = strtol(tokuimnum, NULL, 10);
        if (tokuimden!=NULL)
//             sscanf( tokuimden, "%ld", d + 7 );
            d[7] = strtol(tokuimden, NULL, 10);
        else
            d[7] = 1;
    }
    return;
}

int domain_is_point( const domain_t d ) {
    
    int res= domain_is_finite(d);
    res = res && (fmpq_cmp( domain_lreref(d), domain_ureref(d) ) == 0);
    res = res && (fmpq_cmp( domain_limref(d), domain_uimref(d) ) == 0);
    return res;
}

int domain_contains_zero( const domain_t d ) {
    
    int res = 1;
    res = res && ( (!domain_is_lre_finite(d)) || (fmpq_sgn(domain_lreref(d))<=0) );
    res = res && ( (!domain_is_ure_finite(d)) || (fmpq_sgn(domain_ureref(d))>=0) );
    res = res && ( (!domain_is_lim_finite(d)) || (fmpq_sgn(domain_limref(d))<=0) );
    res = res && ( (!domain_is_uim_finite(d)) || (fmpq_sgn(domain_uimref(d))>=0) );
        
    return res;
}
int domain_contains_zero_in_interior( const domain_t d ) {
    
    int res = 1;
    res = res && ( (!domain_is_lre_finite(d)) || (fmpq_sgn(domain_lreref(d))<0) );
    res = res && ( (!domain_is_ure_finite(d)) || (fmpq_sgn(domain_ureref(d))>0) );
    res = res && ( (!domain_is_lim_finite(d)) || (fmpq_sgn(domain_limref(d))<0) );
    res = res && ( (!domain_is_uim_finite(d)) || (fmpq_sgn(domain_uimref(d))>0) );
     
    return res;
}

/* is symmetric with respect to real line */
int domain_is_symetric_real_line ( const domain_t d ){
    if ( (!domain_is_lim_finite(d)) && (!domain_is_uim_finite(d)) )
        return 1;
    if ( (!domain_is_lim_finite(d)) || (!domain_is_uim_finite(d)) )
        return 0;
    fmpq_t muim;
    fmpq_init(muim);
    fmpq_neg(muim, domain_uimref(d));
    int res = (fmpq_cmp( domain_limref(d), muim )==0);
    fmpq_clear(muim);
    
    return res;
}

int _box_has_real_part_lt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a ){
    fmpq_t temp;
    fmpq_init(temp);
    fmpq_add(temp, c_re, hw);
    int res = (fmpq_cmp( temp, a ) < 0);
    fmpq_clear(temp);
    return res;
}
int _box_has_real_part_gt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a ){
    fmpq_t temp;
    fmpq_init(temp);
    fmpq_sub(temp, c_re, hw);
    int res = (fmpq_cmp( temp, a ) > 0);
    fmpq_clear(temp);
    return res;
}
int _box_has_imag_part_lt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a ){
    fmpq_t temp;
    fmpq_init(temp);
    fmpq_add(temp, c_im, hw);
    int res = (fmpq_cmp( temp, a ) < 0);
    fmpq_clear(temp);
    return res;
}
int _box_has_imag_part_gt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a ){
    fmpq_t temp;
    fmpq_init(temp);
    fmpq_sub(temp, c_im, hw);
    int res = (fmpq_cmp( temp, a ) > 0);
    fmpq_clear(temp);
    return res;
}

void domain_get_max_width( fmpq_t max_width, const domain_t dom ) {
    fmpq_t temp;
    fmpq_init(temp);
    fmpq_sub( max_width, domain_ureref(dom), domain_lreref(dom) );
    fmpq_sub( temp, domain_uimref(dom), domain_limref(dom) );
    if (fmpq_cmp( temp, max_width ) > 0)
        fmpq_set( max_width, temp );
    fmpq_clear(temp);
}

// void get_point_minimizing_2norm( fmpq_t p_re, fmpq_t p_im, const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw ){
//     
//     fmpq_t mhw;
//     fmpq_init(mhw);
//     fmpq_neg(mhw, hw);
//     if ( fmpq_cmp( c_re, hw ) > 0 ) { //the box is in the E half plane
//         fmpq_sub( p_re, c_re, hw );
//     } else if ( fmpq_cmp( c_re, mhw ) < 0 ) { //the box is in the W half plane
//         fmpq_add( p_re, c_re, hw );
//     } else { //the box contains the imaginary line
//         fmpq_zero( p_re );
//     }
//     if ( fmpq_cmp( c_im, hw ) > 0 ) { //the box is in the N half plane
//         fmpq_sub( p_im, c_im, hw );
//     } else if ( fmpq_cmp( c_im, mhw ) < 0 ) { //the box is in the S half plane
//         fmpq_add( p_im, c_im, hw );
//     } else { //the box contains the imaginary line
//         fmpq_zero( p_im );
//     }
//     fmpq_clear(mhw);
// }
// 
// void get_point_maximizing_2norm( fmpq_t p_re, fmpq_t p_im, const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw ) {
//     if ( fmpq_sgn( c_re ) >= 0 )
//        fmpq_add( p_re, c_re, hw );
//     else
//        fmpq_sub( p_re, c_re, hw );
//     if ( fmpq_sgn( c_im ) >= 0 )
//        fmpq_add( p_im, c_im, hw );
//     else
//        fmpq_sub( p_im, c_im, hw );
// }

void get_2norm_square( fmpq_t res, const fmpq_t re, const fmpq_t im ){
    fmpq_t temp;
    fmpq_init(temp);
    fmpq_pow_si( temp, re, 2);
    fmpq_pow_si( res,  im, 2);
    fmpq_add(res, res, temp);
    fmpq_clear(temp);
}

// /* assume box_depthref(b) < 0                                                                   */
// /* For a rational, check if for any alpha in b, Re(1/alpha) < a                                 */
// /* if a=0 <=> check if for any alpha in b, Re(alpha) < 0                                        */
// /* if a>0 <=> check if b is fully outside the disc of center (1/(2a)) + 0*I and radius (1/(2a)) */
// /* if a<0 <=> check if b is fully inside  the disc of center (1/(2a)) + 0*I and radius (1/(2a)) */
// int _box_inv_has_real_part_lt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a ) {
//     
//     if (fmpq_is_zero(a)) {
//         fmpq_t mhw;
//         fmpq_init(mhw);
//         fmpq_neg(mhw, hw);
//         int res = ( fmpq_cmp( c_re, mhw ) < 0 );
//         fmpq_clear(mhw);
//         return res;
//     }
//     fmpq_t dcenter, p_re, p_im;
//     fmpq_init(dcenter);
//     fmpq_init(p_re);
//     fmpq_init(p_im);
//     /* set dcenter to 1/(2a) */
//     fmpq_set_si(dcenter, 1,2);
//     fmpq_div(dcenter,dcenter, a);
// //     printf("dcenter: "); fmpq_print(dcenter); printf("\n");
//     /* shift center of 1/(2a) toward 0 along real axis */
//     fmpq_sub( p_re, c_re, dcenter );
// //     printf("shifted center: "); fmpq_print(p_re); printf("+I*("); fmpq_print(c_im); printf(") halfwidth: "); fmpq_print(hw); printf("\n");
//     /* square the radius of the circle */
//     fmpq_pow_si(dcenter, dcenter, 2);
//     
//     int res;
//     if ( fmpq_cmp_si( a, 0 ) > 0 ) {
//         /* find the point in b minimizing the distance to zero */
//         get_point_minimizing_2norm( p_re, p_im, p_re, c_im, hw );
// //      printf("point minimizing 2-norm: "); fmpq_print(p_re); printf("+I*("); fmpq_print(p_im); printf(")");printf("\n");
//         /* get the square of the two norm of c_re, c_im */
//         get_2norm_square(p_re, p_re, p_im);
//         /* b is fully outside the disc of center 1/(2a) + 0*I and radius 1/(2a) */
//         /* <=> p_re > dcenter */
//         res = ( fmpq_cmp( p_re, dcenter ) > 0 );
//     } else {
//         /* find the point in b maximizing the distance to zero */
//         get_point_maximizing_2norm( p_re, p_im, p_re, c_im, hw );
//         get_2norm_square(p_re, p_re, p_im);
//         /* b is fully inside  the disc of center 1/(2a) + 0*I and radius 1/(2a) */
//         /* <=> p_re > dcenter */
//         res = ( fmpq_cmp( p_re, dcenter ) < 0 );
//     }
//     fmpq_clear(dcenter);
//     fmpq_clear(p_re);
//     fmpq_clear(p_im);
//     
//     return res;
// }
// int box_inv_has_real_part_lt_fmpq( const box_t b, const fmpq_t a ) {
//     
//     if (fmpz_is_zero(box_depthref(b)))
//         return 0;
//     
//     fmpq_t c_re, c_im, hw;
//     fmpq_init(c_re);
//     fmpq_init(c_im);
//     fmpq_init(hw);
//     /* get real and imag parts of the center and halfwidth of b */
//     box_get_center_halfwidth_fmpq( c_re, c_im, hw, b );
// //     printf("center: "); fmpq_print(c_re); printf("+I*("); fmpq_print(c_im); printf(") halfwidth: "); fmpq_print(hw); printf("\n");
//     /* compare */
//     int res = _box_inv_has_real_part_lt_fmpq( c_re, c_im, hw, a );
//     
//     fmpq_clear(c_re);
//     fmpq_clear(c_im);
//     fmpq_clear(hw);
//     
//     return res;
// }
// /* assume box_depthref(b) < 0                                                                   */
// /* For a rational, check if for any alpha in b, Re(1/alpha) > a                                 */
// /* if a=0 <=> check if for any alpha in b, Re(alpha) > 0                                        */
// /* if a>0 <=> check if b is fully  inside the disc of center (1/(2a)) + 0*I and radius (1/(2a)) */
// /* if a<0 <=> check if b is fully outside the disc of center (1/(2a)) + 0*I and radius (1/(2a)) */
// int _box_inv_has_real_part_gt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a ) {
//     
//     if (fmpq_is_zero(a)) {
//         int res = ( fmpq_cmp( c_re, hw ) > 0 );
//         return res;
//     }
//     
//     fmpq_t dcenter, p_re, p_im;
//     fmpq_init(dcenter);
//     fmpq_init(p_re);
//     fmpq_init(p_im);
//     /* set dcenter to 1/(2a) */
//     fmpq_set_si(dcenter, 1,2);
//     fmpq_div(dcenter,dcenter, a);
//     /* shift center of 1/(2a) toward 0 along real axis */
//     fmpq_sub( p_re, c_re, dcenter );
//     /* square the radius of the circle */
//     fmpq_pow_si(dcenter, dcenter, 2);
//     int res;
//     if ( fmpq_cmp_si( a, 0 ) > 0 ) {
//         /* find the point in b maximizing the distance to zero */
//         get_point_maximizing_2norm( p_re, p_im, p_re, c_im, hw );
//         /* get the square of the two norm of c_re, c_im */
//         get_2norm_square(p_re, p_re, p_im);
//         /* b is fully inside the disc of center 1/(2a) + 0*I and radius 1/(2a) */
//         /* <=> p_re > dcenter */
//         res = ( fmpq_cmp( p_re, dcenter ) < 0 );
//     } else {
//         /* find the point in b minimizing the distance to zero */
//         get_point_minimizing_2norm( p_re, p_im, p_re, c_im, hw );
//         /* get the square of the two norm of c_re, c_im */
//         get_2norm_square(p_re, p_re, p_im);
//         /* b is fully outside the disc of center 1/(2a) + 0*I and radius 1/(2a) */
//         /* <=> p_re > dcenter */
//         res = ( fmpq_cmp( p_re, dcenter ) > 0 );
//     }
//     fmpq_clear(dcenter);
//     fmpq_clear(p_re);
//     fmpq_clear(p_im);
//     
//     return res;
// }
// int box_inv_has_real_part_gt_fmpq( const box_t b, const fmpq_t a ) {
//     
//     if (fmpz_is_zero(box_depthref(b)))
//         return 0;
//     
//     fmpq_t c_re, c_im, hw;
//     fmpq_init(c_re);
//     fmpq_init(c_im);
//     fmpq_init(hw);
//     /* get real and imag parts of the center and halfwidth of b */
//     box_get_center_halfwidth_fmpq( c_re, c_im, hw, b );
//     /* compare */
//     int res = _box_inv_has_real_part_gt_fmpq( c_re, c_im, hw, a );
//     
//     fmpq_clear(c_re);
//     fmpq_clear(c_im);
//     fmpq_clear(hw);
//     
//     return res;
// }
// /* assume box_depthref(b) < 0                                                                   */
// /* For a rational, check if for any alpha in b, Im(1/alpha) < a                                 */
// /* if a=0 <=> check if for any alpha in b, -Im(alpha) < 0 <=> Im(alpha)>0                       */
// /* if a>0 <=> check if b is fully outside the disc of center 0 - (1/(2a))*I and radius (1/(2a)) */
// /* if a<0 <=> check if b is fully  inside the disc of center 0 - (1/(2a))*I and radius (1/(2a)) */
// int _box_inv_has_imag_part_lt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a ) {
//     
//     if (fmpq_is_zero(a)) {
//         int res = ( fmpq_cmp( c_im, hw ) > 0 );
//         return res;
//     }
//     
//     fmpq_t dcenter, p_re, p_im;
//     fmpq_init(dcenter);
//     fmpq_init(p_re);
//     fmpq_init(p_im);
//     /* set dcenter to -1/(2a) */
//     fmpq_set_si(dcenter, -1,2);
//     fmpq_div(dcenter, dcenter, a);
// //     printf("dcenter: "); fmpq_print(dcenter); printf("\n");
//     /* shift center of -1/(2a) toward 0 along imag axis */
//     fmpq_sub( p_im, c_im, dcenter );
// //     printf("shifted center: "); fmpq_print(c_re); printf("+I*("); fmpq_print(p_im); printf(") halfwidth: "); fmpq_print(hw); printf("\n");
//     /* square the radius of the circle */
//     fmpq_pow_si(dcenter, dcenter, 2);
//     int res;
//     if ( fmpq_cmp_si( a, 0 ) > 0 ) {
//         /* find the point in b minimizing the distance to zero */
//         get_point_minimizing_2norm( p_re, p_im, c_re, p_im, hw );
//         /* get the square of the two norm of c_re, c_im */
//         get_2norm_square(p_re, p_re, p_im);
//         /* b is fully outside the disc of center 0 + 1/(2a)*I and radius 1/(2a) */
//         /* <=> p_re > dcenter */
//         res = ( fmpq_cmp( p_re, dcenter ) > 0 );
//     } else {
//         /* find the point in b maximizing the distance to zero */
//         get_point_maximizing_2norm( p_re, p_im, c_re, p_im, hw );
//         /* get the square of the two norm of c_re, c_im */
//         get_2norm_square(p_re, p_re, p_im);
//         /* b is fully inside the disc of center 0 + 1/(2a)*I and radius 1/(2a) */
//         /* <=> p_re > dcenter */
//         res = ( fmpq_cmp( p_re, dcenter ) < 0 );
//     }
//     
//     fmpq_clear(dcenter);
//     fmpq_clear(p_re);
//     fmpq_clear(p_im);
//     
//     return res;
// }
// int box_inv_has_imag_part_lt_fmpq( const box_t b, const fmpq_t a ) {
//     
//     if (fmpz_is_zero(box_depthref(b)))
//         return 0;
//     
//     fmpq_t c_re, c_im, hw;
//     fmpq_init(c_re);
//     fmpq_init(c_im);
//     fmpq_init(hw);
//     /* get real and imag parts of the center and halfwidth of b */
//     box_get_center_halfwidth_fmpq( c_re, c_im, hw, b );
// //     printf("center: "); fmpq_print(c_re); printf("+I*("); fmpq_print(c_im); printf(") halfwidth: "); fmpq_print(hw); printf("\n");
//     /* compare */
//     int res = _box_inv_has_imag_part_lt_fmpq( c_re, c_im, hw, a );
//     
//     fmpq_clear(c_re);
//     fmpq_clear(c_im);
//     fmpq_clear(hw);
//     
//     return res;
// }
// /* assume box_depthref(b) < 0                                                                   */
// /* For a rational, check if for any alpha in b, Im(1/alpha) > a                                 */
// /* if a=0 <=> check if for any alpha in b, -Im(alpha) > 0 <=> Im(alpha)<0                       */
// /* if a>0 <=> check if b is fully  inside the disc of center 0 - (1/(2a))*I and radius (1/(2a)) */
// /* if a<0 <=> check if b is fully outside the disc of center 0 - (1/(2a))*I and radius (1/(2a)) */
// int _box_inv_has_imag_part_gt_fmpq( const fmpq_t c_re, const fmpq_t c_im, const fmpq_t hw, const fmpq_t a )  {
//     
//     if (fmpq_is_zero(a)) {
//         fmpq_t mhw;
//         fmpq_init(mhw);
//         fmpq_neg(mhw, hw);
//         int res = ( fmpq_cmp( c_im, mhw ) < 0 );
//         fmpq_clear(mhw);
//         return res;
//     }
//     
//     fmpq_t dcenter, p_re, p_im;
//     fmpq_init(dcenter);
//     fmpq_init(p_re);
//     fmpq_init(p_im);
//     /* set dcenter to -1/(2a) */
//     fmpq_set_si(dcenter, -1,2);
//     fmpq_div(dcenter,dcenter, a);
//     /* shift center of -1/(2a) toward 0 along imag axis */
//     fmpq_sub( p_im, c_im, dcenter );
//     /* square the radius of the circle */
//     fmpq_pow_si(dcenter, dcenter, 2);
//     int res;
//     if ( fmpq_cmp_si( a, 0 ) > 0 ) {
//         /* find the point in b maximizing the distance to zero */
//         get_point_maximizing_2norm( p_re, p_im, c_re, p_im, hw );
//         /* get the square of the two norm of c_re, c_im */
//         get_2norm_square(p_re, p_re, p_im);
//         /* b is fully outside the disc of center 0 + 1/(2a)*I and radius 1/(2a) */
//         /* <=> p_re > dcenter */
//         res = ( fmpq_cmp( p_re, dcenter ) < 0 );
//     } else {
//         /* find the point in b minimizing the distance to zero */
//         get_point_minimizing_2norm( p_re, p_im, c_re, p_im, hw );
//         /* get the square of the two norm of c_re, c_im */
//         get_2norm_square(p_re, p_re, p_im);
//         /* b is fully inside the disc of center 0 + 1/(2a)*I and radius 1/(2a) */
//         /* <=> p_re > dcenter */
//         res = ( fmpq_cmp( p_re, dcenter ) > 0 );
//     }
//  
//     fmpq_clear(dcenter);
//     fmpq_clear(p_re);
//     fmpq_clear(p_im);
//     
//     return res;
// }
// int box_inv_has_imag_part_gt_fmpq( const box_t b, const fmpq_t a )  {
//     
//     if (fmpz_is_zero(box_depthref(b)))
//         return 0;
//     
//     fmpq_t c_re, c_im, hw;
//     fmpq_init(c_re);
//     fmpq_init(c_im);
//     fmpq_init(hw);
//     /* get real and imag parts of the center and halfwidth of b */
//     box_get_center_halfwidth_fmpq( c_re, c_im, hw, b );
//     /* compare */
//     int res = _box_inv_has_imag_part_gt_fmpq( c_re, c_im, hw, a );
//     
//     fmpq_clear(c_re);
//     fmpq_clear(c_im);
//     fmpq_clear(hw);
//     
//     return res;
// }

int domain_is_box_outside_domain( const domain_t d, const box_t b ) {
    
    if (domain_finiteref(d)==0)
        return 0;
    
    fmpq_t c_re, c_im, hw;
    fmpq_init(c_re);
    fmpq_init(c_im);
    fmpq_init(hw);
    box_get_center_halfwidth_fmpq( c_re, c_im, hw, b );
    
    int res = 0;
    res = res || (domain_is_lre_finite(d) && _box_has_real_part_lt_fmpq( c_re, c_im, hw, domain_lreref(d) ));
    res = res || (domain_is_ure_finite(d) && _box_has_real_part_gt_fmpq( c_re, c_im, hw, domain_ureref(d) ));
    res = res || (domain_is_lim_finite(d) && _box_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_limref(d) ));
    res = res || (domain_is_uim_finite(d) && _box_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_uimref(d) ));
    
    fmpq_clear(c_re);
    fmpq_clear(c_im);
    fmpq_clear(hw);
    
    return res;
}

int domain_is_box_outside_inflated_domain( const domain_t d, const box_t b ) {
    
    if (domain_finiteref(d)==0)
        return 0;
    
    fmpq_t c_re, c_im, hw;
    fmpq_init(c_re);
    fmpq_init(c_im);
    fmpq_init(hw);
    box_get_center_halfwidth_fmpq( c_re, c_im, hw, b );
    
    int res = 0;
    res = res || (domain_is_lre_finite(d) && _box_has_real_part_lt_fmpq( c_re, c_im, hw, domain_inf_lreref(d) ));
    res = res || (domain_is_ure_finite(d) && _box_has_real_part_gt_fmpq( c_re, c_im, hw, domain_inf_ureref(d) ));
    res = res || (domain_is_lim_finite(d) && _box_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_inf_limref(d) ));
    res = res || (domain_is_uim_finite(d) && _box_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_inf_uimref(d) ));
    
    fmpq_clear(c_re);
    fmpq_clear(c_im);
    fmpq_clear(hw);
    
    return res;
}
// 
int domain_is_box_inside_domain( const domain_t d, const box_t b ) {
    
    if (domain_finiteref(d)==0)
        return 1;
    
    fmpq_t c_re, c_im, hw;
    fmpq_init(c_re);
    fmpq_init(c_im);
    fmpq_init(hw);
    box_get_center_halfwidth_fmpq( c_re, c_im, hw, b );
    
    int res = 1;
    res = res && ( (!domain_is_lre_finite(d)) || _box_has_real_part_gt_fmpq( c_re, c_im, hw, domain_lreref(d) ));
    res = res && ( (!domain_is_ure_finite(d)) || _box_has_real_part_lt_fmpq( c_re, c_im, hw, domain_ureref(d) ));
    res = res && ( (!domain_is_lim_finite(d)) || _box_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_limref(d) ));
    res = res && ( (!domain_is_uim_finite(d)) || _box_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_uimref(d) ));
    
    fmpq_clear(c_re);
    fmpq_clear(c_im);
    fmpq_clear(hw);
    
    return res;
}

int domain_is_box_inside_inflated_domain( const domain_t d, const box_t b ) {
    
    if (domain_finiteref(d)==0)
        return 1;
    
    fmpq_t c_re, c_im, hw;
    fmpq_init(c_re);
    fmpq_init(c_im);
    fmpq_init(hw);
    box_get_center_halfwidth_fmpq( c_re, c_im, hw, b );
    
    int res = 1;
    res = res && ( (!domain_is_lre_finite(d)) || _box_has_real_part_gt_fmpq( c_re, c_im, hw, domain_inf_lreref(d) ));
    res = res && ( (!domain_is_ure_finite(d)) || _box_has_real_part_lt_fmpq( c_re, c_im, hw, domain_inf_ureref(d) ));
    res = res && ( (!domain_is_lim_finite(d)) || _box_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_inf_limref(d) ));
    res = res && ( (!domain_is_uim_finite(d)) || _box_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_inf_uimref(d) ));
    
    fmpq_clear(c_re);
    fmpq_clear(c_im);
    fmpq_clear(hw);
    
    return res;
}
// 
// int domain_is_box_CoCo_contBox_outside_domain( const domain_t d, const box_CoCo_t cc, const int reverse ){
//     
//     if (domain_finiteref(d)==0)
//         return 0;
//     
//     fmpq_t c_re, c_im, hw;
//     fmpq_init(c_re);
//     fmpq_init(c_im);
//     fmpq_init(hw);
//     box_CoCo_get_center_halfwidth_fmpq( c_re, c_im, hw, cc );
//     
//     int res = 0;
//     if (reverse == HEFPOLY_INSIDE) {
//         res = res || (domain_is_lre_finite(d) && _box_has_real_part_lt_fmpq( c_re, c_im, hw, domain_lreref(d) ));
//         res = res || (domain_is_ure_finite(d) && _box_has_real_part_gt_fmpq( c_re, c_im, hw, domain_ureref(d) ));
//         res = res || (domain_is_lim_finite(d) && _box_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_limref(d) ));
//         res = res || (domain_is_uim_finite(d) && _box_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_uimref(d) ));
//     } else {
//         res = res || (domain_is_lre_finite(d) && _box_inv_has_real_part_lt_fmpq( c_re, c_im, hw, domain_lreref(d) ));
//         res = res || (domain_is_ure_finite(d) && _box_inv_has_real_part_gt_fmpq( c_re, c_im, hw, domain_ureref(d) ));
//         res = res || (domain_is_lim_finite(d) && _box_inv_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_limref(d) ));
//         res = res || (domain_is_uim_finite(d) && _box_inv_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_uimref(d) ));
//     }
//     
//     fmpq_clear(c_re);
//     fmpq_clear(c_im);
//     fmpq_clear(hw);
//     
//     return res;
//     
// }
// 
// int domain_is_box_CoCo_contBox_outside_inflated_domain( const domain_t d, const box_CoCo_t cc, const int reverse ){
//     
//     if (domain_finiteref(d)==0)
//         return 0;
//     
//     fmpq_t c_re, c_im, hw;
//     fmpq_init(c_re);
//     fmpq_init(c_im);
//     fmpq_init(hw);
//     box_CoCo_get_center_halfwidth_fmpq( c_re, c_im, hw, cc );
//     
//     int res = 0;
//     if (reverse == HEFPOLY_INSIDE) {
//         res = res || (domain_is_lre_finite(d) && _box_has_real_part_lt_fmpq( c_re, c_im, hw, domain_inf_lreref(d) ));
//         res = res || (domain_is_ure_finite(d) && _box_has_real_part_gt_fmpq( c_re, c_im, hw, domain_inf_ureref(d) ));
//         res = res || (domain_is_lim_finite(d) && _box_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_inf_limref(d) ));
//         res = res || (domain_is_uim_finite(d) && _box_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_inf_uimref(d) ));
//     } else {
//         res = res || (domain_is_lre_finite(d) && _box_inv_has_real_part_lt_fmpq( c_re, c_im, hw, domain_inf_lreref(d) ));
//         res = res || (domain_is_ure_finite(d) && _box_inv_has_real_part_gt_fmpq( c_re, c_im, hw, domain_inf_ureref(d) ));
//         res = res || (domain_is_lim_finite(d) && _box_inv_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_inf_limref(d) ));
//         res = res || (domain_is_uim_finite(d) && _box_inv_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_inf_uimref(d) ));
//     }
//     
//     fmpq_clear(c_re);
//     fmpq_clear(c_im);
//     fmpq_clear(hw);
//     
//     return res;
//     
// }
// 
// int domain_is_box_CoCo_contBox_inside_domain( const domain_t d, const box_CoCo_t cc, const int reverse ) {
//     
//     if (domain_finiteref(d)==0)
//         return 1;
//     
//     fmpq_t c_re, c_im, hw;
//     fmpq_init(c_re);
//     fmpq_init(c_im);
//     fmpq_init(hw);
//     box_CoCo_get_center_halfwidth_fmpq( c_re, c_im, hw, cc );
//     
//     int res = 1;
//     if (reverse == HEFPOLY_INSIDE) {
//         res = res && ( (!domain_is_lre_finite(d)) || _box_has_real_part_gt_fmpq( c_re, c_im, hw, domain_lreref(d) ));
//         res = res && ( (!domain_is_ure_finite(d)) || _box_has_real_part_lt_fmpq( c_re, c_im, hw, domain_ureref(d) ));
//         res = res && ( (!domain_is_lim_finite(d)) || _box_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_limref(d) ));
//         res = res && ( (!domain_is_uim_finite(d)) || _box_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_uimref(d) ));
//     } else {
//         res = res && ( (!domain_is_lre_finite(d)) || _box_inv_has_real_part_gt_fmpq( c_re, c_im, hw, domain_lreref(d) ));
//         res = res && ( (!domain_is_ure_finite(d)) || _box_inv_has_real_part_lt_fmpq( c_re, c_im, hw, domain_ureref(d) ));
//         res = res && ( (!domain_is_lim_finite(d)) || _box_inv_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_limref(d) ));
//         res = res && ( (!domain_is_uim_finite(d)) || _box_inv_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_uimref(d) ));
//     }
//     
//     fmpq_clear(c_re);
//     fmpq_clear(c_im);
//     fmpq_clear(hw);
//     
//     return res;
// }
// 
// int domain_is_box_CoCo_contBox_inside_inflated_domain( const domain_t d, const box_CoCo_t cc, const int reverse ) {
//     
//     if (domain_finiteref(d)==0)
//         return 1;
//     
//     fmpq_t c_re, c_im, hw;
//     fmpq_init(c_re);
//     fmpq_init(c_im);
//     fmpq_init(hw);
//     box_CoCo_get_center_halfwidth_fmpq( c_re, c_im, hw, cc );
//     
//     int res = 1;
//     if (reverse == HEFPOLY_INSIDE) {
//         res = res && ( (!domain_is_lre_finite(d)) || _box_has_real_part_gt_fmpq( c_re, c_im, hw, domain_inf_lreref(d) ));
//         res = res && ( (!domain_is_ure_finite(d)) || _box_has_real_part_lt_fmpq( c_re, c_im, hw, domain_inf_ureref(d) ));
//         res = res && ( (!domain_is_lim_finite(d)) || _box_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_inf_limref(d) ));
//         res = res && ( (!domain_is_uim_finite(d)) || _box_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_inf_uimref(d) ));
//     } else {
//         res = res && ( (!domain_is_lre_finite(d)) || _box_inv_has_real_part_gt_fmpq( c_re, c_im, hw, domain_inf_lreref(d) ));
//         res = res && ( (!domain_is_ure_finite(d)) || _box_inv_has_real_part_lt_fmpq( c_re, c_im, hw, domain_inf_ureref(d) ));
//         res = res && ( (!domain_is_lim_finite(d)) || _box_inv_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_inf_limref(d) ));
//         res = res && ( (!domain_is_uim_finite(d)) || _box_inv_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_inf_uimref(d) ));
//     }
//     
//     fmpq_clear(c_re);
//     fmpq_clear(c_im);
//     fmpq_clear(hw);
//     
//     return res;
// }
// 
// int domain_is_box_CoCo_inflated_contBox_inside_inflated_domain( const domain_t d, const box_CoCo_t cc, const ulong factor, const int reverse ){
//     
//     if (domain_finiteref(d)==0)
//         return 1;
//     
//     fmpq_t c_re, c_im, hw;
//     fmpq_init(c_re);
//     fmpq_init(c_im);
//     fmpq_init(hw);
//     box_CoCo_get_center_halfwidth_fmpq( c_re, c_im, hw, cc );
//     /* inflate */
//     fmpq_mul_ui(hw, hw, factor);
//     
//     int res = 1;
//     if (reverse == HEFPOLY_INSIDE) {
//         res = res && ( (!domain_is_lre_finite(d)) || _box_has_real_part_gt_fmpq( c_re, c_im, hw, domain_inf_lreref(d) ));
//         res = res && ( (!domain_is_ure_finite(d)) || _box_has_real_part_lt_fmpq( c_re, c_im, hw, domain_inf_ureref(d) ));
//         res = res && ( (!domain_is_lim_finite(d)) || _box_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_inf_limref(d) ));
//         res = res && ( (!domain_is_uim_finite(d)) || _box_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_inf_uimref(d) ));
//     } else {
//         res = res && ( (!domain_is_lre_finite(d)) || _box_inv_has_real_part_gt_fmpq( c_re, c_im, hw, domain_inf_lreref(d) ));
//         res = res && ( (!domain_is_ure_finite(d)) || _box_inv_has_real_part_lt_fmpq( c_re, c_im, hw, domain_inf_ureref(d) ));
//         res = res && ( (!domain_is_lim_finite(d)) || _box_inv_has_imag_part_gt_fmpq( c_re, c_im, hw, domain_inf_limref(d) ));
//         res = res && ( (!domain_is_uim_finite(d)) || _box_inv_has_imag_part_lt_fmpq( c_re, c_im, hw, domain_inf_uimref(d) ));
//     }
//     
//     fmpq_clear(c_re);
//     fmpq_clear(c_im);
//     fmpq_clear(hw);
//     
//     return res;
// }
// 

int domain_is_box_CoCo_outside_domain( const domain_t d, const box_CoCo_t cc ){
    /* a box_CoCo is outside the domain iff all its boxes are outside the domain */
    int res = 1;
    box_list_iterator it = box_list_begin(box_CoCo_boxesref(cc));
    while( res && (it != box_list_end()) ){
        res = domain_is_box_outside_domain( d, box_list_elmt(it) );
        it = box_list_next(it);
    }
    
    return res;
}

// int domain_is_box_CoCo_outside_inflated_domain( const domain_t d, const box_CoCo_t cc, const int reverse ){
//     /* a box_CoCo is outside the domain iff all its boxes are outside the domain */
//     int res = 1;
//     box_list_iterator it = box_list_begin(box_CoCo_boxesref(cc));
//     while( res && (it != box_list_end()) ){
//         res = domain_is_box_outside_inflated_domain( d, box_list_elmt(it), reverse );
//         it = box_list_next(it);
//     }
//     
//     return res;
// }
// 
int domain_is_box_CoCo_inside_domain( const domain_t d, const box_CoCo_t cc ){
    /* a box_CoCo is inside the domain iff all its boxes are inside the domain */
    int res = 1;
    box_list_iterator it = box_list_begin(box_CoCo_boxesref(cc));
    while( res && (it != box_list_end()) ){
        res = domain_is_box_inside_domain( d, box_list_elmt(it) );
        it = box_list_next(it);
    }
    
    return res;
}

int domain_is_box_CoCo_inside_inflated_domain( const domain_t d, const box_CoCo_t cc ){
    /* a box_CoCo is inside the domain iff all its boxes are inside the domain */
    int res = 1;
    box_list_iterator it = box_list_begin(box_CoCo_boxesref(cc));
    while( res && (it != box_list_end()) ){
        res = domain_is_box_inside_inflated_domain( d, box_list_elmt(it) );
        it = box_list_next(it);
    }
    
    return res;
}

/* returns 0 if a contains q, -1 if a<q, 1 if a>q */
int arb_cmp_fmpq( const arb_t a, const fmpq_t q ) {
    if ( arb_contains_fmpq(a, q) )
        return 0;
    arb_t qp;
    arb_init(qp);
    slong prec = PWPOLY_DEFAULT_PREC;
    arb_set_fmpq(qp, q, prec);
    while ( arb_overlaps(a, qp) ) {
        prec = 2*prec;
        arb_set_fmpq(qp, q, prec);
    }
    int res = arb_lt(a, qp);
    res = (res==1? -1: 1);
    arb_clear(qp);
    return res;
}

int domain_acb_intersect_domain( const domain_t d, const acb_t b ){
    if ( domain_is_C(d) )
        return 1;
    
    int res = 1;
    res = res && ( (!domain_is_lre_finite(d)) || ( arb_cmp_fmpq( acb_realref(b), domain_lreref(d) ) >= 0 ) );
    res = res && ( (!domain_is_ure_finite(d)) || ( arb_cmp_fmpq( acb_realref(b), domain_ureref(d) ) <= 0 ) );
    res = res && ( (!domain_is_lim_finite(d)) || ( arb_cmp_fmpq( acb_imagref(b), domain_limref(d) ) >= 0 ) );
    res = res && ( (!domain_is_uim_finite(d)) || ( arb_cmp_fmpq( acb_imagref(b), domain_uimref(d) ) <= 0 ) );
    
    return res;
}


int domain_acb_is_strictly_in_domain( const domain_t d, const acb_t b ){
    if ( domain_is_C(d) )
        return 1;
    
    int res = 1;
    res = res && ( (!domain_is_lre_finite(d)) || ( arb_cmp_fmpq( acb_realref(b), domain_lreref(d) ) > 0 ) );
    res = res && ( (!domain_is_ure_finite(d)) || ( arb_cmp_fmpq( acb_realref(b), domain_ureref(d) ) < 0 ) );
    res = res && ( (!domain_is_lim_finite(d)) || ( arb_cmp_fmpq( acb_imagref(b), domain_limref(d) ) > 0 ) );
    res = res && ( (!domain_is_uim_finite(d)) || ( arb_cmp_fmpq( acb_imagref(b), domain_uimref(d) ) < 0 ) );
    
    return res;
}

// /* returns 1 => Re(disc(center, radius)) < a */ 
// int _acb_disc_has_real_part_lt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec ){
//     arb_t temp;
//     arb_init(temp);
//     arb_set_fmpq(temp, a, prec);
//     arb_sub(temp, temp, acb_realref(center), prec);
//     arb_sub(temp, temp, radius, prec); /* temp = a - Re(center) - radius */
//                                        /* temp > 0 <=> a > Re(center) + radius */
//     int res = arb_is_positive( temp );
//     arb_clear(temp);
//     return res;
// }
// /* returns 1 => Re(disc(center, radius)) > a */ 
// int _acb_disc_has_real_part_gt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec ){
//     arb_t temp;
//     arb_init(temp);
//     arb_set_fmpq(temp, a, prec);
//     arb_sub(temp, temp, acb_realref(center), prec);
//     arb_add(temp, temp, radius, prec); /* temp = a - Re(center) + radius */
//                                        /* temp < 0 <=> a < Re(center) - radius */
//     int res = arb_is_negative( temp );
//     arb_clear(temp);
//     return res;
// }
// /* returns 1 => Im(disc(center, radius)) < a */ 
// int _acb_disc_has_imag_part_lt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec ){
//     arb_t temp;
//     arb_init(temp);
//     arb_set_fmpq(temp, a, prec);
//     arb_sub(temp, temp, acb_imagref(center), prec);
//     arb_sub(temp, temp, radius, prec); /* temp = a - Im(center) - radius */
//                                        /* temp > 0 <=> a > Im(center) + radius */
//     int res = arb_is_positive( temp );
//     arb_clear(temp);
//     return res;
// }
// /* returns 1 => Im(disc(center, radius)) > a */ 
// int _acb_disc_has_imag_part_gt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec ){
//     arb_t temp;
//     arb_init(temp);
//     arb_set_fmpq(temp, a, prec);
//     arb_sub(temp, temp, acb_imagref(center), prec);
//     arb_add(temp, temp, radius, prec); /* temp = a - Im(center) + radius */
//                                        /* temp < 0 <=> a < Im(center) - radius */
//     int res = arb_is_negative( temp );
//     arb_clear(temp);
//     return res;
// }
// 
// /* returns 1 => disc(center, radius) is strictly outside disc( cre+Icim, rad) */
// int _acb_disc_strictly_outside_fmpq_disc( const acb_t center, const arb_t radius, 
//                                           const fmpq_t cre, const fmpq_t cim, const fmpq_t rad, 
//                                           slong prec ) {
//     acb_t temp;
//     acb_init(temp);
//     /* translate center of fmpq disc to zero */
//     arb_set_fmpq(acb_realref(temp), cre, prec);
//     arb_set_fmpq(acb_imagref(temp), cim, prec);
//     acb_sub(temp, center, temp, prec);
//     /* get distance from zero to translated center */
//     acb_abs( acb_realref(temp), temp, prec );
//     /* add the two radii */
//     arb_set_fmpq(acb_imagref(temp), rad, prec );
//     arb_add( acb_imagref(temp), acb_imagref(temp), radius, prec );
//     /* see if dist between two center is > sum of 2 radii */
//     int res = arb_gt( acb_realref(temp), acb_imagref(temp) );
//     acb_clear(temp);
//   
//     return res;
// }
// 
// /* returns 1 => disc(center, radius) is strictly inside disc( cre+Icim, rad) */
// int _acb_disc_strictly_inside_fmpq_disc( const acb_t center, const arb_t radius, 
//                                          const fmpq_t cre, const fmpq_t cim, const fmpq_t rad, 
//                                          slong prec ) {
//     acb_t temp;
//     acb_init(temp);
//     /* translate center of fmpq disc to zero */
//     arb_set_fmpq(acb_realref(temp), cre, prec);
//     arb_set_fmpq(acb_imagref(temp), cim, prec);
//     acb_sub(temp, center, temp, prec);
//     /* get distance from zero to translated center */
//     acb_abs( acb_realref(temp), temp, prec );
//     /* add radius to that distance */
//     arb_add( acb_realref(temp), acb_realref(temp), radius, prec );
//     /* see if abs+radius is < rad */
//     arb_set_fmpq(acb_imagref(temp), rad, prec );
//     int res = arb_lt( acb_realref(temp), acb_imagref(temp) );
//     acb_clear(temp);
//   
//     return res;
// }
// 
// // #define DEBUG_DOMAIN
// /* alpha\in d=Disc(center, radius) s.t. Re(1/alpha) < a */ 
// /* <=> if a=0: Re(alpha) < 0 */
// /*     if a>0: check if d is strictly outside the disc of center (1/(2a)) + 0*I and radius (1/(2a)) */
// /*     if a<0: check if d is strictly inside  the disc of center (1/(2a)) + 0*I and radius (1/(2a)) */
// 
// int _acb_disc_inv_has_real_part_lt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec ) {
// 
// #ifdef DEBUG_DOMAIN
//     printf(" inv has real part < "); fmpq_print(a);
// #endif    
//     fmpq_t cen, rad, zero;
//     fmpq_init(cen);
//     fmpq_init(rad);
//     fmpq_init(zero);
//     fmpq_zero(zero);
//     int res = 0;
//     if (fmpq_is_zero(a)) { /* verify center+radius < 0 */
//         res = _acb_disc_has_real_part_lt_fmpq( center, radius, a, prec );
// #ifdef DEBUG_DOMAIN
//         printf(" <=> Re(disc) < 0 <=> Re(center) + radius < 0: %d", res);
// #endif
//     } else { 
//         fmpq_mul_si(cen, a, 2);
//         fmpq_inv(cen, cen);
//         if ( fmpq_cmp_si( a, 0 ) > 0 ) { /* a > 0: */
//             fmpq_set(rad, cen);
//             res = _acb_disc_strictly_outside_fmpq_disc( center, radius, cen, zero, rad, prec );
// #ifdef DEBUG_DOMAIN
//             printf(" <=> disc is strictly outside D("); fmpq_print(cen); printf("+0I, "); fmpq_print(rad); printf("): %d", res);
// #endif
//         } else { /* a < 0: */
//             fmpq_neg(rad, cen);
//             res = _acb_disc_strictly_inside_fmpq_disc( center, radius, cen, zero, rad, prec );
// #ifdef DEBUG_DOMAIN
//             printf(" <=> disc is strictly inside D("); fmpq_print(cen); printf("+0I, "); fmpq_print(rad); printf("): %d", res);
// #endif
//         }
//         
//     }
//     fmpq_clear(zero);
//     fmpq_clear(rad);
//     fmpq_clear(cen);
//     return res;
// }
// 
// /* alpha\in d=Disc(center, radius) s.t. Re(1/alpha) < a */ 
// /* <=> if a=0: Re(alpha) > 0 */
// /*     if a>0: check if d is strictly inside  the disc of center (1/(2a)) + 0*I and radius (1/(2a)) */
// /*     if a<0: check if d is strictly outside the disc of center (1/(2a)) + 0*I and radius (1/(2a)) */
// int _acb_disc_inv_has_real_part_gt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec ) {
//    
// #ifdef DEBUG_DOMAIN
//     printf(" inv has real part > "); fmpq_print(a);
// #endif
//     
//     fmpq_t cen, rad, zero;
//     fmpq_init(cen);
//     fmpq_init(rad);
//     fmpq_init(zero);
//     fmpq_zero(zero);
//     int res = 0;
//     if (fmpq_is_zero(a)) { /* verify center-radius > 0 */
//         res = _acb_disc_has_real_part_gt_fmpq( center, radius, a, prec );
// #ifdef DEBUG_DOMAIN
//         printf(" <=> Re(disc) > 0 <=> Re(center) - radius > 0: %d", res);
// #endif
//     } else { 
//         fmpq_mul_si(cen, a, 2);
//         fmpq_inv(cen, cen);
//         if ( fmpq_cmp_si( a, 0 ) > 0 ) { /* a > 0: */
//             fmpq_set(rad, cen);
//             res = _acb_disc_strictly_inside_fmpq_disc( center, radius, cen, zero, rad, prec );
// #ifdef DEBUG_DOMAIN
//             printf(" <=> disc is strictly inside D("); fmpq_print(cen); printf("+0I, "); fmpq_print(rad); printf("): %d", res);
// #endif
//         } else { /* a < 0: */
//             fmpq_neg(rad, cen);
//             res = _acb_disc_strictly_outside_fmpq_disc( center, radius, cen, zero, rad, prec );
// #ifdef DEBUG_DOMAIN
//             printf(" <=> disc is strictly outside D("); fmpq_print(cen); printf("+0I, "); fmpq_print(rad); printf("): %d", res);
// #endif
//         }
//         
//     }
//     fmpq_clear(zero);
//     fmpq_clear(rad);
//     fmpq_clear(cen);
//     return res;
// }
// 
// /* alpha\in d=Disc(center, radius) s.t. Im(1/alpha) < a */ 
// /* <=> if a=0: -Im(alpha) < 0 <=> Im(alpha)>0 */
// /*     if a>0: check if d is strictly outside the disc of center 0 - I*(1/(2a)) and radius (1/(2a)) */
// /*     if a<0: check if d is strictly inside  the disc of center 0 - I*(1/(2a)) and radius (1/(2a)) */
// int _acb_disc_inv_has_imag_part_lt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec ) {
// 
// #ifdef DEBUG_DOMAIN
//     printf(" inv has imag part < "); fmpq_print(a);
// #endif
//     
//     fmpq_t cen, rad, zero;
//     fmpq_init(cen);
//     fmpq_init(rad);
//     fmpq_init(zero);
//     fmpq_zero(zero);
//     int res = 0;
//     if (fmpq_is_zero(a)) { /* verify center-radius > 0 */
//         res = _acb_disc_has_imag_part_gt_fmpq( center, radius, a, prec );
// #ifdef DEBUG_DOMAIN
//         printf(" <=> Im(disc) > 0 <=> Im(center) - radius > 0: %d", res);
// #endif
//     } else { 
//         fmpq_mul_si(cen, a, -2);
//         fmpq_inv(cen, cen);
//         if ( fmpq_cmp_si( a, 0 ) > 0 ) { /* a > 0: */
//             fmpq_neg(rad, cen);
//             res = _acb_disc_strictly_outside_fmpq_disc( center, radius, zero, cen, rad, prec );
// #ifdef DEBUG_DOMAIN
//             printf(" <=> disc is strictly outside D(0+"); fmpq_print(cen); printf("I, "); fmpq_print(rad); printf("): %d", res);
// #endif
//         } else { /* a < 0: */
//             fmpq_set(rad, cen);
//             res = _acb_disc_strictly_inside_fmpq_disc( center, radius, zero, cen, rad, prec );
// #ifdef DEBUG_DOMAIN
//             printf(" <=> disc is strictly inside D(0+"); fmpq_print(cen); printf("I, "); fmpq_print(rad); printf("): %d", res);
// #endif
//         }
//         
//     }
//     fmpq_clear(zero);
//     fmpq_clear(rad);
//     fmpq_clear(cen);
//     return res;
// }
// 
// /* alpha\in d=Disc(center, radius) s.t. Im(1/alpha) > a */ 
// /* <=> if a=0: -Im(alpha) > 0 <=> Im(alpha)<0 */
// /*     if a>0: check if d is strictly inside  the disc of center 0 - I*(1/(2a)) and radius (1/(2a)) */
// /*     if a<0: check if d is strictly outside the disc of center 0 - I*(1/(2a)) and radius (1/(2a)) */
// int _acb_disc_inv_has_imag_part_gt_fmpq( const acb_t center, const arb_t radius, const fmpq_t a, slong prec ) {
// 
// #ifdef DEBUG_DOMAIN
//     printf(" inv has imag part > "); fmpq_print(a);
// #endif
//     
//     fmpq_t cen, rad, zero;
//     fmpq_init(cen);
//     fmpq_init(rad);
//     fmpq_init(zero);
//     fmpq_zero(zero);
//     int res = 0;
//     if (fmpq_is_zero(a)) { /* verify center-radius > 0 */
//         res = _acb_disc_has_imag_part_lt_fmpq( center, radius, a, prec );
// #ifdef DEBUG_DOMAIN
//         printf(" <=> Im(disc) < 0 <=> Im(center) + radius < 0: %d", res);
// #endif
//     } else { 
//         fmpq_mul_si(cen, a, -2);
//         fmpq_inv(cen, cen);
//         if ( fmpq_cmp_si( a, 0 ) > 0 ) { /* a > 0: */
//             fmpq_neg(rad, cen);
//             res = _acb_disc_strictly_inside_fmpq_disc( center, radius, zero, cen, rad, prec );
// #ifdef DEBUG_DOMAIN
//             printf(" <=> disc is strictly inside D(0+"); fmpq_print(cen); printf("I, "); fmpq_print(rad); printf("): %d", res);
// #endif
//         } else { /* a < 0: */
//             fmpq_set(rad, cen);
//             res = _acb_disc_strictly_outside_fmpq_disc( center, radius, zero, cen, rad, prec );
// #ifdef DEBUG_DOMAIN
//             printf(" <=> disc is strictly outside D(0+"); fmpq_print(cen); printf("I, "); fmpq_print(rad); printf("): %d", res);
// #endif
//         }
//         
//     }
//     fmpq_clear(zero);
//     fmpq_clear(rad);
//     fmpq_clear(cen);
//     return res;
// }
//

/* returns 1 => disc(center, radius) is strictly outside domain */
int domain_is_acb_disc_strictly_outside_domain( const domain_t d, const acb_t center, const arb_t radius, slong prec ){
    
    /* get the point of the domain minimizing the distance to center */
    acb_t p;
    arb_t t;
    acb_init(p);
    arb_init(t);
    
    acb_set(p, center);
    if ( domain_is_lre_finite(d) ){
        arb_set_fmpq(t, domain_lreref(d), prec);
        arb_max(acb_realref(p), acb_realref(p), t, prec);
    } 
    if ( domain_is_ure_finite(d) ){
        arb_set_fmpq(t, domain_ureref(d), prec);
        arb_min(acb_realref(p), acb_realref(p), t, prec);
    }
    if ( domain_is_lim_finite(d) ){
        arb_set_fmpq(t, domain_limref(d), prec);
        arb_max(acb_imagref(p), acb_imagref(p), t, prec);
    } 
    if ( domain_is_uim_finite(d) ){
        arb_set_fmpq(t, domain_uimref(d), prec);
        arb_min(acb_imagref(p), acb_imagref(p), t, prec);
    }
    /* get the square of its distance to center */
    acb_sub( p, p, center, prec );
    arb_sqr(acb_realref(p), acb_realref(p), prec );
    arb_sqr(acb_imagref(p), acb_imagref(p), prec );
    arb_add(acb_realref(p), acb_realref(p), acb_imagref(p), prec );
    /* the domain is outside the disc <=> the point of the domain */
    /* minimizing the distance to the centre of the circle */
    /* has such distance > radius */
    arb_sqr(t, radius, prec );
    int res = arb_gt(acb_realref(p), t);
    
    acb_clear(p);
    arb_clear(t);
    return res;
    
}

/* assume r,R are >=0 */
/* returns 1 => A(0,r,R) has empty intersection with the domain */
int domain_acb_annulus_empty_intersection( const domain_t d, const arb_t r, const arb_t R, slong prec ){
    acb_t zero;
    acb_init(zero);
    acb_zero(zero);
//     printf("A(0, "); arb_printd(r, 10); printf(", "); arb_printd(R, 10); printf(")");
    int res = domain_is_acb_disc_strictly_outside_domain(d, zero, R, prec);
//     printf("res: %d\n", res);
    if ( (!res) && domain_is_finite(d) && (arb_is_positive(r)) ) {
        /* D(0,R) overlaps domain */
        /* the domain is compact */
        /* see if domain is strictly included in D(0,r) */ 
        fmpq_t pre, pim, abs;
        fmpq_init(pre);
        fmpq_init(pim);
        fmpq_init(abs);
        arb_t sqrr, norm;
        arb_init(sqrr);
        arb_init(norm);
        /* get the point of the domain maximizing the two norm */
        fmpq_abs(pre, domain_lreref(d));
        fmpq_abs(abs, domain_ureref(d));
        if (fmpq_cmp(abs, pre) > 0)
            fmpq_set(pre, abs);
        fmpq_abs(pim, domain_limref(d));
        fmpq_abs(abs, domain_uimref(d));
        if (fmpq_cmp(abs, pim) > 0)
            fmpq_set(pim, abs);
        /* get its norm */
        get_2norm_square( abs, pre, pim );
        arb_set_fmpq(norm, abs, prec);
        /*the domain is strictly inside D(0,r) <=>*/
        /*sqr of norm of the point of the domain maximizing the norm */
        /* is < r^2 */
        arb_sqr(sqrr, r, prec);
        res = arb_lt( norm, sqrr);
        fmpq_clear(pre);
        fmpq_clear(pim);
        fmpq_clear(abs);
        arb_clear(sqrr);
        arb_clear(norm);
    }
    acb_clear(zero);
    return res;
}
