/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef BOX_H
#define BOX_H

#include "pw_base.h"
#include "list/pw_list.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    fmpz       depth; /* integer,                  log of the radius of the box */
    fmpz       creal; /* an integer in [ -2^(depth -1) -1, 2^(depth-1) -1 + 1 ] */
    fmpz       cimag; /* an integer in [ -2^(depth -1) -1, 2^(depth-1) -1 + 1 ] */
    acb_struct b_acb; /* the box as an acb; always initialized but not always filled */
                      /*                    when filled, it is EXACT, independently of precision */
    
    slong      pprec; /* arithmetic precision used for parent box; PWPOLY_DEFAULT_PREC by default */ 
    slong      nbMro; /* an upper bound on the number of root in the containinf disc of the box; -1 if not known */
    acb_struct peval; /* value of pol on parent box or box itself */
    /* test */
    slong      nbRin; /*nb of roots contained in the box */
    slong      lenRI; /* if nbRin>0, the initialized length of roInt */
    acb_ptr    roInt; /*a table of nbRin roots in the box   */
                      /*initialized only if nbRin > 0, otherwise NULL */
} box;

typedef box box_t[1];
typedef box * box_ptr;

#define box_depthref(X) ( &(X)->depth )
#define box_crealref(X) ( &(X)->creal )
#define box_cimagref(X) ( &(X)->cimag )
#define box_b_acbref(X) ( &(X)->b_acb )

#define box_pprecref(X) (  (X)->pprec )
#define box_nbMroref(X) (  (X)->nbMro )
#define box_pevalref(X) ( &(X)->peval )

#define box_nbRinref(X) (  (X)->nbRin )
#define box_lenRIref(X) (  (X)->lenRI )
#define box_roIntref(X) (  (X)->roInt )

void box_init(box_t b);
void box_clear(box_t b);

// /* !!! sets the depth to -depth */
// void box_set_ui_si_si(box_t b, ulong depth, slong creal, slong cimag);

/* !!! sets the depth to depth */
void box_set_si_si_si(box_t b, slong depth, slong creal, slong cimag);

/* set r to the complex ball of real and imag radii 2^(-depth)                    */
/*                           and center ((2*creal+1) + sqrt(-1)*(2*cimag+1))*2^(-depth) */
void box_get_acb(acb_t r, const box_t box);

void box_set(box_t dest, const box_t src);

/* printing */
#ifndef PW_SILENT
void box_fprint(FILE * file, const box_t x);

PWPOLY_INLINE void box_print (const box_t b) {
    box_fprint(stdout, b);
}
#endif
/* Precondition:  b1 and b2 have disjoint centers and same depth                               */
/* Specification: returns 1 if b1 and b2 are connected (8 adjacency),                          */
/*                        0 otherwise                                                          */
int box_are_8connected ( const box_t b1, const box_t b2 );

/*Specification: returns true iff box is completely outside unit disc */
int box_is_outside_unit_disc ( const box_t b );
/*Specification: returns true iff box is completely  inside unit disc */
int box_is_inside_unit_disc ( const box_t b );

void box_get_center_halfwidth_fmpq( fmpq_t c_re, fmpq_t c_im, fmpq_t hw, const box_t b );
void box_get_width( fmpq_t width, const box_t b );

/* ordering */
int box_isless( const box_t b1, const box_t b2 );
int box_equals( const box_t b1, const box_t b2 );

int box_is_outside_unit_inflated       ( box_t b, double inflation );
int box_is_outside_unit_inflated_strict( box_t b, double inflation );
int box_is_inside_unit_inflated        ( box_t b, double inflation );
int box_is_inside_unit_inflated_strict ( box_t b, double inflation );

PWPOLY_INLINE int box_is_imaginary_negative( const box_t b ) {
        return ( fmpz_cmp_si ( box_cimagref(b), -1 ) <= 0 );
}

PWPOLY_INLINE int box_is_imaginary_negative_strict( const box_t b ) {
        return ( fmpz_cmp_si ( box_cimagref(b), -1 ) < 0 );
}

PWPOLY_INLINE int box_is_imaginary_positive( const box_t b ) {
        return ( fmpz_cmp_si ( box_cimagref(b), 0 ) >= 0 );
}

PWPOLY_INLINE int box_is_imaginary_positive_strict( const box_t b ) {
        return ( fmpz_cmp_si ( box_cimagref(b), 0 ) > 0 );
}

void box_conjugate( box_t dest, const box_t b );

void box_conjugate_inplace( box_t b );

/* list of boxes, sorted by increasing real part then increasing imag part */
PWPOLY_INLINE void box_clear_for_list(void * b){
    box_clear( (box_ptr) b );
}

PWPOLY_INLINE int box_isless_for_list(const void * b1, const void * b2){
    return box_isless( (box_ptr) b1, (box_ptr) b2 );
}

PWPOLY_INLINE int box_equals_for_list(const void * b1, const void * b2){
    return box_equals( (box_ptr) b1, (box_ptr) b2 );
}

#ifndef PW_SILENT
PWPOLY_INLINE void box_fprint_for_list(FILE * file, const void * b){
    box_fprint(file, (box_ptr) b);
}
#endif

typedef struct list   box_list;
typedef struct list   box_list_t[1];
typedef struct list * box_list_ptr;
    
// PWPOLY_INLINE void    box_list_init ( box_list_t l )                 { list_init(l, box_clear_for_list); }
PWPOLY_INLINE void    box_list_init ( box_list_t l )                 { list_init(l); }
PWPOLY_INLINE void    box_list_swap ( box_list_t l1, box_list_t l2 ) { list_swap(l1, l2); }
// PWPOLY_INLINE void    box_list_clear( box_list_t l )                 { list_clear(l); }
PWPOLY_INLINE void    box_list_clear( box_list_t l )                 { list_clear(l, box_clear_for_list); }
PWPOLY_INLINE void    box_list_empty( box_list_t l )                 { list_empty(l); }
PWPOLY_INLINE void    box_list_push ( box_list_t l, box_ptr b )      { list_push(l,b); }
PWPOLY_INLINE void    box_list_push_front ( box_list_t l, box_ptr b ){ list_push_front(l,b); }
PWPOLY_INLINE box_ptr box_list_pop  ( box_list_t l )                 { return (box_ptr) list_pop(l); }
PWPOLY_INLINE box_ptr box_list_first( box_list_t l )                 { return (box_ptr) list_first(l); }
PWPOLY_INLINE box_ptr box_list_last ( box_list_t l )                 { return (box_ptr) list_last(l); }
PWPOLY_INLINE void    box_list_insert_sorted ( box_list_t l, box_ptr b ) { list_insert_sorted(l, b, box_isless_for_list); }
// PWPOLY_INLINE int     box_list_insert_sorted_unique ( box_list_t l, box_ptr b ) { return list_insert_sorted_unique(l, b, box_isless_for_list); }
#ifndef PW_SILENT
PWPOLY_INLINE void    box_list_fprint( FILE * file, box_list_t l )   { list_fprint(file, l, box_fprint_for_list); }
PWPOLY_INLINE void    box_list_print (              box_list_t l )   { box_list_fprint(stdout, l); }
#endif
PWPOLY_INLINE int     box_list_is_empty  ( box_list_t l )            { return list_is_empty(l); }
PWPOLY_INLINE slong   box_list_get_size  ( const box_list_t l )            { return list_get_size(l); }

/*iterator */
typedef list_iterator box_list_iterator;
PWPOLY_INLINE box_list_iterator box_list_begin(const box_list_t l)   { return list_begin(l); }
PWPOLY_INLINE box_list_iterator box_list_endEl(const box_list_t l)   { return list_endEl(l); }
PWPOLY_INLINE box_list_iterator box_list_next(box_list_iterator it)  { return list_next(it); }
PWPOLY_INLINE box_list_iterator box_list_prev(box_list_iterator it)  { return list_prev(it); }
PWPOLY_INLINE box_list_iterator box_list_end()                       { return list_end(); }
PWPOLY_INLINE box_ptr           box_list_elmt(box_list_iterator it)  { return (box_ptr) list_elmt(it); }

/* quadrisection */
/* insert sub-boxes to the input list */
/* assume input list is sorted */
void box_quadrisect( box_list_t l, box_t b );
void box_list_quadrisect( box_list_t subboxes, box_list_t boxes );

/* insert sub-boxes to the input list */
void box_quadrisect_not_sorted( box_list_t l, box_t b );

/* OLD SPECIF */
/* assume rad(b) < 1/2 */
/* returns a list of at most 4 boxes */
/* so that the re-union of those boxes contains b */
/* and each box has radius r s.t. r/2 <=rad(b) < r */
/* NEW SPECIF */
/* assume rad(b) < 1/2 ... is it necessary? I do not think so*/
/* returns a list of at most 9 boxes of the subdivision three */
/* so that the re-union of those boxes contains b */
/* and each box has radius r s.t. rad(b)/2 < r <= rad(b) < 2r */
/* thus the union of boxes is contained in 3b */
void box_get_box_list_acb( box_list_t l, const acb_t b );

slong box_get_box_list_domainfmpq( box_list_t l, const fmpq_t lre, const fmpq_t ure, const fmpq_t lim, const fmpq_t uim);

void box_get_box_list_acb2( box_list_t l, const acb_t b );
#ifdef __cplusplus
}
#endif

#endif
