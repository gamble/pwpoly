/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "box.h"

void box_init(box_t b) {
    fmpz_init( box_depthref(b) );
    fmpz_init( box_crealref(b) );
    fmpz_init( box_cimagref(b) );
    fmpz_zero( box_depthref(b) );
    fmpz_zero( box_crealref(b) );
    fmpz_zero( box_cimagref(b) );
    
    acb_init( box_b_acbref(b) );
    acb_zero( box_b_acbref(b) );
    
    box_pprecref(b) = PWPOLY_DEFAULT_PREC;
    box_nbMroref(b) = -1;
    acb_init( box_pevalref(b) );
    acb_zero( box_pevalref(b) );
    
    box_nbRinref(b) = 0;
    box_lenRIref(b) = 0;
    box_roIntref(b) = NULL;
}

void box_clear(box_t b){
    fmpz_clear( box_depthref(b) );
    fmpz_clear( box_crealref(b) );
    fmpz_clear( box_cimagref(b) );
    acb_clear ( box_b_acbref(b) );
    acb_clear ( box_pevalref(b) );
    if (box_nbRinref(b)>0)
        _acb_vec_clear( box_roIntref(b), box_lenRIref(b) );
//     box_nbRinref(b) = 0;
//     box_lenRIref(b) = 0;
}

// void box_set_ui_si_si(box_t b, ulong depth, slong creal, slong cimag){
//     fmpz_set_si( box_depthref(b), (slong) -depth );
//     fmpz_set_si( box_crealref(b), creal );
//     fmpz_set_si( box_cimagref(b), cimag );
// }

void box_set_si_si_si(box_t b, slong depth, slong creal, slong cimag){
    fmpz_set_si( box_depthref(b), depth );
    fmpz_set_si( box_crealref(b), creal );
    fmpz_set_si( box_cimagref(b), cimag );
}

/* set r to the complex ball of real and imag radii 2^(-depth)                    */
/*                           and center ((2*creal+1) + sqrt(-1)*(2*cimag+1))*2^(-depth) */
void box_get_acb(acb_t r, const box_t b) {
    
    fmpz_t one, creal, cimag;
    fmpz_init(one);
    fmpz_init(creal);
    fmpz_init(cimag);
    fmpz_mul_ui(creal, box_crealref(b), 2);
    fmpz_add_ui(creal, creal, 1);
    fmpz_mul_ui(cimag, box_cimagref(b), 2);
    fmpz_add_ui(cimag, cimag, 1);
    fmpz_one(one);
    /* set the radii to 2^(depth) */
    mag_set_fmpz_2exp_fmpz(arb_radref(acb_realref(r)), one, box_depthref(b));
    mag_set( arb_radref(acb_imagref(r)), arb_radref(acb_realref(r)) );
    
    /* set the center to ((2*creal+1) + sqrt(-1)*(2*cimag+1))*2^(depth) */
    arf_set_fmpz_2exp(arb_midref(acb_realref(r)), creal, box_depthref(b));
    arf_set_fmpz_2exp(arb_midref(acb_imagref(r)), cimag, box_depthref(b));
    
    fmpz_clear(one);
    fmpz_clear(creal);
    fmpz_clear(cimag);
}

void box_set(box_t dest, const box_t src){
    fmpz_set( box_depthref(dest), box_depthref(src) );
    fmpz_set( box_crealref(dest), box_crealref(src) );
    fmpz_set( box_cimagref(dest), box_cimagref(src) );
    acb_set ( box_b_acbref(dest), box_b_acbref(src) );
    
    box_pprecref(dest) = box_pprecref(src);
    box_nbMroref(dest) = box_nbMroref(src);
    acb_set ( box_pevalref(dest), box_pevalref(src) );
    
    if (box_nbRinref(dest)>0) {
        _acb_vec_clear( box_roIntref(dest), box_lenRIref(dest) );
        box_nbRinref(dest) = 0;
        box_lenRIref(dest) = 0;
        box_roIntref(dest) = NULL;
    }
    if (box_nbRinref(src)>0) {
        box_nbRinref(dest) = box_nbRinref(src);
        box_lenRIref(dest) = box_nbRinref(src);
        box_roIntref(dest) = _acb_vec_init( box_lenRIref(dest) );
        for (slong i=0; i<box_nbRinref(src); i++)
            acb_set( box_roIntref(dest) + i, box_roIntref(src) + i );
    }
}

/*Specification: returns true iff box is completely outside unit disc */
int box_is_outside_unit_disc ( const box_t b ){
    
    fmpz_t rv, iv;
    arf_t abs;
    fmpz_init(rv);
    fmpz_init(iv);
    arf_init(abs);
    
    if (fmpz_cmp_ui ( box_crealref(b), 0 ) >= 0 ) {
        if (fmpz_cmp_ui ( box_cimagref(b), 0 ) >= 0 ) {
            /* b in in NE quarter -> get SW vertex */
            fmpz_mul_ui(rv, box_crealref(b), 2);
            fmpz_mul_ui(iv, box_cimagref(b), 2);
        } else {
            /* b in in SE quarter -> get NW vertex */
            fmpz_mul_ui(rv, box_crealref(b), 2);
            fmpz_mul_ui(iv, box_cimagref(b), 2);
            fmpz_add_ui(iv, iv, 2);
        }
    } else {
        if (fmpz_cmp_ui ( box_cimagref(b), 0 ) >= 0 ) {
            /* b in in NW quarter -> get SE vertex */
            fmpz_mul_ui(rv, box_crealref(b), 2);
            fmpz_add_ui(rv, rv, 2);
            fmpz_mul_ui(iv, box_cimagref(b), 2);
        } else {
            /* b in in SW quarter -> get NE vertex */
            fmpz_mul_ui(rv, box_crealref(b), 2);
            fmpz_add_ui(rv, rv, 2);
            fmpz_mul_ui(iv, box_cimagref(b), 2);
            fmpz_add_ui(iv, iv, 2);
        }
    }
    
    fmpz_mul( rv, rv, rv );
    fmpz_mul( iv, iv, iv );
    fmpz_add( rv, rv, iv );
    fmpz_mul_ui( iv, box_depthref(b), 2);
    arf_set_fmpz_2exp(abs, rv, iv);
    int res = (arf_cmp_si(abs, 1)>0);
    
    fmpz_clear(rv);
    fmpz_clear(iv);
    arf_clear(abs);
    return res;
}

/*Specification: returns true iff box is completely outside unit disc */
int box_is_inside_unit_disc ( const box_t b ){
    
    /* if the depth is >= 0, the box is NOT stricly inside the unit disc */
    if (fmpz_sgn( box_depthref(b) ) >= 0 )
        return 0;
    
    /* here depth is < 0 */
    fmpz_t rv, iv, two;
    fmpz_init(rv);
    fmpz_init(iv);
    fmpz_init(two);
    
    /* get the vertex maximizing the 2-norm */
    if (fmpz_cmp_ui ( box_crealref(b), 0 ) >= 0 ) {
        /* the box is on the right of the imaginary axis */
        /* point maximizing the 2-norm is either NE or SE vertex */
        fmpz_mul_ui(rv, box_crealref(b), 2);
        fmpz_add_ui(rv, rv, 2);
    } else {
        /* the box is on the left of the imaginary axis */
        /* point maximizing the 2-norm is either NW or SW vertex */
        fmpz_mul_ui(rv, box_crealref(b), 2);
    }
    if (fmpz_cmp_ui ( box_cimagref(b), 0 ) >= 0 ) {
        /* the box is above the real axis */
        /* point maximizing the 2-norm is either NW or NE vertex */
        fmpz_mul_ui(iv, box_cimagref(b), 2);
        fmpz_add_ui(iv, iv, 2);
    } else {
        /* the box is below of the real axis */
        /* point maximizing the 2-norm is either SW or SE vertex */
        fmpz_mul_ui(iv, box_cimagref(b), 2);
    }
    /* (rv*2^d)^2 + (iv*2^d)^2 < 1 <=> (rv^2 + iv^2)*2^(2d) < 1 */
    /*                             <=> rv^2 + iv^2 < 2^(-2d)    */
    /* since d<0,                  <=> rv^2 + iv^2 < 2^(2|d|)   */
    fmpz_mul( rv, rv, rv );
    fmpz_mul( iv, iv, iv );
    fmpz_add( rv, rv, iv );
    fmpz_abs( iv, box_depthref(b) );
    fmpz_mul_ui( iv, iv, 2 );
    fmpz_set_ui(two, 2);
    fmpz_pow_fmpz( iv, two, iv );
    
    int res = (fmpz_cmp( rv, iv ) < 0);
    
    fmpz_clear(rv);
    fmpz_clear(iv);
    fmpz_clear(two);
    
    return res;
}

void box_get_center_halfwidth_fmpq( fmpq_t c_re, fmpq_t c_im, fmpq_t hw, const box_t b ){
    /* set hw to 2^-box_depthref(b) */
    if ( fmpz_cmp_si( box_depthref(b), 0 ) < 0 ){ 
        fmpz_abs( fmpq_numref( hw ), box_depthref(b) );
        fmpz_set_si(fmpq_denref( hw ), 2);
        fmpz_pow_fmpz(fmpq_denref( hw ), fmpq_denref( hw ), fmpq_numref( hw ));
        fmpz_one( fmpq_numref( hw ) );
    } else {
        fmpz_set_si(fmpq_numref( hw ), 2);
        fmpz_pow_fmpz(fmpq_numref( hw ), fmpq_numref( hw ), box_depthref(b));
        fmpz_one( fmpq_denref( hw ) );
    }
    /* set c_re to (2*creal+1)*2^(-depth) */
    fmpz_set( fmpq_numref( c_re ), box_crealref(b));
    fmpz_one( fmpq_denref( c_re ) );
    fmpq_mul_ui( c_re, c_re, 2);
    fmpq_add_ui( c_re, c_re, 1);
    fmpq_mul( c_re, c_re, hw );
    /* set c_im to (2*cimag+1)*2^(-depth) */
    fmpz_set( fmpq_numref( c_im ), box_cimagref(b));
    fmpz_one( fmpq_denref( c_im ) );
    fmpq_mul_ui( c_im, c_im, 2);
    fmpq_add_ui( c_im, c_im, 1);
    fmpq_mul( c_im, c_im, hw );
}

void box_get_width( fmpq_t width, const box_t b ) {
    /* set width to 2^-box_depthref(b) */
    if ( fmpz_cmp_si( box_depthref(b), 0 ) < 0 ){ 
        fmpz_abs( fmpq_numref( width ), box_depthref(b) );
        fmpz_set_si(fmpq_denref( width ), 2);
        fmpz_pow_fmpz(fmpq_denref( width ), fmpq_denref( width ), fmpq_numref( width ));
        fmpz_one( fmpq_numref( width ) );
    } else {
        fmpz_set_si(fmpq_numref( width ), 2);
        fmpz_pow_fmpz(fmpq_numref( width ), fmpq_numref( width ), box_depthref(b));
        fmpz_one( fmpq_denref( width ) );
    }
    fmpq_mul_2exp(width, width, 1);
}

void box_get_infRe_arf(arf_t r, const box_t x){
    fmpz_t temp;
    fmpz_init(temp);
    fmpz_mul_ui(temp, box_crealref(x), 2);
    /* set to 2*box_crealref(x) * 2^(depth) */
    arf_set_fmpz_2exp(r, temp, box_depthref(x));
    fmpz_clear(temp);
}

void box_get_supRe_arf(arf_t r, const box_t x){
    fmpz_t temp;
    fmpz_init(temp);
    fmpz_mul_ui(temp, box_crealref(x), 2);
    fmpz_add_ui(temp, temp, 2);
    /* set to (2*box_crealref(x)+2) * 2^(depth) */
    arf_set_fmpz_2exp(r, temp, box_depthref(x));
    fmpz_clear(temp);
}

void box_get_infIm_arf(arf_t r, const box_t x){
    fmpz_t temp;
    fmpz_init(temp);
    fmpz_mul_ui(temp, box_cimagref(x), 2);
    /* set to 2*box_cimagref(x) * 2^(depth) */
    arf_set_fmpz_2exp(r, temp, box_depthref(x));
    fmpz_clear(temp);
}

void box_get_supIm_arf(arf_t r, const box_t x){
    fmpz_t temp;
    fmpz_init(temp);
    fmpz_mul_ui(temp, box_cimagref(x), 2);
    fmpz_add_ui(temp, temp, 2);
    /* set to (2*box_cimagref(x)+2) * 2^(depth) */
    arf_set_fmpz_2exp(r, temp, box_depthref(x));
    fmpz_clear(temp);
}

int box_is_outside_unit_inflated( box_t b, double inflation ){
    
    arf_t bound;
    arf_init(bound);
    
    int res = 0;
    
    if (res==0) {
        box_get_infRe_arf(bound, b);
        res = (arf_cmp_d(bound, inflation) >= 0);
    }
    
    if (res==0) {
        box_get_supRe_arf(bound, b);
        res = (arf_cmp_d(bound, -inflation) <= 0);
    }
    
    if (res==0) {
        box_get_infIm_arf(bound, b);
        res = (arf_cmp_d(bound, inflation) >= 0);
    }
    
    if (res==0) {
        box_get_supIm_arf(bound, b);
        res = (arf_cmp_d(bound, -inflation) <= 0);
    }
    
    arf_clear(bound);
    
    return res;
}

int box_is_outside_unit_inflated_strict( box_t b, double inflation ){
    
    arf_t bound;
    arf_init(bound);
    
    int res = 0;
    
    if (res==0) {
        box_get_infRe_arf(bound, b);
        res = (arf_cmp_d(bound, inflation) > 0);
    }
    
    if (res==0) {
        box_get_supRe_arf(bound, b);
        res = (arf_cmp_d(bound, -inflation) < 0);
    }
    
    if (res==0) {
        box_get_infIm_arf(bound, b);
        res = (arf_cmp_d(bound, inflation) > 0);
    }
    
    if (res==0) {
        box_get_supIm_arf(bound, b);
        res = (arf_cmp_d(bound, -inflation) < 0);
    }
    
    arf_clear(bound);
    
    return res;
}

int box_is_inside_unit_inflated( box_t b, double inflation ){
    
    arf_t bound;
    arf_init(bound);
    
    int res = 1;
    
    if (res==1) {
        box_get_supRe_arf(bound, b);
        res = (arf_cmp_d(bound, inflation) <= 0);
    }
    
    if (res==1) {
        box_get_infRe_arf(bound, b);
        res = (arf_cmp_d(bound, -inflation) >= 0);
    }
    
    if (res==1) {
        box_get_supIm_arf(bound, b);
        res = (arf_cmp_d(bound, inflation) <= 0);
    }
    
    if (res==0) {
        box_get_infIm_arf(bound, b);
        res = (arf_cmp_d(bound, -inflation) >= 0);
    }
    
    arf_clear(bound);
    
    return res;
}

int box_is_inside_unit_inflated_strict( box_t b, double inflation ){
    
    arf_t bound;
    arf_init(bound);
    
    int res = 1;
    
    if (res==1) {
        box_get_supRe_arf(bound, b);
        res = (arf_cmp_d(bound, inflation) < 0);
    }
    
    if (res==1) {
        box_get_infRe_arf(bound, b);
        res = (arf_cmp_d(bound, -inflation) > 0);
    }
    
    if (res==1) {
        box_get_supIm_arf(bound, b);
        res = (arf_cmp_d(bound, inflation) < 0);
    }
    
    if (res==0) {
        box_get_infIm_arf(bound, b);
        res = (arf_cmp_d(bound, -inflation) > 0);
    }
    
    arf_clear(bound);
    
    return res;
}

void box_conjugate( box_t dest, const box_t b ) {
    box_set(dest, b);
    fmpz_neg( box_cimagref(dest), box_cimagref(dest) );
    fmpz_add_si( box_cimagref(dest), box_cimagref(dest), -1 );
}

void box_conjugate_inplace( box_t b ) {
    fmpz_neg( box_cimagref(b), box_cimagref(b) );
    fmpz_add_si( box_cimagref(b), box_cimagref(b), -1 );
}

#ifndef PW_SILENT
void box_fprint(FILE * file, const box_t b){
    fmpz_t real, imag;
    fmpz_init(real);
    fmpz_init(imag);
    fmpz_mul_si(real, box_crealref(b), 2);
    fmpz_add_si(real, real, 1);
    fmpz_mul_si(imag, box_cimagref(b), 2);
    fmpz_add_si(imag, imag, 1);
    fprintf(file, "center: ( "); fmpz_fprint(file, real); fprintf(file, " + I"); fmpz_fprint(file, imag);
    fprintf(file, " )x 2^("); fmpz_fprint(file, box_depthref(b)); fprintf(file, "), ");
    fprintf(file, "radius: 2^("); fmpz_fprint(file, box_depthref(b)); fprintf(file, ")");
    
    fmpz_clear(real);
    fmpz_clear(imag);
}
#endif

/* Precondition:  b1 and b2 have disjoint centers and same depth                               */
/* Specification: returns 1 if b1 and b2 are connected (8 adjacency),                          */
/*                        0 otherwise                                                          */
int box_are_8connected ( const box_t b1, const box_t b2 ){
    
    int res;
    fmpz_t dist;
    fmpz_init(dist);
    /* distance between real indexes */
    fmpz_sub( dist, box_crealref(b1), box_crealref(b2) );
    fmpz_abs( dist, dist );
    res = fmpz_cmp_ui( dist, 1 ) <= 0;
    if (res) {
        /* distance between imag indexes */
        fmpz_sub( dist, box_cimagref(b1), box_cimagref(b2) );
        fmpz_abs( dist, dist );
        res = fmpz_cmp_ui( dist, 1 ) <= 0;
    }
    
    fmpz_clear(dist);
    
    return res;
}

/* ordering assuming depths are the same */
int box_isless( const box_t b1, const box_t b2 ){
    int r = fmpz_cmp( box_crealref(b1), box_crealref(b2) );
    return ( (r < 0)||( (r==0)&&( fmpz_cmp( box_cimagref(b1), box_cimagref(b2) ) < 0 ) ) );
}

int box_equals( const box_t b1, const box_t b2 ) {
    return (fmpz_cmp( box_depthref(b1), box_depthref(b2) ) == 0)
         &&(fmpz_cmp( box_crealref(b1), box_crealref(b2) ) == 0)
         &&(fmpz_cmp( box_cimagref(b1), box_cimagref(b2) ) == 0);
}

/* quadrisection */
/* insert sub-boxes to the input list */
/* assume input list is sorted */
/* assume boxes are quadrisected in increasing order */
void box_quadrisect( box_list_t l, box_t b ) {
    box_ptr bSW = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bNW = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bSE = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bNE = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_init(bSW);
    box_init(bNW);
    box_init(bSE);
    box_init(bNE);
    
    fmpz_add_si( box_depthref(bSW), box_depthref(b), -1 );
    fmpz_mul_ui( box_crealref(bSW), box_crealref(b), 2 );
    fmpz_mul_ui( box_cimagref(bSW), box_cimagref(b), 2 );
    
    fmpz_set   ( box_depthref(bNW), box_depthref(bSW));
    fmpz_set   ( box_crealref(bNW), box_crealref(bSW));
    fmpz_add_ui( box_cimagref(bNW), box_cimagref(bSW), 1 );
    
    fmpz_set   ( box_depthref(bSE), box_depthref(bSW));
    fmpz_add_ui( box_crealref(bSE), box_crealref(bSW), 1 );
    fmpz_set   ( box_cimagref(bSE), box_cimagref(bSW));
    
    fmpz_set   ( box_depthref(bNE), box_depthref(bSW));
    fmpz_add_ui( box_crealref(bNE), box_crealref(bSW), 1 );
    fmpz_add_ui( box_cimagref(bNE), box_cimagref(bSW), 1 );
    
    box_pprecref( bSW ) = box_pprecref( b );
    box_pprecref( bNW ) = box_pprecref( b );
    box_pprecref( bSE ) = box_pprecref( b );
    box_pprecref( bNE ) = box_pprecref( b );
    box_nbMroref( bSW ) = box_nbMroref( b );
    box_nbMroref( bNW ) = box_nbMroref( b );
    box_nbMroref( bSE ) = box_nbMroref( b );
    box_nbMroref( bNE ) = box_nbMroref( b );
    
    acb_set( box_pevalref(bSW), box_pevalref(b) );
    acb_set( box_pevalref(bNW), box_pevalref(b) );
    acb_set( box_pevalref(bSE), box_pevalref(b) );
    acb_set( box_pevalref(bNE), box_pevalref(b) );
    
    /* test: maintain list of contained roots */
    if ( box_nbRinref(b) > 0 ) {
        acb_t acbSW, acbNW, acbSE, acbNE;
        acb_init(acbSW);
        acb_init(acbNW);
        acb_init(acbSE);
        acb_init(acbNE);
        
        box_roIntref(bSW) = _acb_vec_init( box_nbRinref(b) );
        box_lenRIref(bSW) = box_nbRinref(b);
        box_nbRinref(bSW) = 0;
        box_get_acb(acbSW, bSW);
        mag_mul_2exp_si( arb_radref( acb_realref( acbSW ) ), arb_radref( acb_realref( acbSW ) ), 1 );
        mag_mul_2exp_si( arb_radref( acb_imagref( acbSW ) ), arb_radref( acb_imagref( acbSW ) ), 1 );
        
        box_roIntref(bNW) = _acb_vec_init( box_nbRinref(b) );
        box_lenRIref(bNW) = box_nbRinref(b);
        box_nbRinref(bNW) = 0;
        box_get_acb(acbNW, bNW);
        mag_mul_2exp_si( arb_radref( acb_realref( acbNW ) ), arb_radref( acb_realref( acbNW ) ), 1 );
        mag_mul_2exp_si( arb_radref( acb_imagref( acbNW ) ), arb_radref( acb_imagref( acbNW ) ), 1 );
        
        box_roIntref(bSE) = _acb_vec_init( box_nbRinref(b) );
        box_lenRIref(bSE) = box_nbRinref(b);
        box_nbRinref(bSE) = 0;
        box_get_acb(acbSE, bSE);
        mag_mul_2exp_si( arb_radref( acb_realref( acbSE ) ), arb_radref( acb_realref( acbSE ) ), 1 );
        mag_mul_2exp_si( arb_radref( acb_imagref( acbSE ) ), arb_radref( acb_imagref( acbSE ) ), 1 );
        
        box_roIntref(bNE) = _acb_vec_init( box_nbRinref(b) );
        box_lenRIref(bNE) = box_nbRinref(b);
        box_nbRinref(bNE) = 0;
        box_get_acb(acbNE, bNE);
        mag_mul_2exp_si( arb_radref( acb_realref( acbNE ) ), arb_radref( acb_realref( acbNE ) ), 1 );
        mag_mul_2exp_si( arb_radref( acb_imagref( acbNE ) ), arb_radref( acb_imagref( acbNE ) ), 1 );
        
        for (slong i=0; i< box_nbRinref(b); i++) {
            if ( acb_contains( acbSW, box_roIntref(b)+i ) ) {
                acb_set( box_roIntref(bSW) + box_nbRinref(bSW), box_roIntref(b)+i );
                box_nbRinref(bSW)++;
            }
            if ( acb_contains( acbNW, box_roIntref(b)+i ) ) {
                acb_set( box_roIntref(bNW) + box_nbRinref(bNW), box_roIntref(b)+i );
                box_nbRinref(bNW)++;
            }
            if ( acb_contains( acbSE, box_roIntref(b)+i ) ) {
                acb_set( box_roIntref(bSE) + box_nbRinref(bSE), box_roIntref(b)+i );
                box_nbRinref(bSE)++;
            }
            if ( acb_contains( acbNE, box_roIntref(b)+i ) ) {
                acb_set( box_roIntref(bNE) + box_nbRinref(bNE), box_roIntref(b)+i );
                box_nbRinref(bNE)++;
            }
        }
        
        if ( box_nbRinref(bNE)==0 ) {
            _acb_vec_clear( box_roIntref(bNE), box_lenRIref(bNE) );
            box_lenRIref(bNE) = 0;
        }
        
        if ( box_nbRinref(bSE)==0 ) {
            _acb_vec_clear( box_roIntref(bSE), box_lenRIref(bSE) );
            box_lenRIref(bSE) = 0;
        }
        
        if ( box_nbRinref(bNW)==0 ) {
            _acb_vec_clear( box_roIntref(bNW), box_lenRIref(bNW) );
            box_lenRIref(bNW) = 0;
        }
        
        if ( box_nbRinref(bSW)==0 ) {
            _acb_vec_clear( box_roIntref(bSW), box_lenRIref(bSW) );
            box_lenRIref(bSW) = 0;
        }
        
        acb_clear(acbNE);
        acb_clear(acbSE);
        acb_clear(acbNW);
        acb_clear(acbSW);
    }
    
    box_list_insert_sorted(l, bSW);
    box_list_insert_sorted(l, bNW);
    box_list_push(l, bSE);
    box_list_push(l, bNE);
}

void box_list_quadrisect( box_list_t subboxes, box_list_t boxes ) {
    box_ptr cur_box;
    
    while ( !box_list_is_empty(boxes) ) {
        
        cur_box = box_list_pop(boxes);
        box_quadrisect( subboxes, cur_box );
        box_clear(cur_box);
        pwpoly_free(cur_box);
    
    }
}

void box_quadrisect_not_sorted( box_list_t l, box_t b ){
    box_ptr bSW = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bNW = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bSE = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bNE = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_init(bSW);
    box_init(bNW);
    box_init(bSE);
    box_init(bNE);
    
    fmpz_add_si( box_depthref(bSW), box_depthref(b), -1 );
    fmpz_mul_ui( box_crealref(bSW), box_crealref(b), 2 );
    fmpz_mul_ui( box_cimagref(bSW), box_cimagref(b), 2 );
    
    fmpz_set   ( box_depthref(bNW), box_depthref(bSW));
    fmpz_set   ( box_crealref(bNW), box_crealref(bSW));
    fmpz_add_ui( box_cimagref(bNW), box_cimagref(bSW), 1 );
    
    fmpz_set   ( box_depthref(bSE), box_depthref(bSW));
    fmpz_add_ui( box_crealref(bSE), box_crealref(bSW), 1 );
    fmpz_set   ( box_cimagref(bSE), box_cimagref(bSW));
    
    fmpz_set   ( box_depthref(bNE), box_depthref(bSW));
    fmpz_add_ui( box_crealref(bNE), box_crealref(bSW), 1 );
    fmpz_add_ui( box_cimagref(bNE), box_cimagref(bSW), 1 );
        
    
    box_pprecref( bSW ) = box_pprecref( b );
    box_pprecref( bNW ) = box_pprecref( b );
    box_pprecref( bSE ) = box_pprecref( b );
    box_pprecref( bNE ) = box_pprecref( b );
    box_nbMroref( bSW ) = box_nbMroref( b );
    box_nbMroref( bNW ) = box_nbMroref( b );
    box_nbMroref( bSE ) = box_nbMroref( b );
    box_nbMroref( bNE ) = box_nbMroref( b );
    acb_set( box_pevalref(bSW), box_pevalref(b) );
    acb_set( box_pevalref(bNW), box_pevalref(b) );
    acb_set( box_pevalref(bSE), box_pevalref(b) );
    acb_set( box_pevalref(bNE), box_pevalref(b) );
    
    box_list_push(l, bSW);
    box_list_push(l, bNW);
    box_list_push(l, bSE);
    box_list_push(l, bNE);
}

void box_get_box_list_acb( box_list_t l, const acb_t b ){
    acb_t bt;
    arf_t radarf;
    fmpz_t depth, d, cre, cim;
    acb_init(bt);
    arf_init(radarf);
    fmpz_init(depth);
    fmpz_init(d);
    fmpz_init(cre);
    fmpz_init(cim);
    acb_set(bt, b);
    /* set bt to a square box */
    mag_max(arb_radref( acb_realref(bt) ), arb_radref( acb_realref(b) ), arb_radref(acb_imagref(b) ) );
    mag_set(arb_radref( acb_imagref(bt) ), arb_radref( acb_realref(bt) ) );
    /* sets d st 2^(d-1) <= radarf < 2^d */
    arf_set_mag(radarf, arb_radref( acb_realref(bt) ));
    arf_abs_bound_lt_2exp_fmpz(depth, radarf);
//     printf("depth: "); fmpz_print(depth); printf("\n");
    fmpz_add_si(d, depth, 1); /* 2^(d-1) <= 2*radarf < 2^d */
    fmpz_neg(d, d);
    /* let bt = 2^(-(d+1))*bt */
    arb_mul_2exp_fmpz( acb_realref(bt), acb_realref(bt), d);
    arb_mul_2exp_fmpz( acb_imagref(bt), acb_imagref(bt), d);
    /* bt has width 2r st 1/2<= 2r < 1 */
    /* check if real and imaginary parts contain unique integers */
    int c1 = arb_get_unique_fmpz(cre, acb_realref(bt));
    int c2 = arb_get_unique_fmpz(cim, acb_imagref(bt));
    
    box_ptr bSW = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bNW = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bSE = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bNE = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_init(bSW);
    box_init(bNW);
    box_init(bSE);
    box_init(bNE);
    fmpz_set( box_depthref(bSW), depth );
    fmpz_set( box_depthref(bNW), depth );
    fmpz_set( box_depthref(bSE), depth );
    fmpz_set( box_depthref(bNE), depth );
        
    if (c1 && c2) {
        
        fmpz_set( box_crealref(bNE), cre );
        fmpz_set( box_cimagref(bNE), cim );
        
        fmpz_add_si( box_crealref(bNW), cre, -1 );
        fmpz_set( box_cimagref(bNW), cim );
        
        fmpz_set( box_crealref(bSE), cre );
        fmpz_add_si( box_cimagref(bSE), cim, -1 );
        
        fmpz_add_si( box_crealref(bSW), cre, -1 );
        fmpz_add_si( box_cimagref(bSW), cim, -1 );
        
        box_list_push(l, bSW);
        box_list_push(l, bNW);
        box_list_push(l, bSE);
        box_list_push(l, bNE);
    } else if (c1) {
        /* get the floor of imag part of bt */
        arf_floor( arb_midref(acb_imagref(bt)), arb_midref(acb_imagref(bt)) );
        /* acb_imagref(bt) contains a unique integer? */
//         int c2t = arb_get_unique_fmpz(cim, acb_imagref(bt));
        arb_get_unique_fmpz(cim, acb_imagref(bt));
        
        fmpz_set( box_crealref(bNE), cre );
        fmpz_set( box_cimagref(bNE), cim );
        
        fmpz_add_si( box_crealref(bNW), cre, -1 );
        fmpz_set( box_cimagref(bNW), cim );
        
        box_list_push(l, bNW);
        box_list_push(l, bNE);
        
        box_clear(bSW);
        box_clear(bSE);
        pwpoly_free(bSW);
        pwpoly_free(bSE);
        
    } else if (c2) {
        /* get the floor of real part of bt */
        arf_floor( arb_midref(acb_realref(bt)), arb_midref(acb_realref(bt)) );
        /* acb_realref(bt) contains a unique integer? */
//         int c1t = arb_get_unique_fmpz(cre, acb_realref(bt));
        arb_get_unique_fmpz(cre, acb_realref(bt));
        
        fmpz_set( box_crealref(bNE), cre );
        fmpz_set( box_cimagref(bNE), cim );
        
        fmpz_set( box_crealref(bSE), cre );
        fmpz_add_si( box_cimagref(bSE), cim, -1 );
        
        box_list_push(l, bSE);
        box_list_push(l, bNE);
        
        box_clear(bNW);
        box_clear(bSW);
        pwpoly_free(bNW);
        pwpoly_free(bSW);
    } else {
        arf_floor( arb_midref(acb_realref(bt)), arb_midref(acb_realref(bt)) );
        arf_floor( arb_midref(acb_imagref(bt)), arb_midref(acb_imagref(bt)) );
        /* acb_realref(bt) contains a unique integer? */
        /* acb_imagref(bt) contains a unique integer? */
//         int c1t = arb_get_unique_fmpz(cre, acb_realref(bt));
//         int c2t = arb_get_unique_fmpz(cim, acb_imagref(bt));
        arb_get_unique_fmpz(cre, acb_realref(bt));
        arb_get_unique_fmpz(cim, acb_imagref(bt));
        
        fmpz_set( box_crealref(bNE), cre );
        fmpz_set( box_cimagref(bNE), cim );
        
        box_list_push(l, bNE);
        
        box_clear(bNW);
        box_clear(bSW);
        box_clear(bSE);
        pwpoly_free(bNW);
        pwpoly_free(bSW);
        pwpoly_free(bSE);
    }
    
    /* boxes in l have radius r s.t. rad(b)<r<=2*rad(b) */
    /*  => boxes in subboxes have radius r s.t. rad(b)/2 < r <= rad(b) < 2r */
    /* at most 9 boxes of radius satisfying r <= rad(b) < 2r intersect b */
    box_list_t subboxes;
    box_list_init(subboxes);
    box_list_quadrisect(subboxes, l);
    while ( ! box_list_is_empty(subboxes) ) {
        box_ptr bcur = box_list_pop(subboxes);
        box_get_acb(bt, bcur);
        if (acb_overlaps(bt,b))
            box_list_push(l, bcur);
        else {
            box_clear(bcur);
            pwpoly_free(bcur);
        }
    }
        
    box_list_clear(subboxes);
    
    acb_clear(bt);
    arf_clear(radarf);
    fmpz_clear(depth);
    fmpz_clear(d);
    fmpz_clear(cre);
    fmpz_clear(cim);
}

slong box_get_box_list_domainfmpq( box_list_t lb, const fmpq_t lre, const fmpq_t ure, const fmpq_t lim, const fmpq_t uim){
    
    /* transform domain in acb */
    arb_t l,u;
    acb_t infd;
    arb_init(l);
    arb_init(u);
    acb_init(infd);
    /* set initial precision according to the smallest width of the domain */
    slong prec = PWPOLY_DEFAULT_PREC;
    fmpq_t widthR, widthI;
    fmpq_init(widthR);
    fmpq_init(widthI);
    fmpq_sub(widthR, ure, lre );
    fmpq_sub(widthI, uim, lim );
    if ( fmpq_cmp(widthR, widthI) >0 )
        fmpq_set( widthR, widthI );
    slong logWidth = fmpz_flog_ui( fmpq_numref(widthR), 2 );
    logWidth = logWidth - fmpz_clog_ui( fmpq_denref(widthR), 2 );
    if (logWidth < 0)
        prec = PWPOLY_MAX( prec, -2*logWidth );
    
    arb_set_fmpq( l, lre, prec );
    arb_set_fmpq( u, ure, prec );
    arb_union( acb_realref(infd), l, u,    prec );
    arb_set_fmpq( l, lim, prec );
    arb_set_fmpq( u, uim, prec );
    arb_union( acb_imagref(infd), l, u,    prec );
    
    box_get_box_list_acb( lb, infd );
    
    arb_clear(l);
    arb_clear(u);
    acb_clear(infd);
    fmpq_clear(widthR);
    fmpq_clear(widthI);
    
    return prec;
}

void box_get_box_list_acb2( box_list_t l, const acb_t b ){
    acb_t bt;
    arf_t radarf;
    fmpz_t depth, d, cre, cim;
    acb_init(bt);
    arf_init(radarf);
    fmpz_init(depth);
    fmpz_init(d);
    fmpz_init(cre);
    fmpz_init(cim);
    acb_set(bt, b);
    /* set bt to a square box */
    mag_max(arb_radref( acb_realref(bt) ), arb_radref( acb_realref(b) ), arb_radref(acb_imagref(b) ) );
    mag_set(arb_radref(acb_imagref(bt) ), arb_radref( acb_realref(bt) ) );
    /* sets d st 2^(d+1) < radarf <= 2^d */
    arf_set_mag(radarf, arb_radref( acb_realref(bt) ));
    arf_abs_bound_lt_2exp_fmpz(depth, radarf);
//     printf("depth: "); fmpz_print(depth); printf("\n");
    fmpz_add_si(d, depth, 1);
    fmpz_neg(d, d);
    /* let bt = 2^(-(d+1))*bt */
    arb_mul_2exp_fmpz( acb_realref(bt), acb_realref(bt), d);
    arb_mul_2exp_fmpz( acb_imagref(bt), acb_imagref(bt), d);
    /* bt has width 2r st 1/2<= 2r < 1 */
    /* check if real and imaginary parts contain unique integers */
    int c1 = arb_get_unique_fmpz(cre, acb_realref(bt));
    int c2 = arb_get_unique_fmpz(cim, acb_imagref(bt));
    
    box_ptr bSW = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bNW = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bSE = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_ptr bNE = (box_ptr) pwpoly_malloc ( sizeof(box) );
    box_init(bSW);
    box_init(bNW);
    box_init(bSE);
    box_init(bNE);
    fmpz_set( box_depthref(bSW), depth );
    fmpz_set( box_depthref(bNW), depth );
    fmpz_set( box_depthref(bSE), depth );
    fmpz_set( box_depthref(bNE), depth );
        
    if (c1 && c2) {
        
        fmpz_set( box_crealref(bNE), cre );
        fmpz_set( box_cimagref(bNE), cim );
        
        fmpz_add_si( box_crealref(bNW), cre, -1 );
        fmpz_set( box_cimagref(bNW), cim );
        
        fmpz_set( box_crealref(bSE), cre );
        fmpz_add_si( box_cimagref(bSE), cim, -1 );
        
        fmpz_add_si( box_crealref(bSW), cre, -1 );
        fmpz_add_si( box_cimagref(bSW), cim, -1 );
        
        box_list_push(l, bSW);
        box_list_push(l, bNW);
        box_list_push(l, bSE);
        box_list_push(l, bNE);
    } else if (c1) {
        /* get the floor of imag part of bt */
        arf_floor( arb_midref(acb_imagref(bt)), arb_midref(acb_imagref(bt)) );
        /* acb_imagref(bt) contains a unique integer? */
//         int c2t = arb_get_unique_fmpz(cim, acb_imagref(bt));
        arb_get_unique_fmpz(cim, acb_imagref(bt));
        
        fmpz_set( box_crealref(bNE), cre );
        fmpz_set( box_cimagref(bNE), cim );
        
        fmpz_add_si( box_crealref(bNW), cre, -1 );
        fmpz_set( box_cimagref(bNW), cim );
        
        box_list_push(l, bNW);
        box_list_push(l, bNE);
        
        box_clear(bSW);
        box_clear(bSE);
        pwpoly_free(bSW);
        pwpoly_free(bSE);
        
    } else if (c2) {
        /* get the floor of real part of bt */
        arf_floor( arb_midref(acb_realref(bt)), arb_midref(acb_realref(bt)) );
        /* acb_realref(bt) contains a unique integer? */
//         int c1t = arb_get_unique_fmpz(cre, acb_realref(bt));
        arb_get_unique_fmpz(cre, acb_realref(bt));
        
        fmpz_set( box_crealref(bNE), cre );
        fmpz_set( box_cimagref(bNE), cim );
        
        fmpz_set( box_crealref(bSE), cre );
        fmpz_add_si( box_cimagref(bSE), cim, -1 );
        
        box_list_push(l, bSE);
        box_list_push(l, bNE);
        
        box_clear(bNW);
        box_clear(bSW);
        pwpoly_free(bNW);
        pwpoly_free(bSW);
    } else {
        arf_floor( arb_midref(acb_realref(bt)), arb_midref(acb_realref(bt)) );
        arf_floor( arb_midref(acb_imagref(bt)), arb_midref(acb_imagref(bt)) );
        /* acb_realref(bt) contains a unique integer? */
        /* acb_imagref(bt) contains a unique integer? */
//         int c1t = arb_get_unique_fmpz(cre, acb_realref(bt));
//         int c2t = arb_get_unique_fmpz(cim, acb_imagref(bt));
        arb_get_unique_fmpz(cre, acb_realref(bt));
        arb_get_unique_fmpz(cim, acb_imagref(bt));
        
        fmpz_set( box_crealref(bNE), cre );
        fmpz_set( box_cimagref(bNE), cim );
        
        box_list_push(l, bNE);
        
        box_clear(bNW);
        box_clear(bSW);
        box_clear(bSE);
        pwpoly_free(bNW);
        pwpoly_free(bSW);
        pwpoly_free(bSE);
    }
    
    /* boxes in l have radius r s.t. rad(b)<r<=2*rad(b) */
    /*  => boxes in subboxes have radius r s.t. rad(b)/2 < r <= rad(b) < 2r */
    /* at most 9 boxes of radius satisfying r <= rad(b) < 2r intersect b */
//     box_list_t subboxes;
//     box_list_init(subboxes);
//     box_list_quadrisect(subboxes, l);
//     while ( ! box_list_is_empty(subboxes) ) {
//         box_ptr bcur = box_list_pop(subboxes);
//         box_get_acb(bt, bcur);
//         if (acb_overlaps(bt,b))
//             box_list_push(l, bcur);
//         else {
//             box_clear(bcur);
//             pwpoly_free(bcur);
//         }
//     }
//         
//     box_list_clear(subboxes);
    
    acb_clear(bt);
    arf_clear(radarf);
    fmpz_clear(depth);
    fmpz_clear(d);
    fmpz_clear(cre);
    fmpz_clear(cim);
}
