/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef BOX_COCO_H
#define BOX_COCO_H

#include "pw_base.h"
#include "list/pw_list.h"
#include "geometry/box.h"

#ifdef __cplusplus
extern "C" {
#endif
    
typedef struct {
    
    box_list _boxes;
    fmpz     _depth; /* depth of the boxes in _boxes */
    fmpz     _infRe; /* the index of the leftmost boxes */
    fmpz     _supRe; /* the index of the rightmost boxes */
    fmpz     _infIm; /* the index of the lowest boxes */
    fmpz     _supIm; /* the index of the highest boxes */
    fmpz     _QBRSp; /* Quadratic Box Refinement (QBR) speed */
    
    slong    _Mprec; /* the highest precision used for tstar test, PWPOLY_PREC by default */
    slong    _nbRoo; /* the number of roots in the CC, as given by Tstar */
                     /* -1 if not decided yet */
    int      _2_Sep; /* 1 => 2 times the containing box intersect no other box */
                     /*   => the containing disc has no root near its boundary */
                     /* 0 otherwise */
    int      _6_Sep; /* 1 => 6 times the containing box intersect no other box */
                     /*   => 4 times the containing disc contain no other root that the ones in the cc */
                     /* 0 otherwise */
    int      _QBRSu; /* a flag set to 1 iff the last QBR iteration was successful*/
                     /*               2 iff the last QBR iteration was unsuccessful*/
                     /*               0 iff QBR has not been tested on the cc*/
                     
    /* for refining roots comming for earoots */
    int        _w_enc; /* 1 if there is an enclosure containing the roots of interest in the CC */
                     /* 0 otherwise                                                           */
    int        _1_Sep; /* 1 => the containing box intersect no other box    */
                     /* 0 => the containing box may intersect other boxes */
    slong      _nbRIE; /* if _enclo is set, the nb of roots in the enclosure */
    acb_struct _enclo; /* if _w_enc==1, the acb_box containing the roots of interest in the CC */
                     /* We also wish to have connected components for exact boxes */
                     /* This is done by letting empty list of component box       */
                     /* and exact enclosure */
    
} box_CoCo;
    
typedef box_CoCo box_CoCo_t[1];
typedef box_CoCo * box_CoCo_ptr;

#define box_CoCo_boxesref(X) (&(X)->_boxes)
#define box_CoCo_depthref(X) (&(X)->_depth)
#define box_CoCo_infReref(X) (&(X)->_infRe)
#define box_CoCo_supReref(X) (&(X)->_supRe)
#define box_CoCo_infImref(X) (&(X)->_infIm)
#define box_CoCo_supImref(X) (&(X)->_supIm)
#define box_CoCo_QBRSpref(X) (&(X)->_QBRSp)
#define box_CoCo_Mprecref(X) ( (X)->_Mprec)
#define box_CoCo_nbRooref(X) ( (X)->_nbRoo)
#define box_CoCo_2_Sepref(X) ( (X)->_2_Sep)
#define box_CoCo_6_Sepref(X) ( (X)->_6_Sep)
#define box_CoCo_QBRSuref(X) ( (X)->_QBRSu)

#define box_CoCo_w_encref(X) ( (X)->_w_enc)
#define box_CoCo_1_Sepref(X) ( (X)->_1_Sep)
#define box_CoCo_nbRIEref(X) ( (X)->_nbRIE)
#define box_CoCo_encloref(X) (&(X)->_enclo)

/* memory managment */
void box_CoCo_init(box_CoCo_t x);

void box_CoCo_init_box(box_CoCo_t x, box_t b);

void box_CoCo_clear(box_CoCo_t x);
void box_CoCo_clear_light(box_CoCo_t x);

void box_CoCo_init_set_from_acb_slong_int( box_CoCo_t cc, 
                                           const acb_ptr enclosure, slong nbRootsInEnclosure, 
                                           int is_1_Sep, int is_2_Sep, int is_6_Sep, slong prec );

void box_CoCo_init_set_from_enclosure ( box_CoCo_t cc, 
                                        const acb_ptr enclosure, slong nbRootsInEnclosure, 
                                        int is_1_Sep, int is_2_Sep, int is_6_Sep, slong prec );

void box_CoCo_init_set_from_domainfmpq_int( box_CoCo_t cc, 
                                            const fmpq_t lre, const fmpq_t ure,  const fmpq_t lim,  const fmpq_t uim,   
                                            slong nbRoots, 
                                            int is_2_Sep, int is_6_Sep, slong prec );

void box_CoCo_initiali_QBRSp(box_CoCo_t x);
void box_CoCo_increase_QBRSp(box_CoCo_t x);
void box_CoCo_decrease_QBRSp(box_CoCo_t x);
int  box_CoCo_isminima_QBRSp(box_CoCo_t x);
PWPOLY_INLINE void box_CoCo_reuse_QBRSp(box_CoCo_t x, const box_CoCo_t y){
    fmpz_set( box_CoCo_QBRSpref(x), box_CoCo_QBRSpref(y) );
}

PWPOLY_INLINE slong box_CoCo_get_nbBoxes( box_CoCo_t x ) {
    return box_list_get_size( box_CoCo_boxesref( x ) );
}

void box_CoCo_get_infRe_arf(arf_t r, const box_CoCo_t x);
void box_CoCo_get_supRe_arf(arf_t r, const box_CoCo_t x);
void box_CoCo_get_infIm_arf(arf_t r, const box_CoCo_t x);
void box_CoCo_get_supIm_arf(arf_t r, const box_CoCo_t x);
void box_CoCo_get_contBox(acb_t r, const box_CoCo_t x);
void box_CoCo_get_center_halfwidth_fmpq( fmpq_t c_re, fmpq_t c_im, fmpq_t hw, const box_CoCo_t x );

/* maxdepth is either >=0 of -1, it which case it means +inf */
PWPOLY_INLINE int box_CoCo_is_deeper_than_slong( const box_CoCo_t x, const slong maxdepth ) {
    return (maxdepth>=0) && ( fmpz_cmp_si( box_CoCo_depthref(x), -maxdepth ) <= 0 );
}

/* sets r exactly to the max of real width, imag width */
void box_CoCo_diameter(arb_t r, const box_CoCo_t x);

/* returns 1 <=> x has no more than 3 boxes horizontally and vertically */
int box_CoCo_is_compact(const box_CoCo_t x);

int  box_CoCo_is_strictly_in_2_unit(const box_CoCo_t x);
int  box_CoCo_is_strictly_in_inflated_unit ( box_CoCo_t b, double inflation );

PWPOLY_INLINE int  box_CoCo_is_imaginary_negative(const box_CoCo_t x){
        return ( fmpz_cmp_si ( box_CoCo_supImref(x), -1 ) <= 0 );
}

PWPOLY_INLINE int  box_CoCo_is_imaginary_negative_strict(const box_CoCo_t x){
        return ( fmpz_cmp_si ( box_CoCo_supImref(x), -1 ) <  0 );
}

PWPOLY_INLINE int  box_CoCo_is_imaginary_positive(const box_CoCo_t x){
        return ( fmpz_cmp_si ( box_CoCo_infImref(x),  0 ) >= 0 );
}

PWPOLY_INLINE int  box_CoCo_is_imaginary_positive_strict(const box_CoCo_t x){
        return ( fmpz_cmp_si ( box_CoCo_infImref(x),  0 ) >  0 );
}

/* assume dest is an empty, initialized, CoCo */
void box_CoCo_set_conjugate_closure( box_CoCo_t dest, const box_CoCo_t src );
/* assume dest is an empty, initialized, CoCo */
/* assume src is imaginary positive */
void box_CoCo_set_conjugate( box_CoCo_t dest, const box_CoCo_t src );

/* printing */
#ifndef PW_SILENT
void box_CoCo_fprint(FILE * file, box_CoCo_t x);

PWPOLY_INLINE void box_CoCo_print (box_CoCo_t b) {
    box_CoCo_fprint(stdout, b);
}
#endif

/* maintains order in cc and hull */
/* if a box is already in there, assume depths are the same */
void box_CoCo_insert_box(box_CoCo_t x, box_t b);

/*Precondition:  nbSubd>=0 */
/*Specification: allocate and initializes a cc, and its constituent boxes, so that: */
/*               the re-union of the constituent boxes is (1 + 2^(-(nbSubd-1))*B(0,1) */
/*               contains (2^nbSubd + 2)^2 boxes */
box_CoCo_ptr box_CoCo_initial_CoCo(int nbSubd);

box_CoCo_ptr box_CoCo_unit_box();
/* get four child of the box centered in zero with halfwidth 2^scale */
box_CoCo_ptr box_CoCo_unit_box_scaled(slong scale, const acb_ptr InRoots, slong nbInRoots);

/*Precondition:  boxes of cc and b have the same width  */
/*               b > boxes of cc => crealref(b) > supReref(cc) */
/*                                  or (crealref(b) = supReref(cc)) and (cimagref(b) > supImref(cc)) */
/*Specification: returns true if (cc \cap b)\neq \emptyset */
/*                       false otherwise                   */
int box_CoCo_are_8connected( box_CoCo_t cc, box_t b );

/*Specification: returns true iff boxes of cc are all strictly outside unit disc */
int box_CoCo_is_outside_unit_disc (const box_CoCo_t cc );

/*Specification: returns true iff boxes of cc are all strictly inside unit disc */
int  box_CoCo_is_inside_unit_disc(const box_CoCo_t x);

/* Preconditions: cc1, cc2 are not empty, have the same depth and are connected */
/* Specifs      : set cc1 to the union of cc1 and cc2 */
/*              : let cc2 empty but do not clear it   */
void box_CoCo_merge_2_box_CoCo( box_CoCo_t cc1, box_CoCo_t cc2 );

/* ordering: cc1 < cc2 <=> depth(cc1) > depth (cc2) */
/*                     <=> radii of boxes of cc1 are greater than radii of boxes of cc2 */
PWPOLY_INLINE int box_CoCo_isless( const box_CoCo_t cc1, const box_CoCo_t cc2 ) {
    return (fmpz_cmp( box_CoCo_depthref(cc1), box_CoCo_depthref(cc2) ) > 0 );
}

/* list of CC of boxes, sorted by decreasing radii of boxes */
PWPOLY_INLINE void box_CoCo_clear_for_list(void * cc){
    box_CoCo_clear( (box_CoCo_ptr) cc );
}

PWPOLY_INLINE int box_CoCo_isless_for_list(const void * cc1, const void * cc2){
    return box_CoCo_isless( (box_CoCo_ptr) cc1, (box_CoCo_ptr) cc2 );
}

#ifndef PW_SILENT
PWPOLY_INLINE void box_CoCo_fprint_for_list(FILE * file, const void * cc){
    box_CoCo_fprint(file, (box_CoCo_ptr) cc);
}
#endif

typedef struct list   box_CoCo_list;
typedef struct list   box_CoCo_list_t[1];
typedef struct list * box_CoCo_list_ptr;

// PWPOLY_INLINE void         box_CoCo_list_init ( box_CoCo_list_t l )                      { list_init(l, box_CoCo_clear_for_list); }
PWPOLY_INLINE void         box_CoCo_list_init ( box_CoCo_list_t l )                      { list_init(l); }
PWPOLY_INLINE void         box_CoCo_list_swap ( box_CoCo_list_t l1, box_CoCo_list_t l2 ) { list_swap(l1, l2); }
// PWPOLY_INLINE void         box_CoCo_list_clear( box_CoCo_list_t l )                      { list_clear(l); }
PWPOLY_INLINE void         box_CoCo_list_clear( box_CoCo_list_t l )                      { list_clear(l, box_CoCo_clear_for_list); }
PWPOLY_INLINE void         box_CoCo_list_empty( box_CoCo_list_t l )                      { list_empty(l); }
PWPOLY_INLINE void         box_CoCo_list_push ( box_CoCo_list_t l, box_CoCo_ptr b )      { list_push(l,b); }
PWPOLY_INLINE void         box_CoCo_list_push_front ( box_CoCo_list_t l, box_CoCo_ptr b ){ list_push_front(l,b); }
PWPOLY_INLINE box_CoCo_ptr box_CoCo_list_pop  ( box_CoCo_list_t l )                      { return (box_CoCo_ptr) list_pop(l); }
PWPOLY_INLINE box_CoCo_ptr box_CoCo_list_first( box_CoCo_list_t l )                      { return (box_CoCo_ptr) list_first(l); }
PWPOLY_INLINE box_CoCo_ptr box_CoCo_list_last ( box_CoCo_list_t l )                      { return (box_CoCo_ptr) list_last(l); }
PWPOLY_INLINE void         box_CoCo_list_insert_sorted ( box_CoCo_list_t l, box_CoCo_ptr b ) { list_insert_sorted(l, b, box_CoCo_isless_for_list); }
#ifndef PW_SILENT
PWPOLY_INLINE void         box_CoCo_list_fprint( FILE * file, box_CoCo_list_t l )        { list_fprint(file, l, box_CoCo_fprint_for_list); }
PWPOLY_INLINE void         box_CoCo_list_print (              box_CoCo_list_t l )        { box_CoCo_list_fprint(stdout, l); }
#endif
PWPOLY_INLINE int          box_CoCo_list_is_empty  ( box_CoCo_list_t l )                 { return list_is_empty(l); }
PWPOLY_INLINE slong        box_CoCo_list_get_size  ( const box_CoCo_list_t l )           { return list_get_size(l); }

/*iterator */
typedef list_iterator box_CoCo_list_iterator;
PWPOLY_INLINE box_CoCo_list_iterator box_CoCo_list_begin(const box_CoCo_list_t l)   { return list_begin(l); }
PWPOLY_INLINE box_CoCo_list_iterator box_CoCo_list_endEl(const box_CoCo_list_t l)   { return list_endEl(l); }
PWPOLY_INLINE box_CoCo_list_iterator box_CoCo_list_next(box_CoCo_list_iterator it)  { return list_next(it); }
PWPOLY_INLINE box_CoCo_list_iterator box_CoCo_list_prev(box_CoCo_list_iterator it)  { return list_prev(it); }
PWPOLY_INLINE box_CoCo_list_iterator box_CoCo_list_end()                       { return list_end(); }
PWPOLY_INLINE box_CoCo_ptr           box_CoCo_list_elmt(box_CoCo_list_iterator it)  { return (box_CoCo_ptr) list_elmt(it); }

/*Precondition:  cc in ccs are disjoint connected components of the same width */
/*               boxes of ccs and b are distinct and have the same depth  */
/*               b > boxes of ccs                        */
/*Specification: modifies inplace the list of ccs so that b is in boxes of ccs */
/*               and cc in ccs are disjoint connected components */
void box_CoCo_union_box( box_CoCo_list_t ccs, box_t b);
#ifdef __cplusplus
}
#endif

#endif
