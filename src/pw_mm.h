/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PWPOLY_MM_H
#define PWPOLY_MM_H

/*********************/
/* compiling options */
/*********************/
// #define PW_SILENT               /* when defined at pre-compiling,                                */
                                   /* all sections of code containing prints to stdout are removed  */
// #define PWMM_STATS              /* profile memory allocator;                                     */
                                   /* BE CAREFUL: if this is defined, it slows down dramatically execution ! */

#ifdef PW_SILENT
#undef PWMM_STATS
#endif

#ifdef PWPOLY_NO_INLINES           /* cancels inlining */
#define MM_INLINE static
#else
#define MM_INLINE static inline
#endif

/*********************/
/* MACROS            */
/*********************/

#define PWMM_MIN(A,B) (A<=B? A : B)

#define PWMM_BLOCK_LT(A, B) (A->_ptr <  B->_ptr)
#define PWMM_BLOCK_LE(A, B) (A->_ptr <= B->_ptr)

#define PWMM_PTR_LT(A, B) (A <  B)
#define PWMM_PTR_LE(A, B) (A <= B)
#define PWMM_PTR_EQ(A, B) (A == B)

#define PWMM_MALLOC(A) malloc(A)
#define PWMM_REALLOC(A, B) realloc(A,B)
#define PWMM_CALLOC(A, B) calloc(A,B)
#define PWMM_FREE(A) free(A)

// #if (defined(WIN32) || defined(WIN64))
#define PWMM_MEMMOVE(a,b,c) memmove((void*)a,(void *)b, (size_t)c)
// #else
// #define PWMM_MEMMOVE(a,b,c) bcopy((void*)b,(void *)a, (size_t) c) /* why doing this? */
// #endif

#define PWMM_ALLOC_SIZE 1024

#define PWMM_SIZ size_t
#define PWMM_PTR char*

/*********************/
/* includes          */
/*********************/
#ifndef PW_SILENT
#include <stdio.h>
#endif
#include <stdlib.h> /* for malloc, free, ... */
#include <string.h> /* for pwmmset */

#include "windows.h"

/*********************/
/* typedefs          */
/*********************/

#ifndef ulong
typedef unsigned long int ulong;
#endif

#ifndef slong
typedef long int slong;
#endif

struct pwmm_block {
    PWMM_PTR            _ptr;  /* a pointer to a memory block */
    PWMM_SIZ            _size; /* the size of the memory poited by _ptr */
    int                 _used; /* 1 if the memory has not been freed yet */
                               /* 0 if the memory block is not used (_ptr==NULL) or has been freed */ 
};

typedef struct pwmm_block pwmm_block_t[1];
typedef struct pwmm_block *pwmm_block_ptr;

struct pwmm_table {
    ulong              _size_allocated; /*the allocated length*/
    ulong              _size;           /*the actual length; <= _size_allocated*/
    ulong              _used;           /*the number of used memory block; <= _size */
    ulong              _last_freed;     /* heuristic: last_pointer freed should be the one of the next malloc */
    pwmm_block_ptr     _table;          /* the table of memory blocks */
    void *             _prev;           /* another pwmm_table */
};

typedef struct pwmm_table pwmm_table_t[1];
typedef struct pwmm_table *pwmm_table_ptr;

/*********************/
/* access macros     */
/*********************/
#define PWMM_TAB_SIAL_REF(A) ( A->_size_allocated )
#define PWMM_TAB_SIZE_REF(A) ( A->_size )
#define PWMM_TAB_USED_REF(A) ( A->_used )
#define PWMM_TAB_LASF_REF(A) ( A->_last_freed )
#define PWMM_TAB_TABL_REF(A) ( A->_table )
#define PWMM_TAB_PREV_REF(A) ( A->_prev )

#define PWMM_TAB_PTR(A,I)  ( (A->_table + I)->_ptr )
#define PWMM_TAB_SIZ(A,I)  ( (A->_table + I)->_size )
#define PWMM_TAB_USE(A,I)  ( (A->_table + I)->_used )

/**************************************************/
/* those tables can be used as default tables     */
/**************************************************/

extern pwmm_table_t default_pwmm_table;
extern pwmm_table_ptr current_pwmm_table;

/**************************************************/
/* main functions of the memory manager           */
/**************************************************/

/* initialization of a table */
void pwmm_table_init( pwmm_table_t a );

/* alloc functions */
PWMM_PTR pwmm_malloc( PWMM_SIZ size, pwmm_table_t mm );

PWMM_PTR pwmm_realloc( PWMM_PTR old_ptr, PWMM_SIZ new_size, pwmm_table_t mm );

PWMM_PTR pwmm_calloc( PWMM_SIZ num, PWMM_SIZ size, pwmm_table_t mm );

void     pwmm_free( PWMM_PTR ptr, pwmm_table_t mm );

/* free all the memory tracked AND dealloc the table */
void     pwmm_cleanup( pwmm_table_t mm );

/* clearing tables: useless when pwmm_cleanup is used */
void pwmm_table_clear( pwmm_table_t a );
void pwmm_table_clear_and_free( pwmm_table_t a );

/**************************************************/
/* printing: only for devel                       */
/**************************************************/

void pwmm_table_print( pwmm_table_t a );
void pwmm_table_print_short( pwmm_table_t a );

/* NOT USED */
void pwmm_table_defrag(pwmm_table_t a);
void pwmm_table_merge( pwmm_table_t a, pwmm_table_t b );

#ifdef PWMM_STATS
#include <time.h>
extern double time_in_search;
extern double time_in_insert;
extern double time_in_insert_at;
extern double time_in_sea_in_in;
extern double time_in_malloc;
extern double time_in_realloc;
extern double time_in_calloc;
extern double time_in_free;
extern ulong  nb_call_to_search;
extern ulong  nb_hit_with_heur;
extern ulong  nb_hit_with_last;
extern ulong  nb_call_malloc;
extern ulong  nb_call_realloc;
extern ulong  nb_call_calloc;
extern ulong  nb_call_free;

#define PWMM_INIT_PROFILE {\
    time_in_search = 0.;\
    time_in_insert = 0.;\
    time_in_insert_at = 0.;\
    time_in_sea_in_in = 0.;\
    time_in_malloc = 0.;\
    time_in_realloc = 0.;\
    time_in_calloc = 0.;\
    time_in_free = 0.;\
    nb_call_to_search = 0;\
    nb_hit_with_heur = 0;\
    nb_hit_with_last = 0;\
    nb_call_malloc = 0;\
    nb_call_realloc = 0;\
    nb_call_calloc = 0;\
    nb_call_free = 0;\
}

#define PWMM_PRINT_PROFILE {\
    printf( "time_in_search   :    %f\n", time_in_search/CLOCKS_PER_SEC );\
    printf( "time_in_insert   :    %f\n", time_in_insert/CLOCKS_PER_SEC );\
    printf( "time_in_insert_at:    %f\n", time_in_insert_at/CLOCKS_PER_SEC );\
    printf( "time_in_sea_in_in:    %f\n", time_in_sea_in_in/CLOCKS_PER_SEC );\
    printf( "av time_in_malloc:    %.16f, %lu, %f\n", (time_in_malloc/CLOCKS_PER_SEC)/nb_call_malloc, nb_call_malloc, time_in_malloc/CLOCKS_PER_SEC );\
    printf( "av time_in_realloc:   %.16f, %lu, %f\n", (time_in_realloc/CLOCKS_PER_SEC)/nb_call_realloc, nb_call_realloc, time_in_realloc/CLOCKS_PER_SEC );\
    printf( "av time_in_calloc:    %.16f, %lu, %f\n", (time_in_calloc/CLOCKS_PER_SEC)/nb_call_calloc, nb_call_calloc, time_in_calloc/CLOCKS_PER_SEC );\
    printf( "av time_in_free  :    %.16f, %lu, %f\n", (time_in_free/CLOCKS_PER_SEC)/nb_call_free, nb_call_free, time_in_free/CLOCKS_PER_SEC );\
    printf( "nb_hit_with_last/nb_hit_with_heur/nb_call_to_search: %lu/%lu/%lu\n", nb_hit_with_last, nb_hit_with_heur, nb_call_to_search );\
}
#endif

#endif
