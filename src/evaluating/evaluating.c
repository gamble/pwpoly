/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_evaluating.h"

double clicks_in_locate_eval;
double clicks_in_sort_eval;
double clicks_in_eval_eval;

/* app is initialized, but not necessarily completely computed */
void _pw_approximation_evaluate_acb_sectors( acb_t value, acb_t point, pw_approximation_t app, pw_sector_list_t sectors PW_VERBOSE_ARGU(verbose)  ){
    
#ifndef PW_SILENT 
    int level = 2;
#endif    
    slong lprec = pw_approximation_precref(app);
    ulong N    = pw_approximation_Nref(app);
    
    arb_t error, abspoint;
    arb_init(error);
    arb_init(abspoint);
    acb_abs(abspoint, point, lprec);
    arb_get_ubound_arf( arb_midref(abspoint), abspoint, lprec );
    mag_zero( arb_radref(abspoint) );
    _pw_approximation_evaluation_error( error, app, abspoint, lprec PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT
    if (verbose>=level) {
        printf("_pw_approximation_evaluate_acb_sectors: error: "); arb_printd(error, 10); printf("\n");
    }
#endif

    acb_t powpoint, shipoint, valuetemp;
    acb_init(powpoint);
    acb_init(shipoint);
    acb_init(valuetemp);
    ulong nprev = 0;
    pw_sector_list_iterator it = pw_sector_list_begin( sectors );
    ulong i=0;
    
    while( it!=pw_sector_list_end() ) {
        pw_sector_ptr sector = pw_sector_list_elmt(it);
        ulong n = pw_sector_nref(sector);
        ulong k = pw_sector_kref(sector);
        ulong K = pw_approximation_nbsectref(app)[n];
        
        /* if approxs are not computed in the n-th annulus, compute them */
        if (pw_approximation_approx_lengthsref(app)[n]<=0)
            pw_approximation_compute_approx_annulus( app, n, 0 );
        
        if ( (i==0) || (n!=nprev) ) {
            if (n<N-1)
                acb_pow_ui( powpoint, point, pw_approximation_indlowref(app)[n], lprec );
            else
                acb_pow_ui( powpoint, point, pw_approximation_indhigref(app)[n-1], lprec );
        }
        nprev = n;
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pw_approximation_evaluate_acb_sectors: \n");
            printf(" index        : (%lu/%lu, %ld/%ld) \n", n, N-1,  k, K-1 );
            printf(" point        : "); acb_printd( point, 10); printf("\n");
            printf(" indlow       : %lu\n", pw_approximation_indlowref(app)[n]);
            printf(" powered point: "); acb_printd( powpoint, 10); printf("\n");
        }
#endif   
        acb_srcptr coeffs;
        ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, k );
        if (K==1) {
            if (n<N-1)
                _acb_poly_evaluate(valuetemp, coeffs, lenapprox, point, lprec);
            else {
                acb_inv(shipoint, point, lprec);
                _acb_poly_evaluate(valuetemp, coeffs, lenapprox, shipoint, lprec);
            }
        } else {
            /* shift point: z = (gamma + scale*rho*x)e^(i2Pik/K) */
            /*          <=> x = (1/(scale*rho))*(z*e^(-i2Pik/K)-gamma) */
            _pw_covering_shift_point( shipoint, pw_approximation_coveringref(app), n, k, point, lprec );
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("_pw_approximation_evaluate_acb_sectors: shifted point: "); acb_printd( shipoint, 10); printf("\n");
            }
#endif
            /* evaluate point */
            _acb_poly_evaluate(valuetemp, coeffs, lenapprox, shipoint, lprec);
        }
        acb_mul(valuetemp, valuetemp, powpoint, lprec);
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pw_approximation_evaluate_acb_sectors: evaluation before error: "); acb_printd(valuetemp, 10); printf("\n");
        }
#endif 
        acb_add_error_arb( valuetemp, error );
#ifndef PW_SILENT        
        if (verbose>=level) {
            printf("_pw_approximation_evaluate_acb_sectors: evaluation after error: "); acb_printd(valuetemp, 10); printf("\n\n");
        }
#endif        
        if (i==0) {
            acb_set(value, valuetemp);
        } else {
            acb_union(value, value, valuetemp, lprec);
        }
        
        i++;
        it = pw_sector_list_next(it);
    }
    
    acb_clear(valuetemp);
    acb_clear(shipoint);
    acb_clear(powpoint);
    arb_clear(abspoint);
    arb_clear(error);
    
}

/* app is initialized, but not necessarily completely computed */
void _pw_approximation_evaluate2_acb_sectors( acb_t value, acb_t valueder, acb_t point, pw_approximation_t app, pw_sector_list_t sectors PW_VERBOSE_ARGU(verbose)  ){
    
#ifndef PW_SILENT 
    int level = 2;
#endif    
    
    slong lprec = pw_approximation_precref(app);
    
    /* for each sector, use evaluate2 in sector */
    acb_t vt, vtder;
    acb_init(vt);
    acb_init(vtder);
    
    pw_sector_list_iterator it = pw_sector_list_begin( sectors );
    ulong i=0;
    while( it!=pw_sector_list_end() ) {
        pw_sector_ptr sector = pw_sector_list_elmt(it);
        ulong n = pw_sector_nref(sector);
        ulong k = pw_sector_kref(sector); 
        
#ifndef PW_SILENT
        if (verbose>=level) {
            ulong N    = pw_approximation_Nref(app);
            printf(" _pw_approximation_evaluate2_acb_sectors: \n");
            printf(" ***index        : (%lu/%lu, %ld/%ld) \n", n, N-1,  k, pw_approximation_nbsectref(app)[n]-1 );
            printf(" ***point        : "); acb_printd(point, 10); printf("\n");
        }
#endif

        /* if approxs are not computed in the n-th annulus, compute them */
        if (pw_approximation_approx_lengthsref(app)[n]<=0)
            pw_approximation_compute_approx_annulus( app, n, 0 );
        _pw_approximation_evaluate2_acb_nk( vt, vtder, app, n, k, point PW_VERBOSE_CALL(verbose)  );
        if (i==0) {
            acb_set(value, vt);
            acb_set(valueder, vtder);
        } else {
            acb_union(value, value, vt, lprec);
            acb_union(valueder, valueder, vtder, lprec);
        }
#ifndef PW_SILENT
        if (verbose>=level) {
            ulong N    = pw_approximation_Nref(app);
            printf(" _pw_approximation_evaluate2_acb_sectors: \n");
            printf(" ***index        : (%lu/%lu, %ld/%ld) \n", n, N-1,  k, pw_approximation_nbsectref(app)[n]-1 );
            printf(" ***point        : "); acb_printd(point, 10); printf("\n");
            printf(" ***evaluation   : "); acb_printd(vt, 10); printf("\n");
            printf(" ***derivative   : "); acb_printd(vtder, 10); printf("\n");
        }
#endif
        i++;
        it = pw_sector_list_next(it);
    }
    
    acb_clear(vtder);
    acb_clear(vt);
    
}

struct acb_for_sort{
    acb_ptr _elmt;
    arb_ptr _elab;
    slong   _indx;
};
typedef struct acb_for_sort * acb_for_sort_ptr;

acb_for_sort_ptr acb_for_sort_init( acb_ptr xs, arb_ptr ys, slong len ){
    acb_for_sort_ptr res = (acb_for_sort_ptr) pwpoly_malloc ( len*sizeof(struct acb_for_sort) );
    for (slong i=0; i<len; i++) {
        (res+i)->_elmt = xs + i;
        (res+i)->_elab = ys + i;
        (res+i)->_indx = i;
    }
    return res;
}

void acb_for_sort_clear( acb_for_sort_ptr x ) {
    pwpoly_free(x);
}

int _compare_abs ( acb_for_sort_ptr u, acb_for_sort_ptr v ) {
    return arf_cmp(arb_midref( u->_elab ), arb_midref( v->_elab ) );
}


void _pw_approximation_evaluate_multipoints( acb_ptr vals, acb_ptr valsDer, const acb_ptr points, slong len,
                                             pw_approximation_t app PW_VERBOSE_ARGU(verbose)  ) {
    
    /* compute the approximation     */
    /* NO, it is done ONLY if NEEDED */
//     pw_approximation_compute_approx_annulii( app, 0 );

#ifdef PW_PROFILE
    clock_t start = clock();
#endif
    
    slong lprec = pw_approximation_precref(app);
    pw_sector_list_t sectors;
    pw_sector_list_init(sectors);

#ifdef PW_PROFILE
    clock_t start_sort = clock();
#endif    
    /* sort input points by increasing lower bound of abs of points */
    arb_ptr abspoints = _arb_vec_init(len);
    for (slong i=0; i<len; i++) {
        acb_abs( abspoints + i, points + i, lprec );
        arb_get_lbound_arf( arb_midref( abspoints + i ), abspoints + i, lprec );
    }
    acb_for_sort_ptr spoints = acb_for_sort_init( points, abspoints, len );
    
    qsort (spoints, len, sizeof(struct acb_for_sort), (__compar_fn_t) _compare_abs);
    
#ifdef PW_PROFILE
    clicks_in_sort_eval += (clock() - start_sort);
#endif
    /*evaluate*/
    ulong n=0, nprev=0;
    for (slong i = 0; i<len; i++) {
        
        slong indexInOrig = (spoints+i)->_indx;
//         printf("indexInOrig: %ld\n", indexInOrig);
        
#ifdef PW_PROFILE
        clock_t start2 = clock();
#endif        
        /* locate point in sectors */
        n = pw_covering_locate_point( sectors, nprev, pw_approximation_coveringref(app), points+indexInOrig, lprec );
#ifdef PW_PROFILE
        clock_t start3 = clock();
        clicks_in_locate_eval += (start3 - start2);
#endif        
        /* evaluate in sectors */
        if (valsDer==NULL)
            _pw_approximation_evaluate_acb_sectors( vals+indexInOrig, points+indexInOrig, app, sectors PW_VERBOSE_CALL(verbose)  );
        else {
            _pw_approximation_evaluate2_acb_sectors( vals+indexInOrig, valsDer+indexInOrig, points+indexInOrig, app, sectors PW_VERBOSE_CALL(verbose)  );
        }

#ifdef PW_PROFILE
        clicks_in_eval_eval += (clock() - start3);
#endif        
        if (nprev<n) {
            /* since n is non-decreasing, nprev-th annulus is now useless */
            if (pw_approximation_approx_lengthsref(app)[nprev] > 0)
                pw_approximation_clear_approx_annulus( app, nprev );
        }
        
        pw_sector_list_clear(sectors);
        pw_sector_list_init(sectors);
        nprev = n;
    }
    
#ifdef PW_PROFILE
    clicks_in_evaluate += (clock() - start);
    clicks_in_evaluate -= clicks_in_approx_annulii;
    clicks_in_eval_eval -= clicks_in_approx_annulii;
#endif
    
    acb_for_sort_clear( spoints );
    _arb_vec_clear(abspoints, len);
    pw_sector_list_clear(sectors);
    
}

void _pwpoly_evaluate_pw_polynomial( acb_ptr vals, const acb_ptr points, slong len,
                                     pw_polynomial_t poly, ulong m,
                                     const ulong c, const fmpq_t scale, const fmpq_t ratio PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile)) {
    
    pw_approximation_t app;
    /* initialize the approximation */
    pw_approximation_init_pw_polynomial( app, poly, m, c, scale, ratio );
    
    _pw_approximation_evaluate_multipoints( vals, NULL, points, len, app PW_VERBOSE_CALL(verbose)  );

#ifndef PW_NO_INTERFACE     
    if (coveringFile!=NULL) {
        _pw_covering_with_points_sectors_gnuplot( coveringFile, pw_approximation_coveringref(app), points, len );
    }
#endif    
    pw_approximation_clear(app);
}

void _pwpoly_evaluate2_pw_polynomial( acb_ptr vals, acb_ptr valsDer, const acb_ptr points, slong len,
                                      pw_polynomial_t poly, ulong m,
                                      const ulong c, const fmpq_t scale, const fmpq_t ratio PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile)) {
    
    pw_approximation_t app;
    /* initialize the approximation */
    pw_approximation_init_pw_polynomial( app, poly, m, c, scale, ratio );

// #ifndef PW_SILENT
//     printf("_pwpoly_evaluate2_pw_polynomial: \n");
//         for (slong i=0; i<len; i++) {
//             acb_printd( points + i, 10 ); printf("\n");
//         }
// #endif

    _pw_approximation_evaluate_multipoints( vals, valsDer, points, len, app PW_VERBOSE_CALL(verbose)  );

#ifndef PW_NO_INTERFACE     
    if (coveringFile!=NULL) {
        _pw_covering_with_points_sectors_gnuplot( coveringFile, pw_approximation_coveringref(app), points, len );
    }
#endif    
    pw_approximation_clear(app);
}

void pwpoly_evaluate_acb_poly( acb_ptr vals, const acb_ptr points, slong len, 
                               const acb_poly_t pacb, ulong m,
                               const ulong c, const fmpq_t scale, const fmpq_t ratio PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile) ){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, pacb );
    
    _pwpoly_evaluate_pw_polynomial( vals, points, len, poly, m, c, scale, ratio PW_VERBOSE_CALL(verbose) , coveringFile );
    
    pw_polynomial_clear(poly);
}

void pwpoly_evaluate_2fmpq_poly( acb_ptr vals, const acb_ptr points, slong len, 
                                 const fmpq_poly_t p_re, const fmpq_poly_t p_im, ulong m,
                                 const ulong c, const fmpq_t scale, const fmpq_t ratio PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile) ){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    
    _pwpoly_evaluate_pw_polynomial( vals, points, len, poly, m, c, scale, ratio PW_VERBOSE_CALL(verbose) PW_FILE_CALL(coveringFile) );
    
    pw_polynomial_clear(poly);
}

void pwpoly_evaluate2_acb_poly( acb_ptr vals, acb_ptr valsDer, const acb_ptr points, slong len, 
                               const acb_poly_t pacb, ulong m,
                               const ulong c, const fmpq_t scale, const fmpq_t ratio PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile) ){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, pacb );
    
    _pwpoly_evaluate2_pw_polynomial( vals, valsDer, points, len, poly, m, c, scale, ratio PW_VERBOSE_CALL(verbose) PW_FILE_CALL(coveringFile) );
    
    pw_polynomial_clear(poly);
}

void pwpoly_evaluate2_2fmpq_poly( acb_ptr vals, acb_ptr valsDer, const acb_ptr points, slong len, 
                                 const fmpq_poly_t p_re, const fmpq_poly_t p_im, ulong m,
                                 const ulong c, const fmpq_t scale, const fmpq_t ratio PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile) ){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    
    _pwpoly_evaluate2_pw_polynomial( vals, valsDer, points, len, poly, m, c, scale, ratio PW_VERBOSE_CALL(verbose) PW_FILE_CALL(coveringFile) );
    
    pw_polynomial_clear(poly);
}

void _pwpoly_init_set_approximation_pw_polynomial( pw_approximation_t app, 
                                                   pw_polynomial_t poly, ulong m,
                                                   const ulong c, const fmpq_t scale, const fmpq_t ratio ){
    
    /* initialize the approximation */
    pw_approximation_init_pw_polynomial( app, poly, m, c, scale, ratio );
    
    /* compute the approximation     */
    pw_approximation_compute_approx_annulii( app, 0 );   

}

void pwpoly_init_set_approximation_acb_poly_python( pw_approximation_ptr app, 
                                                    const acb_poly_t pacb, slong m) {
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, pacb );
    
    ulong c = 2;
    fmpq_t scale, ratio;
    fmpq_init(scale);
    fmpq_init(ratio);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(ratio, 2, 5);
    
    _pwpoly_init_set_approximation_pw_polynomial( app, poly, (ulong)m, c, scale, ratio );
    
    fmpq_clear(ratio);
    fmpq_clear(scale);
    
    pw_polynomial_clear(poly);
}

void pwpoly_init_set_approximation_2fmpq_poly_python( pw_approximation_ptr app, 
                                                      const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong m){
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    
    ulong c = 2;
    fmpq_t scale, ratio;
    fmpq_init(scale);
    fmpq_init(ratio);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(ratio, 2, 5);
    
    _pwpoly_init_set_approximation_pw_polynomial( app, poly, (ulong)m, c, scale, ratio );
    
    fmpq_clear(ratio);
    fmpq_clear(scale);
    
    pw_polynomial_clear(poly);
}    

void pwpoly_approximation_init_python( pw_approximation_ptr * app ){
    *app = (pw_approximation_ptr) pwpoly_malloc ( sizeof(pw_approximation) );
    pw_approximation_init_dummy(*app);
//     return app;
}

void pwpoly_approximation_clear_python( pw_approximation_ptr app ){
    pw_approximation_clear(app);
    pwpoly_free(app);
}

#ifndef PW_SILENT
void pwpoly_approximation_print_short_python( pw_approximation_ptr app ){
    pw_approximation_print_short( app );
}
#endif

void pwpoly_approximation_evaluate_python( acb_t y, pw_approximation_ptr app, const acb_t x ){
    pw_approximation_evaluate_acb( y, app, x PW_VERBOSE_CALL(0));
}

void pwpoly_approximation_evaluate2_python( acb_t y, acb_t z, pw_approximation_ptr app, const acb_t x ){
    pw_approximation_evaluate2_acb( y, z, app, x PW_VERBOSE_CALL(0) );
//     printf("y: "); acb_printd(y, 10); printf("\n");
//     printf("z: "); acb_printd(z, 10); printf("\n");
}

void pwpoly_evaluate_acb_poly_python( acb_ptr vals, const acb_ptr points, slong len, 
                                      const acb_poly_t pacb, slong m){
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, pacb );
    
    ulong c = 2;
    fmpq_t scale, ratio;
    fmpq_init(scale);
    fmpq_init(ratio);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(ratio, 2, 5);
    
    _pwpoly_evaluate_pw_polynomial( vals, points, len, poly, (ulong)m, c, scale, ratio PW_VERBOSE_CALL(0)
#ifndef PW_NO_INTERFACE    
    , NULL 
#endif
    );
    
    fmpq_clear(ratio);
    fmpq_clear(scale);
    
    pw_polynomial_clear(poly);
    
}

void pwpoly_evaluate_2fmpq_poly_python( acb_ptr vals, const acb_ptr points, slong len, 
                                        const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong m) {
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    
    ulong c = 2;
    fmpq_t scale, ratio;
    fmpq_init(scale);
    fmpq_init(ratio);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(ratio, 2, 5);
    
    _pwpoly_evaluate_pw_polynomial( vals, points, len, poly, (ulong)m, c, scale, ratio PW_VERBOSE_CALL(0)
#ifndef PW_NO_INTERFACE    
    , NULL 
#endif        
    );
    
    fmpq_clear(ratio);
    fmpq_clear(scale);
    
    pw_polynomial_clear(poly);
}

void pwpoly_evaluate2_acb_poly_python( acb_ptr vals, acb_ptr valsder, const acb_ptr points, slong len, 
                                      const acb_poly_t pacb, slong m){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, pacb );
    
    ulong c = 2;
    fmpq_t scale, ratio;
    fmpq_init(scale);
    fmpq_init(ratio);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(ratio, 2, 5);
    
    _pwpoly_evaluate2_pw_polynomial( vals, valsder, points, len, poly, (ulong)m, c, scale, ratio PW_VERBOSE_CALL(0)
#ifndef PW_NO_INTERFACE    
    , NULL 
#endif
    );
    
    fmpq_clear(ratio);
    fmpq_clear(scale);
    
    pw_polynomial_clear(poly);
}

void pwpoly_evaluate2_2fmpq_poly_python( acb_ptr vals, acb_ptr valsder, const acb_ptr points, slong len, 
                                        const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong m){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, p_re, p_im );
    
    ulong c = 2;
    fmpq_t scale, ratio;
    fmpq_init(scale);
    fmpq_init(ratio);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(ratio, 2, 5);
    
    _pwpoly_evaluate2_pw_polynomial( vals, valsder, points, len, poly, (ulong)m, c, scale, ratio PW_VERBOSE_CALL(0)
#ifndef PW_NO_INTERFACE    
    , NULL 
#endif        
    );
    
    fmpq_clear(ratio);
    fmpq_clear(scale);
    
    pw_polynomial_clear(poly);
}
