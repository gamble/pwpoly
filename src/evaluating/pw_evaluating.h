/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_EVALUATING_H
#define PW_EVALUATING_H

#include "pw_base.h"
#include "approximation/pw_approximation.h"

#ifdef __cplusplus
extern "C" {
#endif

    
/* Preconditions: poly is a polynomial of degree d                                                               */
/*                c, scale, b are positive, m is positive                                                        */
/*                points contains len acb's                                                                      */
/*                vals contains room for at least len acb's                                                      */
/* Postconditions: for 0 < i <= len-1, vals + i contains the value of poly(i)                                    */
/*                 let poly(x) = a0 + a1x + a2x^2 + ... + adx^d                                                  */
/*                 and |poly|(|x|) = |a0| + |a1||x| + |a2||x|^2 + ... + |ad||x|^d                                */
/*                 then forall 0 < i <= len-1,                                                                   */ 
/*                      (radius of vals + i)/|poly|(|points + i|)     is more or less 2^-m                       */
/* Brief: computes a piecewise polynomial approximation with parameters m, c, scale, b                           */ 
/*        for each point, evaluate the approximation                                                             */
void _pwpoly_evaluate_pw_polynomial( acb_ptr vals, const acb_ptr points, slong len, 
                                     pw_polynomial_t poly, ulong m,
                                     const ulong c, const fmpq_t scale, const fmpq_t b PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile) );
/* for acb polys, possibly with error */
void pwpoly_evaluate_acb_poly( acb_ptr vals, const acb_ptr points, slong len, 
                               const acb_poly_t pacb, ulong m,
                               const ulong c, const fmpq_t scale, const fmpq_t b PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile) );
/* for polys given as a pair (real, imaginary) of fmpq polys */
void pwpoly_evaluate_2fmpq_poly( acb_ptr vals, const acb_ptr points, slong len, 
                                 const fmpq_poly_t p_re, const fmpq_poly_t p_im, ulong m,
                                 const ulong c, const fmpq_t scale, const fmpq_t b PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile) );
/* see pre/post conditions for _pwpoly_evaluate_pw_polynomial  */
/* in addition, evaluates the derivative of poly at the points */
void _pwpoly_evaluate2_pw_polynomial( acb_ptr vals, acb_ptr valsDer, const acb_ptr points, slong len, 
                                     pw_polynomial_t poly, ulong m,
                                     const ulong c, const fmpq_t scale, const fmpq_t b PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile));
/* for acb polys, possibly with error */
void pwpoly_evaluate2_acb_poly( acb_ptr vals, acb_ptr valsDer, const acb_ptr points, slong len, 
                               const acb_poly_t pacb, ulong m,
                               const ulong c, const fmpq_t scale, const fmpq_t b PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile) );
/* for polys given as a pair (real, imaginary) of fmpq polys */
void pwpoly_evaluate2_2fmpq_poly( acb_ptr vals, acb_ptr valsDer, const acb_ptr points, slong len, 
                                 const fmpq_poly_t p_re, const fmpq_poly_t p_im, ulong m,
                                 const ulong c, const fmpq_t scale, const fmpq_t b PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(coveringFile) );

/* FOR PYTHON WRAPPER */
/* allocates memory for a pw_approximation, set app to a pointer on this memory */
void pwpoly_approximation_init_python( pw_approximation_ptr * app );

/* deallocate memory pointed by input */
void pwpoly_approximation_clear_python( pw_approximation_ptr );

/* Preconditions: poly is a polynomial of degree d                                                               */
/*                c, scale, ratio are positive, m is positive                                                    */
/*                app is a non-initialized pw_approximation_t, possibly allocated by pwpoly_approximation_init_python */
/* Postconditions: app is a piecewise polynomial approximation for poly                                          */
void _pwpoly_init_set_approximation_pw_polynomial( pw_approximation_t app, 
                                                   pw_polynomial_t poly, ulong m,
                                                   const ulong c, const fmpq_t scale, const fmpq_t ratio );
/* for acb polys, possibly with error */
void pwpoly_init_set_approximation_acb_poly_python( pw_approximation_ptr app, 
                                                    const acb_poly_t pacb, slong m );
/* for polys given as a pair (real, imaginary) of fmpq polys */
void pwpoly_init_set_approximation_2fmpq_poly_python( pw_approximation_ptr app, 
                                                      const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong m );


#ifndef PW_SILENT
void pwpoly_approximation_print_short_python( pw_approximation_ptr );
#endif

/* Preconditions: app is an initialized and computed piecewise polynomial approximation                          */
/*                x is an acb point                                                                              */
/*                y is an initialized acb                                                                        */
/* Postconditions: y contains poly(x)                                                                            */
/*                 assume app has been computed at precision m                                                   */
/*                 let poly(x) = a0 + a1x + a2x^2 + ... + adx^d                                                  */
/*                 and |poly|(|x|) = |a0| + |a1||x| + |a2||x|^2 + ... + |ad||x|^d                                */
/*                 then (radius of y)/|poly|(|y|) is more or less 2^-m                                           */
/* Brief: evaluate approximation on x                                                                            */
void pwpoly_approximation_evaluate_python( acb_t y, pw_approximation_ptr app, const acb_t x );

/* see pre/post conditions for pwpoly_approximation_evaluate_python  */
/* in addition, evaluates the derivative of poly at the points       */
void pwpoly_approximation_evaluate2_python( acb_t y, acb_t z, pw_approximation_ptr app, const acb_t x );

/* see pre/post conditions of _pwpoly_evaluate_pw_polynomial, with m = 10, c=2, scale = 3/2, b = 2/5 */
void pwpoly_evaluate_acb_poly_python( acb_ptr vals, const acb_ptr points, slong len, 
                                      const acb_poly_t pacb, slong m);
/* see pre/post conditions of _pwpoly_evaluate_pw_polynomial, with m = 10, c=2, scale = 3/2, b = 2/5 */
void pwpoly_evaluate_2fmpq_poly_python( acb_ptr vals, const acb_ptr points, slong len, 
                                        const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong m);
/* see pre/post conditions of _pwpoly_evaluate2_pw_polynomial, with m = 10, c=2, scale = 3/2, b = 2/5 */
void pwpoly_evaluate2_acb_poly_python( acb_ptr vals, acb_ptr valsder, const acb_ptr points, slong len, 
                                      const acb_poly_t pacb, slong m);
/* see pre/post conditions of _pwpoly_evaluate2_pw_polynomial, with m = 10, c=2, scale = 3/2, b = 2/5 */
void pwpoly_evaluate2_2fmpq_poly_python( acb_ptr vals, acb_ptr valsder, const acb_ptr points, slong len, 
                                        const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong m);

#ifdef __cplusplus
}
#endif

#endif
