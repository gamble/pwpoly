/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "solvers_few_roots.h"

void _be_inv( double * dest_re, double * dest_im, 
              double   srca_re, double   srca_im){
    double temp = 1/(srca_re*srca_re + srca_im*srca_im);
    *dest_re = srca_re*temp;
    *dest_im = -srca_im*temp;
}

void _be_div_si( double * dest_re, double * dest_im, 
                 double   srca_re, double   srca_im,
                 slong b ){
    *dest_re = srca_re/(double)b;
    *dest_im = srca_im/(double)b;
}

void _be_mul_si( double * dest_re, double * dest_im, 
                 double   srca_re, double   srca_im,
                 slong b ){
    *dest_re = srca_re*(double)b;
    *dest_im = srca_im*(double)b;
}

void _one_EA_from_vals( double *nroots, double *roots,               /* nroots, roots have size 3*l */
                        double *pvals, double *pdervals, slong l PW_VERBOSE_ARGU(verbose)  ) { /* pvals, pdervals have size 2*l */
    
    double t[2], invDiffs[2];
    slong i, k;
#ifndef PW_SILENT
    int level = 5;
#endif
    
    for (i=0; i<l; i++) {
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_one_EA_from_vals: %ld-th root: \n", i);
            printf(" ---         %.10e + %.10eI +/- %.10e\n", roots[3*i], roots[3*i+1], roots[3*i+2] );
            printf(" --- pval    %.10e + %.10eI\n", pvals[2*i], pvals[2*i+1] );
            printf(" --- pderval %.10e + %.10eI\n", pdervals[2*i], pdervals[2*i+1] );
        }
#endif
        if ( ( (pvals[2*i])!=0 ) || (pvals[2*i+1]!=0 ) ) {
            /* compute p'(roots+i)/p(roots+i) */
            _be_inv( pvals + 2*i, pvals + (2*i+1), pvals[2*i], pvals[2*i+1] );
            _be_mul( t+0, t+1, pdervals[2*i], pdervals[2*i+1], pvals[2*i], pvals[2*i+1] );
            pvals[2*i] = t[0];
            pvals[2*i+1] = t[1];
            /* compute invDiffs = sum of the 1/(roots+i - roots+k) */
            invDiffs[0] = 0.;
            invDiffs[1] = 0.;
            for (k=0; k<l; k++) {
                if (k!=i) {
                    _be_sub( t+0,        t+1,
                             roots[3*i], roots[3*i + 1],    
                             roots[3*k], roots[3*k + 1] ); 
                    _be_inv( t+0, t+1, t[0], t[1] );
                    _be_add( invDiffs + 0, invDiffs + 1, 
                             invDiffs[0],  invDiffs[1],
                             t[0], t[1]);
                }
            }
            /* compute 1/( p'(roots+i)/p(roots+i) - sum of the 1/(roots+i - roots+k) ) */
            _be_sub( pvals + 2*i, pvals + (2*i+1),
                     pvals[2*i], pvals[2*i+1],    
                     invDiffs[0],  invDiffs[1]    );
            _be_inv( pvals + 2*i, pvals + (2*i+1), pvals[2*i], pvals[2*i+1] );
            /* compute nroots+i = roots+i - 1/( p(roots+i)/p'(roots+i) - sum of the 1/(roots+i - roots+k) ) */
            _be_sub( nroots + 2*i, nroots + (2*i+1),
                     roots[3*i], roots[3*i + 1],    
                     pvals[2*i], pvals[2*i+1]    );
        } else { /* do not actualize root+i */
            nroots[2*i]   = roots[3*i];
            nroots[2*i+1] = roots[3*i+1];
        }
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_one_EA_from_vals: %ld-th nroot: \n", i);
            printf(" --- %.10e + %.10eI\n", nroots[2*i], nroots[2*i+1] );
        }
#endif
    }
        
}

void _inclusion_radii_from_vals( double *rads,                               /* nrads has size 2*l */
                                 double *pvals,      double *pdervals,       /* pvals, pdervals have size 2*l */
                                 double *pvals_aber, double *pdervals_aber,  /* pvals_aber, pdervals_aber has size l */
                                 slong l, slong deg PW_VERBOSE_ARGU(verbose)  ) {
#ifndef PW_SILENT    
    int level = 5;
#endif
    double abspval[2], abspderval[2];
    slong i;
    
    for (i=0; i<l; i++) {
#ifndef PW_SILENT
        if (verbose>=level) {
            printf("_inclusion_radii_from_vals: \n");
            printf(" --- pvals      : %.10e + %.10eI +/- %.10e\n", pvals[2*i], pvals[2*i+1], pvals_aber[i] );
            printf(" --- pdervals   : %.10e + %.10eI +/- %.10e\n", pdervals[2*i], pdervals[2*i+1], pdervals_aber[i] );
        }
#endif
        /* compute rads[i] = (l-1)*|p(roots+i)|/|p'(roots+i)| */
        _be_abs_aber( abspval+0, abspval+1, pvals[2*i], pvals[2*i+1], pvals_aber[i] );
        _be_abs_aber( abspderval+0, abspderval+1, pdervals[2*i], pdervals[2*i+1], pdervals_aber[i] );
#ifndef PW_SILENT
        if (verbose>=level) {
            printf(" --- abspvals   : %.10e +/- %.10e\n", abspval[0], abspval[1] );
            printf(" --- abspdervals: %.10e +/- %.10e\n", abspderval[0], abspderval[1] );
        }
#endif
        _be_real_div_aber( rads + (2*i), rads + (2*i+1), 
                           abspval[0], abspval[1],
                           abspderval[0], abspderval[1] );
#ifndef PW_SILENT
        if (verbose>=level) {
            printf(" --- abspvals/abspdervals: %.10e +/- %.10e\n", rads[2*i],  rads[2*i+1] );
        }
#endif
        _be_real_mul_si_aber( rads + (2*i), rads + (2*i+1), 
                              rads[2*i],  rads[2*i+1],
                              deg               );
#ifndef PW_SILENT
        if (verbose>=level) {
            printf(" --- inclusion rad       : %.10e +/- %.10e\n", rads[2*i],  rads[2*i+1] );
        }
#endif
    }
}

#ifndef PWPOLY_HAS_EAROOTS
void _pw_acb_poly_evaluate2_be( double * vals, double * valsaber, 
                                double * valsder, double * valsderaber, 
                                double * points, double pointsMaxAber, slong nbPoints, 
                                acb_poly_t pacb ) {
    
    acb_ptr pointsacb = _acb_vec_init(nbPoints);
    acb_ptr valsacb   = _acb_vec_init(nbPoints);
    acb_ptr valsderacb= _acb_vec_init(nbPoints);
    
    slong lprec = PWPOLY_DEFAULT_PREC;
    
    for (slong j=0; j<nbPoints; j++) {
        _be_get_acb ( pointsacb + j, points[2*j], points[2*j+1], pointsMaxAber );
        pw_acb_poly_evaluate2_horner_mid( valsacb+j, valsderacb+j, pacb->coeffs, pacb->length, pointsacb+j, lprec );
        _be_set_acb( vals   +(2*j), vals   +(2*j+1), valsaber +j, valsacb+j );
        _be_set_acb( valsder+(2*j), valsder+(2*j+1), valsderaber+j, valsderacb+j );
    }
    
    _acb_vec_clear(valsderacb, nbPoints);
    _acb_vec_clear(valsacb, nbPoints);
    _acb_vec_clear(pointsacb, nbPoints);
}
#endif

slong _EA_its_on_l_roots_be( double * roots, slong l, slong log2_eps, slong nbIts, 
                             double * poly, double * polyder, slong len PW_VERBOSE_ARGU(verbose)  ) {

#ifndef PW_SILENT    
    int level = 5;
#endif
    slong res = 0;
    
    /* save rounding mode and set double rounding mode to NEAREST */
    int rounding_save = fegetround();
    int ressetnearest = fesetround (FE_TONEAREST);
    if (ressetnearest!=0) {
        res = PWPOLY_SET_ROUND_NEAREST_FAILED;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("_EA_its_on_l_roots_be: setting rounding to nearest failed!!!\n");
#endif
        fesetround (rounding_save);
        return res;
    }
    slong it, i;
    
    double * nRoots = (double *) pwpoly_malloc ( 2*l*sizeof(double) );
    
    double * rootsForEval   = (double *) pwpoly_malloc ( 2*l*sizeof(double) );
    double   rootsForEvalRelErr = 0.;
    double * pvals          = (double *) pwpoly_malloc ( 2*l*sizeof(double) );
    double * pvalsAbsErr    = (double *) pwpoly_malloc ( l*sizeof(double) );
    double * pdervals       = (double *) pwpoly_malloc ( 2*l*sizeof(double) );
    double * pdervalsAbsErr = (double *) pwpoly_malloc ( l*sizeof(double) );
    
    double * rads     = (double *) pwpoly_malloc ( 2*l*sizeof(double) );
    double * absroots = (double *) pwpoly_malloc ( 2*l*sizeof(double) );

#ifdef PWPOLY_HAS_EAROOTS    
    double * polyForEvalCoeffs = (double *) pwpoly_malloc ( 2*len*sizeof(double) );
    double * polyForEvalAbsErr = (double *) pwpoly_malloc ( len*sizeof(double) );
    double * pderForEvalCoeffs = (double *) pwpoly_malloc ( 2*len*sizeof(double) );
    double * pderForEvalAbsErr = (double *) pwpoly_malloc ( len*sizeof(double) );
    for (i=0; i<len-1; i++) {
        polyForEvalCoeffs[2*i]     = poly[3*i];
        polyForEvalCoeffs[2*i + 1] = poly[3*i+1];
        polyForEvalAbsErr[i]       = poly[3*i+2];
        pderForEvalCoeffs[2*i]     = polyder[3*i];
        pderForEvalCoeffs[2*i + 1] = polyder[3*i+1];
        pderForEvalAbsErr[i]       = polyder[3*i+2];
    }
    polyForEvalCoeffs[2*i]     = poly[3*i];
    polyForEvalCoeffs[2*i + 1] = poly[3*i+1];
    polyForEvalAbsErr[i]       = poly[3*i+2];
#else 
    acb_poly_t pacb;
    acb_poly_init2(pacb, len);
    _acb_poly_set_length(pacb, len);
    for (i=0; i<len; i++)
        _be_get_acb ( pacb->coeffs+i, poly[3*i], poly[3*i+1], poly[3*i+2] );
#endif    
    for (it = 0; (it<nbIts)&&(res==0); it++) {
        
        /* evaluate p and p' at roots */
        for (i=0; i<l; i++) {
            rootsForEval[2*i]   = roots[3*i];
            rootsForEval[2*i+1] = roots[3*i+1];
            rootsForEvalRelErr = 0.;
        }
#ifdef PWPOLY_HAS_EAROOTS
        ea_evaluate(polyForEvalCoeffs, polyForEvalAbsErr, (int) len,
                    rootsForEval, l, rootsForEvalRelErr, // input points: points has size 2*plen,
                    pvals, pvalsAbsErr );
        ea_evaluate(pderForEvalCoeffs, pderForEvalAbsErr, (int)(len-1),
                    rootsForEval, l, rootsForEvalRelErr, // input points: points has size 2*plen,
                    pdervals, pdervalsAbsErr );
#else 
        _pw_acb_poly_evaluate2_be( pvals, pvalsAbsErr, 
                                   pdervals, pdervalsAbsErr, 
                                   rootsForEval, rootsForEvalRelErr, l, 
                                   pacb );
#endif
        
        /* apply one EA iteration to roots */
        _one_EA_from_vals( nRoots, roots, pvals, pdervals, l PW_VERBOSE_CALL(verbose) );
        for (i=0; i<l; i++) {
            roots[3*i]   = nRoots[2*i];
            roots[3*i+1] = nRoots[2*i+1];
            roots[3*i+2] = 0.;
        }
        
        if ( (it%5==0)||(it==nbIts-1) ) {
            res = 1;
            
            /* save exception flags and re-init exceptions */
            fexcept_t except_save;
            fegetexceptflag (&except_save, FE_ALL_EXCEPT);
            feclearexcept (FE_ALL_EXCEPT);
            
            /* evaluate p and p' at roots */
            for (i=0; i<l; i++) {
                rootsForEval[2*i]   = roots[3*i];
                rootsForEval[2*i+1] = roots[3*i+1];
                rootsForEvalRelErr = 0.;
            }
#ifdef PWPOLY_HAS_EAROOTS 
            ea_evaluate(polyForEvalCoeffs, polyForEvalAbsErr, (int) len,
                        rootsForEval, l, rootsForEvalRelErr, // input points: points has size 2*plen,
                        pvals, pvalsAbsErr );
            ea_evaluate(pderForEvalCoeffs, pderForEvalAbsErr, (int)(len-1),
                        rootsForEval, l, rootsForEvalRelErr, // input points: points has size 2*plen,
                        pdervals, pdervalsAbsErr );
#else 
            _pw_acb_poly_evaluate2_be( pvals, pvalsAbsErr, 
                                       pdervals, pdervalsAbsErr, 
                                       rootsForEval, rootsForEvalRelErr, l, 
                                       pacb );
#endif
            
            /* compute rads[i] = (l-1)*|p(roots+i)|/|p'(roots+i)| */
            _inclusion_radii_from_vals( rads, pvals, pdervals, pvalsAbsErr, pdervalsAbsErr, l, len-1 PW_VERBOSE_CALL(verbose) );
            /* stop if all the roots that are in the unit disc have inclusion radius < 2^(log2_eps-1) */
            for (i=0; (i<l)&&(res==1); i++) {
                
//                 _be_add_error_be_real_pos( roots + (3*i), roots + (3*i+1), roots + (3*i+2),
//                                            roots[3*i], roots[3*i+1], roots[3*i+2],
//                                            rads[2*i],  rads[2*i+1] );
                _be_abs_aber( absroots + (2*i), absroots + (2*i+1), 
                              roots[3*i], roots[3*i+1], roots[3*i+2] );
                int inUnit = _be_real_pos_lt_2exp_si( absroots[2*i], absroots[2*i+1], 0 );
                res = res && ( !inUnit || _be_real_pos_lt_2exp_si( rads[2*i], rads[2*i+1], log2_eps-1 ));
#ifndef PW_SILENT                
                if (verbose >=level) {
                    printf("_EA_its_on_l_roots_be: %ld-th iteration, %ld-th root: \n", it, i);
                    printf(" --- %.10e + %.10eI +/- %.10e\n", roots[3*i], roots[3*i+1], roots[3*i+2] );
                    printf(" --- inclusion radius        : %.10e +/- %.10e\n", rads[2*i], rads[2*i+1] );
                    printf(" --- is in unit disc         : %d\n", inUnit );
                    printf(" --- has radius < 2^log2_eps : %d\n", _be_real_pos_lt_2exp_si( rads[2*i], rads[2*i+1], log2_eps-1 ) );
                    printf(" --- 2^(log2_eps-1):         : %e\n", ldexp(0x1, log2_eps-1) );
                }
#endif
            }
            
//             if (res==0) {
//                 for (i=0; i<l; i++) {
//                     roots[3*i+2] = 0.;
//                 }
//             }
            if (res==1) {
                for (i=0; i<l; i++)
                    _be_add_error_be_real_pos( roots + (3*i), roots + (3*i+1), roots + (3*i+2),
                                               roots[3*i], roots[3*i+1], roots[3*i+2],
                                               rads[2*i],  rads[2*i+1] );
            }
            
            if (res==1) {
                int double_exception = _pwpoly_test_and_print_exception(PW_PREAMB_CALL("_EA_its_on_l_roots_be: ") PW_VERBOSE_CALL(verbose));
                if (double_exception) {
                    res = PWPOLY_DOUBLE_EXCEPTION;
                }
    
            }
            
            /* restore exception flags and rounding mode */
            fesetexceptflag (&except_save, FE_ALL_EXCEPT);
        }
        
    }

#ifndef PWPOLY_HAS_EAROOTS
    acb_poly_clear(pacb);
#else
    pwpoly_free( polyForEvalCoeffs );
    pwpoly_free( polyForEvalAbsErr );
    pwpoly_free( pderForEvalCoeffs );
    pwpoly_free( pderForEvalAbsErr );
#endif
    pwpoly_free( nRoots );
    pwpoly_free( rootsForEval );
    pwpoly_free( pvals );
    pwpoly_free( pvalsAbsErr );
    pwpoly_free( pdervals );
    pwpoly_free( pdervalsAbsErr );
    pwpoly_free( absroots );
    pwpoly_free( rads );
    
    /* restore rounding mode */
    fesetround (rounding_save);
    
    if (res)
        res = it;
    return res;
}

void _cauchy_sums_be( double * sums, slong nbSums, double * poly, double * polyder, slong len ) {
    
    /* get the power of 2 above len */
    uint ceilLog2Len = 1;
    while( ((slong)0x1 << ceilLog2Len) < len )
        ceilLog2Len++;
    
    slong nlen = (slong)0x1 << ceilLog2Len;
    
    double * p_real   = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * p_imag   = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * p_aber   = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * pp_real  = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * pp_imag  = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * pp_aber  = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * pv_real  = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * pv_imag  = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * pv_aber  = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * ppv_real = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * ppv_imag = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    double * ppv_aber = (double *) pwpoly_malloc ( nlen*sizeof(double) );
    
    be_vec_t omegas;
    be_vec_init(omegas, nlen);
    
    for (slong i=0; i<len-1; i++) {
        p_real[i] = poly[3*i];
        p_imag[i] = poly[3*i+1];
        p_aber[i] = poly[3*i+2];
        pp_real[i] = polyder[3*i];
        pp_imag[i] = polyder[3*i+1];
        pp_aber[i] = polyder[3*i+2];;
    }
    p_real[len-1] = poly[3*(len-1)];
    p_imag[len-1] = poly[3*(len-1)+1];
    p_aber[len-1] = poly[3*(len-1)+2];;
    pp_real[len-1] = 0.;
    pp_imag[len-1] = 0.;
    pp_aber[len-1] = 0.;
        
    for (slong i=len; i<nlen; i++) {
        p_real[i] = 0.;
        p_imag[i] = 0.;
        p_aber[i] = 0.;
        pp_real[i] = 0.;
        pp_imag[i] = 0.;
        pp_aber[i] = 0.;
    }
    
    befft_fft_rad2_t rad2;
    befft_fft_rad2_init( rad2, ceilLog2Len );
    
    /* evaluate poly at the nlen nlen-th roots of unit */
    befft_fft_rad2_dynamic_precomp    ( pv_real, pv_imag, pv_aber,
                                        p_real, p_imag, p_aber, 
                                        rad2 );

    /* evaluate pder at the nlen nlen-th roots of unit */
    befft_fft_rad2_dynamic_precomp    ( ppv_real, ppv_imag, ppv_aber,
                                        pp_real,  pp_imag,  pp_aber, 
                                        rad2 );

    befft_fft_rad2_clear( rad2 );
    
    double t_re, t_im;
    /* get the nlen nlen-th roots of unit */
    be_vec_omega_uint( omegas, ceilLog2Len-1 );
    slong nlenO2 = (nlen>>1);
    for (slong i = nlenO2; i<nlen; i++) {
        be_vec_realref(omegas)[i] = -be_vec_realref(omegas)[i-nlenO2];
        be_vec_imagref(omegas)[i] = -be_vec_imagref(omegas)[i-nlenO2];
    }
    /* set pvals+i to (pdervals+i)/(pvals+i), without error computation*/
    for (slong i=0; i<nlen; i++) {
        _be_inv( pv_real+i, pv_imag+i, pv_real[i], pv_imag[i] );
        _be_mul( &t_re, &t_im, ppv_real[i], ppv_imag[i], pv_real[i], pv_imag[i] );
        pv_real[i] = t_re;
        pv_imag[i] = t_im;
    }
    /* compute the nbSums Cauchy sums, without error computation*/
    for (slong h=1; h<=nbSums; h++) {
        sums[ 3*(h-1)     ] = 0.;
        sums[ 3*(h-1) + 1 ] = 0.;
        sums[ 3*(h-1) + 2 ] = 0.;
        /* set sh to (1/nlen)( sum for g from 0 to nlen-1 of (rootsOfUnit+(g*(h+1))%nlen)*(pdervals+g)/(pvals+g) ) */
        for (slong g=0; g<nlen; g++) {
            _be_mul( &t_re, &t_im, 
                     (be_vec_realref(omegas))[ (g*(h+1))%nlen ], (be_vec_imagref(omegas))[ (g*(h+1))%nlen ],
                     pv_real[g], pv_imag[g] );
            _be_add( sums + (3*(h-1)), sums + (3*(h-1)+1), 
                     sums[3*(h-1)],    sums[3*(h-1)+1],
                     t_re,             t_im               );  
        }
        _be_div_si ( sums + (3*(h-1)), sums + (3*(h-1)+1), 
                     sums[3*(h-1)],    sums[3*(h-1)+1],
                     nlen                                 ); 
    }
    
    be_vec_clear(omegas);
    pwpoly_free(ppv_aber);
    pwpoly_free(ppv_imag);
    pwpoly_free(ppv_real);
    pwpoly_free(pv_aber);
    pwpoly_free(pv_imag);
    pwpoly_free(pv_real);
    pwpoly_free(pp_aber);
    pwpoly_free(pp_imag);
    pwpoly_free(pp_real);
    pwpoly_free(p_aber);
    pwpoly_free(p_imag);
    pwpoly_free(p_real);
    
}

int _be_overlaps_acb( const double yre, const double yim, const double yab, const acb_t z ){
    acb_t y;
    acb_init(y);
    
    arf_set_d( arb_midref(acb_realref(y)), yre);
    arf_set_d( arb_midref(acb_imagref(y)), yim);
    mag_set_d( arb_radref(acb_realref(y)), yab);
    mag_set_d( arb_radref(acb_imagref(y)), yab);
    int res = acb_overlaps(y,z);
    
    acb_clear(y);
    
    return res;
}

int _be_poly_overlaps_acb_poly ( const double * poly, const acb_poly_t pacb ) {
    int res=1;
    for (slong i=0; (i<pacb->length)&&(res==1); i++) {
        res = res && _be_overlaps_acb( poly[3*i], poly[3*i+1], poly[3*i+2], pacb->coeffs + i );
    }
      
    return res;
}

int pw_solver_123_roots_be( acb_ptr roots, slong nbRoots, slong log2_eps, double * poly, slong len PW_VERBOSE_ARGU(verbose)  ) {
    
    if ( (nbRoots<1) || (nbRoots >3) )
        return 0;
    
#ifndef PW_SILENT    
    int level = 5;
#endif
    int res = 0;
    
    double * polyder = (double *) pwpoly_malloc (3*(len-1)*sizeof(double));
    res = pwpoly_be_poly_derivative( polyder, poly, len PW_VERBOSE_CALL(verbose) );
    if (res<=PWPOLY_DOUBLE_EXCEPTION) {
        pwpoly_free(polyder);
        return res;
    }
    
    double * droots=     (double *) pwpoly_malloc (3*nbRoots*sizeof(double));
    double * cauchySums= (double *) pwpoly_malloc (3*nbRoots*sizeof(double));
    double * absdroots=   (double *) pwpoly_malloc (2*nbRoots*sizeof(double));
    
    /* compute 1,2,3 first Cauchy's sums that approximate 1,2,3 first power sums */
    /* of the roots of poly in the unit disc                                     */
    _cauchy_sums_be( cauchySums, nbRoots, poly, polyder, len );
#ifndef PW_SILENT     
    if (verbose>=level) {
        printf("--------------solver_123_roots_be, nbRoots: %ld-------------------\n", nbRoots);
        for (slong i=0; i<nbRoots; i++) {
            _be_abs_aber( absdroots + 2*i, absdroots + (2*i+1), cauchySums[3*i], cauchySums[3*i+1], cauchySums[3*i+2] );
            printf("--- %ld-th Cauchy sum : %.10f + %.10fI +/- %.10E\n", i, cauchySums[3*i], cauchySums[3*i+1], cauchySums[3*i+2] );
            printf("   |%ld-th Cauchy sum|: %.10f +/- %.10E\n", i, absdroots[2*i], absdroots[2*i+1]);
        }
    }
#endif 

    /* compute approximations of the roots in the unit disc from the Cauchy's sums */
    /* with Newton's identities */
    if (nbRoots>1) {
        acb_ptr ncauchySums = _acb_vec_init( nbRoots );
        for (slong i=0; i<nbRoots; i++)
            _be_get_acb ( ncauchySums+i, cauchySums[3*i], cauchySums[3*i+1], cauchySums[3*i+2] );
        
        acb_poly_t ptemp;
        acb_poly_init(ptemp);
        slong prec = PWPOLY_DEFAULT_PREC;
        
        pw_newton_identites ( ptemp, ncauchySums, nbRoots, prec );
        if (nbRoots==2)
            pw_solver_quadratic( roots, ptemp->coeffs, prec PW_VERBOSE_CALL(verbose) );
        else
            pw_solver_cubic( roots, ptemp->coeffs, prec PW_VERBOSE_CALL(verbose) );
        acb_poly_clear(ptemp);
        
        for (slong i=0; i<nbRoots; i++)
            _be_set_acb( droots+(3*i), droots+(3*i+1), droots+(3*i+2), roots+i );
        
        _acb_vec_clear(ncauchySums, nbRoots);
        
    } else {
        for (slong i=0; i<3*nbRoots; i++)
            droots[i]=cauchySums[i];
    }
    
#ifndef PW_SILENT     
    if (verbose>=level) {
        printf("--------------solver_123_roots_be, nbRoots: %ld-------------------\n", nbRoots);
        for (slong i=0; i<nbRoots; i++) {
            _be_abs_aber( absdroots + 2*i, absdroots + (2*i+1), droots[3*i], droots[3*i+1], droots[3*i+2] );
            printf("--- %ld-th candidate  : %.10f + %.10fI +/- %.10E\n", i, droots[3*i], droots[3*i+1], droots[3*i+2] );
            printf("   |%ld-th candidate| : %.10f +/- %.10E\n", i, absdroots[2*i], absdroots[2*i+1]);
        }
    }
#endif

    /* apply EA iterations to the roots */
    slong resEA = _EA_its_on_l_roots_be( droots, nbRoots, log2_eps, 50, poly, polyder, len PW_VERBOSE_CALL(verbose) );
    
    /* check if the approximations are pairwise disjoint and strictly in the unit disc */
    if (resEA <= 0)
        res = resEA;
    else {
        res = 1;
        for (slong i=0; (i<nbRoots)&&(res); i++) {
            _be_abs_aber( absdroots + 2*i, absdroots + (2*i+1), 
                          droots[3*i], droots[3*i+1], droots[3*i+2] );
            res = res&&_be_real_pos_lt_2exp_si( absdroots[2*i], absdroots[2*i+1], 0 );
            _be_get_acb ( roots+i, droots[3*i], droots[3*i+1], droots[3*i+2] );
            for (slong k=0; k<i; k++)
                res=res&&(acb_overlaps(roots + k, roots + i)==0);
        }
    }
    
#ifndef PW_SILENT     
    if (verbose>=level) {
        printf("--------------solver_123_roots, nbRoots: %ld-------------------\n", nbRoots);
        printf(" resEA: %ld, res: %d\n", resEA, res);
        arb_t absroot;
        arb_init(absroot);
        for (slong i=0; i<nbRoots; i++) {
            acb_abs( absroot+i, roots+i, PWPOLY_DEFAULT_PREC );
            printf("--- %ld-th root       : ", i); acb_printd(roots+i, 10); printf("\n");
            printf("   |%ld-th root|      : ", i); arb_printd(absroot, 10); printf("\n");
        }
        arb_clear(absroot);
        printf("\n\n");
    }
#endif
    
    for (slong i=0; (i<nbRoots)&&(res); i++)
        acb_get_mid( roots+i, roots+i );
    
    pwpoly_free(absdroots);
    pwpoly_free(cauchySums);
    pwpoly_free(droots);
    pwpoly_free(polyder);
    
    return res;
}
