/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "tstar.h"

#ifdef PW_PROFILE
double clicks_in_arbpellet;
double clicks_in_arbDLG;
#endif

#ifdef PW_COCO_PROFILE
double clicks_in_tstar_exclusion_shift;
double clicks_in_tstar_exclusion_DLG;
double clicks_in_shift;
double clicks_in_anticipate;
ulong nb_tstar_exclusion_DLG;
ulong nb_shift;
ulong nb_shift_prec53;
ulong nb_anticipate;
#endif

void _tstar_taylorShift_inplace( const acb_t c, const arb_t r, acb_poly_t poly, int round, slong prec){
    
    arb_t f;
    arb_init(f);
    _acb_poly_taylor_shift_convolution( poly->coeffs, c, poly->length, prec );
    arb_set(f, r);
    acb_t zero;
    acb_init(zero);
    acb_zero(zero);
    mag_set_ui_2exp_si( arb_radref( acb_realref( zero ) ), 2, -prec );
    mag_set_ui_2exp_si( arb_radref( acb_imagref( zero ) ), 2, -prec );
//     arb_t zero;
//     arb_init(zero);
//     arb_zero(zero);
//     mag_set_ui_2exp_si( arb_radref( zero ), 2, -prec );
    for (slong i = 1; i < poly->length; i++){
        acb_mul_arb( poly->coeffs + i, poly->coeffs + i, f, prec);
        if ((round)&&(acb_contains( zero, poly->coeffs + i ))) {
            acb_set( poly->coeffs + i, zero );
        }
//         if (arb_contains( zero, acb_realref(poly->coeffs + i) )) {
//             arb_set( acb_realref(poly->coeffs + i), zero );
//         }
//         if (arb_contains( zero, acb_imagref(poly->coeffs + i) )) {
//             arb_set( acb_imagref(poly->coeffs + i), zero );
//         }
        arb_mul(f, f, r, prec);
    } 
//     arb_clear(zero);
    acb_clear(zero);
    arb_clear(f);
    
}

/* computes the coeff of x^(index) of one Graeffe iteration of f, where f has length and degree len-1 */
/* requires: the 2*index first coeffs of f */
void _tstar_oneGraeffeIteration_coeff( acb_ptr coeff, acb_srcptr f, slong index, slong len, slong prec) {
    
    slong deltaindex, i, deg;
    acb_t temp;
    
    acb_init(temp);
    
    acb_mul(coeff, f+index, f+index, prec ); /* sets coeff to coeff(f,index)*coeff(f,index) */
    if (index%2 == 1)
        acb_neg(coeff, coeff);               /* sets coeff to (-1)^index * coeff(f,index)*coeff(f,index) */
    
    deg = len-1;
    deltaindex = ( index <= (deg - index) ? index : (deg - index) );
    
    for ( i = 1; i<=deltaindex; i++){
        acb_mul(temp, f+(index-i), f+(index+i), prec ); /* sets temp to coeff(f,index-i)*coeff(f,index + i) */
        acb_mul_si(temp, temp, 2, prec);                /* sets temp to 2*coeff(f,index-i)*coeff(f,index + i) */
        if ((index-i)%2 == 1)
            acb_neg(temp, temp);                        /* sets temp to (-1)^(index-i) * 2*coeff(f,index-i)*coeff(f,index + i) */
        acb_add( coeff, temp, coeff, prec);
    }
        
    acb_clear(temp);
}
/* computes the coeffs up to power n of one Graeffe iteration of f, where f has length len and degree len-1 */
/* require: the 2n first coeffs of f */
/*          res has enough space to contain n+1 coeffs */ 
void _tstar_oneGraeffeIteration_first_n_coeff( acb_poly_t res, const acb_poly_t f, slong n, slong len, slong prec){
    
    slong i;
    acb_srcptr fptr = f->coeffs;
    acb_ptr resptr = res->coeffs;
    
    acb_mul( resptr, fptr, fptr, prec ); /* sets coeff(res,0) to coeff(f,0)*coeff(f,0) */
    for (i=1; i<=n; i++)                     /* computes coeff(res,i) */ 
        _tstar_oneGraeffeIteration_coeff( resptr + i, fptr, i, len, prec);
    _acb_poly_set_length(res, n+1);
}

void _tstar_graeffe_iterations_abs_two_first_coeffs( arb_t coeff0, arb_t coeff1, const acb_poly_t pApprox, int N, slong prec){
    acb_poly_t p1, p2;
    acb_poly_init2(p1, pApprox->length);
    acb_poly_init2(p2, pApprox->length);
    acb_poly_set(p1, pApprox);
    slong bound = 0x1 << N; /* assume 2^N fits in an slong: N<32 for 32bits machine... */
    for( int i = 0; i < N; i++) {
        bound = bound >> 1;
//         printf("bound: %d\n", (int) bound);
        _tstar_oneGraeffeIteration_first_n_coeff( p2, p1, PWPOLY_MIN(pApprox->length -1, bound), pApprox->length, prec);
        acb_poly_swap(p1,p2);
    }
    
    acb_abs( coeff0, p1->coeffs + 0, prec);
    acb_abs( coeff1, p1->coeffs + 1, prec);
    
    acb_poly_clear(p1);
    acb_poly_clear(p2);
}

void _tstar_oneGraeffeIteration_inplace( acb_poly_t poly, slong prec){

    acb_ptr fptr = poly->coeffs;
    slong len1 = poly->length;
    slong len2 = (len1/2)+1;
    slong i, rem, quo;
    
    acb_poly_t fe, fo;
    acb_poly_init2(fe, len2);
    acb_poly_init2(fo, len2);
    acb_ptr feptr = fe->coeffs;
    acb_ptr foptr = fo->coeffs;
    
    for (i = 0; i < len1; i++){
        rem = i%2;
        quo = i>>1;
        if (rem == 0) 
            acb_set( feptr + quo, fptr+i);
        else
            acb_set( foptr + quo, fptr+i);
    }
    _acb_poly_set_length(fe, len2);
    _acb_poly_set_length(fo, len2);
    
    acb_poly_t fes, fos;
    acb_poly_init2(fes, len1);
    acb_poly_init2(fos, len1);
    acb_poly_mullow( fes, fe, fe, len1, prec);
    acb_poly_mullow( fos, fo, fo, len1, prec);
    acb_poly_shift_left( fos, fos, 1 );
    acb_poly_sub(poly, fes, fos, prec);
    if ((len1%2)==0)
        acb_poly_neg(poly, poly);
    
    acb_poly_clear(fe);
    acb_poly_clear(fo);
    acb_poly_clear(fes);
    acb_poly_clear(fos);
}

void _tstar_sum_abs_coeffs_pos( arb_t res, const acb_poly_t poly, slong prec){
   
    arb_t t;
    arb_init(t);
    
    acb_abs(res, poly->coeffs + 1, prec);
    for(slong i = 2; i<poly->length; i++){
        acb_abs(t, poly->coeffs + i, prec);
        arb_add(res, res, t, prec);
    }
    
    arb_clear(t);
}

/* applies a Graeffe Pellet test to poly of degree d in the unit disc, at fixed working precision prec */
/* let N = 4 + ceil( log2(1 + log2 d) )                                                                */
/* applies n <= min( nbMDLG, N ) Graeffe iterations, INPLACE, to poly                                  */
/* assume nbMsols >= number of roots of poly in the unit disc                                          */
/* returns either -1, in which case poly has roots near the unit circle                                */
/*          or res>=0, in which case poly has exactly res roots in the unit disc                       */
slong _pwpoly_tstar_unit_disc_fixed_prec( acb_poly_t poly, int * n, int nbMDLG, slong nbMsols, slong prec){
    
#ifdef PW_PROFILE
    clock_t start = clock();
#endif
    
    slong len = acb_poly_length(poly);
    if (len==1) {
        if (    arb_is_nonzero( acb_realref(poly->coeffs + 0) )
             || arb_is_nonzero( acb_imagref(poly->coeffs + 0) ) )
            return 0;
    }
    arb_t absIthCoeff, absOtherCoeffs;
    arb_init(absIthCoeff);
    arb_init(absOtherCoeffs);
    
    int N = (int) 4+ceil(log2(1+log2(len-1)));
    
    if (nbMDLG>=0)
        N=PWPOLY_MIN(N, nbMDLG);
    
    *n = 0;
    slong res = -1;
    
    while ( (res==-1) && (*n<=N) ) {
        
        slong i = 0;
        
        acb_abs(absIthCoeff, poly->coeffs + i, prec);
            
        _tstar_sum_abs_coeffs_pos( absOtherCoeffs, poly, prec);
//         printf("n: %d, i:%ld, absIthCoeff: ", *n, i); arb_printd(absIthCoeff, 10);
//         printf(" absOtherCoeffs: "); arb_printd(absOtherCoeffs, 10); printf("\n");
        
        if (arb_gt(absIthCoeff, absOtherCoeffs))
            res = i;
        
        while ( (res==-1) && (i < len-1) && (i <= nbMsols) ){
            arb_add(absOtherCoeffs, absOtherCoeffs, absIthCoeff, prec);
            i = i+1;
            acb_abs(absIthCoeff, poly->coeffs + i, prec);
            arb_sub(absOtherCoeffs, absOtherCoeffs, absIthCoeff, prec);
            if (arb_gt(absIthCoeff, absOtherCoeffs))
                res = i;
        }
        
        if ( (res==-1) && (*n<N)) {
#ifdef PW_PROFILE
            clock_t start = clock();
#endif
            _tstar_oneGraeffeIteration_inplace( poly, prec);
#ifdef PW_PROFILE
            clicks_in_arbDLG += (clock() - start);
#endif
            *n = *n+1;
        } else if ( (res==-1) && (*n==N) ) {
            break;
        }
    }
    
    arb_clear(absIthCoeff);
    arb_clear(absOtherCoeffs);
    
#ifdef PW_PROFILE
    clicks_in_arbpellet += (clock() - start);
#endif
    
    return res;
    
}

// #define TSTAR_GT 1
// #define TSTAR_LT 0
// #define TSTAR_SI -1
// #define TSTAR_PR -2
int _tstar_soft_compare (const arb_t a, const arb_t b, slong prec) {
    if (arb_gt(a,b))
        return TSTAR_GT;
    if (arb_lt(a,b))
        return TSTAR_LT;
    
    int res = TSTAR_PR;
    arb_t m;
    arb_init(m);
    arb_mul_si(m, b, 3, prec);
    arb_div_si(m, m, 2, prec);
    if (arb_le(a,m)){
        arb_mul_si(m, a, 3, prec);
        arb_div_si(m, m, 2, prec);
        if (arb_le(b,m))
            res = TSTAR_SI; 
    }
    arb_clear(m);
    return res;
}

// #define TSTAR_MAX_PREC_REACHED -2
// #define TSTAR_ROOTS_NEAR_BOUND -1
// #define TSTAR_NO_ROOT          0
/* assume acb box b is square */
slong tstar_acb_arb( slong *prec, const acb_t center, const arb_t radius, 
                      pw_polynomial_t cache, slong nbMsols, int exclusion, int round, int anticipate ){
    
    slong len = pw_polynomial_length(cache);
    if (len==0) 
        return TSTAR_CAN_NOT_DECIDE;
    if (len==1) {
        acb_poly_srcptr polyapp = pw_polynomial_cacheANDgetApproximation(cache, *prec);
        if (    arb_is_nonzero( acb_realref(polyapp->coeffs + 0) )
             || arb_is_nonzero( acb_imagref(polyapp->coeffs + 0) ) )
            return TSTAR_NO_ROOT;
        else
            return TSTAR_CAN_NOT_DECIDE;
    }
    int N = (int) 4+ceil(log2(1+log2(len-1)));
    int n = 0;
    int anticipate_already_applied = 0;
    slong res = TSTAR_ROOTS_NEAR_BOUND;
    slong maxprec = 2*pw_polynomial_maxPreref(cache);
    maxprec = (maxprec>pw_polynomial_maxPreref(cache) ? maxprec : pw_polynomial_maxPreref(cache) );
    int max_prec_reached = ( (*prec) >= maxprec );
    
    arb_t absIthCoeff, absOtherCoeffs;
    arb_init(absIthCoeff);
    arb_init(absOtherCoeffs);
    
    acb_poly_t poly;
    acb_poly_init(poly);
    acb_poly_set( poly, pw_polynomial_cacheANDgetApproximation(cache, *prec) );
    /* apply taylor shift in the containing disk of box */
#ifdef PW_COCO_PROFILE
    clock_t start = clock();
#endif
    if ( (!acb_is_zero(center))||(!arb_is_one(radius)))
        _tstar_taylorShift_inplace( center, radius, poly, round, *prec);
#ifdef PW_COCO_PROFILE
    if (*prec==PWPOLY_DEFAULT_PREC)
        nb_shift_prec53++;
    if (exclusion)
        clicks_in_tstar_exclusion_shift += (clock() - start);
#endif    
    while ( (n<=N) && (res < TSTAR_NO_ROOT) ) {
        
        if ( (res == TSTAR_PR)&&(!max_prec_reached) ) { /* double precision */
            *prec = PWPOLY_MIN( 2*(*prec), maxprec );
            max_prec_reached = ( (*prec) >= maxprec );
            
            acb_poly_set( poly, pw_polynomial_cacheANDgetApproximation(cache, *prec) );
            /* apply taylor shift in the containing disk of box */
#ifdef PW_COCO_PROFILE
            start = clock();
#endif
            if ( (!acb_is_zero(center))||(!arb_is_one(radius)))
                _tstar_taylorShift_inplace( center, radius, poly, round, *prec);
#ifdef PW_COCO_PROFILE
            if (*prec==PWPOLY_DEFAULT_PREC)
                nb_shift_prec53++;
            if (exclusion)
                clicks_in_tstar_exclusion_shift += (clock() - start);
            start = clock();
#endif
            /* apply max(n-1,0) Graeffe iterations */
            for (int i = 0; i<n-1; i++)
                _tstar_oneGraeffeIteration_inplace( poly, *prec);
#ifdef PW_COCO_PROFILE
            if (exclusion) {
                clicks_in_tstar_exclusion_DLG += (clock() - start);
                nb_tstar_exclusion_DLG+=n-1;
            }
#endif
        }
#ifdef PW_COCO_PROFILE
        start = clock();
#endif        
        if (n>0)
            _tstar_oneGraeffeIteration_inplace( poly, *prec );
#ifdef PW_COCO_PROFILE
        if (exclusion) {
            clicks_in_tstar_exclusion_DLG += (clock() - start);
            nb_tstar_exclusion_DLG+=1;
        }
#endif        
        slong i = 0;
        acb_abs(absIthCoeff, poly->coeffs + i, *prec);
        _tstar_sum_abs_coeffs_pos( absOtherCoeffs, poly, *prec);
        res = _tstar_soft_compare (absIthCoeff, absOtherCoeffs, *prec);
        
        while ( ( (res==TSTAR_LT) || (res==TSTAR_SI) || ( (res==TSTAR_PR)&&(max_prec_reached) ) ) 
             && (i < nbMsols) ){
            i = i+1;
            arb_add(absOtherCoeffs, absOtherCoeffs, absIthCoeff, *prec);
            acb_abs(absIthCoeff, poly->coeffs + i, *prec);
            arb_sub(absOtherCoeffs, absOtherCoeffs, absIthCoeff, *prec);
            res = _tstar_soft_compare (absIthCoeff, absOtherCoeffs, *prec);
            
//             printf( "---enter while loop, i: %ld, res: %ld\n", i, res );
        }
        
        if ((res==TSTAR_LT)||(res==TSTAR_SI)) /* there is no i s.t. abs of i-th coeff is > sum of abs of other coeffs */
            res = TSTAR_SI;
        
        if (res==TSTAR_GT) /* abs of i-th coeff is > sum of abs of other coeffs */
            res=i;
        
        if ( anticipate&&(res==TSTAR_SI) ) {
            /* test */
            int test_anticipate = (exclusion==1)&&(anticipate_already_applied==0)&&((0x1 << (N-n)) <= ((len-1)/4));
//             test_anticipate=0;
            if (test_anticipate) {
#ifdef PW_COCO_PROFILE
                start = clock();
#endif
//                 printf(" n: %d, apply anticipate\n", n);
                anticipate_already_applied = 1;
                
                arb_t coeff0, coeff1, coeffn;
                arb_init(coeff0);
                arb_init(coeff1);
                arb_init(coeffn);
                _tstar_graeffe_iterations_abs_two_first_coeffs( coeff0, coeff1, poly, N-n, *prec);
                acb_abs( coeffn, poly->coeffs + (len-1), *prec);
                arb_pow_ui( coeffn, coeffn, (ulong)(0x1 << (N-n)), *prec);
                arb_add(coeff1, coeffn, coeff1, *prec);
                int restemp = _tstar_soft_compare (coeff0, coeff1, *prec);
//                 printf(" n: %d, res anticipate: %d\n", n, restemp);
                arb_clear(coeff0);
                arb_clear(coeff1);
                arb_clear(coeffn);
                if (restemp==TSTAR_LT) {
                    res = TSTAR_ROOTS_NEAR_BOUND;
                    n=N;
                }
#ifdef PW_COCO_PROFILE
                clicks_in_anticipate += (clock() - start);
                nb_anticipate+=1;
#endif                
            } 
        }
        
        if ( (!(res==TSTAR_PR)) || ( (res==TSTAR_PR)&&(max_prec_reached) ) ) {
            n = n+1;
        }
    }
    
    arb_clear(absIthCoeff);
    arb_clear(absOtherCoeffs);
    
    acb_poly_clear(poly);
    
    return res;
}

void  _tstar_get_center_radius_contDisc( acb_t center, arb_t radius, const acb_t bInit, slong prec ){
    acb_set(center, bInit);
    arb_get_rad_arb(radius, acb_realref(center));
    arb_get_mid_arb(acb_realref(center), acb_realref(center));
    arb_get_mid_arb(acb_imagref(center), acb_imagref(center));
    arb_mul_si(radius, radius, 3, prec);
    arb_div_si(radius, radius, 2, prec);
}

slong tstar_box( slong *prec, const box_t b, pw_polynomial_t cache, slong nbMsols, int exclusion, int round, int anticipate ){
    
    acb_t t, center;
    arb_t radius;
    acb_init(t);
    acb_init(center);
    arb_init(radius);
    box_get_acb(t, b);
    _tstar_get_center_radius_contDisc( center, radius, t, *prec );
//     slong res = _tstar_acb( prec, t, cache, nbMsols, exclusion, round, anticipate);
    slong res = tstar_acb_arb( prec, center, radius, cache, nbMsols, exclusion, round, anticipate);
    arb_clear(radius);
    acb_clear(center);
    acb_clear(t);
    
    return res;
    
}

slong tstar_box_CoCo( slong *prec, const box_CoCo_t cc, pw_polynomial_t cache, slong nbMsols ){
        
    acb_t t, center;
    arb_t radius;
    acb_init(t);
    acb_init(center);
    arb_init(radius);
    box_CoCo_get_contBox(t, cc);
    _tstar_get_center_radius_contDisc( center, radius, t, *prec );
//     slong res = _tstar_acb( prec, t, cache, nbMsols, 0, 0, 0); 
    slong res = tstar_acb_arb( prec, center, radius, cache, nbMsols, 0, 0, 0); 
    
    arb_clear(radius);
    acb_clear(center);
    acb_clear(t);
    
    return res;
    
}

void  _pwpoly_verif_get_center_radius_contDisc( acb_t center, arb_t radius, const acb_t b, slong prec ){
    
    arb_get_rad_arb(acb_realref(center), acb_realref(b));
    arb_get_rad_arb(acb_imagref(center), acb_imagref(b));
    if (arb_is_zero(acb_imagref(center)))
        arb_set(radius, acb_realref(center));
    else
        arb_min(radius, acb_realref(center), acb_imagref(center), prec);
    arb_get_mid_arb(acb_realref(center), acb_realref(b));
    arb_get_mid_arb(acb_imagref(center), acb_imagref(b));
}

/* the following function is ONLY USED FOR DEVELOPMENT, more precisely to verify correctness of an     */
/* output of our root isolator                                                                         */
/* applies a Graeffe Pellet test to p of degree d in the disc D CONTAINED IN b with max radius         */
/* assume nbMsols >= number of roots of p in the unit disc                                             */ 
/* if returns res >= 0, then p has exactly res roots in D                                              */
slong _pwpoly_tstar_acb( const acb_t b, const acb_poly_t p, slong nbMsols, slong *prec, slong maxprec ){
    
    slong len = p->length;
    if (len==0) 
        return TSTAR_CAN_NOT_DECIDE;
    if (len==1) {
        if (    arb_is_nonzero( acb_realref(p->coeffs + 0) )
             || arb_is_nonzero( acb_imagref(p->coeffs + 0) ) )
            return TSTAR_NO_ROOT;
        else
            return TSTAR_CAN_NOT_DECIDE;
    }
    
    int N = (int) 4+ceil(log2(1+log2(len-1)));
    int n = 0;
    slong res = TSTAR_ROOTS_NEAR_BOUND;
    int max_prec_reached = ( (*prec) >= maxprec );
    
    acb_t center;
    arb_t radius;
    acb_init(center);
    arb_init(radius);
    acb_poly_t poly;
    acb_poly_init2(poly, len);
    
    arb_t absIthCoeff, absOtherCoeffs;
    arb_init(absIthCoeff);
    arb_init(absOtherCoeffs);
    
    acb_poly_set( poly, p );
    _pwpoly_verif_get_center_radius_contDisc( center, radius, b, *prec );
    /* apply taylor shift in the containing disk of box */
    if ( (!acb_is_zero(center))||(!arb_is_one(radius)))
        _tstar_taylorShift_inplace( center, radius, poly, 0, *prec );
    
    while ( (n<=N) && (res < TSTAR_NO_ROOT) ) {
        
        if ( (res == TSTAR_PR)&&(!max_prec_reached) ) { /* double precision */
            *prec = PWPOLY_MIN( 2*(*prec), maxprec );
            max_prec_reached = ( (*prec) >= maxprec );
            acb_poly_set( poly, p );
            _pwpoly_verif_get_center_radius_contDisc( center, radius, b, *prec );
            /* apply taylor shift in the containing disk of box */
            if ( (!acb_is_zero(center))||(!arb_is_one(radius)))
                _tstar_taylorShift_inplace( center, radius, poly, 0, *prec );
            /* apply max(n-1,0) Graeffe iterations */
            for (int i = 0; i<n-1; i++)
                _tstar_oneGraeffeIteration_inplace( poly, *prec );
        }
        
        if (n>0)
            _tstar_oneGraeffeIteration_inplace( poly, *prec );
        
        slong i = 0;
        acb_abs(absIthCoeff, poly->coeffs + i, *prec);
        _tstar_sum_abs_coeffs_pos( absOtherCoeffs, poly, *prec );
        res = _tstar_soft_compare (absIthCoeff, absOtherCoeffs, *prec);
        
        while ( ( (res==TSTAR_LT) || (res==TSTAR_SI) || ( (res==TSTAR_PR)&&(max_prec_reached) ) ) 
             && (i < nbMsols) ){
            i = i+1;
            arb_add(absOtherCoeffs, absOtherCoeffs, absIthCoeff, *prec);
            acb_abs(absIthCoeff, poly->coeffs + i, *prec);
            arb_sub(absOtherCoeffs, absOtherCoeffs, absIthCoeff, *prec);
            res = _tstar_soft_compare (absIthCoeff, absOtherCoeffs, *prec);
        }
        
        if ( (!(res==TSTAR_PR)) || ( (res==TSTAR_PR)&&(max_prec_reached) ) ) {
            n = n+1;
        }
        
        if ((res==TSTAR_LT)||(res==TSTAR_SI)) /* there is no i s.t. abs of i-th coeff is > sum of abs of other coeffs */
            res = TSTAR_SI;
        
        if (res==TSTAR_GT) /* abs of i-th coeff is > sum of abs of other coeffs */
            res=i;
        
    }
    
    arb_clear(absIthCoeff);
    arb_clear(absOtherCoeffs);
    
    acb_clear(center);
    arb_clear(radius);
    acb_poly_clear(poly);
    
    return res;
}

slong _pwpoly_tstar_acb_2fmpq_poly( const acb_t b, const fmpq_poly_t p_re, const fmpq_poly_t p_im, 
                                    slong nbMsols, slong *prec, slong maxprec ){
    
    slong len = PWPOLY_MAX( p_re->length, p_im->length );
    if (len==0) 
        return TSTAR_CAN_NOT_DECIDE;
    if (len==1) {
        if ( (fmpz_is_zero( p_re->coeffs + 0 )==0) || (fmpz_is_zero( p_im->coeffs + 0 )==0) )
            return TSTAR_NO_ROOT;
        else
            return TSTAR_CAN_NOT_DECIDE;
    }
    
    int N = (int) 4+ceil(log2(1+log2(len-1)));
    int n = 0;
    slong res = TSTAR_ROOTS_NEAR_BOUND;
    int max_prec_reached = ( (*prec) >= maxprec );
    
    acb_t center;
    arb_t radius;
    acb_init(center);
    arb_init(radius);
    acb_poly_t poly;
    acb_poly_init2(poly, len);
    
    arb_t absIthCoeff, absOtherCoeffs;
    arb_init(absIthCoeff);
    arb_init(absOtherCoeffs);
    
    acb_poly_set2_fmpq_poly( poly, p_re, p_im, *prec );
    _pwpoly_verif_get_center_radius_contDisc( center, radius, b, *prec );
    /* apply taylor shift in the containing disk of box */
    if ( (!acb_is_zero(center))||(!arb_is_one(radius)))
        _tstar_taylorShift_inplace( center, radius, poly, 0, *prec );
    
    while ( (n<=N) && (res < TSTAR_NO_ROOT) ) {
        
        if ( (res == TSTAR_PR)&&(!max_prec_reached) ) { /* double precision */
            *prec = PWPOLY_MIN( 2*(*prec), maxprec );
            max_prec_reached = ( (*prec) >= maxprec );
            acb_poly_set2_fmpq_poly( poly, p_re, p_im, *prec );
            _pwpoly_verif_get_center_radius_contDisc( center, radius, b, *prec );
            /* apply taylor shift in the containing disk of box */
            if ( (!acb_is_zero(center))||(!arb_is_one(radius)))
                _tstar_taylorShift_inplace( center, radius, poly, 0, *prec );
            /* apply max(n-1,0) Graeffe iterations */
            for (int i = 0; i<n-1; i++)
                _tstar_oneGraeffeIteration_inplace( poly, *prec );
        }
        
        if (n>0)
            _tstar_oneGraeffeIteration_inplace( poly, *prec );
        
        slong i = 0;
        acb_abs(absIthCoeff, poly->coeffs + i, *prec);
        _tstar_sum_abs_coeffs_pos( absOtherCoeffs, poly, *prec );
        res = _tstar_soft_compare (absIthCoeff, absOtherCoeffs, *prec);
        
        while ( ( (res==TSTAR_LT) || (res==TSTAR_SI) || ( (res==TSTAR_PR)&&(max_prec_reached) ) ) 
             && (i < nbMsols) ){
            i = i+1;
            arb_add(absOtherCoeffs, absOtherCoeffs, absIthCoeff, *prec);
            acb_abs(absIthCoeff, poly->coeffs + i, *prec);
            arb_sub(absOtherCoeffs, absOtherCoeffs, absIthCoeff, *prec);
            res = _tstar_soft_compare (absIthCoeff, absOtherCoeffs, *prec);
        }
        
        if ( (!(res==TSTAR_PR)) || ( (res==TSTAR_PR)&&(max_prec_reached) ) ) {
            n = n+1;
        }
        
        if ((res==TSTAR_LT)||(res==TSTAR_SI)) /* there is no i s.t. abs of i-th coeff is > sum of abs of other coeffs */
            res = TSTAR_SI;
        
        if (res==TSTAR_GT) /* abs of i-th coeff is > sum of abs of other coeffs */
            res=i;
        
    }
    
    arb_clear(absIthCoeff);
    arb_clear(absOtherCoeffs);
    
    acb_clear(center);
    arb_clear(radius);
    acb_poly_clear(poly);
    
    return res;
}

// slong _tstar_acb( slong *prec, const acb_t b, pw_polynomial_t cache, slong nbMsols, int exclusion, int round, int anticipate ){
//     
//     slong len = pw_polynomial_length(cache);
//     if (len==1) {
//         acb_poly_srcptr polyapp = pw_polynomial_cacheANDgetApproximation(cache, *prec);
//         if (    arb_is_nonzero( acb_realref(polyapp->coeffs + 0) )
//              || arb_is_nonzero( acb_imagref(polyapp->coeffs + 0) ) )
//             return 0;
//     }
//     int N = (int) 4+ceil(log2(1+log2(len-1)));
//     int n = 0;
//     int anticipate_already_applied = 0;
//     slong res = TSTAR_ROOTS_NEAR_BOUND;
//     slong maxprec = 2*pw_polynomial_maxPreref(cache);
//     maxprec = (maxprec>pw_polynomial_maxPreref(cache) ? maxprec : pw_polynomial_maxPreref(cache) );
//     int max_prec_reached = ( (*prec) >= maxprec );
//     
//     acb_t center;
//     arb_t radius;
//     acb_init(center);
//     arb_init(radius);
//     _tstar_get_center_radius_contDisc( center, radius, b, *prec );
//     
//     arb_t absIthCoeff, absOtherCoeffs;
//     arb_init(absIthCoeff);
//     arb_init(absOtherCoeffs);
//     
//     acb_poly_t poly;
//     acb_poly_init(poly);
//     acb_poly_set( poly, pw_polynomial_cacheANDgetApproximation(cache, *prec) );
//     /* apply taylor shift in the containing disk of box */
// #ifdef PW_COCO_PROFILE
//     clock_t start = clock();
// #endif
//     if ( (!acb_is_zero(center))||(!arb_is_one(radius)))
//         _tstar_taylorShift_inplace( center, radius, poly, round, *prec);
// #ifdef PW_COCO_PROFILE
//     if (exclusion)
//         clicks_in_tstar_exclusion_shift += (clock() - start);
// #endif    
//     while ( (n<=N) && (res < TSTAR_NO_ROOT) ) {
//         
//         if ( (res == TSTAR_PR)&&(!max_prec_reached) ) { /* double precision */
//             *prec = PWPOLY_MIN( 2*(*prec), maxprec );
//             max_prec_reached = ( (*prec) >= maxprec );
//             _tstar_get_center_radius_contDisc( center, radius, b, *prec );
//             
//             acb_poly_set( poly, pw_polynomial_cacheANDgetApproximation(cache, *prec) );
//             /* apply taylor shift in the containing disk of box */
// #ifdef PW_COCO_PROFILE
//             start = clock();
// #endif
//             if ( (!acb_is_zero(center))||(!arb_is_one(radius)))
//                 _tstar_taylorShift_inplace( center, radius, poly, round, *prec );
// #ifdef PW_COCO_PROFILE
//             if (exclusion)
//                 clicks_in_tstar_exclusion_shift += (clock() - start);
//             start = clock();
// #endif
//             /* apply max(n-1,0) Graeffe iterations */
//             for (int i = 0; i<n-1; i++)
//                 _tstar_oneGraeffeIteration_inplace( poly, *prec );
// #ifdef PW_COCO_PROFILE
//             if (exclusion) {
//                 clicks_in_tstar_exclusion_DLG += (clock() - start);
//                 nb_tstar_exclusion_DLG+=n-1;
//             }
// #endif
//         }
//         
// #ifdef PW_COCO_PROFILE
//         start = clock();
// #endif
//         if (n>0)
//             _tstar_oneGraeffeIteration_inplace( poly, *prec);
// #ifdef PW_COCO_PROFILE
//         if (exclusion) {
//             clicks_in_tstar_exclusion_DLG += (clock() - start);
//             nb_tstar_exclusion_DLG+=1;
//         }
// #endif        
//         slong i = 0;
//         acb_abs(absIthCoeff, poly->coeffs + i, *prec);
//         _tstar_sum_abs_coeffs_pos( absOtherCoeffs, poly, *prec );
//         res = _tstar_soft_compare (absIthCoeff, absOtherCoeffs, *prec);
//         
//         while ( ( (res==TSTAR_LT) || (res==TSTAR_SI) || ( (res==TSTAR_PR)&&(max_prec_reached) ) ) 
//              && (i < nbMsols) ){
//             i = i+1;
//             arb_add(absOtherCoeffs, absOtherCoeffs, absIthCoeff, *prec);
//             acb_abs(absIthCoeff, poly->coeffs + i, *prec);
//             arb_sub(absOtherCoeffs, absOtherCoeffs, absIthCoeff, *prec);
//             res = _tstar_soft_compare (absIthCoeff, absOtherCoeffs, *prec);
//         }
//         
//         if ((res==TSTAR_LT)||(res==TSTAR_SI)) /* there is no i s.t. abs of i-th coeff is > sum of abs of other coeffs */
//             res = TSTAR_SI;
//         
//         if (res==TSTAR_GT) /* abs of i-th coeff is > sum of abs of other coeffs */
//             res=i;
//         
//         if ( anticipate&&(res==TSTAR_SI) ) {
//             /* test */
//             int test_anticipate = (exclusion==1)&&(anticipate_already_applied==0)&&((0x1 << (N-n)) <= ((len-1)/4));
// //             test_anticipate=0;
//             if (test_anticipate) {
// #ifdef PW_COCO_PROFILE
//                 start = clock();
// #endif
// //                 printf(" n: %d, apply anticipate\n", n);
//                 anticipate_already_applied = 1;
//                 
//                 arb_t coeff0, coeff1, coeffn;
//                 arb_init(coeff0);
//                 arb_init(coeff1);
//                 arb_init(coeffn);
//                 _tstar_graeffe_iterations_abs_two_first_coeffs( coeff0, coeff1, poly, N-n, *prec);
//                 acb_abs( coeffn, poly->coeffs + (len-1), *prec);
//                 arb_pow_ui( coeffn, coeffn, (ulong)(0x1 << (N-n)), *prec);
//                 arb_add(coeff1, coeffn, coeff1, *prec);
//                 int restemp = _tstar_soft_compare (coeff0, coeff1, *prec);
// //                 printf(" n: %d, res anticipate: %d\n", n, restemp);
//                 arb_clear(coeff0);
//                 arb_clear(coeff1);
//                 arb_clear(coeffn);
//                 if (restemp==TSTAR_LT) {
//                     res = TSTAR_ROOTS_NEAR_BOUND;
//                     n=N;
//                 }
// #ifdef PW_COCO_PROFILE
//                 clicks_in_anticipate += (clock() - start);
//                 nb_anticipate+=1;
// #endif                
//             } 
//         }     
//         
//         if ( (!(res==TSTAR_PR)) || ( (res==TSTAR_PR)&&(max_prec_reached) ) ) {
//             n = n+1;
//         }
//         
//     }
//     
//     arb_clear(absIthCoeff);
//     arb_clear(absOtherCoeffs);
//     
//     acb_clear(center);
//     arb_clear(radius);
//     acb_poly_clear(poly);
//     
//     return res;
// }
