/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef TSTAR_H
#define TSTAR_H

#include "pw_base.h"
#include "polynomial/pw_polynomial.h"
#include "geometry/box.h"
#include "geometry/box_CoCo.h"

#ifdef __cplusplus
extern "C" {
#endif

void _tstar_taylorShift_inplace( const acb_t c, const arb_t r, acb_poly_t poly, int round, slong prec);
void _tstar_oneGraeffeIteration_inplace( acb_poly_t poly, slong prec);
void _tstar_sum_abs_coeffs_pos( arb_t res, const acb_poly_t poly, slong prec);

/* applies a Graeffe Pellet test to poly of degree d in the unit disc, at fixed working precision prec */
/* let N = 4 + ceil( log2(1 + log2 d) )                                                                */
/* applies n <= min( nbMDLG, N ) Graeffe iterations, INPLACE, to poly                                  */
/* assume nbMsols >= number of roots of poly in the unit disc                                          */
/* returns either -1, in which case poly has roots near the unit circle                                */
/*          or res>=0, in which case poly has exactly res roots in the unit disc                       */
slong _pwpoly_tstar_unit_disc_fixed_prec( acb_poly_t poly, int * n, int nbMDLG, slong nbMsols, slong prec);

#define TSTAR_GT 1
#define TSTAR_LT 0
#define TSTAR_SI -1
#define TSTAR_PR -2
int _tstar_soft_compare (const arb_t a, const arb_t b, slong prec);

#define TSTAR_MAX_PREC_REACHED -2
#define TSTAR_ROOTS_NEAR_BOUND -1
#define TSTAR_CAN_NOT_DECIDE   -1
#define TSTAR_NO_ROOT          0
slong tstar_acb_arb( slong *prec, const acb_t c, const arb_t r, 
                              pw_polynomial_t cache, slong nbMsols, int exclusion, int round, int anticipate);
/* assume acb box b is square */
void  _tstar_get_center_radius_contDisc( acb_t center, arb_t radius, const acb_t bInit, slong prec );
slong tstar_box( slong *prec, const box_t b, pw_polynomial_t cache, slong nbMsols, int exclusion, int round, int anticipate);
slong tstar_box_CoCo( slong *prec, const box_CoCo_t cc, pw_polynomial_t cache, slong nbMsols);

// slong _tstar_acb( slong *prec, const acb_t b, pw_polynomial_t cache, slong nbMsols, int exclusion, int round, int anticipate);

/* the following function is ONLY USED FOR DEVELOPMENT, more precisely to verify correctness of an     */
/* output of our root isolator                                                                         */
/* applies a Graeffe Pellet test to p of degree d in the disc D CONTAINED IN b with max radius         */
/* assume nbMsols >= number of roots of p in the unit disc                                             */ 
/* if returns res >= 0, then p has exactly res roots in D                                              */
slong _pwpoly_tstar_acb( const acb_t b, const acb_poly_t p, slong nbMsols, slong *prec, slong maxprec );
slong _pwpoly_tstar_acb_2fmpq_poly( const acb_t b, const fmpq_poly_t p_re, const fmpq_poly_t p_im, 
                                    slong nbMsols, slong *prec, slong maxprec );

#ifdef PWPOLY_HAS_BEFFT
void _be_poly_oneDLGIteration_naive( double * dest, double * src, slong len );
slong tstar_unit_disc_be( double * poly, slong len, int * n, int nbMDLG, slong nbMsols);
#endif
#ifdef __cplusplus
}
#endif

#endif
