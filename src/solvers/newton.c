/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "newton.h"

#ifdef PW_PROFILE
#endif

/* input:  poly of degree len-1, a point point and a box box */
/* output: constant = poly(point), slope = poly'(point) and acceleration = poly''(box) */
/* with horner */
void _eval_point_slope_acceleration(acb_t constant, acb_t slope, acb_t acceleration, acb_srcptr poly, slong len, const acb_t point, const acb_t box, slong prec)
{
    slong i;
    acb_t c, s, a;
    acb_init(c);
    acb_init(s);
    acb_init(a);
    acb_zero(a);
    acb_zero(s);
    acb_set(c, poly+len-1);
    for(i=0; i<len-1; i++)
    {
        acb_mul(a, a, box, prec);
        acb_add(a, a, s, prec);
        acb_mul(s, s, point, prec);
        acb_add(s, s, c, prec);
        acb_mul(c, c, point, prec);
        acb_add(c, c, poly + len-1-i-1, prec);
    }
    acb_set(slope, s);
    acb_set(constant, c);
    acb_set(acceleration, a);
    acb_clear(a);
    acb_clear(s);
    acb_clear(c);
}

void _eval_order_one(acb_t result, const acb_t constant, const acb_t slope, const acb_t point, const acb_t box, slong prec)
{
    acb_t delta, df;
    acb_init(delta);
    acb_init(df);
    
    acb_sub(delta, box, point, prec);
    acb_mul(df, slope, delta, prec);
    acb_add(result, df, constant, prec);

    acb_clear(df);
    acb_clear(delta);
}

/* order two evaluation of a poly at a box */
/* input:  a point point and a box box, */
/*         constant = poly(point), slope = poly'(point) and acceleration =  poly''(box)  */
/* output: poly(point) + (box - point)*poly'(point) +  (box - point)^2*poly''(box)*/
void _eval_order_two(acb_t result, const acb_t constant, const acb_t slope, const acb_t acceleration, const acb_t point, const acb_t box, slong prec)
{
    acb_t delta;
    acb_init(delta);
    
    acb_sub(delta, box, point, prec);
    acb_mul(result, acceleration, delta, prec);
    acb_add(result, result, slope, prec);
    acb_mul(result, result, delta, prec);
    acb_add(result, result, constant, prec);

    acb_clear(delta);
}

void _newton_step(acb_t result, const acb_t point, const acb_ptr constant, const acb_t slope, slong prec)
{
    acb_t tmp, z, u;
    mag_t m;
    arb_t x, y;
    arf_t a, b;
    mag_init(m);
    acb_init(z);
    acb_init(u);
    arb_init(x);
    arb_init(y);
    arf_init(a);
    arf_init(b);
    acb_init(tmp);

    mag_hypot(m, arb_radref(acb_realref(slope)), arb_radref(acb_imagref(slope)));
    acb_get_mid(z, slope);
    acb_abs(x, z, prec);
    arb_add_error_mag(x, m);
    // better division using inversion of disk not containing 0 is a disk
    if(!arb_contains_zero(x))
    {
        acb_sgn(u, z, prec);
        arb_get_interval_arf(a, b, x, prec);
        arf_ui_div(a, 1, a, prec, ARF_RND_UP);
        arf_ui_div(b, 1, b, prec, ARF_RND_DOWN);
        arb_set_interval_arf(x, b, a, prec);
        arb_get_mid_arb(y, x);
        acb_mul_arb(z, constant, y, prec);
        acb_div(tmp, z, u, prec);
        acb_get_mag(m, constant);
        mag_mul(m, m, arb_radref(x));
        acb_add_error_mag(tmp, m);
    }
    else
    {
        acb_div(tmp, constant, slope, prec);
    }
    acb_sub(result, point, tmp, prec);

    acb_clear(tmp);
    arf_clear(b);
    arf_clear(a);
    arb_clear(y);
    arb_clear(x);
    acb_clear(u);
    acb_clear(z);
    mag_clear(m);
}

void _newton_point_step(acb_t result, acb_srcptr poly, slong len, const acb_t point, slong prec)
{
    acb_t eval, slope;
    acb_init(eval);
    acb_init(slope);

    _acb_poly_evaluate2(eval, slope, poly, len, point, prec);
//     _newton_step(result, point, eval, slope, prec);
    acb_div(eval, eval, slope, prec);
    acb_sub(result, point, eval, prec);
    acb_get_mid(result, result);
//     printf("eval: "); acb_printd(eval, 100); printf("\n");
    
    acb_clear(slope);
    acb_clear(eval);
}

void _newton_interval_step(acb_t result, acb_srcptr poly, slong len, const acb_t point, const acb_t box, slong prec)
{
    acb_t constant, slope, acceleration;
    acb_init(constant);
    acb_init(slope);
    acb_init(acceleration);

    _eval_point_slope_acceleration(constant, slope, acceleration, poly, len, point, box, prec);
    _eval_order_one(slope, slope, acceleration, point, box, prec);
    _newton_step(result, point, constant, slope, prec);

    // eval_point_slope(eval, slope, poly, len, point, box, prec);
    // //acb_div(eval, eval, slope, prec);
    // //acb_sub(result, point, eval, prec);
    // newton_step(result, point, eval, slope, prec);
    
    acb_clear(acceleration);
    acb_clear(slope);
    acb_clear(constant);
}

void _intersect(acb_t box, const acb_t crop, slong prec)
{
    arb_intersection(acb_realref(box), acb_realref(box), acb_realref(crop), prec);
    arb_intersection(acb_imagref(box), acb_imagref(box), acb_imagref(crop), prec);
}

void _inflate(acb_t inflated, const acb_t box, double scale)
{
    mag_ptr realmag, imagmag;
    mag_t epsilon;
    mag_init(epsilon);
    mag_set_d(epsilon, scale);
    acb_set(inflated, box);
    realmag = arb_radref(acb_realref(inflated));
    imagmag = arb_radref(acb_imagref(inflated)); 
    mag_mul(realmag, realmag, epsilon);
    mag_mul(imagmag, imagmag, epsilon);
    mag_clear(epsilon);
}

slong _newton_inclusion_test_acb_box( acb_t cbox, acb_srcptr poly, slong len,
                                      acb_t pivot, acb_t constant, acb_t slope, acb_t acceleration,
                                      slong prec ) {
    slong res = -1;
    acb_t eval, inflated, enclosure;
    acb_init(eval);
    acb_init(inflated);
    acb_init(enclosure);
    
    _eval_order_one(eval, slope, acceleration, pivot, cbox, prec);
    _newton_step(eval, pivot, constant, eval, prec);
    
    if(acb_is_finite(eval)) {
        acb_set(inflated, cbox);
        for(int i=0; (i<5) && !acb_ne(cbox, eval) 
                           && !acb_contains(inflated, eval) 
                           && acb_contains_zero(enclosure) 
                           && acb_is_finite(eval) 
                           && !acb_contains(eval, cbox); i++){
            _intersect(cbox, eval, prec);
            _inflate(inflated, cbox, 1.1);
            acb_get_mid(pivot, inflated);
            _eval_point_slope_acceleration(constant, slope, acceleration, poly, len, pivot, inflated, prec);        
            _eval_order_one(eval, slope, acceleration, pivot, inflated, prec);
            _newton_step(eval, pivot, constant, eval, prec);
            _eval_order_two(enclosure, constant, slope, acceleration, pivot, cbox, prec);
        }
        
        if(acb_ne(cbox, eval) || !acb_contains_zero(enclosure)) {
            res = 0; /* should never happen ? or maybe for cc intersecting the unit circle*/
        } else if(acb_contains(inflated, eval)) {
            acb_set(cbox, eval);
            res = 1;
        } else {
            if(acb_is_finite(eval)) {
                arb_intersection(acb_realref(cbox), acb_realref(cbox), acb_realref(eval), prec);
                arb_intersection(acb_realref(cbox), acb_realref(cbox), acb_realref(inflated), prec);
                arb_intersection(acb_imagref(cbox), acb_imagref(cbox), acb_imagref(eval), prec);
                arb_intersection(acb_imagref(cbox), acb_imagref(cbox), acb_imagref(inflated), prec);
                res = -1;
            } else
                res = -2;
        }
    } else {
        res = -2;
    }
    
    acb_clear(eval);
    acb_clear(inflated);
    acb_clear(enclosure);
    
    return res;
}

slong newton_inclusion_test_acb_box( acb_t cbox, acb_srcptr poly, slong len, slong prec ) {
    
    slong res = -1;
    acb_t pivot, constant, slope, acceleration;
    acb_init(pivot);
    acb_init(constant);
    acb_init(slope);
    acb_init(acceleration);
    
    acb_get_mid(pivot, cbox);
    _eval_point_slope_acceleration(constant, slope, acceleration, poly, len, pivot, cbox, prec);    
    res = _newton_inclusion_test_acb_box( cbox, poly, len, pivot, constant, slope, acceleration, prec);
    
    acb_clear(pivot);
    acb_clear(constant);
    acb_clear(slope);
    acb_clear(acceleration);
    
    return res;
}

/* apply newton contraction to box until */
/* either 2*radius(box) < 2^(log2_eps), in which case returns 1 */
/* or 2*radius(box) >= 2^(log2_eps)                             */
/* and Newton(box) is not strictly included in box,             */
/* in which case returns 0 meaning that prec must be increased  */
int solver_newton_refine_box_conv  (acb_t box, slong log2_eps, acb_srcptr poly, slong len_poly, slong prec){
    
    int res = 0;
    mag_t radius;
    mag_init(radius);
    
    mag_max(radius, arb_radref(acb_realref(box)), arb_radref(acb_imagref(box)) );

    if (mag_cmp_2exp_si(radius, log2_eps-1)>0) { /* width(box) > 2^(log2_eps) */
        /* apply newton iterations */
        acb_t pivot, eval, evaln, nbox;
        mag_t epsilon;
        
        acb_init(pivot);
        acb_init(eval);
        acb_init(evaln);
        acb_init(nbox);
        mag_init(epsilon);
        
        acb_get_mid(pivot, box);
        //get evaluation error
        _newton_point_step(pivot, poly, len_poly, pivot, prec);
        mag_max(epsilon, arb_radref(acb_realref(pivot)), arb_radref(acb_imagref(pivot)));
        mag_mul_ui(epsilon, epsilon, 2);
        acb_get_mid(pivot, pivot);
        acb_set(eval, pivot);
        acb_add_error_mag(eval, epsilon);
        
        _newton_interval_step(evaln, poly, len_poly, pivot, eval, prec);
        for(slong j=0; (j<prec) && !acb_contains(eval, evaln); j++)
        {
            //printf(".");
            _newton_point_step(pivot, poly, len_poly, pivot, prec);
            acb_get_mid(pivot, pivot);
            acb_set(eval, pivot);
            acb_add_error_mag(eval, epsilon);
            _newton_interval_step(evaln, poly, len_poly, pivot, eval, prec);
        }
        if(acb_contains(eval, evaln)) {
            acb_set(box, evaln);
            
            mag_max(radius, arb_radref(acb_realref(box)), arb_radref(acb_imagref(box)) );
            
            res = mag_cmp_2exp_si(radius, log2_eps-1)<=0;
        }
        
        while (res==0) {
            _newton_interval_step(nbox, poly, len_poly, pivot, box, prec);
            /* check if nbox is strictly included in box */
            /* and if radius(nbox) < 1/2 radius(box) */
            mag_max(epsilon, arb_radref(acb_realref(nbox)), arb_radref(acb_imagref(nbox)));
            mag_mul_ui(epsilon, epsilon, 2);
            mag_max(radius, arb_radref(acb_realref(box)), arb_radref(acb_imagref(box)));
            if ( (acb_contains_interior(box, nbox) == 0) || (mag_cmp(epsilon, radius)>=0) ) {
                res = -1;
            } else {
                acb_set(box, nbox);
                mag_max(radius, arb_radref(acb_realref(box)), arb_radref(acb_imagref(box)) );
                res = mag_cmp_2exp_si(radius, log2_eps-1)<=0;
            }
        }
            
        
        acb_clear(pivot);
        acb_clear(eval);
        acb_clear(evaln);
        acb_clear(nbox);
        mag_clear(epsilon);
        
    } else {
        res = 1;
    }
//     if ( reverse ) {
//         printf("solver_newton_refine_box_conv:            box: "); acb_printd(box, 10); printf("\n");
//         printf("                               transformedBox: "); acb_printd(transformedBox, 10); printf("\n");
//         printf("                                       radius: %.15le\n", mag_get_d(radius) );
//         printf("                                          res: %d\n", res); printf("\n");
//     }
    mag_clear(radius);
    if (res==-1)
        res = 0;
    return res;
}

// int solver_one_root( acb_t box, slong log2_eps, acb_srcptr poly, slong len, slong prec ) {
// 
//     int res = -1;
//     
//     acb_t pivot, eval, constant, slope, acceleration, inflated, enclosure;
//     acb_init(pivot);
//     acb_init(eval);
//     acb_init(constant);
//     acb_init(slope);
//     acb_init(acceleration);
//     acb_init(inflated);
//     acb_init(enclosure);
//     
//     arb_t abs, one;
//     arb_init(abs);
//     arb_init(one);
//     arb_one(one);
//     
//     /* set initial sol to zero */
//     acb_zero(box);
//     acb_abs(abs, box, prec);
//     
//     /* apply several newton iterations */
// //     clock_t start2 = clock();
//     _eval_point_slope_acceleration(constant, slope, acceleration, poly, len, box, box, prec);
// //     metadata_add_time_eval_newton_tests( meta, (double) (clock() - start2) );
//     
//     int i, j=0;
//     while( (j<4) ) {
//         for (i=0;i<10 && !acb_contains_zero(constant)
//                      &&  arb_lt(abs, one); i++ ) {
//             _eval_order_one(eval, slope, acceleration, box, box, prec);
//             _newton_step(box, box, constant, eval, prec);
//             acb_abs(abs, box, prec);
// //             clock_t start2 = clock();
//             _eval_point_slope_acceleration(constant, slope, acceleration, poly, len, box, box, prec);
// //             metadata_add_time_eval_newton_tests( meta, (double) (clock() - start2) );
//         }
//         
//         if (arb_lt(abs, one))
//             j=4;
//             
//         if ( !(arb_lt(abs, one)) && (j==0) ) {
//             arb_set_d ( acb_realref( box ), .5 );
//             arb_set_d ( acb_imagref( box ), .5 );
//             j++;
//             acb_abs(abs, box, prec);
// //             clock_t start2 = clock();
//             _eval_point_slope_acceleration(constant, slope, acceleration, poly, len, box, box, prec);
// //             metadata_add_time_eval_newton_tests( meta, (double) (clock() - start2) );
//         }
//         
//         if ( !(arb_lt(abs, one)) && (j==1) ) {
//             arb_set_d ( acb_realref( box ), .5 );
//             arb_set_d ( acb_imagref( box ), -0.5 );
//             j++;
//             acb_abs(abs, box, prec);
// //             clock_t start2 = clock();
//             _eval_point_slope_acceleration(constant, slope, acceleration, poly, len, box, box, prec);
// //             metadata_add_time_eval_newton_tests( meta, (double) (clock() - start2) );
//         }
//         
//         if ( !(arb_lt(abs, one)) && (j==2) ) {
//             arb_set_d ( acb_realref( box ), -0.5 );
//             arb_set_d ( acb_imagref( box ), -0.5);
//             j++;
//             acb_abs(abs, box, prec);
// //             clock_t start2 = clock();
//             _eval_point_slope_acceleration(constant, slope, acceleration, poly, len, box, box, prec);
// //             metadata_add_time_eval_newton_tests( meta, (double) (clock() - start2) );
//         }
//         
//         if ( !(arb_lt(abs, one)) && (j==3) ) {
//             arb_set_d ( acb_realref( box ), -0.5 );
//             arb_set_d ( acb_imagref( box ), 0.5);
//             j++;
//             acb_abs(abs, box, prec);
// //             clock_t start2 = clock();
//             _eval_point_slope_acceleration(constant, slope, acceleration, poly, len, box, box, prec);
// //             metadata_add_time_eval_newton_tests( meta, (double) (clock() - start2) );
//         }
//     }
//     
//     if ( arb_lt(abs, one) ) {
//         /* draw a small box around box */
//         arb_add_error_2exp_si( acb_realref(box), -6 );
//         arb_add_error_2exp_si( acb_imagref(box), -6 );
//         acb_abs(abs, box, prec);
//         if ( arb_lt(abs, one) ) {
//             acb_set(pivot, box);
//             res = newton_inclusion_test_acb_box(  box, poly, len, prec );
// //             printf("result newton: %ld\n", res);
//             if (res==1) {
//                 /*refine until wanted precision is met */
// //                 res = solver_newton_refine_box_conv_natural(box, pivot, 3, poly, len, transform, arg, prec, meta);
//                 res = solver_newton_refine_box_conv  (box, log2_eps, poly, len, prec);
//             }
//         }
//     }
//     
//     acb_clear(pivot);
//     acb_clear(eval);
//     acb_clear(constant);
//     acb_clear(slope);
//     acb_clear(acceleration);
//     acb_clear(inflated);
//     acb_clear(enclosure);
//     arb_clear(abs);
//     arb_clear(one);
//     
//     return res;
//     
// }
