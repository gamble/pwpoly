/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "QBR.h"

#ifdef PW_COCO_PROFILE
double clicks_in_QBR_evaluation;
double clicks_in_QBR_newtonInte;
double clicks_in_QBR_derivative;
#endif

void _QBR_acb_poly_evaluate (acb_t y, const acb_poly_t f, const acb_t x, slong prec) {
#ifdef PW_COCO_PROFILE
    clock_t start_QBR_eval = clock();
#endif
//     acb_poly_evaluate (y, f, x, prec);
    acb_poly_evaluate_rectangular (y, f, x, prec);
#ifdef PW_COCO_PROFILE
    clicks_in_QBR_evaluation += clock()-start_QBR_eval;
#endif    
}

void _QBR_acb_poly_evaluate2 (acb_t y, acb_t z, const acb_poly_t f, const acb_t x, slong prec) {
#ifdef PW_COCO_PROFILE
    clock_t start_QBR_eval = clock();
#endif
//     acb_poly_evaluate2 (y, z, f, x, prec);
    acb_poly_evaluate2_rectangular (y, z, f, x, prec);    
#ifdef PW_COCO_PROFILE
    clicks_in_QBR_evaluation += clock()-start_QBR_eval;
#endif
}

/* Preconditions:  c, d are assumed to be exact (i.e. error = 0)                  */
/*                      and are given in the transformed reference                */
/* Postconditions: transfroms c,r in the global reference and get center, radius  */
/*                 evaluate fcenter = f(center) and fpcenter = f'(center)         */
/*                 at prec so that soft_compare( 4*radius*|fpcenter|, |fcenter| ) */
/*                 does not return -2 (not enough prec)                           */
/*                 if it returns QBR_LT, returns FAIL                             */
/*                 if it returns QBR_GT or QBR_SI, returns SUCCESS                */
int _QBR_first_condition( slong *prec, acb_t fcenter, acb_t fpcenter, pw_polynomial_t cache,     
                          const acb_t center, const arb_t radius){
    
    int soft_comp_res = 0;
    int res = QBR_FAILURE;
    int max_prec_reached = ( (*prec) >= pw_polynomial_maxPreref(cache) );
    
    arb_t fcenterabs, fpcenterabs;
    arb_init(fcenterabs);
    arb_init(fpcenterabs);
    
    acb_poly_srcptr pApprox = pw_polynomial_cacheANDgetApproximation(cache, *prec);
    
    _QBR_acb_poly_evaluate2(fcenter, fpcenter, pApprox, center, *prec);
    acb_abs(fcenterabs, fcenter, *prec);
    acb_abs(fpcenterabs, fpcenter, *prec);
    arb_mul(fpcenterabs, fpcenterabs, radius, *prec);
    soft_comp_res = _tstar_soft_compare(fpcenterabs, fcenterabs, *prec);

    while( (soft_comp_res == TSTAR_PR) && (!max_prec_reached) ){
        *prec = PWPOLY_MIN( 2*(*prec), pw_polynomial_maxPreref(cache) );
        max_prec_reached = ( (*prec) >= pw_polynomial_maxPreref(cache) );
        pApprox = pw_polynomial_cacheANDgetApproximation(cache, *prec);
        _QBR_acb_poly_evaluate2(fcenter, fpcenter, pApprox, center, *prec);
        acb_abs(fcenterabs, fcenter, *prec);
        acb_abs(fpcenterabs, fpcenter, *prec);
        arb_mul(fpcenterabs, fpcenterabs, radius, *prec);
        soft_comp_res = _tstar_soft_compare(fpcenterabs, fcenterabs, *prec); 
    }
    
    arb_clear(fcenterabs);
    arb_clear(fpcenterabs);
    
    if (soft_comp_res==TSTAR_PR)
        res = QBR_MAXPREC;
    else if (soft_comp_res==TSTAR_LT)
        res = QBR_FAILURE;
    else
        res = QBR_SUCCESS;
    return res;
}

/* Preconditions:  c is assumed to be exact and given in the transformed reference */
/*                 fcenter=f(transform(c)), fpcenter = f'(transform(c))            */ 
/* Postconditions: compute one Schroeder's iteration, in the global frame          */
int _QBR_iteration( slong *prec, acb_t iteration, pw_polynomial_t cache, 
                    const box_CoCo_t CC, const acb_t center, 
                    acb_t fcenter, acb_t fpcenter){
    
    int res = QBR_SUCCESS;
    int max_prec_reached = ( (*prec) >= pw_polynomial_maxPreref(cache) );
    
    fmpz_t log2_errorBound, one;
    arb_t iterationError, errorBound;
    fmpz_init(log2_errorBound);
    fmpz_init(one);
    arb_init(iterationError);
    arb_init(errorBound);
    
    fmpz_set_si(one, 1);
    fmpz_set_si(log2_errorBound, -6);
    fmpz_sub( log2_errorBound, log2_errorBound, box_CoCo_QBRSpref(CC) );
    fmpz_add( log2_errorBound, log2_errorBound, box_CoCo_depthref(CC) );
    arb_set_fmpz_2exp( errorBound, one, log2_errorBound );
    
    acb_div(iteration, fcenter, fpcenter, *prec);
    acb_mul_si(iteration, iteration, box_CoCo_nbRooref(CC), *prec);
    acb_sub(iteration, center, iteration, *prec);
    acb_abs(iterationError, iteration, *prec);
    arb_get_rad_arb(iterationError, iterationError);
    
    while ( ( (arb_is_finite(iterationError)==0)||(arb_ge(iterationError, errorBound)==1))
            && ( !max_prec_reached ) ) {
        *prec = PWPOLY_MIN( 2*(*prec), pw_polynomial_maxPreref(cache) );
        max_prec_reached = ( (*prec) >= pw_polynomial_maxPreref(cache) );
        acb_poly_srcptr pApprox = pw_polynomial_cacheANDgetApproximation(cache, *prec);
        _QBR_acb_poly_evaluate2(fcenter, fpcenter, pApprox, center, *prec);
        acb_div(iteration, fcenter, fpcenter, *prec);
        acb_mul_si(iteration, iteration, box_CoCo_nbRooref(CC), *prec);
        acb_sub(iteration, center, iteration, *prec);
        acb_abs(iterationError, iteration, *prec);
        arb_get_rad_arb(iterationError, iterationError);
    }
    
    if ( (arb_is_finite(iterationError)==0)||(arb_ge(iterationError, errorBound)==1))
        res = QBR_MAXPREC;
    else
        res = QBR_SUCCESS;
    
    fmpz_clear(log2_errorBound);
    fmpz_clear(one);
    arb_clear(iterationError);
    arb_clear(errorBound);
    
    return res;
}

/*test: interval newton */
/* performs a newton test for the acb box ball = B(c, (2/3)*r) contained in the disc D(c,r);
 * returns 1 if interval newton certifies the existence of a solution in b;
 *         0 otherwise */

/* Preconditions:  c, r are given in the transformed reference                     */ 
/* Postconditions: */
int _QBR_newton_interval(  acb_t nball, const acb_t center, const arb_t radius, pw_polynomial_t cache,
                          slong log2_eps, slong prec PW_VERBOSE_ARGU(verbose) ) {
    
#ifdef PW_COCO_PROFILE
    clock_t start_QBR_newtonInt = clock();
#endif
    
    int res = QBR_FAILURE;
#ifndef PW_SILENT
    int level = 4;

    if (verbose >=level) {
        printf("_QBR_newton_interval,       input disk: ");
        printf("center: "); acb_printd(center, 10); 
        printf("radius: "); arb_printd(radius, 10);
        printf("\n");
    }
#endif
    
    acb_t cBall, ball, fcBall, fpBall;
    arb_t error;
    
    acb_init(cBall);
    acb_init(ball);
    acb_init(fcBall);
    acb_init(fpBall);
    arb_init(error);
    
    acb_poly_t ppApprox;
    acb_poly_init2(ppApprox, pw_polynomial_lengthref(cache));
    
    acb_set(ball, center);
    arb_set(error, radius);
    arb_mul_si( error, error, 2, prec );
    arb_div_si( error, error, 3, prec );
    acb_add_error_arb( ball, error );
#ifndef PW_SILENT
    if (verbose >=level) {
        printf("_QBR_newton_interval, disk transformed in acb_ball: ");
        acb_printd(ball, 10);
        printf("\n");
    }
#endif
    
    acb_set(cBall, center);
    
    acb_poly_srcptr pApprox = pw_polynomial_cacheANDgetApproximation(cache, prec);
#ifdef PW_COCO_PROFILE
    clock_t start_QBR_derivative = clock();
#endif    
    acb_poly_derivative( ppApprox, pApprox, prec );
#ifdef PW_COCO_PROFILE
    clicks_in_QBR_derivative += clock()-start_QBR_derivative;
#endif
    _QBR_acb_poly_evaluate(fpBall, ppApprox, ball, prec);
        
#ifndef PW_SILENT
    if (verbose >=level) {
        printf("_QBR_newton_interval, derivative evaluated on disk: ");
        acb_printd(fpBall, 10);
        printf("\n");
    }
#endif
    
    if (acb_contains_zero(fpBall)){
        res = QBR_DER_CONT_ZERO; /* do nothing */
    } else {
        _QBR_acb_poly_evaluate(fcBall, pApprox, cBall, prec);
        acb_div( fcBall, fcBall, fpBall, prec);
        acb_sub( fcBall, cBall, fcBall, prec);
        if (acb_contains(ball, fcBall)) {
            res=QBR_SUCCESS;
        }
    }
    if (res==QBR_SUCCESS){
        if (acb_is_exact(fcBall)) { /* to prevent the rare case where newton iteration is the exact root */
            slong nlog2_eps = log2_eps - 4;
            /* check that log2_eps + 4 does not produce an overflow */
            if (nlog2_eps < log2_eps) {
                arb_add_error_2exp_si( acb_realref(fcBall), nlog2_eps );
                arb_add_error_2exp_si( acb_imagref(fcBall), nlog2_eps );
            }
        }
        if (acb_contains(ball, fcBall))
            acb_set(nball, fcBall);
    }
    
    acb_clear(cBall);
    acb_clear(ball);
    acb_clear(fcBall);
    acb_clear(fpBall);
    arb_clear(error);
    acb_poly_clear(ppApprox);
    
#ifdef PW_COCO_PROFILE
    clicks_in_QBR_newtonInte += clock()-start_QBR_newtonInt;
#endif
    
    return res;
}

void _QBR_get_new_radius( arb_t radius, const box_CoCo_t CC ) {
    
    fmpz_t log2_radius, one;
    
    fmpz_init(log2_radius);
    fmpz_init(one);
    
    fmpz_set_si(log2_radius, -3);
    fmpz_sub(log2_radius, log2_radius, box_CoCo_QBRSpref(CC) );
    fmpz_add(log2_radius, log2_radius, box_CoCo_depthref(CC) );
    fmpz_set_si(one, 1);
    arb_set_fmpz_2exp( radius, one, log2_radius );
    fmpz_clear(one);
    fmpz_clear(log2_radius);
}

int QBR_box_CoCo( slong *prec, box_CoCo_ptr * CC, pw_polynomial_t cache, slong log2_eps PW_VERBOSE_ARGU(verbose) ) {
    
#ifndef PW_SILENT    
    int level = 4;
#endif
    
    int res = QBR_FAILURE;
    
    arb_t twodiam, radius;
    acb_t initialPoint, fcenter, fpcenter, iteration, initialContBox, iterationContBox;
    
    arb_init(twodiam);
    arb_init(radius);
    acb_init(fcenter);
    acb_init(fpcenter);
    acb_init(iteration);
    acb_init(initialPoint);
    acb_init(initialContBox);
    acb_init(iterationContBox);
    
    /* get containing box of CC */
    box_CoCo_get_contBox(initialContBox, *CC);
    
    /* initial point */
    if (box_CoCo_nbRooref(*CC)==1) {
        acb_get_mid(initialPoint, initialContBox);
    } else {
        acb_zero(initialPoint);
        box_CoCo_get_supRe_arf( arb_midref ( acb_realref(initialPoint) ) , *CC );
        box_CoCo_get_supIm_arf( arb_midref ( acb_imagref(initialPoint) ) , *CC );
    }
        
    box_CoCo_diameter(twodiam, *CC);
    arb_mul_2exp_si(twodiam, twodiam, 1);
    
    res = _QBR_first_condition( prec, fcenter, fpcenter, cache, initialPoint, twodiam);
#ifndef PW_SILENT
    if (verbose >=level)
        printf(" QBR first condition: %d\n", res );
#endif
    if (res == QBR_SUCCESS) {
        res = _QBR_iteration( prec, iteration, cache, *CC, initialPoint, fcenter, fpcenter);
#ifndef PW_SILENT        
        if (verbose >=level)
            printf(" QBR iteration: %d\n", res );
#endif
    }
        
    if (res == QBR_SUCCESS) {    
        acb_get_mid(iteration, iteration);
        _QBR_get_new_radius( radius, *CC );
        /* sets iterationContBox as the acb_box containing the disc D(iteration, radius)*/
        acb_set(iterationContBox, iteration);
        acb_add_error_arb( iterationContBox, radius );
        if ( acb_contains( initialContBox, iterationContBox ) )
            res= QBR_SUCCESS;
        else 
            res= QBR_FAILURE;
        
#ifndef PW_SILENT
        if (verbose >=level)
            printf(" QBR transform back: %d\n", res );
#endif
    }
    
    if (res==QBR_SUCCESS) {
        res = QBR_FAILURE;
        if ( box_CoCo_nbRooref(*CC)==1 ) {
            res = _QBR_newton_interval( iterationContBox, iteration, radius, cache, log2_eps, *prec PW_VERBOSE_CALL(verbose) );
#ifndef PW_SILENT
            if (verbose >=level)
                printf(" res interval newton in QBR: %d\n", res );
#endif       
            if (res==QBR_DER_CONT_ZERO) {
                int Nres = QBR_DER_CONT_ZERO;
                int nb_Niter = 0;
                slong Nprec = *prec;
                acb_t Niteration, NiterationContBox;
                arb_t Nradius;
                acb_init(Niteration);
                acb_init(NiterationContBox);
                arb_init(Nradius); 
                acb_set(Niteration, iteration);
                acb_set(NiterationContBox, iterationContBox);
                
                /* pretend the disc contains the root */
                while ( (Nres == QBR_DER_CONT_ZERO) && ( nb_Niter < 10 ) ) {
                    nb_Niter++;
                    /* increase speed */
                    box_CoCo_increase_QBRSp(*CC);
                    /* apply another Schroeder iteration */
                    acb_get_mid(initialPoint, Niteration);
                    acb_poly_srcptr pApprox = pw_polynomial_cacheANDgetApproximation(cache, Nprec);
                    _QBR_acb_poly_evaluate2(fcenter, fpcenter, pApprox, initialPoint, Nprec);
                    Nres = _QBR_iteration( &Nprec, Niteration, cache, *CC, initialPoint, fcenter, fpcenter);
#ifndef PW_SILENT
                    if (verbose >=level)
                        printf(" nb_Niter: %d, res Schroeder's iteration in QBR: %d\n", nb_Niter, Nres );
#endif
                    if (Nres == QBR_SUCCESS) {    
                        acb_get_mid(Niteration, Niteration);
                        _QBR_get_new_radius( Nradius, *CC );
                        /* sets iterationContBox as the acb_box containing the disc D(iteration, radius)*/
                        acb_set(NiterationContBox, Niteration);
                        acb_add_error_arb( NiterationContBox, Nradius );
                        if ( acb_contains( initialContBox, NiterationContBox ) )
                            Nres= QBR_SUCCESS;
                        else 
                            Nres= QBR_FAILURE;
#ifndef PW_SILENT
                        if (verbose >=level)
                            printf(" nb_Niter: %d, res construct new disc in QBR: %d\n", nb_Niter, Nres );
#endif
                    }
                    /* apply another interval newton test */
                    if (Nres == QBR_SUCCESS) {
                        Nres = _QBR_newton_interval( NiterationContBox, Niteration, Nradius, cache, log2_eps, Nprec PW_VERBOSE_CALL(verbose) );       
#ifndef PW_SILENT
                        if (verbose >=level)
                            printf(" nb_Niter: %d, res interval newton in QBR: %d\n", nb_Niter, Nres );
#endif
                    }
               }
               if ( Nres == QBR_SUCCESS ) {
                   acb_set(iterationContBox, NiterationContBox);
                   *prec = Nprec;
                   res = QBR_SUCCESS;
               } else {
                   /* reset speed */
                   while ( nb_Niter >= 0 ) {
                       nb_Niter--;
                       box_CoCo_decrease_QBRSp(*CC);
                   }
               }
               acb_clear(Niteration);
               acb_clear(NiterationContBox);
               arb_clear(Nradius);
               
            }
        }
        
        if (res<=0) {
            slong nbR = tstar_acb_arb( prec, iteration, radius, cache, box_CoCo_nbRooref(*CC), 0, 0, 0);
#ifndef PW_SILENT
            if (verbose >=level)
                printf(" res tstar in QBR: %ld\n", nbR );
#endif            
            if (nbR == TSTAR_PR)
                res = QBR_MAXPREC;
            else
                res = (nbR==box_CoCo_nbRooref(*CC));
            
        }
    }
#ifndef PW_SILENT
    if (verbose >=level)
        printf(" QBR validation: %d, prec: %ld\n\n", res, *prec);
#endif    
    if (res==QBR_SUCCESS) {
        box_list_t temp;
        box_list_init(temp);
        box_CoCo_ptr nCC = (box_CoCo_ptr) pwpoly_malloc ( sizeof(box_CoCo) );
        box_CoCo_init(nCC);
        box_get_box_list_acb( temp, iterationContBox );
        while ( !box_list_is_empty(temp) )
            box_CoCo_insert_box(nCC, box_list_pop(temp));
        box_list_clear(temp);
        
        box_CoCo_Mprecref(nCC) = *prec;
        box_CoCo_nbRooref(nCC) = box_CoCo_nbRooref(*CC);
        box_CoCo_2_Sepref(nCC) = box_CoCo_2_Sepref(*CC);
        box_CoCo_6_Sepref(nCC) = box_CoCo_6_Sepref(*CC);
        
        box_CoCo_w_encref(nCC) = box_CoCo_w_encref(*CC);
        box_CoCo_1_Sepref(nCC) = box_CoCo_1_Sepref(*CC);
        box_CoCo_nbRIEref(nCC) = box_CoCo_nbRIEref(*CC);
        if(box_CoCo_w_encref(*CC))
            acb_set(box_CoCo_encloref(nCC), box_CoCo_encloref(*CC));
        
        box_CoCo_reuse_QBRSp(nCC, *CC);
        
        box_CoCo_clear(*CC);
        pwpoly_free(*CC);
        *CC = nCC;
        res = 1;
        
    }
    
    arb_clear(twodiam);
    arb_clear(radius);
    acb_clear(initialPoint);
    acb_clear(fcenter);
    acb_clear(fpcenter);
    acb_clear(iteration);
    acb_clear(initialContBox);
    acb_clear(iterationContBox);
    
    return res;
}
