/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_ACB_LIST_H
#define PW_ACB_LIST_H

#include "pw_base.h"
#include "common/pw_common.h"
#include "list/pw_list.h"

#ifdef __cplusplus
extern "C" {
#endif
    
/* list of acb_t boxes */
PWPOLY_INLINE void acb_clear_for_list(void * b){
    acb_clear( (acb_ptr) b );
}
#ifndef PW_SILENT
PWPOLY_INLINE void acb_fprint_for_list(FILE * file, const void * b){
    acb_fprint(file, (acb_ptr) b);
}
PWPOLY_INLINE void acb_fprintd_for_list(FILE * file, const void * b, slong digits){
    acb_fprintd(file, (acb_ptr) b, digits);
}
#endif
PWPOLY_INLINE int acb_real_part_lt_for_list(const void * b1, const void * b2){
    return arb_lt( acb_imagref( (acb_ptr) b1 ), acb_imagref( (acb_ptr) b2 ) );
}

typedef struct list   acb_list;
typedef struct list   acb_list_t[1];
typedef struct list * acb_list_ptr;

// PWPOLY_INLINE void    acb_list_init ( acb_list_t l )                 { list_init(l, acb_clear_for_list); }
PWPOLY_INLINE void    acb_list_init ( acb_list_t l )                 { list_init(l); }
PWPOLY_INLINE void    acb_list_swap ( acb_list_t l1, acb_list_t l2 ) { list_swap(l1, l2); }
PWPOLY_INLINE void    acb_list_append ( acb_list_t l1, acb_list_t l2 ) { list_append(l1, l2); }
// PWPOLY_INLINE void    acb_list_clear( acb_list_t l )                 { list_clear(l); }
PWPOLY_INLINE void    acb_list_clear( acb_list_t l )                 { list_clear(l, acb_clear_for_list); }
PWPOLY_INLINE void    acb_list_empty( acb_list_t l )                 { list_empty(l); }
PWPOLY_INLINE void    acb_list_push ( acb_list_t l, acb_ptr b )      { list_push(l,b); }
PWPOLY_INLINE void    acb_list_push_front ( acb_list_t l, acb_ptr b ){ list_push_front(l,b); }
PWPOLY_INLINE void    acb_list_insert_sorted( acb_list_t l, acb_ptr b, int (isless_func)(const void * d1, const void * d2) )
{ list_insert_sorted(l, b, isless_func); }
PWPOLY_INLINE acb_ptr acb_list_pop  ( acb_list_t l )                 { return (acb_ptr) list_pop(l); }
PWPOLY_INLINE acb_ptr acb_list_first( acb_list_t l )                 { return (acb_ptr) list_first(l); }
PWPOLY_INLINE acb_ptr acb_list_last ( acb_list_t l )                 { return (acb_ptr) list_last(l); }
#ifndef PW_SILENT
PWPOLY_INLINE void    acb_list_fprint( FILE * file, acb_list_t l )   { list_fprint(file, l, acb_fprint_for_list); }
PWPOLY_INLINE void    acb_list_fprintd( FILE * file, acb_list_t l, slong digits )   { list_fprintd(file, l, digits, acb_fprintd_for_list); }
PWPOLY_INLINE void    acb_list_print (              acb_list_t l )   { acb_list_fprint(stdout, l); }
#endif
PWPOLY_INLINE int     acb_list_is_empty  ( acb_list_t l )            { return list_is_empty(l); }
PWPOLY_INLINE slong   acb_list_get_size  ( const acb_list_t l )            { return list_get_size(l); }

/*iterator */
typedef list_iterator acb_list_iterator;
PWPOLY_INLINE acb_list_iterator acb_list_begin(const acb_list_t l)   { return list_begin(l); }
PWPOLY_INLINE acb_list_iterator acb_list_endEl(const acb_list_t l)   { return list_endEl(l); }
PWPOLY_INLINE acb_list_iterator acb_list_next(acb_list_iterator it)  { return list_next(it); }
PWPOLY_INLINE acb_list_iterator acb_list_prev(acb_list_iterator it)  { return list_prev(it); }
PWPOLY_INLINE acb_list_iterator acb_list_end()                       { return list_end(); }
PWPOLY_INLINE acb_ptr           acb_list_elmt(acb_list_iterator it)  { return (acb_ptr) list_elmt(it); }
    
#ifdef __cplusplus
}
#endif    

#endif
