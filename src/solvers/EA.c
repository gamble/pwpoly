/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_EA.h"

#ifdef PW_EA_PROFILE
double clicks_in_initial;
double clicks_in_newtoEA;
double clicks_in_EA_itts;
double clicks_in_EA_dists;
double clicks_in_inclEA;
double clicks_in_inc_evals;
double clicks_in_weierstrass;
double clicks_in_weier_dists;
double clicks_in_evaluations;
double clicks_in_zoom_clusts;
double clicks_in_union_find;
double clicks_in_pd_ntamed;
double clicks_in_check_nat;
double clicks_in_actualize_ccs;
slong  nb_newtEA;
slong  nb_evals;
slong  nb_EA_dists;
slong  nb_inc_evals;
slong  nb_weier_dists;
slong  nb_zoom_clusts;
#endif

void _slong_clear(void * i){
}

void _slong_list_copy( slong_list_t res, const slong_list_t src ) {
    _slong_list_clear(res);
    _slong_list_init(res);
    slong_list_iterator it = _slong_list_begin(src);
    while ( it!=_slong_list_end() ) {
        _slong_list_push ( res, _slong_list_elmt(it) );
        it = _slong_list_next( it );
    }
}

void _conCom_init_acb( conCom_t cc, acb_srcptr boxes, slong index ) {
    acb_init( conCom_containing_boxref(cc) );
    acb_set( conCom_containing_boxref(cc), boxes + index );
    _slong_list_init( conCom_component_discsref(cc) );
    _slong_list_push( conCom_component_discsref(cc), index );
    conCom_naturalref(cc) = 0;
    conCom_sizeOKref(cc)  = 0;
    conCom_sepCritref(cc) = 0;
//     conCom_tamedClref(cc) = 0;
}

void _conCom_init_set( conCom_t res, const conCom_t src ) {
    acb_init( conCom_containing_boxref(res) );
    acb_set( conCom_containing_boxref(res), conCom_containing_boxref(src) );
    _slong_list_init( conCom_component_discsref(res) );
    _slong_list_copy( conCom_component_discsref(res), conCom_component_discsref(src) );
    conCom_naturalref(res) = conCom_naturalref(src);
    conCom_sizeOKref(res) = conCom_sizeOKref(src);
    conCom_sepCritref(res) = conCom_sepCritref(src);
//     conCom_tamedClref(res) = conCom_tamedClref(src);
}

void _conCom_clear( conCom_t cc ) {
    acb_clear( conCom_containing_boxref(cc) );
    _slong_list_clear( conCom_component_discsref(cc) );
}

void _acb_set_square( acb_t box ) {
    if ( mag_cmp( arb_radref( acb_realref( box )), 
                  arb_radref( acb_imagref( box )) ) > 0 )
        mag_set( arb_radref( acb_imagref( box )),
                 arb_radref( acb_realref( box )) );
    else 
        mag_set( arb_radref( acb_realref( box )),
                 arb_radref( acb_imagref( box )) );
}
void _conCom_union( conCom_t cc1, conCom_t cc2, slong prec ) {
    acb_union( conCom_containing_boxref(cc1), conCom_containing_boxref(cc1), conCom_containing_boxref(cc2), prec );
    /* make union a square box */
    _acb_set_square( conCom_containing_boxref(cc1) );
    /* actualize indexes */
    while ( !_slong_list_is_empty( conCom_component_discsref(cc2) ) ) {
        list_insert_sorted( conCom_component_discsref(cc1), list_pop( conCom_component_discsref(cc2) ), _slong_isless );
//         _slong_list_insert_sorted( conCom_component_discsref(cc1), _slong_list_pop( conCom_component_discsref(cc2) )); 
    }
    /* reset flags */
    conCom_naturalref(cc1) = 0;
    conCom_sizeOKref(cc1) = 0;
    conCom_sepCritref(cc1) = 0;
//     conCom_tamedClref(cc1) = 0;
    _conCom_clear( cc2 );
}

#ifndef PW_SILENT
void _conCom_fprintd( FILE * file, const conCom_t cc, slong digits ){
    fprintf( file, "connected component of %ld discs with containing box: ", 
             _slong_list_get_size( conCom_component_discsref(cc) ) );
    acb_fprintd( file, conCom_containing_boxref(cc), digits );
    fprintf(file, "\n indices of discs: [ ");
    slong_list_iterator it = _slong_list_begin( conCom_component_discsref(cc) );
    while ( it!=_slong_list_end() ) {
        fprintf(file, "%ld", _slong_list_elmt(it));
        it = _slong_list_next(it);
        if (it!=_slong_list_end())
            fprintf(file, ", ");
    }
    fprintf(file, "]\n");
    fprintf(file, "natural: %d, sizeOK: %d, sepCrit: %d\n",
                   conCom_naturalref(cc), conCom_sizeOKref(cc), conCom_sepCritref(cc));
//     fprintf(file, "natural: %d, sizeOK: %d, sepCrit: %d, tamedCl: %d\n",
//                    conCom_naturalref(cc), conCom_sizeOKref(cc), conCom_sepCritref(cc), conCom_tamedClref(cc));
}
#endif

void _conCom_list_union_find( conCom_list_t l, conCom_ptr ncc, slong prec){
    
    conCom_list_t res;
    _conCom_list_init(res);
    conCom_ptr b = ncc;
    conCom_ptr c;
    int steady = 0;
    
    while (steady==0){
        
        steady = 1;
        while (!_conCom_list_is_empty(l)) {
            c = _conCom_list_pop(l);
            if ( _conCom_overlaps( c, b ) ) {
                steady=0;
                _conCom_union( c, b, prec ); /* b is cleared here */
                pwpoly_free(b);
                b = c;
            } else {
                _conCom_list_push( res, c );
            }
        }
        _conCom_list_swap( res, l );
        
    }
    
    _conCom_list_push( l, b );
    
    _conCom_list_clear(res);
}

void _conCom_list_compute( conCom_list_t l, acb_srcptr discs, slong len, slong prec) {
#ifdef PW_EA_PROFILE
    clock_t start_uf = clock();
#endif
    for (slong i=0; i<len; i++) {
        conCom_ptr b = (conCom_ptr) pwpoly_malloc (sizeof(conCom));
        _conCom_init_acb( b, discs, i );
        _conCom_list_union_find( l, b, prec );
    }
#ifdef PW_EA_PROFILE
    clicks_in_union_find += (clock() - start_uf);
#endif
}

void _conCom_list_check_natural_and_size( conCom_list_t l, slong log2eps, int goal) {

#ifdef PW_EA_PROFILE
    clock_t start = clock();
#endif
    
    acb_t threeBox;
    acb_init(threeBox);
    conCom_ptr ccur, ccur2;
    
    conCom_list_iterator it = _conCom_list_begin(l);
    while ( it!=_conCom_list_end() ) {
        
        ccur = _conCom_list_elmt(it);
        acb_set( threeBox, conCom_containing_boxref(ccur) );
        
        conCom_sizeOKref(ccur) = conCom_sizeOKref(ccur) || (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                               ||pwpoly_width_less_eps( threeBox, log2eps, PW_GOAL_PREC_RELATIVE(goal) );
                               
        if ( conCom_naturalref(ccur) == 0 ) {
            conCom_naturalref(ccur) = 1;
        
            /* verify that containing box does not intersect 0 */
            if ( acb_contains_zero( threeBox ) )
                conCom_naturalref(ccur) = 0;
        
            /* inflate by factor 3 */
            mag_mul_ui( arb_radref(acb_realref(threeBox)), arb_radref(acb_realref(threeBox)), 3);
            mag_mul_ui( arb_radref(acb_imagref(threeBox)), arb_radref(acb_imagref(threeBox)), 3);
        
            /* verify that 3*containing box does not intersect 0 */
            if ( acb_contains_zero( threeBox ) )
                conCom_naturalref(ccur) = 0;
        
            /* check if threeBox is separated from other cc */
            conCom_list_iterator it2 = _conCom_list_begin(l);
            while ( (it2!=_conCom_list_end()) && (conCom_naturalref(ccur)==1)  ) {
            
                if (it2!=it) {
                    ccur2 = _conCom_list_elmt(it2);
                    if (acb_overlaps( threeBox, conCom_containing_boxref(ccur2) )) {
                        conCom_naturalref(ccur) = 0;
                    }
                }
            
                it2 = _conCom_list_next(it2);
            }
        }
        
        if ( (_slong_list_get_size( conCom_component_discsref(ccur) ) > 1) &&
             (conCom_naturalref(ccur) == 1) &&
             (PW_GOAL_MUST_ISOLATE(goal) || (conCom_sizeOKref(ccur)==0) ) &&
             (conCom_sepCritref(ccur) == 0) ) {
            
            acb_set( threeBox, conCom_containing_boxref(ccur) );
            /* inflate by factor 12 */
            mag_mul_ui( arb_radref(acb_realref(threeBox)), arb_radref(acb_realref(threeBox)), 12);
            mag_mul_ui( arb_radref(acb_imagref(threeBox)), arb_radref(acb_imagref(threeBox)), 12);
            conCom_sepCritref(ccur)=1;
            /* verify that 12*containing box does not intersect 0 */
            if ( acb_contains_zero( threeBox ) )
                conCom_sepCritref(ccur) = 0;
            /* check if threeBox is separated from other cc */
            conCom_list_iterator it2 = _conCom_list_begin(l);
            while ( (it2!=_conCom_list_end()) && (conCom_sepCritref(ccur)==1)  ) {
            
                if (it2!=it) {
                    ccur2 = _conCom_list_elmt(it2);
                    if (acb_overlaps( threeBox, conCom_containing_boxref(ccur2) )) {
                        conCom_sepCritref(ccur) = 0;
                    }
                }
            
                it2 = _conCom_list_next(it2);
            }
//             printf("---_conCom_list_check_natural_and_size: 12 separated: %d\n", conCom_sepCritref(ccur) );
        }
        
        it = _conCom_list_next(it);
    }
    
    acb_clear(threeBox);
    
#ifdef PW_EA_PROFILE
    clicks_in_check_nat += (clock() - start);
#endif
    
}

void _conCom_list_actualize( conCom_list_t l, 
                             acb_srcptr roots, int *tamed, 
                             slong log2eps, int goal, slong prec) {

#ifdef PW_EA_PROFILE
    clock_t start = clock();
#endif
    
    conCom_list_t decreased, increased;
    _conCom_list_init(decreased);
    _conCom_list_init(increased);
    
    conCom_ptr ccur;
    acb_t n_cont_box;
    acb_init(n_cont_box);
    int allTamed;
    slong_list_iterator itind;
    slong ind;
    
    /* separate ccs in decreased and increased */
    while ( !_conCom_list_is_empty(l) ) {
        ccur = _conCom_list_pop(l);
        itind = _slong_list_begin( conCom_component_discsref( ccur ) );
        ind = _slong_list_elmt(itind);
        allTamed = PW_EA_IS_TAMED(tamed[ind]);
        acb_set( n_cont_box, roots + ind );
        itind = _slong_list_next( itind );
        while ( itind != _slong_list_end() ) {
            ind = _slong_list_elmt(itind);
            allTamed = allTamed && PW_EA_IS_TAMED(tamed[ind]);
            acb_union( n_cont_box, n_cont_box, roots + ind, prec ); 
            itind = _slong_list_next( itind );
        }
        /* make union a square box */
        _acb_set_square( n_cont_box );
        
        if ( allTamed || acb_contains( conCom_containing_boxref( ccur ), n_cont_box ) ) {
            _conCom_list_push( decreased, ccur );
        }
        else {
            _conCom_list_push( increased, ccur );
        }
    }
    
    /* process ccs that have decreased */
    while ( !_conCom_list_is_empty(decreased) ) {
        ccur = _conCom_list_pop(decreased);
        slong mult = _slong_list_get_size( conCom_component_discsref( ccur ) );
        itind = _slong_list_begin( conCom_component_discsref( ccur ) );
        ind = _slong_list_elmt(itind);
        if (mult==1) {
            /* actualize containing box and keep natural and size flags */
            acb_set( conCom_containing_boxref( ccur ), roots + ind );
            /* if some CC have increased, reset natural flag */
            if (_conCom_list_get_size(increased)>0)
                conCom_naturalref(ccur) = 0;
            
            _conCom_list_push( l, ccur );
        } else {
            int wasNatural = conCom_naturalref( ccur )&&(_conCom_list_get_size(increased)==0);
            int wasSeparat = conCom_sepCritref( ccur )&&(_conCom_list_get_size(increased)==0);
//             int wasSizeOK  = conCom_sizeOKref( ccur );
            /* do a union find between boxes in the ccs */
            conCom_list_t ccs;
            _conCom_list_init(ccs);
            conCom_ptr ncc = (conCom_ptr) pwpoly_malloc ( sizeof(conCom) );
            _conCom_init_acb( ncc, roots, ind );
            _conCom_list_push( ccs, ncc );
            itind = _slong_list_next( itind );
            while ( itind != _slong_list_end() ) {
                ind = _slong_list_elmt(itind);
                ncc = (conCom_ptr) pwpoly_malloc ( sizeof(conCom) );
                _conCom_init_acb( ncc, roots, ind );
                _conCom_list_union_find( ccs, ncc, prec );
                itind = _slong_list_next( itind );
            }
            if ( _conCom_list_get_size(ccs)==1 ) { /* ccur has not been splitted */
                /* copy natural and sep flag */
                conCom_naturalref(_conCom_list_first(ccs)) = wasNatural;
                conCom_sepCritref(_conCom_list_first(ccs)) = wasSeparat;
            } else { /* ccur has been splitted */
                /* check naturalness between new connected components */
                if (wasNatural)
                    _conCom_list_check_natural_and_size( ccs, log2eps, goal);
            }
            
            /* push ccs in l */
            while ( !_conCom_list_is_empty(ccs) )
                _conCom_list_push( l, _conCom_list_pop(ccs) );
            
            _conCom_clear(ccur);
            pwpoly_free(ccur);
            _conCom_list_clear(ccs);
        }
    }
    /* l is a correct list of connected components */
    /* process ccs that have increased: do union find with constituant boxes */
    while ( !_conCom_list_is_empty(increased) ) {
        ccur = _conCom_list_pop(increased);
        while ( !_slong_list_is_empty( conCom_component_discsref(ccur) ) ) {
            ind = _slong_list_pop(conCom_component_discsref(ccur));
            conCom_ptr ncc = (conCom_ptr) pwpoly_malloc ( sizeof(conCom) );
            _conCom_init_acb( ncc, roots, ind );
            _conCom_list_union_find( l, ncc, prec );
        }
        _conCom_clear(ccur);
        pwpoly_free(ccur);
    }
    
    acb_clear(n_cont_box);
    _conCom_list_clear(increased);
    _conCom_list_clear(decreased);
    
#ifdef PW_EA_PROFILE
    clicks_in_actualize_ccs += (clock() - start);
#endif
}

/* if res is not empty, empty it */
void _conCom_list_copy( conCom_list_t res, const conCom_list_t src ) {
    _conCom_list_clear(res);
    _conCom_list_init(res);
    conCom_list_iterator it = _conCom_list_begin( src );
    while (it!=_conCom_list_end()) {
        conCom_ptr ccur = (conCom_ptr) pwpoly_malloc ( sizeof( conCom ) );
        _conCom_init_set(ccur, _conCom_list_elmt(it));
        _conCom_list_push( res, ccur );
        it = _conCom_list_next(it);
    }
}

int _pw_EA_arb_cmp( const arb_t u, const arb_t v ) {
    return arf_cmp( arb_midref( u ), arb_midref( v ));
}

/* assume radii[0] = -infinity, radii[len-1] = +infinity                */
/* radii is sorted increasing                                           */
/* assume modpoint is finite and modpoint >=0                           */
/* assume from < len-1                                                  */
/* if there is at least one non-saturated annulus, returns 1<=res<len-1 */
ulong _pw_EA_closest_non_saturated_annulus( const arb_t modpoint, arb_srcptr radii, slong *nbval, slong *nbsat, ulong len, ulong *from,  
                                            slong prec PW_VERBOSE_ARGU(verbose)  ) {
#ifndef PW_SILENT    
    int level = 4;
#endif
    arb_t distinf, distsup;
    arb_init(distinf);
    arb_init(distsup);
    ulong inf = *from;
#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("_pw_EA_closest_non_saturated_annulus: from: %ld, len: %ld\n", *from, len);
        printf("---modpoint: "); arb_printd(modpoint, 10); printf("\n");
    }
#endif
    /* find 0<=inf<len-1 s.t. mid(radii[inf])<=mid(modpoint)<mid(radii[inf+1]) */
    while ( (inf>0)     && (arf_cmp( arb_midref(radii + inf), arb_midref(modpoint) ) > 0) )
        inf--;
    while ( (inf+1<len) && (arf_cmp( arb_midref(radii + (inf+1)), arb_midref(modpoint) ) <= 0) )
        inf++;
    ulong sup = inf+1;
    *from = inf;
    /* find the first non saturated annulii, downward and upward from inf and sup */
    while ( (nbsat[inf] == nbval[inf]) && (inf>0) )
        inf--;
    while ( (nbsat[sup] == nbval[sup]) && (sup<len-1) )
        sup++;

#ifndef PW_SILENT    
    if (verbose>=level) {
        printf("---inf: %ld\n", inf);
    }
#endif
    if (inf==0)
        arb_pos_inf(distinf);
    else {
        arb_sub( distinf, modpoint, radii + inf, prec );
        arb_abs( distinf, distinf );
    }
#ifndef PW_SILENT
    if (verbose>=level) {
        printf("---sup: %ld\n", sup);
    }
#endif
    if (sup==len-1)
        arb_pos_inf(distsup);
    else {
        arb_sub( distsup, modpoint, radii + sup, prec );
        arb_abs( distsup, distsup );
    }
    ulong res = inf;
    if (arf_cmp( arb_midref(distsup), arb_midref(distinf) ) < 0)
        res = sup;
    
    arb_clear(distsup);
    arb_clear(distinf);
#ifndef PW_SILENT    
    if (verbose>=level)
        printf("---res: %ld, nbsat[res]/nbval[res]: %ld/%ld, from: %ld\n", 
               res, nbsat[res], nbval[res], *from);
#endif    
    return res;
}

/* Pre-conditions:  let d = degree(poly), assume poly_0\neq0 and poly_d\neq0                      */
/*                  assume roots, tamed have enough room for d acb's, and int                     */ 
/*                  assume i < lenNatural => (roots + i) is a natural isolator for 1 root of poly */
/* Post-conditions: keep unchanged the lenNatural first elements of roots                         */
/*                  set approprately the lenNatural first elements of tamed                       */
/*                  fill the d-lenNatural last elements of roots with initial values for EA itts  */
/*                  set the d-lenNatural last elements of tamed to 0                              */
slong _pw_EA_initial_values_acb_poly ( acb_ptr roots, int *tamed, slong *nbWild, slong *nbTamed,
                                       slong lenNatural, 
//                                        const acb_poly_t poly,
                                       pw_polynomial_t cache,
                                       slong log2eps, int goal, const domain_t dom, 
                                       slong prec PW_VERBOSE_ARGU(verbose)  ) {
#ifndef PW_SILENT    
    int level = 2;
#endif    
    acb_poly_srcptr poly = pw_polynomial_cacheANDgetApproximation(cache, prec);
    slong len = poly->length;
    arb_ptr mantissas = _arb_vec_init( len );
    fmpq_ptr exponents= _fmpq_vec_init( len );
    
    /* compute approx abs of coeffs */
    for (slong i = 0; i<len; i++) {
        acb_abs(mantissas+i, poly->coeffs + i, prec);
        arb_get_mid_arb( mantissas+i, mantissas+i );
    }
    
    _pw_approximation_compute_mantissas_exponents_arb_ptr( mantissas, 
                                                           exponents, 
                                                           mantissas, (ulong) len );
    
    /* compute newton polygon with exponents */
    for (slong i = 0; i<len; i++)
        fmpq_neg(exponents+i, exponents+i );
    /* compute the lower part of the convex hull of -exponents */
    ulong *CH = (ulong *) pwpoly_malloc ( len*sizeof(ulong) );
    ulong lenCH = _pw_covering_fmpq_convex_hull( CH, exponents, len );
// #ifndef PW_SILENT    
//     if (verbose >= level) {
//         printf("indexes of the edges of the convex: \n[");
//         for (ulong i=0; i<lenCH; i++)
//             printf("%lu, ", CH[i]);
//         printf("]\n");
//         
//         FILE * CHFile;
//         char CHFileName[] = "CH.plt\0";
//         CHFile = fopen (CHFileName,"w");
//     
//         _pw_covering_fmpq_convex_hull_gnuplot( CHFile, CH, lenCH, exponents, len);
//         
//         fclose (CHFile);
//     }
// #endif
    /* compute lenCH+1 radii of circles and lenCH+1 numb. of initial values on each annulus */
    /* with radii[0] = -inf, radii[lenCH] = +inf */
    /*      nbval[0] = 0,    nbval[lenCH] = 0 */
    arb_ptr radii = _arb_vec_init( lenCH+1 );
    slong  *nbval = (slong *) pwpoly_malloc ( (lenCH+1)*sizeof(slong) );
    arb_neg_inf(radii + 0);
    arb_pos_inf(radii + lenCH);
    nbval[0] = 0;
    nbval[lenCH] = 0;
    fmpq_t logr, den;
    fmpq_init(logr);
    fmpq_init(den);
    for (ulong i = 0; i<lenCH-1; i++) {
        nbval[i+1] = CH[i+1] - CH[i];
        /* r[i+1] = | 2^exponents[CH[i]] / 2^exponents[CH[i+1]] | ^ (1/CH[i+1]-CH[i]) */
        /*        =   2^(exponents[CH[i]] - exponents[CH[i+1]]) ^ (1/CH[i+1]-CH[i])   */
        /*        =   2^ -(exponents[CH[i+1]])-exponents[CH[i]])/nbval[i+1]           */
        fmpq_sub( logr, exponents + CH[i+1], exponents + CH[i] );
        fmpq_set_ui( den, 1, (ulong) nbval[i+1] );
        fmpq_mul(logr, logr, den);
        arb_set_si( radii + (i+1), 2 );
        arb_pow_fmpq( radii + (i+1), radii + (i+1), logr, prec );
    }
#ifndef PW_SILENT    
    if (verbose >= level) {
        printf("_pw_EA_initial_values_acb_poly: radii and nb of vals: \n");
        for (ulong i = 0; i<lenCH+1; i++) {
            printf("--- %lu-th: ", i); arb_printd( radii + i, 10 ); printf(" , %ld initial values\n", nbval[i]);
        }
        slong tot = 0;
        for (ulong i = 0; i<lenCH+1; i++)
            tot += nbval[i];
        printf("_pw_EA_initial_values_acb_poly: nb of initial values: %ld\n", tot);
    }
#endif    
    slong  *nbsat = (slong *) pwpoly_malloc ( (lenCH+1)*sizeof(slong) );
    for (ulong i = 0; i<lenCH+1; i++) {
        nbsat[i] = 0;
    }
    
    if (lenNatural>0) {
        arb_ptr mods = _arb_vec_init(lenNatural);
        for (slong i=0; i<lenNatural; i++) {
            acb_abs( mods + i, roots + i, prec );
            arb_get_mid_arb(mods + i, mods + i);
        }
        /* sort centers of tamed roots by increasing modulii */
        qsort(mods, lenNatural, sizeof(arb_struct), (__compar_fn_t) _pw_EA_arb_cmp);
        
        /*locate each root in the closest annulus */
        ulong from = 0;
        for (slong i=0; i<lenNatural; i++) {
            ulong ann = _pw_EA_closest_non_saturated_annulus( mods+i, radii, nbval, nbsat, lenCH+1, &from, prec PW_VERBOSE_CALL(verbose)  );
            nbsat[ann]++;
        }
        
        _arb_vec_clear(mods, lenNatural);
    }
    
    
    slong indInInitialValues = lenNatural;

    acb_t rot;
    acb_init(rot);
    acb_ptr unit_roots = _acb_vec_init( len );
    for (ulong i = 1; i<lenCH; i++) {
        slong nbValsInAnn =  nbval[i] - nbsat[i];
        if (nbValsInAnn>0) {
            
            _acb_vec_unit_roots(unit_roots, nbValsInAnn, nbValsInAnn, prec);
            acb_unit_root(rot, 10*nbValsInAnn, prec);
            for (slong j=0; j< nbValsInAnn; j++ ) {
                acb_mul_arb( roots + (indInInitialValues+j), unit_roots + j, radii+i, prec );
                acb_mul( roots + (indInInitialValues+j), roots + (indInInitialValues+j), rot, prec );
                acb_get_mid( roots + (indInInitialValues+j), roots + (indInInitialValues+j) );
                tamed[ indInInitialValues+j ] = 0;
                if (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                    PW_EA_SET_APPROXIMD( tamed[ indInInitialValues+j ] );
                (*nbWild)++;
            }
        }
        indInInitialValues += nbValsInAnn;
    }
    _acb_vec_clear(unit_roots, len);
    acb_clear(rot);
    
    for (slong i = 0; i<lenNatural; i++) {
//         acb_set( dest + (indInInitialValues+i), roots + i );
//         PW_EA_SET_TAMED( tamed[ indInInitialValues+i ] );
        tamed[i]=0;
        PW_EA_SET_INCLUSION( tamed[ i ], PW_NATURAL_I );
        int sizeOK = (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                    ||pwpoly_width_less_eps( roots+i, log2eps, PW_GOAL_PREC_RELATIVE(goal) );
        int domaOK = !( domain_is_C(dom)||domain_acb_intersect_domain(dom, roots+i) );
        if ( (sizeOK)||(domaOK) )
            PW_EA_SET_APPROXIMD(tamed[ i ]);
        if (PW_EA_IS_TAMED(tamed[i]))
            (*nbTamed)++;
        else 
            (*nbWild)++;
    }
    
    pwpoly_free(nbsat);
    fmpq_clear(den);
    fmpq_clear(logr);
    pwpoly_free(nbval);
    _arb_vec_clear( radii, lenCH+1 );
    pwpoly_free(CH);
    _fmpq_vec_clear( exponents, len );
    _arb_vec_clear( mantissas, len );
    
    return indInInitialValues;
}

int _EA_check_newtamed_pairwise_disjoint( acb_ptr roots, int * tamed, int *ntamed, slong lenRoots ) {

#ifdef PW_EA_PROFILE
    clock_t start = clock();
#endif
    
    int res = 1;
    for (slong i=0; (i<lenRoots-1) && (res==1); i++) {
        if (PW_EA_IS_TAMED( ntamed[i] )) {
            for (slong j=0; (j<lenRoots-1) && (res==1); j++) {
                if ((j!=i)&&(PW_EA_IS_TAMED( ntamed[j] )||PW_EA_IS_TAMED( tamed[j] )))
                    res = (acb_overlaps( roots + i, roots + j )==0);
            }
        }
//         if (PW_EA_IS_NATURAL_I( ntamed[i] )) {
//             for (slong j=0; (j<lenRoots-1) && (res==1); j++) {
//                 if ((j!=i)&&(PW_EA_IS_NATURAL_I( ntamed[j] )||PW_EA_IS_NATURAL_I( tamed[j] )))
//                     res = (acb_overlaps( roots + i, roots + j )==0);
//             }
//         }
    }
    
#ifdef PW_EA_PROFILE
    clicks_in_pd_ntamed += (clock() - start);
#endif
    return res;
}

#define midre(X) arb_midref(acb_realref(X))
#define midim(X) arb_midref(acb_imagref(X))
#define radre(X) arb_radref(acb_realref(X))
#define radim(X) arb_radref(acb_imagref(X))
void _EA_acb_sub_mid( acb_t res, acb_t a, acb_t b, slong prec ) {
    arf_sub( midre(res), midre(a), midre(b), prec, ARF_RND_NEAR );
    arf_sub( midim(res), midim(a), midim(b), prec, ARF_RND_NEAR );
    mag_zero(radre(res));
    mag_zero(radim(res));
}
void _EA_acb_add_mid( acb_t res, acb_t a, acb_t b, slong prec ) {
    arf_add( midre(res), midre(a), midre(b), prec, ARF_RND_NEAR );
    arf_add( midim(res), midim(a), midim(b), prec, ARF_RND_NEAR );
    mag_zero(radre(res));
    mag_zero(radim(res));
}
void _EA_acb_inv_mid( acb_t res, acb_t a, slong prec ) {
    arf_t den;
    arf_init(den);
    arf_mul( den, midre(a), midre(a), prec, ARF_RND_NEAR );
    arf_addmul( den, midim(a), midim(a), prec, ARF_RND_NEAR );
    arf_div( midre(res), midre(a), den, prec, ARF_RND_NEAR );
    arf_div( midim(res), midim(a), den, prec, ARF_RND_NEAR );
    arf_neg( midim(res), midim(res) );
    mag_zero(radre(res));
    mag_zero(radim(res));
    arf_clear(den);
}
#undef midre
#undef midim
#undef radre
#undef radim

/* not used */
// void _EA_one_iteration_on_wild_roots( acb_ptr roots, int * tamed, 
//                                       acb_ptr polyvals, acb_ptr pdervals,
//                                       slong lenRoots,
//                                       slong prec ) {
// 
// #ifdef PW_EA_PROFILE
//     clock_t start_ea = clock();
// #endif
//         
// //     slong lprec = PWPOLY_MAX(PWPOLY_DEFAULT_PREC, prec/4);
//     slong lprec = prec;
//     acb_ptr sumOfInvDists = _acb_vec_init( lenRoots );
//     acb_t temp, pval, pderval;
//     acb_init(temp);
//     acb_init(pval);
//     acb_init(pderval);
//     /* compute sums of inv dists */
// #ifdef PW_EA_PROFILE
//     clock_t start_di = clock();
// #endif
//     for (slong i=0; i<lenRoots; i++) {
//         if ( PW_EA_IS_TAMED(tamed[i]) )
//             continue;
//         
//         if (PW_EA_IS_NATURAL_I( tamed[i] ))
//             continue;
//         
//         acb_get_mid( roots + i, roots + i );
//         acb_zero( sumOfInvDists + i );
//         for (slong j=0; j<lenRoots; j++) {
//             if ( !(j==i) ) {
//                 _EA_acb_sub_mid( temp, roots+i, roots+j, lprec );
//                 _EA_acb_inv_mid( temp, temp, lprec );
//                 _EA_acb_add_mid( sumOfInvDists + i, sumOfInvDists + i, temp, lprec );
//             }
//         }
// #ifdef PW_EA_PROFILE
//         nb_EA_dists++;
// #endif
//     }
// #ifdef PW_EA_PROFILE
//     clicks_in_EA_dists += (clock() - start_di);
// #endif 
//         
//     /* apply EA iteration to wild roots */
//     for (slong i=0; i<lenRoots; i++) {
//         if ( PW_EA_IS_TAMED(tamed[i]) )
//             continue;
//         
//         if (PW_EA_IS_NATURAL_I( tamed[i] ))
//             continue;
//         
//         acb_get_mid( pval, polyvals+i );
//         acb_get_mid( pderval, pdervals+i );
//         acb_div( temp, pval, pderval, lprec );
//         acb_get_mid( temp, temp );
//         acb_mul( pval, temp, sumOfInvDists + i, lprec );
//         acb_get_mid( pval, pval );
//         acb_one( pderval );
//         acb_sub( pval, pderval, pval, lprec );
//         acb_get_mid( pval, pval );
//         acb_div( temp, temp, pval, lprec );
//         acb_get_mid( temp, temp );
//         acb_sub( roots+i, roots+i, temp, lprec );
//         acb_get_mid( roots+i, roots+i );
//     }
//     acb_clear(pderval);
//     acb_clear(pval);
//     acb_clear(temp);
//     _acb_vec_clear(sumOfInvDists, lenRoots);
//     
// #ifdef PW_EA_PROFILE
//         clicks_in_EA_itts += (clock() - start_ea);
// #endif 
//         
// }

int  _EA_get_inclusion_disc_and_status( arb_t radius, const acb_t root, acb_t polyval, acb_t pderval,
                                        pw_polynomial_t cache,
                                        slong prec PW_VERBOSE_ARGU(verbose)  ) {
    int res = PW_INDETERMI;
    
    slong lprec = 2*prec;
    acb_poly_srcptr polyD = pw_polynomial_cacheANDgetApproxDerivat(cache, lprec);
    acb_srcptr polyder = polyD->coeffs;
    slong lenPoly = polyD->length +1;
    
    acb_t r, r3, f, fpp;
    acb_init(r);
    acb_init(r3);
    acb_init(f);
    acb_init(fpp);
    arb_t one, left, K, absf, absfp, absfpp;
    arb_init(one);
    arb_init(left);
    arb_init(K);
    arb_init(absf);
    arb_init(absfp);
    arb_init(absfpp);
    acb_get_mid(r, root);
    /* eval poly and polyder on r, get absf, absfp */
    acb_abs( absf, polyval, prec);
    acb_abs( absfp, pderval, prec);

#ifndef PW_SILENT    
    if (verbose) {
        printf("_EA_get_inclusion_disc_and_status: prec: %ld, input root: ", prec);
        acb_printd(root, 10); printf("\n");
    }
#endif
    
    /* set radius to 2|f/fp| */
    arb_div(radius, absf, absfp, prec);
    arb_mul_2exp_si(radius, radius, 1);
    
    acb_add_error_arb(r, radius);
    acb_set(r3, r);
    mag_mul_ui( arb_radref( acb_realref(r3) ), arb_radref( acb_realref(r3) ), 3);
    mag_mul_ui( arb_radref( acb_imagref(r3) ), arb_radref( acb_imagref(r3) ), 3);
   
#ifndef PW_SILENT
    if (verbose) {
        printf(" --- radius = 2|f/fp|: ");
        arb_printd(radius, 10); printf("\n");
        printf(" --- r : ");
        acb_printd(r, 10); printf("\n");
    }
#endif
    /* check if r contains zero */
    if (acb_contains_zero(r)) {
        /* set radius to d|f/fp| */
        arb_div(radius, absf, absfp, prec);
        arb_mul_ui( radius, radius, lenPoly-1, prec );
        if (arb_is_finite(radius)){
            res = PW_INCLUDING;
        } else {
            arb_indeterminate( radius );
        }
    } else {
#ifndef PW_SILENT
        if (verbose) {
            printf(" --- does not contain 0 \n");
        }
#endif
        /* evaluate poly'' on D(r, 4*radius), get absfpp */
        mag_mul_2exp_si( arb_radref( acb_realref(r) ), arb_radref( acb_realref(r) ), 2);
        mag_mul_2exp_si( arb_radref( acb_imagref(r) ), arb_radref( acb_imagref(r) ), 2);
#ifdef PW_EA_PROFILE
        clock_t start_ev = clock();
#endif
        _acb_poly_evaluate2_rectangular(f, fpp, polyder, lenPoly-1, r, lprec);
#ifdef PW_EA_PROFILE
        clicks_in_inc_evals += (clock() - start_ev);
        nb_inc_evals++;
#endif
        acb_abs( absfpp, fpp, prec );
        /* set K to |fp/fp| */
        arb_div(K, absfpp, absfp, prec);
        
        arb_one(one);
        /* set left to 5rK */
        arb_mul(left, radius, K, prec);
        arb_mul_ui(left, left, 5, prec);
        
        if (arb_le(left,one)) {
#ifndef PW_SILENT
            if (verbose) {
                printf(" --- is isolating \n");
            }
#endif
            res=PW_ISOLATING;
            /* check if 3 contains zero */
            if ( acb_contains_zero(r3)==0 ) {
                /* try to prove that the disc D(r, radius) is a natural isolator */
                /* i.e. D(x,3*radius) contains a unique root                     */
                mag_mul_ui( arb_radref( acb_realref(r) ), arb_radref( acb_realref(r) ), 3);
                mag_mul_ui( arb_radref( acb_imagref(r) ), arb_radref( acb_imagref(r) ), 3);
#ifdef PW_EA_PROFILE
                start_ev = clock();
#endif
                _acb_poly_evaluate2_rectangular(f, fpp, polyder, lenPoly-1, r, prec);
#ifdef PW_EA_PROFILE
                clicks_in_inc_evals += (clock() - start_ev);
                nb_inc_evals++;
#endif
                acb_abs( absfpp, fpp, prec );
                /* set K to |fp/fp| */
                arb_div(K, absfpp, absfp, prec);
                /* set left to 5*3*r*K */
                arb_mul(left, radius, K, prec);
                arb_mul_ui(left, left, 15, prec);
                if ( arb_le(left,one) ) {
                    res=PW_NATURAL_I;
#ifndef PW_SILENT
                    if (verbose) {
                        printf(" --- is natural \n");
                    }
#endif
                }
            }
        } else {
            /* set radius to d|f/fp| */
            arb_div(radius, absf, absfp, prec);
            arb_mul_ui( radius, radius, lenPoly-1, prec );
            if (arb_is_finite(radius)){
                res = PW_INCLUDING;
            } else {
                arb_indeterminate( radius );
            }
#ifndef PW_SILENT
            if (verbose) {
                printf(" --- radius: ");
                arb_printd(radius, 10);
                printf("\n");
            }
#endif
        }
        
    }
    
    arb_clear(absfpp);
    arb_clear(absfp);
    arb_clear(absf);
    arb_clear(K);
    arb_clear(left);
    arb_clear(one);
    acb_clear(fpp);
    acb_clear(f);
    acb_clear(r3);
    acb_clear(r);
    
#ifndef PW_SILENT
    if (verbose) {
        printf(" --- res: %d\n", res);
    }
#endif
    
    return res;
}

void _EA_actualize_wild_tamed_from_ccs( int *tamed, const conCom_list_t ccs,
                                        slong *nbWild, slong *nbTamed,
                                        int goal, const domain_t dom ){
    
    conCom_ptr ccur;
    
    *nbWild = 0;
    *nbTamed= 0;
    
    conCom_list_iterator it = _conCom_list_begin(ccs);
    while ( it!=_conCom_list_end() ) {
        ccur = _conCom_list_elmt(it);
        slong mul = _slong_list_get_size( conCom_component_discsref( ccur ) );
            
        if ( conCom_naturalref(ccur) && conCom_sizeOKref(ccur) 
          && ( (PW_GOAL_MUST_ISOLATE(goal)==0) || (mul==1) ) ) {
            /* actualize tamed,  nbTamed and nbwild */
           slong_list_iterator itind = _slong_list_begin(conCom_component_discsref( ccur ));
           while ( itind != _slong_list_end() ) {
               slong indexInTamed = _slong_list_elmt( itind );
               PW_EA_SET_TAMED( tamed[indexInTamed] );
               itind = _slong_list_next(itind);
           }
           (*nbTamed) += mul;
        } else if ( (!domain_is_C(dom)) && 
                    (conCom_naturalref(ccur))&&(!domain_acb_intersect_domain(dom, conCom_containing_boxref(ccur))) ) {
            /* actualize tamed,  nbTamed and nbwild */
            slong_list_iterator itind = _slong_list_begin(conCom_component_discsref( ccur ));
           while ( itind != _slong_list_end() ) {
               slong indexInTamed = _slong_list_elmt( itind );
               PW_EA_SET_TAMED( tamed[indexInTamed] );
               itind = _slong_list_next(itind);
           }
           (*nbTamed) += mul;
        } else { /* do not modify tamed but just actualize nbTamed and nbwild */
           slong_list_iterator itind = _slong_list_begin(conCom_component_discsref( ccur ));
           while ( itind != _slong_list_end() ) {
               slong indexInTamed = _slong_list_elmt( itind );
               
               if (PW_EA_IS_TAMED(tamed[indexInTamed]))
                  (*nbTamed)++;
               else
                   (*nbWild)++;
               itind = _slong_list_next(itind);
           }
        }
        
        it = _conCom_list_next(it);
        
    }
    
}

void _pw_EA_evaluate_on_middle_wild_roots( acb_ptr polyvals, acb_ptr pdervals, 
                                           acb_ptr roots, int * tamed, slong lenRoots, 
                                           pw_polynomial_t cache, slong prec ) {
#ifdef PW_EA_PROFILE
    clock_t start_eval = clock();
#endif
    acb_poly_srcptr poly = pw_polynomial_cacheANDgetApproximation( cache, prec );
    acb_t mid;
    acb_init(mid);
    for (slong i=0; i<lenRoots; i++) {
        if (PW_EA_IS_TAMED(tamed[i]))
            continue;
        
        acb_get_mid(mid, roots+i);
        _acb_poly_evaluate2_rectangular(polyvals+i, pdervals+i, poly->coeffs, poly->length, mid, prec);
//         _acb_poly_evaluate2_horner(polyvals+i, pdervals+i, poly->coeffs, poly->length, mid, prec);
        
#ifdef PW_EA_PROFILE
        nb_evals++;
#endif
    }
    acb_clear(mid);
#ifdef PW_EA_PROFILE
    clicks_in_evaluations += (clock() - start_eval);
#endif
}

void _pw_EA_newton_on_wild_natural_roots( acb_ptr roots, int * tamed, 
                                          acb_ptr polyvals, acb_ptr pdervals,
                                          slong lenRoots, slong prec ) {

#ifdef PW_EA_PROFILE
    clock_t start_newt = clock();
#endif
    
    acb_t nroot, temp;
    acb_init(nroot);
    acb_init(temp);
    
//     printf("_pw_EA_newton_on_wild_natural_roots: \n");
    
    for (slong i=0; i<lenRoots; i++) {
        if (PW_EA_IS_TAMED(tamed[i]))
            continue;
        
        if ( PW_EA_IS_NATURAL_I(tamed[i]) ) {
            
//             printf("---%ld-th root: ", i); acb_printd(roots+i, 10); printf("\n");
            /* do nroot = mid( mid(root) - p(mid(root))/p'(mid(root)) ) */
            acb_get_mid(temp, polyvals+i);
            acb_get_mid(nroot, pdervals+i);
            acb_div( temp, temp, nroot, prec );
            acb_get_mid(nroot, roots+i);
            acb_sub(nroot, nroot, temp, prec);
            acb_get_mid(roots+i, nroot);
#ifdef PW_EA_PROFILE
            nb_newtEA++;
#endif
        }
    }
    
    acb_clear(temp);
    acb_clear(nroot);
    
#ifdef PW_EA_PROFILE
    clicks_in_newtoEA += (clock() - start_newt);
#endif
}

/* not used */
// void _EA_one_iteration_on_wild_roots_precs( acb_ptr roots, int * tamed, 
//                                             acb_ptr polyvals, acb_ptr pdervals,
//                                             slong lenRoots,
//                                             slong * precs ) {
// 
// #ifdef PW_EA_PROFILE
//     clock_t start_ea = clock();
// #endif
//         
//     slong lprec = 0;
//     acb_ptr sumOfInvDists = _acb_vec_init( lenRoots );
//     acb_t temp, pval, pderval;
//     acb_init(temp);
//     acb_init(pval);
//     acb_init(pderval);
//     /* compute sums of inv dists */
// #ifdef PW_EA_PROFILE
//     clock_t start_di = clock();
// #endif
//     for (slong i=0; i<lenRoots; i++) {
//         if ( PW_EA_IS_TAMED(tamed[i]) )
//             continue;
//         
//         if (PW_EA_IS_NATURAL_I( tamed[i] ))
//             continue;
//         
// //         lprec = precs[i];
//         
//         acb_get_mid( roots + i, roots + i );
//         acb_zero( sumOfInvDists + i );
//         for (slong j=0; j<lenRoots; j++) {
//             if ( !(j==i) ) {
//                 lprec = PWPOLY_MAX( precs[i], precs[j] );
//                 _EA_acb_sub_mid( temp, roots+i, roots+j, lprec );
//                 _EA_acb_inv_mid( temp, temp, lprec );
//                 _EA_acb_add_mid( sumOfInvDists + i, sumOfInvDists + i, temp, lprec );
//             }
//         }
// #ifdef PW_EA_PROFILE
//         nb_EA_dists++;
// #endif
//     }
// #ifdef PW_EA_PROFILE
//     clicks_in_EA_dists += (clock() - start_di);
// #endif 
//         
//     /* apply EA iteration to wild roots */
//     for (slong i=0; i<lenRoots; i++) {
//         if ( PW_EA_IS_TAMED(tamed[i]) )
//             continue;
//         
//         if (PW_EA_IS_NATURAL_I( tamed[i] ))
//             continue;
//         
//         lprec = precs[i];
//         
//         acb_get_mid( pval, polyvals+i );
//         acb_get_mid( pderval, pdervals+i );
//         acb_div( temp, pval, pderval, lprec );
//         acb_get_mid( temp, temp );
//         acb_mul( pval, temp, sumOfInvDists + i, lprec );
//         acb_get_mid( pval, pval );
//         acb_one( pderval );
//         acb_sub( pval, pderval, pval, lprec );
//         acb_get_mid( pval, pval );
//         acb_div( temp, temp, pval, lprec );
//         acb_get_mid( temp, temp );
//         acb_sub( roots+i, roots+i, temp, lprec );
//         acb_get_mid( roots+i, roots+i );
//     }
//     acb_clear(pderval);
//     acb_clear(pval);
//     acb_clear(temp);
//     _acb_vec_clear(sumOfInvDists, lenRoots);
//     
// #ifdef PW_EA_PROFILE
//         clicks_in_EA_itts += (clock() - start_ea);
// #endif 
//         
// }

void _EA_one_iteration_on_wild_roots_precs2( acb_ptr roots, int * tamed, 
                                             acb_ptr polyvals, acb_ptr pdervals,
                                             slong lenRoots, acb_mat_t invDists,
                                             slong * precs ) {

#ifdef PW_EA_PROFILE
    clock_t start_ea = clock();
#endif
        
    slong lprec = 0;
    acb_ptr sumOfInvDists = _acb_vec_init( lenRoots );
//     acb_mat_t invDists;
//     acb_mat_init(invDists, lenRoots, lenRoots);
    
    acb_t temp, pval, pderval;
    acb_init(temp);
    acb_init(pval);
    acb_init(pderval);
    /* compute sums of inv dists */
#ifdef PW_EA_PROFILE
    clock_t start_di = clock();
#endif
    for (slong i=0; i<lenRoots; i++) {
        if ( PW_EA_IS_TAMED(tamed[i]) )
            continue;
        
        if (PW_EA_IS_NATURAL_I( tamed[i] ))
            continue;
        
//         lprec = precs[i];
        
        acb_get_mid( roots + i, roots + i );
        acb_zero( sumOfInvDists + i );
//         for (slong j=0; j<lenRoots; j++) {
//             if ( !(j==i) ) {
//                 lprec = PWPOLY_MAX( precs[i], precs[j] );
//                 _EA_acb_sub_mid( temp, roots+i, roots+j, lprec );
//                 _EA_acb_inv_mid( temp, temp, lprec );
//                 _EA_acb_add_mid( sumOfInvDists + i, sumOfInvDists + i, temp, lprec );
//             }
//         }
        for (slong j=0; j<i; j++) {
            lprec = PWPOLY_MAX( precs[i], precs[j] );
            if (( PW_EA_IS_TAMED(tamed[j]) )||(PW_EA_IS_NATURAL_I( tamed[j] ))){
                /* 1/(roots+j - roots+i) has not been computed: compute it and store it in invDists[j,i] */
                _EA_acb_sub_mid( acb_mat_entry(invDists, j, i), roots+j, roots+i, lprec );
                _EA_acb_inv_mid( acb_mat_entry(invDists, j, i), acb_mat_entry(invDists, j, i), lprec );
            } 
            _EA_acb_sub_mid( sumOfInvDists + i, sumOfInvDists + i, acb_mat_entry(invDists, j, i), lprec );
        }
        for (slong j=i+1; j<lenRoots; j++) {
            /* 1/(roots+i - roots+j) has not been computed: compute it and store it in invDists[i,j] */
            lprec = PWPOLY_MAX( precs[i], precs[j] );
            _EA_acb_sub_mid( acb_mat_entry(invDists, i, j), roots+i, roots+j, lprec );
            _EA_acb_inv_mid( acb_mat_entry(invDists, i, j), acb_mat_entry(invDists, i, j), lprec );
            _EA_acb_add_mid( sumOfInvDists + i, sumOfInvDists + i, acb_mat_entry(invDists, i, j), lprec );
        }
#ifdef PW_EA_PROFILE
        nb_EA_dists++;
#endif
    }
#ifdef PW_EA_PROFILE
    clicks_in_EA_dists += (clock() - start_di);
#endif 
        
    /* apply EA iteration to wild roots */
    for (slong i=0; i<lenRoots; i++) {
        if ( PW_EA_IS_TAMED(tamed[i]) )
            continue;
        
        if (PW_EA_IS_NATURAL_I( tamed[i] ))
            continue;
        
        lprec = precs[i];
        
        acb_get_mid( pval, polyvals+i );
        acb_get_mid( pderval, pdervals+i );
        acb_div( temp, pval, pderval, lprec );
        acb_get_mid( temp, temp );
        acb_mul( pval, temp, sumOfInvDists + i, lprec );
        acb_get_mid( pval, pval );
        acb_one( pderval );
        acb_sub( pval, pderval, pval, lprec );
        acb_get_mid( pval, pval );
        acb_div( temp, temp, pval, lprec );
        acb_get_mid( temp, temp );
        acb_sub( roots+i, roots+i, temp, lprec );
        acb_get_mid( roots+i, roots+i );
    }
    acb_clear(pderval);
    acb_clear(pval);
    acb_clear(temp);
//     acb_mat_clear(invDists);
    _acb_vec_clear(sumOfInvDists, lenRoots);
    
#ifdef PW_EA_PROFILE
        clicks_in_EA_itts += (clock() - start_ea);
#endif 
        
}
    
slong _EA_inclusion_discs_precs( acb_ptr roots, int * tamed, int * ntamed, 
                                 acb_ptr polyvals, acb_ptr pdervals,
                                 slong lenRoots, 
                                 pw_polynomial_t cache,
                                 int goal, slong log2eps, slong * precs ) {
    
#ifdef PW_EA_PROFILE
    clock_t start_inc = clock();
#endif
        
    slong nbNtamed = 0;
    arb_t radius;
    arb_init(radius);
    
    for (slong i=0; i<lenRoots; i++) {
        if ( PW_EA_IS_TAMED(tamed[i]) ) {
            ntamed[i] = 0;
            continue;
        }
        
        int inclusion = _EA_get_inclusion_disc_and_status( radius, roots+i, polyvals+i, pdervals+i, cache, precs[i] PW_VERBOSE_CALL(0) );
        acb_get_mid( roots+i, roots+i );
        acb_add_error_arb( roots+i, radius);
        int sizeOK = (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                    ||pwpoly_width_less_eps( roots+i, log2eps, PW_GOAL_PREC_RELATIVE(goal) );
        
        if ( (inclusion == PW_NATURAL_I) && sizeOK ) {
            PW_EA_SET_TAMED( ntamed[i] );
            nbNtamed++;
        } else {
            ntamed[i] = 0;
            if (inclusion == PW_NATURAL_I) {
                PW_EA_SET_INCLUSION(ntamed[i], PW_NATURAL_I);
                PW_EA_SET_NEWTO_SUCCEE(ntamed[i]);
            }
        }
        
    }
    
    arb_clear(radius);
    
#ifdef PW_EA_PROFILE
    clicks_in_inclEA += (clock() - start_inc);
#endif
    
    return nbNtamed;
}



void _EA_weierstrass_discs_precs( acb_ptr roots, int * tamed, 
                                  acb_ptr polyvals,
                                  slong lenRoots,
                                  pw_polynomial_t cache,
                                  slong * precs, slong maxprec ) {

#ifdef PW_EA_PROFILE
    clock_t start_weier = clock();
#endif
    
    arb_ptr dists    = _arb_vec_init(lenRoots);
    arb_ptr radii    = _arb_vec_init(lenRoots);
    acb_t dist;
    acb_init(dist);
    arb_t temp, absleading;
    arb_init(temp);
    arb_init(absleading);
    acb_poly_srcptr pol = pw_polynomial_cacheANDgetApproximation(cache, maxprec);
    acb_ptr poly = pol->coeffs;
    slong lenPoly = pol->length;
    acb_abs( absleading, poly + (lenPoly-1), maxprec);
    
    slong lprec = 0;
    
    for (slong i=0; i<lenRoots; i++) {
        if ( PW_EA_IS_TAMED(tamed[i]) )
            continue;
        
        acb_get_mid(roots+i, roots+i);
    }
    
    for (slong i=0; i<lenRoots; i++) {
        if ( PW_EA_IS_TAMED(tamed[i]) )
            continue;
        
//         slong lprec = precs[i];
#ifdef PW_EA_PROFILE
        clock_t start_dists = clock();
#endif
        arb_set(dists+i, absleading);
        for (slong j=0; j<lenRoots; j++) {
            if (j!=i) {
                lprec = PWPOLY_MAX( precs[i], precs[j] );
                acb_sub(dist, roots+i, roots+j, lprec);
                acb_abs(temp, dist, lprec);
                arb_mul(dists+i, dists+i, temp, lprec);
            }
        }
        lprec = precs[i];
#ifdef PW_EA_PROFILE
        clicks_in_weier_dists += (clock() - start_dists);
        nb_weier_dists++;
#endif
        acb_abs(radii+i, polyvals+i, lprec);
        arb_div(radii+i, radii+i, dists+i, lprec); 
        arb_mul_ui(radii+i, radii+i, (lenPoly-1), lprec);
    }
    
    for (slong i=0; i<lenRoots; i++) {
        if ( PW_EA_IS_TAMED(tamed[i]) )
            continue;
        
        acb_add_error_arb( roots+i, radii + i);
    }
    
    arb_clear(absleading);
    arb_clear(temp);
    acb_clear(dist);
    _arb_vec_clear(radii, lenRoots);
    _arb_vec_clear(dists, lenRoots);
    
#ifdef PW_EA_PROFILE
    clicks_in_weierstrass += (clock() - start_weier);
#endif
}

void _pw_EA_evaluate_on_middle_wild_roots_precs( acb_ptr polyvals, acb_ptr pdervals, 
                                                 acb_ptr roots, int * tamed, slong lenRoots, 
                                                 pw_polynomial_t cache, slong *precs ) {
#ifdef PW_EA_PROFILE
    clock_t start_eval = clock();
#endif    
    
    acb_t mid;
    acb_init(mid);
    for (slong i=0; i<lenRoots; i++) {
        if (PW_EA_IS_TAMED(tamed[i]))
            continue;
        
        slong lprec = precs[i];
        acb_poly_srcptr poly = pw_polynomial_cacheANDgetApproximation( cache, lprec );
        acb_get_mid(mid, roots+i);
        _acb_poly_evaluate2_rectangular(polyvals+i, pdervals+i, poly->coeffs, poly->length, mid, lprec);
//         _acb_poly_evaluate2_horner(polyvals+i, pdervals+i, poly->coeffs, poly->length, mid, lprec);
        
#ifdef PW_EA_PROFILE
        nb_evals++;
#endif
    }
    acb_clear(mid);
#ifdef PW_EA_PROFILE
    clicks_in_evaluations += (clock() - start_eval);
#endif
}


void _pw_EA_evaluate_on_middle_root( acb_t polyval, acb_t pderval, 
                                     acb_t root, 
                                     pw_polynomial_t cache, slong prec ) {
#ifdef PW_EA_PROFILE
    clock_t start_eval = clock();
#endif    
    
    acb_t mid;
    acb_init(mid);
    acb_get_mid(mid, root);
    acb_poly_srcptr poly = pw_polynomial_cacheANDgetApproximation( cache, prec );
    _acb_poly_evaluate2_rectangular(polyval, pderval, poly->coeffs, poly->length, mid, prec);

#ifdef PW_EA_PROFILE
    nb_evals++;
#endif 
    
    acb_clear(mid);
#ifdef PW_EA_PROFILE
    clicks_in_evaluations += (clock() - start_eval);
#endif
}

void _pw_EA_evaluate_on_middle_wild_roots_N_inDomain_precs( acb_ptr polyvals, acb_ptr pdervals, 
                                                            acb_ptr roots, int * tamed, slong lenRoots, 
                                                            const domain_t dom, slong goalNbRoots,
                                                            pw_polynomial_t cache, slong *precs ) {
#ifdef PW_EA_PROFILE
    clock_t start_eval = clock();
#endif    
    
    acb_t mid;
    acb_init(mid);
    slong nbEvs = 0;
    for (slong i=0; (i<lenRoots) && (nbEvs<goalNbRoots); i++) {
        if (PW_EA_IS_TAMED(tamed[i]))
            continue;
        
        if (PW_EA_NEWTO_SUCCEE(tamed[i]) && domain_acb_intersect_domain( dom, roots+i ) ) {
            slong lprec = precs[i];
            acb_poly_srcptr poly = pw_polynomial_cacheANDgetApproximation( cache, lprec );
            acb_get_mid(mid, roots+i);
            _acb_poly_evaluate2_rectangular(polyvals+i, pdervals+i, poly->coeffs, poly->length, mid, lprec);
//         _acb_poly_evaluate2_horner(polyvals+i, pdervals+i, poly->coeffs, poly->length, mid, lprec);
#ifdef PW_EA_PROFILE
            nb_evals++;
#endif
            nbEvs++;
        }
        
    }
    acb_clear(mid);
#ifdef PW_EA_PROFILE
    clicks_in_evaluations += (clock() - start_eval);
#endif
}

void _pw_EA_newton_on_wild_natural_roots_precs( acb_ptr roots, int * tamed, 
                                                acb_ptr polyvals, acb_ptr pdervals,
                                                slong lenRoots, slong * precs ) {

#ifdef PW_EA_PROFILE
    clock_t start_newt = clock();
#endif
    
    acb_t nroot, temp;
    acb_init(nroot);
    acb_init(temp);
    
//     printf("_pw_EA_newton_on_wild_natural_roots: \n");
    
    for (slong i=0; i<lenRoots; i++) {
        if (PW_EA_IS_TAMED(tamed[i]))
            continue;
        
        if ( PW_EA_IS_NATURAL_I(tamed[i]) ) {
            
            slong lprec = precs[i];
            
//             printf("---%ld-th root: ", i); acb_printd(roots+i, 10); printf("\n");
            /* do nroot = mid( mid(root) - p(mid(root))/p'(mid(root)) ) */
            acb_get_mid(temp, polyvals+i);
            acb_get_mid(nroot, pdervals+i);
            acb_div( temp, temp, nroot, lprec );
            acb_get_mid(nroot, roots+i);
            acb_sub(nroot, nroot, temp, lprec);
            acb_get_mid(roots+i, nroot);
#ifdef PW_EA_PROFILE
            nb_newtEA++;
#endif
        }
    }
    
    acb_clear(temp);
    acb_clear(nroot);
    
#ifdef PW_EA_PROFILE
    clicks_in_newtoEA += (clock() - start_newt);
#endif
}

void _pw_EA_newton_on_wild_natural_roots_N_inDomain_precs( acb_ptr roots, int * tamed, 
                                                           acb_ptr polyvals, acb_ptr pdervals,
                                                           const domain_t dom, slong goalNbRoots,
                                                           slong lenRoots, slong * precs ) {

#ifdef PW_EA_PROFILE
    clock_t start_newt = clock();
#endif
    
    acb_t nroot, temp;
    acb_init(nroot);
    acb_init(temp);
    
//     printf("_pw_EA_newton_on_wild_natural_roots: \n");
    slong nbNew = 0;
    for (slong i=0; (i<lenRoots)&&(nbNew<goalNbRoots); i++) {
        if (PW_EA_IS_TAMED(tamed[i]))
            continue;
        
        if ( PW_EA_NEWTO_SUCCEE(tamed[i]) && domain_acb_intersect_domain( dom, roots+i ) ) {
            
            slong lprec = precs[i];
            
//             printf("---%ld-th root: ", i); acb_printd(roots+i, 10); printf("\n");
            /* do nroot = mid( mid(root) - p(mid(root))/p'(mid(root)) ) */
            acb_get_mid(temp, polyvals+i);
            acb_get_mid(nroot, pdervals+i);
            acb_div( temp, temp, nroot, lprec );
            acb_get_mid(nroot, roots+i);
            acb_sub(nroot, nroot, temp, lprec);
            acb_get_mid(roots+i, nroot);
#ifdef PW_EA_PROFILE
            nb_newtEA++;
#endif
            nbNew++;
        }
    }
    
    acb_clear(temp);
    acb_clear(nroot);
    
#ifdef PW_EA_PROFILE
    clicks_in_newtoEA += (clock() - start_newt);
#endif
}

void _pw_EA_zoom_on_clusters( acb_ptr roots, int * tamed,
                              acb_ptr polyvals, acb_ptr pdervals, 
                              pw_polynomial_t poly,
                              conCom_list_t ccs, 
                              slong *nbWild, slong *nbTamed,
                              slong log2eps, int goal, const domain_t dom,
                              slong *maxprec, slong *precs PW_VERBOSE_ARGU(verbose)  ) {
    
#ifdef PW_EA_PROFILE
    clock_t start_zoom = clock();
#endif
#ifndef PW_SILENT
    int level = 1;
#endif
    conCom_list_iterator it = _conCom_list_begin(ccs);
    while (it!=_conCom_list_end()){
        conCom_ptr ccur = _conCom_list_elmt(it);
        if ((conCom_sepCritref(ccur))&&(PW_GOAL_MUST_ISOLATE(goal)||(conCom_sizeOKref(ccur)==0))) {
            
            if ( domain_is_C(dom) || domain_acb_intersect_domain(dom, conCom_containing_boxref(ccur)) ) { 
            
                slong mult = _slong_list_get_size( conCom_component_discsref(ccur) );
#ifndef PW_SILENT
                if (verbose >=level) {
                    printf("---Zoom on 12 separated cluster with %ld roots\n", mult);
                    _conCom_printd( _conCom_list_elmt(it), 10 );
                    printf("\n");
                }
#endif
                acb_t ncluster;
                acb_init(ncluster);
                acb_set(ncluster, conCom_containing_boxref(ccur));
                slong resprec = solver_CoCo_zoom_pw_polynomial( ncluster, mult, poly, log2eps, goal PW_VERBOSE_CALL(verbose) );
                
#ifdef PW_EA_PROFILE
                nb_zoom_clusts++;
#endif
            
                int sizeOk = (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                            ||pwpoly_width_less_eps( ncluster, log2eps, PW_GOAL_PREC_RELATIVE(goal) );
#ifndef PW_SILENT                            
                if (verbose >=level) {
                    printf("---after zoom: new containing box: ");
                    acb_printd(ncluster, 10);
                    printf("\n");
                    printf("---            size OK: %d\n", sizeOk);
                }
#endif                
                if ( (PW_GOAL_MUST_ISOLATE(goal)==0)&&sizeOk ) {
                    acb_set( conCom_containing_boxref(ccur), ncluster );
                    conCom_sizeOKref(ccur) = 1;
                    slong_list_iterator it2 = _slong_list_begin( conCom_component_discsref(ccur) );
                    while( it2 != _slong_list_end() ) {
                        slong index = _slong_list_elmt( it2 );
                        acb_set( roots+index, ncluster );
                        PW_EA_SET_TAMED(tamed[index]);
                        it2 = _slong_list_next(it2);
                    }
                    (*nbTamed) += mult;
                    (*nbWild) -= mult;
                    
                    *maxprec = PWPOLY_MAX( *maxprec, resprec);
                    
                } else if (acb_contains_interior( conCom_containing_boxref(ccur), ncluster ) ) {
                    /* generate new initial values from ncluster */
                    arb_t radClus;
                    arb_init(radClus);
                    acb_t rot;
                    acb_init(rot);
                    arb_get_rad_arb( acb_realref(rot), acb_realref(ncluster) );
                    arb_get_rad_arb( acb_imagref(rot), acb_imagref(ncluster) );
                    arb_max(radClus, acb_realref(rot), acb_imagref(rot), resprec );
                    acb_ptr unit_roots = _acb_vec_init( mult );
                    _acb_vec_unit_roots(unit_roots, mult, mult, 2*resprec);
                    acb_unit_root(rot, 10*mult, 2*resprec);
                    acb_get_mid(ncluster, ncluster);
                    slong_list_iterator it2 = _slong_list_begin( conCom_component_discsref(ccur) );
                    for (slong i=0; i<mult; i++){
                        acb_mul( unit_roots + i, unit_roots + i, rot, 2*resprec );
                        acb_mul_arb( unit_roots + i, unit_roots + i, radClus, 2*resprec );
                        acb_add( unit_roots + i, ncluster, unit_roots + i, 2*resprec );
#ifndef PW_SILENT
                        if (verbose >=level) {
                            printf("new initial value: "); acb_printd( unit_roots + i, 10);
                            printf("\n");
                        }
#endif
                        slong index = _slong_list_elmt( it2 );
                        acb_set( roots+index, unit_roots + i );
                        precs[index] = resprec;
                        _pw_EA_evaluate_on_middle_root( polyvals+index, pdervals+index, roots+index, poly, precs[index] );
                        it2 = _slong_list_next(it2);
                    }
                    _acb_vec_clear(unit_roots, mult);
                    acb_clear(rot);
                    arb_clear(radClus);
                    *maxprec = PWPOLY_MAX( *maxprec, resprec);
                }
                
                acb_clear(ncluster);
            
            }
        }
        it = _conCom_list_next(it);
    }
#ifdef PW_EA_PROFILE
        clicks_in_zoom_clusts += (clock() - start_zoom);
#endif
}

void _pw_EA_refine_n_natural_isolators_in_domain ( acb_ptr roots, int * tamed, 
                                                    pw_polynomial_t poly,
                                                    acb_ptr polyvals, acb_ptr pdervals,
                                                    slong *nbWild, slong *nbTamed, slong *nbTamedInDomain,
                                                    int goal, slong log2eps, const domain_t dom, slong goalNbRoots,
                                                    slong *precs, slong *prec, slong nbMaxItts PW_VERBOSE_ARGU(verbose) ) {
    
#ifndef PW_SILENT    
    int level = 1;
#endif
    
    slong len = pw_polynomial_length(poly);
    slong nbItts = 0;
    slong nbIttsWithoutCheck = 5;
    
    arb_t radius;
    arb_init(radius);
    
    /* to control convergence */
    arb_t old_prod, prod, rad;
    arb_init(old_prod);
    arb_init(prod);
    arb_init(rad);
    arb_pos_inf(old_prod);
    
#ifndef PW_SILENT    
    if ( verbose >=level ) {
        printf("_pw_EA_refine_n_natural_isolators_in_domain: nbTamedInDomain: %ld, goalNbRoots: %ld\n",
               *nbTamedInDomain, goalNbRoots );
    }
#endif

    while ( ( nbItts < nbMaxItts )
            && ( ( (*nbTamed) < len-1 ) && (goalNbRoots>0) ) ) {
        
        /* apply newton iterations on the first goalNbRoots natural isolators in domain */
        _pw_EA_newton_on_wild_natural_roots_N_inDomain_precs( roots, tamed, polyvals, pdervals, dom, goalNbRoots,
                                                              len-1, precs );
        /* evaluate on the first goalNbRoots natural isolators in domain */
        _pw_EA_evaluate_on_middle_wild_roots_N_inDomain_precs( polyvals, pdervals, roots, tamed, len-1, 
                                                               dom, goalNbRoots, poly, precs );
        
        /* compute isolating discs every nbIttsWithoutCheck iterations */
        if ( (nbItts>0) && ( (nbItts%nbIttsWithoutCheck==0)||(nbItts==(nbMaxItts-1)) ) ) {
            slong oldGoalNbRoots = goalNbRoots;
#ifdef PW_EA_PROFILE
            clock_t start_inc = clock();
#endif
            slong nbInc = 0;
            for (slong i=0; (i<len-1)&&(nbInc<goalNbRoots); i++) {
                if ( PW_EA_IS_TAMED(tamed[i]) )
                    continue;
                
                if ( PW_EA_NEWTO_SUCCEE(tamed[i]) && domain_acb_intersect_domain( dom, roots+i ) ) {
                    
//                     int inclusion = _EA_get_inclusion_disc_and_status( radius, roots+i, polyvals+i, pdervals+i, poly, precs[i] PW_VERBOSE_CALL(0) );
                    _EA_get_inclusion_disc_and_status( radius, roots+i, polyvals+i, pdervals+i, poly, precs[i] PW_VERBOSE_CALL(0) );
                    acb_get_mid( roots+i, roots+i );
                    acb_add_error_arb( roots+i, radius);
                    int sizeOK = pwpoly_width_less_eps( roots+i, log2eps, PW_GOAL_PREC_RELATIVE(goal));
                    /* inclusion should always be PW_NATURAL_I */
                    if ( sizeOK ) {
                        PW_EA_SET_TAMED( tamed[i] );
                        (*nbTamed)++;
                        (*nbWild)--;
                        goalNbRoots--;
                        (*nbTamedInDomain)++;
                    }
                }
            }
#ifdef PW_EA_PROFILE
            clicks_in_inclEA += (clock() - start_inc);
#endif
#ifndef PW_SILENT    
            if ( verbose >=level ) {
                printf("_pw_EA_refine_n_natural_isolators_in_domain: nbItts: %ld, nbTamedInDomain: %ld, goalNbRoots: %ld\n",
                       nbItts, *nbTamedInDomain, goalNbRoots );
            }
#endif

            /* see if one must increase precision */
            if ( (nbItts < nbMaxItts-1)&&(goalNbRoots>0) ) {
                /* compute the product of radii of roots still to be refined */
                arb_one(prod);
                slong nbProd = 0;
                for (slong i=0; (i<len-1)&&(nbProd<goalNbRoots); i++) {
                    if (PW_EA_IS_TAMED(tamed[i]))
                        continue;
                    if ( PW_EA_NEWTO_SUCCEE(tamed[i]) && domain_acb_intersect_domain( dom, roots+i ) ) {
                        arb_get_rad_arb(rad, acb_realref(roots+i) );
                        arb_mul(prod, prod, rad, precs[i]);
                    }
                }
#ifndef PW_SILENT
                if (verbose >=level+1 ) {
                    printf("---old prod of rads: ");
                    arb_printd(old_prod, 5);
                    printf("\n---new prod of rads: ");
                    arb_printd(prod, 5);
                    printf("\n");
                }
#endif
                arb_mul_2exp_si( rad, prod, 1 );
            }
            if ( (nbItts < nbMaxItts-1)&&(goalNbRoots>0)&&(oldGoalNbRoots==goalNbRoots)&&(!arb_lt(rad, old_prod)) ) {
                (*prec) *= 2;
                for (slong i=0; i<len-1; i++) {
                    if (PW_EA_NEWTO_SUCCEE(tamed[i]) && domain_acb_intersect_domain( dom, roots+i ) && (precs[i]<*prec))
                        precs[i] = *prec;
                }
#ifndef PW_SILENT
                if (verbose >=level ) {
                    printf("---increase prec, new prec: %ld\n", *prec );
                }
#endif
                /* evaluate on the first goalNbRoots natural isolators in domain */
                _pw_EA_evaluate_on_middle_wild_roots_N_inDomain_precs( polyvals, pdervals, roots, tamed, len-1, 
                                                                           dom, goalNbRoots, poly, precs );
            }
            /* actualize old_prod */
            arb_set(old_prod, prod);
            
        }
        nbItts++;
    }
    arb_clear(rad);
    arb_clear(prod);
    arb_clear(old_prod);
    arb_clear(radius);
}


/* Pre-conditions  : assume poly has degree d with non-zero leading and trailing coeffs         */
/*                   roots and mults have enough rooms for d acb, slong                         */
/*                   for 0 <= i <= lenNatural, letting B(ci,ri) = roots + i,                    */
/*                        the disc D(ci,ri) is a natural isolator for a root of poly            */
/*                        and mults[i]=1                                                        */
/*                   nbMaxItts>=1                                                               */
/* Post-conditions : for 0 <= i < *nbclusts, let B(ci,ri) = roots + i,                          */
/*                   Let res be the returned value.                                             */
/*                   if 0<=res<nbMaxItts then the solving process succeeded:                    */
/*                   (i)   the D(ci,ri)'s are pairwise disjoint                                 */
/*                   (ii)  any root of poly is in a D(ci, ri) and SUM_{0<=1<*nbclusts}mults[i] = degree */
/*                   (iii) any D(ci,ri) contains mults[i] roots                                 */
/*                   (iv)  any D(ci,ri) is a natural cluster of mults[i] roots                  */
/*                   (v)   if goal is isolate, *nbclusts = d and mults[i] = 1 for all i         */
/*                   (vi)  if goal is approximate, B(ci,ri) has                                 */
/*                             relative (or absolute) precision at least log2eps                */
/*                   if res >= nbMaxItts, then (i), (ii), (iii), (iv) hold but not (v) and (vi) */
/*                   if res < 0           then (i), (ii), (iii) hold but not (iv), (v) and (vi) */
slong pw_EA_solve_pw_polynomial ( acb_ptr roots, slong * mults, slong * nbclusts, 
                                  slong lenNatural,
                                  pw_polynomial_t poly,
                                  int goal, slong log2eps, const domain_t dom, slong goalNbRoots,
                                  slong *prec, slong nbMaxItts, slong nbIttsBeforeIncreasePrec PW_VERBOSE_ARGU(verbose)  PW_FILE_ARGU(f)) {
#ifndef PW_SILENT    
    int level = 1;
#endif    
    *nbclusts = 0;
    slong len = pw_polynomial_length(poly);
    int realCoeffs = pw_polynomial_is_real(poly);
    slong nbWild = 0;
    slong nbTamed = 0, nbTamedInDomain = 0, nbNewtConvInDomain = 0;
    slong nbItts = 0;
    slong nbIttsWithoutCheck = 5;
    
    /* cap goalNbRoots */
    if (goalNbRoots<0)
        goalNbRoots = len-1;
    else
        goalNbRoots = PWPOLY_MIN( goalNbRoots, len-1 );
    
    acb_t mid;
    acb_init(mid);
    
    /* to control convergence */
    arb_t old_prod, prod, rad;
    arb_init(old_prod);
    arb_init(prod);
    arb_init(rad);
    arb_pos_inf(old_prod);
    /* structure for union find */
    conCom_list_t ccs;
    _conCom_list_init(ccs);
    
    slong nprec = *prec;
    slong * precs = (slong *) pwpoly_malloc ( (len-1)*sizeof(slong) );
    for (slong i=0; i<len-1; i++)
        precs[i] = *prec;
    
    int * tamed = (int *) pwpoly_malloc ( (len-1)*sizeof(int) );
#ifndef PW_SILENT    
    if ( verbose >=level ) {
        printf("pw_EA_solve_pw_polynomial: degree: %ld, %ld already isolated, input prec: %ld \n", 
               len-1, lenNatural, *prec);
        printf("                           goal: "); pwroots_print_goal(goal); 
        printf(" log2eps: %ld, domain: ", log2eps); domain_print_short(dom);
        printf(" goalNbRoots: %ld\n", goalNbRoots );
    }
#endif
    /* get initial values */
#ifdef PW_EA_PROFILE
    clock_t start_initial = clock();
#endif    
    _pw_EA_initial_values_acb_poly ( roots, tamed, &nbWild, &nbTamed, lenNatural, poly, log2eps, goal, dom, *prec PW_VERBOSE_CALL(verbose)  ); 
#ifdef PW_EA_PROFILE
    clicks_in_initial += (clock() - start_initial);
#endif
    
#ifndef PW_NO_INTERFACE
    if (f!=NULL)
        pw_points_twocolors_gnuplot( f, roots, lenNatural, len-1 );
#endif
#ifndef PW_SILENT    
    if ( verbose >=level ) {
        printf("---after initialize: nbWild : %ld, nbTamed: %ld\n", nbWild, nbTamed);
        if ( verbose >=level+2 ) {
            for (slong i=0; i<len-1; i++) {
                printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
                        i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
                acb_printd(roots+(*nbclusts+i), 10); 
                printf("\n");
            }
            printf("\n");
        }
    }
#endif

    /* get the number of tamed roots in domain and the number of natural isolators in domain */
    for (slong i=0; i<len-1; i++) {
        
            if (PW_EA_IS_NATURAL_I(tamed[i]) && domain_acb_intersect_domain( dom, roots+i ) ) {
                if (PW_EA_IS_TAMED(tamed[i]))
                    nbTamedInDomain++;
                if (PW_EA_NEWTO_SUCCEE(tamed[i]))
                    nbNewtConvInDomain++;
            }
    }
#ifndef PW_SILENT    
    if ( verbose >=level ) {
        printf("pw_EA_solve_pw_polynomial: nbTamedInDomain: %ld, nbNewtConvInDomain: %ld\n", nbTamedInDomain, nbNewtConvInDomain );
    }
#endif

    acb_ptr polyvals = _acb_vec_init( len-1 );
    acb_ptr pdervals = _acb_vec_init( len-1 );
    acb_mat_t invDists;
    acb_mat_init(invDists, len-1, len-1);
    
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    if (nbTamedInDomain < goalNbRoots)
        /* evaluate poly and its derivative at wild roots */
        _pw_EA_evaluate_on_middle_wild_roots_precs( polyvals, pdervals, roots, tamed, len-1, poly, precs );
    
    /* apply iterations */
//     while ( (nbItts < nbMaxItts) && ( nbTamed < len-1 ) ) {
    while ( (nbItts < nbMaxItts) 
         && ( ( nbTamed < len-1 ) && (nbTamedInDomain < goalNbRoots) && (nbNewtConvInDomain < goalNbRoots) ) ) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif
        
        /* apply interval Newton on wild roots that are natural isolators */
        _pw_EA_newton_on_wild_natural_roots_precs( roots, tamed, polyvals, pdervals, len-1, precs );
        /* apply EA iterations */
//         _EA_one_iteration_on_wild_roots_precs( roots, tamed, polyvals, pdervals, len-1, precs );
        _EA_one_iteration_on_wild_roots_precs2( roots, tamed, polyvals, pdervals, len-1, invDists, precs );   
        /* evaluate poly and its derivative at wild roots */
        _pw_EA_evaluate_on_middle_wild_roots_precs( polyvals, pdervals, roots, tamed, len-1, poly, precs );

        /* compute isolating discs and Weierstrass discs every nbIttsWithoutCheck iterations */
#ifdef PW_CHECK_INTERRUPT
        if (!pwpoly_interrupt(0))
#endif
        if ( (nbItts>0) && ( (nbItts%nbIttsWithoutCheck==0)||(nbItts==(nbMaxItts-1)) ) ) {
#ifndef PW_SILENT            
            if (verbose >=level+1 ) {
                printf("---after %ld its: nbWild : %ld, nbTamed: %ld, prec: %ld\n", nbItts, nbWild, nbTamed, *prec);
                if (verbose >=level+2 ) {
                    for (slong i=0; i<len-1; i++) {
                        printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
                                i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
                        acb_printd(roots+i, 10); 
                        printf("\n");
                    }
                    printf("\n");
                }
            }
#endif
            
            slong oldLenWild = nbWild;
            /* compute isolating discs with f, f', f'' and get new natural/tamed roots */
            int *ntamed = (int *) pwpoly_malloc ( (len-1)*sizeof(int) );
#ifndef PW_SILENT
            slong nbNtamed = _EA_inclusion_discs_precs( roots, tamed, ntamed, polyvals, pdervals, 
                                                  len-1, poly, goal, log2eps, precs );   
#else
                             _EA_inclusion_discs_precs( roots, tamed, ntamed, polyvals, pdervals, 
                                                  len-1, poly, goal, log2eps, precs );
#endif
            /* check if any natural/tamed roots is disjoints from other natural/tamed roots and old natural/tamed roots */
            int ntamed_pairwise_disjoint = _EA_check_newtamed_pairwise_disjoint( roots, tamed, ntamed, len-1 );
#ifndef PW_SILENT            
            if (verbose >= level+1 ) {
                printf("---after %ld its and inclusion discs: nb new tamed: %ld, pairwise disjoint: %d\n", 
                       nbItts, nbNtamed, ntamed_pairwise_disjoint);
                if (verbose >=level+2 ) {
                    for (slong i=0; i<len-1; i++) {
                        printf("------%ld-th: tamed: %d, new tamed: %d ", 
                                i, PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_TAMED(ntamed[i])); 
                        acb_printd(roots+i, 10); 
                        printf("\n");
                    }
                    printf("\n");
                }
            }
#endif            
            /* if yes, set new tamed roots as tamed */
            if (ntamed_pairwise_disjoint) {
                for (slong i=0; i<len-1; i++) {
                    if (PW_EA_IS_TAMED(ntamed[i])||PW_EA_IS_NATURAL_I(ntamed[i]))
                        tamed[i] = ntamed[i];
                }
            }
            
            pwpoly_free(ntamed);
            
            /* compute weierstrass discs */
            _EA_weierstrass_discs_precs( roots, tamed, polyvals, len-1, poly, precs, nprec );
#ifndef PW_SILENT    
            if (verbose >=level+1 ) {
                printf("---after %ld its and weierstrass: nbWild : %ld, nbTamed: %ld\n", nbItts, nbWild, nbTamed);
                if (verbose >=level+2 ) {
                    for (slong i=0; i<len-1; i++) {
                        printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
                                i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
                        acb_printd(roots+(*nbclusts+i), 10); 
                        printf("\n");
                    }
                    printf("\n");
                }
            }
#endif            
            /* compute connected components */
            if (_conCom_list_get_size(ccs)<=1) {
                _conCom_list_clear(ccs);
                _conCom_list_compute( ccs, roots, len-1, nprec);
            } else {
                _conCom_list_actualize( ccs, roots, tamed, log2eps, goal, nprec);
            }
#ifndef PW_SILENT
            if (verbose >=level ) {
                printf("---%ld connected component(s)\n", _conCom_list_get_size(ccs) );
//                 /* get nb of cc intersecting the domain */
//                 slong nbInter=0;
//                 acb_t contBox;
//                 acb_init(contBox);
//                 conCom_list_iterator it = _conCom_list_begin(ccs);
//                 while (it != _conCom_list_end() ) {
//                     acb_set( contBox, conCom_containing_boxref(_conCom_list_elmt(it)) );
//                     if (domain_acb_intersect_domain( dom, contBox ))
//                         nbInter++;
//                     it = _conCom_list_next(it);
//                 }
//                 acb_clear(contBox);
//                 printf("---%ld connected component(s) intersect domain\n", nbInter );
                if (verbose >=level+2 ) {
                    conCom_list_iterator it;
                    it = _conCom_list_begin(ccs);
                    while (it != _conCom_list_end() ) {
                        _conCom_printd( _conCom_list_elmt(it), 10 );
                        printf("\n");
                        it = _conCom_list_next(it);
                    }
                    printf("\n");
                }
            }
#endif            
            /* for each CC, check if it has size < 2^log2eps                   */
            /*              check if it is a natural cluster, ie 3-separated   */
            /*              if it has >1 component disc, check if 12-separated */
            /*                           (required to apply zoom on component) */
            _conCom_list_check_natural_and_size( ccs, log2eps, goal);

            /* for each CC, if it is natural and has size OK, and if the goal is not isolate, */
            /*                                               set its component discs as tamed */
            /*              if it is natural and not in the domain,                           */
            /*                                               set its component discs as tamed */
            _EA_actualize_wild_tamed_from_ccs( tamed, ccs, &nbWild, &nbTamed, goal, dom );
#ifndef PW_SILENT
            if (verbose >=level+1 ) {
                printf("---after %ld its CCs: nbWild : %ld, nbTamed: %ld\n", nbItts, nbWild, nbTamed);
                if (verbose >=level+2 ) {
                    for (slong i=0; i<len-1; i++) {
                        printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
                                i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
                        acb_printd(roots+i, 10); 
                        printf("\n");
                    }
                    printf("\n");
                }
            }
#endif    
            /* zoom on cluster if necessary */
#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            if ( (nbItts < nbMaxItts-1)&&(nbTamed < len-1) ) {
                _pw_EA_zoom_on_clusters( roots, tamed, polyvals, pdervals, poly, ccs, 
                                         &nbWild, &nbTamed, log2eps, goal, dom, &nprec, precs PW_VERBOSE_CALL(verbose)  );
            }

#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            if ( (nbItts < nbMaxItts-1)&&(nbTamed < len-1) ) {
                /* compute the product of radii of wild roots */
                arb_one(prod);
                for (slong i=0; i<len-1; i++) {
                    if (PW_EA_IS_TAMED(tamed[i]))
                        continue;
                    arb_get_rad_arb(rad, acb_realref(roots+i) );
                    arb_mul(prod, prod, rad, nprec);
                }
#ifndef PW_SILENT
                if (verbose >=level+1 ) {
                    printf("---old prod of rads: ");
                    arb_printd(old_prod, 5);
                    printf("\n---new prod of rads: ");
                    arb_printd(prod, 5);
                    printf("\n");
                }
#endif
                arb_mul_2exp_si( rad, prod, 1 );
            }

#ifdef PW_CHECK_INTERRUPT
            if (!pwpoly_interrupt(0))
#endif
            if ( (nbItts >= nbIttsBeforeIncreasePrec)&&
                 (nbItts < nbMaxItts-1)&&(nbTamed < len-1)&&(oldLenWild==nbWild)&&(!arb_lt(rad, old_prod)) ) {
                    
                (*prec) *= 2;
                for (slong i=0; i<len-1; i++) {
                    if (precs[i]<*prec)
                        precs[i] = *prec;
                }
#ifndef PW_SILENT
                if (verbose >=level+1 ) {
                    printf("---increase prec, new prec: %ld\n", *prec );
                }
#endif
                /* evaluate poly and its derivative at wild roots */
                _pw_EA_evaluate_on_middle_wild_roots_precs( polyvals, pdervals, roots+(*nbclusts), tamed, len-1, poly, precs );
            }
            /* actualize old_prod */
            arb_set(old_prod, prod);
#ifndef PW_SILENT            
            if (verbose >=level+1 ) {
                printf("\n\n");
            }
#endif       
            /* get the number of tamed roots in domain */
            nbTamedInDomain=0;
            nbNewtConvInDomain=0;
            for (slong i=0; i<len-1; i++) {
                
                if (PW_EA_IS_NATURAL_I(tamed[i]) && domain_acb_intersect_domain( dom, roots+i ) ) {
                    if (PW_EA_IS_TAMED(tamed[i]))
                        nbTamedInDomain++;
                    if (PW_EA_NEWTO_SUCCEE(tamed[i]))
                        nbNewtConvInDomain++;
                }
            
            }
#ifndef PW_SILENT    
            if ( verbose >=level ) {
                printf("pw_EA_solve_pw_polynomial: nbItts: %ld, nbTamedInDomain: %ld, nbNewtConvInDomain: %ld\n", nbItts, nbTamedInDomain, nbNewtConvInDomain );
            }
#endif
        }
        

        if ( ( nbTamed < len-1 ) && (nbTamedInDomain < goalNbRoots) && (nbNewtConvInDomain < goalNbRoots) )
            nbItts++;
        
    }
    
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    if ( (nbItts<nbMaxItts) && (nbTamedInDomain < goalNbRoots) && (nbNewtConvInDomain >= goalNbRoots) ) {
        _pw_EA_refine_n_natural_isolators_in_domain ( roots, tamed, poly, polyvals, pdervals,
                                                      &nbWild, &nbTamed, &nbTamedInDomain,
                                                      goal, log2eps, dom, goalNbRoots,
                                                      precs, prec, nbMaxItts-nbItts PW_VERBOSE_CALL(verbose)  );
    }
    pwpoly_free(precs);
    acb_mat_clear(invDists);
    _acb_vec_clear( pdervals, len-1 );
//     _acb_vec_clear( polyvals, len-1 );
    /* save roots in polyvals */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    for (slong i=0; i<len-1; i++)
        acb_set( polyvals + i, roots + i );
    
    /* set the result from connected components */
    *nbclusts=0;
    slong nbroots = 0;
//     int allNatural = 1;
    conCom_list_iterator it = _conCom_list_begin(ccs);
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    while( (it!=_conCom_list_end())&&(nbroots<goalNbRoots) ) {
        conCom_ptr ccur = _conCom_list_elmt(it);
        /* check if it is tamed */
        int isTamed = conCom_sizeOKref(ccur)&&conCom_naturalref(ccur);
        /* check if it intersects domain */
        int inDomain = domain_acb_intersect_domain( dom, conCom_containing_boxref( ccur ) );
//         allNatural = allNatural&&((!inDomain)||conCom_naturalref(ccur));
        slong mult = _slong_list_get_size( conCom_component_discsref( ccur ) );
        isTamed = isTamed && ( (mult==1) || (PW_GOAL_MUST_ISOLATE(goal)==0) );
        
        if (isTamed && inDomain) { /* use the connected component as the root */
            acb_set( roots+(*nbclusts), conCom_containing_boxref( ccur ) );
            if ( realCoeffs && (mult==1) && ( arb_contains_zero( acb_imagref(roots+(*nbclusts) )) ) )
                arb_zero( acb_imagref(roots+ (*nbclusts)) );
            mults[ (*nbclusts) ] = mult;
            (*nbclusts) += 1;
            nbroots += mult;
        } else { /* get the tamed roots in the ccs */
            slong_list_iterator dit = _slong_list_begin( conCom_component_discsref(ccur) );
            while( (dit!=_slong_list_end())&&(nbroots<goalNbRoots) ) {
                slong ind = _slong_list_elmt(dit);
                
                if (PW_EA_IS_TAMED(tamed[ind]) && domain_acb_intersect_domain( dom, polyvals + ind )) {
                    acb_set( roots+(*nbclusts), polyvals + ind );
                    if ( realCoeffs && ( arb_contains_zero( acb_imagref(roots+(*nbclusts) )) ) )
                        arb_zero( acb_imagref(roots+ (*nbclusts)) );
                    mults[ (*nbclusts) ] = 1;
                    (*nbclusts) += 1;
                    nbroots += 1;
                }
                
                dit = _slong_list_next(dit);
            }
        }
        it = _conCom_list_next(it);
    }
    
    _acb_vec_clear( polyvals, len-1 );
    
    slong res = nbItts;
    /* (i), (ii), (iii) hold */
    /* if allNatural = 0, then necessarily nbItts = nbMaxItts (otherwise nbTamed would be len-1) */
//     if (allNatural==0)
//         res = -res;
    
    /* if nbItts=nbMaxItts, then all clusters are natural but not isolated or not approximated */
#ifndef PW_SILENT    
    if (verbose >=level ) {
        printf("after %ld EA its, res: %ld \n", nbItts, res); 
        printf("--- nb CC: %ld\n", _conCom_list_get_size(ccs) );
        printf("--- nb wild roots: %ld, nb tamed roots: %ld, nb tamed roots in domain: %ld\n", nbWild, nbTamed, nbTamedInDomain); 
        printf("--- prec: %ld\n", *prec);
    }
#endif    
    pwpoly_free(tamed);
    _conCom_list_clear(ccs);
    arb_clear(rad);
    arb_clear(prod);
    arb_clear(old_prod);
    acb_clear(mid);
    
    return res;
}

slong pw_EA_solve_2fmpq_polynomial ( acb_ptr roots, slong * mults, slong * nbclusts,
                                     const fmpq_poly_t pre, const fmpq_poly_t pim,
                                     int goal, slong log2eps, const domain_t dom, slong goalNbRoots, 
                                     slong *prec, slong nbMaxItts PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(f)){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq( poly, pre, pim );
    
    slong multOfZero = pw_polynomial_remove_root_zero(poly);
    
    if (multOfZero>0) {
        acb_zero( roots + 0 );
        mults[0] = multOfZero;
        multOfZero = 1;
    }
    slong res = pw_EA_solve_pw_polynomial ( roots+multOfZero, mults+multOfZero, nbclusts, 0, poly, goal, log2eps, dom, goalNbRoots, prec, nbMaxItts, 0 PW_VERBOSE_CALL(verbose) PW_FILE_CALL(f) );
    
    *nbclusts+=multOfZero;
    
    pw_polynomial_clear(poly);
    
    return res;
}

slong pw_EA_solve_exact_acb_polynomial   ( acb_ptr roots, slong * mults, slong * nbclusts,
                                           const acb_poly_t p,
                                           int goal, slong log2eps, const domain_t dom, slong goalNbRoots, 
                                           slong *prec, slong nbMaxItts PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(f)){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, p );
    
    slong multOfZero = pw_polynomial_remove_root_zero(poly);
    
    if (multOfZero>0) {
        acb_zero( roots + 0 );
        mults[0] = multOfZero;
        multOfZero = 1;
    }
    slong res = pw_EA_solve_pw_polynomial ( roots+multOfZero, mults+multOfZero, nbclusts, 0, poly, goal, log2eps, dom, goalNbRoots, prec, nbMaxItts, 0 PW_VERBOSE_CALL(verbose) PW_FILE_CALL(f) );
    
    *nbclusts+=multOfZero;
    
    pw_polynomial_clear(poly);
    
    return res;
}
/* Pre-conditions  : assume poly has degree d with non-zero leading and trailing coeffs         */
/*                   roots and mults have enough rooms for d acb, slong                         */
/*                   for 0 <= i <= lenNatural, letting B(ci,ri) = roots + i,                    */
/*                        the disc D(ci,ri) is a natural isolator for a root of poly            */
/*                        and mults[i]=1                                                        */
/*                   nbMaxItts>=1                                                               */
/* Post-conditions : for 0 <= i < *nbclusts, let B(ci,ri) = roots + i,                          */
/*                   Let res be the returned value.                                             */
/*                   if 0<=res<nbMaxItts then the solving process succeeded:                    */
/*                   (i)   the D(ci,ri)'s are pairwise disjoint                                 */
/*                   (ii)  any root of poly is in a D(ci, ri) and SUM_{0<=1<*nbclusts}mults[i] = degree */
/*                   (iii) any D(ci,ri) contains mults[i] roots                                 */
/*                   (iv)  any D(ci,ri) is a natural cluster of mults[i] roots                  */
/*                   (v)   if goal is isolate, *nbclusts = d and mults[i] = 1 for all i         */
/*                   (vi)  if goal is approximate, B(ci,ri) has                                 */
/*                             relative (or absolute) precision at least log2eps                */
/*                   if res >= nbMaxItts, then (i), (ii), (iii), (iv) hold but not (v) and (vi) */
/*                   if res < 0           then (i), (ii), (iii) hold but not (iv), (v) and (vi) */
// slong pw_EA_solve_pw_polynomial ( acb_ptr roots, slong * mults, slong * nbclusts, 
//                                   slong lenNatural,
//                                   pw_polynomial_t poly,
//                                   int goal, slong log2eps, const domain_t dom, slong *prec, 
//                                   slong nbMaxItts, slong nbIttsBeforeIncreasePrec PW_VERBOSE_ARGU(verbose), FILE * f) {
// #ifndef PW_SILENT    
//     int level = 2;
// #endif    
//     *nbclusts = 0;
//     slong len = pw_polynomial_length(poly);
//     int realCoeffs = pw_polynomial_is_real(poly);
//     slong nbWild = 0;
//     slong nbTamed = 0;
//     slong nbItts = 0;
//     slong nbIttsWithoutCheck = 5;
//     
//     acb_t mid;
//     acb_init(mid);
//     
//     /* to control convergence */
//     arb_t old_prod, prod, rad;
//     arb_init(old_prod);
//     arb_init(prod);
//     arb_init(rad);
//     arb_pos_inf(old_prod);
//     /* structure for union find */
//     conCom_list_t ccs;
//     _conCom_list_init(ccs);
//     
//     slong nprec = *prec;
//     
//     int * tamed = (int *) pwpoly_malloc ( (len-1)*sizeof(int) );
// #ifndef PW_SILENT    
//     if ( verbose >=level ) {
//         printf("pw_EA_solve_pw_polynomial: degree: %ld, %ld already isolated, input prec: %ld \n", 
//                len-1, lenNatural, *prec);
//     }
// #endif
//     /* get initial values */
// #ifdef PW_EA_PROFILE
//     clock_t start_initial = clock();
// #endif    
//     _pw_EA_initial_values_acb_poly ( roots, tamed, &nbWild, &nbTamed, lenNatural, poly, log2eps, goal, *prec PW_VERBOSE_CALL(verbose)  ); 
// #ifdef PW_EA_PROFILE
//     clicks_in_initial += (clock() - start_initial);
// #endif
//     
//     if (f!=NULL)
//         pw_points_twocolors_gnuplot( f, roots, lenNatural, len-1 );
// #ifndef PW_SILENT    
//     if ( verbose >=level ) {
//         printf("---after initialize: nbWild : %ld, nbTamed: %ld\n", nbWild, nbTamed);
//         if ( verbose >=level+1 ) {
//             for (slong i=0; i<len-1; i++) {
//                 printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
//                         i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
//                 acb_printd(roots+(*nbclusts+i), 10); 
//                 printf("\n");
//             }
//             printf("\n");
//         }
//     }
// #endif
//     
//     acb_ptr polyvals = _acb_vec_init( len-1 );
//     acb_ptr pdervals = _acb_vec_init( len-1 );
//     /* evaluate poly and its derivative at wild roots */
//     _pw_EA_evaluate_on_middle_wild_roots( polyvals, pdervals, roots, tamed, len-1, poly, *prec );
//     
//     /* apply iterations */
//     while ( (nbItts < nbMaxItts) && ( nbTamed < len-1 ) ) {
//         
//         /* apply interval Newton on wild roots that are natural isolators */
//         _pw_EA_newton_on_wild_natural_roots( roots, tamed, polyvals, pdervals, len-1, *prec );
//         /* apply EA iterations */
//         _EA_one_iteration_on_wild_roots( roots, tamed, polyvals, pdervals, len-1, *prec );   
//         /* evaluate poly and its derivative at wild roots */
//         _pw_EA_evaluate_on_middle_wild_roots( polyvals, pdervals, roots, tamed, len-1, poly, *prec );
//         
//         /* compute isolating discs and Weierstrass discs every nbIttsWithoutCheck iterations */
//         if ( (nbItts>0) && ( (nbItts%nbIttsWithoutCheck==0)||(nbItts==(nbMaxItts-1)) ) ) {
// #ifndef PW_SILENT            
//             if (verbose >=level+1 ) {
//                 printf("---after %ld its: nbWild : %ld, nbTamed: %ld, prec: %ld\n", nbItts, nbWild, nbTamed, *prec);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
//                                 i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
//                         acb_printd(roots+i, 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
// #endif
//             slong oldLenWild = nbWild;
//             /* compute isolating discs with f, f', f'' and get new natural/tamed roots */
//             int *ntamed = (int *) pwpoly_malloc ( (len-1)*sizeof(int) );
// #ifndef PW_SILENT
//             slong nbNtamed = _EA_inclusion_discs( roots, tamed, ntamed, polyvals, pdervals, 
//                                                   len-1, poly, goal, log2eps, *prec );   
// #else
//                              _EA_inclusion_discs( roots, tamed, ntamed, polyvals, pdervals, 
//                                                   len-1, poly, goal, log2eps, *prec );
// #endif
//             /* check if any natural/tamed roots is disjoints from other natural/tamed roots and old natural/tamed roots */
//             int ntamed_pairwise_disjoint = _EA_check_newtamed_pairwise_disjoint( roots, tamed, ntamed, len-1 );
// 
// #ifndef PW_SILENT            
//             if (verbose >= level+1 ) {
//                 printf("---after %ld its and inclusion discs: nb new tamed: %ld, pairwise disjoint: %d\n", 
//                        nbItts, nbNtamed, ntamed_pairwise_disjoint);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, new tamed: %d ", 
//                                 i, PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_TAMED(ntamed[i])); 
//                         acb_printd(roots+i, 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
// #endif
//             /* if yes, set new tamed roots as tamed */
//             if (ntamed_pairwise_disjoint) {
//                 for (slong i=0; i<len-1; i++) {
//                     if (PW_EA_IS_TAMED(ntamed[i])||PW_EA_IS_NATURAL_I(ntamed[i]))
//                         tamed[i] = ntamed[i];
//                 }
//             }
//             
//             pwpoly_free(ntamed);
//             
//             /* compute weierstrass discs */
//             _EA_weierstrass_discs( roots, tamed, polyvals, len-1, poly, *prec );
// #ifndef PW_SILENT    
//             if (verbose >=level+1 ) {
//                 printf("---after %ld its and weierstrass: nbWild : %ld, nbTamed: %ld\n", nbItts, nbWild, nbTamed);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
//                                 i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
//                         acb_printd(roots+(*nbclusts+i), 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
// #endif            
//             /* compute connected components */
//             if (_conCom_list_get_size(ccs)<=1) {
//                 _conCom_list_clear(ccs);
//                 _conCom_list_compute( ccs, roots, len-1, nprec);
//             } else {
//                 _conCom_list_actualize( ccs, roots, tamed, log2eps, goal, nprec);
//             }
// #ifndef PW_SILENT
//             if (verbose >=level+1 ) {
//                 printf("---%ld connected component(s)\n", _conCom_list_get_size(ccs) );
//                 if (verbose >=level+2 ) {
//                     conCom_list_iterator it;
//                     it = _conCom_list_begin(ccs);
//                     while (it != _conCom_list_end() ) {
//                         _conCom_printd( _conCom_list_elmt(it), 10 );
//                         printf("\n");
//                         it = _conCom_list_next(it);
//                     }
//                     printf("\n");
//                 }
//             }
// #endif            
//             _conCom_list_check_natural_and_size( ccs, log2eps, goal);
// 
//             _EA_actualize_wild_tamed_from_ccs( tamed, ccs, &nbWild, &nbTamed, goal, dom );
// #ifndef PW_SILENT
//             if (verbose >=level+1 ) {
//                 printf("---after %ld its CCs: nbWild : %ld, nbTamed: %ld\n", nbItts, nbWild, nbTamed);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
//                                 i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
//                         acb_printd(roots+i, 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
// #endif            
//             if ( (nbItts < nbMaxItts-1)&&(nbTamed < len-1) ) {
//                 /* compute the product of radii of wild roots */
//                 arb_one(prod);
//                 for (slong i=0; i<len-1; i++) {
//                     if (PW_EA_IS_TAMED(tamed[i]))
//                         continue;
//                     arb_get_rad_arb(rad, acb_realref(roots+i) );
//                     arb_mul(prod, prod, rad, *prec);
//                 }
// #ifndef PW_SILENT
//                 if (verbose >=level+1 ) {
//                     printf("---old prod of rads: ");
//                     arb_printd(old_prod, 5);
//                     printf("\n---new prod of rads: ");
//                     arb_printd(prod, 5);
//                     printf("\n");
//                 }
// #endif
//                 arb_mul_2exp_si( rad, prod, 1 );
//             }
//             
//             if ( (nbItts >= nbIttsBeforeIncreasePrec)&&
//                  (nbItts < nbMaxItts-1)&&(nbTamed < len-1)&&(oldLenWild==nbWild)&&(!arb_lt(rad, old_prod)) ) {
//                     
//                 (*prec) *= 2;
// #ifndef PW_SILENT
//                 if (verbose >=level+1 ) {
//                     printf("---increase prec, new prec: %ld\n", *prec );
//                 }
// #endif
//                 /* evaluate poly and its derivative at wild roots */
//                 _pw_EA_evaluate_on_middle_wild_roots( polyvals, pdervals, roots+(*nbclusts), tamed, len-1, poly, *prec );
//             }
//             /* actualize old_prod */
//             arb_set(old_prod, prod);
// #ifndef PW_SILENT            
//             if (verbose >=level+1 ) {
//                 printf("\n\n");
//             }
// #endif            
//         }
//         if ( nbTamed < len-1 )
//             nbItts++;
//         
//     }
//     _acb_vec_clear( pdervals, len-1 );
//     _acb_vec_clear( polyvals, len-1 );
//     
//     /* set the result from connected components */
//     *nbclusts=0;
//     int allNatural = 1;
//     conCom_list_iterator it = _conCom_list_begin(ccs);
//     while( it!=_conCom_list_end() ) {
//         conCom_ptr ccur = _conCom_list_elmt(it);
//         slong mult = _slong_list_get_size( conCom_component_discsref( ccur ) );
//         allNatural = allNatural&&conCom_naturalref(ccur);
//         acb_set( roots+(*nbclusts), conCom_containing_boxref( ccur ) );
//         if ( realCoeffs && (mult==1) && conCom_naturalref(ccur) && ( arb_contains_zero( acb_imagref(roots+(*nbclusts) )) ) )
//             arb_zero( acb_imagref(roots+ (*nbclusts)) );
//         mults[ (*nbclusts) ] = mult;
//         /* check if it is in the domain */
//         if (domain_acb_intersect_domain( dom, roots+(*nbclusts) ))
//             (*nbclusts) += 1;
//         it = _conCom_list_next(it);
//     }
//     
//     slong res = nbItts;
//     /* (i), (ii), (iii) hold */
//     /* if allNatural = 0, the necessarily nbItts = nbMaxItts (otherwise nbTamed would be len-1) */
//     if (allNatural==0)
//         res = -res;
//     
//     /* if nbItts=nbMaxItts, then all clusters are natural but not isolated of not approximated */
// #ifndef PW_SILENT    
//     if (verbose >=level ) {
//         printf("after %ld EA its, res: %ld \n", nbItts, res); 
//         printf("--- nb CC: %ld\n", _conCom_list_get_size(ccs) );
//         printf("--- nb wild roots: %ld, nb tamed roots: %ld\n", nbWild, nbTamed); 
//         printf("--- prec: %ld\n", *prec);
//     }
// #endif    
//     pwpoly_free(tamed);
//     _conCom_list_clear(ccs);
//     arb_clear(rad);
//     arb_clear(prod);
//     arb_clear(old_prod);
//     acb_clear(mid);
//     
//     return res;
// }

// slong pw_EA_solve_pw_polynomial_double ( acb_ptr roots, slong * mults, slong * nbclusts, 
//                                          pw_polynomial_t poly,
//                                          int goal, slong log2eps, const domain_t dom, slong *prec, 
//                                          slong nbMaxItts, slong nbIttsBeforeIncreasePrec PW_VERBOSE_ARGU(verbose), FILE * f) {
// #ifndef PW_SILENT    
//     int level = 1;
// #endif    
//     *nbclusts = 0;
//     slong len = pw_polynomial_length(poly);
//     int realCoeffs = pw_polynomial_is_real(poly);
//     slong nbWild = 0;
//     slong nbTamed = 0;
//     slong nbItts = 0;
//     slong nbIttsWithoutCheck = 5;
//     
//     acb_t mid;
//     acb_init(mid);
//     
//     /* to control convergence */
//     arb_t old_prod, prod, rad;
//     arb_init(old_prod);
//     arb_init(prod);
//     arb_init(rad);
//     arb_pos_inf(old_prod);
//     /* structure for union find */
//     conCom_list_t ccs;
//     _conCom_list_init(ccs);
//     
//     slong nprec = *prec;
//     slong * precs = (slong *) pwpoly_malloc ( (len-1)*sizeof(slong) );
//     for (slong i=0; i<len-1; i++)
//         precs[i] = *prec;
//     
//     int * tamed = (int *) pwpoly_malloc ( (len-1)*sizeof(int) );
// #ifndef PW_SILENT    
//     if ( verbose >=level ) {
//         printf("pw_EA_solve_pw_polynomial: degree: %ld, input prec: %ld \n", 
//                len-1, *prec);
//     }
// #endif
// 
//     slong nbfinite=0;
//     /* first use earoots for doubles */
//     int res1 = pw_polynomial_compute_double_approximation_balanced( poly );
//     if (res1==1) {
// #ifndef PW_SILENT
//         if (verbose>=level) {
//             printf("pw_EA_solve_pw_polynomial_double: input poly fits in double\n");
//         }
// #endif
//         double * Pmidds = (double *) pwpoly_malloc (2*len*sizeof(double));
//         double * Pabers  = (double *) pwpoly_malloc (len*sizeof(double));
//         for (slong i=0; i<len; i++ ) {
//             Pmidds[2*i] = pw_polynomial_Dcoeffref(poly)[2*i];
//             Pmidds[2*i+1] = pw_polynomial_Dcoeffref(poly)[2*i+1];
// //             Pabers[i] = pw_polynomial_Dabersref(poly)[i];
//             Pabers[i] = 0.;
//         }
//         double * Rmidds = (double *) pwpoly_malloc ( 2*(len-1)*sizeof(double) );
//         double * Rabers = (double *) pwpoly_malloc (   (len-1)*sizeof(double) );
// //         double max_error = ldexp(1, log2eps);
//         double max_error = ldexp(1, -53);
//         int maxits = 100;
//         int verb = 0;
//         int res_earoots = ea_roots(Pmidds, Pabers, (int) len, maxits, max_error, verb, Rmidds, Rabers);
//         
// // #ifndef PW_SILENT
// //         if (verbose>=level) {
// //             printf("pw_EA_solve_pw_polynomial_double: res_earoots: %d\n", res_earoots);
// //             slong nbFinite = len-1;
// //             slong nbFiniteApp = len-1;
// //             for (slong i=0; (i<(len-1)); i++) {
// //                 if ( !(isfinite(Rmidds[2*i])&&isfinite(Rmidds[2*i+1])&&isfinite(Rabers[i])) )
// //                     nbFinite--;
// //                 if ( !(isfinite(Rmidds[2*i])&&isfinite(Rmidds[2*i+1])) )
// //                     nbFiniteApp--;
// //             }
// //             printf("pw_EA_solve_pw_polynomial_double: %ld/%ld roots are finite, %ld/%ld roots have finite app\n", nbFinite, len-1, nbFiniteApp, len-1);
// //         }
// // #endif
//         /* write the output points with finite middle in roots */
//         for (slong i=0; (i<(len-1)); i++) {
//             if ( isfinite(Rmidds[2*i])&&isfinite(Rmidds[2*i+1]) ) {
//                 _be_get_acb( roots + nbfinite, Rmidds[2*i], Rmidds[2*i+1], 0 );
//                 nbfinite++;
//             }
//         }
// #ifndef PW_SILENT
//         if (verbose>=level) {
//             printf("pw_EA_solve_pw_polynomial_double: res_earoots: %d, nb of roots with finite center: %ld\n", res_earoots, nbfinite);
//         }
// #endif
//         pwpoly_free( Rabers );
//         pwpoly_free( Rmidds );
//         pwpoly_free( Pabers );
//         pwpoly_free( Pmidds );
// #ifndef PW_SILENT    
//         
// #endif 
//         
//     } else {
// #ifndef PW_SILENT
//         if (verbose>=level) {
//             printf("pw_EA_solve_pw_polynomial_double: input poly does not fit in double\n");
//         }
// #endif   
//     }
//     
//     if (nbfinite<(len-1)) {
//         /* get initial values */
// #ifdef PW_EA_PROFILE
//         clock_t start_initial = clock();
// #endif    
//         _pw_EA_initial_values_acb_poly ( roots, tamed, &nbWild, &nbTamed, 0, poly, log2eps, goal, *prec PW_VERBOSE_CALL(verbose)  ); 
// #ifdef PW_EA_PROFILE
//         clicks_in_initial += (clock() - start_initial);
// #endif
//     } else {
//         for (slong i=0; (i<(len-1)); i++){
//             tamed[i]=0;
//             precs[i]=2*(*prec);
//         }
//         nbWild = nbfinite;
//         nbTamed = 0;
//         (*prec)*=2;
//     }
//     
//     if (f!=NULL)
//         pw_points_twocolors_gnuplot( f, roots, 0, len-1 );
// #ifndef PW_SILENT    
//     if ( verbose >=level ) {
//         printf("---after initialize: nbWild : %ld, nbTamed: %ld\n", nbWild, nbTamed);
//         if ( verbose >=level+2 ) {
//             for (slong i=0; i<len-1; i++) {
//                 printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
//                         i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
//                 acb_printd(roots+(*nbclusts+i), 10); 
//                 printf("\n");
//             }
//             printf("\n");
//         }
//     }
// #endif
//     
//     acb_ptr polyvals = _acb_vec_init( len-1 );
//     acb_ptr pdervals = _acb_vec_init( len-1 );
//     acb_mat_t invDists;
//     acb_mat_init(invDists, len-1, len-1);
//     /* evaluate poly and its derivative at wild roots */
//     _pw_EA_evaluate_on_middle_wild_roots_precs( polyvals, pdervals, roots, tamed, len-1, poly, precs );
// //     _pw_EA_evaluate_middle_on_middle_wild_roots_precs( polyvals, pdervals, roots, tamed, len-1, poly, precs );
//     
//     /* apply iterations */
//     while ( (nbItts < nbMaxItts) && ( nbTamed < len-1 ) ) {
//         
//         /* apply interval Newton on wild roots that are natural isolators */
//         _pw_EA_newton_on_wild_natural_roots_precs( roots, tamed, polyvals, pdervals, len-1, precs );
//         /* apply EA iterations */
// //         _EA_one_iteration_on_wild_roots_precs( roots, tamed, polyvals, pdervals, len-1, precs );
//         _EA_one_iteration_on_wild_roots_precs2( roots, tamed, polyvals, pdervals, len-1, invDists, precs );   
//         /* evaluate poly and its derivative at wild roots */
// //         if ( (nbItts>0) && ( (nbItts%nbIttsWithoutCheck==0)||(nbItts==(nbMaxItts-1)) ) ) {
//             _pw_EA_evaluate_on_middle_wild_roots_precs( polyvals, pdervals, roots, tamed, len-1, poly, precs );
// //         } else {
// //             _pw_EA_evaluate_middle_on_middle_wild_roots_precs( polyvals, pdervals, roots, tamed, len-1, poly, precs );
// //         }
//         
//         /* compute isolating discs and Weierstrass discs every nbIttsWithoutCheck iterations */
//         if ( (nbItts>0) && ( (nbItts%nbIttsWithoutCheck==0)||(nbItts==(nbMaxItts-1)) ) ) {
// #ifndef PW_SILENT            
//             if (verbose >=level+1 ) {
//                 printf("---after %ld its: nbWild : %ld, nbTamed: %ld, prec: %ld\n", nbItts, nbWild, nbTamed, *prec);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
//                                 i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
//                         acb_printd(roots+i, 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
// #endif
//             
//             slong oldLenWild = nbWild;
//             /* compute isolating discs with f, f', f'' and get new natural/tamed roots */
//             int *ntamed = (int *) pwpoly_malloc ( (len-1)*sizeof(int) );
// #ifndef PW_SILENT
//             slong nbNtamed = _EA_inclusion_discs_precs( roots, tamed, ntamed, polyvals, pdervals, 
//                                                   len-1, poly, goal, log2eps, precs );   
// #else
//                              _EA_inclusion_discs_precs( roots, tamed, ntamed, polyvals, pdervals, 
//                                                   len-1, poly, goal, log2eps, precs );
// #endif
//             /* check if any natural/tamed roots is disjoints from other natural/tamed roots and old natural/tamed roots */
//             int ntamed_pairwise_disjoint = _EA_check_newtamed_pairwise_disjoint( roots, tamed, ntamed, len-1 );
// #ifndef PW_SILENT            
//             if (verbose >= level+1 ) {
//                 printf("---after %ld its and inclusion discs: nb new tamed: %ld, pairwise disjoint: %d\n", 
//                        nbItts, nbNtamed, ntamed_pairwise_disjoint);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, new tamed: %d ", 
//                                 i, PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_TAMED(ntamed[i])); 
//                         acb_printd(roots+i, 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
// #endif            
//             /* if yes, set new tamed roots as tamed */
//             if (ntamed_pairwise_disjoint) {
//                 for (slong i=0; i<len-1; i++) {
//                     if (PW_EA_IS_TAMED(ntamed[i])||PW_EA_IS_NATURAL_I(ntamed[i]))
//                         tamed[i] = ntamed[i];
//                 }
//             }
//             
//             pwpoly_free(ntamed);
//             
//             /* compute weierstrass discs */
//             _EA_weierstrass_discs_precs( roots, tamed, polyvals, len-1, poly, precs, nprec );
// #ifndef PW_SILENT    
//             if (verbose >=level+1 ) {
//                 printf("---after %ld its and weierstrass: nbWild : %ld, nbTamed: %ld\n", nbItts, nbWild, nbTamed);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
//                                 i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
//                         acb_printd(roots+(*nbclusts+i), 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
// #endif            
//             /* compute connected components */
//             if (_conCom_list_get_size(ccs)<=1) {
//                 _conCom_list_clear(ccs);
//                 _conCom_list_compute( ccs, roots, len-1, nprec);
//             } else {
//                 _conCom_list_actualize( ccs, roots, tamed, log2eps, goal, nprec);
//             }
// #ifndef PW_SILENT
//             if (verbose >=level ) {
//                 printf("---%ld connected component(s)\n", _conCom_list_get_size(ccs) );
// //                 /* get nb of cc intersecting the domain */
// //                 slong nbInter=0;
// //                 acb_t contBox;
// //                 acb_init(contBox);
// //                 conCom_list_iterator it = _conCom_list_begin(ccs);
// //                 while (it != _conCom_list_end() ) {
// //                     acb_set( contBox, conCom_containing_boxref(_conCom_list_elmt(it)) );
// //                     if (domain_acb_intersect_domain( dom, contBox ))
// //                         nbInter++;
// //                     it = _conCom_list_next(it);
// //                 }
// //                 acb_clear(contBox);
// //                 printf("---%ld connected component(s) intersect domain\n", nbInter );
//                 if (verbose >=level+2 ) {
//                     conCom_list_iterator it;
//                     it = _conCom_list_begin(ccs);
//                     while (it != _conCom_list_end() ) {
//                         _conCom_printd( _conCom_list_elmt(it), 10 );
//                         printf("\n");
//                         it = _conCom_list_next(it);
//                     }
//                     printf("\n");
//                 }
//             }
// #endif            
//             /* for each CC, check if it has size < 2^log2eps                   */
//             /*              check if it is a natural cluster, ie 3-separated   */
//             /*              if it has >1 component disc, check if 12-separated */
//             /*                           (required to apply zoom on component) */
//             _conCom_list_check_natural_and_size( ccs, log2eps, goal);
// 
//             /* for each CC, if it is natural and has size OK, and if the goal is not isolate, */
//             /*                                               set its component discs as tamed */
//             /*              if it is natural and not in the domain,                           */
//             /*                                               set its component discs as tamed */
//             _EA_actualize_wild_tamed_from_ccs( tamed, ccs, &nbWild, &nbTamed, goal, dom );
// #ifndef PW_SILENT
//             if (verbose >=level+1 ) {
//                 printf("---after %ld its CCs: nbWild : %ld, nbTamed: %ld\n", nbItts, nbWild, nbTamed);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d ", 
//                                 i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i])); 
//                         acb_printd(roots+i, 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
// #endif    
//             /* zoom on cluster if necessary */
//             if ( (nbItts < nbMaxItts-1)&&(nbTamed < len-1) ) {
//                 _pw_EA_zoom_on_clusters( roots, tamed, polyvals, pdervals, poly, ccs, 
//                                          &nbWild, &nbTamed, log2eps, goal, dom, &nprec, precs PW_VERBOSE_CALL(verbose)  );
//             }
//             
//             if ( (nbItts < nbMaxItts-1)&&(nbTamed < len-1) ) {
//                 /* compute the product of radii of wild roots */
//                 arb_one(prod);
//                 for (slong i=0; i<len-1; i++) {
//                     if (PW_EA_IS_TAMED(tamed[i]))
//                         continue;
//                     arb_get_rad_arb(rad, acb_realref(roots+i) );
//                     arb_mul(prod, prod, rad, nprec);
//                 }
// #ifndef PW_SILENT
//                 if (verbose >=level+1 ) {
//                     printf("---old prod of rads: ");
//                     arb_printd(old_prod, 5);
//                     printf("\n---new prod of rads: ");
//                     arb_printd(prod, 5);
//                     printf("\n");
//                 }
// #endif
//                 arb_mul_2exp_si( rad, prod, 1 );
//             }
//             
//             if ( (nbItts >= nbIttsBeforeIncreasePrec)&&
//                  (nbItts < nbMaxItts-1)&&(nbTamed < len-1)&&(oldLenWild==nbWild)&&(!arb_lt(rad, old_prod)) ) {
//                     
//                 (*prec) *= 2;
//                 for (slong i=0; i<len-1; i++) {
//                     if (precs[i]<*prec)
//                         precs[i] = *prec;
//                 }
// #ifndef PW_SILENT
//                 if (verbose >=level+1 ) {
//                     printf("---increase prec, new prec: %ld\n", *prec );
//                 }
// #endif
//                 /* evaluate poly and its derivative at wild roots */
//                 _pw_EA_evaluate_on_middle_wild_roots_precs( polyvals, pdervals, roots+(*nbclusts), tamed, len-1, poly, precs );
//             }
//             /* actualize old_prod */
//             arb_set(old_prod, prod);
// #ifndef PW_SILENT            
//             if (verbose >=level+1 ) {
//                 printf("\n\n");
//             }
// #endif            
//         }
//         if ( nbTamed < len-1 )
//             nbItts++;
//         
//     }
//     pwpoly_free(precs);
//     acb_mat_clear(invDists);
//     _acb_vec_clear( pdervals, len-1 );
//     _acb_vec_clear( polyvals, len-1 );
//     
//     /* set the result from connected components */
//     *nbclusts=0;
//     int allNatural = 1;
//     conCom_list_iterator it = _conCom_list_begin(ccs);
//     while( it!=_conCom_list_end() ) {
//         conCom_ptr ccur = _conCom_list_elmt(it);
//         slong mult = _slong_list_get_size( conCom_component_discsref( ccur ) );
//         allNatural = allNatural&&conCom_naturalref(ccur);
//         acb_set( roots+(*nbclusts), conCom_containing_boxref( ccur ) );
//         if ( realCoeffs && (mult==1) && conCom_naturalref(ccur) && ( arb_contains_zero( acb_imagref(roots+(*nbclusts) )) ) )
//             arb_zero( acb_imagref(roots+ (*nbclusts)) );
//         mults[ (*nbclusts) ] = mult;
//         /* check if it is in the domain */
//         if (domain_acb_intersect_domain( dom, roots+(*nbclusts) ))
//             (*nbclusts) += 1;
//         it = _conCom_list_next(it);
//     }
//     
//     slong res = nbItts;
//     /* (i), (ii), (iii) hold */
//     /* if allNatural = 0, the necessarily nbItts = nbMaxItts (otherwise nbTamed would be len-1) */
//     if (allNatural==0)
//         res = -res;
//     
//     /* if nbItts=nbMaxItts, then all clusters are natural but not isolated of not approximated */
// #ifndef PW_SILENT    
//     if (verbose >=level ) {
//         printf("after %ld EA its, res: %ld \n", nbItts, res); 
//         printf("--- nb CC: %ld\n", _conCom_list_get_size(ccs) );
//         printf("--- nb wild roots: %ld, nb tamed roots: %ld\n", nbWild, nbTamed); 
//         printf("--- prec: %ld\n", *prec);
//     }
// #endif    
//     pwpoly_free(tamed);
//     _conCom_list_clear(ccs);
//     arb_clear(rad);
//     arb_clear(prod);
//     arb_clear(old_prod);
//     acb_clear(mid);
//     
//     return res;
// }

// /* Pre-conditions  : assume poly has degree d with non-zero leading and trailing coeffs         */
// /*                   roots and mults have enough rooms for d acb, slong                         */
// /*                   for 0 <= i <= lenNatural, letting B(ci,ri) = roots + i,                    */
// /*                        the disc D(ci,ri) is a natural isolator for a root of poly            */
// /*                        and mults[i]=1                                                        */
// /*                   nbMaxItts>=1                                                               */
// /* Post-conditions : for 0 <= i < *nbclusts, let B(ci,ri) = roots + i,                          */
// /*                   Let res be the returned value.                                             */
// /*                   if 0<=res<nbMaxItts then the solving process succeeded:                    */
// /*                   (i)   the D(ci,ri)'s are pairwise disjoint                                 */
// /*                   (ii)  any root of poly is in a D(ci, ri) and SUM_{0<=1<*nbclusts}mults[i] = degree */
// /*                   (iii) any D(ci,ri) contains mults[i] roots                                 */
// /*                   (iv)  any D(ci,ri) is a natural cluster of mults[i] roots                  */
// /*                   (v)   if goal is isolate, *nbclusts = d and mults[i] = 1 for all i         */
// /*                   (vi)  if goal is approximate, B(ci,ri) has                                 */
// /*                             relative (or absolute) precision at least log2eps                */
// /*                   if res >= nbMaxItts, then (i), (ii), (iii), (iv) hold but not (v) and (vi) */
// /*                   if res < 0           then (i), (ii), (iii) hold but not (iv), (v) and (vi) */
// slong pw_EA_solve_pw_polynomial_precs ( acb_ptr roots, slong * mults, slong * nbclusts, 
//                                         slong lenNatural,
//                                         pw_polynomial_t poly,
//                                         int goal, slong log2eps, slong *prec, 
//                                         slong nbMaxItts, slong nbIttsBeforeIncreasePrec PW_VERBOSE_ARGU(verbose), FILE * f) {
//     
//     int level = 2;
//     
//     *nbclusts = 0;
// //     slong nbroots = 0;
//     slong len = pw_polynomial_length(poly);
//     int realCoeffs = pw_polynomial_is_real(poly);
//     slong nbWild = 0;
//     slong nbTamed = 0;
//     slong nbItts = 0;
//     slong nbIttsWithoutCheck = 5;
//     slong nbIttsWithoutIncrease = 5;
//     
// //     acb_poly_t polyder;
// //     acb_poly_init(polyder);
//     
// //     slong nbIttsBeforeIncreasePrec = 0;
// //     slong prec2=*prec;
// //     while( prec2>=PWPOLY_DEFAULT_PREC ) {
// //         nbIttsBeforeIncreasePrec += 2*nbIttsWithoutCheck;
// //         prec2 = prec2/2;
// //     }
// //     slong nbIttsBeforeIncreasePrec = 0;
//     
//     acb_t mid;
//     acb_init(mid);
//     
//     /* to control convergence */
//     arb_t old_prod, prod, rad, rad2;
//     arb_init(old_prod);
//     arb_init(prod);
//     arb_init(rad);
//     arb_init(rad2);
//     arb_pos_inf(old_prod);
//     /* structure for union find */
//     conCom_list_t ccs;
//     _conCom_list_init(ccs);
//     
// //     int last_increased = 0;
//     
//     int * tamed = (int *) pwpoly_malloc ( (len-1)*sizeof(int) );
//     
// //     acb_poly_srcptr polyacb = pw_polynomial_getApproximation( poly, *prec );
// //     acb_poly_derivative(polyder, polyacb, *prec );
//     
//     /* places to save last good roots and ccs */
// //     acb_ptr roots_save = _acb_vec_init( len-1 );
// //     int *   tamed_save = (int *) pwpoly_malloc ( (len-1)*sizeof(int) );
// //     conCom_list_t ccs_save;
// //     _conCom_list_init(ccs_save);
// //     slong nbWild_save, nbTamed_save;
// 
//     mag_ptr minRads = _mag_vec_init( len -1 );
//     for (slong i=0; i< len-1; i++)
//         mag_inf(minRads+i);
//     slong *precs = (slong *) pwpoly_malloc ( (len-1)*sizeof(slong) );
//     for (slong i=0; i< len-1; i++)
//         precs[i] = *prec;
//     slong minprec = *prec;
// //     slong maxprec = *prec;
//     if ( verbose >=level ) {
//         printf("pw_EA_solve_pw_polynomial: degree: %ld, %ld already isolated, input prec: %ld \n", 
//                len-1, lenNatural, *prec);
//     }
//     /* get initial values */
// #ifdef PW_EA_PROFILE
//     clock_t start_initial = clock();
// #endif    
//     _pw_EA_initial_values_acb_poly ( roots, tamed, &nbWild, &nbTamed, lenNatural, poly, log2eps, goal, *prec PW_VERBOSE_CALL(verbose)  ); 
// #ifdef PW_EA_PROFILE
//     clicks_in_initial += (clock() - start_initial);
// #endif
//     
//     if (f!=NULL)
//         pw_points_twocolors_gnuplot( f, roots, lenNatural, len-1 );
//     
//     if ( verbose >=level ) {
//         printf("---after initialize: nbWild : %ld, nbTamed: %ld\n", nbWild, nbTamed);
//         if ( verbose >=level+1 ) {
//             for (slong i=0; i<len-1; i++) {
//                 printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d, prec: %ld ", 
//                         i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i]), precs[i]); 
//                 acb_printd(roots+(*nbclusts+i), 10); 
//                 printf("\n");
//             }
//             printf("\n");
//         }
//     }
//     
//     acb_ptr polyvals = _acb_vec_init( len-1 );
//     acb_ptr pdervals = _acb_vec_init( len-1 );
//     /* evaluate poly and its derivative at wild roots */
//     _pw_EA_evaluate_on_middle_wild_roots_precs( polyvals, pdervals, roots, tamed, len-1, poly, precs );
//     
//     /* apply iterations */
//     while ( (nbItts < nbMaxItts) && ( nbTamed < len-1 ) ) {
//         
//         /* apply interval Newton on wild roots that are natural isolators */
//         _pw_EA_newton_on_wild_natural_roots_precs( roots, tamed, polyvals, pdervals, len-1, precs );
//         /* apply EA iterations */
//         _EA_one_iteration_on_wild_roots_precs( roots, tamed, polyvals, pdervals, len-1, goal, precs );   
//         /* evaluate poly and its derivative at wild roots */
//         _pw_EA_evaluate_on_middle_wild_roots_precs( polyvals, pdervals, roots, tamed, len-1, poly, precs );
//         
//         /* compute Weierstrass discs every nbIttsWithoutCheck iterations */
//         if ( (nbItts>0) && ( (nbItts%nbIttsWithoutCheck==0)||(nbItts==(nbMaxItts-1)) ) ) {
//             
//             if (verbose >=level+1 ) {
//                 printf("---after %ld its: nbWild : %ld, nbTamed: %ld, maxprec: %ld\n", nbItts, nbWild, nbTamed, *prec);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d, prec: %ld ", 
//                                 i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i]), precs[i]); 
//                         acb_printd(roots+i, 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
//             
//             slong oldLenWild = nbWild;
//         
//             /* compute isolating discs with f, f', f'' and get new natural/tamed roots */
//             int *ntamed = (int *) pwpoly_malloc ( (len-1)*sizeof(int) );
//             
//             slong nbNtamed = _EA_inclusion_discs_precs( roots, tamed, ntamed, polyvals, pdervals, len-1, poly,
//                                                         goal, log2eps, precs);   
//             
//             /* check any natural/tamed roots is disjoints from other natural/tamed roots and old natural/tamed roots */
//             int ntamed_pairwise_disjoint = _EA_check_newtamed_pairwise_disjoint( roots, tamed, ntamed, len-1 );
//             
//             if (verbose >= level+1 ) {
//                 printf("---after %ld its and inclusion discs: nb new tamed: %ld, pairwise disjoint: %d\n", 
//                        nbItts, nbNtamed, ntamed_pairwise_disjoint);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, new tamed: %d ", 
//                                 i, PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_TAMED(ntamed[i])); 
//                         acb_printd(roots+i, 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
//             
//             /* if yes, set new tamed roots as tamed */
//             if (ntamed_pairwise_disjoint) {
//                 for (slong i=0; i<len-1; i++) {
//                     if (PW_EA_IS_TAMED(ntamed[i])||PW_EA_IS_NATURAL_I(ntamed[i]))
//                         tamed[i] = ntamed[i];
//                 }
//             }
//             
//             pwpoly_free(ntamed);
//             
//             /* compute weierstrass discs */
//             _EA_weierstrass_discs_precs( roots, tamed, polyvals, len-1, poly, precs, *prec );
//     
//             if (verbose >=level+1 ) {
//                 printf("---after %ld its and weierstrass: nbWild : %ld, nbTamed: %ld, maxprec: %ld\n", 
//                         nbItts, nbWild, nbTamed, *prec);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d, prec: %ld ", 
//                                 i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i]), precs[i]); 
//                         acb_printd(roots+(*nbclusts+i), 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
//             
//             /* compute connected components */
//             if (_conCom_list_get_size(ccs)<=1) {
//                 _conCom_list_clear(ccs);
//                 _conCom_list_compute( ccs, roots, len-1, *prec);
//             } else {
//                 _conCom_list_actualize( ccs, roots, tamed, 
//                                         log2eps, goal, *prec);
//             }
// 
//             if (verbose >=level+1 ) {
//                 printf("---%ld connected component(s)\n", _conCom_list_get_size(ccs) );
//                 if (verbose >=level+2 ) {
//                     conCom_list_iterator it;
//                     it = _conCom_list_begin(ccs);
//                     while (it != _conCom_list_end() ) {
//                         _conCom_printd( _conCom_list_elmt(it), 10 );
//                         printf("\n");
//                         it = _conCom_list_next(it);
//                     }
//                     printf("\n");
//                 }
//             }
//             
//             _conCom_list_check_natural_and_size( ccs, log2eps, goal);
// 
//             _EA_actualize_wild_tamed_from_ccs( tamed, ccs, &nbWild, &nbTamed, goal );
// 
//             if (verbose >=level+1 ) {
//                 printf("---after %ld its CCs: nbWild : %ld, nbTamed: %ld, maxprec: %ld\n", 
//                         nbItts, nbWild, nbTamed, *prec);
//                 if (verbose >=level+2 ) {
//                     for (slong i=0; i<len-1; i++) {
//                         printf("------%ld-th: tamed: %d, %d, nat.: %d, app.: %d, prec: %ld ", 
//                                 i, tamed[i], PW_EA_IS_TAMED(tamed[i]), PW_EA_IS_NATURAL_I(tamed[i]), PW_EA_IS_APPROXIMD(tamed[i]), precs[i]); 
//                         acb_printd(roots+i, 10); 
//                         printf("\n");
//                     }
//                     printf("\n");
//                 }
//             }
//             
// //             slong nbDecreased = 0;
// //             slong nbIncreased = 0;
//             
//             if ( (nbItts%nbIttsWithoutIncrease==0)&&(nbItts < nbMaxItts-1)&&(nbTamed < len-1)&&(oldLenWild==nbWild) ) {
// //                 /* compute the product of radii of wild roots */
// //                 arb_one(prod);
// //                 for (slong i=0; i<len-1; i++) {
// //                     if (PW_EA_IS_TAMED(tamed[i]))
// //                         continue;
// //                     arb_get_rad_arb(rad, acb_realref(roots+i) );
// //                     arb_mul(prod, prod, rad, *prec);
// //                 }
// //                 if (verbose >=level+1 ) {
// //                     printf("---old prod of rads: ");
// //                     arb_printd(old_prod, 5);
// //                     printf("\n---new prod of rads: ");
// //                     arb_printd(prod, 5);
// //                     printf("\n");
// //                 }
// //                 arb_mul_2exp_si( rad, prod, 1 );
//                 /* actualize minprec and maxprec */
// //                 maxprec = LONG_MIN;
//                 minprec = LONG_MAX;
//                 for (slong i=0; i<len-1; i++) {
//                     if (PW_EA_IS_TAMED(tamed[i]))
//                         continue;
// //                     maxprec = PWPOLY_MAX( maxprec, precs[i] );
//                     minprec = PWPOLY_MIN( minprec, precs[i] );
//                 }
//                 if (verbose >= level) {
//                     printf("---minprec: %ld, maxprec: %ld \n", minprec, *prec);
//                 }
//                 mag_t radd;
//                 mag_init(radd);
//                 if (minprec < *prec) {
//                     int increaseAll = 0;
//                     for (slong i=0; (i<len-1)&&(increaseAll==0); i++) {
//                         if (PW_EA_IS_TAMED(tamed[i]))
//                             continue;
//                         
//                         mag_mul_2exp_si(radd, minRads+i, 0);
//                         if ( mag_cmp( arb_radref( acb_realref( roots+i ) ), radd )<0 )
//                             increaseAll = 1;
//                     }
//                     if (increaseAll) {
//                         if (verbose >= level) {
//                             printf("---minprec: %ld, maxprec: %ld; increase precision for all at minprec \n", minprec, *prec);
//                         }
//                         for (slong i=0; i<len-1; i++) {
//                             if (PW_EA_IS_TAMED(tamed[i]))
//                                 continue;
//                             if (precs[i]==minprec) {
//                                 precs[i] = *prec;
//                                 _pw_EA_evaluate_on_middle_root( polyvals+i, pdervals+i, roots+i, poly, precs[i] );
//                             }
//                         }
//                         minprec = *prec;
//                     }
//                 } else {
//                     if (verbose >= level) {
//                         printf("---minprec: %ld, maxprec: %ld; increase precision for non decreased \n", minprec, *prec);
//                     }
//                     for (slong i=0; i<len-1; i++) {
//                         if (PW_EA_IS_TAMED(tamed[i]))
//                             continue;
//                         mag_mul_2exp_si(radd, minRads+i, 0);
//                         if ( mag_cmp( arb_radref( acb_realref( roots+i ) ), radd )<0 ) {
//                             precs[i]*=2;
//                             *prec = PWPOLY_MAX( *prec, precs[i] );
//                             _pw_EA_evaluate_on_middle_root( polyvals+i, pdervals+i, roots+i, poly, precs[i] );
//                         }
//                     }
//                 }
//                 mag_clear(radd);
//                 /* actualize the min of rads */
//                 for (slong i=0; i<len-1; i++) {
//                     if (PW_EA_IS_TAMED(tamed[i]))
//                         continue;
//                     mag_min( minRads+i, minRads+i, arb_radref( acb_realref( roots+i ) ) );
//                 }
//                 
//             }
//             
//             /* actualize old_prod */
//             arb_set(old_prod, prod);
//             
//             if (verbose >=level+1 ) {
//                 printf("\n\n");
//             }
//             
//         }
//         if ( nbTamed < len-1 )
//             nbItts++;
//         
//     }
//     
//     pwpoly_free(precs);
//     _mag_vec_clear(minRads, len-1);
// //     _conCom_list_clear(ccs_save);
// //     pwpoly_free(tamed_save);
// //     _acb_vec_clear(roots_save, len-1);
//     
//     _acb_vec_clear( pdervals, len-1 );
//     _acb_vec_clear( polyvals, len-1 );
//     
//     /* set the result from connected components */
//     *nbclusts=0;
//     int allNatural = 1;
//     conCom_list_iterator it = _conCom_list_begin(ccs);
//     while( it!=_conCom_list_end() ) {
//         conCom_ptr ccur = _conCom_list_elmt(it);
//         slong mult = _slong_list_get_size( conCom_component_discsref( ccur ) );
//         allNatural = allNatural&&conCom_naturalref(ccur);
//         acb_set( roots+(*nbclusts), conCom_containing_boxref( ccur ) );
//         if ( realCoeffs && (mult==1) && conCom_naturalref(ccur) && ( arb_contains_zero( acb_imagref(roots+(*nbclusts) )) ) )
//             arb_zero( acb_imagref(roots+ (*nbclusts)) );
//         mults[ (*nbclusts) ] = mult;
//         (*nbclusts) += 1;
//         it = _conCom_list_next(it);
//     }
//     
//     slong res = nbItts;
//     /* (i), (ii), (iii) hold */
//     /* if allNatural = 0, the necessarily nbItts = nbMaxItts (otherwise nbTamed would be len-1) */
//     if (allNatural==0)
//         res = -res;
//     
//     /* if nbItts=nbMaxItts, then all clusters are natural but not isolated of not approximated */
//     
//     if (verbose >=level ) {
//         printf("after %ld EA its, res: %ld \n", nbItts, res); 
//         printf("--- nb CC: %ld\n", _conCom_list_get_size(ccs) );
//         printf("--- nb wild roots: %ld, nb tamed roots: %ld\n", nbWild, nbTamed); 
//         printf("--- prec: %ld\n", *prec);
//     }
//     
//     pwpoly_free(tamed);
//     _conCom_list_clear(ccs);
//     arb_clear(rad2);
//     arb_clear(rad);
//     arb_clear(prod);
//     arb_clear(old_prod);
//     acb_clear(mid);
// //     acb_poly_clear(polyder);
//     
//     return res;
// }

/* DEPRECATED */
// slong _EA_inclusion_discs( acb_ptr roots, int * tamed, int * ntamed, 
//                            acb_ptr polyvals, acb_ptr pdervals,
//                            slong lenRoots, 
//                            pw_polynomial_t cache,
//                            int goal, slong log2eps, slong prec ) {
//     
// #ifdef PW_EA_PROFILE
//     clock_t start_inc = clock();
// #endif
//         
//     slong nbNtamed = 0;
//     arb_t radius;
//     arb_init(radius);
//     
//     for (slong i=0; i<lenRoots; i++) {
//         if ( PW_EA_IS_TAMED(tamed[i]) ) {
//             ntamed[i] = 0;
//             continue;
//         }
//         
//         int inclusion = _EA_get_inclusion_disc_and_status( radius, roots+i, polyvals+i, pdervals+i, cache, prec PW_VERBOSE_CALL(0) );
//         acb_get_mid( roots+i, roots+i );
//         acb_add_error_arb( roots+i, radius);
//         int sizeOK = (PW_GOAL_MUST_APPROXIMATE(goal)==0)
//                     ||pwpoly_width_less_eps( roots+i, log2eps, PW_GOAL_PREC_RELATIVE(goal) );
//         
//         if ( (inclusion == PW_NATURAL_I) && sizeOK ) {
//             PW_EA_SET_TAMED( ntamed[i] );
//             nbNtamed++;
//         } else {
//             ntamed[i] = 0;
//             if (inclusion == PW_NATURAL_I) {
//                 PW_EA_SET_INCLUSION(ntamed[i], PW_NATURAL_I);
//             }
//         }
//         
//     }
//     
//     arb_clear(radius);
//     
// #ifdef PW_EA_PROFILE
//     clicks_in_inclEA += (clock() - start_inc);
// #endif
//     
//     return nbNtamed;
// }
// void _EA_weierstrass_discs( acb_ptr roots, int * tamed, 
//                             acb_ptr polyvals,
//                             slong lenRoots,
//                             pw_polynomial_t cache,
//                             slong prec ) {
// 
// #ifdef PW_EA_PROFILE
//     clock_t start_weier = clock();
// #endif
//     
//     arb_ptr dists    = _arb_vec_init(lenRoots);
//     arb_ptr radii    = _arb_vec_init(lenRoots);
//     acb_t dist;
//     acb_init(dist);
//     arb_t temp, absleading;
//     arb_init(temp);
//     arb_init(absleading);
//     acb_poly_srcptr pol = pw_polynomial_cacheANDgetApproximation(cache, prec);
//     acb_ptr poly = pol->coeffs;
//     slong lenPoly = pol->length;
//     acb_abs( absleading, poly + (lenPoly-1), prec);
//     
//     for (slong i=0; i<lenRoots; i++) {
//         if ( PW_EA_IS_TAMED(tamed[i]) )
//             continue;
//         
//         acb_get_mid(roots+i, roots+i);
//     }
//     
//     for (slong i=0; i<lenRoots; i++) {
//         if ( PW_EA_IS_TAMED(tamed[i]) )
//             continue;
//         
// #ifdef PW_EA_PROFILE
//         clock_t start_dists = clock();
// #endif
//         arb_set(dists+i, absleading);
//         for (slong j=0; j<lenRoots; j++) {
//             if (j!=i) {
//                 acb_sub(dist, roots+i, roots+j, prec);
//                 acb_abs(temp, dist, prec);
//                 arb_mul(dists+i, dists+i, temp, prec);
//             }
//         }
// #ifdef PW_EA_PROFILE
//         clicks_in_weier_dists += (clock() - start_dists);
//         nb_weier_dists++;
// #endif
//         acb_abs(radii+i, polyvals+i, prec);
//         arb_div(radii+i, radii+i, dists+i, prec); 
//         arb_mul_ui(radii+i, radii+i, (lenPoly-1), prec);
//     }
//     
//     for (slong i=0; i<lenRoots; i++) {
//         if ( PW_EA_IS_TAMED(tamed[i]) )
//             continue;
//         
//         acb_add_error_arb( roots+i, radii + i);
//     }
//     
//     arb_clear(absleading);
//     arb_clear(temp);
//     acb_clear(dist);
//     _arb_vec_clear(radii, lenRoots);
//     _arb_vec_clear(dists, lenRoots);
//     
// #ifdef PW_EA_PROFILE
//     clicks_in_weierstrass += (clock() - start_weier);
// #endif
// }
// int _EA_is_tamed( int status, int goal ) {
//     int res = 1;
//     if (PW_GOAL_MUST_ISOLATE(goal))
//         res = res&&PW_EA_IS_NATURAL_I(status);
//     if (PW_GOAL_MUST_APPROXIMATE(goal))
//         res = res&&PW_EA_IS_INCLUDING(status)&&PW_EA_IS_APPROXIMD(status);
//     return res;
// }
// slong _conCom_list_check_natural( conCom_list_t l, slong prec) {
//     
//     slong nbNatural = 0;
//     acb_t threeBox;
//     acb_init(threeBox);
//     conCom_ptr ccur, ccur2;
//     
//     conCom_list_iterator it = _conCom_list_begin(l);
//     while ( it!=_conCom_list_end() ) {
//         ccur = _conCom_list_elmt(it);
//         acb_set( threeBox, conCom_containing_boxref(ccur) );
//         conCom_naturalref(ccur) = 1;
//         
//         /* verify that containing box does not intersect 0 */
//         if ( acb_contains_zero( threeBox ) )
//             conCom_naturalref(ccur) = 0;
//         
//         /* inflate by factor 3 */
//         mag_mul_ui( arb_radref(acb_realref(threeBox)), arb_radref(acb_realref(threeBox)), 3);
//         mag_mul_ui( arb_radref(acb_imagref(threeBox)), arb_radref(acb_imagref(threeBox)), 3);
//         
//         /* verify that 3*containing box does not intersect 0 */
//         if ( acb_contains_zero( threeBox ) )
//             conCom_naturalref(ccur) = 0;
//         
//         /* check if threeBox is separated from other cc */
//         conCom_list_iterator it2 = _conCom_list_begin(l);
//         while ( (it2!=_conCom_list_end()) && (conCom_naturalref(ccur)==1)  ) {
//             
//             if (it2!=it) {
//                 ccur2 = _conCom_list_elmt(it2);
//                 if (acb_overlaps( threeBox, conCom_containing_boxref(ccur2) )) {
//                     conCom_naturalref(ccur) = 0;
//                 }
//             }
//             
//             it2 = _conCom_list_next(it2);
//         }
//         
//         if (conCom_naturalref(ccur)==1)
//             nbNatural++;
//         
//         it = _conCom_list_next(it);
//     }
//     
//     acb_clear(threeBox);
//     return nbNatural;
// }
// 
// slong _conCom_list_check_sizeOK( conCom_list_t l, slong log2eps, int goal) {
//     
//     slong nbSizeOK = 0;
//     conCom_ptr ccur;
//     
//     conCom_list_iterator it = _conCom_list_begin(l);
//     while ( it!=_conCom_list_end() ) {
//         ccur = _conCom_list_elmt(it);
//         conCom_sizeOKref(ccur) = pwpoly_width_less_eps( conCom_containing_boxref(ccur), log2eps, PW_GOAL_PREC_RELATIVE(goal) );
//         nbSizeOK += conCom_sizeOKref(ccur);
//         it = _conCom_list_next(it);
//     }
//     
//     return nbSizeOK;
//     
// }
// 
// int _EA_pairwise_disjoint( acb_ptr roots, int * tamed, slong lenRoots ) {
//     int res = 1;
//     for (slong i=0; (i<lenRoots-1) && (res==1); i++) {
//         if (tamed[i]>=1)
//             for (slong j=i+1; (j<lenRoots) && (res==1); j++)
//                 res = (tamed[j]>=1)&&(acb_overlaps( roots + i, roots + j )==0);
//         else
//             res = 0;
//     }
//     return res;
// }
// 
// int _EA_natural_pairwise_disjoint( acb_ptr roots, int * tamed, slong lenRoots ) {
//     int res = 1;
//     for (slong i=0; (i<lenRoots-1) && (res==1); i++) {
//         if (PW_EA_IS_NATURAL_I(tamed[i])) {
//             for (slong j=i+1; (j<lenRoots) && (res==1); j++) {
//                 if (PW_EA_IS_NATURAL_I(tamed[j]))
//                     res = (acb_overlaps( roots + i, roots + j )==0);
//             }
//         }
//     }
//     return res;
// }
// void _EA_actualize_wild_tamed( acb_ptr roots, int *tamed, slong lenRoots, 
//                                slong *nbWild, slong *nbTamed,
//                                slong log2eps, int goal ){
//     
//     *nbWild = 0;
//     *nbTamed= 0;
//     
//     for (slong i=0; i<lenRoots; i++) {
// //         if (PW_EA_IS_INCLUDING(tamed[i])&&PW_EA_IS_APPROXIMD(tamed[i]))
//         if (_EA_is_tamed(tamed[i], goal))
//             (*nbTamed)++;
//         else {
//             if ( PW_EA_IS_INCLUDING(tamed[i]) && pwpoly_width_less_eps( roots + i, log2eps, PW_GOAL_PREC_RELATIVE(goal) ) )
//                 PW_EA_SET_APPROXIMD(tamed[i]);
//             if (_EA_is_tamed(tamed[i], goal)) {
//                 (*nbTamed)++;
//             } else {
// //                 acb_get_mid(roots + i, roots + i);
//                 (*nbWild)++;
//             }
//         }
//     }
//     
// }
