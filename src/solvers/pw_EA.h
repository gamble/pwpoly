/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_EA_H
#define PW_EA_H

#include "pw_base.h"
#include "common/pw_common.h"
#include "covering/pw_covering.h"
#include "approximation/pw_approximation.h"
#include "common/pw_roots.h"
#include "solvers/acb_list.h"
#include "solvers/solver_CoCo.h"
#include "geometry/domain.h"

#ifndef PW_NO_INTERFACE
#include "common/pw_gnuplot.h"
#endif

// #define PW_INDETERMI 0
// #define PW_INCLUDING 1
// #define PW_ISOLATING 2
// #define PW_NATURAL_I 3
#define PW_EA_APPROXIMD 4

#define PW_EA_IS_INCLUDING(S) ( (S&0x3) > 0    )
#define PW_EA_IS_ISOLATING(S) ( (S&0x3) > 1    )
#define PW_EA_IS_NATURAL_I(S) ( (S&0x3) > 2    )
#define PW_EA_IS_APPROXIMD(S) ( (S&0x4) == 0x4 )
// #define PW_EA_NEWTO_FAILED(S) ( (S&0x8) == 0x8 )
#define PW_EA_NEWTO_SUCCEE(S) ( (S&0x8) == 0x8 )

#define PW_EA_SET_INCLUSION(T,S) ( T = ( (T&0x4)|S   ) )
#define PW_EA_SET_APPROXIMD(T)   ( T = ( T|0x4 ) )
#define PW_EA_UNSET_APPROXIMD(T)   ( T = ( T&0x3 ) )

// #define PW_EA_SET_NEWTO_FAILED(T) ( T = (T&0x7)|0x8 )
#define PW_EA_SET_NEWTO_SUCCEE(T) ( T = (T&0x7)|0x8 )

/* assume that when the goal is not to approximate, PW_EA_IS_APPROXIMD(T) is 1 */
#define PW_EA_IS_TAMED(T) (PW_EA_IS_NATURAL_I(T)&&PW_EA_IS_APPROXIMD(T))
#define PW_EA_SET_TAMED(T) ( T = ( 0x7 ) )

#ifdef __cplusplus
extern "C" {
#endif
    
/* connected components for union find */
// PWPOLY_INLINE void _slong_clear(void * i){
//     pwpoly_free ( (slong *)i );
// }
void _slong_clear(void * i);
#ifndef PW_SILENT
PWPOLY_INLINE void _slong_fprint(FILE * file, const void * i){
    fprintf(file, "%ld", *( (slong *) i) );
}
#endif
PWPOLY_INLINE int _slong_isless(const void * d1, const void * d2){
    return ( *((slong *) d1) <= *((slong *) d2) );
}

typedef struct list   slong_list;
typedef struct list   slong_list_t[1];

PWPOLY_INLINE void    _slong_list_init ( slong_list_t l )                 { list_init(l); }
PWPOLY_INLINE void    _slong_list_swap ( slong_list_t l1, slong_list_t l2 ) { list_swap(l1, l2); }
PWPOLY_INLINE void    _slong_list_append ( slong_list_t l1, slong_list_t l2 ) { list_append(l1, l2); }
PWPOLY_INLINE void    _slong_list_clear( slong_list_t l )                 { list_clear(l, _slong_clear); }
PWPOLY_INLINE void    _slong_list_empty( slong_list_t l )                 { list_empty(l); }
PWPOLY_INLINE void    _slong_list_push ( slong_list_t l, slong b )          { 
    slong * pb = (slong *) pwpoly_malloc (sizeof(slong));
    *pb = b;
    list_push(l, (void *)pb); 
}
PWPOLY_INLINE void    _slong_list_insert_sorted( slong_list_t l, slong b ){ 
    slong * pb = (slong *) pwpoly_malloc (sizeof(slong));
    *pb = b;
    list_insert_sorted(l, (void *)pb, _slong_isless); 
}
PWPOLY_INLINE slong _slong_list_pop  ( acb_list_t l ) {
    slong * pb = (slong *) list_pop(l);
    slong b = *pb;
    pwpoly_free(pb);
    return b;
}

/* if res is not empty, empty it */
void _slong_list_copy( slong_list_t res, const slong_list_t src );

PWPOLY_INLINE int     _slong_list_is_empty  ( const slong_list_t l )            { return list_is_empty(l); }
PWPOLY_INLINE slong   _slong_list_get_size  ( const slong_list_t l )            { return list_get_size(l); }

/*iterator */
typedef list_iterator slong_list_iterator;
PWPOLY_INLINE slong_list_iterator _slong_list_begin(const slong_list_t l)   { return list_begin(l); }
PWPOLY_INLINE slong_list_iterator _slong_list_endEl(const slong_list_t l)   { return list_endEl(l); }
PWPOLY_INLINE slong_list_iterator _slong_list_next(slong_list_iterator it)  { return list_next(it); }
PWPOLY_INLINE slong_list_iterator _slong_list_prev(slong_list_iterator it)  { return list_prev(it); }
PWPOLY_INLINE slong_list_iterator _slong_list_end()                         { return list_end(); }
PWPOLY_INLINE slong               _slong_list_elmt(slong_list_iterator it)  { return *((slong*)list_elmt(it)); }

typedef struct {
    acb_struct containing_box;
    slong_list component_discs;
    int        natural;
    int        sizeOK;
    int        sepCrit;
//     int        tamedCl;
} conCom;

typedef conCom conCom_t[1];
typedef conCom * conCom_ptr;

#define conCom_containing_boxref(X) ( &(X)->containing_box )
#define conCom_component_discsref(X) ( &(X)->component_discs )
#define conCom_naturalref(X) ( (X)->natural )
#define conCom_sizeOKref(X) ( (X)->sizeOK )
#define conCom_sepCritref(X) ( (X)->sepCrit )
// #define conCom_tamedClref(X) ( (X)->tamedCl )

void _conCom_init_acb( conCom_t cc, acb_srcptr boxes, slong index );
void _conCom_init_set( conCom_t res, const conCom_t src );
void _conCom_clear( conCom_t cc );

PWPOLY_INLINE int _conCom_overlaps( const conCom_t cc1, const conCom_t cc2 ) {
    return acb_overlaps( conCom_containing_boxref(cc1), conCom_containing_boxref(cc2) );
}

void _conCom_union( conCom_t cc1, conCom_t cc2, slong prec );

#ifndef PW_SILENT 
void _conCom_fprintd( FILE * file, const conCom_t cc, slong digits );
PWPOLY_INLINE void _conCom_printd( const conCom_t cc, slong digits ){
    return _conCom_fprintd(stdout, cc, digits);
}
#endif

/* list of connected components for union find */
PWPOLY_INLINE void _conCom_clear_for_list(void * b){
    _conCom_clear( (conCom_ptr) b );
}

typedef struct list   conCom_list;
typedef struct list   conCom_list_t[1];

PWPOLY_INLINE void    _conCom_list_init ( conCom_list_t l )                 { list_init(l); }
PWPOLY_INLINE void    _conCom_list_swap ( conCom_list_t l1, conCom_list_t l2 ) { list_swap(l1, l2); }
PWPOLY_INLINE void    _conCom_list_append ( conCom_list_t l1, conCom_list_t l2 ) { list_append(l1, l2); }
PWPOLY_INLINE void    _conCom_list_clear( conCom_list_t l )                 { list_clear(l, _conCom_clear_for_list); }
PWPOLY_INLINE void    _conCom_list_empty( conCom_list_t l )                 { list_empty(l); }
/* push in the back of the list */
PWPOLY_INLINE void    _conCom_list_push ( conCom_list_t l, conCom_ptr b )   { list_push(l,b); }
/* pop the front of the list */
PWPOLY_INLINE conCom_ptr _conCom_list_pop  ( conCom_list_t l )   { return (conCom_ptr) list_pop(l); }

/* get the front of the list */
PWPOLY_INLINE conCom_ptr _conCom_list_first  ( conCom_list_t l )   { return (conCom_ptr) list_first(l); }

PWPOLY_INLINE int     _conCom_list_is_empty  ( const conCom_list_t l )            { return list_is_empty(l); }
PWPOLY_INLINE slong   _conCom_list_get_size  ( const conCom_list_t l )            { return list_get_size(l); }

/* if res is not empty, empty it */
void _conCom_list_copy( conCom_list_t res, const conCom_list_t src );

/*iterator */
typedef list_iterator conCom_list_iterator;
PWPOLY_INLINE conCom_list_iterator _conCom_list_begin(const conCom_list_t l)   { return list_begin(l); }
PWPOLY_INLINE conCom_list_iterator _conCom_list_endEl(const conCom_list_t l)   { return list_endEl(l); }
PWPOLY_INLINE conCom_list_iterator _conCom_list_next(conCom_list_iterator it)  { return list_next(it); }
PWPOLY_INLINE conCom_list_iterator _conCom_list_prev(conCom_list_iterator it)  { return list_prev(it); }
PWPOLY_INLINE conCom_list_iterator _conCom_list_end()                       { return list_end(); }
PWPOLY_INLINE conCom_ptr           _conCom_list_elmt(conCom_list_iterator it)  { return (conCom_ptr) list_elmt(it); }

/* Pre-conditions  : assume poly has degree d with non-zero leading and trailing coeffs         */
/*                   roots and mults have enough rooms for d acb, slong                         */
/*                   for 0 <= i <= lenNatural, letting B(ci,ri) = roots + i,                    */
/*                        the disc D(ci,ri) is a natural isolator for a root of poly            */
/*                        and mults[i]=1                                                        */
/*                   nbMaxItts>=1                                                               */
/* Post-conditions : for 0 <= i < *nbclusts, let B(ci,ri) = roots + i,                          */
/*                   Let res be the returned value.                                             */
/*                   if 0<=res<nbMaxItts then the solving process succeeded:                    */
/*                   (i)   the D(ci,ri)'s are pairwise disjoint                                 */
/*                   (ii)  any root of poly is in a D(ci, ri) and SUM_{0<=1<*nbclusts}mults[i] = degree */
/*                   (iii) any D(ci,ri) contains mults[i] roots                                 */
/*                   (iv)  any D(ci,ri) is a natural cluster of mults[i] roots                  */
/*                   (v)   if goal is isolate, *nbclusts = d and mults[i] = 1 for all i         */
/*                   (vi)  if goal is approximate, B(ci,ri) has                                 */
/*                             relative (or absolute) precision at least log2eps                */
/*                   if res >= nbMaxItts, then (i), (ii), (iii), (iv) hold but not (v) and (vi) */
/*                   if res < 0           then (i), (ii), (iii) hold but not (iv), (v) and (vi) */

slong pw_EA_solve_pw_polynomial ( acb_ptr roots, slong * mults, slong * nbclusts, 
                                  slong lenNatural,
                                  pw_polynomial_t poly,
                                  int goal, slong log2eps, const domain_t dom, slong goalNbRoots,
                                  slong *prec, slong nbMaxItts, slong nbIttsBeforeIncreasePrec PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(f));

slong pw_EA_solve_2fmpq_polynomial ( acb_ptr roots, slong * mults, slong * nbclusts,
                                     const fmpq_poly_t pre, const fmpq_poly_t pim,
                                     int goal, slong log2eps, const domain_t dom, slong goalNbRoots,
                                     slong *prec, slong nbMaxItts PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(f));

slong pw_EA_solve_exact_acb_polynomial   ( acb_ptr roots, slong * mults, slong * nbclusts,
                                           const acb_poly_t p,
                                           int goal, slong log2eps, const domain_t dom, slong goalNbRoots,
                                           slong *prec, slong nbMaxItts PW_VERBOSE_ARGU(verbose) PW_FILE_ARGU(f));

// slong pw_EA_solve_pw_polynomial_double ( acb_ptr roots, slong * mults, slong * nbclusts, 
//                                          pw_polynomial_t poly,
//                                          int goal, slong log2eps, const domain_t dom, slong *prec, 
//                                          slong nbMaxItts, slong nbIttsBeforeIncreasePrec PW_VERBOSE_ARGU(verbose), FILE * f);

// slong pw_EA_solve_pw_polynomial_precs ( acb_ptr roots, slong * mults, slong * nbclusts, 
//                                         slong lenNatural,
//                                         pw_polynomial_t poly,
//                                         int goal, slong log2eps, slong *prec, 
//                                         slong nbMaxItts, slong nbIttsBeforeIncreasePrec PW_VERBOSE_ARGU(verbose), FILE * f);

// #define PW_EA_PROFILE
#ifdef PW_EA_PROFILE
#include <time.h>
extern double clicks_in_initial;
extern double clicks_in_newtoEA;
extern double clicks_in_EA_itts;
extern double clicks_in_EA_dists;
extern double clicks_in_inclEA;
extern double clicks_in_inc_evals;
extern double clicks_in_weierstrass;
extern double clicks_in_weier_dists;
extern double clicks_in_evaluations;
extern double clicks_in_zoom_clusts;
extern double clicks_in_union_find;
extern double clicks_in_pd_ntamed;
extern double clicks_in_check_nat;
extern double clicks_in_actualize_ccs;
extern slong  nb_newtEA;
extern slong  nb_evals;
extern slong  nb_EA_dists;
extern slong  nb_inc_evals;
extern slong  nb_weier_dists;
extern slong  nb_zoom_clusts;

#define PW_EA_INIT_PROFILE {\
    clicks_in_initial = 0.; \
    clicks_in_newtoEA = 0.; \
    clicks_in_EA_itts = 0.; \
    clicks_in_EA_dists = 0.; \
    clicks_in_inclEA = 0.; \
    clicks_in_inc_evals = 0.; \
    clicks_in_weierstrass = 0.; \
    clicks_in_weier_dists = 0.; \
    clicks_in_evaluations = 0.; \
    clicks_in_zoom_clusts = 0.; \
    clicks_in_union_find = 0.; \
    clicks_in_pd_ntamed = 0.; \
    clicks_in_check_nat = 0.; \
    clicks_in_actualize_ccs = 0.; \
    nb_newtEA = 0; \
    nb_evals = 0; \
    nb_EA_dists = 0; \
    nb_inc_evals= 0; \
    nb_weier_dists = 0;\
    nb_zoom_clusts = 0;\
}

#ifndef PW_NO_INTERFACE
#define PW_EA_PRINT_PROFILE {\
    printf("\n"); \
    printf("&&&&&&&&&&&&&&&&&&&&&&& PW EA PROFILE &&&&&&&&&&&&&&&&&&&&&&&&\n"); \
    if (clicks_in_initial!=0) { \
    printf("   --> initial values:        \n" ); \
    printf("      --> total time:              %lf\n", clicks_in_initial/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_newtoEA!=0) { \
    printf("   --> newton iterations:     \n" ); \
    printf("      --> nb itts:                 %ld\n", nb_newtEA ); \
    printf("      --> total time:              %lf\n", clicks_in_newtoEA/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_EA_itts!=0) { \
    printf("   --> EA iterations:         \n" ); \
    printf("      --> nb dists:                %ld\n", nb_EA_dists ); \
    printf("      --> time in dists:           %lf\n", clicks_in_EA_dists/CLOCKS_PER_SEC ); \
    printf("      --> total time:              %lf\n", clicks_in_EA_itts/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_inclEA!=0) { \
    printf("   --> inclusion discs:       \n" ); \
    printf("      --> nb evals:                %ld\n", nb_inc_evals ); \
    printf("      --> time in evals:           %lf\n", clicks_in_inc_evals/CLOCKS_PER_SEC ); \
    printf("      --> total time:              %lf\n", clicks_in_inclEA/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_weierstrass!=0) { \
    printf("   --> weierstrass discs:     \n" ); \
    printf("      --> nb dists:                %ld\n", nb_weier_dists ); \
    printf("      --> time in dists:           %lf\n", clicks_in_weier_dists/CLOCKS_PER_SEC ); \
    printf("      --> total time:              %lf\n", clicks_in_weierstrass/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_zoom_clusts!=0) { \
    printf("   --> zooming on clusters:     \n" ); \
    printf("      --> nb zooms:                %ld\n", nb_zoom_clusts ); \
    printf("      --> total time:              %lf\n", clicks_in_zoom_clusts/CLOCKS_PER_SEC ); \
    } \
    if (clicks_in_evaluations!=0) { \
    printf("   --> evaluations:           \n" ); \
    printf("      --> nb evals of p and p':     %ld\n", nb_evals ); \
    printf("      --> time in :                 %lf\n", clicks_in_evaluations/CLOCKS_PER_SEC ); \
    } \
    if ( (clicks_in_pd_ntamed+clicks_in_union_find+clicks_in_actualize_ccs+clicks_in_check_nat)!=0) { \
    printf("   --> geometry:            \n" ); \
    printf("      --> pair. disj. ntamed:      %lf\n", clicks_in_pd_ntamed/CLOCKS_PER_SEC ); \
    printf("      --> union find:              %lf\n", clicks_in_union_find/CLOCKS_PER_SEC ); \
    printf("      --> actualize ccs:           %lf\n", clicks_in_actualize_ccs/CLOCKS_PER_SEC ); \
    printf("      --> check natural ccs:       %lf\n", clicks_in_check_nat/CLOCKS_PER_SEC ); \
    } \
    printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"); \
    printf("\n\n"); \
}
#else
#define PW_EA_PRINT_PROFILE
#endif
#endif

/* DEPRECATED */
// void _EA_one_iteration_on_wild_roots( acb_ptr wild, slong lenWild, acb_ptr tamed, slong lenTamed,
//                                       acb_srcptr poly, slong len, slong prec );
// 
// void _EA_inclusion_discs( acb_ptr wild, slong lenWild,
//                           acb_srcptr poly, slong len, slong prec );
// 
// void _EA_actualize_wild_tamed( acb_ptr wild, slong *lenWild, acb_ptr tamed, slong *lenTamed,
//                                slong log2eps, int goal );

#ifdef __cplusplus
}
#endif

#endif
