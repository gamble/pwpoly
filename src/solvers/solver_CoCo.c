/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "solver_CoCo.h"

#ifdef PW_COCO_PROFILE
double clicks_in_tstar_exclusion;
double clicks_in_QBR;
ulong nb_tstar_exclusion;
ulong nb_tstar_exclusion_zero;
ulong nb_tstar_exclusion_posi;
ulong nb_tstar_exclusion_fail;
ulong nb_tstar_exclusion_prec53;
ulong nb_tstar_exclusion_prec106;
#endif

int      _realCoeffs;
int      _unitDisc;  /* 1 if subdivision of the unit disc */
int      _tstar_round;
int      _tstar_antic;
double   _inflation; /* set to 1.2 */
domain_ptr _domain; /* initialized if must use it, NULL otherwise */
int      _strInDom; /* assume there are no roots on boundary, */
                    /* and get the roots strictly inside the domain */

#define EXCL_NO_ROOT 1
#define EXCL_CAN_NOT_DECIDE 0
#define EXCL_MAX_PREC_REACHED -1
/* returns -2 if all exclusion test returned -2 */
/* otherwise returns 1 */
/* increases precres to max prec used */
int _exclusion_test_box_list ( slong *precres, box_list_t boxes, pw_polynomial_t cache PW_VERBOSE_ARGU(verbose)) {
  
#ifndef PW_SILENT
    int level = 4;
#endif
    
    int res = -2;
    
    box_list_t temp;
    box_list_init(temp);
    box_ptr cur_box;
    
    int boxWidthGreatThanDomainWidth = 0;
    if ( (_domain!=NULL) && domain_is_finite(_domain) ){
        fmpq_t wd, wb;
        fmpq_init(wd);
        fmpq_init(wb);
        domain_get_max_width( wd, _domain );
        if ( ! box_list_is_empty(boxes) )
            box_get_width( wb, box_list_first(boxes) );
        boxWidthGreatThanDomainWidth = (fmpq_cmp( wb, wd ) > 0);
        fmpq_clear(wb);
        fmpq_clear(wd);
    }
        
    while ( ! box_list_is_empty(boxes) ) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif
            
        cur_box = box_list_pop(boxes);
// #ifndef PW_SILENT
//         if (verbose >=level) {
//                 printf("------------exclusion_test_box_list: box:"); box_print(cur_box); printf("\n");
//                 printf("                                     flag: %d\n", _unitDisc);
//         }
// #endif
        
        /* check if cur_box is within inflation*B(0,1) */
        int excluded = _unitDisc && box_is_outside_unit_inflated( cur_box, _inflation );
        /* if real coeffs, test if cur_box is strictly negative */
        excluded = excluded || ( _realCoeffs && box_is_imaginary_negative_strict(cur_box) );
        /* test if cur_box is within inflated domain */
        excluded = excluded || ( (_domain!=NULL) && domain_is_box_outside_inflated_domain( _domain, cur_box ) );
#ifndef PW_SILENT        
        if (verbose >=level) {
            printf("------------exclusion_test_box_list: is imaginary negative strict: %d\n", excluded);
        }
#endif  
        
        if (excluded==0){
            
            if ( box_nbRinref(cur_box) ) { /* cur_box contains some roots ! */
#ifndef PW_SILENT        
//             if (verbose >=level) {
//                 printf("------------exclusion_test_box_list: contains roots!\n");
//             }
#endif
                box_list_push(temp, cur_box);
                continue;
            }
            
            if ( boxWidthGreatThanDomainWidth ) {
#ifndef PW_SILENT        
//             if (verbose >=level) {
//                 printf("------------exclusion_test_box_list: box greater than domain\n");
//             }
#endif
                box_list_push(temp, cur_box);
                continue;                
            }
            
            slong *prec = &(box_pprecref(cur_box));
#ifndef PW_SILENT
            if (verbose >=level)
                printf("------------exclusion_test_box_list: prec: %ld, precres: %ld\n", *prec, *precres);
#endif            
            *prec = *precres;

            slong tstar_res = -2;

#ifdef PW_COCO_PROFILE
            clock_t start = clock();
#endif
            slong nbMsols = ( box_nbMroref(cur_box)>=0?box_nbMroref(cur_box):pw_polynomial_degree(cache) );
            nbMsols = PWPOLY_MIN( pw_polynomial_maxNbRref(cache), nbMsols );
            tstar_res = tstar_box( prec, cur_box, cache, nbMsols, 1, _tstar_round, _tstar_antic );  
#ifdef PW_COCO_PROFILE
            nb_tstar_exclusion++;
            clicks_in_tstar_exclusion += (clock() - start);
            if (*prec==PWPOLY_DEFAULT_PREC)
                nb_tstar_exclusion_prec53++;
            if (*prec==2*PWPOLY_DEFAULT_PREC)
                nb_tstar_exclusion_prec106++;
            if (tstar_res==0)
                nb_tstar_exclusion_zero++;
            if (tstar_res>0)
                nb_tstar_exclusion_posi++;
            if (tstar_res<0)
                nb_tstar_exclusion_fail++;
#endif
            if (tstar_res > -2)
                res = 1;
            if (tstar_res > -1)
                box_nbMroref(cur_box) = tstar_res;
#ifndef PW_SILENT            
            if (verbose >=level)
                printf("------------exclusion_test_box_list: tstar res: %ld, prec: %ld\n\n", tstar_res, *prec);
#endif  

            excluded=(tstar_res==0);
            if (*prec > *precres)
                *precres = *prec;
        }
        
        if(excluded) {
            box_clear(cur_box);
            pwpoly_free(cur_box);
        } else {
            box_list_push(temp, cur_box);
        }
    }
    
    box_list_swap(boxes, temp);
    box_list_clear(temp);
    
    return res;
    
}

void _quadrisect_box_CoCo ( box_CoCo_list_t res, box_CoCo_t cc, pw_polynomial_t cache PW_VERBOSE_ARGU(verbose)) {
  
#ifndef PW_SILENT        
    int level=4;
#endif     
    box_list_t subboxes;
    box_list_init(subboxes);
    
    /* RealCoeffs */
    int cc_contains_real_line = 0;
    /* Check if cc contains the real line */
    if ( _realCoeffs && (!box_CoCo_is_imaginary_positive(cc)) )
        cc_contains_real_line = 1;
    /* end RealCoeffs */
    
    slong nbRoo_save = box_CoCo_nbRooref(cc);
    int   T_Sep_save = box_CoCo_2_Sepref(cc);
    int   E_Sep_save = box_CoCo_6_Sepref(cc);
    int   QBRSu_save = box_CoCo_QBRSuref(cc);
    
    /* quadrisect boxes of cc, push it into subboxes */
    /* subboxes is now a sorted list of boxes */
    box_list_quadrisect( subboxes, box_CoCo_boxesref(cc) );
    
    /* apply exclusion test to boxes of the list subboxes */
    /* subboxes is still a sorted list of boxes */
    slong maxprec = PWPOLY_DEFAULT_PREC;
    maxprec = PWPOLY_MAX( PWPOLY_DEFAULT_PREC, box_CoCo_Mprecref(cc)/2 );
#ifndef PW_SILENT     
    if (verbose >=level) {
        printf("------quadrisect_box_CoCo: prec of cc: %ld, prec for exclusion tests: %ld\n", box_CoCo_Mprecref(cc), maxprec);
    }
#endif
    int max_prec_reached = _exclusion_test_box_list ( &maxprec, subboxes, cache PW_VERBOSE_CALL(verbose)  );
    max_prec_reached = (max_prec_reached==-2);
    max_prec_reached = max_prec_reached || (box_list_get_size(subboxes) > 9*pw_polynomial_degree(cache) );
#ifndef PW_SILENT     
    if (verbose >=level)
        printf("------quadrisect_box_CoCo: maxprec: %ld, max_prec_reached: %d\n", maxprec, max_prec_reached);
#endif     
    
    /* construct the list of connected components */
    box_ptr btemp;
    while (! box_list_is_empty(subboxes) ) {
        btemp = box_list_pop(subboxes);
        box_CoCo_union_box( res, btemp);
    }

    if (box_CoCo_list_get_size(res) == 1) { /* cc has not been split */
        
        int specialFlag = 0;
        if ( _realCoeffs && (cc_contains_real_line == 1) ) {
            if ( box_CoCo_is_imaginary_positive(box_CoCo_list_first(res)) )
                specialFlag = 1;
        }
        
        if (specialFlag==0) {
            /* re-use nb of roots and separation flag */
            box_CoCo_ptr x = box_CoCo_list_first(res);
            box_CoCo_nbRooref(x) = nbRoo_save;
            box_CoCo_2_Sepref(x) = T_Sep_save;
            box_CoCo_6_Sepref(x) = E_Sep_save;
            box_CoCo_reuse_QBRSp(x, cc);
            box_CoCo_QBRSuref(x) = QBRSu_save;
        }
    }
    
    box_list_clear(subboxes);
    
}

/* returns 1 if box has empty intersection with all boxes in l */
/* returns 0 otherwise */
// int _solver_CoCo_acb_is_separated_from_acb_list( acb_t box, acb_list_t l ){
//     int res = 1;
//     /* check separation with l */
//     acb_list_iterator ita = acb_list_begin(l);
//     while ( (ita != acb_list_end()) && (res==1) ) {
//         acb_ptr b = acb_list_elmt(ita);
//         if ( acb_overlaps( box, b ) ){
//             res = 0;
//         }
//         ita = acb_list_next(ita);
//     }
//     return res;
// }

/* returns 1 if box has empty intersection with cc*/
/* returns 0 otherwise */
int _solver_CoCo_acb_is_separated_from_CoCo( acb_t box, box_CoCo_t cc ){
    int res = 1;
    acb_t cBox;
    acb_init(cBox);
    box_CoCo_get_contBox(cBox, cc);
    if ( acb_overlaps( box, cBox ) ) {
        box_list_iterator itb = box_list_begin( box_CoCo_boxesref( cc ) );
        while ( (itb != box_list_end()) && (res==1) ) {
            box_get_acb(cBox, box_list_elmt(itb));
            if ( acb_overlaps( box, cBox ) ){
                res = 0;
            }
            itb = box_CoCo_list_next(itb);
        }
    }
    acb_clear(cBox);
    return res;
}

/* returns 1 if box has empty intersection with all cc's in l*/
/* returns 0 otherwise */
int _solver_CoCo_acb_is_separated_from_CoCo_list( acb_t box, box_CoCo_list_t l){
    int res = 1;
    acb_t cBox;
    acb_init(cBox);
    /* check separation with l */
    box_CoCo_list_iterator it = box_CoCo_list_begin(l);
    while ( (it != box_CoCo_list_end()) && (res==1) ) {
        
        if ( box_CoCo_w_encref(box_CoCo_list_elmt(it)) && (box_CoCo_get_nbBoxes(box_CoCo_list_elmt(it))==0) ) {
            res = (acb_overlaps( box, box_CoCo_encloref(box_CoCo_list_elmt(it)) )==0);
            it = box_CoCo_list_next(it);
            continue;
            /* otherwise check separation with boxes of the cc */
        }
        
        box_CoCo_get_contBox(cBox, box_CoCo_list_elmt(it));
        if ( acb_overlaps( box, cBox ) ) {
            box_list_iterator itb = box_list_begin( box_CoCo_boxesref( box_CoCo_list_elmt(it) ) );
            while ( (itb != box_list_end()) && (res==1) ) {
                box_get_acb(cBox, box_list_elmt(itb));
                if ( acb_overlaps( box, cBox ) ){
                    res = 0;
                }
                itb = box_CoCo_list_next(itb);
            }
        }
        it = box_CoCo_list_next(it);
    }
    acb_clear(cBox);
    return res;
}

/* returns 1 if inflation times unit box contains b */
/* returns 0 otherwise */
int _acb_is_inside_unit_inflated ( const acb_t b, double inflation ) {
    acb_t unit_inflated;
    acb_init( unit_inflated );
    acb_zero( unit_inflated );
    mag_set_d( arb_radref( acb_realref( unit_inflated ) ), inflation );
    mag_set_d( arb_radref( acb_imagref( unit_inflated ) ), inflation );
    int res = acb_contains( unit_inflated, b );
    acb_clear( unit_inflated );
    return (res!=0);
}

/* returns 1 if the containing box of ccur has empty intersection with */
/*           all cc in mainQueue, outsideQueue and results */
/* returns 0 otherwise */
int _solver_CoCo_is_1separated( box_CoCo_t ccur, box_CoCo_list_t prepQueue,
                                                box_CoCo_list_t mainQueue, 
                                                box_CoCo_list_t outsideQueue,
                                                box_CoCo_list_t precQueue, 
                                                box_CoCo_list_t results){
    if ( box_CoCo_1_Sepref( ccur ) == 1 )
        return 1;
    
    int res = 1;
    acb_t containingBox;
    acb_init(containingBox);
    box_CoCo_get_contBox(containingBox, ccur);
    /* check if containingBox is within inflated unit box */
    if (_unitDisc)
        res = res && _acb_is_inside_unit_inflated( containingBox, _inflation );
    /* check if ccur is 1 separated from its conjugate */
    if ( _realCoeffs && box_CoCo_is_imaginary_positive(ccur) ) {
        arb_neg( acb_imagref( containingBox ), acb_imagref( containingBox ) );
        res = res && _solver_CoCo_acb_is_separated_from_CoCo( containingBox, ccur );
        arb_neg( acb_imagref( containingBox ), acb_imagref( containingBox ) );
    }
    /* check separation with prepQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, prepQueue );
    /* check separation with mainQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, mainQueue );
    /* check separation with outsideQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, outsideQueue );
    /* check separation with precQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, precQueue );
    /* check separation with results */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, results );
    
    acb_clear(containingBox);
    if (res==1)
        box_CoCo_1_Sepref( ccur ) = 1;
    
    return res;
}

/* returns 1 if 2 times the containing box of ccur has empty intersection with */
/*           all cc in mainQueue, outsideQueue and results */
/*           => 4/3 times the containing disc of cc contains no other roots than the ones in cc */
/* returns 0 otherwise */
int _solver_CoCo_is_2separated( box_CoCo_t ccur, box_CoCo_list_t prepQueue,
                                               box_CoCo_list_t mainQueue, 
                                               box_CoCo_list_t outsideQueue,
                                               box_CoCo_list_t precQueue, 
                                               box_CoCo_list_t results ){
    if ( box_CoCo_2_Sepref( ccur ) == 1 )
        return 1;
    
    int res = 1;
    acb_t containingBox;
    acb_init(containingBox);
    box_CoCo_get_contBox(containingBox, ccur);
    /* inflate by factor 2 */
    mag_mul_2exp_si( arb_radref(acb_realref(containingBox)), arb_radref(acb_realref(containingBox)), 1);
    mag_mul_2exp_si( arb_radref(acb_imagref(containingBox)), arb_radref(acb_imagref(containingBox)), 1);
    /* check if containingBox is within inflated unit box */
    if (_unitDisc)
        res = res && _acb_is_inside_unit_inflated( containingBox, _inflation );
    /* check if ccur is 2 separated from its conjugate */
    if ( _realCoeffs && box_CoCo_is_imaginary_positive(ccur) ) {
        arb_neg( acb_imagref( containingBox ), acb_imagref( containingBox ) );
        res = res && _solver_CoCo_acb_is_separated_from_CoCo( containingBox, ccur );
        arb_neg( acb_imagref( containingBox ), acb_imagref( containingBox ) );
    }
    /* check separation with prepQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, prepQueue );
    /* check separation with mainQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, mainQueue );
    /* check separation with outsideQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, outsideQueue );
    /* check separation with precQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, precQueue );
    /* check separation with results */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, results );
    
    acb_clear(containingBox);
    if (res==1)
        box_CoCo_2_Sepref( ccur ) = 1;
    
    return res;
}

/* returns 1 if 6 times the containing box of ccur has empty intersection with */
/*           all cc in mainQueue, outsideQueue and results */
/*           => 4 times the containing disc of cc contains no other roots than the ones in cc */
/* returns 0 otherwise */
int _solver_CoCo_is_6separated( box_CoCo_t ccur, box_CoCo_list_t prepQueue,
                                                box_CoCo_list_t mainQueue, 
                                                box_CoCo_list_t outsideQueue, 
                                                box_CoCo_list_t precQueue, 
                                                box_CoCo_list_t results ){
    if ( box_CoCo_6_Sepref( ccur ) == 1 )
        return 1;
    
    int res = 1;
    acb_t containingBox;
    acb_init(containingBox);
    box_CoCo_get_contBox(containingBox, ccur);
    /* inflate by factor 6 */
    mag_mul_ui( arb_radref(acb_realref(containingBox)), arb_radref(acb_realref(containingBox)), 6);
    mag_mul_ui( arb_radref(acb_imagref(containingBox)), arb_radref(acb_imagref(containingBox)), 6);
    /* check if containingBox is within inflated unit box */
    if (_unitDisc)
        res = res && _acb_is_inside_unit_inflated( containingBox, _inflation );
    /* check if ccur is 6 separated from its conjugate */
    if ( _realCoeffs && box_CoCo_is_imaginary_positive(ccur) ) {
        arb_neg( acb_imagref( containingBox ), acb_imagref( containingBox ) );
        res = res && _solver_CoCo_acb_is_separated_from_CoCo( containingBox, ccur);
        arb_neg( acb_imagref( containingBox ), acb_imagref( containingBox ) );
    }
    /* check separation with prepQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, prepQueue );
    /* check separation with mainQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, mainQueue );
    /* check separation with outsideQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, outsideQueue );
    /* check separation with precQueue */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, precQueue );
    /* check separation with results */
    res = res && _solver_CoCo_acb_is_separated_from_CoCo_list( containingBox, results );
    
    acb_clear(containingBox);
    if (res==1)
        box_CoCo_6_Sepref( ccur ) = 1;
    
    return res;
}

int _solver_CoCo_is_f_separated_from_zero( box_CoCo_t ccur, ulong f ){
    
    int res = 1;
    acb_t containingBox;
    acb_init(containingBox);
    box_CoCo_get_contBox(containingBox, ccur);
    /* inflate by factor f */
    mag_mul_ui( arb_radref(acb_realref(containingBox)), arb_radref(acb_realref(containingBox)), f);
    mag_mul_ui( arb_radref(acb_imagref(containingBox)), arb_radref(acb_imagref(containingBox)), f);
    res = res && (!acb_contains_zero(containingBox));
    acb_clear(containingBox);
    
    return res;
}

slong _solver_CoCo_count_nb_roots( box_CoCo_t ccur, pw_polynomial_t cache ){
    
    if ( box_CoCo_nbRooref(ccur) > -1 )
        return box_CoCo_nbRooref(ccur);
    
    slong prec = PWPOLY_DEFAULT_PREC;
    slong res=0;

    res = tstar_box_CoCo( &prec, ccur, cache, pw_polynomial_maxNbRref(cache) );
    box_CoCo_Mprecref(ccur) = prec;
    
    if (res > -1)
        box_CoCo_nbRooref(ccur) = res;
    
    return res;
}

/* returns 1 if 2*radius(containingbox(ccur)) <= 2^(log2_eps) */
/* otherwise returns 0 */
// int _solver_CoCo_width_less_eps_acb( const acb_t box, slong log2_eps, int goal ) {
//     mag_t radius;
//     mag_init(radius);
//     int res=1;
//     mag_max(radius, arb_radref(acb_realref(box)), arb_radref(acb_imagref(box)));
//     res = res && (mag_cmp_2exp_si(radius, log2_eps-1)<=0);
//     mag_clear(radius);
//     return res;
// }

int _solver_CoCo_width_less_eps( box_CoCo_t ccur, slong log2_eps, int goal ) {
    
    acb_t box;
    acb_init(box);
    box_CoCo_get_contBox(box, ccur);
    int res = pwpoly_width_less_eps( box, log2_eps, PW_GOAL_PREC_RELATIVE(goal) );
    acb_clear(box);
    return res;
}

void _solver_CoCo_prep_loop( box_CoCo_list_t prepQueue, 
                             box_CoCo_list_t mainQueue, 
                             box_CoCo_list_t outsQueue, 
                             box_CoCo_list_t precQueue,
                             pw_polynomial_t cache,
                             slong maxdepth /* either -1 or positive, -1 meaning +infinity */
                             PW_VERBOSE_ARGU(verbose)){
    
    box_CoCo_ptr ccur;
    
    box_CoCo_list_t subccs;
    box_CoCo_list_init(subccs);
    
    acb_t box;
    acb_init(box);
// #ifndef PW_SILENT    
//     int level = 2;
// #endif    
    while (!box_CoCo_list_is_empty(prepQueue)) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif
        
        int outsideUnitDisk=0, insideInflatedUnit=0, outsideDomain=0, insideInflatedDomain=0;
        
        /* get connected components with boxes with largest radii */
        ccur = box_CoCo_list_pop( prepQueue );
        
        if ( box_CoCo_is_deeper_than_slong( ccur, maxdepth ) ){
            /* push bcur in front of precQueue and continue */
            box_CoCo_list_push_front(precQueue, ccur);
            continue;
        }
        
        if (box_list_get_size( box_CoCo_boxesref(ccur) ) > 9*pw_polynomial_degree(cache) ) {
            /* push bcur in front of precQueue and continue */
            box_CoCo_list_push_front(precQueue, ccur);
            continue;
        }
        
        box_CoCo_get_contBox(box, ccur);
        /* check if ccur is completely outside the unit disc */
        outsideUnitDisk = _unitDisc && box_CoCo_is_outside_unit_disc ( ccur );
        
        /* check if ccur is completely outside the domain */
        if (!outsideUnitDisk)
            outsideDomain = (_domain!=NULL) && domain_is_box_CoCo_outside_domain( _domain, ccur );
        
        if ( outsideUnitDisk || outsideDomain )
            box_CoCo_list_insert_sorted( outsQueue, ccur);
        else {
            /* check if ccur is strictly whithin inflation*B(0,1) */
            insideInflatedUnit= (_unitDisc==0) || box_CoCo_is_strictly_in_inflated_unit(ccur, _inflation);
            /* check if ccur is strictly whithin inflated domain */
            insideInflatedDomain= (_domain==NULL) || domain_is_box_CoCo_inside_inflated_domain( _domain, ccur );
            
            if ( insideInflatedUnit && insideInflatedDomain ) /* push in main queue */
                box_CoCo_list_insert_sorted( mainQueue, ccur);
            else { /* quadrisect */
                _quadrisect_box_CoCo ( subccs, ccur, cache PW_VERBOSE_CALL(verbose)  );
                while (!box_CoCo_list_is_empty(subccs))
                    box_CoCo_list_insert_sorted(prepQueue, box_CoCo_list_pop(subccs));
                box_CoCo_clear(ccur);
                pwpoly_free(ccur);
            }
        }
    }
    
    box_CoCo_list_clear(subccs);
    acb_clear(box);
}

/* returns the number of roots with mult. in results */
slong _solver_CoCo_main_loop( box_CoCo_list_t results,
                              box_CoCo_list_t prepQueue, // not used, but here for arguments of separation functions
                              box_CoCo_list_t mainQueue,
                              box_CoCo_list_t outsQueue,
                              box_CoCo_list_t precQueue,
                              pw_polynomial_t cache, 
                              slong log2_eps,
                              int goal, slong goalNbRoots,
                              slong maxdepth, // either >=0 or -1, meaning +inf 
                              int separated_from_zero//if 1, then output box must be separated from zero
                               PW_VERBOSE_ARGU(verbose)){
#ifndef PW_SILENT     
    int level = 4;
#endif 
    box_CoCo_ptr ccur;
    
    acb_t box;
    acb_init(box);
    /* get total multiplicity of roots in result */
    slong nbRootsInResults = 0;
    box_CoCo_list_iterator itres = box_CoCo_list_begin(results);
    while( itres != box_CoCo_list_end() ) {
        nbRootsInResults += box_CoCo_nbRooref( box_CoCo_list_elmt(itres) );
        itres = box_CoCo_list_next( itres );
    }
    
    /* cap goalNbRoots */
    slong len = pw_polynomial_length(cache);
    if (goalNbRoots<0)
        goalNbRoots = len-1;
    else
        goalNbRoots = PWPOLY_MIN( goalNbRoots, len-1 );
    
    slong precQBR = PWPOLY_DEFAULT_PREC;
    while (precQBR < -log2_eps)
        precQBR*=2;
    
    while ( (!box_CoCo_list_is_empty(mainQueue))&&(nbRootsInResults<goalNbRoots) ) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif
    
        int outsideUnitDisk=0, outsideDomain=0, compact=0, TwoSeparated=0, SixSeparated=0, SixSeparatedFromZero=0, widthLessEps=0;
        int intBoundariesOfDomain = 0;
        /* Real Coeff */
        box_CoCo_ptr ccurConjClo, ccurConj;
        ccurConjClo = NULL;
        ccurConj = NULL;
        
        /* get connected components with boxes with largest radii */
        ccur = box_CoCo_list_pop( mainQueue );
        
        slong maxNbRootsInCcur = pw_polynomial_maxNbRref(cache) - nbRootsInResults - box_CoCo_list_get_size(mainQueue);
        if ( box_CoCo_nbRooref(ccur) > -1 )
            maxNbRootsInCcur = box_CoCo_nbRooref(ccur);
#ifndef PW_SILENT         
        if (verbose >=level) {
            printf("--- CC of depth "); fmpz_print(box_CoCo_depthref(ccur)); 
            printf(" with %ld boxes\n", box_list_get_size(box_CoCo_boxesref(ccur)));
            printf("--- size of mainQueue: %ld\n", box_CoCo_list_get_size(mainQueue) );
            printf("--- max nb of roots:   %ld\n", maxNbRootsInCcur );
        }
#endif         
        if ( box_CoCo_is_deeper_than_slong( ccur, maxdepth ) ){
            /* push bcur in front of mainQueue and continue */
            box_CoCo_list_push_front(precQueue, ccur);
            continue;
        }
        
        if (box_CoCo_w_encref(ccur)==0) {
            if (box_list_get_size( box_CoCo_boxesref(ccur) ) > 9*maxNbRootsInCcur ) {
                /* push bcur in front of precQueue and continue */
                box_CoCo_list_push_front(precQueue, ccur);
                continue;
            }
        }
        
        if ( _realCoeffs && !box_CoCo_is_imaginary_positive(ccur) ){
            ccurConjClo = ( box_CoCo_ptr ) pwpoly_malloc (sizeof(box_CoCo));
            box_CoCo_init( ccurConjClo );
            box_CoCo_set_conjugate_closure(ccurConjClo, ccur);
            box_CoCo_clear(ccur);
            pwpoly_free(ccur);
            ccur = ccurConjClo;
#ifndef PW_SILENT 
            if (verbose >=level) {
                printf("------ is replaced by conjugate closure, new nb of boxes: %ld\n", box_list_get_size(box_CoCo_boxesref(ccur)));
            }
#endif             
        }
        
        box_CoCo_get_contBox(box, ccur);
#ifndef PW_SILENT 
        if (verbose >=level) {
            printf("--- containing box:"); acb_printd(box, 10); printf("\n");
        }
#endif         
        if ( PW_GOAL_MUST_APPROXIMATE(goal) )
            widthLessEps = _solver_CoCo_width_less_eps( ccur, log2_eps, goal );
        else
            widthLessEps = 1;
#ifndef PW_SILENT         
        if (verbose >=level) {
            printf("------ width less than eps : %d\n", widthLessEps );
        }
#endif         
        /* check if ccur is completely outside the unit disc */
        outsideUnitDisk = _unitDisc && box_CoCo_is_outside_unit_disc ( ccur );
        /* check if ccur is completely outside the domain */
        outsideDomain = (_domain!=NULL) && domain_is_box_CoCo_outside_domain( _domain, ccur );
#ifndef PW_SILENT         
        if (verbose >=level) {
            printf("------ is outside unit disc: %d\n", outsideUnitDisk );
            printf("------ is outside domain   : %d\n", outsideDomain );
        }
#endif 
        if ( outsideUnitDisk || outsideDomain ) {
            box_CoCo_list_insert_sorted( outsQueue, ccur);
#ifndef PW_SILENT 
            if (verbose >=level)
                printf("------ push in outside queue\n\n");
#endif 
            continue;
        } 

        TwoSeparated = _solver_CoCo_is_2separated( ccur, prepQueue, mainQueue, outsQueue, precQueue, results );
        /* here it is NOT necessary to check if it is also 2-separated from zero */
#ifndef PW_SILENT             
        if (verbose >=level) {
            printf("------ is 2 separated: %d\n", TwoSeparated );
        }
#endif            
        slong included = -1;
        
        if (TwoSeparated) {
            /* apply inclusion test to ccur */
            included = _solver_CoCo_count_nb_roots( ccur, cache );
#ifndef PW_SILENT 
            if (verbose >=level) {
                printf("------ is included by tstar: %ld\n", included );
            }
#endif                 
            SixSeparated = _solver_CoCo_is_6separated( ccur, prepQueue, mainQueue, outsQueue, precQueue, results );
            SixSeparatedFromZero = _solver_CoCo_is_f_separated_from_zero( ccur, 6 );
                
            compact = box_CoCo_is_compact(ccur);
#ifndef PW_SILENT 
            if (verbose >=level) {
                printf("------ is 6 separated: %d\n", SixSeparated );
                printf("------ is compact    : %d\n", compact );
            }
#endif             
        }
        
        /* in case where there are no roots on boundary, check if current cc is strictly inside the domain */
        intBoundariesOfDomain = (_strInDom && !domain_is_box_CoCo_inside_domain(_domain, ccur));
        

        if ( (included>=1) && (SixSeparated==1) && 
             ( (compact==0) || ((PW_GOAL_MUST_APPROXIMATE(goal))&&(widthLessEps==0))
                            || ((PW_GOAL_MUST_ISOLATE(goal))&&(included>1))
                            || (separated_from_zero&&(SixSeparatedFromZero==0))
                            || intBoundariesOfDomain
            ) ) {
#ifdef PW_COCO_PROFILE
            clock_t start_QBR = clock();
#endif
//             slong prec = PWPOLY_DEFAULT_PREC;
            slong prec = precQBR;
#ifndef PW_SILENT 
            if (verbose >=level) {
                printf("------ run QBR with prec: %ld, speed: 2^", prec );
                fmpz_print( box_CoCo_QBRSpref(ccur) ); printf("\n");
            }
#endif 
            int resQBR = QBR_box_CoCo( &prec, &ccur, cache, log2_eps PW_VERBOSE_CALL(verbose) );
#ifndef PW_SILENT 
            if (verbose >=level) 
                printf("------ res QBR: %d, prec: %ld\n", resQBR, prec );
#endif 
#ifdef PW_COCO_PROFILE
            clicks_in_QBR += clock()-start_QBR;
#endif
            if (resQBR==QBR_SUCCESS) {
                box_CoCo_increase_QBRSp(ccur);
#ifndef PW_SILENT 
                if (verbose >=level) {
                    box_CoCo_get_contBox(box, ccur);
                    widthLessEps = _solver_CoCo_width_less_eps( ccur, log2_eps, goal );
                    SixSeparated = _solver_CoCo_is_6separated( ccur, prepQueue, mainQueue, outsQueue, precQueue, results );
                    printf("------ new CC of depth"); fmpz_print(box_CoCo_depthref(ccur)); 
                    printf(" with %ld boxes\n", box_list_get_size(box_CoCo_boxesref(ccur)));
                    printf("------ QBR speed: 2^"); fmpz_print( box_CoCo_QBRSpref(ccur) ); printf("\n");
//                     printf("--- QBR success: %d\n", box_CoCo_QBRSuref(ccur) );
                    printf("------ containing box:"); acb_printd(box, 10); printf("\n");
                    
                    printf("------ width less than eps : %d\n", widthLessEps );
                    printf("------ is 6 separated      : %d\n", SixSeparated );
                    printf("------ push in main queue\n\n");
                }
#endif 
                /* insert ccur in mainloop */
                if ( (goalNbRoots < len-1)&&(included==1) )
                    /* the cc contains one root -> refine it until one gets a solution */
                    box_CoCo_list_push_front (mainQueue, ccur);
                else
                    box_CoCo_list_insert_sorted(mainQueue, ccur);
                continue;
                        
            } else {
                if ( !box_CoCo_isminima_QBRSp(ccur) ) {
                    box_CoCo_decrease_QBRSp(ccur);
                    if ( (goalNbRoots < len-1)&&(included==1) )
                        /* the cc contains one root -> refine it until one gets a solution */
                        box_CoCo_list_push_front (mainQueue, ccur);
                    else
                        box_CoCo_list_insert_sorted(mainQueue, ccur);
                    continue;
                }
            }
        }
        
        if (included==0) {
#ifndef PW_SILENT 
            if (verbose >=level) {
                printf("------ is excluded by tstar counting test (included = %ld)\n", included );
            }
#endif 
                /* do nothing: the cc will be discarded */
         } else if ( (included>=1) && (SixSeparated==1) && (compact==1) 
                && ( (PW_GOAL_MUST_APPROXIMATE(goal)==0)||(widthLessEps==1) )
                && ( (PW_GOAL_MUST_ISOLATE(goal)==0)||(included==1) )
                && ( (separated_from_zero==0)||(SixSeparatedFromZero==1) )
                && (intBoundariesOfDomain==0)    ) {
             /* push box in results */
             box_CoCo_list_push( results, ccur );
             nbRootsInResults += included;
#ifndef PW_SILENT 
             if (verbose >=level) {
                 printf("------ approximated: push box in results (%ld/%ld)!\n\n", nbRootsInResults, goalNbRoots);
             }
#endif                 
             /* push conjugate */
             if (_realCoeffs && box_CoCo_is_imaginary_positive(ccur) ) {
                 ccurConj = (box_CoCo_ptr) pwpoly_malloc ( sizeof(box_CoCo) );
                 box_CoCo_init( ccurConj );
                 box_CoCo_set_conjugate( ccurConj, ccur );
                 box_CoCo_list_push( results, ccurConj );
                 nbRootsInResults += included;
#ifndef PW_SILENT 
                 if (verbose >=level)
                     printf("------ push conjugate in results (%ld/%ld)\n\n", nbRootsInResults, goalNbRoots);
#endif 
            }
                
            continue;
                
        } else {
            /* subdivide ccur and push obtained CC in main queue*/
#ifndef PW_SILENT 
            if (verbose >=level) {
                printf("--- subdivide CC of depth"); fmpz_print(box_CoCo_depthref(ccur)); 
                printf(" with %ld boxes\n", box_list_get_size(box_CoCo_boxesref(ccur)));
            }
#endif 
            box_CoCo_list_t subccs;
            box_CoCo_list_init(subccs);
            _quadrisect_box_CoCo ( subccs, ccur, cache PW_VERBOSE_CALL(verbose)  );
            while (!box_CoCo_list_is_empty(subccs))
                box_CoCo_list_insert_sorted(mainQueue, box_CoCo_list_pop(subccs));
            box_CoCo_list_clear(subccs);
        }
#ifndef PW_SILENT 
        if (verbose >=level) {
            printf("--- delete CC of depth"); fmpz_print(box_CoCo_depthref(ccur)); 
            printf(" with %ld boxes\n", box_list_get_size(box_CoCo_boxesref(ccur)));
            printf("\n\n");
        }
#endif 
        /*get rid of current CC */
        box_CoCo_clear(ccur);
        pwpoly_free(ccur);
    
    }
#ifndef PW_SILENT     
    if (verbose >=level) {
        printf("solver_CoCo_main_loop_unit_disc end, nb roots in result: %ld/%ld\n", nbRootsInResults, goalNbRoots );
    }
#endif     
    acb_clear(box);
    
    return nbRootsInResults;
}

slong _solver_CoCo_solve_unit_disc_cache(  box_CoCo_list_ptr roots, box_CoCo_list_ptr unresolved,
                                          pw_polynomial_t cache, slong log2_eps, int goal,
                                          slong maxdepth, // either >=0 or -1, meaning +inf
                                           slong nbRootsInUnitDisc PW_VERBOSE_ARGU(verbose)){    
#ifndef PW_SILENT     
    int level = 3;
#endif     
    
    box_CoCo_ptr cc;
    if (nbRootsInUnitDisc >=1) {/* there are no roots on the unit circle */
        cc = box_CoCo_unit_box();
        _inflation  = 1.0;
    } else {
        /* set initial cc as cc with 4 boxes and containing box is 2*B(0,1) */
        cc = box_CoCo_initial_CoCo(0);
    }
    /* initialize queues */
    box_CoCo_list_t prepQueue, outsQueue, precQueue;
    box_CoCo_list_init(prepQueue);
    box_CoCo_list_init(outsQueue);
    box_CoCo_list_init(precQueue);
    box_CoCo_list_push(prepQueue, cc);
    _solver_CoCo_prep_loop( prepQueue, unresolved, outsQueue, precQueue, cache, maxdepth PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT 
    if ( verbose >=level) {
        printf(" after prep loop: size of prepQueue: %ld\n", box_CoCo_list_get_size(prepQueue) );
        printf("                  size of mainQueue: %ld\n", box_CoCo_list_get_size(unresolved) );
        printf("                  size of outsQueue: %ld\n", box_CoCo_list_get_size(outsQueue) );
        printf("                  size of precQueue: %ld\n", box_CoCo_list_get_size(precQueue) );
    }
#endif 
    slong nbRoots = _solver_CoCo_main_loop( roots, prepQueue, unresolved, outsQueue, precQueue, cache, log2_eps, goal, -1, maxdepth, 0 PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT 
    if ( verbose >=level) {
        printf(" after main loop: size of prepQueue: %ld\n", box_CoCo_list_get_size(prepQueue) );
        printf("                  size of mainQueue: %ld\n", box_CoCo_list_get_size(unresolved) );
        printf("                  size of outsQueue: %ld\n", box_CoCo_list_get_size(outsQueue) );
        printf("                  size of precQueue: %ld\n", box_CoCo_list_get_size(precQueue) );
        printf("                  size of roots    : %ld\n", box_CoCo_list_get_size(roots) );
        printf("                  numb of roots    : %ld\n", nbRoots );
    }
#endif         
    /* move ccs of precQueue in unresolved */
    while ( !(box_CoCo_list_is_empty(precQueue) ) )
        box_CoCo_list_push( unresolved, box_CoCo_list_pop(precQueue) );
    
    box_CoCo_list_clear(prepQueue);
    box_CoCo_list_clear(outsQueue);
    box_CoCo_list_clear(precQueue);
    
    return nbRoots;
}

slong solver_CoCo_approximate_unit_disc_pw_polynomial( acb_ptr roots, slong * mults, pw_polynomial_t poly,
                                                       slong log2_eps, int realCoeffs, 
                                                       slong nbRootsInUnitDisc PW_VERBOSE_ARGU(verbose)) {
    
    _realCoeffs = realCoeffs;
    _inflation  = 1.2;
    _unitDisc   = 1;
    _domain     = NULL;
    _tstar_round=0;
    _tstar_antic=0;
    _strInDom=0;
    
    box_CoCo_list_t resolved, unresolved;
    box_CoCo_list_init(resolved);
    box_CoCo_list_init(unresolved);
    
    _solver_CoCo_solve_unit_disc_cache( resolved, unresolved, poly, log2_eps, PW_GOAL_APPROXIMATE_ABS, LONG_MAX, nbRootsInUnitDisc PW_VERBOSE_CALL(verbose)  );
    
    /* unresolved should be empty */
    if ( !box_CoCo_list_is_empty(unresolved) ) {
    }
    slong res = 0;
    /* convert each CoCo in resolved into a root */
    box_CoCo_list_iterator it = box_CoCo_list_begin(resolved);
    while ( it!=box_CoCo_list_end() ) {
        box_CoCo_get_contBox(roots+res, box_CoCo_list_elmt(it));
        mults[res] = box_CoCo_nbRooref( box_CoCo_list_elmt(it));
        res++;
        it = box_CoCo_list_next(it);
    }
    
    box_CoCo_list_clear(unresolved);
    box_CoCo_list_clear(resolved);
    
    return res;
}

slong solver_CoCo_approximate_unit_disc_exact_acb_poly( acb_ptr roots, slong * mults, 
                                                        acb_poly_ptr p, slong log2_eps, int realCoeffs, 
                                                        slong nbRootsInUnitDisc PW_VERBOSE_ARGU(verbose)) {
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb( poly, p);
    
    slong res = solver_CoCo_approximate_unit_disc_pw_polynomial( roots, mults, poly, log2_eps, realCoeffs, nbRootsInUnitDisc PW_VERBOSE_CALL(verbose)  );
    
    pw_polynomial_clear(poly);
    
    return res;
}

slong solver_CoCo_approximate_unit_disc_2fmpq_poly( acb_ptr roots, slong * mults, 
                                                    const fmpq_poly_t re, const fmpq_poly_t im, 
                                                    slong log2_eps, int realCoeffs, 
                                                    slong nbRootsInUnitDisc PW_VERBOSE_ARGU(verbose)){
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq ( poly, re, im);
    
    slong res = solver_CoCo_approximate_unit_disc_pw_polynomial( roots, mults, poly, log2_eps, realCoeffs, nbRootsInUnitDisc PW_VERBOSE_CALL(verbose)  );
    
    pw_polynomial_clear(poly);
    
    return res;
}

// /* assume there are no root on the disc D(0, 2^scale) */
slong _solver_CoCo_solve_unit_disc_scaled_cache(  box_CoCo_list_ptr roots, box_CoCo_list_ptr unresolved,
                                                  const acb_ptr InRoots, slong nbInRoots,
                                                  pw_polynomial_t cache, slong log2_eps, int goal, domain_t dom, slong goalNbRoots,
                                                   slong maxdepth, // either >=0 or -1, meaning +inf
                                                   slong scale,
                                                  int separated_from_zero PW_VERBOSE_ARGU(verbose)){    
#ifndef PW_SILENT     
    int level = 1;
#endif     
    
    box_CoCo_ptr cc = box_CoCo_unit_box_scaled(scale, InRoots, nbInRoots);
    _realCoeffs = pw_polynomial_is_real(cache);
    _inflation  = 1.2;
    if (scale==0)
        _unitDisc = 1;
    else
        _unitDisc = 0;
    if (domain_is_C(dom))
        _domain     = NULL;
    else {
        _domain = dom+0;
        _realCoeffs = _realCoeffs&&domain_is_symetric_real_line(dom);
    }
    _tstar_round=1;
    _tstar_antic=1;
    _strInDom=0;
    
    /* initialize queues */
    box_CoCo_list_t prepQueue, outsQueue, precQueue;
    box_CoCo_list_init(prepQueue);
    box_CoCo_list_init(outsQueue);
    box_CoCo_list_init(precQueue);
    box_CoCo_list_push(prepQueue, cc);
    _solver_CoCo_prep_loop( prepQueue, unresolved, outsQueue, precQueue, cache, maxdepth PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT 
    if ( verbose >=level) {
        printf("_solver_CoCo_solve_unit_disc_scaled_cache:\n");
        printf(" after prep loop: size of prepQueue: %ld\n", box_CoCo_list_get_size(prepQueue) );
        printf("                  size of mainQueue: %ld\n", box_CoCo_list_get_size(unresolved) );
        printf("                  size of outsQueue: %ld\n", box_CoCo_list_get_size(outsQueue) );
        printf("                  size of precQueue: %ld\n", box_CoCo_list_get_size(precQueue) );
    }
#endif 
    slong nbRoots = _solver_CoCo_main_loop( roots, prepQueue, unresolved, outsQueue, precQueue, cache, log2_eps, goal, goalNbRoots, maxdepth, separated_from_zero PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT 
    if ( verbose >=level) {
        printf("_solver_CoCo_solve_unit_disc_scaled_cache:\n");
        printf(" after main loop: size of prepQueue: %ld\n", box_CoCo_list_get_size(prepQueue) );
        printf("                  size of mainQueue: %ld\n", box_CoCo_list_get_size(unresolved) );
        printf("                  size of outsQueue: %ld\n", box_CoCo_list_get_size(outsQueue) );
        printf("                  size of precQueue: %ld\n", box_CoCo_list_get_size(precQueue) );
        printf("                  size of roots    : %ld\n", box_CoCo_list_get_size(roots) );
        printf("                  numb of roots    : %ld\n", nbRoots );
    }
#endif         
    /* move ccs of precQueue in unresolved */
    while ( !(box_CoCo_list_is_empty(precQueue) ) )
        box_CoCo_list_push( unresolved, box_CoCo_list_pop(precQueue) );
    
    box_CoCo_list_clear(prepQueue);
    box_CoCo_list_clear(outsQueue);
    box_CoCo_list_clear(precQueue);
    
    return nbRoots;
}

slong solver_CoCo_solve_pw_polynomial( acb_ptr roots, slong * mults, slong nbInRoots,
                                       pw_polynomial_t poly, 
                                       slong log2_eps, int goal, domain_t dom, slong goalNbRoots PW_VERBOSE_ARGU(verbose)){
#ifndef PW_SILENT    
    int level = 1;
#endif    
    slong multOfZero = 0;
    if ( PW_GOAL_MUST_APPROXIMATE(goal) && PW_GOAL_PREC_RELATIVE(goal) ) {
        multOfZero = pw_polynomial_remove_root_zero(poly);
        if (multOfZero>0) {
            acb_zero( roots + 0 );
            mults[0] = multOfZero;
        }
        multOfZero = (multOfZero>0 ? 1 : 0);
    }
    
    slong scale = pw_polynomial_get_log2_rootBound ( poly );
#ifndef PW_SILENT    
    if (verbose >=level) {
        printf("solver_CoCo_solve_pw_polynomial: nbInRoots: %ld, log2_eps: %ld, realCoeffs: %d, rootBound: 2^(%ld)\n",
                nbInRoots, log2_eps, _realCoeffs, scale);
    }
#endif    
    box_CoCo_list_t resolved, unresolved;
    box_CoCo_list_init(resolved);
    box_CoCo_list_init(unresolved);
    
    int separated_from_zero = pw_polynomial_multZrref(poly)>0;
    _solver_CoCo_solve_unit_disc_scaled_cache( resolved, unresolved, roots, nbInRoots, poly, log2_eps, goal, dom, goalNbRoots, LONG_MAX, scale, separated_from_zero PW_VERBOSE_CALL(verbose)  );
    
    /* unresolved should be empty */
    if ( !box_CoCo_list_is_empty(unresolved) ) {
    }
    slong res = 0;
    /* convert each CoCo in resolved into a root, decide realness */
    _realCoeffs = pw_polynomial_is_real(poly);
    box_CoCo_list_iterator it = box_CoCo_list_begin(resolved);
    while ( it!=box_CoCo_list_end() ) {
        box_CoCo_get_contBox(roots+(multOfZero+res), box_CoCo_list_elmt(it));
        mults[multOfZero+res] = box_CoCo_nbRooref( box_CoCo_list_elmt(it));
        if (_realCoeffs && arb_contains_zero( acb_imagref(roots+(multOfZero+res)) ) && (mults[multOfZero+res]==1) )
            arb_zero( acb_imagref(roots+(multOfZero+res)) );
        res++;
        it = box_CoCo_list_next(it);
    }
    
    box_CoCo_list_clear(unresolved);
    box_CoCo_list_clear(resolved);
    
    return multOfZero+res;
}

slong solver_CoCo_solve_2fmpq_poly( acb_ptr roots, slong * mults, slong nbInRoots,
                                          const fmpq_poly_t re, const fmpq_poly_t im, 
                                          slong log2_eps, int goal, domain_t dom, slong goalNbRoots PW_VERBOSE_ARGU(verbose)){
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_fmpq ( poly, re, im);
    
    slong res = solver_CoCo_solve_pw_polynomial( roots, mults, nbInRoots, poly, log2_eps, goal, dom, goalNbRoots PW_VERBOSE_CALL(verbose)  );
    
    pw_polynomial_clear(poly);
    
    return res;
}

slong solver_CoCo_solve_exact_acb_poly( acb_ptr roots, slong * mults, slong nbInRoots, 
                                              const acb_poly_t p,  
                                              slong log2_eps, int goal, domain_t dom, slong goalNbRoots PW_VERBOSE_ARGU(verbose)){
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb ( poly, p );
    
    slong res = solver_CoCo_solve_pw_polynomial( roots, mults, nbInRoots, poly, log2_eps, goal, dom, goalNbRoots PW_VERBOSE_CALL(verbose)  );
    
    pw_polynomial_clear(poly);
    
    return res;
}

/* Pre conditions: roots is a list of lenToRefine SQUARE boxes */
/*                 for each box B(ci,ri) in roots, the disc D(ci,ri) is a natural isolator of mults[i] roots */
/*                 let N be the sum of mult[i] for i=0 to lenToRefine-1; */
/*                 assume roots and mults have enough room for lenToRefine items */
/*                 assume trailing zeros of poly have already been removed */
slong solver_CoCo_refine_pw_polynomial( acb_ptr roots, slong * mults, slong lenToRefine,
                                        pw_polynomial_t poly, 
                                        slong log2_eps, int goal, const domain_t dom, slong goalNbRoots,
                                        int useRealCoeffs PW_VERBOSE_ARGU(verbose)) {
#ifndef PW_SILENT
    int level = 2;
#endif
    _realCoeffs = useRealCoeffs&&pw_polynomial_is_real(poly);
    _unitDisc   = 0;
    int separated_from_zero = (int)pw_polynomial_multZrref(poly);
    if (domain_is_C(dom))
        _domain     = NULL;
    else {
        _domain = (domain_ptr)dom;
        _realCoeffs = _realCoeffs&&domain_is_symetric_real_line(dom);
    }
    
    
#ifndef PW_SILENT    
    if (verbose >=level) {
        printf("solver_CoCo_refine_pw_polynomial begin: realCoeffs: %d, lenToRefine: %ld, goalNbRoots: %ld, domain:",
                _realCoeffs, lenToRefine, goalNbRoots);
        domain_print(dom);
        printf("\n");
    }
#endif  
    _tstar_round=0;
    _tstar_antic=0;
    _strInDom=0;
    /* initialize queues */
    box_CoCo_list_t resolved, unresolved, prepQueue, outsQueue, precQueue;
    box_CoCo_list_init(resolved);
    box_CoCo_list_init(unresolved);
    box_CoCo_list_init(prepQueue);
    box_CoCo_list_init(outsQueue);
    box_CoCo_list_init(precQueue);
    
    box_list_t boxes, subboxes;
    box_list_init(boxes);
    box_list_init(subboxes);
    acb_t inflatedRoot, contBox;
    acb_init(inflatedRoot);
    acb_init(contBox);
    
    /* transform each root of the input list in a connected component */
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    for (slong i=0; i<lenToRefine; i++) {
        
        if ( domain_is_C(dom) || domain_acb_intersect_domain( dom, roots+i ) ) {
            /* if imaginary part is zero, transform it into a square box */
            if ( arb_is_zero( acb_imagref( roots+i ) ) )
                mag_set( arb_radref(acb_imagref( roots+i )), arb_radref(acb_realref( roots+i )) );
        
            if ( (_realCoeffs==0) || (arb_is_negative(acb_imagref(roots+i))==0) ) {
                box_CoCo_ptr cur = (box_CoCo_ptr) pwpoly_malloc (sizeof(box_CoCo));
            
                /* check if must be moved in result */
                int sizeOK = (PW_GOAL_MUST_APPROXIMATE(goal)==0)
                            ||pwpoly_width_less_eps( roots+i, log2_eps, PW_GOAL_PREC_RELATIVE(goal) );
                if ( ( (PW_GOAL_MUST_ISOLATE(goal)==0)||(mults[i]==1) ) && sizeOK ){
                    /* create a connected component from enclosure and push it in results */
                    box_CoCo_init_set_from_enclosure ( cur, roots+i, mults[i], 1, 1, 0, PWPOLY_DEFAULT_PREC );
                    box_CoCo_list_push(resolved, cur);
                    if ( (_realCoeffs==1)&&arb_is_positive(acb_imagref(roots+i)) ) {
                        /* also push the conjugate */
                        cur = (box_CoCo_ptr) pwpoly_malloc (sizeof(box_CoCo));
                        box_CoCo_init_set_from_enclosure ( cur, roots+i, mults[i], 1, 1, 0, PWPOLY_DEFAULT_PREC );
                        acb_conj( box_CoCo_encloref(cur), roots+i );
                        box_CoCo_list_push(resolved, cur);
                    }
                    continue;
                }
            
                /* get a CC with at most 9 boxes included in 3*(roots+i) */
                box_CoCo_init_set_from_acb_slong_int( cur, roots+i, mults[i], 1, 0, 0, PWPOLY_DEFAULT_PREC );
                /* subdivide the CC until it is included in 2*(roots+i) to ensure separation */
                acb_set(inflatedRoot, roots+i);
                mag_mul_2exp_si( arb_radref( acb_realref( inflatedRoot ) ), arb_radref( acb_realref( inflatedRoot ) ), 1 );
                mag_mul_2exp_si( arb_radref( acb_imagref( inflatedRoot ) ), arb_radref( acb_imagref( inflatedRoot ) ), 1 );
                box_CoCo_get_contBox( contBox, cur );
                while ( !acb_contains(inflatedRoot, contBox) ) {
                    box_list_quadrisect( subboxes, box_CoCo_boxesref(cur) );
                    while ( ! box_list_is_empty(subboxes) ) {
                        box_ptr bcur = box_list_pop(subboxes);
                        box_get_acb(contBox, bcur);
                        if (acb_overlaps(contBox,roots+i))
                            box_CoCo_insert_box( cur, bcur );
                        else {
                            box_clear(bcur);
                            pwpoly_free(bcur);
                        }   
                    }
                    box_CoCo_get_contBox( contBox, cur );
                }
                box_CoCo_nbRooref(cur) = mults[i];
                /* push the CC in mainqueue */
                box_CoCo_list_push(unresolved, cur);
#ifndef PW_SILENT            
                if (verbose >=level) {
                    printf("solver_CoCo_refine_pw_polynomial begin: push cluster in main queue:\n");
                    box_CoCo_print(cur);
                    printf("\n");
                }
#endif
            }
        } else { /* push it in outsQueue */
            /* create a connected component from enclosure and push it in outsQueue */
            box_CoCo_ptr cur = (box_CoCo_ptr) pwpoly_malloc (sizeof(box_CoCo));
            box_CoCo_init_set_from_enclosure ( cur, roots+i, mults[i], 1, 1, 0, PWPOLY_DEFAULT_PREC );
            box_CoCo_list_push(outsQueue, cur);
        }
    }
    acb_clear(contBox);
    acb_clear(inflatedRoot);
    box_list_clear(subboxes);
    box_list_clear(boxes);
#ifndef PW_SILENT 
    if ( verbose >=level) {
        printf("solver_CoCo_refine_pw_polynomial:\n");
        printf(" before main loop: size of mainQueue: %ld\n", box_CoCo_list_get_size(unresolved) );
        printf("                   size of outsQueue: %ld\n", box_CoCo_list_get_size(outsQueue) );
        printf("                   size of precQueue: %ld\n", box_CoCo_list_get_size(precQueue) );
        printf("                   size of resolved : %ld\n", box_CoCo_list_get_size(resolved) );
    }
#endif
#ifdef PW_SILENT
                    _solver_CoCo_main_loop( resolved, prepQueue, unresolved, outsQueue, precQueue, poly, log2_eps, goal, goalNbRoots, -1, separated_from_zero PW_VERBOSE_CALL(verbose)  );
#else
    slong nbRoots = _solver_CoCo_main_loop( resolved, prepQueue, unresolved, outsQueue, precQueue, poly, log2_eps, goal, goalNbRoots, -1, separated_from_zero PW_VERBOSE_CALL(verbose)  ); 
    if ( verbose >=level) {
        printf("solver_CoCo_refine_pw_polynomial:\n");
        printf(" after main loop: size of prepQueue: %ld\n", box_CoCo_list_get_size(prepQueue) );
        printf("                  size of mainQueue: %ld\n", box_CoCo_list_get_size(unresolved) );
        printf("                  size of outsQueue: %ld\n", box_CoCo_list_get_size(outsQueue) );
        printf("                  size of precQueue: %ld\n", box_CoCo_list_get_size(precQueue) );
        printf("                  size of resolved : %ld\n", box_CoCo_list_get_size(resolved) );
        printf("                  numb of roots    : %ld\n", nbRoots );
    }
#endif 
    /* convert each CoCo in resolved into a root, decide realness */
    _realCoeffs = useRealCoeffs&&pw_polynomial_is_real(poly);
    slong res = 0;
    box_CoCo_list_iterator it = box_CoCo_list_begin(resolved);
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    while ( it!=box_CoCo_list_end() ) {
        box_CoCo_ptr ccur = box_CoCo_list_elmt(it);
        if ( box_CoCo_w_encref( ccur ) && (box_CoCo_get_nbBoxes(ccur)==0) ) {
            acb_set( roots+res, box_CoCo_encloref(ccur) );
            mults[res] = box_CoCo_nbRooref( ccur);
        } else {
            box_CoCo_get_contBox(roots+res, ccur);
            mults[res] = box_CoCo_nbRooref( ccur);
        }
        if (_realCoeffs && arb_contains_zero( acb_imagref(roots+res) ) && (mults[res]==1) )
            arb_zero( acb_imagref(roots+res) );
        res++;
        it = box_CoCo_list_next(it);
    }
    slong restemp=res;
    /* write ccs of outsqueue after the refined ones */
    it = box_CoCo_list_begin(outsQueue);
#ifdef PW_CHECK_INTERRUPT
    if (!pwpoly_interrupt(0))
#endif
    while ( it!=box_CoCo_list_end() ) {
        box_CoCo_ptr ccur = box_CoCo_list_elmt(it);
        if ( box_CoCo_w_encref( ccur ) && (box_CoCo_get_nbBoxes(ccur)==0) ) {
            acb_set( roots+restemp, box_CoCo_encloref(ccur) );
            mults[restemp] = box_CoCo_nbRooref( ccur);
        } else {
            box_CoCo_get_contBox(roots+restemp, ccur);
            mults[restemp] = box_CoCo_nbRooref( ccur);
        }
        if (_realCoeffs && arb_contains_zero( acb_imagref(roots+restemp) ) && (mults[restemp]==1) )
            arb_zero( acb_imagref(roots+restemp) );
        restemp++;
        it = box_CoCo_list_next(it);
    }
    
    box_CoCo_list_clear(precQueue);
    box_CoCo_list_clear(outsQueue);
    box_CoCo_list_clear(prepQueue);
    box_CoCo_list_clear(unresolved);
    box_CoCo_list_clear(resolved);
    
    return res;
}

/* Pre conditions: roots is a list of lenToRefine SQUARE boxes */
/*                 for each box B(ci,ri) in roots, the disc D(ci,ri) is a natural isolator of mults[i] roots */
/*                 let N be the sum of mult[i] for i=0 to lenToRefine-1; */
/*                 assume roots and mults have enough room for N items */
/*                 assume trailing zeros of poly have already been removed */
slong solver_CoCo_zoom_pw_polynomial( acb_t cluster, slong mult,
                                      pw_polynomial_t poly, 
                                      slong log2_eps, int goal PW_VERBOSE_ARGU(verbose)) {
#ifndef PW_SILENT
    int level = 2;
#endif
    _realCoeffs = 0;
    _unitDisc   = 0;
    _domain     = NULL;
    _tstar_round=0;
    _tstar_antic=0;
    _strInDom=0;
//     int separated_from_zero = (int)pw_polynomial_multZrref(poly);
#ifndef PW_SILENT    
    if (verbose >=level) {
        printf("solver_CoCo_zoom_pw_polynomial: realCoeffs: %d\n",
                _realCoeffs);
    }
#endif    
    
    box_list_t boxes, subboxes;
    box_list_init(boxes);
    box_list_init(subboxes);
    acb_t inflatedRoot, contBox;
    acb_init(inflatedRoot);
    acb_init(contBox);
    
    /* transform cluster in a connected component of boxes */
    box_CoCo_ptr cc = (box_CoCo_ptr) pwpoly_malloc (sizeof(box_CoCo));
    /* get a CC with at most 9 boxes included in 3*cluster */
    box_CoCo_init_set_from_acb_slong_int( cc, cluster, mult, 1, 1, 1, PWPOLY_DEFAULT_PREC );
    /* subdivide the CC until it is included in 2*cluster to ensure separation */
    acb_set(inflatedRoot, cluster);
    mag_mul_2exp_si( arb_radref( acb_realref( inflatedRoot ) ), arb_radref( acb_realref( inflatedRoot ) ), 1 );
    mag_mul_2exp_si( arb_radref( acb_imagref( inflatedRoot ) ), arb_radref( acb_imagref( inflatedRoot ) ), 1 );
    box_CoCo_get_contBox( contBox, cc );
    while ( !acb_contains(inflatedRoot, contBox) ) {
        box_list_quadrisect( subboxes, box_CoCo_boxesref(cc) );
        while ( ! box_list_is_empty(subboxes) ) {
            box_ptr bcur = box_list_pop(subboxes);
            box_get_acb(contBox, bcur);
            if (acb_overlaps(contBox,cluster))
                box_CoCo_insert_box( cc, bcur );
            else {
                box_clear(bcur);
                pwpoly_free(bcur);
            }   
        }
        box_CoCo_get_contBox( contBox, cc );
    }
#ifndef PW_SILENT    
    if (verbose >=level) {
        printf("solver_CoCo_zoom_pw_polynomial: cluster :\n");
        box_CoCo_print(cc);
        printf("\n");
    }
#endif    
    int stop = 0, widthLessEps=0, resQBR=0;
    slong prec = PWPOLY_DEFAULT_PREC;
    while (stop==0) {
        
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif        
        
#ifndef PW_SILENT        
        if (verbose >=level) {
                printf("------ run QBR with prec: %ld, speed: 2^", prec );
                fmpz_print( box_CoCo_QBRSpref(cc) ); printf("\n");
        }
#endif        
        resQBR = QBR_box_CoCo( &prec, &cc, poly, log2_eps PW_VERBOSE_CALL(verbose) );
#ifndef PW_SILENT        
        if (verbose >=level) 
            printf("------ res QBR: %d, prec: %ld\n", resQBR, prec );
#endif        
        if (resQBR==QBR_SUCCESS) {
            box_CoCo_increase_QBRSp(cc);
#ifndef PW_SILENT
            if (verbose >=level) {
                printf("------ new CC of depth"); fmpz_print(box_CoCo_depthref(cc)); 
                printf(" with %ld boxes\n", box_list_get_size(box_CoCo_boxesref(cc)));
                printf("------ QBR speed: 2^"); fmpz_print( box_CoCo_QBRSpref(cc) ); printf("\n");
            }
#endif
        } else {
            if ( !box_CoCo_isminima_QBRSp(cc) )
                box_CoCo_decrease_QBRSp(cc);
        }
        
        if ( PW_GOAL_MUST_APPROXIMATE(goal) )
            widthLessEps = _solver_CoCo_width_less_eps( cc, log2_eps, goal );
        else
            widthLessEps = 1;
        
        stop = ((PW_GOAL_MUST_ISOLATE(goal)==0)&&(widthLessEps==1))
             ||( (resQBR==QBR_FAILURE)&&(box_CoCo_isminima_QBRSp(cc)) );
#ifndef PW_SILENT             
        if (verbose >=level) {
            printf("------ resQBR : %d, speed is minimal: %d\n", resQBR, box_CoCo_isminima_QBRSp(cc) );
            printf("------ width less than eps : %d\n", widthLessEps );
            printf("------ stop : %d\n", stop );
        }
#endif        
    }
    
    box_CoCo_get_contBox( cluster, cc );

    acb_clear(contBox);
    acb_clear(inflatedRoot);
    box_list_clear(subboxes);
    box_list_clear(boxes);
    
    box_CoCo_clear(cc);
    pwpoly_free(cc);
    
    return prec;
}

/* assume domain is finite */
slong solver_CoCo_refine_domain_pw_polynomial( acb_ptr roots, slong * mults,
                                               pw_polynomial_t poly,
                                               int goal, slong log2_eps, const domain_t dom, slong multInDomain PW_VERBOSE_ARGU(verbose)) {
#ifndef PW_SILENT    
    int level = 1;
#endif
    _realCoeffs = 0;
    _unitDisc = 0;
    _tstar_round=1;
    _tstar_antic=1;
    _inflation=1.2;
    _domain = (domain_ptr)dom;
    _strInDom=1;
//     slong len = pw_polynomial_length(poly);
    int separated_from_zero = pw_polynomial_multZrref(poly)>0;

    /* get a square box_CoCo covering the domain */
    box_CoCo_ptr cc = (box_CoCo_ptr) pwpoly_malloc ( sizeof(box_CoCo) );
    box_CoCo_init(cc);
    box_list_t boxes;
    box_list_init(boxes);
    box_get_box_list_domainfmpq( boxes, domain_lreref(dom), 
                                        domain_ureref(dom), 
                                        domain_limref(dom), 
                                        domain_uimref(dom));
    while ( !box_list_is_empty(boxes) )
            box_CoCo_insert_box(cc, box_list_pop(boxes));
    
    box_list_clear(boxes);
    
    /* try to apply QBR iterations to cc */
    box_CoCo_nbRooref(cc) = multInDomain;
    int stop = 0, widthLessEps=0, resQBR=0;
    int atLeastOnesuccess = 0;
    slong prec = PWPOLY_DEFAULT_PREC;
    while (stop==0) {
#ifdef PW_CHECK_INTERRUPT
        if (pwpoly_interrupt(0))
            break;
#endif        
        
#ifndef PW_SILENT        
        if (verbose >=level) {
            printf("solver_CoCo_refine_domain_pw_polynomial: run QBR with prec: %ld, speed: 2^", prec );
            fmpz_print( box_CoCo_QBRSpref(cc) ); printf("\n");
        }
#endif
        resQBR = QBR_box_CoCo( &prec, &cc, poly, log2_eps PW_VERBOSE_CALL(verbose) );
#ifndef PW_SILENT        
        if (verbose >=level) 
            printf("------ res QBR: %d, prec: %ld\n", resQBR, prec );
#endif
        if (resQBR==QBR_SUCCESS) {
            box_CoCo_increase_QBRSp(cc);
            atLeastOnesuccess=1;
#ifndef PW_SILENT
            if (verbose >=level) {
                printf("------ new CC of depth"); fmpz_print(box_CoCo_depthref(cc)); 
                printf(" with %ld boxes\n", box_list_get_size(box_CoCo_boxesref(cc)));
                printf("------ QBR speed: 2^"); fmpz_print( box_CoCo_QBRSpref(cc) ); printf("\n");
            }
#endif
        } else {
            if ( !box_CoCo_isminima_QBRSp(cc) )
                box_CoCo_decrease_QBRSp(cc);
        }
        widthLessEps = _solver_CoCo_width_less_eps( cc, log2_eps, goal );
        stop = ((widthLessEps==1)||((resQBR==QBR_FAILURE)&&(box_CoCo_isminima_QBRSp(cc))) );
#ifndef PW_SILENT             
        if (verbose >=level) {
            printf("------ resQBR : %d, speed is minimal: %d\n", resQBR, box_CoCo_isminima_QBRSp(cc) );
            printf("------ width less than eps : %d\n", widthLessEps );
            printf("------ stop : %d, atLeastOnesuccess: %d\n", stop, atLeastOnesuccess );
        }
#endif 
    }
    
    box_CoCo_list_t resolved;
    box_CoCo_list_init(resolved);
    
    if (widthLessEps) {
        box_CoCo_list_push(resolved, cc);
    } else { /* subdivide cc */
        /* initialize queues */
        box_CoCo_list_t unresolved, prepQueue, outsQueue, precQueue;
        box_CoCo_list_init(unresolved);
        box_CoCo_list_init(prepQueue);
        box_CoCo_list_init(outsQueue);
        box_CoCo_list_init(precQueue);
        
        if (atLeastOnesuccess==0) {
            box_CoCo_nbRooref(cc) = -1;
            box_CoCo_list_push(prepQueue, cc);
            _solver_CoCo_prep_loop( prepQueue, unresolved, outsQueue, precQueue, poly, -1 PW_VERBOSE_CALL(verbose)  );
#ifndef PW_SILENT 
            if ( verbose >=level) {
                printf("solver_CoCo_refine_domain_pw_polynomial:\n");
                printf(" after prep loop: size of prepQueue: %ld\n", box_CoCo_list_get_size(prepQueue) );
                printf("                  size of mainQueue: %ld\n", box_CoCo_list_get_size(unresolved) );
                printf("                  size of outsQueue: %ld\n", box_CoCo_list_get_size(outsQueue) );
                printf("                  size of precQueue: %ld\n", box_CoCo_list_get_size(precQueue) );
            }
#endif
        } else {
            box_CoCo_list_push(unresolved, cc);
        }
#ifdef PW_SILENT
                        _solver_CoCo_main_loop( resolved, prepQueue, unresolved, outsQueue, precQueue, 
                                                poly, log2_eps, goal, multInDomain, -1, separated_from_zero PW_VERBOSE_CALL(verbose)  );
#else
        slong nbRoots = _solver_CoCo_main_loop( resolved, prepQueue, unresolved, outsQueue, precQueue, 
                                                poly, log2_eps, goal, multInDomain, -1, separated_from_zero PW_VERBOSE_CALL(verbose)  ); 
        if ( verbose >=level) {
            printf("solver_CoCo_refine_domain_pw_polynomial:\n");
            printf(" after main loop: size of prepQueue: %ld\n", box_CoCo_list_get_size(prepQueue) );
            printf("                  size of mainQueue: %ld\n", box_CoCo_list_get_size(unresolved) );
            printf("                  size of outsQueue: %ld\n", box_CoCo_list_get_size(outsQueue) );
            printf("                  size of precQueue: %ld\n", box_CoCo_list_get_size(precQueue) );
            printf("                  size of roots    : %ld\n", box_CoCo_list_get_size(resolved) );
            printf("                  numb of roots    : %ld\n", nbRoots );
        }
#endif

        /* move ccs of precQueue in unresolved */
        while ( !(box_CoCo_list_is_empty(precQueue) ) )
            box_CoCo_list_push( unresolved, box_CoCo_list_pop(precQueue) );
        
        box_CoCo_list_clear(precQueue);
        box_CoCo_list_clear(outsQueue);
        box_CoCo_list_clear(prepQueue);
        box_CoCo_list_clear(unresolved);
    }
    
    slong res = 0;
    /* convert each CoCo in resolved into a root, decide realness */
    _realCoeffs = pw_polynomial_is_real(poly);
    box_CoCo_list_iterator it = box_CoCo_list_begin(resolved);
    while ( it!=box_CoCo_list_end() ) {
        box_CoCo_get_contBox(roots+res, box_CoCo_list_elmt(it));
        mults[res] = box_CoCo_nbRooref( box_CoCo_list_elmt(it));
        if (_realCoeffs && arb_contains_zero( acb_imagref(roots+res) ) && (mults[res]==1) )
            arb_zero( acb_imagref(roots+res) );
        res++;
        it = box_CoCo_list_next(it);
    }
    
    box_CoCo_list_clear(resolved);
    
    
    return res;
}

slong solver_CoCo_refine_exact_acb_poly( acb_ptr roots, slong * mults, slong lenToRefine, const acb_poly_t p,  
                                         slong log2_eps, int goal,
                                         int useRealCoeffs PW_VERBOSE_ARGU(verbose)){
    
    pw_polynomial_t poly;
    pw_polynomial_init_from_exact_acb ( poly, p );
    
    domain_t domC;
    domain_init(domC);
    slong res = solver_CoCo_refine_pw_polynomial( roots, mults, lenToRefine, poly, log2_eps, goal, domC, -1, useRealCoeffs PW_VERBOSE_CALL(verbose)  );
    domain_clear(domC);
    pw_polynomial_clear(poly);
    
    return res;
}

