/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef SOLVER_COCO_H
#define SOLVER_COCO_H

#include "pw_base.h"
#include "common/pw_common.h"
#include "polynomial/pw_polynomial.h"
#include "solvers/acb_list.h"
#include "geometry/box.h"
#include "geometry/box_CoCo.h"
#include "geometry/domain.h"
#include "solvers/QBR.h"

#ifdef __cplusplus
extern "C" {
#endif

slong solver_CoCo_approximate_unit_disc_pw_polynomial( acb_ptr roots, slong * mults, pw_polynomial_t poly,
                                                       slong log2_eps, int realCoeffs, 
                                                       slong nbRootsInUnitDisc PW_VERBOSE_ARGU(verbose));

slong solver_CoCo_approximate_unit_disc_exact_acb_poly( acb_ptr roots, slong * mults, 
                                                        acb_poly_ptr p, slong log2_eps, int realCoeffs, 
                                                        slong nbRootsInUnitDisc PW_VERBOSE_ARGU(verbose));

slong solver_CoCo_approximate_unit_disc_2fmpq_poly( acb_ptr roots, slong * mults, 
                                                    const fmpq_poly_t re, const fmpq_poly_t im, 
                                                    slong log2_eps, int realCoeffs, 
                                                    slong nbRootsInUnitDisc PW_VERBOSE_ARGU(verbose));

slong solver_CoCo_solve_pw_polynomial( acb_ptr roots, slong * mults, slong nbInRoots,
                                       pw_polynomial_t poly, 
                                       slong log2_eps, int goal, domain_t dom, slong goalNbRoots PW_VERBOSE_ARGU(verbose));

slong solver_CoCo_solve_2fmpq_poly( acb_ptr roots, slong * mults, slong nbInRoots,
                                          const fmpq_poly_t re, const fmpq_poly_t im, 
                                          slong log2_eps, int goal, domain_t dom, slong goalNbRoots PW_VERBOSE_ARGU(verbose));

slong solver_CoCo_solve_exact_acb_poly( acb_ptr roots, slong * mults, slong nbInRoots,
                                              const acb_poly_t p,  
                                              slong log2_eps, int goal, domain_t dom, slong goalNbRoots PW_VERBOSE_ARGU(verbose));

/* Pre conditions: roots is a list of lenToRefine SQUARE boxes */
/*                 for each box B(ci,ri) in roots, the disc D(ci,ri) is a natural isolator of mults[i] roots */
/*                 let N be the sum of mult[i] for i=0 to lenToRefine-1; */
/*                 assume roots and mults have enough room for N items */
/*                 assume trailing zeros of poly have already been removed */
slong solver_CoCo_refine_pw_polynomial( acb_ptr roots, slong * mults, slong lenToRefine, 
                                        pw_polynomial_t poly,
                                        slong log2_eps, int goal, const domain_t dom, slong goalNbRoots,
                                        int useRealCoeffs PW_VERBOSE_ARGU(verbose));

slong solver_CoCo_refine_exact_acb_poly( acb_ptr roots, slong * mults, slong lenToRefine, const acb_poly_t p,  
                                         slong log2_eps, int goal, 
                                         int useRealCoeffs PW_VERBOSE_ARGU(verbose));

slong solver_CoCo_zoom_pw_polynomial( acb_t cluster, slong mult,
                                      pw_polynomial_t poly, 
                                      slong log2_eps, int goal PW_VERBOSE_ARGU(verbose));

/* assume the domain in input is finite      */
/* assume root zero has already been removed */
slong solver_CoCo_refine_domain_pw_polynomial( acb_ptr roots, slong * mults,
                                               pw_polynomial_t poly,
                                               int goal, slong log2_eps, const domain_t dom, slong multInDomain PW_VERBOSE_ARGU(verbose));

#ifdef PW_COCO_PROFILE
#include <time.h>
extern double clicks_in_tstar_exclusion;
extern double clicks_in_tstar_exclusion_shift;
extern double clicks_in_tstar_exclusion_DLG;
extern double clicks_in_shift;
extern double clicks_in_anticipate;
extern double clicks_in_QBR;
extern double clicks_in_QBR_evaluation;
extern double clicks_in_QBR_newtonInte;
extern double clicks_in_QBR_derivative;
extern ulong nb_tstar_exclusion;
extern ulong nb_tstar_exclusion_DLG;
extern ulong nb_tstar_exclusion_zero;
extern ulong nb_tstar_exclusion_posi;
extern ulong nb_tstar_exclusion_fail;
extern ulong nb_tstar_exclusion_prec53;
extern ulong nb_tstar_exclusion_prec106;
extern ulong nb_shift;
extern ulong nb_shift_prec53;
extern ulong nb_anticipate;

#define PW_COCO_INIT_PROFILE {\
    clicks_in_tstar_exclusion = 0.; \
    clicks_in_tstar_exclusion_shift = 0.; \
    clicks_in_tstar_exclusion_DLG = 0.; \
    clicks_in_shift = 0.; \
    clicks_in_anticipate = 0.; \
    clicks_in_QBR = 0.; \
    clicks_in_QBR_evaluation = 0.;\
    clicks_in_QBR_newtonInte = 0.;\
    clicks_in_QBR_derivative = 0.; \
    nb_tstar_exclusion = 0; \
    nb_tstar_exclusion_DLG = 0; \
    nb_tstar_exclusion_zero = 0; \
    nb_tstar_exclusion_posi = 0; \
    nb_tstar_exclusion_fail = 0; \
    nb_tstar_exclusion_prec53 = 0; \
    nb_tstar_exclusion_prec106 = 0; \
    nb_shift = 0; \
    nb_shift_prec53 = 0; \
    nb_anticipate = 0; \
}

#ifndef PW_NO_INTERFACE
#define PW_COCO_PRINT_PROFILE {\
    printf("\n"); \
    printf("&&&&&&&&&&&&&&&&&&&&&&& PW COCO PROFILE &&&&&&&&&&&&&&&&&&&&&&&&\n"); \
    printf("   --> tstar exclusion tests:         \n" ); \
    printf("      --> nb calls:                %lu\n", nb_tstar_exclusion ); \
    printf("      --> nb returning no roots:   %lu\n", nb_tstar_exclusion_zero ); \
    printf("      --> nb returning    roots:   %lu\n", nb_tstar_exclusion_posi ); \
    printf("      --> nb without decision  :   %lu\n", nb_tstar_exclusion_fail ); \
    printf("      --> nb with prec 53:         %lu\n", nb_tstar_exclusion_prec53 ); \
    printf("      --> nb with prec 106:        %lu\n", nb_tstar_exclusion_prec106 ); \
    printf("      --> nb DLGs:                 %lu\n", nb_tstar_exclusion_DLG ); \
    printf("      --> time in shifts:          %lf\n", clicks_in_tstar_exclusion_shift/CLOCKS_PER_SEC ); \
    printf("      --> time in DLGs:            %lf\n", clicks_in_tstar_exclusion_DLG/CLOCKS_PER_SEC ); \
    if (nb_anticipate>0) { \
    printf("      --> nb anticipate:           %lu\n", nb_anticipate ); \
    printf("      --> time in anticipate:      %lf\n", clicks_in_anticipate/CLOCKS_PER_SEC ); \
    } \
    printf("      --> total time:              %lf\n", clicks_in_tstar_exclusion/CLOCKS_PER_SEC ); \
    printf("   --> QBR Iterations:                \n" ); \
    printf("      --> time in evaluations:     %lf\n", clicks_in_QBR_evaluation/CLOCKS_PER_SEC ); \
    printf("      --> time in Interval Newton: %lf\n", clicks_in_QBR_newtonInte/CLOCKS_PER_SEC ); \
    printf("      --> time in derivative:      %lf\n", clicks_in_QBR_derivative/CLOCKS_PER_SEC ); \
    printf("      --> total time:              %lf\n", clicks_in_QBR/CLOCKS_PER_SEC ); \
    printf("   --> taylor shifts:                 \n" ); \
    printf("      --> nb calls:                %lu\n", nb_shift ); \
    printf("      --> nb with prec 53:         %lu\n", nb_shift_prec53 ); \
    printf("      --> total time:              %lf\n", clicks_in_shift/CLOCKS_PER_SEC ); \
    printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"); \
    printf("\n\n"); \
}
#else
#define PW_COCO_PRINT_PROFILE
#endif
#endif

#ifdef __cplusplus
}
#endif

#endif
