/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "tstar.h"
#include "be.h"

#ifdef PW_PROFILE
double clicks_in_beDLG;
double clicks_in_bepellet;
#endif

void _be_poly_oneDLGIteration_naive( double * dest, double * src, slong len ) {

#ifdef PW_PROFILE
    clock_t start = clock();
#endif
    
// #ifdef PWPOLY_TRACK_EXCEPT
//     /* save exception flags and re-init exceptions */
//     fexcept_t except_save;
//     fegetexceptflag (&except_save, FE_ALL_EXCEPT);
//     feclearexcept (FE_ALL_EXCEPT);
// #endif
    
    slong i, ii, kmin, k, kk, j, jj;
    slong d = len-1;
    double t_re, t_im, t_aber;
    for (i=0; i<len; i++) {
        ii = 3*i;
        /* set dest+(3i, 3i+1, 3i+2) to (-1)^(len-1-i)(src+(3i, 3i+1, 3i+2))^2 */
        _be_mul_aber( dest + ii, dest + (ii+1), dest + (ii+2),
                      src[ii],   src[ii+1],     src[ii+2],
                      src[ii],   src[ii+1],     src[ii+2] );
        if ((d-i)%2==1)
            _be_neg_aber ( dest + ii, dest + (ii+1), dest + (ii+2),
                           dest[ii],  dest[ii+1],    dest[ii+2] );
        
        kmin = PWPOLY_MAX( 0, 2*i-d );
        for (k=kmin; k<i; k++) {
            kk=3*k;
            j =2*i-k;
            jj=3*j;
            _be_mul_aber( &t_re,   &t_im,    &t_aber,
                          src[kk], src[kk+1], src[kk+2],
                          src[jj], src[jj+1], src[jj+2] );
            t_re*=2;
            t_im*=2;
            t_aber*=2;
            if ((d-k)%2==1)
                _be_sub_aber( dest + ii, dest + (ii+1), dest + (ii+2),
                              dest[ii],  dest [ii+1],   dest[ii+2],
                              t_re,      t_im,          t_aber);
            else
                _be_add_aber( dest + ii, dest + (ii+1), dest + (ii+2),
                              dest[ii],  dest [ii+1],   dest[ii+2],
                              t_re,      t_im,          t_aber);
        }
    }
#ifdef PWPOLY_TRACK_EXCEPT
    _pwpoly_test_and_print_exception("_be_poly_oneDLGIteration_naive: ", 1);
    /* restore exception flags and rounding mode */
//     fesetexceptflag (&except_save, FE_ALL_EXCEPT);
#endif 
    
#ifdef PW_PROFILE
    clicks_in_beDLG += (clock() - start);
#endif
}

void _be_real_sum_aber( double * dest, double *dest_aber,
                        double * src,  slong len ) {
// #ifdef PWPOLY_TRACK_EXCEPT
//     /* save exception flags and re-init exceptions */
//     fexcept_t except_save;
//     fegetexceptflag (&except_save, FE_ALL_EXCEPT);
//     feclearexcept (FE_ALL_EXCEPT);
// #endif
    *dest=0;
    *dest_aber=0;
    for (slong i=0; i<len; i++)
        _be_real_add_aber( dest,  dest_aber,
                           *dest, *dest_aber,
                           src[2*i], src[2*i+1] );
#ifdef PWPOLY_TRACK_EXCEPT
    _pwpoly_test_and_print_exception("_be_real_sum_aber: ", 1);
    /* restore exception flags and rounding mode */
//     fesetexceptflag (&except_save, FE_ALL_EXCEPT);
#endif
}

/* assume len>1 */
slong tstar_unit_disc_be( double * poly, slong len, int * n, int nbMDLG, slong nbMsols){
    
#ifdef PW_PROFILE
    clock_t start = clock();
#endif
    
// #ifdef PWPOLY_TRACK_EXCEPT
//     /* save exception flags and re-init exceptions */
//     fexcept_t except_save;
//     fegetexceptflag (&except_save, FE_ALL_EXCEPT);
//     feclearexcept (FE_ALL_EXCEPT);
// #endif
    
    double * polyDLG   = (double *) pwpoly_malloc ( 3*len*sizeof(double) );
    double * abscoeffs = (double *) pwpoly_malloc ( 2*len*sizeof(double) );
    double sumAbsCoeffs = .0, sumAbsCoeffs_aber = .0;
    
#ifdef PWPOLY_TRACK_EXCEPT
    _pwpoly_test_and_print_exception("_pwpoly_tstar_unit_disc_be: 0", 1);
#endif

    int N = (int) 4+ceil(log2(1+log2(len-1)));
#ifdef PWPOLY_TRACK_EXCEPT
    _pwpoly_test_and_print_exception("_pwpoly_tstar_unit_disc_be: 1", 1);
#endif
    if (nbMDLG>=0)
        N=PWPOLY_MIN(N, nbMDLG);
    
    *n = 0;
    slong res = -1;

    while ( (res==-1) && (*n<=N) ) {
        
        /* compute abs of coeffs */
        for (slong j=0; j<len; j++)
            _be_abs_aber( abscoeffs + (2*j), abscoeffs + (2*j+1), 
                          poly[3*j],         poly[3*j+1], poly[3*j+2] );
        
        slong i = 0;
    
        _be_real_sum_aber( &sumAbsCoeffs, &sumAbsCoeffs_aber,
                           abscoeffs + 2*(i+1),  len-1 );

        
        if ( _be_real_pos_gt( abscoeffs[2*i], abscoeffs[2*i+1], sumAbsCoeffs, sumAbsCoeffs_aber ) )
            res=i;
        
        while ( (res==-1) && (i < len-1) && (i <= nbMsols) ) {
            _be_real_add_aber( &sumAbsCoeffs, &sumAbsCoeffs_aber,
                               sumAbsCoeffs,   sumAbsCoeffs_aber,
                               abscoeffs[2*i], abscoeffs[2*i+1]);
            i = i+1;
            _be_real_sub_aber( &sumAbsCoeffs, &sumAbsCoeffs_aber,
                               sumAbsCoeffs,   sumAbsCoeffs_aber,
                               abscoeffs[2*i], abscoeffs[2*i+1]);
            if ( _be_real_pos_gt( abscoeffs[2*i], abscoeffs[2*i+1], sumAbsCoeffs, sumAbsCoeffs_aber ) )
                res=i;
        }
        
        if ( (res==-1) && (*n<N)) {
            _be_poly_oneDLGIteration_naive( polyDLG, poly, len );
            for (slong j=0; j<len; j++) {
                poly[3*j]   = polyDLG[3*j];
                poly[3*j+1] = polyDLG[3*j+1];
                poly[3*j+2] = polyDLG[3*j+2];
            }
            *n = *n+1;

        } else if ( (res==-1) && (*n==N) ) {
            break;
        }
    }
    
    pwpoly_free(abscoeffs);
    pwpoly_free(polyDLG);
    
#ifdef PWPOLY_TRACK_EXCEPT
    _pwpoly_test_and_print_exception("_pwpoly_tstar_unit_disc_be: ", 1);
    /* restore exception flags and rounding mode */
//     fesetexceptflag (&except_save, FE_ALL_EXCEPT);
#endif
    
#ifdef PW_PROFILE
    clicks_in_bepellet += (clock() - start);
#endif
    
    return res;
    
}
                               
