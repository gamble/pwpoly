/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "solvers_few_roots.h"

#ifdef PW_PROFILE
double clicks_in_fewRoot_ffts;
double clicks_in_fewRoot_ffts_precomp;
#endif

#define midre(X) arb_midref(acb_realref(X))
#define midim(X) arb_midref(acb_imagref(X))

void pw_acb_poly_evaluate2_horner_mid(acb_t y, acb_t z, acb_srcptr poly,
                                      slong len, const acb_t x, slong prec) {
    if (len == 0)
    {
        acb_zero(y);
        acb_zero(z);
    }
    else if (len == 1)
    {
//         acb_set_round(y, poly + 0, prec);
        arf_set( midre(y), midre( poly + 0 ) );
        arf_set( midim(y), midre( poly + 0 ) );
        acb_zero(z);
    }
    else if (acb_is_zero(x))
    {
//         acb_set_round(y, poly + 0, prec);
        arf_set( midre(y), midre( poly + 0 ) );
        arf_set( midim(y), midim( poly + 0 ) );
//         acb_set_round(z, poly + 1, prec);
        arf_set( midre(z), midre( poly + 1 ) );
        arf_set( midim(z), midim( poly + 1 ) );
    }
    else if (len == 2)
    {
//         acb_mul(y, x, poly + 1, prec);
        arf_complex_mul(midre(y), midim(y),
                        midre(x), midim(x),
                        midre( poly + 1 ), midim( poly + 1 ), prec, ARF_RND_NEAR);
//         acb_add(y, y, poly + 0, prec);
        arf_add( midre(y), midre(y), midre( poly + 0 ), prec, ARF_RND_NEAR);
        arf_add( midim(y), midim(y), midim( poly + 0 ), prec, ARF_RND_NEAR);
//         acb_set_round(z, poly + 1, prec);
        arf_set( midre(z), midre( poly + 1 ) );
        arf_set( midim(z), midim( poly + 1 ) );
    }
    else
    {
        acb_t t, u, v;
        slong i;

        acb_init(t);
        acb_init(u);
        acb_init(v);

//         acb_set_round(u, poly + len - 1, prec);
        arf_set( midre(u), midre( poly + len - 1 ) );
        arf_set( midim(u), midim( poly + len - 1 ) );
        acb_zero(v);

        for (i = len - 2; i >= 0; i--)
        {
//             acb_mul(t, v, x, prec);
            arf_complex_mul(midre(t), midim(t),
                            midre(v), midim(v),
                            midre(x), midim(x), prec, ARF_RND_NEAR);
//             acb_add(v, u, t, prec);
            arf_add( midre(v), midre(u), midre(t), prec, ARF_RND_NEAR);
            arf_add( midim(v), midim(u), midim(t), prec, ARF_RND_NEAR);
//             acb_mul(t, u, x, prec);
            arf_complex_mul(midre(t), midim(t),
                            midre(u), midim(u),
                            midre(x), midim(x), prec, ARF_RND_NEAR);
//             acb_add(u, t, poly + i, prec);
            arf_add( midre(u), midre(t), midre(poly + i), prec, ARF_RND_NEAR);
            arf_add( midim(u), midim(t), midim(poly + i), prec, ARF_RND_NEAR);
        }

//         acb_swap(y, u);
        arf_swap( midre(y), midre(u) );
        arf_swap( midim(y), midim(u) );
//         acb_swap(z, v);
        arf_swap( midre(z), midre(v) );
        arf_swap( midim(z), midim(v) );

        acb_clear(t);
        acb_clear(u);
        acb_clear(v);
    }
}
#undef midre
#undef midim

slong _EA_its_on_l_roots( acb_ptr roots, slong l, slong log2_eps, slong nbIts,  acb_srcptr poly, slong len, slong prec, int unitDisc PW_VERBOSE_ARGU(verbose)  ) {

#ifndef PW_SILENT    
    int level = 5;
#endif
    slong res = 0;
    
    acb_ptr nRoots   = _acb_vec_init(l);
    acb_t invDiffs;
    acb_t pval   ;
    acb_t pderval;
    acb_init(invDiffs);
    acb_init(pval);
    acb_init(pderval);
    
    arb_t abspval, abspderval, radius;
    arb_init(abspval);
    arb_init(abspderval);
    arb_init(radius);
    mag_t rad;
    mag_init(rad);
    
    slong it;
    
    for (it = 0; (it<nbIts)&&(res==0); it++) {
        
        for (slong i=0; i<l; i++) {
            acb_get_mid(roots+i, roots+i);
            /* compute p(roots+i) and p'(roots+i) */
//             _acb_poly_evaluate2( pval, pderval, poly, len, roots+i, prec);
            pw_acb_poly_evaluate2_horner_mid(pval, pderval, poly, len, roots+i, prec);
            acb_get_mid(pval, pval);
            acb_get_mid(pderval, pderval);
//             if (verbose>=level) {
//                 printf("--------------_EA_its_on_l_roots: it=%ld, i=%ld-------------------\n", it, i);
//                 printf("point   : "); acb_printd(roots+i, 10); printf("\n");
//                 printf("pval    : "); acb_printd(pval, 10); printf("\n");
//                 printf("pderval : "); acb_printd(pderval, 10); printf("\n");
//             }
            if (!acb_is_zero(pval)) {
                /* compute p'(roots+i)/p(roots+i) */
                acb_div(pval, pderval, pval, prec);
                acb_get_mid(pval, pval);
//                 if (verbose>=level) {
//                     printf("p'/p    : "); acb_printd(pval, 10); printf("\n");
//                 }
                /* compute invDiffs = sum of the 1/(roots+i - roots+k) */
                acb_zero(invDiffs);
                for (slong k=0; k<l; k++) {
                    if (k!=i) {
                        acb_sub(pderval, roots+i, roots+k, prec);
                        acb_get_mid(pderval, pderval);
                        acb_inv(pderval, pderval, prec);
                        acb_get_mid(pderval, pderval);
                        acb_add(invDiffs, invDiffs, pderval, prec);
                        acb_get_mid(invDiffs, invDiffs);
                    }
                }
                /* compute 1/( p'(roots+i)/p(roots+i) - sum of the 1/(roots+i - roots+k) ) */
                acb_sub(pval, pval, invDiffs, prec);
                acb_get_mid(pval, pval);
                acb_inv(pval, pval, prec);
                acb_get_mid(pval, pval);
                /* compute nroots+i = roots+i - 1/( p(roots+i)/p'(roots+i) - sum of the 1/(roots+i - roots+k) ) */
                acb_sub(nRoots+i, roots+i, pval, prec);
                acb_get_mid(nRoots+i, nRoots+i);
            } else {
                acb_set(nRoots+i, roots+i);
            }
        }
        
        for (slong i=0; i<l; i++)
            acb_set(roots+i, nRoots+i);
        
        if ( (it%5==0)||(it==nbIts-1) ) {
            res = 1;
            for (slong i=0; (i<l)&&(res==1); i++) {
                acb_abs(abspval, roots+i, prec);
                if ( (!unitDisc)||(arf_cmp_si( arb_midref(abspval), 1) < 0)) {
                    /* compute p(roots+i) and p'(roots+i) */
//                  _acb_poly_evaluate2( pval, pderval, poly, len, roots+i, prec);
                    pw_acb_poly_evaluate2_horner_mid(pval, pderval, poly, len, roots+i, prec);
                    /* compute d|p(roots+i)|/|p'(roots+i)| */
                    acb_abs(abspval, pval, prec);
                    acb_abs(abspderval, pderval, prec);
                    arb_div(radius, abspval, abspderval, prec);
                    arb_mul_ui(radius, radius, len-1, prec);
                    acb_add_error_arb( roots+i, radius);
                    mag_max( rad, arb_radref( acb_realref(roots+i) ), arb_radref( acb_imagref(roots+i) ) );
                    res = res && (mag_cmp_2exp_si(rad, log2_eps-1) < 0);
                } 
//                 else {
//                     res = 0;
//                 }
            }
#ifndef PW_SILENT
            if (verbose>=level) {
                printf("--------------_EA_its_on_l_roots: it=%ld, res=%ld-------------------\n", it, res);
                for (slong i=0; i<l; i++) {
                    pw_acb_poly_evaluate2_horner_mid(pval, pderval, poly, len, roots+i, prec);
                    acb_get_mid(pval, pval);
                    acb_get_mid(pderval, pderval);
                    printf("---i = %ld\n",i);
                    printf("   point   : "); acb_printd(roots+i, 10); printf("\n");
                    printf("   pval    : "); acb_printd(pval, 10); printf("\n");
                    printf("   pderval : "); acb_printd(pderval, 10); printf("\n");
                }
            }
#endif
        }
        
    }
    
    mag_clear(rad);
    arb_clear(radius);
    arb_clear(abspderval);
    arb_clear(abspval);
    
    _acb_vec_clear(nRoots, l);
    acb_clear(invDiffs);
    acb_clear(pval);
    acb_clear(pderval);
    
    if (res)
        res = it;
    return res;
}

void _cauchy_sums_RAD2FFT( acb_ptr sums, slong nbSums, acb_srcptr poly, slong len, slong prec ) {
    
    /* get the power of 2 above len */
    int ceilLog2Len = 1;
    while( ((slong)0x1 << ceilLog2Len) < len )
        ceilLog2Len++;
    
    slong nlen = (slong)0x1 << ceilLog2Len;
    
    acb_ptr rootsOfUnit = _acb_vec_init(nlen);
    acb_ptr pvals       = _acb_vec_init(nlen);
    acb_ptr pdervals    = _acb_vec_init(nlen);
    acb_t temp;
    acb_init(temp);
    
    acb_dft_rad2_t dft;
    
// #ifdef PW_PROFILE
//     clock_t start_precomp = clock();
// #endif
    
    acb_dft_rad2_init(dft, ceilLog2Len, prec);
    
// #ifdef PW_PROFILE
//     clicks_in_fewRoot_ffts_precomp += (clock() - start_precomp);
// #endif
    
    acb_poly_t p, pder;
    acb_poly_init2(p, nlen);
    acb_poly_init2(pder, nlen);
    _acb_poly_set_length(p, nlen);
    _acb_poly_set_length(pder, nlen);
    for (slong i=0; i<len; i++)
        acb_set(p->coeffs + i, poly+i);
    _acb_poly_derivative(pder->coeffs, poly, len, prec);
    acb_zero( (pder->coeffs)+(len-1) );
    for (slong i=len; i<nlen; i++) {
        acb_zero( p->coeffs + i );
        acb_zero( pder->coeffs + i );
    }
    
// #ifdef PW_PROFILE
//     clock_t start_fft = clock();
// #endif
    
    /* evaluate poly at the nlen nlen-th roots of unit */
    acb_dft_rad2_precomp( pvals, p->coeffs, dft, prec);
    /* evaluate pder at the nlen nlen-th roots of unit */
    acb_dft_rad2_precomp( pdervals, pder->coeffs, dft, prec);
    
// #ifdef PW_PROFILE
//     clicks_in_fewRoot_ffts += (clock() - start_fft);
// #endif
    
    acb_poly_clear(pder);
    acb_poly_clear(p);
    acb_dft_rad2_clear(dft);
    
    /* get the nlen nlen-th roots of unit */
    _acb_vec_unit_roots(rootsOfUnit, -nlen, nlen, prec);
    
    for (slong i=0; i<nlen; i++) {
        /* set pvals+i to (pdervals+i)/(pvals+i), set its radius to zero */
        acb_div( pvals+i, pdervals+i, pvals+i, prec );
        acb_get_mid(pvals+i, pvals+i);
    }
    /* compute the nbSums Cauchy sums */
    for (slong h=1; h<=nbSums; h++) {
        acb_zero( sums+(h-1) );
        /* set sh to (1/nlen)( sum for g from 0 to nlen-1 of (rootsOfUnit+(g*(h+1))%nlen)*(pdervals+g)/(pvals+g) ) */
        for (slong g=0; g<nlen; g++) {
            acb_mul( temp, rootsOfUnit+(g*(h+1))%nlen, pvals+g, prec );
            acb_add( sums+(h-1), sums+(h-1), temp, prec );
        }
        acb_div_si( sums+(h-1), sums+(h-1), nlen, prec );
    }
    
    acb_clear(temp);
    _acb_vec_clear(pdervals,    nlen);
    _acb_vec_clear(pvals,       nlen);
    _acb_vec_clear(rootsOfUnit, nlen);
}

/* ps is a vector of nbps acb's  */
/* p  is an initialized acb_poly */
/* sets p to the polynomial of degree nbps which nbps roots have power sums ps */
/* with Newton's identities */
void pw_newton_identites ( acb_poly_t p, acb_srcptr ps, slong nbps, slong prec ) {
    acb_poly_fit_length( p, nbps+1 );
    _acb_poly_set_length(p, nbps+1 );
    
    acb_t temp;
    acb_init(temp);
    
    acb_one( p->coeffs + nbps );
    for (slong k=1; k<=nbps; k++ ) {
        acb_neg( p->coeffs + (nbps-k), ps+(k-1) );
        
        for (slong i=1; i<=k-1; i++) {
            acb_mul( temp, p->coeffs + (nbps-k+i), ps+(i-1), prec );
            if ( (k-1+i)%2 )
                acb_neg( temp, temp );
            acb_add( p->coeffs + (nbps-k), p->coeffs + (nbps-k), temp, prec );
        }
        
        acb_div_si( p->coeffs + (nbps-k), p->coeffs + (nbps-k), k, prec );
        
        if ( k%2 )
            acb_neg( p->coeffs + (nbps-k), p->coeffs + (nbps-k) );
    }
    
    for (slong k=1; k<=nbps; k+=2 )
        acb_neg( p->coeffs + (nbps-k), p->coeffs + (nbps-k) );
    
    acb_clear(temp);
}

void pw_solver_quadratic( acb_ptr roots, acb_srcptr poly, slong prec PW_VERBOSE_ARGU(verbose)  ) {
#define a poly+2
#define b poly+1
#define c poly+0
    
#ifndef PW_SILENT    
    int level = 5;
#endif
    prec = 4*prec;
    
    acb_t Delta;
    acb_init(Delta);
    
    /* set roots to the two roots of (poly+0) + bz + (poly+2)z^2 */
    acb_sqr(roots+0, b, prec);
    acb_mul(roots+1, c, a, prec);
    acb_mul_2exp_si( roots+1, roots+1, 2 );
    acb_sub(Delta, roots+0, roots+1, prec );
    acb_sqrt(Delta, Delta, prec);
    
    acb_get_mid(Delta, Delta);
    
    acb_neg(roots+0, b);
    acb_neg(roots+1, b);
    acb_sub(roots+0, roots+0, Delta, prec);
    acb_add(roots+1, roots+1, Delta, prec);
    acb_div(roots+0, roots+0, a, prec);
    acb_div(roots+1, roots+1, a, prec);
    acb_mul_2exp_si( roots+0, roots+0, -1 );
    acb_mul_2exp_si( roots+1, roots+1, -1 );
    
    acb_get_mid(roots+0, roots+0);
    acb_get_mid(roots+1, roots+1);
#ifndef PW_SILENT    
    if (verbose>=level) {
        acb_t f1, f2;
        acb_init(f1); 
        acb_init(f2);
        printf("--------------solver_quadratic-------------------\n");
        printf("a            : "); acb_printd(a, 10); printf("\n");
        printf("b            : "); acb_printd(b, 10); printf("\n");
        printf("c            : "); acb_printd(c, 10); printf("\n");
        printf("sqrt(delta)  : "); acb_printd(Delta, 10); printf("\n");
        printf("root1        : "); acb_printd(roots+0, 10); printf("\n");
        printf("root2        : "); acb_printd(roots+1, 10); printf("\n");
        _acb_poly_evaluate(f1, poly, 3, roots+0, prec);
        _acb_poly_evaluate(f2, poly, 3, roots+1, prec);
        printf("poly at root1: "); acb_printd(f1, 10); printf("\n");
        printf("poly at root2: "); acb_printd(f2, 10); printf("\n");
        acb_clear(f2);
        acb_clear(f1);
    }
#endif    
    acb_clear(Delta);
#undef a
#undef b
#undef c
}

void pw_solver_cubic( acb_ptr roots, acb_srcptr poly, slong prec PW_VERBOSE_ARGU(verbose)  ) {
#ifndef PW_SILENT 
    int level = 5;
#endif
    
#define a poly+3
#define b poly+2
#define c poly+1
#define d poly+0
    
//     prec = prec;
    
    acb_t Delta0, Delta1, C, zeta, temp;
    acb_init(Delta0);
    acb_init(Delta1);
    acb_init(C);
    acb_init(zeta);
    acb_init(temp);
    
    /* set Delta0 to b^2 - 3ac */
    acb_sqr(roots+0, b, prec );
    acb_mul(roots+1, a, c, prec );
    acb_mul_ui(roots+1, roots+1, 3, prec );
    acb_sub(Delta0, roots+0, roots+1, prec );
    /* set Delta1 to 2b^3 - 9abc + 27a^2d */
    acb_pow_ui(roots+0, b, 3, prec);
    acb_mul_2exp_si(roots+0, roots+0, 1);
    acb_mul(roots+1, roots+1, b, prec);
    acb_mul_ui(roots+1, roots+1, 3, prec );
    acb_sqr(roots+2, a, prec );
    acb_mul(roots+2, roots+2, d, prec);
    acb_mul_ui(roots+2, roots+2, 27, prec );
    acb_sub(Delta1, roots+0, roots+1, prec );
    acb_add(Delta1, Delta1, roots+2, prec );
    /* set C to ( Delta1 + sqrt(Delta1^2 - 4Delta0^3) / 2 )^(1/3) */
    acb_set(C, Delta1);
    acb_sqr(roots+1, Delta1, prec);
    acb_pow_ui(roots+0, Delta0, 3, prec);
    acb_mul_2exp_si(roots+0, roots+0, 2);
    acb_sub(roots+1, roots+1, roots+0, prec);
    acb_sqrt(roots+1, roots+1, prec);
    acb_add(C, C, roots+1, prec);
    acb_mul_2exp_si(C, C, -1);
    acb_root_ui(C, C, 3, prec);
    
    acb_unit_root(zeta, 3, prec);
//     printf("zeta        : "); acb_printd(zeta, 10); printf("\n");
    
    acb_get_mid(C,C);
    acb_get_mid(Delta0,Delta0);
    acb_get_mid(Delta1,Delta1);
    
    
    acb_div(roots+0, Delta0, C, prec);
    acb_add(roots+0, roots+0, C, prec);
    acb_add(roots+0, roots+0, b, prec);
    acb_div(roots+0, roots+0, a, prec);
    acb_div_ui(roots+0, roots+0, 3, prec);
    acb_neg(roots+0, roots+0);
    
    for (int i=1; i<3; i++){
        acb_pow_ui(roots+i, zeta, i, prec );
        acb_mul(roots+i, C, roots+i, prec);
        acb_div(temp, Delta0, roots+i, prec);
        acb_add(roots+i, roots+i, temp, prec);
        acb_add(roots+i, roots+i, b, prec);
        acb_div(roots+i, roots+i, a, prec);
        acb_div_ui(roots+i, roots+i, 3, prec);
        acb_neg(roots+i, roots+i);
    }

#ifndef PW_SILENT     
    if (verbose>=level) {
        acb_t f1, f2, f3;
        acb_init(f1); 
        acb_init(f2);
        acb_init(f3);
        printf("--------------pw_solver_cubic-------------------\n");
        printf("a            : "); acb_printd(a, 10); printf("\n");
        printf("b            : "); acb_printd(b, 10); printf("\n");
        printf("c            : "); acb_printd(c, 10); printf("\n");
        printf("d            : "); acb_printd(d, 10); printf("\n");
        printf("Delta0       : "); acb_printd(Delta0, 10); printf("\n");
        printf("Delta1       : "); acb_printd(Delta1, 10); printf("\n");
        printf("C            : "); acb_printd(C, 10); printf("\n");
        printf("root1        : "); acb_printd(roots+0, 10); printf("\n");
        printf("root2        : "); acb_printd(roots+1, 10); printf("\n");
        printf("root3        : "); acb_printd(roots+2, 10); printf("\n");
        _acb_poly_evaluate(f1, poly, 4, roots+0, prec);
        _acb_poly_evaluate(f2, poly, 4, roots+1, prec);
        _acb_poly_evaluate(f3, poly, 4, roots+2, prec);
        printf("poly at root1: "); acb_printd(f1, 10); printf("\n");
        printf("poly at root2: "); acb_printd(f2, 10); printf("\n");
        printf("poly at root3: "); acb_printd(f3, 10); printf("\n");
        acb_clear(f3);
        acb_clear(f2);
        acb_clear(f1);
    }
#endif

    acb_clear(temp);
    acb_clear(zeta);
    acb_clear(C);
    acb_clear(Delta1);
    acb_clear(Delta0);
    
    acb_get_mid(roots+0, roots+0);
    acb_get_mid(roots+1, roots+1);
    acb_get_mid(roots+2, roots+2);

#undef a
#undef b
#undef c
#undef d
}

int pw_solver_123_roots( acb_ptr roots, slong nbRoots, slong log2_eps, acb_srcptr poly, slong len, slong prec PW_VERBOSE_ARGU(verbose)  ) {
    
    if ( (nbRoots<1) || (nbRoots >3) )
        return 0;
    
#ifndef PW_SILENT    
    int level = 5;
#endif
    int res = 0;
    
    acb_ptr cauchySums = _acb_vec_init( nbRoots );
    arb_ptr absroots   = _arb_vec_init( nbRoots );
    
    arb_t one;
    arb_init(one);
    arb_one(one);
    
    /* compute 1,2,3 first Cauchy's sums that approximate 1,2,3 first power sums */
    /* of the roots of poly in the unit disc                                     */
    _cauchy_sums_RAD2FFT( cauchySums, nbRoots, poly, len, prec  );
#ifndef PW_SILENT     
    if (verbose>=level) {
        printf("--------------solver_123_roots, nbRoots: %ld-------------------\n", nbRoots);
        for (slong i=0; i<nbRoots; i++) {
            acb_abs( absroots+i, cauchySums+i, prec );
            printf("--- %ld-th Cauchy sum : ", i); acb_printd(cauchySums+i, 10); printf("\n");
            printf("   |%ld-th Cauchy sum|: ", i); arb_printd(absroots+i, 10); printf("\n");
        }
    }
#endif    
    
    /* compute approximations of the roots in the unit disc from the Cauchy's sums */
    /* with Newton's identities */
    if (nbRoots>1) {
        acb_poly_t ptemp;
        acb_poly_init(ptemp);
        pw_newton_identites ( ptemp, cauchySums, nbRoots, prec );
        if (nbRoots==2)
            pw_solver_quadratic( roots, ptemp->coeffs, prec PW_VERBOSE_CALL(verbose)  );
        else
            pw_solver_cubic( roots, ptemp->coeffs, prec PW_VERBOSE_CALL(verbose)  );
        acb_poly_clear(ptemp);
    } else {
        acb_set( roots+0, cauchySums+0);
    }
    
#ifndef PW_SILENT     
    if (verbose>=level) {
        printf("--------------solver_123_roots, nbRoots: %ld-------------------\n", nbRoots);
        for (slong i=0; i<nbRoots; i++) {
            acb_abs( absroots+i, roots+i, prec );
            printf("--- %ld-th candidate  : ", i); acb_printd(roots+i, 10); printf("\n");
            printf("   |%ld-th candidate| : ", i); arb_printd(absroots+i, 10); printf("\n");
        }
        acb_t temp;
        acb_init(temp);
        for (slong i=0; i<nbRoots; i++) {
            printf("--- %ld-th Cauchy's sum  : ", i+1); acb_printd(cauchySums+i, 10); printf("\n");
            acb_zero(cauchySums+i);
            for (slong k=0; k<nbRoots; k++) {
                acb_pow_ui( temp, roots+k, i+1, prec);
                acb_add( cauchySums+i, cauchySums+i, temp, prec );
            }
            printf("    %ld-th power sum     : ", i+1); acb_printd(cauchySums+i, 10); printf("\n");
        }
        acb_clear(temp);
    }
#endif     
    
    /* apply EA iterations to the roots */
    slong resEA = _EA_its_on_l_roots( roots, nbRoots, log2_eps, 50,  poly, len, prec, 1 PW_VERBOSE_CALL(verbose)  );
    
    /* check if the approximations are pairwise disjoint and strictly in the unit disc */
    res = (resEA>0);
    for (slong i=0; (i<nbRoots)&&(res); i++) {
        acb_abs(absroots+i, roots+i, prec);
        res = res&&(arb_le( absroots+i, one) == 1);
        for (slong k=0; k<i; k++)
            res=res&&(acb_overlaps(roots + k, roots + i)==0);
    }
#ifndef PW_SILENT     
    if (verbose>=level) {
        printf("--------------solver_123_roots, nbRoots: %ld-------------------\n", nbRoots);
        printf(" resEA: %ld, res: %d\n", resEA, res);
        for (slong i=0; i<nbRoots; i++) {
            acb_abs( absroots+i, roots+i, prec );
            printf("--- %ld-th root       : ", i); acb_printd(roots+i, 10); printf("\n");
            printf("   |%ld-th root|      : ", i); arb_printd(absroots+i, 10); printf("\n");
        }
        
        printf("\n\n");
    }
#endif     
    
    for (slong i=0; (i<nbRoots)&&(res); i++)
        acb_get_mid( roots+i, roots+i );
    
    arb_clear(one);
    _arb_vec_clear(absroots, nbRoots);
    _acb_vec_clear(cauchySums, nbRoots);
    
    return res;
}
