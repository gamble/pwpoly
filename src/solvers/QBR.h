/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef QBR_H
#define QBR_H

#include "pw_base.h"
#include "polynomial/pw_polynomial.h"
#include "geometry/box_CoCo.h"
#include "solvers/tstar.h"

#ifdef __cplusplus
extern "C" {
#endif
    
#define QBR_SUCCESS 1
#define QBR_FAILURE 0
#define QBR_DER_CONT_ZERO -1
#define QBR_MAXPREC -2

int QBR_box_CoCo( slong *prec, box_CoCo_ptr * CC, pw_polynomial_t cache, slong log2_eps PW_VERBOSE_ARGU(verbose));

#ifdef __cplusplus
}
#endif

#endif
