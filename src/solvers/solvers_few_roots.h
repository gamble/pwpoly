/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef SOLVERS_FEW_ROOTS_H
#define SOLVERS_FEW_ROOTS_H

#include "pw_base.h"
#include "solvers/pw_EA.h"
#include "acb_dft.h"

#ifdef PWPOLY_HAS_EAROOTS
#include "earoots.h"
#endif

#ifdef PWPOLY_HAS_BEFFT
#include "common/pw_conversion_be.h"
#include "befft.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

void pw_acb_poly_evaluate2_horner_mid(acb_t y, acb_t z, acb_srcptr poly,
                                      slong len, const acb_t x, slong prec);

slong _EA_its_on_l_roots( acb_ptr roots, slong l, slong log2_eps, slong nbIts,  acb_srcptr poly, slong len, slong prec, int unitDisc PW_VERBOSE_ARGU(verbose) );
void pw_newton_identites ( acb_poly_t p, acb_srcptr ps, slong nbps, slong prec );
void pw_solver_quadratic( acb_ptr roots, acb_srcptr poly, slong prec PW_VERBOSE_ARGU(verbose) );
void pw_solver_cubic( acb_ptr roots, acb_srcptr poly, slong prec PW_VERBOSE_ARGU(verbose) );

int pw_solver_123_roots( acb_ptr roots, slong nbRoots, slong log2_eps, acb_srcptr poly, slong len, slong prec PW_VERBOSE_ARGU(verbose) );

#ifdef PWPOLY_HAS_BEFFT
int pw_solver_123_roots_be( acb_ptr roots, slong nbRoots, slong log2_eps, double * poly, slong len PW_VERBOSE_ARGU(verbose) );
#endif
    
#ifdef __cplusplus
}
#endif

#endif
