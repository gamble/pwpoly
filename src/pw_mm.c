/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_mm.h"

pwmm_table_t default_pwmm_table;
pwmm_table_ptr current_pwmm_table;

#ifdef PWMM_STATS
double time_in_search;
double time_in_insert;
double time_in_insert_at;
double time_in_sea_in_in;
double time_in_malloc;
double time_in_realloc;
double time_in_calloc;
double time_in_free;
ulong  nb_call_to_search;
ulong  nb_hit_with_heur;
ulong  nb_hit_with_last;
ulong  nb_call_malloc;
ulong  nb_call_realloc;
ulong  nb_call_calloc;
ulong  nb_call_free;
#endif

void pwmm_table_init( pwmm_table_t a ){
    PWMM_TAB_SIAL_REF(a) = PWMM_ALLOC_SIZE;
    PWMM_TAB_SIZE_REF(a) = 0;
    PWMM_TAB_USED_REF(a) = 0;
    PWMM_TAB_LASF_REF(a) = 0;
    PWMM_TAB_TABL_REF(a) = (pwmm_block_ptr) PWMM_MALLOC ( PWMM_ALLOC_SIZE*sizeof(struct pwmm_block) );
    PWMM_TAB_PREV_REF(a) = NULL;
}

void pwmm_table_clear( pwmm_table_t a ){
    if (PWMM_TAB_SIAL_REF(a))
        PWMM_FREE( PWMM_TAB_TABL_REF(a) );
    
    PWMM_TAB_SIAL_REF(a) = 0;
    PWMM_TAB_SIZE_REF(a) = 0;
    PWMM_TAB_USED_REF(a) = 0;
    PWMM_TAB_LASF_REF(a) = 0;
    PWMM_TAB_TABL_REF(a) = NULL;
    PWMM_TAB_PREV_REF(a) = NULL;
}

void pwmm_table_clear_and_free( pwmm_table_t a ){
    for (ulong i=0; i<PWMM_TAB_SIZE_REF(a); i++)
        if ( PWMM_TAB_USE( a, i ) )
            PWMM_FREE( PWMM_TAB_PTR( a, i ) );
    
    pwmm_table_clear(a);
}

void pwmm_table_empty( pwmm_table_t a ){
//     PWMM_TAB_SIAL_REF(a) = 0;
    PWMM_TAB_SIZE_REF(a) = 0;
    PWMM_TAB_USED_REF(a) = 0;
    PWMM_TAB_LASF_REF(a) = 0;
}

int  pwmm_table_is_empty( pwmm_table_t a ){
    return ( PWMM_TAB_SIZE_REF(a) == 0);
}

void pwmm_table_print( pwmm_table_t a ){
#ifndef PW_SILENT
    printf("size allocated: %lu, size: %lu, used: %lu, last: %lu \n", PWMM_TAB_SIAL_REF(a), 
                                                                     PWMM_TAB_SIZE_REF(a), 
                                                                     PWMM_TAB_USED_REF(a),
                                                                     PWMM_TAB_LASF_REF(a));
    printf("blocks: [ ");
    for(ulong i=0; i<PWMM_TAB_SIZE_REF(a); i++) {
        printf("(%p, %lu, %d), ", (void *) PWMM_TAB_PTR(a,i), (unsigned long int) PWMM_TAB_SIZ(a,i), PWMM_TAB_USE(a,i) );
    }
    printf("]");
#endif
}

void pwmm_table_print_short( pwmm_table_t a ){
#ifndef PW_SILENT
    printf("size allocated: %lu, size: %lu, used: %lu, last: %lu ", PWMM_TAB_SIAL_REF(a), 
                                                                     PWMM_TAB_SIZE_REF(a), 
                                                                     PWMM_TAB_USED_REF(a),
                                                                     PWMM_TAB_LASF_REF(a));
//     printf("blocks: [ ");
//     for(ulong i=0; i<PWMM_TAB_SIZE_REF(a); i++) {
//         printf("(%p, %lu, %d), ", PWMM_TAB_PTR(a,i), PWMM_TAB_SIZ(a,i), PWMM_TAB_USE(a,i) );
//     }
//     printf("]");
#endif
}

void pwmm_table_insert_back( pwmm_table_t a, PWMM_PTR ptr, PWMM_SIZ size, int used ) {
    
    if ( PWMM_TAB_SIZE_REF(a) >= PWMM_TAB_SIAL_REF(a) ) {
        PWMM_TAB_SIAL_REF(a) += PWMM_ALLOC_SIZE;
//         printf("pwmm_table_insert_back: before realloc \n");
        PWMM_TAB_TABL_REF(a) = (pwmm_block_ptr) PWMM_REALLOC ( PWMM_TAB_TABL_REF(a), 
                                                               PWMM_TAB_SIAL_REF(a)*sizeof(struct pwmm_block) );
//         printf("pwmm_table_insert_back: after realloc \n");
    }
    
    PWMM_TAB_PTR( a, PWMM_TAB_SIZE_REF(a) ) = ptr;
    PWMM_TAB_SIZ( a, PWMM_TAB_SIZE_REF(a) ) = size;
    PWMM_TAB_USE( a, PWMM_TAB_SIZE_REF(a) ) = used;
    PWMM_TAB_SIZE_REF(a)++;
    if (used)
        PWMM_TAB_USED_REF(a)++;
}

/* assume 0 <= pos < PWMM_TAB_SIZE_REF(a)                           */
/* insert (ptr, size, used) at position pos in PWMM_TAB_TABL_REF(a) */
/* and possibly extra (ptr', 0, 0) just after pos                   */ 
void pwmm_table_insert_at( pwmm_table_t a, ulong pos, PWMM_PTR ptr, PWMM_SIZ size, int used ){
    
    if ( PWMM_TAB_USE( a, pos ) == 0 ) {
        /* write (ptr, size, used) at position pos */
        PWMM_TAB_PTR( a, pos ) = ptr;
        PWMM_TAB_SIZ( a, pos ) = size;
        PWMM_TAB_USE( a, pos ) = used;
        if (used)
            PWMM_TAB_USED_REF(a)++;
        
        return;
    }
    
    if ( (pos==0) || (pos==(PWMM_TAB_SIZE_REF(a)-1)) ) {
        /* reallocate the table if necessary */
        if ( PWMM_TAB_SIZE_REF(a) >= PWMM_TAB_SIAL_REF(a) ) {
            PWMM_TAB_SIAL_REF(a) += PWMM_ALLOC_SIZE;
            PWMM_TAB_TABL_REF(a) = (pwmm_block_ptr) PWMM_REALLOC ( PWMM_TAB_TABL_REF(a), 
                                                                   PWMM_TAB_SIAL_REF(a)*sizeof(struct pwmm_block) );
        }
        /* move all the table at the right of pos */
        PWMM_MEMMOVE( ((PWMM_PTR)PWMM_TAB_TABL_REF(a) + (pos+1)*sizeof(struct pwmm_block)), /* dest */
                      ((PWMM_PTR)PWMM_TAB_TABL_REF(a) +     pos*sizeof(struct pwmm_block)), /* src */
                      (PWMM_TAB_SIZE_REF(a)-pos)*sizeof(struct pwmm_block) );   /* size */
        /* write (ptr, size, used) at position pos */
        PWMM_TAB_PTR( a, pos ) = ptr;
        PWMM_TAB_SIZ( a, pos ) = size;
        PWMM_TAB_USE( a, pos ) = used;
    
        PWMM_TAB_SIZE_REF(a)++;
        if (used)
            PWMM_TAB_USED_REF(a)++;
        
        return;
    }
    
    if ( PWMM_TAB_USE( a, pos + 1 ) == 0 ){
        /* copy record at position pos to position pos+1 */
        PWMM_TAB_PTR( a, pos+1 ) = PWMM_TAB_PTR( a, pos );
        PWMM_TAB_SIZ( a, pos+1 ) = PWMM_TAB_SIZ( a, pos );
        PWMM_TAB_USE( a, pos+1 ) = PWMM_TAB_USE( a, pos );
        /* write (ptr, size, used) at position pos */
        PWMM_TAB_PTR( a, pos ) = ptr;
        PWMM_TAB_SIZ( a, pos ) = size;
        PWMM_TAB_USE( a, pos ) = used;
        if (used)
            PWMM_TAB_USED_REF(a)++;
        
        return;
    }
    
    /* number of slots to be inserted */
    ulong nb_max_inserted = 1000;
    /* size available in memory between the end of inserted record at position pos */
    ulong size_available = ( PWMM_TAB_PTR( a, pos ) - ptr ) - size;
    ulong nb_empty_slot_to_create=size_available/size;
    nb_empty_slot_to_create = PWMM_MIN( nb_empty_slot_to_create, nb_max_inserted );
//     printf("pwmm_table_insert_at: pos: %lu/%lu, ptr: %p, size: %lu, record at pos-1: (%p, %lu, %d), record at pos: (%p, %lu, %d)\n",
//            pos, PWMM_TAB_SIZE_REF(a), ptr, size, PWMM_TAB_PTR( a, pos-1 ), PWMM_TAB_SIZ( a, pos-1 ), PWMM_TAB_USE( a, pos-1 ),
//            PWMM_TAB_PTR( a, pos ), PWMM_TAB_SIZ( a, pos ), PWMM_TAB_USE( a, pos ) );
//     printf("                    : size available: %lu, nb_empty_slot_to_create: %lu\n",
//            size_available, nb_empty_slot_to_create );
    /* reallocate the table if necessary */
    ulong old_size = PWMM_TAB_SIAL_REF(a);
    while ( ( PWMM_TAB_SIZE_REF(a) + nb_empty_slot_to_create ) >= PWMM_TAB_SIAL_REF(a) )
        PWMM_TAB_SIAL_REF(a) += PWMM_ALLOC_SIZE;
    
    if ( PWMM_TAB_SIAL_REF(a) > old_size )
        PWMM_TAB_TABL_REF(a) = (pwmm_block_ptr) PWMM_REALLOC ( PWMM_TAB_TABL_REF(a), 
                                                               PWMM_TAB_SIAL_REF(a)*sizeof(struct pwmm_block) );
    
    /* move the table of 1+nb_empty_slot_to_create slots */
    PWMM_MEMMOVE( ((PWMM_PTR)PWMM_TAB_TABL_REF(a) + (pos+1+nb_empty_slot_to_create)*sizeof(struct pwmm_block)), /* dest */
                  ((PWMM_PTR)PWMM_TAB_TABL_REF(a) +  pos*sizeof(struct pwmm_block)), /* src */
                 (PWMM_TAB_SIZE_REF(a)-pos)*sizeof(struct pwmm_block) );   /* size */
    /* write (ptr, size, used) at position pos */
    PWMM_TAB_PTR( a, pos ) = ptr;
    PWMM_TAB_SIZ( a, pos ) = size;
    PWMM_TAB_USE( a, pos ) = used;
    if (used)
        PWMM_TAB_USED_REF(a)++;
    PWMM_TAB_SIZE_REF(a)++;
    /* write the empty slots */
    for (ulong i=1; i<= nb_empty_slot_to_create; i++) {
        PWMM_TAB_PTR( a, pos+i ) = ptr + (i*size);
        PWMM_TAB_SIZ( a, pos+i ) = 0;
        PWMM_TAB_USE( a, pos+i ) = 0;
        PWMM_TAB_SIZE_REF(a)++;
    }
    
    PWMM_TAB_LASF_REF(a) = pos+1;
    
    return;
}

/* assume a->_table is sorted by increasing ptr */
/* returns b s.t.: */
/* either b==-1, in which case either a is empty */
/*               or (a->_table[0])._ptr > ptr */
/*        or b>=0, in which case (a->_table[b])._ptr <= ptr and 
 *                 either b==table->_size -1 or (a->_table[b+1])._ptr > ptr */
slong pwmm_table_search( pwmm_table_t a, PWMM_PTR ptr ) {
    
#ifdef PWMM_STATS
    nb_call_to_search +=1;
//     printf("--- search, size of table: %lu\n", PWMM_TAB_SIZE_REF(a));
//     printf("--- search, size of table: %lu, used: %lu, last found: %lu, l2_found: %lu, last: %lu \n", PWMM_TAB_SIZE_REF(a), PWMM_TAB_USED_REF(a), l1_found, l2_found, PWMM_TAB_LASF_REF(a));
//     int nbit=0;
#endif
//     printf("--- search, size of table: %lu, used: %lu\n", PWMM_TAB_SIZE_REF(a), PWMM_TAB_USED_REF(a));
    
    if ( PWMM_TAB_SIZE_REF(a)<=0 )
        return -1;
    
    if ( PWMM_TAB_LASF_REF(a) < (PWMM_TAB_SIZE_REF(a)-1) )
        if (   PWMM_PTR_LE( PWMM_TAB_PTR( a, PWMM_TAB_LASF_REF(a) ), ptr )
            && PWMM_PTR_LT( ptr, PWMM_TAB_PTR( a, PWMM_TAB_LASF_REF(a) + 1 )  ) ) {
            slong res = PWMM_TAB_LASF_REF(a);
#ifdef PWMM_STATS
            nb_hit_with_heur +=1;
//             printf("--- search, nb of iterations: %d, position: %lu (HIT)\n", nbit, res);
#endif
            PWMM_TAB_LASF_REF(a) = 0;
            return res;
        }

    if ( PWMM_PTR_LE( PWMM_TAB_PTR( a, PWMM_TAB_SIZE_REF(a)-1 ), ptr) ) {
#ifdef PWMM_STATS
            nb_hit_with_last +=1;
//             printf("--- search, nb of iterations: %d, position: %lu (HIT)\n", nbit, PWMM_TAB_SIZE_REF(a)-1);
#endif
        return (slong) ( PWMM_TAB_SIZE_REF(a)-1 );
    }
    
    if ( PWMM_PTR_LT( ptr, PWMM_TAB_PTR( a, 0 )  ) )
        return -1;
    
// #ifdef PWMM_STATS
//     clock_t start = clock();
// #endif
    
    ulong l=0, u=PWMM_TAB_SIZE_REF(a)-1;
    ulong m;
    /*here and below PWMM_TAB_PTR( a, l ) <= ptr < PWMM_TAB_PTR( a, u ) */
    while (u-l>1) {
        m = (l+u)/2;
        if (PWMM_PTR_LE( PWMM_TAB_PTR( a, m ), ptr) )
            l = m;
        else
            u = m;
// #ifdef PWMM_STATS        
//         nbit++;
// #endif
    }
    
// #ifdef PWMM_STATS
// //     printf("--- search, nb of iterations: %d, position: %lu\n", nbit, l);
//     time_in_search += (clock() - start);
// #endif

    return (slong) l;
}

/* assume a->_table is sorted by increasing ptr */
/* insert new ptr while keeping a sorted */
void pwmm_table_insert( pwmm_table_t a, PWMM_PTR ptr, PWMM_SIZ size, int used ){
    
    if ( PWMM_TAB_SIZE_REF(a)<=0 ) {
        pwmm_table_insert_back( a, ptr, size, used );
        return;
    }

// #ifdef PWMM_STATS
//     clock_t start = clock();
// #endif
    
    /* assume a is not empty */
    slong pos = pwmm_table_search( a, ptr );
    
// #ifdef PWMM_STATS
//     time_in_sea_in_in += (clock() - start);
// #endif
    
    if (pos==-1) { /* (a->_table[0])._ptr > ptr => insert in the beginning */
        if ( PWMM_TAB_USE(a, 0) == 0 ) {
            PWMM_TAB_PTR( a, 0 ) = ptr;
            PWMM_TAB_SIZ( a, 0 ) = size;
            if ( (used) && (!PWMM_TAB_USE( a, 0 )) )
                PWMM_TAB_USED_REF(a)++;
            PWMM_TAB_USE( a, 0 ) = used;
        } else {
            pwmm_table_insert_at( a, 0, ptr, size, used );
        }
    } else { /* assume 0 <= pos <= a->_size-1 */
        
        if (PWMM_TAB_PTR( a, pos )==ptr) {
//             if (PWMM_TAB_SIZ( a, pos ) > 10*size)
//                 printf("pwmm_table_insert: re-use a slot of size %lu for size %lu; ptr: %p, next: (%p, %lu, %d), pos: %ld, size: %lu\n", PWMM_TAB_SIZ( a, pos ), size, PWMM_TAB_PTR( a, pos ), 
//                 PWMM_TAB_PTR( a, pos + 1 ), PWMM_TAB_SIZ( a, pos + 1 ), PWMM_TAB_USE( a, pos + 1 ),
//                 pos, PWMM_TAB_SIZE_REF(a) );
            PWMM_TAB_PTR( a, pos ) = ptr;
            PWMM_TAB_SIZ( a, pos ) = size;
            if ( (used) && (!PWMM_TAB_USE( a, pos )) )
                PWMM_TAB_USED_REF(a)++;
            PWMM_TAB_USE( a, pos ) = used;
        } else { /* assume (a->_table[pos])._ptr < ptr */
            if ( PWMM_TAB_USE(a, pos) == 0 ) {
//                 if (PWMM_TAB_SIZ( a, pos ) > 10*size)
//                     printf("pwmm_table_insert: re-use a slot of size %lu for size %lu; ptr: %p, next: (%p, %lu, %d), pos: %ld, size: %lu\n", PWMM_TAB_SIZ( a, pos ), size, PWMM_TAB_PTR( a, pos ), 
//                 PWMM_TAB_PTR( a, pos + 1 ), PWMM_TAB_SIZ( a, pos + 1 ), PWMM_TAB_USE( a, pos + 1 ),
//                 pos, PWMM_TAB_SIZE_REF(a) );
                PWMM_TAB_PTR( a, pos ) = ptr;
                PWMM_TAB_SIZ( a, pos ) = size;
                if ( (used) && (!PWMM_TAB_USE( a, pos )) )
                    PWMM_TAB_USED_REF(a)++;
                PWMM_TAB_USE( a, pos ) = used;
                
            } else {
                if (pos==((slong) PWMM_TAB_SIZE_REF(a)-1)) {
                    pwmm_table_insert_back( a, ptr, size, used );
                } else { /* assume pos < a->_size-1 and (a->_table[pos])._ptr < ptr */
                    if ( PWMM_TAB_USE(a, pos+1) == 0 ) {
                        PWMM_TAB_PTR( a, pos+1 ) = ptr;
                        PWMM_TAB_SIZ( a, pos+1 ) = size;
                        if ( (used) && (!PWMM_TAB_USE( a, pos+1 )) )
                            PWMM_TAB_USED_REF(a)++;
                        PWMM_TAB_USE( a, pos+1 ) = used;
                    } else {
//                         printf("pwmm_table_insert   : insert a slot at pos %lu between one of size %lu (%d) and one of size %lu (%d) for size %lu\n", pos+1,
//                                PWMM_TAB_SIZ( a, pos ), PWMM_TAB_USE( a, pos ), PWMM_TAB_SIZ( a, pos+1 ), PWMM_TAB_USE( a, pos+1 ), size );
                        pwmm_table_insert_at( a, pos+1, ptr, size, used );
                    }
                }
            }
        }
        
    }
/*#ifdef PWMM_STATS
    time_in_insert += (clock() - start);
#endif */   
    
    return;
}

slong pwmm_table_search_for_free_and_realloc( pwmm_table_t a, PWMM_PTR ptr ) {
    
    slong res = pwmm_table_search( a, ptr );
    if ( (res>=0) && ( (PWMM_TAB_PTR( a, res ) != ptr ) || (PWMM_TAB_USE(a,res)==0) ) )
        res = -1;
    
    return res;
}

/* assume 0<=pos<=a->_size-1 */
MM_INLINE void pwmm_table_set_unused( pwmm_table_t a, ulong pos ){
    if (PWMM_TAB_USE( a, pos ))
        PWMM_TAB_USED_REF(a)--;
    PWMM_TAB_USE( a, pos ) = 0;
}

MM_INLINE void pwmm_table_set_used( pwmm_table_t a, ulong pos ){
    if (PWMM_TAB_USE( a, pos )==0)
        PWMM_TAB_USED_REF(a)++;
    PWMM_TAB_USE( a, pos ) = 1;
}

MM_INLINE void pwmm_table_set_size( pwmm_table_t a, ulong pos, PWMM_SIZ size ){
    PWMM_TAB_SIZ( a, pos ) = size;
}

MM_INLINE void pwmm_table_set_last_freed( pwmm_table_t a, ulong pos ) {
    PWMM_TAB_LASF_REF(a) = pos;
}

PWMM_PTR pwmm_malloc( PWMM_SIZ size, pwmm_table_t mm ) {
    
#ifdef PWMM_STATS
    clock_t start = clock();
    nb_call_malloc++;
//     printf("---malloc: \n");
#endif
    
    
    PWMM_PTR ptr = PWMM_MALLOC (size);
    pwmm_table_insert( mm, ptr, size, 1 );
    
#ifdef PWMM_STATS
    time_in_malloc += (clock() - start);
#endif
    
    return ptr;
}

PWMM_PTR pwmm_realloc( PWMM_PTR old_ptr, PWMM_SIZ new_size, pwmm_table_t mm ) {
    
    PWMM_PTR ptr;
    
    if (old_ptr!=NULL) {
        
#ifdef PWMM_STATS
        clock_t start = clock();
        nb_call_realloc++;
//         printf("---realloc: \n");
#endif
        
        pwmm_table_ptr nm = mm;
        slong pos = pwmm_table_search_for_free_and_realloc ( nm, old_ptr );
        /* if not found in mm, search it in previous tables  */
        /* not used in current code: pos is necessarily >= 0 */
        while ( (PWMM_TAB_PREV_REF(nm)!=NULL)&&(pos==-1) ) {
            nm = PWMM_TAB_PREV_REF(nm);
            pos = pwmm_table_search_for_free_and_realloc ( nm, old_ptr );
//             printf("realloc: search ptr:%p in global table, found pos: %ld\n", old_ptr, pos);
        }
        ptr = PWMM_REALLOC (old_ptr, new_size);
        
//         printf("realloc: new ptr:%p, new size: %lu \n", ptr, new_size);
        
        if (ptr!=old_ptr) {
            pwmm_table_set_unused( nm, pos );
            pwmm_table_insert( nm, ptr, new_size, 1 );
            
//             printf("realloc pos: %ld\n", pos);
//             printf("realloc (%p, %lu, %d), (%p, %lu, %d), (%p, %lu, %d) \n", 
//                     PWMM_TAB_PTR(nm,pos-1), PWMM_TAB_SIZ(nm,pos-1), PWMM_TAB_USE(nm,pos-1),
//                     PWMM_TAB_PTR(nm,pos),   PWMM_TAB_SIZ(nm,pos),   PWMM_TAB_USE(nm,pos),
//                     PWMM_TAB_PTR(nm,pos+1), PWMM_TAB_SIZ(nm,pos+1), PWMM_TAB_USE(nm,pos+1)
//             );
            
        } else {
            pwmm_table_set_size( nm, pos, new_size );
            pwmm_table_set_used( nm, pos );
        }
    
#ifdef PWMM_STATS
        time_in_realloc += (clock() - start);
#endif
    } else {
        ptr = pwmm_malloc( new_size, mm );
    }
    
//     printf("\n");
    
    return ptr;
}

PWMM_PTR pwmm_calloc( PWMM_SIZ num, PWMM_SIZ size, pwmm_table_t mm ) {
    
#ifdef PWMM_STATS
    clock_t start = clock();
    nb_call_calloc++;
//     printf("---calloc: \n");
#endif
    
    PWMM_PTR ptr = PWMM_CALLOC (num, size);
    pwmm_table_insert( mm, ptr, num*size, 1 );
    
#ifdef PWMM_STATS
    time_in_calloc += (clock() - start);
#endif
    
    return ptr;
}

void     pwmm_free( PWMM_PTR ptr, pwmm_table_t mm ) {
    
#ifdef PWMM_STATS
    clock_t start = clock();
    nb_call_free++;
//     printf("---free: \n");
#endif
    
    if (ptr!=NULL) {
        pwmm_table_ptr nm = mm;
//      
        slong pos = pwmm_table_search_for_free_and_realloc ( nm, ptr );
        /* if not found in mm, search it in previous tables  */
        /* not used in current code: pos is necessarily >= 0 */
        while ( (PWMM_TAB_PREV_REF(nm)!=NULL)&&(pos==-1) ) {
//             printf("free: search ptr:%p in global table\n", ptr);
            nm = PWMM_TAB_PREV_REF(nm);
            pos = pwmm_table_search_for_free_and_realloc ( nm, ptr );
        }
        PWMM_FREE(ptr);
        pwmm_table_set_unused( nm, pos );
        pwmm_table_set_last_freed( nm, pos );
        
    }
    
//     if (PWMM_TAB_SIZE_REF(mm) > 2*PWMM_TAB_USED_REF(mm) )
//         pwmm_table_defrag(mm);

#ifdef PWMM_STATS
    time_in_free += (clock() - start);
#endif
    
    return;
}

void     pwmm_cleanup( pwmm_table_t mm ) {
    pwmm_table_clear_and_free(mm);
}

/* NOT USED */
void pwmm_table_defrag(pwmm_table_t a){
    
    if (PWMM_TAB_SIAL_REF(a)==0)
        return;
        
//     printf("pwmm_table_defrag: table before defrag\n");
//     pwmm_table_print_short(a);
//     printf("\n");
    
    /* allocate a temporary table of size PWMM_TAB_SIAL_REF(a) */
    pwmm_block_ptr temp = (pwmm_block_ptr) PWMM_MALLOC ( PWMM_TAB_SIAL_REF(a)*sizeof(struct pwmm_block) );
    /* copy PWMM_TAB_TABL_REF(a) in temp with no sequences of more than one unused addresses */
    ulong j=0;
    for (ulong i=0; i< PWMM_TAB_SIZE_REF(a); i++) {
        temp[j]._ptr = PWMM_TAB_PTR(a,i);
        temp[j]._size= PWMM_TAB_SIZ(a,i);
        temp[j]._used= PWMM_TAB_USE(a,i);
        j++;
        if ( PWMM_TAB_USE(a,i) == 0 ) {
            /* go to first following used address */
            while ( ((i+1)<PWMM_TAB_SIZE_REF(a)) && ( PWMM_TAB_USE(a,i+1) == 0 ) )
                i++;
        } 
    }
    PWMM_TAB_SIZE_REF(a) = j;
    PWMM_TAB_LASF_REF(a) = 0;
    /* re copy j first elements of temporary table in PWMM_TAB_TABL_REF(a) */
    /* could be faster with a memcpy ? */
    for (ulong i=0; i< j; i++) {
        PWMM_TAB_PTR(a,i) =temp[i]._ptr  ;
        PWMM_TAB_SIZ(a,i) =temp[i]._size ;
        PWMM_TAB_USE(a,i) =temp[i]._used ;
    }
    
//     printf("pwmm_table_defrag: table after defrag\n");
//     pwmm_table_print_short(a);
//     printf("\n");
    /* de allocate the temporary table */
    PWMM_FREE(temp);
}

void pwmm_table_merge( pwmm_table_t a, pwmm_table_t b ) {
    
    /* allocate a new table with sufficient size */
    ulong nb_in_new_table = PWMM_TAB_USED_REF(a)+PWMM_TAB_USED_REF(b);
    ulong fit = PWMM_ALLOC_SIZE;
    while (fit <= nb_in_new_table)
        fit += PWMM_ALLOC_SIZE;
    pwmm_block_ptr new_table = (pwmm_block_ptr) PWMM_MALLOC ( fit*sizeof(struct pwmm_block) );
    
    ulong pos_in_new_table = 0, pos_in_a=0, pos_in_b=0;
    while ( pos_in_new_table < nb_in_new_table ) {
        /* advance pos_in_a, pos_in_b to the first used elements */
        while( ( pos_in_a < PWMM_TAB_SIZE_REF(a) ) && ( PWMM_TAB_USE(a, pos_in_a)==0 ) )
            pos_in_a++;
        while( ( pos_in_b < PWMM_TAB_SIZE_REF(b) ) && ( PWMM_TAB_USE(b, pos_in_b)==0 ) )
            pos_in_b++;
        
        /* copy as many element of a as possible */
        while ( ( pos_in_a < PWMM_TAB_SIZE_REF(a) ) 
                && ( (PWMM_TAB_PTR(a, pos_in_a) <= PWMM_TAB_PTR(b, pos_in_b) ) || (pos_in_b>=PWMM_TAB_SIZE_REF(b)) ) ) {
            if PWMM_TAB_USE(a, pos_in_a) {
                (new_table+pos_in_new_table)->_ptr  = PWMM_TAB_PTR(a,pos_in_a);
                (new_table+pos_in_new_table)->_size = PWMM_TAB_SIZ(a,pos_in_a);
                (new_table+pos_in_new_table)->_used = 1;
                pos_in_new_table++;
            }
            pos_in_a++;
        }
        /* copy as many element of b as possible */
        while ( ( pos_in_b < PWMM_TAB_SIZE_REF(b) ) 
                && ( (PWMM_TAB_PTR(b, pos_in_b) <= PWMM_TAB_PTR(a, pos_in_a) ) || (pos_in_a>=PWMM_TAB_SIZE_REF(a)) ) ) {
            if PWMM_TAB_USE(b, pos_in_b) {
                (new_table+pos_in_new_table)->_ptr  = PWMM_TAB_PTR(b,pos_in_b);
                (new_table+pos_in_new_table)->_size = PWMM_TAB_SIZ(b,pos_in_b);
                (new_table+pos_in_new_table)->_used = 1;
                pos_in_new_table++;
                pwmm_table_set_unused(b,pos_in_b);
            }
            pos_in_b++;
        }
    }
    PWMM_TAB_SIAL_REF(a) = fit;
    PWMM_TAB_SIZE_REF(a) = nb_in_new_table;
    PWMM_TAB_USED_REF(a) = nb_in_new_table;
    PWMM_TAB_LASF_REF(a) = 0;
    PWMM_FREE(PWMM_TAB_TABL_REF(a));
    PWMM_TAB_TABL_REF(a) = new_table;
}
