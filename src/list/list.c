/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_list.h"

// void list_init(list_t l, clear_func clear) {
void list_init(list_t l) {
    l->_begin = l->_end = NULL;
    l->_last_inserted = NULL;
    l->_size  = 0;
//     l->_clear = clear;
}

void list_swap(list_t l1, list_t l2){
    struct elmt *begin = l1->_begin;
    struct elmt *end   = l1->_end  ;
    struct elmt *last_inserted   = l1->_last_inserted  ;
    int              size  = l1->_size ;
//     clear_func       clear = l1->_clear;
    l1->_begin = l2->_begin;
    l1->_end   = l2->_end;
    l1->_last_inserted   = l2->_last_inserted;
    l1->_size  = l2->_size;
//     l1->_clear = l2->_clear;
    l2->_begin = begin;
    l2->_end   = end;
    l2->_last_inserted   = last_inserted;
    l2->_size  = size;
//     l2->_clear = clear;
}

/* appends elements of l2 to l1 */
/* assume clear_func is the same for l1 and l2 */
/* let l2 empty */
void list_append(list_t l1, list_t l2){
    if (list_is_empty(l2))
        return;
    if (list_is_empty(l1)) {
        list_swap(l1, l2);
        return;
    }
    l1->_end->_next = l2->_begin;
    l2->_begin->_prev = l1->_end;
    l1->_end = l2->_end;
    l2->_begin = NULL;
    l2->_end   = NULL;
    l1->_size += l2->_size;
    l2->_size = 0;
}

void list_clear(list_t l, clear_func clear){
    
    struct elmt * voyager = l->_begin;
    
    while (l->_begin!=l->_end) {
        voyager = l->_begin;
//         printf("clearing elmt: %d\n", *((int *) voyager->_elmt) );
//         printf("         next: %p\n", voyager->_next);
        l->_begin = l->_begin->_next;
//         l->_clear(voyager->_elmt);
        clear(voyager->_elmt);
        pwpoly_free(voyager->_elmt);
        pwpoly_free(voyager);
    }
    
    if (l->_begin!=NULL){
//         l->_clear(l->_begin->_elmt);
//         printf("clearing elmt: %d\n", *((int *) l->_begin->_elmt) );
//         printf("         next: %p\n", l->_begin->_next);
        clear(l->_begin->_elmt);
        pwpoly_free(l->_begin->_elmt);
        pwpoly_free(l->_begin);
        l->_begin = l->_end = NULL;
    }

    l->_begin = l->_end = NULL;
    l->_last_inserted = NULL;
    l->_size  = 0;
// #else
//     l->_begin = l->_end = NULL;
//     l->_last_inserted = NULL;
//     l->_size  = 0;
// #endif
}

/* empty the list with NO delete of elements */
void list_empty(list_t l){
    while (!list_is_empty(l))
        list_pop(l);
}

/* push in the back of the list */
void list_push(list_t l, void * data){
    struct elmt * nelmt = (struct elmt *) pwpoly_malloc (sizeof(struct elmt));
    nelmt->_elmt = data;
    nelmt->_next = NULL;
    /* test for union find in large lists of boxes */
    nelmt->_prev = l->_end;
    
    if (l->_begin == NULL) {
        l->_begin = l->_end = nelmt;
    }
    else {
        l->_end->_next = nelmt;
        l->_end = nelmt;
    }
    l->_size +=1;
}

/* push in the front of the list */
void list_push_front(list_t l, void * data){
    struct elmt * nelmt = (struct elmt *) pwpoly_malloc (sizeof(struct elmt));
    nelmt->_elmt = data;
    nelmt->_next = l->_begin;
    nelmt->_prev = NULL;
    
    if (l->_begin == NULL) {
        l->_begin = l->_end = nelmt;
    }
    else {
        l->_begin->_prev = nelmt;
        l->_begin = nelmt;
    }
    l->_size +=1;
}

/* pop the front of the list */
void * list_pop(list_t l){
    void * res;
    struct elmt * temp = l->_begin;
    
    if (l->_last_inserted == l->_begin)
        l->_last_inserted = NULL;
    
    if (l->_begin == NULL) {
//         printf("ici!!!!\n");
        res = NULL;
    }
    else {
        res = l->_begin->_elmt;
        if (l->_begin == l->_end)
            l->_begin = l->_end = NULL;
        else {
            l->_begin = l->_begin->_next;
            /* test for union find in large lists of boxes */
            l->_begin->_prev = NULL;
        }
        pwpoly_free(temp);
        l->_size -=1;
    }
    
    return res;
}

/* test for union find in large lists of boxes */
void * list_pop_back(list_t l){
    void * res;
    struct elmt * temp = l->_end;
    
    if (l->_last_inserted == l->_end)
        l->_last_inserted = NULL;
    
    if (l->_end == NULL) {
        res = NULL;
    }
    else {
        res = l->_end->_elmt;
        if (l->_begin == l->_end)
            l->_begin = l->_end = NULL;
        else {
            l->_end = l->_end->_prev;
            l->_end->_next = NULL;
        }
        pwpoly_free(temp);
        l->_size -=1;
    }
    return res;
}

void * list_first(list_t l){
    if (l->_begin == NULL)
        return NULL;
    else
        return (l->_begin)->_elmt;
}

void * list_last(list_t l){
    if (l->_end == NULL)
        return NULL;
    else
        return (l->_end)->_elmt;
}

void * list_data_at_index(const list_t l, slong index){
    struct elmt * voyager = l->_begin;
    for (int i=0;i<index; i++)
        voyager = voyager->_next;
    return voyager->_elmt;
}
    
void list_insert_sorted(list_t l, void * data, int (isless_func)(const void * d1, const void * d2)){
    
    struct elmt * voyager = l->_begin;
    struct elmt * nelmt = (struct elmt *) pwpoly_malloc (sizeof(struct elmt));
    nelmt->_elmt = data;
    nelmt->_next = NULL;
    nelmt->_prev = NULL;
    
    /* empty list */
    if (voyager == NULL) {
        l->_begin = l->_end = nelmt;
        l->_last_inserted = l->_begin;
    }
    else{
        
        /* try from l->_last_inserted */
        if ( (l->_last_inserted) && isless_func( l->_last_inserted->_elmt, data ) )
            voyager = l->_last_inserted;
        
        if ( isless_func( data, voyager->_elmt ) ){ /* insert at the beginning */
            l->_begin->_prev = nelmt;
            
            nelmt->_next = l->_begin;
            l->_begin = nelmt;
            l->_last_inserted = l->_begin;
        }
        else if ( isless_func( l->_end->_elmt, data ) ){ /* insert at the end */
            
            nelmt->_prev = l->_end;
            l->_end->_next = nelmt;
            l->_end = nelmt;
            l->_last_inserted = l->_end;
            
        }
        else {
                while (voyager->_next!=NULL && isless_func( (voyager->_next->_elmt), data ))
                    voyager = voyager->_next;
            
            if (voyager->_next == NULL) { /* insert at the end */
                
                nelmt->_prev = l->_end;
                l->_end->_next = nelmt;
                l->_end = nelmt;
                l->_last_inserted = l->_end;
                
            }
            else {
                nelmt->_next = voyager->_next;
                nelmt->_prev = voyager;
                voyager->_next->_prev = nelmt;
                voyager->_next = nelmt;
                l->_last_inserted = nelmt;
            }
        }
    }
    l->_size +=1;
}

int list_insert_sorted_unique(list_t l, void * data, int (isless_func)(const void * d1, const void * d2)){
    
    struct elmt * voyager = l->_begin;
    struct elmt * nelmt = (struct elmt *) pwpoly_malloc (sizeof(struct elmt));
    nelmt->_elmt = data;
    nelmt->_next = NULL;
    nelmt->_prev = NULL;
    
    /* empty list */
    if (voyager == NULL) {
        l->_begin = l->_end = nelmt;
        l->_last_inserted = l->_begin;
    } else {
        
        if ( isless_func( data, voyager->_elmt ) ){ /* insert at the beginning */
            l->_begin->_prev = nelmt;
            
            nelmt->_next = l->_begin;
            l->_begin = nelmt;
            l->_last_inserted = l->_begin;
        }
        else if ( isless_func( l->_end->_elmt, data ) ){ /* insert at the end */
            
            nelmt->_prev = l->_end;
            l->_end->_next = nelmt;
            l->_end = nelmt;
            l->_last_inserted = l->_end;
            
        } else {
            
            if ( isless_func( data, l->_end->_elmt )==0 ) {
                pwpoly_free(nelmt);
                return 0;
            }
            
            /* try from l->_last_inserted */
            if ( (l->_last_inserted) && isless_func( l->_last_inserted->_elmt, data ) )
                voyager = l->_last_inserted;
        
            while ( isless_func( voyager->_elmt, data ) )
                voyager = voyager->_next;
        
            /* voyager !=l->_begin because data>=l->_begin->_elmt */
            /* voyager !=NULL because data < l->_end->_elmt       */
            
            /* here isless_func( voyager->_elmt, data )==0  */
            if ( isless_func( data, voyager->_elmt )==0 ) { 
                /* voyager->_elmt==data => data is already in the list */
                pwpoly_free(nelmt);
                return 0;
            }
            /* insert before voyager */
            nelmt->_prev = voyager->_prev;
            nelmt->_next = voyager;
            voyager->_prev->_next = nelmt;
            voyager->_prev = nelmt;
            l->_last_inserted = nelmt;
        }
        
    }
    l->_size +=1;
    return 1;
}

int list_sorted_unique_contains_data(const list_t l, void * data, int (isless_func)(const void * d1, const void * d2)){
    
    struct elmt * voyager = l->_begin;
    
    /* empty list */
    if (voyager == NULL)
        return 0;
    /* non empty list */
    if ( isless_func( data, voyager->_elmt ) ) /* data < l->_begin->_elmt => data is not in list */
        return 0;
    
    if ( isless_func( l->_end->_elmt, data ) ) /* data > l->_end->_elmt => data is not in list */
        return 0;
        
    /* here data <= l->_end->_elmt */ 
    if ( isless_func( data, l->_end->_elmt )==0 ) /* data == l->_end->_elmt => data is in list */
        return 1;
     
    /* here data >= l->_begin->_elmt */
    /* here data < l->_end->_elmt */
    /* try from l->_last_inserted */
    if ( (l->_last_inserted) && isless_func( l->_last_inserted->_elmt, data ) )
        voyager = l->_last_inserted;
        
    while ( isless_func( voyager->_elmt, data ) )
        voyager = voyager->_next;
    
    /* voyager !=l->_begin because data>=l->_begin->_elmt */
    /* voyager !=NULL because data < l->_end->_elmt       */
    /* here isless_func( voyager->_elmt, data )==0  */
    if ( isless_func( data, voyager->_elmt )==0 ) /* voyager->_elmt==data => data is already in the list */
        return 1;
        
    return 0;
}

#ifndef PW_SILENT
void list_fprint(FILE * file, const list_t l, void (* print_func)(FILE *, const void *) ){
    fprintf(file, "length: %ld, elements: [", l->_size);
    fprintf(file, "\n");
    struct elmt * voyager = l->_begin;
    while (voyager != NULL) {
        print_func(file, voyager->_elmt);
        if (voyager->_next != NULL)
            fprintf(file, ", \n");
        else
            fprintf(file, "\n");
        voyager = voyager->_next;
    }
    fprintf(file, "]");
}
#endif

// void list_fprint_deep(FILE * file, const list_t l, void (* print_func)(FILE *, const void *) ){
//     fprintf(file, "length: %ld, elements: [", l->_size);
//     fprintf(file, "\n");
//     struct elmt * voyager = l->_begin;
//     while (voyager != NULL) {
//         print_func(file, voyager->_elmt);
//         if (voyager->_next != NULL)
//             fprintf(file, ", \n");
//         else
//             fprintf(file, "\n");
//         voyager = voyager->_next;
//     }
//     fprintf(file, "]");
// }

#ifndef PW_SILENT
void list_fprintd(FILE * file, const list_t l, slong digits, void (* print_func)(FILE *, const void *, slong digits) ){

    fprintf(file, "length: %ld, elements: [", l->_size);
    fprintf(file, "\n");
    struct elmt * voyager = l->_begin;
    while (voyager != NULL) {
        print_func(file, voyager->_elmt, digits);
        if (voyager->_next != NULL)
            fprintf(file, ", \n");
        else
            fprintf(file, "\n");
        voyager = voyager->_next;
    }
    fprintf(file, "]"); 
}
#endif

int list_is_empty(const list_t l){
    return (l->_size==0);
}
slong list_get_size(const list_t l){
    return l->_size;
}

/* OLD */
// int list_insert_sorted_unique(list_t l, void * data, int (isless_func)(const void * d1, const void * d2)){
//     
//     struct elmt * voyager = l->_begin;
//     struct elmt * nelmt = (struct elmt *) pwpoly_malloc (sizeof(struct elmt));
//     nelmt->_elmt = data;
//     nelmt->_next = NULL;
//     nelmt->_prev = NULL;
//     
//     /* empty list */
//     if (voyager == NULL) {
//         l->_begin = l->_end = nelmt;
//     } else {
//         
//         while ( (voyager!=NULL) && isless_func( voyager->_elmt, data ) )
//             voyager = voyager->_next;
//         if (voyager == NULL) { /* insert at the end */
// //             printf("%ld, insert end\n", l->_size);
//             nelmt->_prev = l->_end;
//             l->_end->_next = nelmt;
//             l->_end = nelmt;
//         } else { /* here isless_func( voyager->_elmt, data )==0 */
//             if ( isless_func( data, voyager->_elmt )==0 ) { /* data is already in the list */
//                 pwpoly_free(nelmt);
//                 return 0;
//             }
//             /* insert before voyager */
//             nelmt->_prev = voyager->_prev;
//             nelmt->_next = voyager;
//             if (nelmt->_prev==NULL) {
// //                 printf("%ld, insert beginning\n", l->_size);
//                 l->_begin = nelmt;
//             }
//             else {
// //                 printf("%ld, insert \n", l->_size);
//                 voyager->_prev->_next = nelmt;
//             }
//             voyager->_prev = nelmt;
//         }
//         
//     }
//     l->_size +=1;
//     return 1;
// }
