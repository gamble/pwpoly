/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_LIST_H
#define PW_LIST_H

#include "pw_base.h"

#ifdef __cplusplus
extern "C" {
#endif
    
struct elmt {
    void        *_elmt;
    struct elmt *_prev;
    struct elmt *_next;
};

typedef struct elmt *elmt_ptr;

typedef void (*clear_func)(void *);

struct list {
    struct elmt *_begin;
    struct elmt *_end;
    struct elmt *_last_inserted;
    slong            _size;
//     clear_func       _clear;
    
};

typedef struct list list_t[1];
typedef struct list *list_ptr;

void list_init(list_t l);

void list_swap(list_t l1, list_t l2);

/* appends elements of l2 to l1 */
/* assume clear_func is the same for l1 and l2 */
/* let l2 empty */
void list_append(list_t l1, list_t l2);

void list_clear(list_t l, clear_func clear);

/* empty the list with no delete of the elements */
void list_empty(list_t l);

/* push in the back of the list */
void list_push(list_t l, void * data);
/* push in the front of the list */
void list_push_front(list_t l, void * data);

/* pop the front of the list */
void * list_pop(list_t l);
/* pop the back of the list */
void * list_pop_back(list_t l);

/* get pointers on first and last elements */
void * list_first(list_t l);
void * list_last(list_t l);
/* get pointer on n-th element */
/* do not check bound!!! */
void * list_data_at_index(const list_t l, slong n);

void list_insert_sorted(list_t l, void * data, int (isless_func)(const void * d1, const void * d2));
/* returns 1 if data is not already in the list */
/* returns 0 if data is already in the list */
int list_insert_sorted_unique(list_t l, void * data, int (isless_func)(const void * d1, const void * d2));
/* returns 1 if data is in the list */
/*         0 otherwise */
int list_sorted_unique_contains_data(const list_t l, void * data, int (isless_func)(const void * d1, const void * d2));

#ifndef PW_SILENT
void list_fprint(FILE * file, const list_t l, void (* print_func)(FILE *, const void *) );
void list_fprintd(FILE * file, const list_t l, slong digits, void (* print_func)(FILE *, const void *, slong digits) );
#endif

int   list_is_empty(const list_t l);
slong list_get_size(const list_t l);

/*iterator */
typedef struct elmt *list_iterator;

PWPOLY_INLINE list_iterator list_begin(const list_t l){
    return l->_begin;
}

/* test for union find in large lists of boxes */
PWPOLY_INLINE list_iterator list_endEl(const list_t l){
    return l->_end;
}

PWPOLY_INLINE list_iterator list_next(list_iterator it){
    return it->_next;
}

/* test for union find in large lists of boxes */
PWPOLY_INLINE list_iterator list_prev(list_iterator it){
    return it->_prev;
}

PWPOLY_INLINE list_iterator list_end(){
    return NULL;
}

PWPOLY_INLINE void * list_elmt(list_iterator it){
    return it->_elmt;
}

/* NOT USED ? */
// void list_clear_for_tables(list_t l);

/* empty the list with NO delete of elements */
// void list_empty(list_t l);
/* copy the list, not the elements */
// void list_copy(list_t ltarget, const list_t lsrc);
// void list_insert_sorted_unique(list_t l, void * data, int (isless_func)(const void * d1, const void * d2),
//                                                               int (isequal_func)(const void * d1, const void * d2));
#ifdef __cplusplus
}
#endif

#endif
