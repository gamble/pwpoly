/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_base.h"

#ifdef PW_CHECK_INTERRUPT
static int _ival;
static int _pwpoly_interrupt ();
static int (*__pwpoly_interrupt) () = _pwpoly_interrupt;

void __pwpoly_set_interrupt_function( interrupt_func ifu ) {
    _ival = 0;
    __pwpoly_interrupt = ifu;
}
void __pwpoly_reset_interrupt_function( ) {
    _ival = 0;
    __pwpoly_interrupt = _pwpoly_interrupt;
}
int _pwpoly_interrupt () {
//     printf("\n\ncheck default interrupt!\n\n");
    return 0;
}
int pwpoly_interrupt (int a) {
#ifndef PW_SILENT
    if (a && (!_ival))
        printf("\n\n interrupt ! \n\n");
#endif
    _ival = _ival||(!(a==0));
    _ival = _ival||(*__pwpoly_interrupt)();
    return _ival;
}
#endif
