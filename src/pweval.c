/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

/*********************/
/* includes          */
/*********************/

#include <stdio.h>
#include <time.h>

/* from pwpoly */
#include "pw_base.h"
#include "common/pw_common.h"     /* for I/O             */
// #include "solving/pw_solving.h"   /* for solving         */
#include "geometry/domain.h"      /* for parsing domains */
#include "evaluating/pw_evaluating.h"   /* for evaluating         */

/*********************/
/* global variables  */
/*********************/
acb_poly_t pacb;
fmpq_poly_t pfmpq_re;
fmpq_poly_t pfmpq_im;
slong length;
int c;
fmpq_t scale, ratio;
int verbosity, check, derVals;
slong m, log2eps, roundInput;

void init_global_variables();

void clear_global_variables();

slong computeCoveringAndGetNbPoints( pw_covering_t cov, int type, slong nbPointsPerSector );
void  generatePointsInCovering( acb_ptr points, pw_covering_t cov, slong nbPointsPerSector );

void shuffle_acb_ptr( acb_ptr points, slong len ) {
    
  /* initialize random seed: */
  srand (time(NULL));
  
  for (slong i = len-1; i>=1; i--) {
      slong j = rand() % (i+1);
      acb_swap(points+i, points+j);
  }
  
}

/*********************/
/* main              */
/*********************/
int main(int argc, char* argv[]) {
    
    int level = 1; /* verbosity level */
    
    if ( (argc<2) || (strcmp( argv[1], "-h" ) == 0) 
                  || (strcmp( argv[1], "--help" ) == 0) ) {
        pweval_fprint_options( stdout, argc, argv );
        exit(0);
    }
    
    init_global_variables();
    
    /* check if an input point file is given in input */
    int inputPointFile = 0;
    if (argc>2) {
        char t = argv[2][0];
//         printf("%c\n", t);
        inputPointFile = !(t=='-');
//         printf("%d\n", inputPointFile);
    }
    
    int parse = parseInputArgsEval( argc+inputPointFile, argv-inputPointFile, &verbosity, &check, &derVals, &log2eps, &m, &roundInput, ratio );
    
    /* type is either PW_PARSE_FAILD, PW_PARSE_EXACT for integer/rational polynomials */
    /*             or PW_PARSE_BALLS for acb poly                                     */
    /* length is the degree+1 of the input poly                                       */
    /* fills pacb if PW_PARSE_BALLS, pfmpq_re and pfmpq_im if PW_PARSE_EXACT          */
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if ( (type==PW_PARSE_FAILD) || (parse==0) ) {
        clear_global_variables();
        flint_cleanup();
        exit(0);
    }
    
    /* input points */
    slong nbPoints = 0;
    acb_ptr points = NULL;
    if (inputPointFile)
        parse = parseInputPointFile( &nbPoints, &points, argv[2], 2*m );
    
    if ( (parse==0) ) {
        clear_global_variables();
        flint_cleanup();
        exit(0);
    }
    
    if (points==NULL) { /* generate input points */
        nbPoints = length;
        points = _acb_vec_init(nbPoints);
        
        slong nbPointsPerSector = 10;
        pw_covering_t cov;
        slong nbPointsGen = computeCoveringAndGetNbPoints( cov, type, nbPointsPerSector );
        acb_ptr pointsGen = _acb_vec_init(nbPointsGen);
        generatePointsInCovering( pointsGen, cov, nbPointsPerSector );
        shuffle_acb_ptr( pointsGen, nbPointsGen );
        for (slong i=0; i<nbPoints; i++)
            acb_set( points + i, pointsGen + i );
        _acb_vec_clear(pointsGen, nbPointsGen);
        pw_covering_clear(cov);
    }
    
    if (verbosity>=level) {
        printf("nbPoints   : %lu\n",nbPoints );
        printf("m          : %ld\n", m );
        printf("log2eps    : %ld\n", log2eps );
        printf("ratio      : "); fmpq_print(ratio); printf("\n");
        printf("roundInput : %ld\n", roundInput);
        printf("derVals    : %d\n", derVals);
        printf("check      : %d\n", check);
        printf("verbosity  : %d\n", verbosity);
    }
    
//     int realCoeffs = 0;
//     if (type==PW_PARSE_EXACT)
//         realCoeffs = fmpq_poly_is_zero(pfmpq_im);
//     else
//         realCoeffs = acb_poly_is_real(pacb);
    
    /* round input */
    if ((type==PW_PARSE_EXACT)&&(roundInput>0)) {
        type = PW_PARSE_BALLS;
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, roundInput );
//         for (slong i=0; i<pacb->length; i++) {
//             acb_get_mid( (pacb->coeffs) + i, (pacb->coeffs) + i );
//         }
    }
    
    /* files for output */
    FILE * covrFile = NULL;
    char covrFileName[] = "covering_with_points.plt\0";
    FILE * outFile = NULL;
    char outFileName[] = "values.txt\0";
    FILE * outDFile = NULL;
    char outDFileName[] = "valuesDer.txt\0";
    if (verbosity!=-1) {
        covrFile = fopen (covrFileName,"w");
        outFile = fopen (outFileName,"w");
        if (derVals)
            outDFile = fopen (outDFileName,"w");
    }
    
    /* allocate memory for the output */
    acb_ptr valuesPWE = _acb_vec_init(nbPoints);
    acb_ptr valuesDer = NULL;
    if (derVals)
        valuesDer = _acb_vec_init(nbPoints);

    ulong wm = m;
    
    /* init profiling */
#ifdef PW_PROFILE
    PW_INIT_PROFILE
#endif

    /* start evaluating */
    clock_t start_evaluate = clock();
    
    if (!derVals) {
        if (type==PW_PARSE_EXACT)
            pwpoly_evaluate_2fmpq_poly( valuesPWE, points, nbPoints, pfmpq_re, pfmpq_im, wm, (ulong)c, scale, ratio, verbosity, covrFile );
        else
            pwpoly_evaluate_acb_poly( valuesPWE, points, nbPoints, pacb, wm, (ulong)c, scale, ratio, verbosity, covrFile );
    } else {
        if (type==PW_PARSE_EXACT)
            pwpoly_evaluate2_2fmpq_poly( valuesPWE, valuesDer, points, nbPoints, pfmpq_re, pfmpq_im, wm, (ulong)c, scale, ratio, verbosity, covrFile );
        else
            pwpoly_evaluate2_acb_poly( valuesPWE, valuesDer, points, nbPoints, pacb, wm, (ulong)c, scale, ratio, verbosity, covrFile );
    }
    
    double clicks_in = (clock() - start_evaluate);
    
    /* print solving info */
    if ((verbosity>=level) || (verbosity==-1)) {
        printf("\n");
        printf("&&&&&&&&&&&& PWEVAL OUTPUT &&&&&&&&&&&&&&&&\n");
        printf(" time for evaluating at %ld points : %.2lf seconds\n", nbPoints, clicks_in/CLOCKS_PER_SEC) ;
        printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
    }

#ifdef PW_PROFILE
    PW_PRINT_PROFILE
#endif

//     for (slong i=0; i< nbPoints; i++) {
//         acb_printd(valuesPWE+i, 10); printf("\n");
//     }
    /* write the values */
    if (outFile != NULL) {
        pwpoly_write_values(outFile, valuesPWE, nbPoints, 10, m);
        fclose (outFile);
    } 
    if (outDFile != NULL) {
        if (derVals) {
            pwpoly_write_values(outDFile, valuesDer, nbPoints, 10, m);
        }
        fclose (outDFile);
    } 
    if (covrFile != NULL)
        fclose (covrFile);

    /* check the output */
    if (check) {
        
        clock_t start_check = clock();
        int checkOK = 0;
        
        if (!derVals) {
            if (type==PW_PARSE_EXACT)
                checkOK = pwpoly_checkOutput_evaluate_2fmpq_poly( valuesPWE, points, nbPoints, pfmpq_re, pfmpq_im, m);
            else
                checkOK = pwpoly_checkOutput_evaluate_acb_poly( valuesPWE, points, nbPoints, pacb, m);
        } else {
            if (type==PW_PARSE_EXACT)
                checkOK = pwpoly_checkOutput_evaluate2_2fmpq_poly( valuesPWE, valuesDer, points, nbPoints, pfmpq_re, pfmpq_im, m);
            else
                checkOK = pwpoly_checkOutput_evaluate2_acb_poly( valuesPWE, valuesDer, points, nbPoints, pacb, m);
        }
        double clicks_in_check = (clock() - start_check);
        printf(" check output                    : %d\n", checkOK);
        printf(" time for checking output        : %lf seconds\n",
                 clicks_in_check/CLOCKS_PER_SEC) ;
    } else {
        if ((verbosity>=level) || (verbosity==-1)) {
            printf(" check output                    : NOT CHECKED\n");
        }
    }
    
    if (derVals)
        _acb_vec_clear(valuesDer, nbPoints);
    _acb_vec_clear(valuesPWE, nbPoints);
    _acb_vec_clear(points, nbPoints);
    clear_global_variables();
    flint_cleanup();
    
    return 0;
}

void init_global_variables(){
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    length = 0;
    c      = 2;
    fmpq_init(scale);
    fmpq_init(ratio);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(ratio, 2, 5);
//     fmpq_set_si(ratio, 1, 10);
    verbosity = 1;
    check = 0;
    derVals=0;
    log2eps = -53;
    roundInput = 0;
    m = 53;
}

void clear_global_variables(){
    fmpq_clear(ratio);
    fmpq_clear(scale);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(pfmpq_re);
    acb_poly_clear(pacb);
}

slong computeCoveringAndGetNbPoints( pw_covering_t cov, int type, slong nbPointsPerSector ) {
    
    ulong working_m = pw_approximation_get_working_m( (ulong)m, length );
    slong prec = pw_approximation_get_working_prec(working_m);
    if (type==PW_PARSE_EXACT)
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, prec);
    
    slong len = pacb->length;
    arb_ptr abs_coeffs = _arb_vec_init( len );
    arb_ptr mantissas  = _arb_vec_init( len );
    fmpq_ptr exponents = _fmpq_vec_init( len );
    for (slong i = 0; i<len; i++) {
        acb_abs(abs_coeffs+i, (pacb->coeffs) + i, prec);
        arb_get_mid_arb( abs_coeffs+i, abs_coeffs+i );
    }
    /* ensure that the center of the abs of the leading coeff is non-zero, */
    /* assuming the leading coeff is non-zero                              */
    slong prectemp = prec;
    while (arb_is_zero(abs_coeffs+(len-1))){
        prectemp=2*prec;
        acb_abs(abs_coeffs+(len-1), (pacb->coeffs) + (len-1), prectemp);
        arb_get_mid_arb( abs_coeffs+(len-1), abs_coeffs+(len-1) );
    }
    /* compute mantissas and exponents */
    _pw_approximation_compute_mantissas_exponents_arb_ptr( mantissas, exponents, abs_coeffs, len );
    
    /* initialize and compute covering */
    ulong working_c = 1;
    pw_covering_init( cov, working_m, len, working_c, scale, ratio );
    _pw_covering_compute_annulii_from_fmpq_weights( cov, exponents );
    /* compute approximations of annulii and roots of unit at precision prec */
    _pw_covering_compute_app_rads( cov, prec );
    
    _fmpq_vec_clear(exponents, len );
    _arb_vec_clear( mantissas, len );
    _arb_vec_clear( abs_coeffs, len );
    
    return nbPointsPerSector*pw_covering_Ksumref(cov);
}

void  generatePointsInCovering( acb_ptr points, pw_covering_t cov, slong nbPointsPerSector ) {
    
    slong prec = 2*pw_covering_app_precref(cov);
    acb_ptr rootsOfUnits = _acb_vec_init(nbPointsPerSector);
    _acb_vec_unit_roots(rootsOfUnits, nbPointsPerSector, nbPointsPerSector, prec);
    
    acb_t temp;
    acb_init(temp);
    
    arb_t s;
    arb_init(s);
    arb_set_d(s, 0.5);
    slong indInPoints = 0;
    
    ulong N  = pw_covering_Nref(cov);
    for (ulong n=0; n<N; n++) {
        ulong K  = pw_covering_nbsectref(cov)[n];
        if (n==N-1) {
            for (slong i=0; i< nbPointsPerSector; i++) {
                acb_div_arb( points+indInPoints, rootsOfUnits+i, s, prec );
                acb_mul_arb( points+indInPoints, points+indInPoints, pw_covering_app_bigradref(cov)+(n-1), prec);
                indInPoints++;
            }
        } else if (K==1) {
            for (int i=0; i< nbPointsPerSector; i++) {
                acb_mul_arb( points+indInPoints, rootsOfUnits+i, pw_covering_app_gammasref(cov)+n, prec );
                indInPoints++;
            }
        } else {
            for (ulong k=0; k<K; k++){
                for (int i=0; i< nbPointsPerSector; i++) {
                    acb_mul_arb( temp, rootsOfUnits+i, s, prec );
                    _pw_covering_shift_back_point( points+indInPoints, cov, n, k, temp, prec );
                    indInPoints++;
                }
            }
        }
    }
    
    arb_clear(s);
    acb_clear(temp);
    _acb_vec_clear(rootsOfUnits, nbPointsPerSector);
}

