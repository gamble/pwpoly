/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_POLYNOMIAL_H
#define PW_POLYNOMIAL_H

#include "pw_base.h"
#ifdef PWPOLY_HAS_BEFFT
#include "common/pw_conversion_be.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif
    
#define PW_POLYNOMIAL_CACHE_DEFAULT_SIZE 10
    
#define PW_POLYNOMIAL_ORACLE      0
#define PW_POLYNOMIAL_EXACT_FMPQ  1
#define PW_POLYNOMIAL_EXACT_ACB   2
#define PW_POLYNOMIAL_APPROX_ACB  3
    
typedef struct {
    slong              _length;       /* length of polynomial */
    slong              _maxPre;       /* maximum prec available: <= LONG_MAX, i.e. prec of _approP */
    slong              _maxNbR;       /* for the solvers in discs: max number of roots in the disc */
    slong              _multZr;       /* the multiplicity of zero as a root of the pol */
    slong              _appPre;       /* the prec to which has been computed _approx */
    slong              _appPreDer;       /* the prec to which has been computed _approxDer */
    int                _ZrRemo;       /* 0 if the trailing zeros have not been removed, 1 otherwise */
    int                _SizePo;       /* nb of approximations in the cache */
    int                _SizeAl;       /* allocated size in the cache */
    int                _SizePoDer;    /* nb of approximations of the derivative */
    int                _SizeAlDer;    /* allocated size in the cache of derivatives */
    int                _from__;       /* 0 when oracle, 1 when exact fmpq, 2 when exact acb, 3 when approx acb */
    int                _realCo;       /* 1 if the pol has real coefficients, 0 otherwise */
    acb_poly_struct  * _table_;       /* the cache: a table of acb_poly_t */
    acb_poly_struct  * _tableDer_;    /* the cache of derivatives: a table of acb_poly_t */
    acb_poly_struct    _approx;       /* approximation at prec _appPre of the pol */
    acb_poly_struct    _approxDer;    /* approximation at prec _appPreDer of the pol */
    acb_poly_srcptr    _approP;       /* when initialized from an acb approximation: the poly at max prec */
    acb_poly_srcptr    _exactC;       /* when initialized from an exact acb pol: the poly */
    fmpq_poly_srcptr   _exactR;       /* when initialized from an exact rat pol: the real part */
    fmpq_poly_srcptr   _exactI;       /*                                         the imag part */
    oracle             _oracle;       /* when initilized from a function: the function */
    /* approximation in doubles */
    double *           _Dcoeff;       /* a table of 2*_length doubles: */
                                      /* even indices are the real parts */
                                      /* odd indices are the imaginary parts */
    double *           _Dabers;       /* a table of _length doubles: */
                                      /* _Dabers[i] is the absolute error of i-th coeff */
    int                _Dfille;       /* =1 if double approximation has been allocated and computed, 0 otherwise */
    int                _Dfits_;        /* =1 if double approximation has been computed and is neither infinite nor nan */
    
} pw_polynomial;

typedef pw_polynomial pw_polynomial_t[1];
typedef pw_polynomial * pw_polynomial_ptr;
    
#define pw_polynomial_lengthref(X) ( (X)->_length )
#define pw_polynomial_maxPreref(X) ( (X)->_maxPre )
#define pw_polynomial_maxNbRref(X) ( (X)->_maxNbR )
#define pw_polynomial_multZrref(X) ( (X)->_multZr )
#define pw_polynomial_appPreref(X) ( (X)->_appPre )
#define pw_polynomial_appPreDerref(X) ( (X)->_appPreDer )
#define pw_polynomial_ZrRemoref(X) ( (X)->_ZrRemo )
#define pw_polynomial_SizePoref(X) ( (X)->_SizePo )
#define pw_polynomial_SizeAlref(X) ( (X)->_SizeAl )
#define pw_polynomial_SizePoDerref(X) ( (X)->_SizePoDer )
#define pw_polynomial_SizeAlDerref(X) ( (X)->_SizeAlDer )
#define pw_polynomial_from__ref(X) ( (X)->_from__ )
#define pw_polynomial_realCoref(X) ( (X)->_realCo )
#define pw_polynomial_table_ref(X) ( (X)->_table_ )
#define pw_polynomial_tableDer_ref(X) ( (X)->_tableDer_ )
#define pw_polynomial_approxref(X) (&(X)->_approx )
#define pw_polynomial_approxDerref(X) (&(X)->_approxDer )
#define pw_polynomial_approPref(X) ( (X)->_approP )
#define pw_polynomial_exactCref(X) ( (X)->_exactC )
#define pw_polynomial_exactRref(X) ( (X)->_exactR )
#define pw_polynomial_exactIref(X) ( (X)->_exactI )
#define pw_polynomial_oracleref(X) ( (X)->_oracle )
#define pw_polynomial_Dcoeffref(X) ( (X)->_Dcoeff )
#define pw_polynomial_Dabersref(X) ( (X)->_Dabers )
#define pw_polynomial_Dfilleref(X) ( (X)->_Dfille )
#define pw_polynomial_Dfits_ref(X) ( (X)->_Dfits_ )

              void         pw_polynomial_init_from_approx( pw_polynomial_t cache, const acb_poly_t approx, slong prec);
              void         pw_polynomial_init_from_oracle( pw_polynomial_t cache, oracle getApprox  );
              void         pw_polynomial_init_from_exact_fmpq ( pw_polynomial_t cache, const fmpq_poly_t exactR, const fmpq_poly_t exactI);
              void         pw_polynomial_init_from_exact_acb  ( pw_polynomial_t cache, const acb_poly_t exactC);

/*                     fills approx and returns a pointer to it */
           acb_poly_srcptr pw_polynomial_getApproximation( pw_polynomial_t cache, slong prec);
/*                     fills _table_ and returns a pointer to it */
           acb_poly_srcptr pw_polynomial_cacheANDgetApproximation( pw_polynomial_t cache, slong prec);
/*                     fills approxDer and returns a pointer to it */
//            acb_poly_srcptr pw_polynomial_getApproxDerivat( pw_polynomial_t cache, slong prec);
/*                     fills _tableDer_ and returns a pointer to it */
           acb_poly_srcptr pw_polynomial_cacheANDgetApproxDerivat( pw_polynomial_t cache, slong prec);
#ifdef PWPOLY_HAS_BEFFT
              /* returns 1 if coeffs are neither infinite nor nan, 0 otherwise */
              int          pw_polynomial_compute_double_approximation( pw_polynomial_t cache );
//               int pw_polynomial_compute_double_approximation_balanced( pw_polynomial_t cache );
#endif
              
              slong        pw_polynomial_get_log2_rootBound ( pw_polynomial_t cache );
              
               /* returns the number r of trailing coeff that are exactly zero, i.e. the multiplicity of zero as a root */
               /* upcomming calls to pw_polynomial_getApproximation and pw_polynomial_cacheANDgetApproximation */
               /* will return polynomials with no trailing zeros */
              slong        pw_polynomial_remove_root_zero( pw_polynomial_t cache );

PWPOLY_INLINE slong        pw_polynomial_degree          ( pw_polynomial_t cache ) { 
    return pw_polynomial_lengthref(cache) - pw_polynomial_multZrref(cache) - 1; 
}
PWPOLY_INLINE slong        pw_polynomial_length          ( pw_polynomial_t cache ) { 
    return pw_polynomial_lengthref(cache) - pw_polynomial_multZrref(cache); 
}
PWPOLY_INLINE int          pw_polynomial_is_approx       ( pw_polynomial_t cache ) { 
    return pw_polynomial_from__ref(cache) == PW_POLYNOMIAL_APPROX_ACB; 
}
PWPOLY_INLINE int          pw_polynomial_is_oracle       ( pw_polynomial_t cache ) { 
    return pw_polynomial_from__ref(cache) == PW_POLYNOMIAL_ORACLE; 
}
PWPOLY_INLINE int          pw_polynomial_is_exact        ( pw_polynomial_t cache ) { 
    return (pw_polynomial_from__ref(cache) == PW_POLYNOMIAL_EXACT_FMPQ)
         ||(pw_polynomial_from__ref(cache) == PW_POLYNOMIAL_EXACT_ACB); 
}
PWPOLY_INLINE int          pw_polynomial_is_exact_fmpq   ( pw_polynomial_t cache ) { 
    return (pw_polynomial_from__ref(cache) == PW_POLYNOMIAL_EXACT_FMPQ); 
}
PWPOLY_INLINE int          pw_polynomial_is_exact_acb   ( pw_polynomial_t cache ) { 
    return (pw_polynomial_from__ref(cache) == PW_POLYNOMIAL_EXACT_ACB); 
}
PWPOLY_INLINE int          pw_polynomial_is_real         ( pw_polynomial_t cache ) { 
    return pw_polynomial_realCoref(cache); 
}
              void         pw_polynomial_clear(            pw_polynomial_t cache );
    
#ifdef __cplusplus
}
#endif

#endif
