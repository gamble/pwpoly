/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_polynomial.h"

void pw_polynomial_init_from_approx( pw_polynomial_t cache, const acb_poly_t approx, slong prec){
    pw_polynomial_lengthref(cache) = approx->length;
    pw_polynomial_maxPreref(cache) = prec;
    pw_polynomial_maxNbRref(cache) = pw_polynomial_lengthref(cache)-1;
    pw_polynomial_multZrref(cache) = 0;
    pw_polynomial_appPreref(cache) = -1;
    pw_polynomial_appPreDerref(cache) = -1;
    pw_polynomial_ZrRemoref(cache) = 0;
    pw_polynomial_SizePoref(cache) = 0;
    pw_polynomial_SizeAlref(cache) = PW_POLYNOMIAL_CACHE_DEFAULT_SIZE;
    pw_polynomial_SizePoDerref(cache) = 0;
    pw_polynomial_SizeAlDerref(cache) = PW_POLYNOMIAL_CACHE_DEFAULT_SIZE;
    pw_polynomial_from__ref(cache) = PW_POLYNOMIAL_APPROX_ACB;
    /* set real coeffs */
    pw_polynomial_realCoref(cache) = acb_poly_is_real(approx);
    
    pw_polynomial_table_ref(cache) = (acb_poly_struct *) pwpoly_malloc ( (pw_polynomial_SizeAlref(cache)) * sizeof(acb_poly_struct) );
    pw_polynomial_tableDer_ref(cache) = (acb_poly_struct *) pwpoly_malloc ( (pw_polynomial_SizeAlDerref(cache)) * sizeof(acb_poly_struct) );
    acb_poly_init(pw_polynomial_approxref(cache));
    acb_poly_init(pw_polynomial_approxDerref(cache));
    
    pw_polynomial_approPref(cache) = (acb_poly_srcptr)(approx+0);
    pw_polynomial_exactCref(cache) = NULL;
    pw_polynomial_exactRref(cache) = NULL;
    pw_polynomial_exactIref(cache) = NULL;
    pw_polynomial_oracleref(cache) = NULL;
    pw_polynomial_Dcoeffref(cache) = NULL;
    pw_polynomial_Dabersref(cache) = NULL;
    pw_polynomial_Dfilleref(cache) = 0;
    pw_polynomial_Dfits_ref(cache) = 0;
}

void pw_polynomial_init_from_oracle( pw_polynomial_t cache, oracle getApprox  ){
    pw_polynomial_lengthref(cache) = -1;
    pw_polynomial_maxPreref(cache) = LONG_MAX;
    pw_polynomial_maxNbRref(cache) = -1;
    pw_polynomial_multZrref(cache) = 0;
    pw_polynomial_appPreref(cache) = -1;
    pw_polynomial_appPreDerref(cache) = -1;
    pw_polynomial_ZrRemoref(cache) = 0;
    pw_polynomial_SizePoref(cache) = 0;
    pw_polynomial_SizeAlref(cache) = PW_POLYNOMIAL_CACHE_DEFAULT_SIZE;
    pw_polynomial_SizePoDerref(cache) = 0;
    pw_polynomial_SizeAlDerref(cache) = PW_POLYNOMIAL_CACHE_DEFAULT_SIZE;
    pw_polynomial_from__ref(cache) = PW_POLYNOMIAL_ORACLE;
    pw_polynomial_realCoref(cache) = 0;
    
    pw_polynomial_table_ref(cache) = (acb_poly_struct *) pwpoly_malloc ( (pw_polynomial_SizeAlref(cache)) * sizeof(acb_poly_struct) );
    pw_polynomial_tableDer_ref(cache) = (acb_poly_struct *) pwpoly_malloc ( (pw_polynomial_SizeAlDerref(cache)) * sizeof(acb_poly_struct) );
    acb_poly_init(pw_polynomial_approxref(cache));
    acb_poly_init(pw_polynomial_approxDerref(cache));
    
    pw_polynomial_approPref(cache) = NULL;
    pw_polynomial_exactCref(cache) = NULL;
    pw_polynomial_exactRref(cache) = NULL;
    pw_polynomial_exactIref(cache) = NULL;
    pw_polynomial_oracleref(cache) = getApprox;
    pw_polynomial_Dcoeffref(cache) = NULL;
    pw_polynomial_Dabersref(cache) = NULL;
    pw_polynomial_Dfilleref(cache) = 0;
    pw_polynomial_Dfits_ref(cache) = 0;
    
    pw_polynomial_getApproximation( cache, PWPOLY_DEFAULT_PREC );
    pw_polynomial_lengthref(cache) = (pw_polynomial_approxref(cache))->length;
    pw_polynomial_maxNbRref(cache) = pw_polynomial_lengthref(cache) -1;
    /* set realCoeffs */
    pw_polynomial_realCoref(cache) = acb_poly_is_real(pw_polynomial_approxref(cache));
    
}

void pw_polynomial_init_from_exact_fmpq ( pw_polynomial_t cache, const fmpq_poly_t exactR, const fmpq_poly_t exactI){
    pw_polynomial_lengthref(cache) = PWPOLY_MAX( fmpq_poly_length(exactR), fmpq_poly_length(exactI) );
    pw_polynomial_maxPreref(cache) = LONG_MAX;
    pw_polynomial_maxNbRref(cache) = pw_polynomial_lengthref(cache) - 1;
    pw_polynomial_multZrref(cache) = 0;
    pw_polynomial_appPreref(cache) = -1;
    pw_polynomial_appPreDerref(cache) = -1;
    pw_polynomial_ZrRemoref(cache) = 0;
    pw_polynomial_SizePoref(cache) = 0;
    pw_polynomial_SizeAlref(cache) = PW_POLYNOMIAL_CACHE_DEFAULT_SIZE;
    pw_polynomial_SizePoDerref(cache) = 0;
    pw_polynomial_SizeAlDerref(cache) = PW_POLYNOMIAL_CACHE_DEFAULT_SIZE;
    pw_polynomial_from__ref(cache) = PW_POLYNOMIAL_EXACT_FMPQ;
    /* set realCoeffs */
    pw_polynomial_realCoref(cache) = fmpq_poly_is_zero(exactI);
    
    pw_polynomial_table_ref(cache) = (acb_poly_struct *) pwpoly_malloc ( (pw_polynomial_SizeAlref(cache)) * sizeof(acb_poly_struct) );
    pw_polynomial_tableDer_ref(cache) = (acb_poly_struct *) pwpoly_malloc ( (pw_polynomial_SizeAlDerref(cache)) * sizeof(acb_poly_struct) );
    acb_poly_init(pw_polynomial_approxref(cache));
    acb_poly_init(pw_polynomial_approxDerref(cache));
    
    pw_polynomial_approPref(cache) = NULL;
    pw_polynomial_exactCref(cache) = NULL;
    pw_polynomial_exactRref(cache) = (fmpq_poly_srcptr)(exactR+0);
    pw_polynomial_exactIref(cache) = (fmpq_poly_srcptr)(exactI+0);
    pw_polynomial_oracleref(cache) = NULL;
    pw_polynomial_Dcoeffref(cache) = NULL;
    pw_polynomial_Dabersref(cache) = NULL;
    pw_polynomial_Dfilleref(cache) = 0;
    pw_polynomial_Dfits_ref(cache) = 0;
}

void pw_polynomial_init_from_exact_acb  ( pw_polynomial_t cache, const acb_poly_t exactC){
    pw_polynomial_lengthref(cache) = exactC->length;
    pw_polynomial_maxPreref(cache) = LONG_MAX;
    pw_polynomial_maxNbRref(cache) = pw_polynomial_lengthref(cache) - 1;
    pw_polynomial_multZrref(cache) = 0;
    pw_polynomial_appPreref(cache) = -1;
    pw_polynomial_appPreDerref(cache) = -1;
    pw_polynomial_ZrRemoref(cache) = 0;
    pw_polynomial_SizePoref(cache) = 0;
    pw_polynomial_SizeAlref(cache) = PW_POLYNOMIAL_CACHE_DEFAULT_SIZE;
    pw_polynomial_SizePoDerref(cache) = 0;
    pw_polynomial_SizeAlDerref(cache) = PW_POLYNOMIAL_CACHE_DEFAULT_SIZE;
    pw_polynomial_from__ref(cache) = PW_POLYNOMIAL_EXACT_ACB;
    /* set real coeffs */
    pw_polynomial_realCoref(cache) = acb_poly_is_real(exactC);
    
    pw_polynomial_table_ref(cache) = (acb_poly_struct *) pwpoly_malloc ( (pw_polynomial_SizeAlref(cache)) * sizeof(acb_poly_struct) );
    pw_polynomial_tableDer_ref(cache) = (acb_poly_struct *) pwpoly_malloc ( (pw_polynomial_SizeAlDerref(cache)) * sizeof(acb_poly_struct) );
    acb_poly_init(pw_polynomial_approxref(cache));
    acb_poly_init(pw_polynomial_approxDerref(cache));
    
    pw_polynomial_approPref(cache) = NULL;
    pw_polynomial_exactCref(cache) = (acb_poly_srcptr)(exactC+0);
    pw_polynomial_exactRref(cache) = NULL;
    pw_polynomial_exactIref(cache) = NULL;
    pw_polynomial_oracleref(cache) = NULL;
    pw_polynomial_Dcoeffref(cache) = NULL;
    pw_polynomial_Dabersref(cache) = NULL;
    pw_polynomial_Dfilleref(cache) = 0;
    pw_polynomial_Dfits_ref(cache) = 0;
}

slong _pw_polynomial_getMultOfZero_2fmpq_poly( const fmpq_poly_t src_re, const fmpq_poly_t src_im ) {
    slong multOfZero = 0;
    slong len        = (slong) PWPOLY_MAX( src_re->length, src_im->length );
    slong len_re     = (slong) src_re->length;
    slong len_im     = (slong) src_im->length;
    while (  (multOfZero < len) &&
             ( (multOfZero>=len_re) || (fmpz_is_zero(src_re->coeffs + multOfZero) ) ) &&
             ( (multOfZero>=len_im) || (fmpz_is_zero(src_im->coeffs + multOfZero) ) )
          ) {
        multOfZero++;
    }
    return multOfZero;
}

slong _pw_polynomial_getMultOfZero_acb_poly( const acb_poly_t src ) {
    slong multOfZero = 0;
    while (  (multOfZero < (src->length)) && (acb_is_zero(src->coeffs+multOfZero)) ) {
        multOfZero++;
    }
    return multOfZero;
}

slong pw_polynomial_remove_root_zero( pw_polynomial_t cache ) {
    /* if the trailing zeros have already been removed, return 0!*/
    if (pw_polynomial_ZrRemoref(cache))
        return 0;
    /* get the multiplicity of zero as a root */
    if ( pw_polynomial_is_approx(cache) )
        pw_polynomial_multZrref(cache) = _pw_polynomial_getMultOfZero_acb_poly( pw_polynomial_approPref(cache) );
    else if ( pw_polynomial_is_oracle(cache) ) {
        (pw_polynomial_oracleref(cache))(pw_polynomial_approxref(cache), PWPOLY_DEFAULT_PREC);
        pw_polynomial_appPreref(cache) = PWPOLY_DEFAULT_PREC;
        pw_polynomial_multZrref(cache) = _pw_polynomial_getMultOfZero_acb_poly( pw_polynomial_approxref(cache) );
        acb_poly_shift_right( pw_polynomial_approxref(cache), pw_polynomial_approxref(cache), pw_polynomial_multZrref(cache) );
    }
    else if ( pw_polynomial_is_exact_acb(cache) )
        pw_polynomial_multZrref(cache) = _pw_polynomial_getMultOfZero_acb_poly( pw_polynomial_exactCref(cache) );
    else 
        pw_polynomial_multZrref(cache) = _pw_polynomial_getMultOfZero_2fmpq_poly( pw_polynomial_exactRref(cache), 
                                                                                  pw_polynomial_exactIref(cache));
    
    pw_polynomial_maxNbRref(cache) = pw_polynomial_maxNbRref(cache) - pw_polynomial_multZrref(cache);
    pw_polynomial_ZrRemoref(cache) = 1;
    
    return pw_polynomial_multZrref(cache);
}

void _pw_polynomial_setApproximation( acb_poly_t dest, pw_polynomial_t cache, slong prec ) {
    
    if ( pw_polynomial_is_approx(cache) ) {
        if (prec<pw_polynomial_maxPreref(cache))
            acb_poly_set_round( dest, pw_polynomial_approPref(cache), prec);
        else
            acb_poly_set( dest, pw_polynomial_approPref(cache) );
    }
    else if ( pw_polynomial_is_oracle(cache) )
        (pw_polynomial_oracleref(cache))(dest, prec);
    else if ( pw_polynomial_is_exact_acb(cache) )
        acb_poly_set_round( dest, pw_polynomial_exactCref(cache), prec);
    else 
        acb_poly_set2_fmpq_poly( dest, 
                                 pw_polynomial_exactRref(cache), pw_polynomial_exactIref(cache), prec);
        
    acb_poly_shift_right( dest, dest, pw_polynomial_multZrref(cache) );
        
}

acb_poly_srcptr pw_polynomial_getApproximation( pw_polynomial_t cache, slong prec ){
    
    if (prec <= 0)
        return NULL;
    
    if ( pw_polynomial_is_approx(cache) )
        prec = PWPOLY_MIN( prec, pw_polynomial_maxPreref(cache) );
    
    if ( pw_polynomial_appPreref(cache) < prec ) {
        
        pw_polynomial_appPreref(cache) = prec;
        _pw_polynomial_setApproximation( pw_polynomial_approxref(cache),cache, prec );
    }
    
    return pw_polynomial_approxref(cache);
}

acb_poly_srcptr pw_polynomial_getApproxDerivat( pw_polynomial_t cache, slong prec ){
    
    if (prec <= 0)
        return NULL;
    
    if ( pw_polynomial_is_approx(cache) )
        prec = PWPOLY_MIN( prec, pw_polynomial_maxPreref(cache) );
    
    if ( pw_polynomial_appPreDerref(cache) < prec ) {
        
        pw_polynomial_appPreDerref(cache) = prec;
        acb_poly_srcptr pol = pw_polynomial_getApproximation( cache, prec );
        acb_poly_derivative( pw_polynomial_approxDerref(cache), pol, prec );
    }
    
    return pw_polynomial_approxDerref(cache);
}

acb_poly_srcptr pw_polynomial_cacheANDgetApproximation( pw_polynomial_t cache, slong prec ){
    
    if (prec <= 0)
        return NULL;
    
    if ( pw_polynomial_is_approx(cache) && (prec >= pw_polynomial_maxPreref(cache)) ) {
            return pw_polynomial_getApproximation( cache, prec );
    }
    
    //get index in cache
    slong log2prec = (slong)(prec/(slong)PWPOLY_DEFAULT_PREC);
    int index = 0;
    while (log2prec>>=1) index++; //index is the log2 of prec/PWPOLY_DEFAULT_PREC
    
    if (index < pw_polynomial_SizePoref(cache))
        return pw_polynomial_table_ref(cache)+index;
    
    /* extends the table if necessary */
    if (index >= pw_polynomial_SizeAlref(cache)) {
//         printf("extend cache\n");
//         int oldsizeAllocated = pw_polynomial_SizeAlref(cache);
        while (index >= pw_polynomial_SizeAlref(cache)) 
            pw_polynomial_SizeAlref(cache) += PW_POLYNOMIAL_CACHE_DEFAULT_SIZE;
        pw_polynomial_table_ref(cache) = 
        (acb_poly_struct *) pwpoly_realloc ( pw_polynomial_table_ref(cache), 
//                                               oldsizeAllocated * sizeof(acb_poly_struct),
                                              pw_polynomial_SizeAlref(cache) * sizeof(acb_poly_struct) );
    }
    
    /* populate the cache */
    while (index >= pw_polynomial_SizePoref(cache)){
        slong nprec = (0x1<<pw_polynomial_SizePoref(cache))*PWPOLY_DEFAULT_PREC;
        acb_poly_init( pw_polynomial_table_ref(cache) + pw_polynomial_SizePoref(cache) );
        _pw_polynomial_setApproximation( pw_polynomial_table_ref(cache) + pw_polynomial_SizePoref(cache), 
                                         cache, nprec );
        pw_polynomial_SizePoref(cache)+=1;
    }
    
    return pw_polynomial_table_ref(cache) + index;
}

acb_poly_srcptr pw_polynomial_cacheANDgetApproxDerivat( pw_polynomial_t cache, slong prec ){
    
    if (prec <= 0)
        return NULL;
    
    if ( pw_polynomial_is_approx(cache) && (prec >= pw_polynomial_maxPreref(cache)) ) {
            return pw_polynomial_getApproxDerivat( cache, prec );
    }
    
    //get index in cache
    slong log2prec = (slong)(prec/(slong)PWPOLY_DEFAULT_PREC);
    int index = 0;
    while (log2prec>>=1) index++; //index is the log2 of prec/PWPOLY_DEFAULT_PREC
    
    if (index < pw_polynomial_SizePoDerref(cache))
        return pw_polynomial_tableDer_ref(cache)+index;
    
    // populate cache of non-derivated polynomial if necessary
    pw_polynomial_cacheANDgetApproximation( cache, prec );
    
    /* extends the table if necessary */
    if (index >= pw_polynomial_SizeAlDerref(cache)) {
//         printf("extend cache\n");
//         int oldsizeAllocated = pw_polynomial_SizeAlref(cache);
        while (index >= pw_polynomial_SizeAlDerref(cache)) 
            pw_polynomial_SizeAlDerref(cache) += PW_POLYNOMIAL_CACHE_DEFAULT_SIZE;
        pw_polynomial_tableDer_ref(cache) = 
        (acb_poly_struct *) pwpoly_realloc ( pw_polynomial_tableDer_ref(cache), 
//                                               oldsizeAllocated * sizeof(acb_poly_struct),
                                              pw_polynomial_SizeAlDerref(cache) * sizeof(acb_poly_struct) );
    }
    
    /* populate the cache */
    while (index >= pw_polynomial_SizePoDerref(cache)){
        slong nprec = (0x1<<pw_polynomial_SizePoDerref(cache))*PWPOLY_DEFAULT_PREC;
        acb_poly_init( pw_polynomial_tableDer_ref(cache) + pw_polynomial_SizePoDerref(cache) );
        acb_poly_srcptr pol = pw_polynomial_cacheANDgetApproximation( cache, nprec );
        acb_poly_derivative( pw_polynomial_tableDer_ref(cache) + pw_polynomial_SizePoDerref(cache), pol, nprec );
        pw_polynomial_SizePoDerref(cache)+=1;
    }
    
    return pw_polynomial_tableDer_ref(cache) + index;
}

#ifdef PWPOLY_HAS_BEFFT
int pw_polynomial_compute_double_approximation( pw_polynomial_t cache ){
    
    if (pw_polynomial_Dfilleref(cache) == 1)
        return pw_polynomial_Dfits_ref(cache);
    
    /* save rounding mode and set double rounding mode to NEAREST */
    int rounding_save = fegetround();
    int ressetnearest = fesetround (FE_TONEAREST);
    if (ressetnearest!=0) {
        pw_polynomial_Dfilleref(cache)=0;
        pw_polynomial_Dfits_ref(cache)=PWPOLY_SET_ROUND_NEAREST_FAILED;
//         printf("pw_polynomial_compute_double_approximation: setting rounding to nearest failed!!!\n");
        fesetround (rounding_save);
        return pw_polynomial_Dfits_ref(cache);
    }
    
    /* save exception flags and re-init exceptions */
    fexcept_t except_save;
    fegetexceptflag (&except_save, FE_ALL_EXCEPT);
    feclearexcept (FE_ALL_EXCEPT);
    
    /* allocate tables */
    slong nlen = pw_polynomial_lengthref(cache) - pw_polynomial_multZrref(cache);
    pw_polynomial_Dcoeffref(cache) = (double *) pwpoly_malloc (2*nlen*sizeof(double));
    pw_polynomial_Dabersref(cache) = (double *) pwpoly_malloc (nlen*sizeof(double));
    /* get an approximation of the polynomial in arb */
    slong prec = PWPOLY_DEFAULT_PREC;
    while (prec <= PWPOLY_DOUBLE_PREC)
        prec = 2*prec;
    
    acb_poly_srcptr app = pw_polynomial_cacheANDgetApproximation( cache, prec );
    
    
    /* populate the table */
    pw_polynomial_Dfits_ref(cache) = 1;
    for (slong i=0; (i<nlen)&&(pw_polynomial_Dfits_ref(cache)); i++) {
        pw_polynomial_Dfits_ref(cache) = (pw_polynomial_Dfits_ref(cache) && 
                                        _be_set_acb( pw_polynomial_Dcoeffref(cache) + (2*i), 
                                        pw_polynomial_Dcoeffref(cache) + (2*i+1), 
                                        pw_polynomial_Dabersref(cache) + i, app->coeffs + i ) );
    }
    
    int double_exception = _pwpoly_test_and_print_exception(PW_PREAMB_CALL("pw_polynomial_compute_double_approximation: ") PW_VERBOSE_CALL(0));
    if (double_exception)
        pw_polynomial_Dfits_ref(cache)=PWPOLY_DOUBLE_EXCEPTION;
    
    /* this is to put all the errors on the 0-th coeff... Do we really want to do it ?*/
//     for(slong i=1; i<nlen; i++) {
//         pw_polynomial_Dabersref(cache)[0] += pw_polynomial_Dabersref(cache)[i];
//         pw_polynomial_Dabersref(cache)[i] = 0;
//     }
//     pw_polynomial_Dabersref(cache)[0] = pw_polynomial_Dabersref(cache)[0]/(1-nlen*PWPOLY_U);
        
    pw_polynomial_Dfilleref(cache)=1;
    
    /* restore exception flags and rounding mode */
    fesetexceptflag (&except_save, FE_ALL_EXCEPT);
    fesetround (rounding_save);
    
    return pw_polynomial_Dfits_ref(cache);
}

// int pw_polynomial_compute_double_approximation_balanced( pw_polynomial_t cache ){
//     
//     if (pw_polynomial_Dfilleref(cache) == 1) {
//         pwpoly_free( pw_polynomial_Dcoeffref(cache) );
//         pwpoly_free( pw_polynomial_Dabersref(cache) );
//     }
//     
//     /* save rounding mode and set double rounding mode to NEAREST */
//     int rounding_save = fegetround();
//     int ressetnearest = fesetround (FE_TONEAREST);
//     if (ressetnearest!=0) {
//         pw_polynomial_Dfilleref(cache)=0;
//         pw_polynomial_Dfits_ref(cache)=PWPOLY_SET_ROUND_NEAREST_FAILED;
// //         printf("pw_polynomial_compute_double_approximation: setting rounding to nearest failed!!!\n");
//         fesetround (rounding_save);
//         return pw_polynomial_Dfits_ref(cache);
//     }
//     
//     /* save exception flags and re-init exceptions */
//     fexcept_t except_save;
//     fegetexceptflag (&except_save, FE_ALL_EXCEPT);
//     feclearexcept (FE_ALL_EXCEPT);
//     
//     acb_t coeff;
//     acb_init(coeff);
//     arb_t abs;
//     arb_init(abs);
//     arf_t mantissa;
//     arf_init(mantissa);
//     fmpz_t exponent, mine, maxe;
//     fmpz_init(exponent);
//     fmpz_init(mine);
//     fmpz_init(maxe);
//     
//     slong nlen = pw_polynomial_lengthref(cache) - pw_polynomial_multZrref(cache);
//     /* allocate tables */
//     pw_polynomial_Dcoeffref(cache) = (double *) pwpoly_malloc (2*nlen*sizeof(double));
//     pw_polynomial_Dabersref(cache) = (double *) pwpoly_malloc (nlen*sizeof(double));
//     
//     /* get an approximation of the polynomial in arb */
//     slong prec = PWPOLY_DEFAULT_PREC;
//     while (prec <= PWPOLY_DOUBLE_PREC)
//         prec = 2*prec;
//     
//     acb_poly_srcptr app = pw_polynomial_cacheANDgetApproximation( cache, prec );
//     
//     /* get min and max exponents */
//     acb_abs(abs, app->coeffs + 0, prec);
//     arf_frexp( mantissa, exponent, arb_midref(abs));
//     fmpz_set(mine, exponent);
//     fmpz_set(maxe, exponent);
//     for ( slong ind=1; ind < nlen; ind++ ) {
//         acb_abs(abs, app->coeffs + ind, prec);
//         arf_frexp( mantissa, exponent, arb_midref(abs));
//         if (fmpz_cmp(exponent, mine)<0)
//             fmpz_set(mine, exponent);
//         if (fmpz_cmp(exponent, maxe)>0)
//             fmpz_set(maxe, exponent);
//     }
// //     printf("mine: "); fmpz_print(mine); printf(" maxe: "); fmpz_print(maxe); printf("\n");
//     fmpz_sub(exponent, maxe, mine);
//     if (!fmpz_is_even(exponent))
//         fmpz_add_ui(exponent, exponent, 1);
//     fmpz_divexact_ui(exponent, exponent, 2);
// //     printf("middle exponent: "); fmpz_print(exponent); printf("\n");
//     fmpz_neg(exponent, exponent);
//     
//     /* populate the table with coefficients balanced around 1 */
//     pw_polynomial_Dfits_ref(cache) = 1;
//     for (slong i=0; (i<nlen)&&(pw_polynomial_Dfits_ref(cache)); i++) {
//         acb_mul_2exp_fmpz( coeff, app->coeffs + i, exponent );
//         pw_polynomial_Dfits_ref(cache) = (pw_polynomial_Dfits_ref(cache) && 
//                                         _be_set_acb( pw_polynomial_Dcoeffref(cache) + (2*i), 
//                                         pw_polynomial_Dcoeffref(cache) + (2*i+1), 
//                                         pw_polynomial_Dabersref(cache) + i, coeff ) );
//     }
//     int double_exception = _pwpoly_test_and_print_exception("pw_polynomial_compute_double_approximation_balanced: " PW_VERBOSE_CALL(0));
//     if (double_exception)
//         pw_polynomial_Dfits_ref(cache)=PWPOLY_DOUBLE_EXCEPTION;
//         
//     pw_polynomial_Dfilleref(cache)=1;
//     
//     fmpz_clear(maxe);
//     fmpz_clear(mine);
//     fmpz_clear(exponent);
//     arf_clear(mantissa);
//     arb_clear(abs);
//     acb_clear(coeff);
//     
//     return pw_polynomial_Dfits_ref(cache);
// }
#endif

/* does not terminate if the leading coeff is zero! */
slong pw_polynomial_get_log2_rootBound ( pw_polynomial_t cache ) {
    
    slong prec = PWPOLY_DEFAULT_PREC;
    mag_t rootBound;
    mag_init(rootBound);
    acb_poly_srcptr pacb = pw_polynomial_cacheANDgetApproximation( cache, prec);
    _acb_poly_root_bound_fujiwara(rootBound, pacb->coeffs, pacb->length);
    
    while ( (!mag_is_finite(rootBound))||(pw_polynomial_is_approx(cache)&&prec>=pw_polynomial_maxPreref(cache)  ) ) {
        prec *=2;
        pacb = pw_polynomial_cacheANDgetApproximation( cache, prec);
        _acb_poly_root_bound_fujiwara(rootBound, pacb->coeffs, pacb->length);
    }
    
    slong ceilLog2rootBound = 0;
    
    if (mag_is_finite(rootBound))
        while ( mag_cmp_2exp_si(rootBound, ceilLog2rootBound) >= 0 )
            ceilLog2rootBound++;
    else
        ceilLog2rootBound = LONG_MAX;
    
    mag_clear(rootBound);
    
    return ceilLog2rootBound;
}

void pw_polynomial_clear( pw_polynomial_t cache ) {
    /* delete the table */
    for (int i=0; i<pw_polynomial_SizePoref(cache); i++)
        acb_poly_clear( pw_polynomial_table_ref(cache)+i );
    pwpoly_free(pw_polynomial_table_ref(cache));
    /* delete the table of derivatives */
    for (int i=0; i<pw_polynomial_SizePoDerref(cache); i++)
        acb_poly_clear( pw_polynomial_tableDer_ref(cache)+i );
    pwpoly_free(pw_polynomial_tableDer_ref(cache));
    /* clear approx and approxDer*/
    acb_poly_clear( pw_polynomial_approxref(cache) );
    acb_poly_clear( pw_polynomial_approxDerref(cache) );
    /* free the double approximation if it has been allocated */
    if (pw_polynomial_Dfilleref(cache) == 1) {
        pwpoly_free( pw_polynomial_Dcoeffref(cache) );
        pwpoly_free( pw_polynomial_Dabersref(cache) );
    }
    pw_polynomial_Dfilleref(cache) = 0;
}
