// Windows specific declarations

#ifdef X86_64_WINDOWS
#  ifdef pwpoly_EXPORTS
#    define EXT_DECL __declspec(dllexport)
#  else
#    define EXT_DECL __declspec(dllimport)
#  endif
#else
#  define EXT_DECL __attribute__ ((visibility ("default")))
#endif

