/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_common.h"

slong pwpoly_get_roots_intersecting_domain( acb_ptr roots, slong * mults, slong nbClusts, const domain_t dom ){
    
    if (domain_is_C(dom) || (nbClusts<=0) )
        return nbClusts;
    
    slong ind = 0, end = nbClusts, temp;
    while (ind < end) {
        if (domain_acb_intersect_domain(dom, roots+ind))
            ind++;
        else {
            /* swap root and corresponding multiplicity */
            acb_swap( roots+ind, roots+(end-1) );
            temp = mults[ind];
            mults[ind] = mults[end-1];
            mults[end-1] = temp;
            end--;
        }
    }
    
    return ind;
    
}

slong pwpoly_get_points_intersecting_domain( acb_ptr points, slong nbPoints, const domain_t dom ){
    
    if (domain_is_C(dom) || (nbPoints<=0) )
        return nbPoints;
    
    slong ind = 0, end = nbPoints;
    while (ind < end) {
        if (domain_acb_intersect_domain(dom, points+ind))
            ind++;
        else {
            /* swap root and corresponding multiplicity */
            acb_swap( points+ind, points+(end-1) );
            end--;
        }
    }
    
    return ind;
    
}
