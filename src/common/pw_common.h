/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_COMMON_H
#define PW_COMMON_H

#include "pw_base.h"
#include "geometry/domain.h"

#ifdef __cplusplus
extern "C" {
#endif
    
/* parsing input polynomial file */
#ifndef PW_NO_INTERFACE
#define PW_PARSE_FAILD 0
#define PW_PARSE_EXACT 1
#define PW_PARSE_BALLS 2

int parseInputFile( slong *length, acb_poly_t pacb, fmpq_poly_t pfmpq_re, fmpq_poly_t pfmpq_im, char * fileName );

int parseInputPointFile( slong *length, acb_ptr * points, char * fileName, slong prec );

/* parsing input arguments of pwroots */
void pwroots_fprint_goal( FILE *file, int goal );
PWPOLY_INLINE void pwroots_print_goal( int goal ){
    pwroots_fprint_goal( stdout, goal );
}
void pwroots_fprint_options( FILE *file, int argc, char* argv[] );
void pweval_fprint_options( FILE *file, int argc, char* argv[] );
int parseInputArgs( int argc, char* argv[], int * verbosity, domain_t dom, slong *nbRoots, slong * m, int * increase, int * check, slong * log2eps, int * goal, int * solver, slong * roundInput, fmpq_t ratio);
int parseInputArgsEval( int argc, char* argv[], int * verbosity, int * check, int * derVals, slong * log2eps, slong *m, slong * roundInput, fmpq_t ratio);
#endif

ulong pwpoly_remove_multOfZero_2fmpq_poly( fmpq_poly_t res_re, fmpq_poly_t res_im,
                                           const fmpq_poly_t src_re, const fmpq_poly_t src_im );
ulong pwpoly_remove_multOfZero_acb_poly( acb_poly_t res, const acb_poly_t src );


PWPOLY_INLINE slong pwpoly_relative_log2width( const acb_t box ) {
    return acb_rel_error_bits(box);
}
/* not rigorous */
PWPOLY_INLINE slong pwpoly_absolute_log2width( const acb_t box ) {
    return (slong)ceil( PWPOLY_MAX( mag_get_d_log2_approx(arb_radref(acb_realref(box))), 
                                    mag_get_d_log2_approx(arb_radref(acb_imagref(box))) ) );
}
/* not rigorous when relative =0 */
PWPOLY_INLINE slong pwpoly_log2width( const acb_t box, int relative ) {
    if (relative==1)
        return pwpoly_relative_log2width(box);
    else
        return pwpoly_absolute_log2width(box);
}

PWPOLY_INLINE int pwpoly_relative_width_less_eps( const acb_t box, slong log2eps ) {
    return acb_rel_error_bits(box) <= log2eps;
}

PWPOLY_INLINE int pwpoly_absolute_width_less_eps( const acb_t box, slong log2eps ) {
    return  (mag_cmp_2exp_si( arb_radref( acb_realref(box) ), log2eps-1 ) <=0)
         && (mag_cmp_2exp_si( arb_radref( acb_imagref(box) ), log2eps-1 ) <=0);
}

PWPOLY_INLINE int pwpoly_width_less_eps( const acb_t box, slong log2eps, int relative ) {
    if (relative==1)
        return pwpoly_relative_width_less_eps(box, log2eps);
    else
        return pwpoly_absolute_width_less_eps(box, log2eps);
}
void pwpoly_log2width_minmax_domain( slong *min, slong *max, acb_srcptr boxes, slong len, int goal, const domain_t domain );

PWPOLY_INLINE int pwpoly_cmp_relative_width( const acb_t a, const acb_t b ) {
    slong wa = pwpoly_relative_log2width(a);
    slong wb = pwpoly_relative_log2width(b);
    if (wa<wb)
        return -1;
    if (wa==wb)
        return 0;
    return 1;
}
/* assume a and b are square boxes */
PWPOLY_INLINE int pwpoly_cmp_absolute_width( const acb_t a, const acb_t b ) {
    return mag_cmp( arb_radref( acb_realref(a) ), arb_radref(acb_realref(b) ) );
}
PWPOLY_INLINE void _pwpoly_sort_increasing_relative_width( acb_ptr points, slong len ) {
    qsort(points, len, sizeof(acb_struct), (__compar_fn_t) pwpoly_cmp_relative_width);
}
PWPOLY_INLINE void _pwpoly_sort_increasing_absolute_width( acb_ptr points, slong len ) {
    qsort(points, len, sizeof(acb_struct), (__compar_fn_t) pwpoly_cmp_absolute_width);
}
PWPOLY_INLINE void pwpoly_sort_increasing_width( acb_ptr points, slong len, int relative ) {
    if (relative==1)
        _pwpoly_sort_increasing_relative_width( points, len );
    else
        _pwpoly_sort_increasing_absolute_width( points, len );
}

void pwpoly_get_bounds_to_refine( slong *firstToRefine, slong *lenToRefine, 
                                  acb_ptr roots, slong len, int goal, slong log2eps, int realCoeffs );

/* filter roots in domain */
/* pre-conditions:  roots, mults are tables of at least nbClusts acbs with multiplicities             */
/*                  dom is an initialized domain                                                      */
/* post-conditions: returns the number of n of clusters in R=roots[0,...,nbClusts-1] intersecting dom */
/*                  roots[0,...n-1] contains the clusters of R intersecting dom, with                 */
/*                  corresponding multiplicities in mults[0,...,n-1]                                  */
/*                  all roots in input roots are in output roots, but permuted                        */
slong pwpoly_get_roots_intersecting_domain( acb_ptr roots, slong * mults, slong nbClusts, const domain_t dom );

slong pwpoly_get_points_intersecting_domain( acb_ptr points, slong nbPoints, const domain_t dom );

#ifndef PW_NO_INTERFACE
/* checking output */
int pwpoly_checkOutput_isolate_exact_acb_poly ( const acb_ptr roots, slong len, const acb_poly_t p);
int pwpoly_checkOutput_isolate_2fmpq_poly ( const acb_ptr roots, slong len, const fmpq_poly_t p_re, const fmpq_poly_t p_im);
int pwpoly_checkOutput_solve_exact_acb_poly ( const acb_ptr roots, const slong * mults, slong len, const acb_poly_t p, slong log2eps, int goal, const domain_t dom, slong goalNbRoots );
int pwpoly_checkOutput_solve_2fmpq_poly ( const acb_ptr roots, const slong * mults, slong len, const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong log2eps, int goal, const domain_t dom, slong goalNbRoots);

int pwpoly_checkOutput_evaluate_acb_poly( const acb_ptr values, const acb_ptr points, slong nbPoints, 
                                          const acb_poly_t pacb, slong m);

int pwpoly_checkOutput_evaluate_2fmpq_poly( const acb_ptr values, const acb_ptr points, slong nbPoints, 
                                            const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong m);

int pwpoly_checkOutput_evaluate2_acb_poly( const acb_ptr values, const acb_ptr valuesDer, const acb_ptr points, slong nbPoints, 
                                           const acb_poly_t pacb, slong m);

int pwpoly_checkOutput_evaluate2_2fmpq_poly( const acb_ptr values, const acb_ptr valuesDer, const acb_ptr points, slong nbPoints, 
                                            const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong m);
#endif

#ifndef PW_NO_INTERFACE
/* writing output */
void pwpoly_write_roots_mults( FILE * outFile, acb_ptr roots, slong * mults, slong len, slong nbDigitsDefault );
void pwpoly_write_values( FILE * outFile, acb_ptr values, slong len, slong nbDigitsDefault, slong m );
#endif

/* NOT USED */
slong pwpoly_sort_increasing_width_and_get_lenToRefine ( acb_ptr roots, slong len, int goal, slong log2eps, int realCoeffs );
/* let balls be a set of len complex balls                    */
/* let res be the return value                                */
/*     the res first elements of balls are pairwise disjoints */
slong _pw_merge_overlaps(acb_ptr balls, slong len, slong prec);

#ifdef __cplusplus
}
#endif

#endif
