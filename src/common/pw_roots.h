/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_ROOTS_H
#define PW_ROOTS_H

#include "pw_base.h"
#include "common/pw_common.h"
#include "geometry/domain.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PW_INDETERMI 0
#define PW_INCLUDING 1
#define PW_ISOLATING 2
#define PW_NATURAL_I 3
    
typedef struct {
    acb_struct _enclosure; /* a SQUARE BOX B(c, w) representing the disk D(c,w/2)          */
    int        _isolation; /* if = PW_NATURAL_I, then D(c,3w/2) and D(c,w/2) contain           */
                           /*                    exactly one root                             */ 
                           /* if = PW_ISOLATING, then the DISC D(c, w/2) is proved to contain */
                           /*                    exactly one root                             */
                           /* if = PW_INCLUDING, then D(c,w/2) and B(c,w) contain at least one root       */
    
} root;

typedef root root_t[1];
typedef root * root_ptr;
typedef const root * root_srcptr;

#define root_enclosureref(X) (&(X)->_enclosure)
#define root_isolationref(X) ( (X)->_isolation)

PWPOLY_INLINE void root_init ( root_t x ) { 
    acb_init( root_enclosureref(x) ); 
    acb_indeterminate(root_enclosureref(x)); 
    root_isolationref(x)=PW_INDETERMI; 
}

PWPOLY_INLINE void root_clear( root_t x ){
    acb_clear( root_enclosureref(x) );
}

PWPOLY_INLINE void root_set ( root_t dest, const root_t src ) { 
    acb_set(root_enclosureref(dest), root_enclosureref(src)); 
    root_isolationref(dest)=root_isolationref(src); 
}

PWPOLY_INLINE void root_set_acb_int ( root_t x, const acb_t enclosure, const int isolation ) { 
    acb_set(root_enclosureref(x), enclosure); 
    root_isolationref(x)=isolation; 
}

/* not used */
// void root_set_d_d_d_int ( root_t x, const double re, const double im, const double rad, const int isolation );

PWPOLY_INLINE void root_conj( root_t dest, const root_t src ) { 
    acb_conj(root_enclosureref(dest), root_enclosureref(src)); 
    root_isolationref(dest)=root_isolationref(src); 
}

PWPOLY_INLINE void root_abs ( arb_t dest, const root_t src, slong prec) {
    acb_abs( dest, root_enclosureref(src), prec );
}
PWPOLY_INLINE void root_arg ( arb_t dest, const root_t src, slong prec) {
    acb_arg( dest, root_enclosureref(src), prec );
}
PWPOLY_INLINE void root_sgn ( acb_t dest, const root_t src, slong prec) {
    acb_sgn( dest, root_enclosureref(src), prec );
}
PWPOLY_INLINE slong root_rel_accuracy_bits ( const root_t src ) {
    return acb_rel_accuracy_bits(root_enclosureref(src));
}
PWPOLY_INLINE slong root_rel_one_accuracy_bits ( const root_t src ) {
    return acb_rel_one_accuracy_bits(root_enclosureref(src));
}
PWPOLY_INLINE slong root_log2width ( const root_t src, int relative ) {
    return pwpoly_log2width(root_enclosureref(src), relative);
}
PWPOLY_INLINE int root_relative_width_less_eps( const root_t src, slong log2eps ) {
    return pwpoly_relative_width_less_eps( root_enclosureref(src), log2eps);
}
PWPOLY_INLINE int root_absolute_width_less_eps( const root_t src, slong log2eps ) {
    return pwpoly_absolute_width_less_eps( root_enclosureref(src), log2eps);
}
PWPOLY_INLINE int root_width_less_eps( const root_t src, slong log2eps, int relative ) {
    return pwpoly_width_less_eps( root_enclosureref(src), log2eps, relative);
}

PWPOLY_INLINE int _root_cmp_relative_width( const root_t a, const root_t b ) {
    return pwpoly_cmp_relative_width( root_enclosureref(a), root_enclosureref(b) );
}
/* assume a and b are square boxes */
PWPOLY_INLINE int _root_cmp_absolute_width( const root_t a, const root_t b ) {
    return pwpoly_cmp_absolute_width( root_enclosureref(a), root_enclosureref(b) );
}

PWPOLY_INLINE int root_overlaps ( const root_t x, const root_t y ) {
    return acb_overlaps( root_enclosureref(x), root_enclosureref(y) );
}
PWPOLY_INLINE void root_swap ( root_t x, root_t y ) {
    acb_swap( root_enclosureref(x), root_enclosureref(y) );
    int t = root_isolationref(x);
    root_isolationref(x) = root_isolationref(y);
    root_isolationref(y) = t;
}

PWPOLY_INLINE root_ptr root_vec_init ( slong len ) {
    root_ptr x = (root_ptr) pwpoly_malloc ( len*sizeof(root) );
    for (slong i = 0; i<len; i++)
        root_init( x + i );
    return x;
}
PWPOLY_INLINE void     root_vec_clear( root_ptr x, slong len ) {
    for (slong i = 0; i<len; i++)
        root_clear( x + i );
    pwpoly_free(x);
}

void root_log2width_minmax( slong *min, slong *max, root_srcptr boxes, slong len, int relative );

PWPOLY_INLINE void _root_sort_increasing_relative_width( root_ptr points, slong len ) {
    qsort(points, len, sizeof(root), (__compar_fn_t) _root_cmp_relative_width);
}
PWPOLY_INLINE void _root_sort_increasing_absolute_width( root_ptr points, slong len ) {
    qsort(points, len, sizeof(root), (__compar_fn_t) _root_cmp_absolute_width);
}
PWPOLY_INLINE void root_sort_increasing_width( root_ptr points, slong len, int relative ) {
    if (relative==1)
        _root_sort_increasing_relative_width( points, len );
    else
        _root_sort_increasing_absolute_width( points, len );
}

slong root_get_roots_intersecting_domain( root_ptr roots, slong len, const domain_t dom );

void root_get_bounds_to_refine( slong *firstToRefine, slong *lenToRefine, 
                                root_ptr roots, slong len, int goal, slong log2eps, int realCoeffs );

slong _pw_roots_merge_overlaps(root_ptr balls, slong len, slong prec);

#ifndef PW_SILENT
void root_fprintd( FILE * f, const root_t r, slong digits );
PWPOLY_INLINE void root_printd( const root_t r, slong digits ){
  root_fprintd( stdout, r, digits );
}
#endif
/* NOT USED */
slong root_sort_increasing_width_and_get_lenToRefine ( root_ptr roots, slong len, int goal, slong log2eps );

#ifdef __cplusplus
}
#endif

#endif    
