/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_base.h"
#include "pw_common.h"
#include <ctype.h>
#include <string.h>

#ifndef PW_NO_INTERFACE

void pwroots_fprint_goal( FILE *file, int goal ) {
    if (goal==PW_GOAL_ISOLATE)
        fprintf(file, "%s (%s)", PW_GOAL_ISOLATE_STR, PW_GOAL_ISOLATE_STR_S);
    else if (goal==PW_GOAL_APPROXIMATE)
        fprintf(file, "%s (%s)", PW_GOAL_APPROXIMATE_STR, PW_GOAL_APPROXIMATE_STR_S);
    else if (goal==PW_GOAL_ISO_AND_APP)
        fprintf(file, "%s (%s)", PW_GOAL_ISO_AND_APP_STR, PW_GOAL_ISO_AND_APP_STR_S);
    else if (goal==PW_GOAL_APPROXIMATE_ABS)
        fprintf(file, "%s (%s)", PW_GOAL_APPROXIMATE_ABS_STR, PW_GOAL_APPROXIMATE_ABS_STR_S);
    else if (goal==PW_GOAL_ISO_AND_APP_ABS)
        fprintf(file, "%s (%s)", PW_GOAL_ISO_AND_APP_ABS_STR, PW_GOAL_ISO_AND_APP_ABS_STR_S);
    else if (goal==PW_GOAL_REFINE)
        fprintf(file, "%s (%s)", PW_GOAL_REFINE_STR, PW_GOAL_REFINE_STR_S);
    else if (goal==PW_GOAL_REFINE_ABS)
        fprintf(file, "%s (%s)", PW_GOAL_REFINE_ABS_STR, PW_GOAL_REFINE_ABS_STR_S);
}

void pwroots_fprint_options( FILE *file, int argc, char* argv[] ){
    
    fprintf(file, "usage: %s <filename> [OPTIONS]\n", argv[0]);
    fprintf(file, "      -v, --verbosity: the verbosity level\n");
    fprintf(file, "                     0 : nothing is printed\n");
    fprintf(file, "                     1 : just prints some stats (>1) for debugging infos)\n");
    fprintf(file, "                    -1 : for timing purpose (no output file)\n");
    fprintf(file, "      -g, --goal     : the goal\n");
    fprintf(file, "                     [default] %s (or %s): isolate the roots (input pol is assumed square free)\n",
                                                   PW_GOAL_ISOLATE_STR, PW_GOAL_ISOLATE_STR_S );
    fprintf(file, "                     %s (or %s): isolate the roots (same requirements than above) and approximate them to relative prec epsilon\n",
                                                   PW_GOAL_ISO_AND_APP_STR, PW_GOAL_ISO_AND_APP_STR_S );
    fprintf(file, "                     %s (or %s): compute relative epsilon-clusters of roots with multiplicities\n",
                                                   PW_GOAL_APPROXIMATE_STR, PW_GOAL_APPROXIMATE_STR_S );
    fprintf(file, "                     %s (or %s): isolate the roots (same requirements than above) and approximate them to absolute prec epsilon\n",
                                                   PW_GOAL_ISO_AND_APP_ABS_STR, PW_GOAL_ISO_AND_APP_ABS_STR_S );
    fprintf(file, "                     %s (or %s): compute absolute epsilon-clusters of roots with multiplicities\n",
                                                   PW_GOAL_APPROXIMATE_ABS_STR, PW_GOAL_APPROXIMATE_ABS_STR_S );
    
    fprintf(file, "      -e, --log2eps: the log2 of eps, the desired precision on the roots\n");
    fprintf(file, "                     [default]: 0\n");
    fprintf(file, "                     an integer t for 2^t\n");
    fprintf(file, "      -d, --domain   : the domain where the roots are sought\n");
    fprintf(file, "                     [default] C: the complex plain\n");
    fprintf(file, "                     a,b,c,d where a<=b, c<=d, a,c in {-inf U Q}, b,d in {Q U +inf}\n");
    fprintf(file, "                     example: -inf,+inf,0,0 for the real line; -inf,3/2,1/2,3/5 for [-inf,3/2]+I[1/2,3/5]\n");
    fprintf(file, "                     the returned roots are certified to belong to domain*(1+2^log2eps)\n");
//     fprintf(file, "                     if goal in %s, must be compact and without roots on boundary\n", HEFPOLY_STR__REFINE);
    fprintf(file, "      -n, --findNroots: the number of roots to be found\n");
    fprintf(file, "                     [default]: -1: find all the roots\n");
    fprintf(file, "                               an integer\n");
//     fprintf(file, "                               if goal in %s, must be >0 and <=degree\n", HEFPOLY_STR__REFINE);
    fprintf(file, "      -m, --initial_m:     the initial value of m (m being the precision of the piecewise approximations on disks)\n");
    fprintf(file, "                     [default]: 10\n");
    fprintf(file, "                     a positive integer\n");
    fprintf(file, "      -f, --fixed_m: only isolate with piecewise pol. approx aet fixed m\n");
    fprintf(file, "      -c, --check: ask to check output (for developpment purpose)\n");
//     fprintf(file, "      -i, --iter:  max number of Ehrlich-Aberth iterations\n");
//     fprintf(file, "                     [default]: 100\n");
//     fprintf(file, "                     a positive integer\n");
//     fprintf(file, "      -ue, --useEaroots:\n");
//     fprintf(file, "                     0: never use earoots\n");
//     fprintf(file, "                     1: try initial solving with earoots for deg <= 3500\n");
//     fprintf(file, "                     2: try solving in discs with earoots\n");
//     fprintf(file, "                     [default]: 3: both \n");
//     fprintf(file, "      -ub, --useBefft:\n");
//     fprintf(file, "                     [default]:0: never use befft\n");
//     fprintf(file, "                     1: try befft for initial piecewise approximation\n");
        
}

void pweval_fprint_options( FILE *file, int argc, char* argv[] ){
    
    fprintf(file, "usage: %s <polynom file name> <points file name> [OPTIONS]\n", argv[0]);
    fprintf(file, "      -v, --verbosity: the verbosity level\n");
    fprintf(file, "                     0 : nothing is printed\n");
    fprintf(file, "                     1 : just prints some stats (>1) for debugging infos)\n");
    fprintf(file, "                    -1 : for timing purpose (no output file)\n");
    fprintf(file, "      -m, --intPrec: the internal precision used for evaluation\n");
    fprintf(file, "      -2, --derVals: also compute values of derivatives at input points\n");
//     fprintf(file, "      -e, --log2eps: the log2 of eps, the desired precision on the evals RELATIVE to input pol\n");
    fprintf(file, "      -c, --check: ask to check output (for developpment purpose)\n");
        
}

/* parsing input arguments of binaries */
int parseInputArgs( int argc, char* argv[], int * verbosity,
                                            domain_t dom,
                                            slong * nbRoots,
                                            slong * m,
                                            int * increase,
//                                             int * max_iter,
                                            int * check,
                                            slong * log2eps,
                                            int * goal, 
                                            int * solver,
                                            slong * roundInput,
                                            fmpq_t b) {
    
    /* loop on arguments to figure out options */
    int parse = 1;
//     int level = 1;
    for (int arg = 2; arg< argc; arg++) {
        
        if ( (strcmp( argv[arg], "-v" ) == 0) || (strcmp( argv[arg], "--verbosity" ) == 0) ) {
            if (argc>arg+1) {
                sscanf(argv[arg+1], "%d", verbosity);
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-g" ) == 0) || (strcmp( argv[arg], "--goal" ) == 0) ) {
            if (argc>arg+1) {
                if ( (strcmp( argv[arg+1], PW_GOAL_ISOLATE_STR ) == 0)
                   ||(strcmp( argv[arg+1], PW_GOAL_ISOLATE_STR_S ) == 0) ){
                    *goal = PW_GOAL_ISOLATE;
                }
                else if ( (strcmp( argv[arg+1], PW_GOAL_APPROXIMATE_STR ) == 0)
                   ||(strcmp( argv[arg+1], PW_GOAL_APPROXIMATE_STR_S ) == 0) ){
                    *goal = PW_GOAL_APPROXIMATE;
                }
                else if ( (strcmp( argv[arg+1], PW_GOAL_ISO_AND_APP_STR ) == 0)
                   ||(strcmp( argv[arg+1], PW_GOAL_ISO_AND_APP_STR_S ) == 0) ){
                    *goal = PW_GOAL_ISO_AND_APP;
                } 
                else if ( (strcmp( argv[arg+1], PW_GOAL_APPROXIMATE_ABS_STR ) == 0)
                   ||(strcmp( argv[arg+1], PW_GOAL_APPROXIMATE_ABS_STR_S ) == 0) ){
                    *goal = PW_GOAL_APPROXIMATE_ABS;
                }
                else if ( (strcmp( argv[arg+1], PW_GOAL_ISO_AND_APP_ABS_STR ) == 0)
                   ||(strcmp( argv[arg+1], PW_GOAL_ISO_AND_APP_ABS_STR_S ) == 0) ){
                    *goal = PW_GOAL_ISO_AND_APP_ABS;
                }
                else if ( (strcmp( argv[arg+1], PW_GOAL_REFINE_STR ) == 0)
                   ||(strcmp( argv[arg+1], PW_GOAL_REFINE_STR_S ) == 0) ){
                    *goal = PW_GOAL_REFINE;
                }
                else if ( (strcmp( argv[arg+1], PW_GOAL_REFINE_ABS_STR ) == 0)
                   ||(strcmp( argv[arg+1], PW_GOAL_REFINE_ABS_STR_S ) == 0) ){
                    *goal = PW_GOAL_REFINE_ABS;
                }
                else {
                    printf("parseInputArgs: not valid goal; use isolate instead\n");
                    *goal = PW_GOAL_ISOLATE;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-n" ) == 0) || (strcmp( argv[arg], "--maxNroots" ) == 0) ) {
            if (argc>arg+1) {
                sscanf(argv[arg+1], "%ld", nbRoots);
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-e" ) == 0) || (strcmp( argv[arg], "--log2eps" ) == 0) ) {
            if (argc>arg+1) {
                sscanf(argv[arg+1], "%ld", log2eps);
                arg++;
            }
            if ( (*log2eps)>0 ) {
                printf("parseInputArgs: positive log2eps not supported, use 0 instead\n");
                (*log2eps)=0;
            }
        }
        
        if ( (strcmp( argv[arg], "-d" ) == 0) || (strcmp( argv[arg], "--domain" ) == 0) ) {
            if (argc>arg+1) {
                slong InDom[8];
                domain_set_str( InDom, argv[arg+1] );
                domain_set_si( dom, InDom );
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-m" ) == 0) || (strcmp( argv[arg], "--intPrec" ) == 0) ) {
            if (argc>arg+1) {
                sscanf(argv[arg+1], "%ld", m);
                arg++;
            }
            if ( (*m)<=0 ) {
                printf("parseInputArgs: non-positive m not supported; abort\n");
                (*log2eps)=0;
                parse=0;
            }
        }
        
        if ( (strcmp( argv[arg], "-f" ) == 0) || (strcmp( argv[arg], "--fixed_m" ) == 0) ) {
            *increase=0;
        }
        
        if ( (strcmp( argv[arg], "-s" ) == 0) || (strcmp( argv[arg], "--solver" ) == 0) ) {
            if (argc>arg+1) {
                sscanf(argv[arg+1], "%d", solver);
                if (!((*solver==8)
                    ||(*solver==9)
                    ||(*solver==11)
                    ||(*solver==12)
                    ||(*solver==13)
                    ||(*solver==15)
                    ||(*solver==25)
                    ||(*solver==27)
                    ||(*solver==29)
                    ||(*solver==31)
                    ||(*solver==32)
                    ||(*solver==34)
                    ||(*solver==47)
                    ||(*solver==63)
                     )){
                    printf("parseInputArgs: invalid solver option: %d, use default value (63) instead\n", *solver);
                    *solver=63;
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-c" ) == 0) || (strcmp( argv[arg], "--check" ) == 0) ) {
            *check=1;
        }
        
        if ( (strcmp( argv[arg], "-r" ) == 0) || (strcmp( argv[arg], "--roundInput" ) == 0) ) {
            if (argc>arg+1) {
                sscanf(argv[arg+1], "%ld", roundInput);
                if (*roundInput <0) {
                    *roundInput=0;
                    printf("parseInputArgs: positive roundInput not supported, use 0 instead\n");
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-b" ) == 0) || (strcmp( argv[arg], "--bfactor" ) == 0) ) {
            if (argc>arg+1) {
                slong num, den;
                sscanf(argv[arg+1], "%ld/%ld", &num, &den);
                if (den <=0) {
                    num = 2;
                    den = 5;
                }
                fmpq_set_si(b, num, den);
                arg++;
            }
        }
        
    }
    return parse;
    
}

/* parsing input arguments of binaries */
int parseInputArgsEval( int argc, char* argv[], int * verbosity,
                                                int * check,
                                                int * derVals,
                                                slong * log2eps,
                                                slong * m,
                                                slong * roundInput,
                                                fmpq_t b) {
    
    /* loop on arguments to figure out options */
    int parse = 1;
//     int level = 1;
    for (int arg = 2; arg< argc; arg++) {
        
        if ( (strcmp( argv[arg], "-v" ) == 0) || (strcmp( argv[arg], "--verbosity" ) == 0) ) {
            if (argc>arg+1) {
                sscanf(argv[arg+1], "%d", verbosity);
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-e" ) == 0) || (strcmp( argv[arg], "--log2eps" ) == 0) ) {
            if (argc>arg+1) {
                sscanf(argv[arg+1], "%ld", log2eps);
                arg++;
            }
            if ( (*log2eps)>0 ) {
                printf("parseInputArgs: positive log2eps not supported, use 0 instead\n");
                (*log2eps)=0;
            }
        }
        
        if ( (strcmp( argv[arg], "-m" ) == 0) || (strcmp( argv[arg], "--initial_m" ) == 0) ) {
            if (argc>arg+1) {
                sscanf(argv[arg+1], "%ld", m);
                arg++;
            }
            if ( (*m)<=0 ) {
                printf("parseInputArgs: non-positive m not supported; abort\n");
                (*log2eps)=0;
                parse=0;
            }
        }
        
        if ( (strcmp( argv[arg], "-c" ) == 0) || (strcmp( argv[arg], "--check" ) == 0) ) {
            *check=1;
        }
        
        if ( (strcmp( argv[arg], "-2" ) == 0) || (strcmp( argv[arg], "--derVals" ) == 0) ) {
            *derVals=1;
        }
        
        if ( (strcmp( argv[arg], "-r" ) == 0) || (strcmp( argv[arg], "--roundInput" ) == 0) ) {
            if (argc>arg+1) {
                sscanf(argv[arg+1], "%ld", roundInput);
                if (*roundInput <0) {
                    *roundInput=0;
                    printf("parseInputArgs: positive roundInput not supported, use 0 instead\n");
                }
                arg++;
            }
        }
        
        if ( (strcmp( argv[arg], "-b" ) == 0) || (strcmp( argv[arg], "--bfactor" ) == 0) ) {
            if (argc>arg+1) {
                slong num, den;
                sscanf(argv[arg+1], "%ld/%ld", &num, &den);
                if (den <=0) {
                    num = 2;
                    den = 5;
                }
                fmpq_set_si(b, num, den);
                arg++;
            }
        }
        
    }
    return parse;
    
}

#endif
