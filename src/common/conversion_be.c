/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_conversion_be.h"

int _pwpoly_test_and_print_exception(PW_PREAMB_ARGU(preamble) PW_VERBOSE_ARGU(verbose)) {
    int double_exception = fetestexcept(FE_UNDERFLOW | FE_OVERFLOW | FE_INVALID);
#ifndef PW_SILENT
    int level = 4;
    if ((verbose>=level)&&double_exception) {
        printf("%s: ", preamble);
        if ( double_exception & FE_UNDERFLOW )
            printf("FE_UNDERFLOW ");
        if ( double_exception & FE_OVERFLOW )
            printf("FE_OVERFLOW ");
        if ( double_exception & FE_INVALID )
            printf("FE_INVALID ");
        printf("\n");
    }
#endif
    return double_exception;
}

int pwpoly_acb_poly_to_be_poly( double * pbe, acb_srcptr coeffs, slong len PW_VERBOSE_ARGU(verbose) ) {
#ifndef PW_SILENT    
    int level = 4;
#endif
    int res = 1;
    /* save exception flags and re-init exceptions */
    fexcept_t except_save;
    fegetexceptflag (&except_save, FE_ALL_EXCEPT);
    feclearexcept (FE_ALL_EXCEPT);
    
    /* save rounding mode and set double rounding mode to NEAREST */
    int rounding_save = fegetround();
    int ressetnearest = fesetround (FE_TONEAREST);
    if (ressetnearest!=0) {
        res = PWPOLY_SET_ROUND_NEAREST_FAILED;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("pwpoly_acb_poly_to_be_poly: setting rounding to nearest failed!!!\n");
#endif
    }
    
    acb_t coeff;
    acb_init(coeff);
    
    int fits_in_doubles=1;
    for( slong i=0; (i<len)&&fits_in_doubles&&(res>0); i++ ) {
        acb_set_round(coeff, coeffs+i, PWPOLY_DOUBLE_PREC);
        fits_in_doubles = fits_in_doubles && _be_set_acb( pbe + (3*i), pbe + (3*i+1), pbe + (3*i+2), coeff );
    }
    
    acb_clear(coeff);
    
    if (!fits_in_doubles) {
        res = PWPOLY_APPROX_DOUBLE_INFINITE;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("pwpoly_acb_poly_to_be_poly: input poly does not fit in doubles!!!\n");
#endif
    }
    
    if (fits_in_doubles) {
        int double_exception = _pwpoly_test_and_print_exception(PW_PREAMB_CALL("pwpoly_acb_poly_to_be_poly: ") PW_VERBOSE_CALL(verbose));
        if (double_exception) {
            res = PWPOLY_DOUBLE_EXCEPTION;
#ifndef PW_SILENT
            if (verbose>=level)
                printf("pwpoly_acb_poly_to_be_poly: double exception!!!\n");
#endif
        }
    }
    
    /* restore exception flags and rounding mode */
    fesetexceptflag (&except_save, FE_ALL_EXCEPT);
    fesetround (rounding_save);
    
    return res;
}

int pwpoly_be_poly_derivative( double * dest, double * src, slong len PW_VERBOSE_ARGU(verbose) ) {
 
#ifndef PW_SILENT
    int level = 4;
#endif
    int res = 1;
    /* save exception flags and re-init exceptions */
    fexcept_t except_save;
    fegetexceptflag (&except_save, FE_ALL_EXCEPT);
    feclearexcept (FE_ALL_EXCEPT);
    
    /* save rounding mode and set double rounding mode to NEAREST */
    int rounding_save = fegetround();
    int ressetnearest = fesetround (FE_TONEAREST);
    if (ressetnearest!=0) {
        res = PWPOLY_SET_ROUND_NEAREST_FAILED;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("pwpoly_be_poly_derivative: setting rounding to nearest failed!!!\n");
#endif
    }
    
    slong i, ind;
    for (i=1; i<len; i++){
        ind = i-1;
        _be_mul_si_aber( dest + (3*ind), dest + (3*ind+1), dest + (3*ind+2), 
                         src[3*i],   src[3*i+1],   src[3*i+2], 
                         i                                                  );
    }
    
    int double_exception = _pwpoly_test_and_print_exception(PW_PREAMB_CALL("pwpoly_be_poly_derivative: ") PW_VERBOSE_CALL(verbose));
    if (double_exception) {
        res = PWPOLY_DOUBLE_EXCEPTION;
#ifndef PW_SILENT
        if (verbose>=level)
            printf("pwpoly_be_poly_derivative: double exception!!!\n");
#endif
    }
    
    /* restore exception flags and rounding mode */
    fesetexceptflag (&except_save, FE_ALL_EXCEPT);
    fesetround (rounding_save);
    
    return res;
}
