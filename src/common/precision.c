/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_base.h"
#include "pw_common.h"

void pwpoly_log2width_minmax_domain( slong *min, slong *max, acb_srcptr boxes, slong len, int goal, const domain_t dom ) {
    slong i, cur;
    *min = LONG_MAX;
    *max = LONG_MIN;
    for(i=0; i<len; i++){
        if (domain_is_C(dom) || domain_acb_intersect_domain(dom, boxes+i)) {
            cur = pwpoly_log2width( boxes+i, PW_GOAL_PREC_RELATIVE(goal) );
            *min = PWPOLY_MIN( *min, cur );
            *max = PWPOLY_MAX( *max, cur );
        }
    }
}

slong _pwpoly_filter_domain( acb_ptr roots, slong len, const domain_t dom ){
    
    if (domain_is_C(dom) || (len<=0) )
        return len;
    
    slong ind = 0, end = len;
    while (ind < end) {
        if (domain_acb_intersect_domain(dom, roots+ind))
            ind++;
        else {
            /* swap */
            acb_swap( roots+ind, roots+(end-1) );
            end--;
        }
    }
    
    return ind;
    
}

void pwpoly_get_bounds_to_refine( slong *firstToRefine, slong *lenToRefine, 
                                  acb_ptr roots, slong len, int goal, slong log2eps, int realCoeffs ) {

    if (realCoeffs) { 
        /* keep only real and imaginary positive roots */
        acb_ptr temp = _acb_vec_init(len);
        slong indInTemp = 0;
        for (slong i=0; i<len; i++) {
            if (arb_is_negative(acb_imagref(roots+i))==0) {
                acb_set(temp + indInTemp, roots + i);
                indInTemp++;
            }
        }
        /* sort it by increasing width */
        pwpoly_sort_increasing_width( temp, indInTemp, PW_GOAL_PREC_RELATIVE(goal) );
        /* reconstruct the roots with pairs of conjugate roots adjacent */
        slong nbRoots = 0;
        for (slong i=0; i<indInTemp; i++) {
            acb_set( roots + nbRoots, temp+i );
            nbRoots++;
            if (arb_is_positive(acb_imagref(temp+i))){
                acb_conj( roots + nbRoots, temp+i );
                nbRoots++;
            }
        }
        _acb_vec_clear(temp, len);
    } else {
        /* sort */
        pwpoly_sort_increasing_width( roots, len, PW_GOAL_PREC_RELATIVE(goal) );
    }
    
    *firstToRefine = 0;
    while ( (*firstToRefine < len ) && 
            (pwpoly_width_less_eps( roots + (*firstToRefine), log2eps, PW_GOAL_PREC_RELATIVE(goal) )) )
        (*firstToRefine)++;

    *lenToRefine = len - (*firstToRefine);

}

/* NOT USED */
/* sort roots by increasing width and get lenToRefine */
slong pwpoly_sort_increasing_width_and_get_lenToRefine ( acb_ptr roots, slong len, int goal, slong log2eps, int realCoeffs ) {
    if (realCoeffs) { /* keep only real and imaginary positive roots and sort */
        acb_ptr temp = _acb_vec_init(len);
        slong indInTemp = 0, indInRoots = 0;
        for (slong i=0; i<len; i++) {
            if (arb_is_negative(acb_imagref(roots+i))==0) {
                acb_set(temp + indInTemp, roots + i);
                indInTemp++;
            }
        }
        pwpoly_sort_increasing_width( temp, indInTemp, PW_GOAL_PREC_RELATIVE(goal) );
        for (slong i=0; i<indInTemp; i++) {
            acb_set(roots + indInRoots, temp+i);
            indInRoots++;
            if (arb_is_positive(acb_imagref(temp+i))){
                acb_conj( roots + indInRoots, temp+i );
                indInRoots++;
            }
        }
        _acb_vec_clear(temp, len);
    } else
        pwpoly_sort_increasing_width( roots, len, PW_GOAL_PREC_RELATIVE(goal) );
    slong ind = 0;
    while ( (ind < len ) && 
            (pwpoly_width_less_eps( roots + ind, log2eps, PW_GOAL_PREC_RELATIVE(goal) )) )
        ind++;
    
//     printf("\n");
//     for (slong i = PWPOLY_MAX(0, ind-2); i<len; i++){
//         printf("i: %ld, root: ", i ); acb_printd(roots+i, 20); printf("\n");
//     }
//     printf("\n");
    
    return len-ind;
}
