/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_solving.h"
#include "pw_roots.h"

int _pw_roots_compare_arg(const root_t u, const root_t v)
{
    arb_t au, av;
    arf_t lu, lv;
    slong bu, bv, prec;
    int result;
    arb_init(au);
    arb_init(av);
    arf_init(lu);
    arf_init(lv);
    bu = root_rel_accuracy_bits(u);
    bv = root_rel_accuracy_bits(v);
    prec = bu>bv?bu:bv;
    prec *= 2;
//     printf("bu: %ld, bv: %ld, prec:%ld\n", bu, bv, prec);
    if (prec<=0) {
        bu = root_rel_one_accuracy_bits(u);
        bv = root_rel_one_accuracy_bits(v);
        prec = bu>bv?bu:bv;
        prec *= 2;
//         printf("bu: %ld, bv: %ld, prec:%ld\n", bu, bv, prec);
        if (prec<=0)
            prec = PWPOLY_DEFAULT_PREC;
    }

    root_arg(au, u, prec);
    root_arg(av, v, prec);
    arb_get_lbound_arf(lu, au, prec);
    arb_get_lbound_arf(lv, av, prec);
    result = arf_cmp(lu, lv);

    arf_clear(lv);
    arf_clear(lu);
    arb_clear(av);
    arb_clear(au);
    return result;
}

int _pw_roots_compare_abs(const root_t u, const root_t v)
{
//     printf("_pw_compare_abs: u: ");
//     acb_printd(u,10);
//     printf(" v: ");
//     acb_printd(v,10);
//     printf("\n");
    arb_t au, av;
    arf_t lu, lv;
    slong bu, bv, prec;
    int result;
    arb_init(au);
    arb_init(av);
    arf_init(lu);
    arf_init(lv);
    bu = root_rel_accuracy_bits(u);
    bv = root_rel_accuracy_bits(v);
    prec = bu>bv?bu:bv;
    prec *= 2;
//     printf("bu: %ld, bv: %ld, prec:%ld\n", bu, bv, prec);
    /* here we assume that the absolute error is less than 1 */
    if (prec<=0) {
        bu = root_rel_one_accuracy_bits(u);
        bv = root_rel_one_accuracy_bits(v);
        prec = bu>bv?bu:bv;
        prec *= 2;
//         printf("bu: %ld, bv: %ld, prec:%ld\n", bu, bv, prec);
        if (prec<=0)
            prec = PWPOLY_DEFAULT_PREC;
    }

    root_abs(au, u, prec);
    root_abs(av, v, prec);
    arb_get_lbound_arf(lu, au, prec);
    arb_get_lbound_arf(lv, av, prec);
    result = arf_cmp(lu, lv);

    arf_clear(lv);
    arf_clear(lu);
    arb_clear(av);
    arb_clear(au);
//     printf("_pw_compare_abs: end:\n ");
    return result;
}

slong _pw_roots_extract_unique(root_ptr balls, slong start, slong abs_size)
{
    slong i, j, size;
    size = 1;
    for(i=0; i<abs_size; i++)
    {
       for(j=i+1; j<abs_size; j++)
       {
           if(!root_overlaps(balls+start+i, balls+start+j))
           {
               //printf("c: %ld, %ld", i, j);
               root_swap(balls+start+i+size, balls+start+j);
               size++;
           } 
           else { 
//                   /* they overlaps: balls+start+i will be kept  */
//                   /*                balls+start+j will be moved to the end of the table */
//                if ( (root_isolationref( balls+start+i ) == PW_INCLUDING) && 
//                     (root_isolationref( balls+start+j ) == PW_ISOLATING) ) {
//                     root_swap(balls+start+i, balls+start+j);
//                }
               /* version with natural isolators */
               if ( root_isolationref( balls+start+i ) < root_isolationref( balls+start+j ) ) {
                   root_swap(balls+start+i, balls+start+j);
               } else if ( (root_isolationref( balls+start+i ) == PW_NATURAL_I) && 
                           (root_isolationref( balls+start+j ) == PW_NATURAL_I) ){
                   /* keep the one with largest radius: it contains the roots of the one with smallest radius */ 
                   /* recall enclosures are square boxes */
                   if ( mag_cmp(arb_radref( acb_realref( root_enclosureref(balls+start+i) ) ), 
                                arb_radref( acb_realref( root_enclosureref(balls+start+j) ) ) ) < 0 ) {
                        root_swap(balls+start+i, balls+start+j);    
                   }
               } 
               else if ( root_isolationref( balls+start+i ) == root_isolationref( balls+start+j ) ) {
                   /* keep the one with smallest radius; recall enclosures are square boxes */
                   if ( mag_cmp(arb_radref( acb_realref( root_enclosureref(balls+start+i) ) ), 
                                arb_radref( acb_realref( root_enclosureref(balls+start+j) ) ) ) > 0 ) {
                        root_swap(balls+start+i, balls+start+j);    
                    }
               }
           }
       }
       abs_size = i+size;
       size = 1;
    }
    return abs_size;
}

slong _pw_roots_merge_overlaps_abs(root_ptr balls, slong start, slong sgn_size, slong prec)
{
//     printf("_pw_roots_merge_overlaps_abs: begin\n");
//     for(slong i=0; i<sgn_size; i++) {acb_printd(balls+start+i, 10); puts("");}
    
    arb_t u, v;
    slong i, ind_min, abs_size;
    arb_init(u);
    arb_init(v);
    qsort(balls+start, sgn_size, sizeof(root), (__compar_fn_t) _pw_roots_compare_abs);
//     printf("_pw_roots_merge_overlaps_abs: after sort\n");
//     for(i=0; i<sgn_size; i++) {acb_printd(balls+start+i, 10); puts("");}
    ind_min=0;
    abs_size = 1;
    root_abs(u, balls+start, prec);
    for(i=0; i<sgn_size-1; i++)
    {
        root_abs(v, balls+start+i+1, prec);
        if(arb_overlaps(u, v))
            abs_size++;
        else if(abs_size==1)
            ind_min++;
        else
        {
            //printf("a: %ld, %ld\n", ind_min, abs_size);
            ind_min += _pw_roots_extract_unique(balls+start, ind_min, abs_size);
            //printf("a: %ld\n", ind_min);
            abs_size = 1;
        }
        if(!arb_contains(u,v))
            arb_set(u, v);
        //    acb_abs(u, balls+start+ind_min+abs_size-1, prec);
        root_swap(balls+start+ind_min+abs_size-1, balls+start+i+1);
    }
    if(abs_size>1)
    {
        //printf("a: %ld, %ld\n", ind_min, abs_size);
        abs_size = _pw_roots_extract_unique(balls+start, ind_min, abs_size);
        //printf("a: %ld\n", ind_min);
    }
    arb_clear(u);
    arb_clear(v);
//     printf("_pw_roots_merge_overlaps_abs: return %ld\n", ind_min+abs_size);
    return ind_min+abs_size;
}

slong _pw_roots_merge_overlaps(root_ptr balls, slong len, slong prec)
{
    acb_t u, v;
    slong i, ind_min, sgn_size, offset;
    if(len==0)
        return 0;
    acb_init(u);
    acb_init(v);
    qsort(balls, len, sizeof(root), (__compar_fn_t) _pw_roots_compare_arg);
//     printf("_pw_root_merge_overlaps: after sort; len: %ld\n", len);
//     for(i=0; i<len; i++) {acb_printd(balls+i, 10); puts("");}
    ind_min=0;
    sgn_size = 1;
    // rotate balls to start in between clusters if necessary
    root_sgn(u, balls+len-1, prec);
    root_sgn(v, balls, prec);
//      slong indu = len-1;
//      slong indv = 0;
//      printf("_pw_roots_merge_overlaps: offset  : %ld\n", offset);
//      printf("_pw_roots_merge_overlaps: sgn u    (%ld): ", indu); acb_printd(u, 10); printf("\n");
//      printf("_pw_roots_merge_overlaps: sgn v    (%ld): ", indv); acb_printd(v, 10); printf("\n");
//      printf("_pw_roots_merge_overlaps: u and v overlap: %d, u contains v: %d\n", acb_overlaps(u,v), acb_contains(u,v));
    for(offset=0; (offset<len-1) && acb_overlaps(u,v); offset++)
    {
//         if(!acb_contains(u,v))
//             indu = offset;
//         indv = ((offset+1) % len);
        
        if(!acb_contains(u,v))
            root_sgn(u, balls+offset, prec);
        root_sgn(v, balls+((offset+1) % len), prec);
        
//         printf("_pw_roots_merge_overlaps: offset  : %ld\n", offset);
//         printf("_pw_roots_merge_overlaps: sgn u    (%ld): ", indu); acb_printd(u, 10); printf("\n");
//         printf("_pw_roots_merge_overlaps: sgn v    (%ld): ", indv); acb_printd(v, 10); printf("\n");
//         printf("_pw_roots_merge_overlaps: u and v overlap: %d, u contains v: %d\n", acb_overlaps(u,v), acb_contains(u,v));
    }
//     printf("_pw_roots_merge_overlaps: len: %ld, offset: %ld\n", len, offset);
    slong lenrec = len;
    for(i=0; i<len && offset>0; i++)
    {
        if(offset+i >= len) {
//             offset = (offset - (len % offset)) % offset;
            slong newoffset = (offset - (lenrec % offset))%offset;
            lenrec=offset;
            offset=newoffset;
        }
        root_swap(balls+i, balls+offset+i);
//         printf("pw_roots_merge_overlaps: swap %ld and %ld\n", i, offset+i);
    }
//     printf("pw_roots_merge_overlaps: after rotate\n");
//     for(i=0; i<len; i++) {acb_printd(balls+i, 10); puts("");}

    root_sgn(u, balls, prec);
    for(i=0; i<len-1; i++)
    {
        root_sgn(v, balls+i+1, prec);
        if(acb_overlaps(u, v))
            sgn_size++;
        else if(sgn_size==1)
            ind_min++;
        else
        {
//             printf("s: %ld, %ld\n", ind_min, sgn_size);
            ind_min += _pw_roots_merge_overlaps_abs(balls, ind_min, sgn_size, prec);
//             printf("s: %ld\n", ind_min);
            sgn_size = 1;
        }
        if(!acb_contains(u,v))
            acb_set(u, v);
        root_swap(balls+ind_min+sgn_size-1, balls+i+1);
    }
    if(sgn_size>1)
    {
        //printf("s: %ld, %ld\n", ind_min, sgn_size);
        sgn_size = _pw_roots_merge_overlaps_abs(balls, ind_min, sgn_size, prec);
        //printf("s: %ld\n", ind_min);
    }
//     for(i=0; i<len; i++) {acb_printd(balls+i, 10); puts("");}
    acb_clear(v);
    acb_clear(u);
    return ind_min + sgn_size;
}

int _pw_compare_arg(const acb_t u, const acb_t v)
{
    arb_t au, av;
    arf_t lu, lv;
    slong bu, bv, prec;
    int result;
    arb_init(au);
    arb_init(av);
    arf_init(lu);
    arf_init(lv);
    bu = acb_rel_accuracy_bits(u);
    bv = acb_rel_accuracy_bits(v);
    prec = bu>bv?bu:bv;
    prec *= 2;
//     printf("bu: %ld, bv: %ld, prec:%ld\n", bu, bv, prec);
    if (prec<=0) {
        bu = acb_rel_one_accuracy_bits(u);
        bv = acb_rel_one_accuracy_bits(v);
        prec = bu>bv?bu:bv;
        prec *= 2;
//         printf("bu: %ld, bv: %ld, prec:%ld\n", bu, bv, prec);
        if (prec<=0)
            prec = PWPOLY_DEFAULT_PREC;
    }

    //acb_get_mid(mu, u);
    //acb_get_mid(mv, v);
    acb_arg(au, u, prec);
    acb_arg(av, v, prec);
    arb_get_lbound_arf(lu, au, prec);
    arb_get_lbound_arf(lv, av, prec);
    result = arf_cmp(lu, lv);

    arf_clear(lu);
    arf_clear(lv);
    arb_clear(au);
    arb_clear(av);
    return result;
}

int _pw_compare_abs(const acb_t u, const acb_t v)
{
//     printf("_pw_compare_abs: u: ");
//     acb_printd(u,10);
//     printf(" v: ");
//     acb_printd(v,10);
//     printf("\n");
    arb_t au, av;
    arf_t lu, lv;
    slong bu, bv, prec;
    int result;
    arb_init(au);
    arb_init(av);
    arf_init(lu);
    arf_init(lv);
    bu = acb_rel_accuracy_bits(u);
    bv = acb_rel_accuracy_bits(v);
    prec = bu>bv?bu:bv;
    prec *= 2;
//     printf("bu: %ld, bv: %ld, prec:%ld\n", bu, bv, prec);
    /* here we assume that the absolute error is less than 1 */
    if (prec<=0) {
        bu = acb_rel_one_accuracy_bits(u);
        bv = acb_rel_one_accuracy_bits(v);
        prec = bu>bv?bu:bv;
        prec *= 2;
//         printf("bu: %ld, bv: %ld, prec:%ld\n", bu, bv, prec);
        if (prec<=0)
            prec = PWPOLY_DEFAULT_PREC;
    }

    acb_abs(au, u, prec);
    acb_abs(av, v, prec);
    arb_get_lbound_arf(lu, au, prec);
    arb_get_lbound_arf(lv, av, prec);
    result = arf_cmp(lu, lv);

    arf_clear(lu);
    arf_clear(lv);
    arb_clear(au);
    arb_clear(av);
//     printf("_pw_compare_abs: end:\n ");
    return result;
}

slong _pw_extract_unique(acb_ptr balls, slong start, slong abs_size)
{
    slong i, j, size;
    size = 1;
    mag_t ri, rj;
    mag_init(ri);
    mag_init(rj);
    for(i=0; i<abs_size; i++)
    {
       for(j=i+1; j<abs_size; j++)
       {
           if(!acb_overlaps(balls+start+i, balls+start+j))
           {
               //printf("c: %ld, %ld", i, j);
               acb_swap(balls+start+i+size, balls+start+j);
               size++;
           } 
//            else {
// //                mag_max( ri, arb_radref( acb_realref( balls+start+i ) ), arb_radref( acb_imagref( balls+start+i ) ) );
// //                mag_max( rj, arb_radref( acb_realref( balls+start+j ) ), arb_radref( acb_imagref( balls+start+j ) ) );
// //                /* keep the ball with smallest radius */
// //                if ( mag_cmp(ri, rj)
//            }
       }
       abs_size = i+size;
       size = 1;
    }
    mag_clear(ri);
    mag_clear(rj);
    return abs_size;
}

slong _pw_merge_overlaps_abs(acb_ptr balls, slong start, slong sgn_size, slong prec)
{
//     printf("_pw_merge_overlaps_abs: begin\n");
//     for(slong i=0; i<sgn_size; i++) {acb_printd(balls+start+i, 10); puts("");}
    
    arb_t u, v;
    slong i, ind_min, abs_size;
    arb_init(u);
    arb_init(v);
    qsort(balls+start, sgn_size, sizeof(acb_struct), (__compar_fn_t) _pw_compare_abs);
//     printf("_pw_merge_overlaps_abs: after sort\n");
//     for(i=0; i<sgn_size; i++) {acb_printd(balls+start+i, 10); puts("");}
    ind_min=0;
    abs_size = 1;
    acb_abs(u, balls+start, prec);
    for(i=0; i<sgn_size-1; i++)
    {
        acb_abs(v, balls+start+i+1, prec);
        if(arb_overlaps(u, v))
            abs_size++;
        else if(abs_size==1)
            ind_min++;
        else
        {
            //printf("a: %ld, %ld\n", ind_min, abs_size);
            ind_min += _pw_extract_unique(balls+start, ind_min, abs_size);
            //printf("a: %ld\n", ind_min);
            abs_size = 1;
        }
        if(!arb_contains(u,v))
            arb_set(u, v);
        //    acb_abs(u, balls+start+ind_min+abs_size-1, prec);
        acb_swap(balls+start+ind_min+abs_size-1, balls+start+i+1);
    }
    if(abs_size>1)
    {
        //printf("a: %ld, %ld\n", ind_min, abs_size);
        abs_size = _pw_extract_unique(balls+start, ind_min, abs_size);
        //printf("a: %ld\n", ind_min);
    }
    arb_clear(u);
    arb_clear(v);
//     printf("_pw_merge_overlaps_abs: return %ld\n", ind_min+abs_size);
    return ind_min+abs_size;
}

slong _pw_merge_overlaps(acb_ptr balls, slong len, slong prec)
{
    acb_t u, v;
    slong i, ind_min, sgn_size, offset;
    if(len==0)
        return 0;
    acb_init(u);
    acb_init(v);
    qsort(balls, len, sizeof(acb_struct), (__compar_fn_t) _pw_compare_arg);
//     printf("_pw_merge_overlaps: after sort; len: %ld\n", len);
//     for(i=0; i<len; i++) {acb_printd(balls+i, 10); puts("");}
    ind_min=0;
    sgn_size = 1;
    // rotate balls to start in between clusters if necessary
    acb_sgn(u, balls+len-1, prec);
    acb_sgn(v, balls, prec);
//      slong indu = len-1;
//      slong indv = 0;
//      printf("_pw_merge_overlaps: offset  : %ld\n", offset);
//      printf("_pw_merge_overlaps: sgn u    (%ld): ", indu); acb_printd(u, 10); printf("\n");
//      printf("_pw_merge_overlaps: sgn v    (%ld): ", indv); acb_printd(v, 10); printf("\n");
//      printf("_pw_merge_overlaps: u and v overlap: %d, u contains v: %d\n", acb_overlaps(u,v), acb_contains(u,v));
    for(offset=0; (offset<len-1) && acb_overlaps(u,v); offset++)
    {
//         if(!acb_contains(u,v))
//             indu = offset;
//         indv = ((offset+1) % len);
        
        if(!acb_contains(u,v))
            acb_sgn(u, balls+offset, prec);
        acb_sgn(v, balls+((offset+1) % len), prec);
        
//         printf("hefroots_merge_overlaps: offset  : %ld\n", offset);
//         printf("hefroots_merge_overlaps: sgn u    (%ld): ", indu); acb_printd(u, 10); printf("\n");
//         printf("hefroots_merge_overlaps: sgn v    (%ld): ", indv); acb_printd(v, 10); printf("\n");
//         printf("hefroots_merge_overlaps: u and v overlap: %d, u contains v: %d\n", acb_overlaps(u,v), acb_contains(u,v));
    }
//     printf("_pw_merge_overlaps: len: %ld, offset: %ld\n", len, offset);
    slong lenrec = len;
    for(i=0; i<len && offset>0; i++)
    {
        if(offset+i >= len) {
//             offset = (offset - (len % offset)) % offset;
            slong newoffset = (offset - (lenrec % offset))%offset;
            lenrec=offset;
            offset=newoffset;
        }
        acb_swap(balls+i, balls+offset+i);
//         printf("_pw_merge_overlaps: swap %ld and %ld\n", i, offset+i);
    }
//     printf("_pw_merge_overlaps: after rotate\n");
//     for(i=0; i<len; i++) {acb_printd(balls+i, 10); puts("");}

    acb_sgn(u, balls, prec);
    for(i=0; i<len-1; i++)
    {
        acb_sgn(v, balls+i+1, prec);
        //if(!acb_contains(u,v))
        //    acb_sgn(u, balls+ind_min+sgn_size-1, prec);
        if(acb_overlaps(u, v))
            sgn_size++;
        else if(sgn_size==1)
            ind_min++;
        else
        {
//             printf("s: %ld, %ld\n", ind_min, sgn_size);
            ind_min += _pw_merge_overlaps_abs(balls, ind_min, sgn_size, prec);
//             printf("s: %ld\n", ind_min);
            sgn_size = 1;
        }
        if(!acb_contains(u,v))
            acb_set(u, v);
        acb_swap(balls+ind_min+sgn_size-1, balls+i+1);
    }
    if(sgn_size>1)
    {
        //printf("s: %ld, %ld\n", ind_min, sgn_size);
        sgn_size = _pw_merge_overlaps_abs(balls, ind_min, sgn_size, prec);
        //printf("s: %ld\n", ind_min);
    }
//     for(i=0; i<len; i++) {acb_printd(balls+i, 10); puts("");}
    acb_clear(v);
    acb_clear(u);
    return ind_min + sgn_size;
}
