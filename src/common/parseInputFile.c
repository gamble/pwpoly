/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_base.h"
#include "pw_common.h"
#include <ctype.h>
#include <string.h>

#ifndef PW_NO_INTERFACE

int acb_load_file( FILE * file, acb_t dest ) {
    int res = ( arb_load_file(acb_realref(dest), file)==0 );
    res = res && ( arb_load_file(acb_imagref(dest), file)==0 );
    return res;
}

int arb_poly_load_file( FILE * file, arb_poly_t dest ) {
    slong length;
    int res = fscanf(file, "%ld ", &length);
    if (res) {
        arb_poly_fit_length(dest, length);
        _arb_poly_set_length(dest, length);
        for (slong i=0; (i<dest->length) && res; i++){
            res = ( arb_load_file(dest->coeffs + i, file) == 0 );
        }
    }
    return res;
}

int acb_poly_load_file( FILE * file, acb_poly_t dest ) {
    slong length;
    int res = fscanf(file, "%ld ", &length);
    if (res) {
        acb_poly_fit_length(dest, length);
        _acb_poly_set_length(dest, length);
        for (slong i=0; (i<dest->length) && res; i++){
            res = ( arb_load_file(acb_realref(dest->coeffs + i), file) == 0 );
            res = ( arb_load_file(acb_imagref(dest->coeffs + i), file) == 0 );
        }
    }
    return res;
}

// #define PW_PARSE_FAILD 0
// #define PW_PARSE_EXACT 1
// #define PW_PARSE_BALLS 2

int parseInputFile( slong *length, acb_poly_t pacb, fmpq_poly_t pfmpq_re, fmpq_poly_t pfmpq_im, char * fileName ) {
    
    FILE * inputFile = fopen (fileName, "r");
    
    if (!inputFile) {
        printf(" !Cannot open input file %s for read, aborting! \n", fileName);
        return PW_PARSE_FAILD;
    }
    
    int res = PW_PARSE_FAILD;
    rewind(inputFile);
    char line [1000];
    char AppA[] = "Ball Approximation Complex\n";
    char ExaR[] = "Exact Real\n";
    char ExaC[] = "Exact Complex\n";
    
    if ( fgets(line, 1000, inputFile)==NULL ) {
        printf(" !Error while parsing the input file %s, aborting! \n", fileName);
        res = PW_PARSE_FAILD;
    }
    
    if (strcmp(line, AppA)==0) {
        int load = acb_poly_load_file( inputFile, pacb );
        if (load==0) {
            printf(" !Error while parsing the ball polynomial in file %s, aborting! \n", fileName);
            res = PW_PARSE_FAILD;
        } else {
            res = PW_PARSE_BALLS;
            *length = pacb->length;
        }
            
    } else if (strcmp(line, ExaR)==0) {
        int load = fmpq_poly_fread(inputFile, pfmpq_re);
        if (load==0) {
            printf(" !Error while parsing the exact real polynomial in file %s, aborting! \n", fileName);
            res = PW_PARSE_FAILD;
        } else {
            fmpq_poly_zero(pfmpq_im);
            res = PW_PARSE_EXACT;
            *length = pfmpq_re->length;
        }
    } else if (strcmp(line, ExaC)==0) {
        int load = fmpq_poly_fread(inputFile, pfmpq_re) && fmpq_poly_fread(inputFile, pfmpq_im);
        if (load==0) {
            printf(" !Error while parsing the exact complex polynomial in file %s, aborting! \n", fileName);
            res = PW_PARSE_FAILD;
        } else {
            res = PW_PARSE_EXACT;
            slong len_re = pfmpq_re->length;
            slong len_im = pfmpq_im->length;
            *length = PWPOLY_MAX(len_re, len_im);
        }
    } else {
        printf(" !Error while parsing the input file %s: unsupproted format, aborting! \n", fileName);
        res = PW_PARSE_FAILD;
    }
        
    fclose (inputFile);
    return res;
    
}

// int parseCompFmpqPoints( fmpq_ptr a, fmpq_ptr b, slong length, FILE * file ) {
//     
// }

int parseInputPointFile( slong *length, acb_ptr * points, char * fileName, slong prec ) {
    
    FILE * inputFile = fopen (fileName, "r");
    
    if (!inputFile) {
        printf(" !Cannot open input file %s for read, aborting! \n", fileName);
        return PW_PARSE_FAILD;
    }
    
    int res = PW_PARSE_FAILD;
    rewind(inputFile);
    char App[]  = "acb";
    char Exa[]  = "fmpq";
    char Dou[]  = "double";
    char type[100];
    int r = fscanf( inputFile, "%s %ld", type, length );
    
    if (r!=2) {
        printf(" !Error while parsing the input file %s, aborting! \n", fileName);
        fclose (inputFile);
        return res;
    }
    
    printf("type: %s, length: %ld\n", type, *length);
    if ((*length)<=0) {
        printf(" !Error while parsing the input file %s: not supported length: %ld \n", fileName, *length);
        fclose (inputFile);
        return res;
    }
    
    if (strcmp(type, Dou)==0) {
        printf("double\n");
        (*points) = _acb_vec_init(*length);
        double re, im;
        for (slong i=0; (i<*length)&&(r==2); i++) {
            r = fscanf( inputFile, "%lf %lf", &re, &im );
            if (r==2)
                acb_set_d_d( (*points) + i, re, im );
        }
        if (r!=2) {
            printf(" !Error while parsing the input file %s, aborting! \n", fileName);
            fclose (inputFile);
            _acb_vec_clear((*points), *length);
            (*points) = NULL;
            (*length) = 0;
            return res;
        } else {
            res = 1;
        }
    } else if (strcmp(type, App)==0) {
        printf("acb\n");
        (*points) = _acb_vec_init(*length);
        r=1;
        for (slong i = 0; (i<(*length)) && r; i++) {
            r = acb_load_file( inputFile, (*points) + i );
        }
        if (!r) {
            printf(" !Error while parsing the input file %s, aborting! \n", fileName);
            fclose (inputFile);
            _acb_vec_clear((*points), *length);
            (*points) = NULL;
            (*length) = 0;
            return res;
        } else {
            res=1;
        }
    } else if (strcmp(type, Exa)==0) {
        printf("fmpq\n");
        (*points) = _acb_vec_init(*length);
        
        mpq_t a;
        fmpq_t temp;
        mpq_init(a);
        fmpq_init(temp);
        
        r=1;
        for (slong i = 0; (i<2*(*length)) && r; i++) {
            r = mpq_inp_str(a, inputFile, 10);
            if (r) {
                fmpq_set_mpq(temp, a);
//                 fmpq_print(temp); printf("\n");
                if (i%2==0)
                    arb_set_fmpq( acb_realref(*points + i/2), temp, prec );
                else
                    arb_set_fmpq( acb_imagref(*points + i/2), temp, prec );
            }
        }
        fmpq_clear(temp);
        mpq_clear(a);
        if (!r) {
            printf(" !Error while parsing the input file %s, aborting! \n", fileName);
            fclose (inputFile);
            _acb_vec_clear((*points), *length);
            (*points) = NULL;
            (*length) = 0;
            return res;
        } else {
            res=1;
        }
        
    }
    
    fclose (inputFile);
    return res;
}

#endif
