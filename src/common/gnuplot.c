/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_gnuplot.h"

#ifndef PW_NO_INTERFACE
void pw_acb_gnuplot(FILE * f, const acb_t ball, int nbdigits){
    arb_t midre, midim, radre, radim;
    arb_init(midre);
    arb_init(midim);
    arb_init(radre);
    arb_init(radim);
    arb_get_mid_arb( midre, acb_realref(ball) );
    arb_get_mid_arb( midim, acb_imagref(ball) );
    arb_get_rad_arb( radre, acb_realref(ball) );
    arb_get_rad_arb( radim, acb_imagref(ball) );
    arb_fprintn(f, midre, nbdigits, ARB_STR_NO_RADIUS);
    fprintf(f, "   ");
    arb_fprintn(f, midim, nbdigits, ARB_STR_NO_RADIUS);
    fprintf(f, "   ");
    arb_fprintn(f, radre, nbdigits, ARB_STR_NO_RADIUS);
    fprintf(f, "   ");
    arb_fprintn(f, radim, nbdigits, ARB_STR_NO_RADIUS);
    arb_clear(midre);
    arb_clear(midim);
    arb_clear(radre);
    arb_clear(radim);
}

void pw_points_gnuplot( FILE * f, const acb_ptr points, slong nbPoints ){
    int nbdigits = 16;
     
    fprintf(f, "set size square\n");
    fprintf(f, "set xrange[-2 : 2]\n");
    fprintf(f, "set yrange[-2 : 2]\n");
    fprintf(f, "plot '-' title 'points' with xyerrorbars lc rgb \"#000000\"\n");
     
    for (slong i=0; i<nbPoints; i++) {
        pw_acb_gnuplot(f, points+i, nbdigits);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n"); 
    
    fprintf(f, "pause mouse close\n");
}

void pw_points_twocolors_gnuplot( FILE * f, const acb_ptr points, slong nbPointsC1, slong nbPoints ){
    int nbdigits = 16;
     
    fprintf(f, "set size square\n");
    fprintf(f, "set xrange[-2 : 2]\n");
    fprintf(f, "set yrange[-2 : 2]\n");
    fprintf(f, "plot '-' title 'points' with xyerrorbars lc rgb \"#008080\", \\\n");
    fprintf(f, "     '-' title 'points' with xyerrorbars lc rgb \"#FF0000\" \n");
     
    for (slong i=0; i<nbPointsC1; i++) {
        pw_acb_gnuplot(f, points+i, nbdigits);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n"); 
    
    for (slong i=nbPointsC1; i<nbPoints; i++) {
        pw_acb_gnuplot(f, points+i, nbdigits);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n"); 
    
    fprintf(f, "pause mouse close\n");
}

void _acb_set_min_max( double * minre, double *maxre, double *minim, double *maxim, const acb_t box ){
    arf_t min, max;
    arf_init(min);
    arf_init(max);
    arb_get_interval_arf(min, max, acb_realref(box), PWPOLY_DOUBLE_PREC);
    *minre = PWPOLY_MIN( *minre, arf_get_d( min, ARF_RND_FLOOR ) );
    *maxre = PWPOLY_MAX( *maxre, arf_get_d( max, ARF_RND_CEIL ) );
    arb_get_interval_arf(min, max, acb_imagref(box), PWPOLY_DOUBLE_PREC);
    *minim = PWPOLY_MIN( *minim, arf_get_d( min, ARF_RND_FLOOR ) );
    *maxim = PWPOLY_MAX( *maxim, arf_get_d( max, ARF_RND_CEIL ) );
    arf_clear(min);
    arf_clear(max);
}

void pw_domain_gnuplot( FILE * f, double *minre, double *maxre, double *minim, double *maxim, const domain_t d ) {
    double minred = *minre, maxred = *maxre, minimd = *minim, maximd = *maxim;
    if (domain_is_lre_finite(d)) {
        *minre = PWPOLY_MIN( *minre, fmpq_get_d(domain_lreref(d)) );
        minred = fmpq_get_d(domain_lreref(d));
    }
    if (domain_is_ure_finite(d)) {
        *maxre = PWPOLY_MAX( *maxre, fmpq_get_d(domain_ureref(d)) );
        maxred = fmpq_get_d(domain_ureref(d));
    }
    if (domain_is_lim_finite(d)) {
        *minim = PWPOLY_MIN( *minim, fmpq_get_d(domain_limref(d)) );
        minimd = fmpq_get_d(domain_limref(d));
    }
    if (domain_is_uim_finite(d)) {
        *maxim = PWPOLY_MAX( *maxim, fmpq_get_d(domain_uimref(d)) );
        maximd = fmpq_get_d(domain_uimref(d));
    }
    double offsetre  = PWPOLY_MAX(((*maxre)-(*minre))*0.1, .1);
    double offsetim  = PWPOLY_MAX(((*maxim)-(*minim))*0.1, .1);
    double offsetred = PWPOLY_MAX((maxred-minred)*0.1, .1);
    double offsetimd = PWPOLY_MAX((maximd-minimd)*0.1, .1);
    minimd = PWPOLY_MAX( *minim-offsetim, minimd-offsetimd );
    maximd = PWPOLY_MIN( *maxim+offsetim, maximd+offsetimd );
    minred = PWPOLY_MAX( *minre-offsetre, minred-offsetred );
    maxred = PWPOLY_MIN( *maxre+offsetre, maxred+offsetred );
    if (domain_is_lre_finite(d)) {
        double temp = fmpq_get_d(domain_lreref(d));
        fprintf(f, "set arrow from %f,%f to %f,%f nohead linecolor \"blue\"\n",
                temp, minimd, temp, maximd);
    }
    if (domain_is_ure_finite(d)) {
        double temp = fmpq_get_d(domain_ureref(d));
        fprintf(f, "set arrow from %f,%f to %f,%f nohead linecolor \"blue\"\n",
                temp, minimd, temp, maximd);
    }
    if (domain_is_lim_finite(d)) {
        double temp = fmpq_get_d(domain_limref(d));
        fprintf(f, "set arrow from %f,%f to %f,%f nohead linecolor \"blue\"\n",
                minred, temp, maxred, temp);
    }
    if (domain_is_uim_finite(d)) {
        double temp = fmpq_get_d(domain_uimref(d));
        fprintf(f, "set arrow from %f,%f to %f,%f nohead linecolor \"blue\"\n",
                minred, temp, maxred, temp);
    }
}

void pw_roots_domain_gnuplot( FILE * f, const acb_ptr points, slong nbPoints, const domain_t d ){
    int nbdigits = 16;
     
    double minre=+INFINITY, maxre=-INFINITY, minim=+INFINITY, maxim=-INFINITY;
    for(int i=0; i < nbPoints; i++)
        _acb_set_min_max( &minre, &maxre, &minim, &maxim, points+i );
    pw_domain_gnuplot( f, &minre, &maxre, &minim, &maxim, d );
    double offsetre = PWPOLY_MAX((maxre-minre)*0.1, .1);
    double offsetim = PWPOLY_MAX((maxim-minim)*0.1, .1);

    fprintf(f, "set xrange[%f:%f]\n", minre-offsetre, maxre+offsetre);
    fprintf(f, "set yrange[%f:%f]\n", minim-offsetim, maxim+offsetim);
    fprintf(f, "set size ratio -1\n");
    
    fprintf(f, "plot '-' title 'points' with xyerrorbars lc rgb \"#000000\"\n");
     
    for (slong i=0; i<nbPoints; i++) {
        pw_acb_gnuplot(f, points+i, nbdigits);
        fprintf(f, "\n");
    }
    fprintf(f, "e\n"); 
    
    fprintf(f, "pause mouse close\n");
}
#endif
