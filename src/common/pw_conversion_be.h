/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_CONVERSION_BE_H
#define PW_CONVERSION_BE_H

#include "pw_base.h"

#ifdef PWPOLY_HAS_BEFFT
#include <fenv.h>
#include "be.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif
    
#ifdef PWPOLY_HAS_BEFFT
    
#ifndef PW_SILENT
#define PW_PREAMB_ARGU(a) const char * a
#define PW_PREAMB_CALL(a) a
#else
#define PW_PREAMB_ARGU(a)
#define PW_PREAMB_CALL(a)
#endif
    
int _pwpoly_test_and_print_exception(PW_PREAMB_ARGU(preamble) PW_VERBOSE_ARGU(verbose));

#define PWPOLY_SET_ROUND_NEAREST_FAILED -7
#define PWPOLY_APPROX_DOUBLE_INFINITE -6
#define PWPOLY_DOUBLE_EXCEPTION -5

int pwpoly_acb_poly_to_be_poly( double * pbe, acb_srcptr coeffs, slong len PW_VERBOSE_ARGU(verbose));

int pwpoly_be_poly_derivative( double * dest, double * src, slong len PW_VERBOSE_ARGU(verbose));

#endif

#ifdef __cplusplus
}
#endif

#endif
