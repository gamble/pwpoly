/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef PW_GNUPLOT_H
#define PW_GNUPLOT_H

#include "pw_base.h"
#include "geometry/domain.h"

#ifdef __cplusplus
extern "C" {
#endif
    
#ifndef PW_NO_INTERFACE
void pw_acb_gnuplot(FILE * f, const acb_t ball, int nbdigits);

void pw_points_gnuplot( FILE * f, const acb_ptr points, slong nbPoints );

void pw_points_twocolors_gnuplot( FILE * f, const acb_ptr points, slong nbPointsC1, slong nbPoints );

void pw_domain_gnuplot( FILE * f, double *minre, double *maxre, double *minim, double *maxim, const domain_t d );

void pw_roots_domain_gnuplot( FILE * f, const acb_ptr points, slong nbPoints, const domain_t d );
#endif

#ifdef __cplusplus
}
#endif

#endif
