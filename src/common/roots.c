/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_roots.h"

void root_log2width_minmax( slong *min, slong *max, root_srcptr boxes, slong len, int relative ) {
    slong i, cur;
    *min = LONG_MAX;
    *max = LONG_MIN;
    for(i=0; i<len; i++){
        cur = root_log2width( boxes+i, relative );
        *min = PWPOLY_MIN( *min, cur );
        *max = PWPOLY_MAX( *max, cur );
    }
}

#ifndef PW_SILENT
void root_fprintd( FILE * f, const root_t r, slong digits ){
    fprintf(f, "[ ");
    acb_fprintd(f, root_enclosureref(r), digits );
    fprintf(f, ", %d ]", root_isolationref(r) );
}
#endif

// void root_set_d_d_d_int ( root_t x, const double re, const double im, const double rad, const int isolation ){
//     acb_set_d_d( root_enclosureref(x), re, im );
//     mag_set_d( arb_radref( acb_realref( root_enclosureref(x) ) ), rad );
//     mag_set_d( arb_radref( acb_imagref( root_enclosureref(x) ) ), rad );
// }

slong root_get_roots_intersecting_domain( root_ptr roots, slong len, const domain_t dom ){
    
    if (domain_is_C(dom) || (len<=0) )
        return len;
    
    slong ind = 0, end = len;
    while (ind < end) {
        if (domain_acb_intersect_domain(dom, root_enclosureref(roots+ind)))
            ind++;
        else {
            /* swap */
            root_swap( roots+ind, roots+(end-1) );
            end--;
        }
    }
    
    return ind;
    
}

void root_get_bounds_to_refine( slong *firstToRefine, slong *lenToRefine, 
                                root_ptr roots, slong len, int goal, slong log2eps, int realCoeffs ) {

    if (realCoeffs) { 
        /* keep only real and imaginary positive roots */
        root_ptr temp = root_vec_init(len);
        slong indInTemp = 0;
        for (slong i=0; i<len; i++) {
            if (arb_is_negative(acb_imagref(root_enclosureref(roots+i)))==0) {
                root_set(temp + indInTemp, roots + i);
                indInTemp++;
            }
        }
        /* sort it by increasing width */
        root_sort_increasing_width( temp, indInTemp, PW_GOAL_PREC_RELATIVE(goal) );
        /* reconstruct the roots with pairs of conjugate roots adjacent */
        slong nbRoots = 0;
        for (slong i=0; i<indInTemp; i++) {
            root_set( roots + nbRoots, temp+i );
            nbRoots++;
            if (arb_is_positive(acb_imagref(root_enclosureref(temp+i)))){
                root_conj( roots + nbRoots, temp+i );
                nbRoots++;
            }
        }
        root_vec_clear(temp, len);
    } else {
        /* sort */
        root_sort_increasing_width( roots, len, PW_GOAL_PREC_RELATIVE(goal) );
    }
    
    *firstToRefine = 0;
    while ( (*firstToRefine < len ) && 
            (root_width_less_eps( roots + (*firstToRefine), log2eps, PW_GOAL_PREC_RELATIVE(goal) )) )
        (*firstToRefine)++;

    *lenToRefine = len - (*firstToRefine);
                                       
}

/* NOT USED */
slong root_sort_increasing_width_and_get_lenToRefine ( root_ptr roots, slong len, int goal, slong log2eps ) {
    root_sort_increasing_width( roots, len, PW_GOAL_PREC_RELATIVE(goal) );
    slong ind = 0;
    while ( (ind < len ) && 
            (root_width_less_eps( roots + ind, log2eps, PW_GOAL_PREC_RELATIVE(goal) )) )
        ind++;
    return len-ind;
}
