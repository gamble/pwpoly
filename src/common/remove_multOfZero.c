/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_base.h"
#include "pw_common.h"

ulong pwpoly_remove_multOfZero_2fmpq_poly( fmpq_poly_t res_re, fmpq_poly_t res_im,
                                           const fmpq_poly_t src_re, const fmpq_poly_t src_im ) {
    
    ulong multOfZero = 0;
    ulong len        = (ulong) PWPOLY_MAX( src_re->length, src_im->length );
    ulong len_re     = (ulong) src_re->length;
    ulong len_im     = (ulong) src_im->length;
    while (  (multOfZero < len) &&
             ( (multOfZero>=len_re) || (fmpz_is_zero(src_re->coeffs + multOfZero) ) ) &&
             ( (multOfZero>=len_im) || (fmpz_is_zero(src_im->coeffs + multOfZero) ) )
          ) {
        multOfZero++;
    }
    fmpq_poly_shift_right( res_re, src_re, (slong)multOfZero );
    fmpq_poly_shift_right( res_im, src_im, (slong)multOfZero );
    
    return multOfZero;
}

ulong pwpoly_remove_multOfZero_acb_poly( acb_poly_t res, const acb_poly_t src ) {
    
    ulong multOfZero = 0;
    ulong len        = (ulong) src->length;
    while (  (multOfZero < len) && (acb_is_zero(src->coeffs+multOfZero)) ) {
        multOfZero++;
    }
    acb_poly_shift_right( res, src, (slong)multOfZero );
    
    return multOfZero;
}
