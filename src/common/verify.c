/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_base.h"
#include "pw_common.h"
#include "solving/pw_solving.h"
#include "solvers/tstar.h"
// #include "solvers/pw_EA.h"

#ifndef PW_NO_INTERFACE
/*         1 if D(mid(x), radius) is an inclusion disc */
/*         0 otherwise */
int _pwpoly_checkOutput_get_inclusion_disc( arb_t radius, const acb_poly_t p, const acb_t x, slong prec, int verbose ) {
    
    int level = 1;
    int res=0;
    
    acb_t midx, f, fp;
    acb_init(midx);
    acb_init(f);
    acb_init(fp);
    arb_t absf, absfp;
    arb_init(absf);
    arb_init(absfp);
    
    /* evaluate p, pprime at the center of x */
    acb_set(midx, x);
    mag_zero( arb_radref(acb_realref(midx)) );
    mag_zero( arb_radref(acb_imagref(midx)) );
    acb_poly_evaluate2( f, fp, p, midx, prec );
    /* set radius to INCLUSION radius d|p(x)|/|p'(x)| */
    /* compute abs of values */
    acb_abs(absf, f, prec);
    acb_abs(absfp, fp, prec);
    arb_div(radius, absf, absfp, prec);
    arb_mul_ui(radius, radius, p->length -1, prec);
    
    if (verbose>=level) {
        printf("--------------_pwpoly_checkOutput_get_inclusion_disc-------------------\n");
        printf("prec   : %ld\n", prec);
        printf("x      : "); acb_printd(x, 10); printf("\n");
        printf("f      : "); acb_printd(f, 10); printf("\n");
        printf("fp     : "); acb_printd(fp, 10); printf("\n");
        printf("radius : "); arb_printd(radius, 10); printf("\n");
    }
    
    if (arb_is_finite(radius))
        res = 1;
    else {
        arb_indeterminate( radius );
        res=0;
    }
    
    if (verbose>=level) {
        printf("-------------------result: %d-----------------------\n\n", res);
    }
     
    arb_clear(absfp);
    arb_clear(absf);
    acb_clear(fp);
    acb_clear(f);
    acb_clear(midx);
    
    return res;
}

int _pwpoly_checkOutput_interval_newton( const acb_t ball, const acb_poly_t p, slong prec ){
    int res = 0;
//     int level = 1;
    
    acb_t cBall, fcBall, fpBall;
    
    acb_init(cBall);
    acb_init(fcBall);
    acb_init(fpBall);
    
    acb_get_mid(cBall, ball);
    acb_poly_evaluate2(fcBall, fpBall, p, ball, prec);
    if (!acb_contains_zero(fpBall)){
        acb_poly_evaluate(fcBall, p, cBall, prec);
        acb_div( fcBall, fcBall, fpBall, prec);
        acb_sub( fcBall, cBall, fcBall, prec);
        if (acb_contains(ball, fcBall)) {
            res=1;
        }
    }
    
    acb_clear(fpBall);
    acb_clear(fcBall);
    acb_clear(cBall);
    
    return res;
}
    
int pwpoly_checkOutput_isolate_exact_acb_poly ( const acb_ptr roots, slong len, const acb_poly_t p) {
    
    int res = 1;
    slong multOfZero = 0;
    while ( (multOfZero < p->length ) && acb_is_zero(p->coeffs + multOfZero) )
        multOfZero++;
    
    int realCoeffs = acb_poly_is_real(p);
    
    /* check number of roots */
    if (len != (p->length-1)){
        printf("CHECK OUTPUT: BAD NUMBER OF ROOTS\n");
        res = 0;
    }
    
    acb_t inflated;
    acb_init(inflated);
    /* check pairwise disjointness and natural isolators*/
    for (slong i=0; (i<len)&&(res==1); i++ ) {
        for (slong j=i+1; (j<len)&&(res==1); j++) {
            if ( acb_overlaps(roots+i, roots+j) ) {
                printf("CHECK OUTPUT: ENCLOSURES %ld AND %ld OVERLAP\n", i, j);
                res = 0;
            }
        }
        acb_set(inflated, roots+i);
        mag_mul_ui( arb_radref( acb_realref( inflated ) ), arb_radref( acb_realref( inflated ) ), 3 );
        mag_mul_ui( arb_radref( acb_imagref( inflated ) ), arb_radref( acb_imagref( inflated ) ), 3 );
        for (slong j=0; (j<len)&&(res==1); j++) {
            if ( (j!=i)&&( acb_overlaps(inflated, roots+j) ) ) {
                printf("CHECK OUTPUT: ENCLOSURES %ld IS NOT A NATURAL ISOLATOR\n", i);
                res = 0;
            }
        }
    }
    acb_clear(inflated);
    
    slong prec    = PWPOLY_DEFAULT_PREC;
    slong maxprec = LONG_MAX; 

    arb_t inclusionRadius;
    arb_init(inclusionRadius);
    acb_t nroot;
    acb_init(nroot);
//     slong nbWorks = 0;
                        
    /* check that each box has a root in the larger disc it contains */
    for (slong i=0; (i<len)&&(res==1); i++) {
        
        /* case where roots+i is exactly 0 */
        if (acb_is_zero( roots + i )) {
            if (multOfZero==1)
                continue;
            else {
                printf("CHECK OUTPUT: ZERO IS REPORTED AS AN EXACT ROOT BUT IT IS NOT\n");
                res = 0;
            }
        }
        
        /* try to compute inclusion disc */
        int resInclusion = _pwpoly_checkOutput_get_inclusion_disc( inclusionRadius, p, roots+i, prec, 0 );
        if (resInclusion>0) {
            acb_set(nroot, roots+i);
            mag_zero( arb_radref(acb_realref(nroot)) );
            mag_zero( arb_radref(acb_imagref(nroot)) );
            acb_add_error_arb( nroot, inclusionRadius );
            resInclusion = acb_contains( roots + i, nroot );
//             printf(" resIsolating: %d\n", resIsolating);
        }
        
        if (resInclusion) {
            continue;
        }
        /* else do a Pellet test */
        slong nbrootsInDisc = _pwpoly_tstar_acb( roots+i, p, p->length-1, &prec, maxprec );
        if (nbrootsInDisc==TSTAR_MAX_PREC_REACHED)
            printf("CHECK OUTPUT: PELLET TEST FAILED FOR %ld-TH ENCLOSURE: MAXPREC (%ld) REACHED\n", i, maxprec);
        else if (nbrootsInDisc==TSTAR_ROOTS_NEAR_BOUND)
            printf("CHECK OUTPUT: PELLET TEST FAILED FOR %ld-TH ENCLOSURE: ROOT(S) NEAR BOUNDARY\n", i);
        else if (nbrootsInDisc!=1) {
            printf("CHECK OUTPUT: PELLET TEST REPORTS %ld ROOTS IN %ld-TH ENCLOSURE: (SHOULD BE %d)\n", nbrootsInDisc, i, 1);
            res = 0;
        }
        
    }
    
    acb_clear(nroot);
    arb_clear(inclusionRadius);
    
    /* check that real roots are reported properly */
    for (slong i=0; (i<len)&&(res==1); i++) {
        if ( realCoeffs && arb_contains_zero( acb_imagref( roots+i) ) && !arb_is_zero(acb_imagref( roots+i)) ) {
            printf("CHECK OUTPUT: %ld-TH ENCLOSURE ISOLATES A REAL ROOT BUT IS NOT REPORTED AS \n", i);
            res = 0;
        }
    }
    
    return res;
    
}

int pwpoly_checkOutput_isolate_2fmpq_poly ( const acb_ptr roots, slong len, const fmpq_poly_t p_re, const fmpq_poly_t p_im) {
    
    int res = 1;
    slong lenp = PWPOLY_MAX( p_re->length, p_im->length );
    slong multOfZero = 0;
    slong len_re     = p_re->length;
    slong len_im     = p_im->length;
    while (  (multOfZero < lenp) &&
             ( (multOfZero>=len_re) || (fmpz_is_zero(p_re->coeffs + multOfZero) ) ) &&
             ( (multOfZero>=len_im) || (fmpz_is_zero(p_im->coeffs + multOfZero) ) )
          ) {
        multOfZero++;
    }
    
    int realCoeffs = fmpq_poly_is_zero(p_im);
    
    /* check number of roots */
    if (len != (lenp-1)){
        printf("CHECK OUTPUT: BAD NUMBER OF ROOTS\n");
        return 0;
    }
    
    acb_t inflated;
    acb_init(inflated);
    /* check pairwise disjointness and natural isolators*/
    for (slong i=0; (i<len)&&(res==1); i++ ) {
        for (slong j=i+1; (j<len)&&(res==1); j++) {
            if ( acb_overlaps(roots+i, roots+j) ) {
                printf("CHECK OUTPUT: ENCLOSURES %ld AND %ld OVERLAP\n", i, j);
                res = 0;
            }
        }
        acb_set(inflated, roots+i);
        mag_mul_ui( arb_radref( acb_realref( inflated ) ), arb_radref( acb_realref( inflated ) ), 3 );
        mag_mul_ui( arb_radref( acb_imagref( inflated ) ), arb_radref( acb_imagref( inflated ) ), 3 );
        for (slong j=0; (j<len)&&(res==1); j++) {
            if ( (j!=i)&&( acb_overlaps(inflated, roots+j) ) ) {
                printf("CHECK OUTPUT: ENCLOSURES %ld IS NOT A NATURAL ISOLATOR\n", i);
                res = 0;
            }
        }
    }
    acb_clear(inflated);
    
    slong prec    = PWPOLY_DEFAULT_PREC;
    slong maxprec = LONG_MAX; 
    
    acb_poly_t p;
    acb_poly_init(p);
    slong prec_p = prec;
    acb_poly_set2_fmpq_poly(p, p_re, p_im, prec_p);

    arb_t inclusionRadius;
    arb_init(inclusionRadius);
    acb_t nroot;
    acb_init(nroot);
//     slong nbWorks = 0;
                        
    /* check that each box has a root in the larger disc it contains */
    for (slong i=0; (i<len)&&(res==1); i++) {
        
        /* case where roots+i is exactly 0 */
        if (acb_is_zero( roots + i )) {
            if (multOfZero==1)
                continue;
            else {
                printf("CHECK OUTPUT: ZERO IS REPORTED AS AN EXACT ROOT BUT IT IS NOT\n");
                res = 0;
            }
        }
        
        /* try to compute inclusion disc */
        int resInclusion = _pwpoly_checkOutput_get_inclusion_disc( inclusionRadius, p, roots+i, prec, 0 );
        if (resInclusion>0) {
            acb_set(nroot, roots+i);
            mag_zero( arb_radref(acb_realref(nroot)) );
            mag_zero( arb_radref(acb_imagref(nroot)) );
            acb_add_error_arb( nroot, inclusionRadius );
            resInclusion = acb_contains( roots + i, nroot );
        }
        if (resInclusion) {
            continue;
        }
        /* else do a Pellet test */
        
        slong nbrootsInDisc = _pwpoly_tstar_acb_2fmpq_poly( roots+i, p_re, p_im, lenp-1, &prec, maxprec );
        if (nbrootsInDisc==TSTAR_MAX_PREC_REACHED)
            printf("CHECK OUTPUT: PELLET TEST FAILED FOR %ld-TH ENCLOSURE: MAXPREC (%ld) REACHED\n", i, maxprec);
        else if (nbrootsInDisc==TSTAR_ROOTS_NEAR_BOUND)
            printf("CHECK OUTPUT: PELLET TEST FAILED FOR %ld-TH ENCLOSURE: ROOT(S) NEAR BOUNDARY\n", i);
        else if (nbrootsInDisc!=1) {
            printf("CHECK OUTPUT: PELLET TEST REPORTS %ld ROOTS IN %ld-TH ENCLOSURE: (SHOULD BE %d)\n", nbrootsInDisc, i, 1);
            res =  0;
        }
        
        if (prec > prec_p) {
            prec_p = prec;
            acb_poly_set2_fmpq_poly(p, p_re, p_im, prec_p);
        }
    }
    
    acb_poly_clear(p);
    acb_clear(nroot);
    arb_clear(inclusionRadius);
    
    /* check that real roots are reported properly */
    for (slong i=0; (i<len)&&(res==1); i++) {
        if ( realCoeffs && arb_contains_zero( acb_imagref( roots+i) ) && !arb_is_zero(acb_imagref( roots+i)) ) {
            printf("CHECK OUTPUT: %ld-TH ENCLOSURE ISOLATES A REAL ROOT BUT IS NOT REPORTED AS \n", i);
            res = 0;
        }
    }
    
    return res;
    
}

int pwpoly_checkOutput_solve_exact_acb_poly ( const acb_ptr roots, const slong * mults, slong len, const acb_poly_t p, slong log2eps, int goal, const domain_t dom, slong goalNbRoots ) {
    
    int res = 1;
    int realCoeffs = acb_poly_is_real(p);
    
    
    /* cap goalNbRoots */
    if (goalNbRoots<0)
        goalNbRoots = (p->length-1);
    else
        goalNbRoots = PWPOLY_MIN( goalNbRoots, p->length-1 );
    
    /* check number of roots */
    slong nbRoots = 0;
    slong i;
    for (i=0; (i<len)&&(res==1); i++) {
        nbRoots += mults[i];
        if ( PW_GOAL_MUST_ISOLATE(goal) && (mults[i]!=1) ) {
            printf("CHECK OUTPUT: ROOTS NOT ISOLATED \n");
            res = 0;
        }
    }
    
    if ( domain_is_C(dom) ) {
        if ((res==1)&&(nbRoots < goalNbRoots)){
            printf("CHECK OUTPUT: WRONG NUMBER OF ROOTS\n");
            res = 0;
        }
    } else {
        /* verify that all the roots intersect the domain */
        for (i=0; (i<len)&&(res==1); i++) {
            if ( !domain_acb_intersect_domain(dom, roots+i) ) {
                printf("CHECK OUTPUT: ENCLOSURE %ld DO NOT OVERLAP DOMAIN\n", i);
                res = 0;
            }
        }
    }
    

    acb_t inflated;
    acb_init(inflated);
    /* check pairwise disjointness and natural isolators*/
    for (i=0; (i<len)&&(res==1); i++ ) {
        for (slong j=i+1; (j<len)&&(res==1); j++) {
            if ( acb_overlaps(roots+i, roots+j) ) {
                printf("CHECK OUTPUT: ENCLOSURES %ld AND %ld OVERLAP\n", i, j);
                res = 0;
            }
        }
        acb_set(inflated, roots+i);
        mag_mul_ui( arb_radref( acb_realref( inflated ) ), arb_radref( acb_realref( inflated ) ), 3 );
        mag_mul_ui( arb_radref( acb_imagref( inflated ) ), arb_radref( acb_imagref( inflated ) ), 3 );
        for (slong j=0; (j<len)&&(res==1); j++) {
            if ( (j!=i)&&( acb_overlaps(inflated, roots+j) ) ) {
                printf("CHECK OUTPUT: ENCLOSURES %ld IS NOT A NATURAL ISOLATOR\n", i);
                res = 0;
            }
        }
    }
    acb_clear(inflated);
    
    /* check precision */
    if ( PW_GOAL_MUST_APPROXIMATE(goal) ) {
        for (i=0; (i<len)&&(res==1); i++) {
            res = pwpoly_width_less_eps( roots+i, log2eps, PW_GOAL_PREC_RELATIVE(goal) );
            if (res==0)
                printf("CHECK OUTPUT: LOG2 OF ERROR OF ENCLOSURE %ld GREATER THAN %ld\n", i, log2eps);
        }
    }
    
    slong prec    = PWPOLY_DEFAULT_PREC;
    slong maxprec = LONG_MAX;
    /* check number of roots */
    arb_t inclusionRadius;
    arb_init(inclusionRadius);
    acb_t nroot;
    acb_init(nroot);
    for (slong i=0; (i<len)&&(res==1); i++) {
        int resInclusion = 0;
        if (mults[i]==1) {
            /* try to compute inclusion disc */
            resInclusion = _pwpoly_checkOutput_get_inclusion_disc( inclusionRadius, p, roots+i, prec, 0 );
            if (resInclusion>0) {
                acb_set(nroot, roots+i);
                mag_zero( arb_radref(acb_realref(nroot)) );
                mag_zero( arb_radref(acb_imagref(nroot)) );
                acb_add_error_arb( nroot, inclusionRadius );
                resInclusion = acb_contains( roots + i, nroot );
//             printf(" resIsolating: %d\n", resIsolating);
            }
        }
        if (resInclusion) {
                continue;
        }
        /* else do a Newton Interval test */
        if (mults[i]==1) {
            resInclusion = _pwpoly_checkOutput_interval_newton( roots+i, p, prec);
        }
        if (resInclusion) {
                continue;
        }
        /* else do a Pellet test */
        slong nbrootsInDisc = _pwpoly_tstar_acb( roots+i, p, p->length-1, &prec, maxprec );
        if (nbrootsInDisc==TSTAR_MAX_PREC_REACHED)
            printf("CHECK OUTPUT: PELLET TEST FAILED FOR %ld-TH ENCLOSURE: MAXPREC (%ld) REACHED\n", i, maxprec);
        else if (nbrootsInDisc==TSTAR_ROOTS_NEAR_BOUND)
            printf("CHECK OUTPUT: PELLET TEST FAILED FOR %ld-TH ENCLOSURE: ROOT(S) NEAR BOUNDARY\n", i);
        else if (nbrootsInDisc!=mults[i]) {
//             printf( "%ld-th enclosure: ", i );
//             acb_printd( roots+i, 10);
//             printf("\n");
            printf("CHECK OUTPUT: PELLET TEST REPORTS %ld ROOTS IN %ld-TH ENCLOSURE (SHOULD BE %ld)\n", nbrootsInDisc, i, mults[i]);
            res = 0;
        }
        
    }
    acb_clear(nroot);
    arb_clear(inclusionRadius);
    
    /* check that real roots are reported properly */
    for (i=0; (i<len)&&(res==1); i++) {
        if ( realCoeffs && arb_contains_zero( acb_imagref( roots+i) ) && (mults[i]==1) && !arb_is_zero(acb_imagref( roots+i)) ) {
            printf("CHECK OUTPUT: %ld-TH ENCLOSURE ISOLATES A REAL ROOT BUT IS NOT REPORTED AS \n", i);
            res = 0;
        }
    }
    
    if ( res && (! domain_is_C(dom)) && (nbRoots<goalNbRoots) ) {
        /* get all the roots */
        domain_t domC;
        domain_init(domC);
        acb_ptr allRoots = _acb_vec_init((p->length)-1);
        slong * allMults = (slong *) pwpoly_malloc ( ((p->length)-1)*sizeof(slong) );
        slong nbAllclusts = 0;
//         prec = PWPOLY_DEFAULT_PREC;
//         slong nbMaxItts = 500;
//         slong resG = pw_EA_solve_exact_acb_polynomial ( allRoots, allMults, &nbAllclusts, p, goal, log2eps, domC, &prec, nbMaxItts, 0, NULL);
//         
//         if ((resG<0) || (resG >= nbMaxItts)) {
//             printf("CHECK OUTPUT: SOLVING IN C FAILED, res: %ld\n", resG);
//             res = 0;
//         }
//         
//         if (res)
//             res = pwpoly_checkOutput_solve_exact_acb_poly ( allRoots, allMults, nbAllclusts, p, log2eps, goal, domC, (slong) goalNbRoots );
        ulong m = 10;
        fmpq_t scale, b;
        fmpq_init(scale);
        fmpq_init(b);
        fmpq_set_si(scale, 3, 2);
        fmpq_set_si(b, 2, 5);
        nbAllclusts = pwpoly_solve_acb_poly( allRoots, allMults, &m, p, goal, log2eps, domC, -1, 2, scale, b, 63 PW_VERBOSE_CALL(0) PW_FILE_CALL(NULL) );
        fmpq_clear(b);
        fmpq_clear(scale);
        res = pwpoly_checkOutput_solve_exact_acb_poly ( allRoots, allMults, nbAllclusts, p, log2eps, goal, domC, (slong) goalNbRoots );
        
        if (res==0) {
            printf("CHECK OUTPUT: CHECKING OUTPUT IN C FAILED, res: %d\n", res);
        } else {
            /* get the number of allRoots strictly in the domain */
            slong rootsstrictlyIn = 0;
            for ( slong i=0; i<nbAllclusts; i++ ) 
                if (domain_acb_is_strictly_in_domain( dom, allRoots+i )) {
                    rootsstrictlyIn += allMults[i];
                }
            /* check that the number of allRoots strictly in the domain <= the number of roots intersecting the domain */
            if ( rootsstrictlyIn > nbRoots ) {
                printf("CHECK OUTPUT: SOME ROOTS ARE MISSING\n");
                res = 0;
            }
        }
        pwpoly_free(allMults);
        _acb_vec_clear(allRoots, (p->length)-1);
        domain_clear(domC);
        
    }
    
    return res;
}

int pwpoly_checkOutput_solve_2fmpq_poly ( const acb_ptr roots, const slong * mults, slong len, const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong log2eps, int goal, const domain_t dom, slong goalNbRoots) {
    
    int res = 1;
    slong lenp = PWPOLY_MAX( p_re->length, p_im->length );
    int realCoeffs = fmpq_poly_is_zero(p_im);
    
    /* cap goalNbRoots */
    if (goalNbRoots<0)
        goalNbRoots = lenp-1;
    else
        goalNbRoots = PWPOLY_MIN( goalNbRoots, lenp-1 );
    
    slong i;
    /* check number of roots */
    slong nbRoots = 0;
    for (i=0; (i<len)&&(res==1); i++) {
        nbRoots += mults[i];
        if ( PW_GOAL_MUST_ISOLATE(goal) && (mults[i]!=1) ) {
            printf("CHECK OUTPUT: ROOTS NOT ISOLATED \n");
            res = 0;
        }
    }
    
    if ( domain_is_C(dom) ) {
        if ((res==1)&&(nbRoots < goalNbRoots)){
            printf("CHECK OUTPUT: BAD NUMBER OF ROOTS\n");
            res = 0;
        }
    } else {
        /* verify that all the roots intersect the domain */
        for (i=0; (i<len)&&(res==1); i++) {
            if ( !domain_acb_intersect_domain(dom, roots+i) ) {
                printf("CHECK OUTPUT: ENCLOSURE %ld DO NOT OVERLAP DOMAIN\n", i);
                res = 0;
            }
        }
    }
    
    /* check pairwise disjointness */
//     for (i=0; (i<len-1)&&(res==1); i++ ) {
//         for (slong j=i+1; (j<len)&&(res==1); j++) {
//             if ( acb_overlaps(roots+i, roots+j) ) {
//                 printf("CHECK OUTPUT: ENCLOSURES %ld AND %ld OVERLAP\n", i, j);
//                 res = 0;
//             }
//         }
//     }
    /* check pairwise disjointness and natural isolators*/
    acb_t inflated;
    acb_init(inflated);
    for (i=0; (i<len)&&(res==1); i++ ) {
        for (slong j=i+1; (j<len)&&(res==1); j++) {
            if ( acb_overlaps(roots+i, roots+j) ) {
//                 printf( "%ld-th enclosure: ", i );
//                 acb_printd( roots+i, 10);
//                 printf("\n");
//                 printf( "%ld-th enclosure: ", j );
//                 acb_printd( roots+j, 10);
//                 printf("\n");
                printf("CHECK OUTPUT: ENCLOSURES %ld AND %ld OVERLAP\n", i, j);
                res = 0;
            }
        }
        acb_set(inflated, roots+i);
        mag_mul_ui( arb_radref( acb_realref( inflated ) ), arb_radref( acb_realref( inflated ) ), 3 );
        mag_mul_ui( arb_radref( acb_imagref( inflated ) ), arb_radref( acb_imagref( inflated ) ), 3 );
        for (slong j=0; (j<len)&&(res==1); j++) {
            if ( (j!=i)&&( acb_overlaps(inflated, roots+j) ) ) {
                printf("CHECK OUTPUT: ENCLOSURES %ld IS NOT A NATURAL ISOLATOR\n", i);
                res = 0;
            }
        }
    }
    acb_clear(inflated);
    
    /* check precision */
    if ( PW_GOAL_MUST_APPROXIMATE(goal) ) {
        for (i=0; (i<len)&&(res==1); i++) {
            res = pwpoly_width_less_eps( roots+i, log2eps, PW_GOAL_PREC_RELATIVE(goal) );
            if (res==0)
                printf("CHECK OUTPUT: LOG2 OF ERROR OF ENCLOSURE %ld GREATER THAN %ld\n", i, log2eps);
        }
    }
    
    slong prec    = PWPOLY_DEFAULT_PREC;
    slong maxprec = LONG_MAX;
    
    acb_poly_t p;
    acb_poly_init(p);
    slong prec_p = prec;
    acb_poly_set2_fmpq_poly(p, p_re, p_im, prec_p);
    
    /* check number of roots */
    arb_t inclusionRadius;
    arb_init(inclusionRadius);
    acb_t nroot;
    acb_init(nroot);
    for (i=0; (i<len)&&(res==1); i++) {
        int resInclusion = 0;
        if (mults[i]==1) {
            /* try to compute inclusion disc */
            resInclusion = _pwpoly_checkOutput_get_inclusion_disc( inclusionRadius, p, roots+i, prec, 0 );
            if (resInclusion>0) {
                acb_set(nroot, roots+i);
                mag_zero( arb_radref(acb_realref(nroot)) );
                mag_zero( arb_radref(acb_imagref(nroot)) );
                acb_add_error_arb( nroot, inclusionRadius );
                resInclusion = acb_contains( roots + i, nroot );
//             printf(" resIsolating: %d\n", resIsolating);
            }
        }
        if (resInclusion) {
                continue;
        }
        /* else do a Newton Interval test */
        if (mults[i]==1) {
            resInclusion = _pwpoly_checkOutput_interval_newton( roots+i, p, prec);
        }
        if (resInclusion) {
                continue;
        }
        /* else do a Pellet test */
        slong nbrootsInDisc = _pwpoly_tstar_acb_2fmpq_poly( roots+i, p_re, p_im, lenp-1, &prec, maxprec );
//         printf( "%ld-th enclosure: ", i );
//         acb_printd( roots+i, 10);
//         printf("\n");
        if (nbrootsInDisc==TSTAR_MAX_PREC_REACHED)
            printf("CHECK OUTPUT: PELLET TEST FAILED FOR %ld-TH ENCLOSURE: MAXPREC (%ld) REACHED\n", i, maxprec);
        else if (nbrootsInDisc==TSTAR_ROOTS_NEAR_BOUND)
            printf("CHECK OUTPUT: PELLET TEST FAILED FOR %ld-TH ENCLOSURE: ROOT(S) NEAR BOUNDARY\n", i);
        else if (nbrootsInDisc!=mults[i]) {
            printf("CHECK OUTPUT: PELLET TEST REPORTS %ld ROOTS IN %ld-TH ENCLOSURE (SHOULD BE %ld)\n", nbrootsInDisc, i, mults[i]);
            res = 0;
        }
            
        if (prec > prec_p) {
            prec_p = prec;
            acb_poly_set2_fmpq_poly(p, p_re, p_im, prec_p);
        }
        
    }
    acb_poly_clear(p);
    acb_clear(nroot);
    arb_clear(inclusionRadius);
    
    /* check that real roots are reported properly */
    for (i=0; (i<len)&&(res==1); i++) {
        if ( realCoeffs && arb_contains_zero( acb_imagref( roots+i) ) && (mults[i]==1) && !arb_is_zero(acb_imagref( roots+i)) ) {
            printf("CHECK OUTPUT: %ld-TH ENCLOSURE ISOLATES A REAL ROOT BUT IS NOT REPORTED AS \n", i);
            res = 0;
        }
    }
    
    if ( res && (! domain_is_C(dom)) && (nbRoots<goalNbRoots) ) {
        /* get all the roots */
        domain_t domC;
        domain_init(domC);
        acb_ptr allRoots = _acb_vec_init(lenp-1);
        slong * allMults = (slong *) pwpoly_malloc ( (lenp-1)*sizeof(slong) );
        slong nbAllclusts = 0;
//         prec = PWPOLY_DEFAULT_PREC;
//         slong nbMaxItts = 500;
//         slong resG = pw_EA_solve_2fmpq_polynomial ( allRoots, allMults, &nbAllclusts, p_re, p_im, goal, log2eps, domC, &prec, nbMaxItts, 0, NULL);
//         
//         if ((resG<0) || (resG >= nbMaxItts)) {
//             printf("CHECK OUTPUT: SOLVING IN C FAILED, res: %ld\n", resG);
//             res = 0;
//         }
//         
//         if (res)
//             res = pwpoly_checkOutput_solve_2fmpq_poly ( allRoots, allMults, nbAllclusts, p_re, p_im, log2eps, goal, domC, (slong) goalNbRoots );
        
        ulong m = 10;
        fmpq_t scale, b;
        fmpq_init(scale);
        fmpq_init(b);
        fmpq_set_si(scale, 3, 2);
        fmpq_set_si(b, 2, 5);
        nbAllclusts = pwpoly_solve_2fmpq_poly( allRoots, allMults, &m, p_re, p_im, goal, log2eps, domC, -1, 2, scale, b, 63 PW_VERBOSE_CALL(0) PW_FILE_CALL(NULL) );
        fmpq_clear(b);
        fmpq_clear(scale);
        res = pwpoly_checkOutput_solve_2fmpq_poly ( allRoots, allMults, nbAllclusts, p_re, p_im, log2eps, goal, domC, (slong) goalNbRoots );
        
        if (res==0) {
            printf("CHECK OUTPUT: CHECKING OUTPUT IN C FAILED, res: %d\n", res);
        } else {
            /* get the number of allRoots strictly in the domain */
            slong rootsstrictlyIn = 0;
            for ( slong i=0; i<nbAllclusts; i++ ) 
                if (domain_acb_is_strictly_in_domain( dom, allRoots+i )) {
                    rootsstrictlyIn += allMults[i];
                }
            /* check that the number of allRoots strictly in the domain <= the number of roots intersecting the domain */
            if ( rootsstrictlyIn > nbRoots ) {
                printf("CHECK OUTPUT: SOME ROOTS ARE MISSING\n");
                res = 0;
            }
        }
        pwpoly_free(allMults);
        _acb_vec_clear(allRoots, lenp-1);
        domain_clear(domC);
        
    }
    
    return res;
    
}

slong acb_log_error_relative_to_tilde( const arb_t ptildeVal, const acb_t eval, slong prec ) {
    
    if (acb_is_exact(eval))
        return LONG_MIN;
    
    acb_t errors;
    acb_init(errors);
    
    arb_get_rad_arb( acb_realref(errors), acb_realref(eval) );
    arb_get_rad_arb( acb_imagref(errors), acb_imagref(eval) );
    arb_max( acb_realref(errors), acb_realref(errors), acb_imagref(errors), prec );
    arb_div( acb_realref(errors), acb_realref(errors), ptildeVal, prec );
    arb_log_base_ui( acb_realref(errors), acb_realref(errors), 2, prec );
    arb_get_ubound_arf( arb_midref( acb_realref(errors) ), acb_realref(errors), prec );
    slong res = arf_get_si( arb_midref( acb_realref(errors) ), ARF_RND_CEIL );
    
    acb_clear(errors);
    
    return res;
}

slong acb_log_error_absolute( const acb_t eval, slong prec ) {
    
    if (acb_is_exact(eval))
        return LONG_MIN;
    
    acb_t errors;
    acb_init(errors);
    
    arb_get_rad_arb( acb_realref(errors), acb_realref(eval) );
    arb_get_rad_arb( acb_imagref(errors), acb_imagref(eval) );
    arb_max( acb_realref(errors), acb_realref(errors), acb_imagref(errors), prec );
    arb_log_base_ui( acb_realref(errors), acb_realref(errors), 2, prec );
    arb_get_ubound_arf( arb_midref( acb_realref(errors) ), acb_realref(errors), prec );
    slong res = arf_get_si( arb_midref( acb_realref(errors) ), ARF_RND_CEIL );
    
    acb_clear(errors);
    
    return res;
}

int compareSlong (const void * a, const void * b)
{
  if ( *(slong*)a <  *(slong*)b ) return -1;
  if ( *(slong*)a == *(slong*)b ) return 0;
//   if ( *(slong*)a >  *(slong*)b ) return 1;
  return 1;
}

int pwpoly_checkOutput_evaluate_acb_poly( const acb_ptr values, const acb_ptr points, slong nbPoints, 
                                          const acb_poly_t pacb, slong m){
    
    int res = 1;
    slong prec = 2*m;
    
    acb_ptr valuesACB = _acb_vec_init( nbPoints );
    arb_ptr valuesTil = _arb_vec_init( nbPoints );
    
    slong  minlgErrRelTildePWE = LONG_MAX;
    slong  minlgErrRelTildeACB = LONG_MAX;
    slong  maxlgErrRelTildePWE = LONG_MIN;
    slong  maxlgErrRelTildeACB = LONG_MIN;
    double avelgErrRelTildePWE = 0.;
    double avelgErrRelTildeACB = 0.;
    
    slong  minlgErrAbsPWE = LONG_MAX;
    slong  minlgErrAbsACB = LONG_MAX;
    slong  maxlgErrAbsPWE = LONG_MIN;
    slong  maxlgErrAbsACB = LONG_MIN;
    double avelgErrAbsPWE = 0.;
    double avelgErrAbsACB = 0.;
    
    slong * lgErrRelTildePWE = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * lgErrRelTildeACB = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * lgErrAbsPWE = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * lgErrAbsACB = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    
    /* initialize and compute ptilde */
    arb_poly_t ptilde;
    arb_poly_init(ptilde);
    arb_poly_fit_length(ptilde, pacb->length);
    _arb_poly_set_length(ptilde, pacb->length);
    for (slong i = 0; i < pacb->length; i++)
        acb_abs( ptilde->coeffs + i, pacb->coeffs + i, prec );
    
    for (slong i=0; (i<nbPoints)&&res; i++) {
        acb_poly_evaluate_rectangular( valuesACB + i, pacb, points + i, prec );
        
//         printf("i: %ld; ", i);
//         acb_printd(valuesACB + i, 10); printf("\n");
//         printf("i: %ld; ", i);
//         acb_printd(values + i, 10); printf("\n");
        
        if (acb_overlaps( valuesACB + i, values + i )==0) {
            printf("CHECK OUTPUT: EVAL OF %ld-TH POINT DOES NOT OVERLAP ARB EVALUATION\n", i);
            res = 0;
        }
        
        acb_abs(valuesTil+i, points+i, prec);
        arb_poly_evaluate_rectangular(valuesTil+i, ptilde, valuesTil+i, prec);
        
        lgErrRelTildePWE[i] = acb_log_error_relative_to_tilde( valuesTil+i, values + i, prec );
        minlgErrRelTildePWE = PWPOLY_MIN(  minlgErrRelTildePWE, lgErrRelTildePWE[i] );
        maxlgErrRelTildePWE = PWPOLY_MAX(  maxlgErrRelTildePWE, lgErrRelTildePWE[i] );
        
        lgErrRelTildeACB[i] = acb_log_error_relative_to_tilde( valuesTil+i, valuesACB + i, prec );
        minlgErrRelTildeACB = PWPOLY_MIN( minlgErrRelTildeACB, lgErrRelTildeACB[i] );
        maxlgErrRelTildeACB = PWPOLY_MAX( maxlgErrRelTildeACB, lgErrRelTildeACB[i] );
        
        avelgErrRelTildePWE += ((double)lgErrRelTildePWE[i])/((double)nbPoints);
        avelgErrRelTildeACB += ((double)lgErrRelTildeACB[i])/((double)nbPoints);
        
        lgErrAbsPWE[i] = acb_log_error_absolute( values + i, prec );
        minlgErrAbsPWE = PWPOLY_MIN(  minlgErrAbsPWE, lgErrAbsPWE[i] );
        maxlgErrAbsPWE = PWPOLY_MAX(  maxlgErrAbsPWE, lgErrAbsPWE[i] );
        
        lgErrAbsACB[i] = acb_log_error_absolute( valuesACB + i, prec );
        minlgErrAbsACB = PWPOLY_MIN( minlgErrAbsACB, lgErrAbsACB[i] );
        maxlgErrAbsACB = PWPOLY_MAX( maxlgErrAbsACB, lgErrAbsACB[i] );
        
        avelgErrAbsPWE += ((double)lgErrAbsPWE[i])/((double)nbPoints);
        avelgErrAbsACB += ((double)lgErrAbsACB[i])/((double)nbPoints);
    }
    
    qsort (lgErrRelTildePWE, nbPoints, sizeof(slong), compareSlong);
    qsort (lgErrRelTildeACB, nbPoints, sizeof(slong), compareSlong);
    slong medlgErrRelTildePWE = lgErrRelTildePWE[nbPoints/2];
    slong medlgErrRelTildeACB = lgErrRelTildeACB[nbPoints/2];
    
    qsort (lgErrAbsPWE, nbPoints, sizeof(slong), compareSlong);
    qsort (lgErrAbsACB, nbPoints, sizeof(slong), compareSlong);
    slong medlgErrAbsPWE = lgErrAbsPWE[nbPoints/2];
    slong medlgErrAbsACB = lgErrAbsACB[nbPoints/2];
    
    printf(" CHECK: log2 of error rel to tilde PWE: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrRelTildePWE, maxlgErrRelTildePWE, medlgErrRelTildePWE, avelgErrRelTildePWE);
    printf(" CHECK: log2 of error rel to tilde ACB: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrRelTildeACB, maxlgErrRelTildeACB, medlgErrRelTildeACB, avelgErrRelTildeACB);
    
    printf(" CHECK: log2 of absolute error     PWE: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrAbsPWE, maxlgErrAbsPWE, medlgErrAbsPWE, avelgErrAbsPWE);
    printf(" CHECK: log2 of absolute error     ACB: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrAbsACB, maxlgErrAbsACB, medlgErrAbsACB, avelgErrAbsACB);
    
    arb_poly_clear(ptilde);
    pwpoly_free(lgErrAbsACB);
    pwpoly_free(lgErrAbsPWE);
    pwpoly_free(lgErrRelTildeACB);
    pwpoly_free(lgErrRelTildePWE);
    
    _arb_vec_clear(valuesTil, nbPoints);
    _acb_vec_clear(valuesACB, nbPoints );
    
    return res;
}

int pwpoly_checkOutput_evaluate2_acb_poly( const acb_ptr values, const acb_ptr valuesDer, const acb_ptr points, slong nbPoints, 
                                           const acb_poly_t pacb, slong m){
    
    int res = 1;
    slong prec = 2*m;
    
    acb_ptr valuesACB = _acb_vec_init( nbPoints );
    arb_ptr valuesTil = _arb_vec_init( nbPoints );
    acb_ptr valuesACBDer = _acb_vec_init( nbPoints );
    arb_ptr valuesTilDer = _arb_vec_init( nbPoints );
    
    slong  minlgErrRelTildePWE = LONG_MAX;
    slong  minlgErrRelTildeACB = LONG_MAX;
    slong  maxlgErrRelTildePWE = LONG_MIN;
    slong  maxlgErrRelTildeACB = LONG_MIN;
    double avelgErrRelTildePWE = 0.;
    double avelgErrRelTildeACB = 0.;
    
    slong  minlgErrAbsPWE = LONG_MAX;
    slong  minlgErrAbsACB = LONG_MAX;
    slong  maxlgErrAbsPWE = LONG_MIN;
    slong  maxlgErrAbsACB = LONG_MIN;
    double avelgErrAbsPWE = 0.;
    double avelgErrAbsACB = 0.;
    
    slong  minlgErrRelTildePWEDer = LONG_MAX;
    slong  minlgErrRelTildeACBDer = LONG_MAX;
    slong  maxlgErrRelTildePWEDer = LONG_MIN;
    slong  maxlgErrRelTildeACBDer = LONG_MIN;
    double avelgErrRelTildePWEDer = 0.;
    double avelgErrRelTildeACBDer = 0.;
    
    slong  minlgErrAbsPWEDer = LONG_MAX;
    slong  minlgErrAbsACBDer = LONG_MAX;
    slong  maxlgErrAbsPWEDer = LONG_MIN;
    slong  maxlgErrAbsACBDer = LONG_MIN;
    double avelgErrAbsPWEDer = 0.;
    double avelgErrAbsACBDer = 0.;
    
    slong * lgErrRelTildePWE = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * lgErrRelTildeACB = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * lgErrAbsPWE = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * lgErrAbsACB = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    
    slong * lgErrRelTildePWEDer = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * lgErrRelTildeACBDer = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * lgErrAbsPWEDer = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * lgErrAbsACBDer = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    
    /* initialize and compute ptilde*/
    arb_poly_t ptilde;
    arb_poly_init(ptilde);
    arb_poly_fit_length(ptilde, pacb->length);
    _arb_poly_set_length(ptilde, pacb->length);
    for (slong i = 0; i < pacb->length; i++)
        acb_abs( ptilde->coeffs + i, pacb->coeffs + i, prec );
    
    
    for (slong i=0; (i<nbPoints)&&res; i++) {
        acb_poly_evaluate2_rectangular( valuesACB + i, valuesACBDer + i, pacb, points + i, prec );
//         printf("i: %ld; ", i);
//         acb_printd(valuesACB + i, 10); printf(", ");
//         acb_printd(valuesACBDer + i, 10); printf("\n");
//         printf("i: %ld; ", i);
//         acb_printd(values + i, 10); printf(", ");
//         acb_printd(valuesDer + i, 10); printf("\n");
        
        if (acb_overlaps( valuesACB + i, values + i )==0) {
            printf("CHECK OUTPUT: EVAL  OF %ld-TH POINT DOES NOT OVERLAP ARB EVALUATION\n", i);
            res = 0;
        }
        if (acb_overlaps( valuesACBDer + i, valuesDer + i )==0) {
            printf("CHECK OUTPUT: EVAL' OF %ld-TH POINT DOES NOT OVERLAP ARB EVALUATION\n", i);
            res = 0;
        }
        
        acb_abs(valuesTil+i, points+i, prec);
        /* because tilde(p')=(tilde(p))' */
        arb_poly_evaluate2_rectangular(valuesTil+i, valuesTilDer+i,  ptilde, valuesTil+i, prec);
        
        lgErrRelTildePWE[i] = acb_log_error_relative_to_tilde( valuesTil+i, values + i, prec );
        minlgErrRelTildePWE = PWPOLY_MIN(  minlgErrRelTildePWE, lgErrRelTildePWE[i] );
        maxlgErrRelTildePWE = PWPOLY_MAX(  maxlgErrRelTildePWE, lgErrRelTildePWE[i] );
        
        lgErrRelTildeACB[i] = acb_log_error_relative_to_tilde( valuesTil+i, valuesACB + i, prec );
        minlgErrRelTildeACB = PWPOLY_MIN( minlgErrRelTildeACB, lgErrRelTildeACB[i] );
        maxlgErrRelTildeACB = PWPOLY_MAX( maxlgErrRelTildeACB, lgErrRelTildeACB[i] );
        
        avelgErrRelTildePWE += ((double)lgErrRelTildePWE[i])/((double)nbPoints);
        avelgErrRelTildeACB += ((double)lgErrRelTildeACB[i])/((double)nbPoints);
        
        lgErrAbsPWE[i] = acb_log_error_absolute( values + i, prec );
        minlgErrAbsPWE = PWPOLY_MIN(  minlgErrAbsPWE, lgErrAbsPWE[i] );
        maxlgErrAbsPWE = PWPOLY_MAX(  maxlgErrAbsPWE, lgErrAbsPWE[i] );
        
        lgErrAbsACB[i] = acb_log_error_absolute( valuesACB + i, prec );
        minlgErrAbsACB = PWPOLY_MIN( minlgErrAbsACB, lgErrAbsACB[i] );
        maxlgErrAbsACB = PWPOLY_MAX( maxlgErrAbsACB, lgErrAbsACB[i] );
        
        avelgErrAbsPWE += ((double)lgErrAbsPWE[i])/((double)nbPoints);
        avelgErrAbsACB += ((double)lgErrAbsACB[i])/((double)nbPoints);
        
        
        
        lgErrRelTildePWEDer[i] = acb_log_error_relative_to_tilde( valuesTilDer+i, valuesDer + i, prec );
        minlgErrRelTildePWEDer = PWPOLY_MIN(  minlgErrRelTildePWEDer, lgErrRelTildePWEDer[i] );
        maxlgErrRelTildePWEDer = PWPOLY_MAX(  maxlgErrRelTildePWEDer, lgErrRelTildePWEDer[i] );
        
        lgErrRelTildeACBDer[i] = acb_log_error_relative_to_tilde( valuesTilDer+i, valuesACBDer + i, prec );
        minlgErrRelTildeACBDer = PWPOLY_MIN( minlgErrRelTildeACBDer, lgErrRelTildeACBDer[i] );
        maxlgErrRelTildeACBDer = PWPOLY_MAX( maxlgErrRelTildeACBDer, lgErrRelTildeACBDer[i] );
        
        avelgErrRelTildePWEDer += ((double)lgErrRelTildePWEDer[i])/((double)nbPoints);
        avelgErrRelTildeACBDer += ((double)lgErrRelTildeACBDer[i])/((double)nbPoints);
        
        lgErrAbsPWEDer[i] = acb_log_error_absolute( valuesDer + i, prec );
        minlgErrAbsPWEDer = PWPOLY_MIN(  minlgErrAbsPWEDer, lgErrAbsPWEDer[i] );
        maxlgErrAbsPWEDer = PWPOLY_MAX(  maxlgErrAbsPWEDer, lgErrAbsPWEDer[i] );
        
        lgErrAbsACBDer[i] = acb_log_error_absolute( valuesACBDer + i, prec );
        minlgErrAbsACBDer = PWPOLY_MIN( minlgErrAbsACBDer, lgErrAbsACBDer[i] );
        maxlgErrAbsACBDer = PWPOLY_MAX( maxlgErrAbsACBDer, lgErrAbsACBDer[i] );
        
        avelgErrAbsPWEDer += ((double)lgErrAbsPWEDer[i])/((double)nbPoints);
        avelgErrAbsACBDer += ((double)lgErrAbsACBDer[i])/((double)nbPoints);
    }
    
    qsort (lgErrRelTildePWE, nbPoints, sizeof(slong), compareSlong);
    qsort (lgErrRelTildeACB, nbPoints, sizeof(slong), compareSlong);
    slong medlgErrRelTildePWE = lgErrRelTildePWE[nbPoints/2];
    slong medlgErrRelTildeACB = lgErrRelTildeACB[nbPoints/2];
    
    qsort (lgErrAbsPWE, nbPoints, sizeof(slong), compareSlong);
    qsort (lgErrAbsACB, nbPoints, sizeof(slong), compareSlong);
    slong medlgErrAbsPWE = lgErrAbsPWE[nbPoints/2];
    slong medlgErrAbsACB = lgErrAbsACB[nbPoints/2];
    
    qsort (lgErrRelTildePWEDer, nbPoints, sizeof(slong), compareSlong);
    qsort (lgErrRelTildeACBDer, nbPoints, sizeof(slong), compareSlong);
    slong medlgErrRelTildePWEDer = lgErrRelTildePWEDer[nbPoints/2];
    slong medlgErrRelTildeACBDer = lgErrRelTildeACBDer[nbPoints/2];
    
    qsort (lgErrAbsPWEDer, nbPoints, sizeof(slong), compareSlong);
    qsort (lgErrAbsACBDer, nbPoints, sizeof(slong), compareSlong);
    slong medlgErrAbsPWEDer = lgErrAbsPWEDer[nbPoints/2];
    slong medlgErrAbsACBDer = lgErrAbsACBDer[nbPoints/2];
    
    printf(" CHECK: log2 of error rel to tilde PWE: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrRelTildePWE, maxlgErrRelTildePWE, medlgErrRelTildePWE, avelgErrRelTildePWE);
    printf(" CHECK: log2 of error rel to tilde ACB: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrRelTildeACB, maxlgErrRelTildeACB, medlgErrRelTildeACB, avelgErrRelTildeACB);
    
    printf(" CHECK: log2 of absolute error     PWE: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrAbsPWE, maxlgErrAbsPWE, medlgErrAbsPWE, avelgErrAbsPWE);
    printf(" CHECK: log2 of absolute error     ACB: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrAbsACB, maxlgErrAbsACB, medlgErrAbsACB, avelgErrAbsACB);
    
    printf(" CHECK: DERIVATIVES \n");
    printf(" CHECK: log2 of error rel to tilde PWE: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrRelTildePWEDer, maxlgErrRelTildePWEDer, medlgErrRelTildePWEDer, avelgErrRelTildePWEDer);
    printf(" CHECK: log2 of error rel to tilde ACB: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrRelTildeACBDer, maxlgErrRelTildeACBDer, medlgErrRelTildeACBDer, avelgErrRelTildeACBDer);
    
    printf(" CHECK: log2 of absolute error     PWE: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrAbsPWEDer, maxlgErrAbsPWEDer, medlgErrAbsPWEDer, avelgErrAbsPWEDer);
    printf(" CHECK: log2 of absolute error     ACB: min: %ld, max: %ld, med: %ld, ave: %lf \n",
           minlgErrAbsACBDer, maxlgErrAbsACBDer, medlgErrAbsACBDer, avelgErrAbsACBDer);
    
    arb_poly_clear(ptilde);
    pwpoly_free(lgErrAbsACBDer);
    pwpoly_free(lgErrAbsPWEDer);
    pwpoly_free(lgErrRelTildeACBDer);
    pwpoly_free(lgErrRelTildePWEDer);
    pwpoly_free(lgErrAbsACB);
    pwpoly_free(lgErrAbsPWE);
    pwpoly_free(lgErrRelTildeACB);
    pwpoly_free(lgErrRelTildePWE);
    
    _arb_vec_clear(valuesTilDer, nbPoints);
    _acb_vec_clear(valuesACBDer, nbPoints );
    _arb_vec_clear(valuesTil, nbPoints);
    _acb_vec_clear(valuesACB, nbPoints );
    
    return res;
}

int pwpoly_checkOutput_evaluate_2fmpq_poly( const acb_ptr values, const acb_ptr points, slong nbPoints, 
                                            const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong m){
    
    int res = 1;
    slong prec = 2*m;
    
    acb_poly_t pacb;
    acb_poly_init(pacb);
    acb_poly_set2_fmpq_poly( pacb, p_re, p_im, prec);
    
    res = pwpoly_checkOutput_evaluate_acb_poly( values, points, nbPoints, pacb, m);
    
    acb_poly_clear(pacb);
    
    return res;
}

int pwpoly_checkOutput_evaluate2_2fmpq_poly( const acb_ptr values, const acb_ptr valuesDer, const acb_ptr points, slong nbPoints, 
                                             const fmpq_poly_t p_re, const fmpq_poly_t p_im, slong m){
    
    int res = 1;
    slong prec = 2*m;
    
    acb_poly_t pacb;
    acb_poly_init(pacb);
    acb_poly_set2_fmpq_poly( pacb, p_re, p_im, prec);
    
    res = pwpoly_checkOutput_evaluate2_acb_poly( values, valuesDer, points, nbPoints, pacb, m);
    
    acb_poly_clear(pacb);
    
    return res;
}

#endif
