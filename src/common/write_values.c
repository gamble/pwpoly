/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_common.h"

#ifndef PW_NO_INTERFACE
void pwpoly_write_values( FILE * outFile, acb_ptr values, slong len, slong nbDigitsDefault, slong m ) {
    
    arb_t fac, temp;
    arb_init(fac);
    arb_init(temp);
    arb_log_ui(fac, 2, PWPOLY_DEFAULT_PREC);
    arb_log_ui(temp, 10, PWPOLY_DEFAULT_PREC);
    arb_div(fac, fac, temp, PWPOLY_DEFAULT_PREC);
    
    for (slong i=0; i< len; i++) {
        
//         printf("i: %ld\n", i);
        
        slong nbd = nbDigitsDefault;
        
        if (!acb_is_zero(values+i)) {
            slong rel_acc = arb_rel_accuracy_bits( acb_realref( values+i ) );
//             printf("rel_acc: %ld\n", rel_acc);
            if (!arb_is_zero( acb_imagref( values+i ) ) )
                rel_acc = PWPOLY_MAX( rel_acc, arb_rel_accuracy_bits( acb_imagref( values+i ) ) );
            
            rel_acc = PWPOLY_MIN( rel_acc, 2*m );
            
            if (rel_acc > 0) {
                arb_set_si( temp, rel_acc );
                arb_mul(temp, temp, fac, PWPOLY_DEFAULT_PREC );
                arb_get_ubound_arf( arb_midref( temp ), temp, PWPOLY_DEFAULT_PREC );
                nbd = PWPOLY_MAX(nbd, arf_get_si( arb_midref( temp ), ARF_RND_UP ) );
            }
        }
        
        acb_fprintn(outFile, values+i, nbd, 0);
        fprintf( outFile, "\n");
        
    }
    
    arb_clear(temp);
    arb_clear(fac);
}
#endif
