#!/bin/bash
POLNAME="randomDense"

DEGREES="128 191 256"
BITSIZES="16"
NBDIGITS="iso 10 16 32"
DOMAINS="C"
NBMXSOLS="-1"
# DEGREES="1024 1535 2048 3073 4096 6145"
# BITSIZES="16"
# NBDIGITS="10 16"
# DEGREES="128"
# BITSIZES="16"
# NBDIGITS="10"
NBPOLS="4"
# CHECKPWOUT=1

source getParams.sh

source getSolvers.sh

source getFunctions.sh

TEST=""
for NBD in $NBDIGITS; do
    TEMP=`convert_log10_log2 $NBD`
    TEST=$TEST$TEMP" "
done
${ECHO} "table_${POLNAME} : degrees = ${DEGREES} bitsizes = ${BITSIZES} nb of correct digits: ${NBDIGITS}, log2 of eps: ${TEST}"

init_rep $REP $PURGE

TEMPTABFILE="temptabfile_$POLNAME.txt"
touch $TEMPTABFILE

TABLEOUT=$POLNAME".tex"
rm -rf $TABLEOUT
touch $TABLEOUT

for BIT in $BITSIZES; do
    for DEG in $DEGREES; do
        for NBP in `seq 1 $NBPOLS`; do
            FILENAME=$REP"/"$POLNAME
            genRandomHyp_with_deg_bs_nbpol $FILENAME $POLNAME $DEG $BIT $NBP
            FILENAME=$FILENAME"_"$DEG"_"$BIT"_"$NBP
            for DOM in $DOMAINS; do
            
                for NBM in $NBMXSOLS; do
        
                    for NBD in $NBDIGITS; do
                        if [ "$NBD" = "iso" ]; then
                            #ISOLATE
                            if [ "$DOM" = "C" ] && [ "$NBM" = "-1" ]; then
                                run_mpsolve_isolate $FILENAME $POLNAME $DEG $BIT
                            fi
                            run_hefroots_isolate $FILENAME $POLNAME $DEG $BIT $DOM $NBM
                            run_pwroots_isolate $FILENAME $POLNAME $DEG $BIT $DOM $NBM
                        else
                            #APPROXIMATE
                            if [ "$DOM" = "C" ] && [ "$NBM" = "-1" ]; then
                                run_mpsolve_approximate $FILENAME $POLNAME $DEG $BIT $NBD
                            fi
                            run_hefroots_approximate $FILENAME $POLNAME $DEG $BIT $NBD $DOM $NBM
                            run_pwroots_approximate $FILENAME $POLNAME $DEG $BIT $NBD $DOM $NBM
                        fi
                    done
                
                done
                
#                 echo "\\n\\n\\n\\n\\n $DOM \\n\\n\\n\\n\\n"
#                 #ISOLATE
#                 if [ "$DOM" = "C" ]; then
#                     run_mpsolve_isolate $FILENAME $POLNAME $DEG $BIT
#                 fi
#                 run_hefroots_isolate $FILENAME $POLNAME $DEG $BIT $DOM
#                 run_pwroots_isolate $FILENAME $POLNAME $DEG $BIT $DOM
#                 #APPROXIMATE
#                 for NBD in $NBDIGITS; do
#                     if [ "$DOM" = "C" ]; then
#                         run_mpsolve_approximate $FILENAME $POLNAME $DEG $BIT $NBD
#                     fi
#                     run_hefroots_approximate $FILENAME $POLNAME $DEG $BIT $NBD $DOM
#                     run_pwroots_approximate $FILENAME $POLNAME $DEG $BIT $NBD $DOM
#                 done
                
            done
        done
    done
done

stats_random()
{
    IN_FILENAME=$1
    IN_DEG=$2
    IN_BIT=$3
    IN_NBD=$4
    IN_NBP=$5
    
    T_MPSOLVE=0
#     TS_MPSOLVE=0
    T_HEFROOT=0
    T_PWROOTS=0
    
    for CURIND in `seq 1 $IN_NBP`; do
        OUT_MPSOLVE=$IN_FILENAME"_"$CURIND".out_mps_"$IN_NBD
        OUT_HEFROOT=$IN_FILENAME"_"$CURIND".out_hef_"$IN_NBD
        OUT_PWROOTS=$IN_FILENAME"_"$CURIND".out_pw_"$IN_NBD
        
        T_MPSOLVE_T=$(grep "cpu"         $OUT_MPSOLVE| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_MPSOLVE=`echo $T_MPSOLVE+$T_MPSOLVE_T|bc -l`
        
        T_HEFROOT_T=$(grep "cpu"         $OUT_HEFROOT| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_HEFROOT=`echo $T_HEFROOT+$T_HEFROOT_T|bc -l`
        
        T_PWROOTS_T=$(grep "cpu"         $OUT_PWROOTS| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_PWROOTS=`echo $T_PWROOTS+$T_PWROOTS_T|bc -l`
    done
    
    LINE_TAB=$IN_DEG" & "$IN_BIT" & "$IN_NBD
    
    T_MPSOLVE=`echo $T_MPSOLVE/$IN_NBP|bc -l`
    T_HEFROOT=`echo $T_HEFROOT/$IN_NBP|bc -l`
    T_PWROOTS=`echo $T_PWROOTS/$IN_NBP|bc -l`
    
    LINE_TAB=$LINE_TAB" & `format_time $T_MPSOLVE`"
    LINE_TAB=$LINE_TAB" & `format_time $T_HEFROOT`"
    LINE_TAB=$LINE_TAB" & `format_time $T_PWROOTS`"
    
    LINE_TAB=$LINE_TAB"\\\\"
    echo $LINE_TAB >> $TEMPTABFILE
}

stats_random_domains()
{
    IN_FILENAME=$1
    IN_DEG=$2
    IN_BIT=$3
    IN_NBD=$4
    IN_NBP=$5
    IN_DOM=$6
    
    T_HEFROOT=0
    T_PWROOTS=0
    
    for CURIND in `seq 1 $IN_NBP`; do
        OUT_HEFROOT=$IN_FILENAME"_"$CURIND".out_hef_"$IN_NBD
        OUT_PWROOTS=$IN_FILENAME"_"$CURIND".out_pw_"$IN_NBD
            
        if [ "$DOM" != "C" ]; then
            TEMP=`convert_domain_str $IN_DOM`
            OUT_HEFROOT=$OUT_HEFROOT"_"$TEMP
            OUT_PWROOTS=$OUT_PWROOTS"_"$TEMP
        fi
        
        T_HEFROOT_T=$(grep "cpu"         $OUT_HEFROOT| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_HEFROOT=`echo $T_HEFROOT+$T_HEFROOT_T|bc -l`
        
        T_PWROOTS_T=$(grep "cpu"         $OUT_PWROOTS| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_PWROOTS=`echo $T_PWROOTS+$T_PWROOTS_T|bc -l`
    done
    
    T_HEFROOT=`echo $T_HEFROOT/$IN_NBP|bc -l`
    T_PWROOTS=`echo $T_PWROOTS/$IN_NBP|bc -l`
    
    LINE_TAB=$LINE_TAB" & `format_time $T_HEFROOT`"
    LINE_TAB=$LINE_TAB" & `format_time $T_PWROOTS`"

}

stats_random_domains_nbr()
{
    IN_FILENAME=$1
    IN_DEG=$2
    IN_BIT=$3
    IN_NBD=$4
    IN_NBP=$5
    IN_DOM=$6
    IN_NBM=$7
    
    T_HEFROOT=0
    T_PWROOTS=0
    
    for CURIND in `seq 1 $IN_NBP`; do
        OUT_HEFROOT=$IN_FILENAME"_"$CURIND".out_hef_"$IN_NBD
        OUT_PWROOTS=$IN_FILENAME"_"$CURIND".out_pw_"$IN_NBD
            
        if [ "$DOM" != "C" ]; then
            TEMP=`convert_domain_str $IN_DOM`
            OUT_HEFROOT=$OUT_HEFROOT"_"$TEMP
            OUT_PWROOTS=$OUT_PWROOTS"_"$TEMP
        fi
        
        if [ "$IN_NBM" != "-1" ]; then
            OUT_HEFROOT=$OUT_HEFROOT"_"$IN_NBM
            OUT_PWROOTS=$OUT_PWROOTS"_"$IN_NBM
        fi
        
        T_HEFROOT_T=$(grep "cpu"         $OUT_HEFROOT| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_HEFROOT=`echo $T_HEFROOT+$T_HEFROOT_T|bc -l`
        
        T_PWROOTS_T=$(grep "cpu"         $OUT_PWROOTS| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_PWROOTS=`echo $T_PWROOTS+$T_PWROOTS_T|bc -l`
    done
    
    
    T_HEFROOT=`echo $T_HEFROOT/$IN_NBP|bc -l`
    LINE_TAB=$LINE_TAB" & `format_time $T_HEFROOT`"

    T_PWROOTS=`echo $T_PWROOTS/$IN_NBP|bc -l`
    LINE_TAB=$LINE_TAB" & `format_time $T_PWROOTS`"

}

if [ $CHECKPWOUT -eq 0 ]; then

    if [ "$DOMAINS" = "C" ]; then

        for DEG in $DEGREES; do
            for BIT in $BITSIZES; do
                FILENAME=$REP"/"$POLNAME
                FILENAME=$FILENAME"_"$DEG"_"$BIT
                for NBD in $NBDIGITS; do
                    stats_random $FILENAME $DEG $BIT $NBD $NBPOLS
                done
                echo "\\hline" >> $TEMPTABFILE
            done
            echo "\\hline" >> $TEMPTABFILE
        done

        HEAD_TABLE="\begin{tabular}{|c|c|c||c||c||c|}"
        FIRST_LINE="   & & & \texttt{MPSolve} & \texttt{HEFRoots} & \texttt{PWRoots}\\\\\\hline"
        SECOND_LINE=" $\\deg$ & $\\bitsize$ & goal/$\log_{10} \\varepsilon$ & cpu time in s & cpu time in s & cpu time in s \\\\\\hline"
        THIRD_LINE="\\multicolumn{6}{|c|}{Averages on $NBPOLS random dense pols with real coeffs }\\\\\\hline"
        TAIL_TAB="\end{tabular}"
        
        echo $HEAD_TABLE >> $TABLEOUT
        echo $FIRST_LINE >> $TABLEOUT
        echo $SECOND_LINE >> $TABLEOUT
        echo $THIRD_LINE >> $TABLEOUT
        cat $TEMPTABFILE >> $TABLEOUT
        echo $TAIL_TAB >> $TABLEOUT
    
    else
    
        HEAD_TABLE="\begin{tabular}{|c|c|c|"
        DOMAIN_LINE="   & &"
        FIRST_LINE="   & &"
        SECOND_LINE=" $\\deg$ & $\\bitsize$ & goal/$\log_{10} \\varepsilon$"
        
        if [ "$NBMXSOLS" != "-1" ]; then
            HEAD_TABLE=$HEAD_TABLE"c|"
            DOMAIN_LINE=$DOMAIN_LINE" &"
            FIRST_LINE=$FIRST_LINE" &"
            SECOND_LINE=$SECOND_LINE" & n"
        fi
        
        NBDOMAINS=0
        for DOM in $DOMAINS; do
        
            HEAD_TABLE=$HEAD_TABLE"|c|c|"
            DOMAIN_LINE=$DOMAIN_LINE" &\\multicolumn{2}{|c|}{[$DOM]}"
            FIRST_LINE=$FIRST_LINE" & \texttt{HEFRoots} & \texttt{PWRoots}"
            SECOND_LINE=$SECOND_LINE" & t in s & t in s"
            NBDOMAINS=`echo "$NBDOMAINS+1" | bc`
            
        done
        
        for DEG in $DEGREES; do
            for BIT in $BITSIZES; do
                FILENAME=$REP"/"$POLNAME
                FILENAME=$FILENAME"_"$DEG"_"$BIT
                
                for NBD in $NBDIGITS; do
                    
                    if [ "$NBMXSOLS" = "-1" ]; then
                        LINE_TAB=$DEG" & "$BIT" & $NBD"
                        for DOM in $DOMAINS; do
                            stats_random_domains $FILENAME $DEG $BIT $NBD $NBPOLS $DOM
                        done
                        LINE_TAB=$LINE_TAB"\\\\"
                        echo $LINE_TAB >> $TEMPTABFILE
                    else
                        for NBM in $NBMXSOLS; do
                            LINE_TAB=$DEG" & "$BIT" & "$NBD" & "
                            if [ "$NBM" != "-1" ]; then
                                LINE_TAB=$LINE_TAB$NBM
                            fi
                            for DOM in $DOMAINS; do
                                stats_random_domains_nbr $FILENAME $DEG $BIT $NBD $NBPOLS $DOM $NBM
                            done
                            LINE_TAB=$LINE_TAB"\\\\"
                            echo $LINE_TAB >> $TEMPTABFILE
                        done
                    fi
                done
                
                echo "\\hline" >> $TEMPTABFILE
            done
            echo "\\hline" >> $TEMPTABFILE
        done
        
        HEAD_TABLE=$HEAD_TABLE"}"
        DOMAIN_LINE=$DOMAIN_LINE"\\\\\\hline"
        FIRST_LINE=$FIRST_LINE"\\\\\\hline"
        SECOND_LINE=$SECOND_LINE"\\\\\\hline"
        NBCOLS=`echo "3 + 2*$NBDOMAINS" | bc`
        THIRD_LINE="\\multicolumn{$NBCOLS}{|c|}{Averages on $NBPOLS random dense pols with real coeffs }\\\\\\hline"
        TAIL_TAB="\end{tabular}"
        
        echo $HEAD_TABLE >> $TABLEOUT
        echo $DOMAIN_LINE >> $TABLEOUT
        echo $FIRST_LINE >> $TABLEOUT
        echo $SECOND_LINE >> $TABLEOUT
        echo $THIRD_LINE >> $TABLEOUT
        cat $TEMPTABFILE >> $TABLEOUT
        echo $TAIL_TAB >> $TABLEOUT
    
    fi

    rm -rf $TEMPTABFILE
    
fi
