#!/bin/bash
POLNAME="randomDenseCha"

# DEGREES="20 30 40 50 60 80"
DEGREES="100 200 400 500 600"
BITSIZES="8"
NBDIGITS="10 16 32"
# DEGREES="1024 1535 2048 3073 4096 6145 8192"
# BITSIZES="16"
# NBDIGITS="10 16"
# DEGREES="128"
# BITSIZES="16"
# NBDIGITS="10"
NBPOLS="2"
# CHECKPWOUT=1
DEGMAXHEF="400"

source getParams.sh

source getSolvers.sh

source getFunctions.sh

TEST=""
for NBD in $NBDIGITS; do
    TEMP=`convert_log10_log2 $NBD`
    TEST=$TEST$TEMP" "
done
${ECHO} "table_${POLNAME} : degrees = ${DEGREES} nb of correct digits: ${NBDIGITS}, log2 of eps: ${TEST}"

init_rep $REP $PURGE

TEMPTABFILE="temptabfile_$POLNAME.txt"
touch $TEMPTABFILE

TABLEOUT=$POLNAME".tex"
rm -rf $TABLEOUT
touch $TABLEOUT

for DEG in $DEGREES; do
    for NBP in `seq 1 $NBPOLS`; do
        FILENAME=$REP"/"$POLNAME
        genRandomCha_with_deg_nbpol $FILENAME $POLNAME $DEG $NBP
        FILENAME=$FILENAME"_"$DEG"_"$NBP
        ${ECHO} ${FILENAME}
        #ISOLATE
        run_mpsolve_isolate $FILENAME $POLNAME $DEG 1
        if [ $DEG -le $DEGMAXHEF ]; then
            run_hefroots_isolate $FILENAME $POLNAME $DEG 1
        fi
        run_pwroots_isolate $FILENAME $POLNAME $DEG 1
        #APPROXIMATE
        for NBD in $NBDIGITS; do
            run_mpsolve_approximate $FILENAME $POLNAME $DEG 1 $NBD
            if [ $DEG -le $DEGMAXHEF ]; then
                run_hefroots_approximate $FILENAME $POLNAME $DEG 1 $NBD
            fi
            run_pwroots_approximate $FILENAME $POLNAME $DEG 1 $NBD "C"
        done
    done
done

stats_random()
{
    IN_FILENAME=$1
    IN_DEG=$2
    IN_NBD=$3
    IN_NBP=$4
    
    T_MPSOLVE=0
#     TS_MPSOLVE=0
    T_HEFROOT=0
    T_PWROOTS=0
    
    for CURIND in `seq 1 $IN_NBP`; do
        OUT_MPSOLVE=$IN_FILENAME"_"$CURIND".out_mps_"$IN_NBD
        OUT_HEFROOT=$IN_FILENAME"_"$CURIND".out_hef_"$IN_NBD
        OUT_PWROOTS=$IN_FILENAME"_"$CURIND".out_pw_"$IN_NBD
        
#         T_MPSOLVE_T=$(grep "real"         $OUT_MPSOLVE| cut -f2 -d'l' | tr -d ' ' | tr -d '\n')
        T_MPSOLVE_T=$(grep "cpu"         $OUT_MPSOLVE| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_MPSOLVE=`echo $T_MPSOLVE+$T_MPSOLVE_T|bc -l`
#         TS_MPSOLVE=`echo $TS_MPSOLVE+$TS_MPSOLVE_T|bc -l`
        
        if [ $DEG -le $DEGMAXHEF ]; then
            T_HEFROOT_T=$(grep "cpu"         $OUT_HEFROOT| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
            T_HEFROOT=`echo $T_HEFROOT+$T_HEFROOT_T|bc -l`
        fi
        
        T_PWROOTS_T=$(grep "cpu"         $OUT_PWROOTS| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_PWROOTS=`echo $T_PWROOTS+$T_PWROOTS_T|bc -l`
    done
    
    LINE_TAB=$IN_DEG" & "$IN_NBD
    
    T_MPSOLVE=`echo $T_MPSOLVE/$IN_NBP|bc -l`
#     TS_MPSOLVE=`echo $TS_MPSOLVE/$IN_NBP|bc -l`
    if [ $DEG -le $DEGMAXHEF ]; then
        T_HEFROOT=`echo $T_HEFROOT/$IN_NBP|bc -l`
    fi
    T_PWROOTS=`echo $T_PWROOTS/$IN_NBP|bc -l`
    
    LINE_TAB=$LINE_TAB" & `format_time $T_MPSOLVE`"
    if [ $DEG -le $DEGMAXHEF ]; then
        LINE_TAB=$LINE_TAB" & `format_time $T_HEFROOT`"
    else
        LINE_TAB=$LINE_TAB" & -"
    fi
    LINE_TAB=$LINE_TAB" & `format_time $T_PWROOTS`"
    
    LINE_TAB=$LINE_TAB"\\\\"
    echo $LINE_TAB >> $TEMPTABFILE
}

if [ $CHECKPWOUT -eq 0 ]; then

    for DEG in $DEGREES; do
            FILENAME=$REP"/"$POLNAME
            FILENAME=$FILENAME"_"$DEG
        
            stats_random $FILENAME $DEG "iso" $NBPOLS
        
            for NBD in $NBDIGITS; do
                stats_random $FILENAME $DEG $NBD $NBPOLS
            done
            echo "\\hline" >> $TEMPTABFILE
    done

    HEAD_TABLE="\begin{tabular}{|c|c||c||c||c|}"
    FIRST_LINE="   & & \texttt{MPSolve} & \texttt{HEFRoots} & \texttt{PWRoots}\\\\\\hline"
    SECOND_LINE=" $\\deg$ & goal / log10 eps & cpu time in s & cpu time in s & cpu time in s \\\\\\hline"
    THIRD_LINE="\\multicolumn{5}{|c|}{Averages on $NBPOLS random dense res pols with real coeffs }\\\\\\hline"
    TAIL_TAB="\end{tabular}"

    echo $HEAD_TABLE >> $TABLEOUT
    echo $FIRST_LINE >> $TABLEOUT
    echo $SECOND_LINE >> $TABLEOUT
    echo $THIRD_LINE >> $TABLEOUT
    cat $TEMPTABFILE >> $TABLEOUT
    echo $TAIL_TAB >> $TABLEOUT

    rm -rf $TEMPTABFILE
    
fi
