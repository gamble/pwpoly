#!/bin/bash
POLNAME="Wilkinson"

DEGREES="64 128"
NBDIGITS="10 16"
DOMAINS="C"
NBMXSOLS="-1"

source getParams.sh

source getSolvers.sh

source getFunctions.sh

TEST=""
for NBD in $NBDIGITS; do
    TEMP=`convert_log10_log2 $NBD`
    TEST=$TEST$TEMP" "
done
${ECHO} "table_${POLNAME} : degrees = ${DEGREES} nb of correct digits: ${NBDIGITS}, log2 of eps: ${TEST}"

init_rep $REP $PURGE

TEMPTABFILE="temptabfile_$POLNAME.txt"
touch $TEMPTABFILE

TABLEOUT=$POLNAME".tex"
rm -rf $TABLEOUT
touch $TABLEOUT

for DEG in $DEGREES; do
    FILENAME=$REP"/"$POLNAME
    genPol_with_deg $FILENAME $POLNAME $DEG
    FILENAME=$FILENAME"_"$DEG
    
    for DOM in $DOMAINS; do
        echo "\\n\\n\\n\\n\\n $DOM \\n\\n\\n\\n\\n"
        
        for NBM in $NBMXSOLS; do
        
            for NBD in $NBDIGITS; do
                if [ "$NBD" = "iso" ]; then
                    #ISOLATE
                    if [ "$DOM" = "C" ] && [ "$NBM" = "-1" ]; then
                        run_mpsolve_isolate $FILENAME $POLNAME $DEG "-"
                    fi
                    run_hefroots_isolate $FILENAME $POLNAME $DEG "-" $DOM $NBM
                    run_pwroots_isolate $FILENAME $POLNAME $DEG "-" $DOM $NBM
                else
                    #APPROXIMATE
                    if [ "$DOM" = "C" ] && [ "$NBM" = "-1" ]; then
                        run_mpsolve_approximate $FILENAME $POLNAME $DEG "-" $NBD
                    fi
                    run_hefroots_approximate $FILENAME $POLNAME $DEG "-" $NBD $DOM $NBM
                    run_pwroots_approximate $FILENAME $POLNAME $DEG "-" $NBD $DOM $NBM
                fi
            done
        
        done 
    done
done

stats_Wilkinson()
{
    IN_FILENAME=$1
    IN_DEG=$2
    IN_NBD=$3
    
    OUT_MPSOLVE=$IN_FILENAME".out_mps_"$IN_NBD
    OUT_HEFROOT=$IN_FILENAME".out_hef_"$IN_NBD
    OUT_PWROOTS=$IN_FILENAME".out_pw_"$IN_NBD
    
    T_MPSOLVE=$(grep "cpu"        $OUT_MPSOLVE| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
    
    T_HEFROOT=$(grep "cpu"        $OUT_HEFROOT| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
    T_PWROOTS=$(grep "cpu"        $OUT_PWROOTS| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
    
    LINE_TAB=$IN_DEG" & "$IN_NBD
    LINE_TAB=$LINE_TAB" & `format_time $T_MPSOLVE`"
    LINE_TAB=$LINE_TAB" & `format_time $T_HEFROOT`"
    LINE_TAB=$LINE_TAB" & `format_time $T_PWROOTS`"
    LINE_TAB=$LINE_TAB"\\\\"
    
    echo $LINE_TAB >> $TEMPTABFILE
}

stats_Wilkinson_domain()
{
    IN_FILENAME=$1
    IN_DEG=$2
    IN_NBD=$3
    
    LINE_TAB=$IN_DEG" & "$IN_NBD
    
    for DOM in $DOMAINS; do
    
        OUT_HEFROOT=$IN_FILENAME".out_hef_"$IN_NBD
        OUT_PWROOTS=$IN_FILENAME".out_pw_"$IN_NBD
        
        if [ "$DOM" != "C" ]; then
            TEMP=`convert_domain_str $DOM`
            OUT_HEFROOT=$OUT_HEFROOT"_"$TEMP
            OUT_PWROOTS=$OUT_PWROOTS"_"$TEMP
        fi
    
        T_HEFROOT=$(grep "cpu"        $OUT_HEFROOT| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_PWROOTS=$(grep "cpu"        $OUT_PWROOTS| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        
        LINE_TAB=$LINE_TAB" & `format_time $T_HEFROOT`"
        LINE_TAB=$LINE_TAB" & `format_time $T_PWROOTS`"
        
    done
    LINE_TAB=$LINE_TAB"\\\\"
    echo $LINE_TAB >> $TEMPTABFILE
}

stats_Wilkinson_domain_nbr()
{
    IN_FILENAME=$1
    IN_DEG=$2
    IN_NBD=$3
    IN_NBM=$4
    
    LINE_TAB=$IN_DEG" & "$IN_NBD" & "
    if [ "$IN_NBM" != "-1" ]; then
        LINE_TAB=$LINE_TAB$IN_NBM
    fi
    
    for DOM in $DOMAINS; do
    
        OUT_HEFROOT=$IN_FILENAME".out_hef_"$IN_NBD
        OUT_PWROOTS=$IN_FILENAME".out_pw_"$IN_NBD
        
        if [ "$DOM" != "C" ]; then
            TEMP=`convert_domain_str $DOM`
            OUT_HEFROOT=$OUT_HEFROOT"_"$TEMP
            OUT_PWROOTS=$OUT_PWROOTS"_"$TEMP
        fi
        
        if [ "$IN_NBM" != "-1" ]; then
            OUT_HEFROOT=$OUT_HEFROOT"_"$IN_NBM
            OUT_PWROOTS=$OUT_PWROOTS"_"$IN_NBM
        fi
    
        T_HEFROOT=$(grep "cpu"        $OUT_HEFROOT| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        T_PWROOTS=$(grep "cpu"        $OUT_PWROOTS| cut -f2 -d'u' | tr -d ' ' | tr -d '\n')
        
        LINE_TAB=$LINE_TAB" & `format_time $T_HEFROOT`"
        LINE_TAB=$LINE_TAB" & `format_time $T_PWROOTS`"
        
    done
    LINE_TAB=$LINE_TAB"\\\\"
    echo $LINE_TAB >> $TEMPTABFILE
}

if [ $CHECKPWOUT -eq 0 ]; then

    if [ "$DOMAINS" = "C" ]; then
    
        for DEG in $DEGREES; do
            FILENAME=$REP"/"$POLNAME
            FILENAME=$FILENAME"_"$DEG
            for NBD in $NBDIGITS; do
                stats_Wilkinson $FILENAME $DEG $NBD
            done
            echo "\\hline" >> $TEMPTABFILE
        done

        HEAD_TABLE="\begin{tabular}{|c|c||c||c||c|}"
        FIRST_LINE="   & & \texttt{MPSolve} & \texttt{HEFRoots} & \texttt{PWRoots}\\\\\\hline"
        SECOND_LINE=" $\\deg$  & goal/$\log_{10} \\varepsilon$ & cpu time in s & real time in s & real time in s \\\\\\hline"
        TAIL_TAB="\end{tabular}"

        echo $HEAD_TABLE >> $TABLEOUT
        echo $FIRST_LINE >> $TABLEOUT
        echo $SECOND_LINE >> $TABLEOUT
        cat $TEMPTABFILE >> $TABLEOUT
        echo $TAIL_TAB >> $TABLEOUT
        
    else
        HEAD_TABLE="\begin{tabular}{|c|c|"
        DOMAIN_LINE="   &"
        FIRST_LINE="   &"
        SECOND_LINE=" $\\deg$ & goal/$\log_{10} \\varepsilon$"
        
        if [ "$NBMXSOLS" != "-1" ]; then
            HEAD_TABLE=$HEAD_TABLE"c|"
            DOMAIN_LINE=$DOMAIN_LINE" &"
            FIRST_LINE=$FIRST_LINE" &"
            SECOND_LINE=$SECOND_LINE" & n"
        fi
        
        NBDOMAINS=0
        for DOM in $DOMAINS; do
        
            HEAD_TABLE=$HEAD_TABLE"|c|c|"
            DOMAIN_LINE=$DOMAIN_LINE" &\\multicolumn{2}{|c|}{[$DOM]}"
            FIRST_LINE=$FIRST_LINE" & \texttt{HEFRoots} & \texttt{PWRoots}"
            SECOND_LINE=$SECOND_LINE" & t in s & t in s"
            NBDOMAINS=`echo "$NBDOMAINS+1" | bc`
            
        done
        
        for DEG in $DEGREES; do
            FILENAME=$REP"/"$POLNAME
            FILENAME=$FILENAME"_"$DEG
            for NBD in $NBDIGITS; do
                if [ "$NBMXSOLS" = "-1" ]; then
                    stats_Wilkinson_domain $FILENAME $DEG $NBD
                else
                    for NBM in $NBMXSOLS; do
                        stats_Wilkinson_domain_nbr $FILENAME $DEG $NBD $NBM
                    done
                fi
            done
            echo "\\hline" >> $TEMPTABFILE
        done
        
        HEAD_TABLE=$HEAD_TABLE"}"
        DOMAIN_LINE=$DOMAIN_LINE"\\\\\\hline"
        FIRST_LINE=$FIRST_LINE"\\\\\\hline"
        SECOND_LINE=$SECOND_LINE"\\\\\\hline"
        NBCOLS=`echo "3 + 2*$NBDOMAINS" | bc`
        TAIL_TAB="\end{tabular}"
        
        echo $HEAD_TABLE >> $TABLEOUT
        echo $DOMAIN_LINE >> $TABLEOUT
        echo $FIRST_LINE >> $TABLEOUT
        echo $SECOND_LINE >> $TABLEOUT
        cat $TEMPTABFILE >> $TABLEOUT
        echo $TAIL_TAB >> $TABLEOUT
    fi

    rm -rf $TEMPTABFILE

fi
