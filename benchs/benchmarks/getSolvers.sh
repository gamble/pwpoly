########################mpsolve
MPSOLVE_APPROXIMATE_CALL="mpsolve -j1 -as -Ga"
MPSOLVE_ISOLATE_CALL="mpsolve -j1 -as -Gi"

########################hefroots
HEFROOTS_PATH="/work/WithMaple/p4/hefroots_maple/internal/kernelext/hefroots/src/"
HEFROOTS_CALL=$HEFROOTS_PATH"bin/hefroots"
HEFROOTS_ISOLATE_OPTS="-v 1 -c 0 -g isolate"
HEFROOTS_APPROXIMATE_OPTS="-v 1 -c 0 -g approximate -e"

########################pwroots
PWPOLY_PATH="../../"
PWROOTS_CALL=$PWPOLY_PATH"bin/pwroots"
PWROOTS_ISOLATE_OPTS="-v 1 -g i -m 10 -s 63"
PWROOTS_APPROXIMATE_OPTS="-v 1 -g a -m 10 -s 63 -e"

########################genPolFile
GENPOLFILE_CALL=$PWPOLY_PATH"bin/genPolFile"
GENPOLFILEHEF_CALL=$HEFROOTS_PATH"bin/genPolFile"

########################maple
MAPLE_CALL="maple"
MAPLE_RESFILE=resultant.mpl
MAPLE_CHAFILE=characPol.mpl

########################time
TIME_CALL="/usr/bin/time -f"
TIME_FORM="real\t%e\ncpu\t%U"
