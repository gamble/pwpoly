#deg:=4:
#bitsize:=8:
#fileout:=cat("isolate/randomDenseRes_", deg, "_", bitsize):
#nbpols:=2

for k from 1 to nbpols-1 do
  f1:=randpoly([x,y], dense, coeffs=rand(-2^bitsize..2^bitsize), degree=deg):
  f2:=randpoly([x,y], dense, coeffs=rand(-2^bitsize..2^bitsize), degree=deg):
end do:

filenamePW:=cat(fileout, ".pw" ):
filenameHE:=cat(fileout, ".hef" ):
filenameMP:=cat(fileout, ".mps" ):
  
f1:=randpoly([x,y], dense, coeffs=rand(-2^bitsize..2^bitsize), degree=deg):
f2:=randpoly([x,y], dense, coeffs=rand(-2^bitsize..2^bitsize), degree=deg):
  
r:=resultant(f1, f2, y):
  
fdPW:=fopen(filenamePW, WRITE):
fprintf(fdPW, "Exact Real\n"):
fprintf(fdPW, "%a  ", degree(r)+1):
for i from 0 to degree(r) do
  fprintf(fdPW, "%a ", coeff(r,x,i)):  
end do:
fclose(fdPW):

fdHE:=fopen(filenameHE, WRITE):
fprintf(fdHE, "%a  ", degree(r)+1):
for i from 0 to degree(r) do
  fprintf(fdHE, "%a ", coeff(r,x,i)):  
end do:
fclose(fdHE):

fdMP:=fopen(filenameMP, WRITE):
fprintf(fdMP, "Dense;\nMonomial;\nReal;\nDegree=%a;\nRational;\n\n",degree(r)):
for i from 0 to degree(r) do
  fprintf(fdMP, "%a\n ", coeff(r,x,i)):
end do:
fclose(fdMP):
  

quit():
