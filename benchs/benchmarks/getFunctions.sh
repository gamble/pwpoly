init_rep()
{
    LREP=$1
    LPURGE=$2
    if [ -d "$LREP" ]; then
        if [ $LPURGE -eq 1 ]; then
            rm -rf $LREP
            mkdir $LREP
        fi
    else
        mkdir $LREP
    fi
}

convert_log10_log2()
{
    IN=$1
    OUT1=`echo "$IN*l(10)/l(2)" | bc -l`
    OUT2=`echo "scale=0; $OUT1/1" | bc -l`
    INT=`echo "$OUT1==$OUT2" | bc -l`
    #echo $INT
    if [ $INT -eq 0 ]; then
        OUT2=`echo "$OUT2+1" | bc -l`
    fi
    echo $OUT2
}

format_time()
{
    TIME1=$1
    TIME2=$1
    TIME1=`echo $TIME1 | cut -f1 -d'.'`
    TIME2=`echo $TIME2 | cut -f2 -d'.'`
    STIME1=${#TIME1}
    STIME1=$(( 3 - $STIME1 ))
    STIME2=$(( 3 < $STIME1 ? 3 : $STIME1 ))
    STIME2=$(( 0 > $STIME2 ? 0 : $STIME2 ))
    if [ $STIME2 -eq 0 ]; then
        echo $TIME1
    else
        TIME2=`echo $TIME2 | cut -c-$( echo $STIME2)`
        echo $TIME1"."$TIME2
    fi
}

BGREEN="\e[1;32m"
BRED="\e[1;31m"
NORMAL="\e[0m"
BYELLOW="\e[1;33m"

print_color_nbroots()
{
    ARG1=$1
    ARG2=$2
    if [ $ARG1 -eq $ARG2 ]; then
        $ECHO -n $BGREEN$ARG1$NORMAL
    else
        $ECHO -n $BRED$ARG1$NORMAL
    fi
}

print_color_check()
{
    LCHECK=$1
    LCHECKOK=$2
    if [ $LCHECK -eq 1 ]; then
        if [ $LCHECKOK -eq 1 ]; then
            $ECHO -n $BGREEN $LCHECKOK $NORMAL
        else
            $ECHO -n $BRED $LCHECKOK $NORMAL
        fi
    else
        $ECHO -n $BYELLOW "NOT CHECKED" $NORMAL
    fi
}

convert_domain_str()
{
    IN=$1
    OUT1=`echo $IN | cut -f1 -d ","`
    if [[ $OUT1 == */* ]]; then
        OUT11=`echo $OUT1 | cut -f1 -d "/"`
        OUT12=`echo $OUT1 | cut -f2 -d "/"`
        OUT1="$OUT11)$OUT12"
    fi
    OUT2=`echo $IN | cut -f2 -d ","`
    if [[ $OUT2 == */* ]]; then
        OUT21=`echo $OUT2 | cut -f1 -d "/"`
        OUT22=`echo $OUT2 | cut -f2 -d "/"`
        OUT2="$OUT21)$OUT22"
    fi
    OUT3=`echo $IN | cut -f3 -d ","`
    if [[ $OUT3 == */* ]]; then
        OUT31=`echo $OUT3 | cut -f1 -d "/"`
        OUT32=`echo $OUT3 | cut -f2 -d "/"`
        OUT3="$OUT31)$OUT32"
    fi
    OUT4=`echo $IN | cut -f4 -d ","`
    if [[ $OUT4 == */* ]]; then
        OUT41=`echo $OUT4 | cut -f1 -d "/"`
        OUT42=`echo $OUT4 | cut -f2 -d "/"`
        OUT4="$OUT41)$OUT42"
    fi
    echo "$OUT1,$OUT2,$OUT3,$OUT4"
}

genPol_with_deg()
{
    LNAME=$1
    LPOLNAME=$2
    LDEG=$3
    
    NAME_IN=$LNAME"_"$LDEG
    
    if [ ! -e $NAME_IN".pw" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating file $NAME_IN.pw"
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 1 -o $NAME_IN".pw"
            else
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 1 -o $NAME_IN".pw" >/dev/null
            fi
    fi
    
    if [ $CHECKPWOUT -eq 0 ]; then
    
        if [ ! -e $NAME_IN".mps" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating file $NAME_IN.mps"
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 2 -o $NAME_IN".mps"
            else
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 2 -o $NAME_IN".mps" >/dev/null
            fi
        fi
    
        if [ ! -e $NAME_IN".hef" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating file $NAME_IN.hef"
                $GENPOLFILEHEF_CALL $LPOLNAME $LDEG -f 1 -o $NAME_IN".hef"
            else
                $GENPOLFILEHEF_CALL $LPOLNAME $LDEG -f 1 -o $NAME_IN".hef" >/dev/null
            fi
        fi
    
    fi
}

genPol_with_deg_bs(){

    LNAME=$1
    LPOLNAME=$2
    LDEG=$3
    LBIT=$4
    
    NAME_IN=$LNAME"_"$LDEG"_"$LBIT
    
    if [ ! -e $NAME_IN".pw" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating file $NAME_IN.pw"
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 1 -b $LBIT -o $NAME_IN".pw"
            else
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 1 -b $LBIT -o $NAME_IN".pw" >/dev/null
            fi
    fi
    
    if [ $CHECKPWOUT -eq 0 ]; then
    
        if [ ! -e $NAME_IN".mps" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating file $NAME_IN.mps"
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 2 -b $LBIT -o $NAME_IN".mps"
            else
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 2 -b $LBIT -o $NAME_IN".mps" >/dev/null
            fi
        fi
    
        if [ ! -e $NAME_IN".hef" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating file $NAME_IN.hef"
                $GENPOLFILEHEF_CALL $LPOLNAME $LDEG -f 1 -b $LBIT -o $NAME_IN".hef"
            else
                $GENPOLFILEHEF_CALL $LPOLNAME $LDEG -f 1 -b $LBIT -o $NAME_IN".hef" >/dev/null
            fi
        fi
    
    fi
}

genRandomHyp_with_deg_bs_nbpol(){

    LNAME=$1
    LPOLNAME=$2
    LDEG=$3
    LBIT=$4
    LNBP=$5
    
    NAME_IN=$LNAME"_"$LDEG"_"$LBIT"_"$LNBP
    
    if [ ! -e $NAME_IN".pw" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating file $NAME_IN.pw"
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 1 -b $LBIT -s $LNBP -o $NAME_IN".pw"
            else
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 1 -b $LBIT -s $LNBP -o $NAME_IN".pw" >/dev/null
            fi
    fi
    
    if [ $CHECKPWOUT -eq 0 ]; then
    
        if [ ! -e $NAME_IN".mps" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating file $NAME_IN.mps"
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 2 -b $LBIT -s $LNBP -o $NAME_IN".mps"
            else
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 2 -b $LBIT -s $LNBP -o $NAME_IN".mps" >/dev/null
            fi
        fi
    
        if [ ! -e $NAME_IN".hef" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating file $NAME_IN.hef"
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 16 -b $LBIT -s $LNBP -o $NAME_IN".hef"
            else
                $GENPOLFILE_CALL $LPOLNAME $LDEG -f 16 -b $LBIT -s $LNBP -o $NAME_IN".hef" >/dev/null
            fi
        fi
    
    fi
}

genRandomRes_with_deg_bs_nbpol(){

    LNAME=$1
    LPOLNAME=$2
    LDEG=$3
    LBIT=$4
    LNBP=$5
    
    NAME_IN=$LNAME"_"$LDEG"_"$LBIT"_"$LNBP
    
    if [ ! -e $NAME_IN".pw" ] || [ ! -e $NAME_IN".mps" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating files $NAME_IN.pw and $NAME_IN.mps"
#                 $GENPOLFILE_CALL $LPOLNAME $LDEG -f 1 -b $LBIT -s $LNBP -o $NAME_IN".pw"
#             else
#                 $GENPOLFILE_CALL $LPOLNAME $LDEG -f 1 -b $LBIT -s $LNBP -o $NAME_IN".pw" >/dev/null
            fi
            $MAPLE_CALL "-c deg:="$LDEG": -c bitsize:="$LBIT": -c nbpols:="$LNBP": -c fileout:="$NAME_IN": -i "$MAPLE_RESFILE
    fi
    
    NAME_INHEF=$NAME_IN".hef"
    if [ -e $NAME_INHEF ]; then
        if [ $LDEG -gt $DEGMAXHEF ]; then
            rm -rf $NAME_INHEF
        fi
    fi

}

genRandomCha_with_deg_nbpol(){

    LNAME=$1
    LPOLNAME=$2
    LDEG=$3
    LNBP=$4
    
    NAME_IN=$LNAME"_"$LDEG"_"$LNBP
    
    if [ ! -e $NAME_IN".pw" ] || [ ! -e $NAME_IN".mps" ]; then
            if [ $VERBOSE -eq 1 ]; then
                $ECHO "Generating files $NAME_IN.pw and $NAME_IN.mps"
#                 $GENPOLFILE_CALL $LPOLNAME $LDEG -f 1 -b $LBIT -s $LNBP -o $NAME_IN".pw"
#             else
#                 $GENPOLFILE_CALL $LPOLNAME $LDEG -f 1 -b $LBIT -s $LNBP -o $NAME_IN".pw" >/dev/null
            fi
            $MAPLE_CALL "-c deg:="$LDEG": -c nbpols:="$LNBP": -c fileout:="$NAME_IN": -i "$MAPLE_CHAFILE
    fi
    
    NAME_INHEF=$NAME_IN".hef"
    if [ -e $NAME_INHEF ]; then
        if [ $LDEG -gt $DEGMAXHEF ]; then
            rm -rf $NAME_INHEF
        fi
    fi

}

run_mpsolve_approximate()
{
    IN_NAME=$1
    IN_POLNAME=$2
    IN_DEG=$3
    IN_BIT=$4
    IN_NBD=$5
    NAME_IN=$IN_NAME".mps"
    NAME_OUT=$IN_NAME".out_mps_"$IN_NBD
    
    if [ $CHECKPWOUT -eq 0 ]; then
    
        if [ $PURGEMPSOLVE -eq 1 ]; then
            rm -f $NAME_OUT
        fi
    
        if [ ! -e $NAME_OUT ]; then
            $ECHO "Approximating roots ($IN_NBD correct digits after the .) with MPSOLVE SECSOLVE output in $NAME_OUT"
            MPSOLVE_ARGS="-o $NBD $NAME_IN"
#             $ECHO "call: $MPSOLVE_APPROXIMATE_CALL $MPSOLVE_ARGS"
#             (/usr/bin/time -f "real\t%e\ncpu\t%U" $MPSOLVE_APPROXIMATE_CALL $MPSOLVE_ARGS > $NAME_OUT) &>> $NAME_OUT
            ($TIME_CALL $TIME_FORM $MPSOLVE_APPROXIMATE_CALL $MPSOLVE_ARGS > $NAME_OUT) &>> $NAME_OUT
        fi
        TIME=$(grep "real" $NAME_OUT| cut -f2 -d'l' | tr -d ' ' | tr -d '\t')
        TIMESEQ=$(grep "cpu" $NAME_OUT| cut -f2 -d'u' | tr -d ' ' | tr -d '\t')
        $ECHO "MPSOLVE SEC    APP $IN_POLNAME $IN_DEG $IN_BIT $IN_NBD,    real time: $TIME, (cpu time $TIMESEQ)"
    
    fi
}

run_mpsolve_isolate()
{
    IN_NAME=$1
    IN_POLNAME=$2
    IN_DEG=$3
    IN_BIT=$4
    NAME_IN=$IN_NAME".mps"
    NAME_OUT=$IN_NAME".out_mps_iso"
    
    if [ $CHECKPWOUT -eq 0 ]; then
        
        if [ $PURGEMPSOLVE -eq 1 ]; then
            rm -f $NAME_OUT
        fi
    
        if [ ! -e $NAME_OUT ]; then
            $ECHO "Isolating roots with MPSOLVE SECSOLVE output in $NAME_OUT"
            MPSOLVE_ARGS="$NAME_IN"
#             $ECHO "call: $MPSOLVE_ISOLATE_CALL $MPSOLVE_ARGS"
            (/usr/bin/time -f "real\t%e\ncpu\t%U" $MPSOLVE_ISOLATE_CALL $MPSOLVE_ARGS > $NAME_OUT) &>> $NAME_OUT
        fi
        TIME=$(grep "real" $NAME_OUT| cut -f2 -d'l' | tr -d ' ' | tr -d '\t')
        TIMESEQ=$(grep "cpu" $NAME_OUT| cut -f2 -d'u' | tr -d ' ' | tr -d '\t')
        $ECHO "MPSOLVE SEC    ISO $IN_POLNAME $IN_DEG $IN_BIT ,      real time: $TIME, (cpu time $TIMESEQ)"
    
    fi
}

run_pwroots_approximate()
{
    IN_NAME=$1
    IN_POLNAME=$2
    IN_DEG=$3
    IN_BIT=$4
    IN_NBD=$5
    IN_DOM=$6
    IN_NBR=$7
    NAME_IN=$IN_NAME".pw"
    NAME_OUT=$IN_NAME".out_pw_"$IN_NBD
    
    if [ -z "$IN_DOM" ]; then
        IN_DOM="C"
    fi
    
    if [ -z "$IN_NBR" ]; then
        IN_NBR="-1"
    fi
    
    if [ "$IN_DOM" != "C" ]; then
        TEMP=`convert_domain_str $IN_DOM`
        NAME_OUT=$NAME_OUT"_"$TEMP
    fi
    
    if [ "$IN_NBR" != "-1" ]; then
        NAME_OUT=$NAME_OUT"_"$IN_NBR
    fi
    
    LCHECKARG=""
    if [ $CHECKPWOUT -eq 1 ]; then
        NAME_OUT=$NAME_OUT"_check"
        rm -f $NAME_OUT
        LCHECKARG="-c"
    fi
    
    L2NBD=`convert_log10_log2 $NBD`
    L2NBD="-$L2NBD"
#     echo $NBD", "$L2NBD
    if [ $PURGEPWROOTS -eq 1 ]; then
        rm -f $NAME_OUT
    fi
    
    if [ ! -e $NAME_OUT ]; then
            $ECHO "Approximating $IN_NBR roots ($IN_NBD correct digits after the .) in $IN_DOM with PWROOTS output in $NAME_OUT"
            CALL="$PWROOTS_CALL $NAME_IN $PWROOTS_APPROXIMATE_OPTS $L2NBD $LCHECKARG -d $IN_DOM -n $IN_NBR"
#             $ECHO "call: ${CALL}"
            (/usr/bin/time -f "real\t%e\ncpu\t%U" $CALL > $NAME_OUT) &>> $NAME_OUT
    fi
    TIME=$(grep "real" $NAME_OUT| cut -f2 -d'l' | tr -d ' ' | tr -d '\t')
    TIMESEQ=$(grep "cpu" $NAME_OUT| cut -f2 -d'u' | tr -d ' ' | tr -d '\t')
    NBROOTS=$(grep "total number of roots" $NAME_OUT | cut -f2 -d':' | tr -d ' ')
    CHECKOK=$(grep "check output"          $NAME_OUT | cut -f2 -d':' | tr -d ' ')
    $ECHO -n "PWROOTS        APP $IN_POLNAME $IN_DEG $IN_BIT $IN_NBD $IN_NBR,    cpu time $TIMESEQ"
    $ECHO -n " number of roots: "
    print_color_nbroots $NBROOTS $IN_DEG
    if [ $CHECKPWOUT -eq 1 ]; then
        $ECHO -n " check OK: "
        print_color_check $CHECKPWOUT $CHECKOK
    fi
    $ECHO " "
}

run_pwroots_isolate()
{
    IN_NAME=$1
    IN_POLNAME=$2
    IN_DEG=$3
    IN_BIT=$4
    IN_DOM=$5
    IN_NBR=$6
    NAME_IN=$IN_NAME".pw"
    NAME_OUT=$IN_NAME".out_pw_iso"
    
    if [ -z "$IN_DOM" ]; then
        IN_DOM="C"
    fi
    
    if [ -z "$IN_NBR" ]; then
        IN_NBR="-1"
    fi
    
    if [ "$IN_DOM" != "C" ]; then
        TEMP=`convert_domain_str $IN_DOM`
        NAME_OUT=$NAME_OUT"_"$TEMP
    fi
    
    if [ "$IN_NBR" != "-1" ]; then
        NAME_OUT=$NAME_OUT"_"$IN_NBR
    fi
    
    LCHECKARG=""
    if [ $CHECKPWOUT -eq 1 ]; then
        NAME_OUT=$NAME_OUT"_check"
        rm -f $NAME_OUT
        LCHECKARG="-c"
    fi
    
    if [ $PURGEPWROOTS -eq 1 ]; then
        rm -f $NAME_OUT
    fi
    
    if [ ! -e $NAME_OUT ]; then
            $ECHO "Isolating $IN_NBR roots in $IN_DOM with PWROOTS output in $NAME_OUT"
            CALL="$PWROOTS_CALL $NAME_IN $PWROOTS_ISOLATE_OPTS $LCHECKARG -d $IN_DOM -n $IN_NBR"
#             $ECHO "call: ${CALL}"
            (/usr/bin/time -f "real\t%e\ncpu\t%U" $CALL > $NAME_OUT) &>> $NAME_OUT
    fi
    TIME=$(grep "real" $NAME_OUT| cut -f2 -d'l' | tr -d ' ' | tr -d '\t')
    TIMESEQ=$(grep "cpu" $NAME_OUT| cut -f2 -d'u' | tr -d ' ' | tr -d '\t')
    NBROOTS=$(grep "total number of roots" $NAME_OUT | cut -f2 -d':' | tr -d ' ')
    CHECKOK=$(grep "check output"          $NAME_OUT | cut -f2 -d':' | tr -d ' ')
    $ECHO -n "PWROOTS        ISO $IN_POLNAME $IN_DEG $IN_BIT $IN_NBR,      cpu time $TIMESEQ"
    $ECHO -n " number of roots: "
    print_color_nbroots $NBROOTS $IN_DEG
    if [ $CHECKPWOUT -eq 1 ]; then
        $ECHO -n " check OK: "
        print_color_check $CHECKPWOUT $CHECKOK
    fi
    $ECHO " "
}

run_hefroots_approximate()
{
    IN_NAME=$1
    IN_POLNAME=$2
    IN_DEG=$3
    IN_BIT=$4
    IN_NBD=$5
    IN_DOM=$6
    IN_NBR=$7
    NAME_IN=$IN_NAME".hef"
    NAME_OUT=$IN_NAME".out_hef_"$IN_NBD
    
    if [ -z "$IN_DOM" ]; then
        IN_DOM="C"
    fi
    
    if [ -z "$IN_NBR" ]; then
        IN_NBR="-1"
    fi
    
    if [ "$IN_DOM" != "C" ]; then
        TEMP=`convert_domain_str $IN_DOM`
        NAME_OUT=$NAME_OUT"_"$TEMP
    fi
    
    if [ "$IN_NBR" != "-1" ]; then
        NAME_OUT=$NAME_OUT"_"$IN_NBR
    fi
    
    if [ $CHECKPWOUT -eq 0 ]; then
    
        L2NBD=`convert_log10_log2 $NBD`
        L2NBD="-$L2NBD"
        if [ $PURGEHEFROOTS -eq 1 ]; then
            rm -f $NAME_OUT
        fi
        
        if [ ! -e $NAME_OUT ]; then
            $ECHO "Approximating $IN_NBR roots ($IN_NBD correct digits after the .) in $IN_DOM with HEFROOTS output in $NAME_OUT"
            CALL="$HEFROOTS_CALL $NAME_IN $HEFROOTS_APPROXIMATE_OPTS $L2NBD -d $IN_DOM -n $IN_NBR"
#             $ECHO "call: ${CALL}"
            (/usr/bin/time -f "real t\t%e\ncpu\t%U" $CALL > $NAME_OUT) &>> $NAME_OUT
        fi
        TIME=$(grep "real t" $NAME_OUT| cut -f2 -d'l' | tr -d ' ' | tr -d '\t')
        TIMESEQ=$(grep "cpu" $NAME_OUT| cut -f2 -d'u' | tr -d ' ' | tr -d '\t')
        $ECHO "HEFROOTS       APP $IN_POLNAME $IN_DEG $IN_BIT $IN_NBD $IN_NBR,    real time: $TIME, (cpu time $TIMESEQ)"
    
    fi
}

run_hefroots_isolate()
{
    IN_NAME=$1
    IN_POLNAME=$2
    IN_DEG=$3
    IN_BIT=$4
    IN_DOM=$5
    IN_NBR=$6
    NAME_IN=$IN_NAME".hef"
    NAME_OUT=$IN_NAME".out_hef_iso"
    
    if [ -z "$IN_DOM" ]; then
        IN_DOM="C"
    fi
    
    if [ -z "$IN_NBR" ]; then
        IN_NBR="-1"
    fi
    
    if [ "$IN_DOM" != "C" ]; then
        TEMP=`convert_domain_str $IN_DOM`
        NAME_OUT=$NAME_OUT"_"$TEMP
    fi
    
    if [ "$IN_NBR" != "-1" ]; then
        NAME_OUT=$NAME_OUT"_"$IN_NBR
    fi
    
    if [ $CHECKPWOUT -eq 0 ]; then
    
        if [ $PURGEHEFROOTS -eq 1 ]; then
            rm -f $NAME_OUT
        fi
        
        if [ ! -e $NAME_OUT ]; then
            $ECHO "Isolating $IN_NBR roots in $IN_DOM with HEFROOTS output in $NAME_OUT"
            CALL="$HEFROOTS_CALL $NAME_IN $HEFROOTS_ISOLATE_OPTS -d $IN_DOM -n $IN_NBR"
#             $ECHO "call: ${CALL}"
            (/usr/bin/time -f "real t\t%e\ncpu \t%U" $CALL > $NAME_OUT) &>> $NAME_OUT
        fi
        TIME=$(grep "real t" $NAME_OUT| cut -f2 -d't' | tr -d ' ' | tr -d '\t')
        TIMESEQ=$(grep "cpu" $NAME_OUT| cut -f2 -d'u' | tr -d ' ' | tr -d '\t')
        $ECHO "HEFROOTS       ISO $IN_POLNAME $IN_DEG $IN_BIT $IN_NBR,      real time: $TIME, (cpu time $TIMESEQ)"
    
    fi
}
