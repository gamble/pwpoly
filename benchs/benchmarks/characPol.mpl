#deg:=4:
#bitsize:=8:
#fileout:=cat("isolate/randomDenseRes_", deg, "_", bitsize):
#nbpols:=2
with(LinearAlgebra):

for k from 1 to nbpols-1 do
  A:=RandomMatrix(deg,deg, generator=rand(0..1)):
end do:

filenamePW:=cat(fileout, ".pw" ):
filenameHE:=cat(fileout, ".hef" ):
filenameMP:=cat(fileout, ".mps" ):

A:=RandomMatrix(deg,deg, generator=rand(0..1)):
P:=CharacteristicPolynomial(A,x):
  
fdPW:=fopen(filenamePW, WRITE):
fprintf(fdPW, "Exact Real\n"):
fprintf(fdPW, "%a  ", degree(P)+1):
for i from 0 to degree(P) do
  fprintf(fdPW, "%a ", coeff(P,x,i)):  
end do:
fclose(fdPW):

fdHE:=fopen(filenameHE, WRITE):
fprintf(fdHE, "%a  ", degree(P)+1):
for i from 0 to degree(P) do
  fprintf(fdHE, "%a ", coeff(P,x,i)):  
end do:
fclose(fdHE):

fdMP:=fopen(filenameMP, WRITE):
fprintf(fdMP, "Dense;\nMonomial;\nReal;\nDegree=%a;\nRational;\n\n",degree(r)):
for i from 0 to degree(P) do
  fprintf(fdMP, "%a\n ", coeff(P,x,i)):
end do:
fclose(fdMP):
  

quit():
