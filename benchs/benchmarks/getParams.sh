#!/bin/bash

usage()
{
   echo "Usage: ./table$POLNAME <options> <args>"
}

##########################get arguments
while [ "$1" != "" ]; do
   PARAM=`echo $1 | sed 's/=.*//'`
   VALUE=`echo $1 | sed 's/[^=]*//; s/=//'`
   case "$PARAM" in
      -h|--help)
         usage
         exit 0
         ;;
      --verbose)
        VERBOSE=1
        ;;
      --degrees)
        DEGREES=$VALUE
        ;;
      --nbpols)
        NBPOLS=$VALUE
        ;;
      --bitsizes)
        BITSIZES=$VALUE
        ;;
      --nbCorrectDigits)
        NBDIGITS=$VALUE
        ;;
      --epsilonMPSOLVE)
        EPSILONMPSOLVE=$VALUE
        ;;
      --epsilonHEFROOTS)
        EPSILONHEFROOTS=$VALUE
        ;;
      --epsilonPWROOTS)
        EPSILONPWROOTS=$VALUE
        ;;
      --purge)
        PURGE=1
        ;;
      --purgeMPSOLVE)
        PURGEMPSOLVE=1
        ;;
      --purgeHEFROOTS)
        PURGEHEFROOTS=1
        ;;
      --purgePWROOTS)
        PURGEPWROOTS=1
        ;;
      --purgeMAPLEHEFROOTSDOMAIN)
        PURGEMAPLEHEFROOTSDOMAIN=1
        ;;
      --rep)
        REP="$VALUE"
        ;;
      --checkpw)
        CHECKPWOUT=1
        ;;
      --domains)
        DOMAINS="$VALUE"
        ;;
      --nbMaxsols)
        NBMXSOLS=$VALUE
        ;;
      *)
        usage
        exit 1
        ;;
    esac
    shift
done

#default values
if [ -z "$VERBOSE" ]; then
   VERBOSE=0
fi
ECHO=""
if [ $VERBOSE -eq 0 ]; then
     ECHO="echo -e \c "
else
     ECHO="echo -e "
fi

if [ -z "$DEGREES" ]; then
   DEGREES="128 256"
fi

if [ -z "$BITSIZES" ]; then
   BITSIZES="8 16"
fi

if [ -z "$NBPOLS" ]; then
    NBPOLS=4
fi

if [ -z "$NBDIGITS" ]; then
   NBDIGITS="10 16"
fi

if [ -z "$EPSILONMPSOLVE" ]; then
   EPSILONMPSOLVE="16"
fi

if [ -z "$EPSILONHEFROOTS" ]; then
   EPSILONHEFROOTS="-54"
fi

if [ -z "$EPSILONPWROOTS" ]; then
   EPSILONPWROOTS="-54"
fi

if [ -z "$PURGE" ]; then
   PURGE=0
fi

if [ -z "$PURGEMPSOLVE" ]; then
   PURGEMPSOLVE=0
fi

if [ -z "$PURGEHEFROOTS" ]; then
   PURGEHEFROOTS=0
fi

if [ -z "$PURGEPWROOTS" ]; then
   PURGEPWROOTS=0
fi

if [ -z "$REP" ]; then
   REP="table_$POLNAME"
fi


if [ -z "$CHECKPWOUT" ]; then
   CHECKPWOUT=0
fi

if [ -z "$DOMAINS" ]; then
   DOMAINS="C"
fi

if [ -z "$NBMXSOLS" ]; then
   NBMXSOLS="-1"
fi
