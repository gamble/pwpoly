#include "pw_base.h"
#include "common/pw_common.h"
#include "pw_roots.h"

int main(int argc, char* argv[]) {
    
    slong log2eps;
    root_t r;
    root_init(r);
    root_set_d_d_d_int( r,  1, 1, .0, 1 );
    log2eps = -2;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r, log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r, log2eps));
    
    root_set_d_d_d_int( r,  1, 1, .5, 1 );
    log2eps = 0;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r, log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r, log2eps));
//     mag_print(arb_radref( acb_realref(root_enclosureref(r)) )); printf("\n");
//     printf(" %d, %d\n", mag_cmp_2exp_si( arb_radref( acb_realref(root_enclosureref(r)) ), log2eps-1 ) <=0,
//                         mag_cmp_2exp_si( arb_radref( acb_imagref(root_enclosureref(r)) ), log2eps-1 ) <=0 );
    root_set_d_d_d_int( r,  1, 1, .4, 1 );
    log2eps = 0;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r, log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r, log2eps)); 
    
    root_set_d_d_d_int( r,  1, 1, 1, 1 );
    log2eps = -2;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r, log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r, log2eps));
    
    root_set_d_d_d_int( r,  1, 1, 2, 1 );
    log2eps = -2;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r, log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r, log2eps));
    
    root_set_d_d_d_int( r,  1, 1, 4, 1 );
    log2eps = -2;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r, log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r, log2eps));
    
    root_set_d_d_d_int( r,  1, 1, .25, 1 );
    log2eps = -2;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r, log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r, log2eps));
    
    root_set_d_d_d_int( r,  .5, 1, .25, 1 );
    log2eps = -2;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r,log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r,log2eps));
    
    root_set_d_d_d_int( r,  .5, 0, .25, 1 );
    log2eps = -2;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r,log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r,log2eps));
    
    root_set_d_d_d_int( r,  0, 0, .25, 1 );
    log2eps = -2;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r, log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r, log2eps));
    
    root_set_d_d_d_int( r,  0, 0, 0, 1 );
    log2eps = -2;
    root_printd( r, 10); printf("\n");
    printf(" acb relative error in bits: %ld\n", acb_rel_error_bits(root_enclosureref(r)));
    printf(" relative width less than 2^(%ld): %d\n", log2eps, root_relative_width_less_eps(r, log2eps));
    printf(" absolute width less than 2^(%ld): %d\n", log2eps, root_absolute_width_less_eps(r, log2eps));
    root_clear(r);
    
    printf("\n");
    
    slong initsize = 7;
    root_ptr v = root_vec_init ( initsize );
    
    root_set_d_d_d_int( v+0,  1,  1, 0.2, 1 );
    root_set_d_d_d_int( v+1,  1, -1, 0.2, 0 );
    root_set_d_d_d_int( v+2, -1, -1, 0.1, 1 );
    
    root_set_d_d_d_int( v+3,     1,     1, 0.2, 1 );
    root_set_d_d_d_int( v+4,  1.01, -1.01, 0.3, 0 );
    root_set_d_d_d_int( v+5, -1.01, -1.01, 0.2, 1 );
    
    root_set_d_d_d_int( v+6,     1,     1, 0.1, 1 );
    
    printf("v before merge overlaps: \n");
    printf("---size: %ld, elements:\n", initsize );
    for (slong i=0; i<initsize; i++) {
        root_printd( v+i, 10); printf("\n");
    }
    
    printf("\n\n");
    
    slong newsize = _pw_roots_merge_overlaps(v, initsize, PWPOLY_DEFAULT_PREC);
    
    printf("v after merge overlaps: \n");
    printf("---size: %ld, elements:\n", newsize );
    for (slong i=0; i<newsize; i++) {
        root_printd( v+i, 10); printf("\n");
    }
    
    printf("\n\n");
    
    root_vec_clear(v, initsize);
    flint_cleanup();
    
    return 0;
}
