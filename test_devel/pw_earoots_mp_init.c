#include "pw_base.h"
#include "common/pw_common.h"
#include "common/pw_gnuplot.h"
// #include "solving/pw_solving.h"
#include "solvers/pw_EA.h"
#include <time.h>

acb_poly_t pacb;
fmpq_poly_t pfmpq_re;
fmpq_poly_t pfmpq_im;
slong length;
int verbosity, m, increase, check, log2eps, goal, solver, roundInput, goalNbRoots;
fmpq_t b; /* not used */
domain_t dom;

void init_global_variables(){
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    length = 0;
    verbosity = 1;
    m = 500;
    increase = 1;
    check = 0;
    log2eps = -53;
    goal = PW_GOAL_ISOLATE;
    solver = 2;
    roundInput = 0;
    fmpq_init(b);
    domain_init(dom); /* by default set to C */
    goalNbRoots = -1; /* find all the roots */
}

void clear_global_variables(){
    fmpq_clear(b);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(pfmpq_re);
    acb_poly_clear(pacb);
    acb_poly_clear(pacb);
}

int main(int argc, char* argv[]) {
    
    int level = 1;
    
    if (argc<2) {
//         printf("Usage: %s filename\n", argv[0]);
        pwroots_fprint_options( stdout, argc, argv );
        exit(0);
    }
    
    init_global_variables();
    parseInputArgs( argc, argv, &verbosity, dom, &goalNbRoots, &m, &increase, &check, &log2eps, &goal, &solver, &roundInput, b );
    
//     slong prec = PWPOLY_DEFAULT_PREC;
    
    if (verbosity>=level) {
        printf("roundInput: %d\n", roundInput);
        printf("log2eps   : %d\n", log2eps);
        printf("goal     : "); pwroots_fprint_goal(stdout, goal); printf("\n");
        printf("nb max itts: %d\n", m);
        printf("check     : %d\n", check);
        printf("verbosity : %d\n", verbosity);
        
//         int testIncl = 0;
//         PW_EA_SET_INCLUSION(testIncl,PW_NATURAL_I);
//         printf(" testIncl after setting to PW_NATURAL_I (%d): %d\n", PW_NATURAL_I, testIncl );
//         printf(" PW_EA_IS_TAMED(testIncl): %d\n", PW_EA_IS_TAMED(testIncl));
//         PW_EA_SET_APPROXIMD(testIncl);
//         printf(" testIncl after setting to PW_EA_APPROXIMD (%d): %d\n", PW_EA_APPROXIMD, testIncl );
//         printf(" PW_EA_IS_TAMED(testIncl): %d\n", PW_EA_IS_TAMED(testIncl));
//         PW_EA_SET_INCLUSION(testIncl,PW_ISOLATING);
//         printf(" PW_EA_IS_TAMED(testIncl): %d\n", PW_EA_IS_TAMED(testIncl));
//         printf(" testIncl after setting to PW_ISOLATING (%d): %d\n", PW_ISOLATING, testIncl );
//         printf(" PW_EA_IS_TAMED(testIncl): %d\n", PW_EA_IS_TAMED(testIncl));
//         PW_EA_UNSET_APPROXIMD(testIncl);
//         printf(" testIncl after unsetting PW_EA_APPROXIMD (%d): %d\n", PW_EA_APPROXIMD, testIncl );
//         printf(" PW_EA_IS_TAMED(testIncl): %d\n", PW_EA_IS_TAMED(testIncl));
//         testIncl = 0;
//         printf("\n");
//         PW_EA_SET_INCLUSION(testIncl,PW_NATURAL_I);
//         printf(" testIncl after setting to PW_NATURAL_I (%d): %d\n", PW_NATURAL_I, testIncl );
//         PW_EA_SET_NEWTO_FAILED(testIncl);
//         printf(" testIncl after setting NEWTON FAILED: %d\n", testIncl );
//         printf(" PW_EA_IS_NATURAL_I(testIncl): %d\n", PW_EA_IS_NATURAL_I(testIncl));
//         printf(" PW_EA_NEWTO_FAILED(testIncl): %d\n", PW_EA_NEWTO_FAILED(testIncl));
//         printf(" PW_EA_NEWTO_SUCCEE(testIncl): %d\n", PW_EA_NEWTO_SUCCEE(testIncl));
//         PW_EA_SET_NEWTO_SUCCEE(testIncl);
//         printf(" testIncl after setting NEWTON SUCCEE: %d\n", testIncl );
//         printf(" PW_EA_IS_NATURAL_I(testIncl): %d\n", PW_EA_IS_NATURAL_I(testIncl));
//         printf(" PW_EA_NEWTO_FAILED(testIncl): %d\n", PW_EA_NEWTO_FAILED(testIncl));
//         printf(" PW_EA_NEWTO_SUCCEE(testIncl): %d\n", PW_EA_NEWTO_SUCCEE(testIncl));
        
    }
    
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if (type==PW_PARSE_FAILD){
        clear_global_variables();
        flint_cleanup();
        exit(0);
    }
    
#ifdef PW_EA_PROFILE
    PW_EA_INIT_PROFILE
#endif
    
    if ((type==PW_PARSE_EXACT)&&(roundInput>0)) {
        type = PW_PARSE_BALLS;
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, (slong) roundInput );
    }
    
    /* result */
    acb_ptr roots = _acb_vec_init( length - 1 );
    slong * mults = (slong *) pwpoly_malloc ( (length - 1)*sizeof(slong) );
    slong nbclusts = 0;
    slong nbroots = 0;
    
    pw_polynomial_t poly;
    if (type==PW_PARSE_EXACT) {
        pw_polynomial_init_from_exact_fmpq( poly, pfmpq_re, pfmpq_im );
    } else {
        for (slong i=0; i<length; i++) {
            mag_zero( arb_radref(acb_realref(pacb->coeffs+i)) );
            mag_zero( arb_radref(acb_imagref(pacb->coeffs+i)) );
        }
        pw_polynomial_init_from_exact_acb( poly, pacb );
    }
    
//     int realCoeffs = pw_polynomial_is_real(poly);
    slong multOfZero = pw_polynomial_remove_root_zero(poly);
//     slong nlen = pw_polynomial_length(poly);
    
    if (multOfZero>0) {
        acb_zero( roots + 0 );
        mults[0] = multOfZero;
        multOfZero = 1;
    }
    
    clock_t start_solve = clock();
    
    slong prec = PWPOLY_DEFAULT_PREC;
    
    slong res = pw_EA_solve_pw_polynomial ( roots+multOfZero, mults+multOfZero, &nbclusts, 0, poly, goal, log2eps, dom, &prec, 1000, 0, 0, NULL );
    nbclusts+=multOfZero;

    /* get nbroots */
    nbroots=0;
    for (slong i=0; i< nbclusts; i++)
        nbroots+=mults[i];
    
    double clicks_in_first_solve = (clock() - start_solve);
    
//     if (verbosity >= level) {
//         printf(" success: %ld\n", nbroots) ;
//         printf(" time for isolating the roots: %lf seconds\n",
//              clicks_in_solve/CLOCKS_PER_SEC) ;
//     }
    
//     FILE * outPltFile;
//     char outPltFileName[] = "roots.plt\0";
//     outPltFile = fopen (outPltFileName,"w");
// //     pw_points_gnuplot( outPltFile, roots, nbclusts );
//     pw_points_gnuplot( outPltFile, roots, length-1 );
//     fclose (outPltFile);
    
// #ifdef PW_EA_PROFILE
//     PW_EA_PRINT_PROFILE
// #endif

    /* check the output */
//     if (check) {
//         clock_t start_check = clock();
//         int checkOK = 0;
//         if (type==PW_PARSE_EXACT){
// //             checkOK = pwpoly_checkOutput_isolate_2fmpq_poly ( roots, (slong)(length-1), pfmpq_re, pfmpq_im );
//             checkOK = pwpoly_checkOutput_solve_2fmpq_poly ( roots, mults, nbclusts, pfmpq_re, pfmpq_im, log2eps, goal );
//         } else {
// //             checkOK = pwpoly_checkOutput_isolate_exact_acb_poly ( roots, (slong)(length-1), pacb );
//             checkOK = pwpoly_checkOutput_solve_exact_acb_poly ( roots, mults, nbclusts, pacb, log2eps, goal );
//         }
//         double clicks_in_check = (clock() - start_check);
//         printf(" check output                    : %d\n", checkOK);
//         printf(" time for checking output        : %lf seconds\n",
//                  clicks_in_check/CLOCKS_PER_SEC) ;
//     } else {
//         if (verbosity>=level) {
//             printf(" check output                    : NOT CHECKED\n");
//         }
//     }
    
#ifdef PW_EA_PROFILE
    PW_EA_INIT_PROFILE
#endif 

    slong nbThrow = 100;
    slong lenNatural = (length-1) - multOfZero - nbThrow;
    
    acb_ptr nroots = _acb_vec_init( length - 1 );
    slong * nmults = (slong *) pwpoly_malloc ( (length - 1)*sizeof(slong) );
    
    printf("roots: \n");
    for (slong i=0; i<nbclusts; i++) {
        printf("--- %ld-th: mult: %ld ", i, mults[i]);
        acb_printd(roots+i, 10); printf("\n");
    }
    printf("\n");
    
    /* keep lenNatural natural isolators */
    if (multOfZero>0) {
        acb_zero( nroots + 0 );
        nmults[0] = mults[0];
    }
    slong indInNroots=0;
    for (slong i=0; (i<(length-1-multOfZero))&&(indInNroots<lenNatural); i++ ){
        if ( mults[i+multOfZero] == 1 ) {
            nmults[indInNroots+multOfZero]=1;
            acb_set( nroots + (indInNroots+multOfZero), roots + (i+multOfZero) );
            indInNroots++;
        }
    }
    
    printf("lenNatural: %ld, multOfZero: %ld, nroots: \n", lenNatural, multOfZero);
    for (slong i=0; i<lenNatural+multOfZero; i++) {
        printf("--- %ld-th: mult: %ld", i, nmults[i]);
        acb_printd(nroots+i, 10); printf("\n");
    }
    printf("\n");
    
    FILE * outPltFile;
    char outPltFileName[] = "init_points.plt\0";
    outPltFile = fopen (outPltFileName,"w");
// //     pw_points_gnuplot( outPltFile, roots, nbclusts );
//     pw_points_gnuplot( outPltFile, roots, length-1 );
//     fclose (outPltFile);
    
    start_solve = clock();
    
    slong nnbclusts=0;
    prec = PWPOLY_DEFAULT_PREC;
    res = pw_EA_solve_pw_polynomial ( nroots+multOfZero, nmults+multOfZero, &nnbclusts, lenNatural, poly, goal, log2eps, dom, &prec, m, 0, verbosity, outPltFile );
    
    nnbclusts+=multOfZero;

    /* get nbroots */
    slong nnbroots=0;
    for (slong i=0; i< nnbclusts; i++)
        nnbroots+=nmults[i];
    
    double clicks_in_solve = (clock() - start_solve);
    
    fclose (outPltFile);
    
    if (verbosity >= level) {
        printf(" res: %ld, success: %ld\n", res, nnbroots) ;
        printf(" first: %lf seconds\n",
             clicks_in_first_solve/CLOCKS_PER_SEC) ;
        printf(" time for isolating the roots: %lf seconds\n",
             clicks_in_solve/CLOCKS_PER_SEC) ;
    }

#ifdef PW_EA_PROFILE
    PW_EA_PRINT_PROFILE
#endif

    /* check the output */
    if (check) {
        clock_t start_check = clock();
        int checkOK = 0;
        if (type==PW_PARSE_EXACT){
            checkOK = pwpoly_checkOutput_solve_2fmpq_poly ( nroots, nmults, nnbclusts, pfmpq_re, pfmpq_im, log2eps, goal, dom );
        } else {
            checkOK = pwpoly_checkOutput_solve_exact_acb_poly ( nroots, nmults, nnbclusts, pacb, log2eps, goal, dom );
        }
        double clicks_in_check = (clock() - start_check);
        printf(" check output                    : %d\n", checkOK);
        printf(" time for checking output        : %lf seconds\n",
                 clicks_in_check/CLOCKS_PER_SEC) ;
    } else {
        if (verbosity>=level) {
            printf(" check output                    : NOT CHECKED\n");
        }
    }
    
    _acb_vec_clear(nroots, length-1);
    pwpoly_free(nmults);
    
    pw_polynomial_clear(poly);
//     _acb_vec_clear( tamed, length - 1 );
    pwpoly_free(mults);
    _acb_vec_clear( roots, length - 1 );
    clear_global_variables();
    
    flint_cleanup();
    
    return 0;
}
