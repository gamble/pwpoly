#include "pw_base.h"
#include "common/pw_common.h"
#include "common/pw_gnuplot.h"
#include "solver_CoCo.h"
#include <time.h>

acb_poly_t pacb;
fmpq_poly_t pfmpq_re;
fmpq_poly_t pfmpq_im;
fmpq_t b; /* not used */
slong length;
int verbosity, increase, check, goal, solver;
slong m, log2eps, roundInput, goalNbRoots;
domain_t dom;

void init_global_variables(){
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    length = 0;
    verbosity = 1;
    m = 30;
    increase = 1;
    check = 0;
    log2eps = -53;
    goal = PW_GOAL_APPROXIMATE;
    solver = 2;
    roundInput = 0;
    fmpq_init(b);
    domain_init(dom); /* by default set to C */
    goalNbRoots = -1; /* find all the roots */
}

void clear_global_variables(){
    domain_clear(dom);
    fmpq_clear(b);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(pfmpq_re);
    acb_poly_clear(pacb);
}

int main(int argc, char* argv[]) {
    
    int level = 1;
    
    if (argc<2) {
//         printf("Usage: %s filename\n", argv[0]);
        pwroots_fprint_options( stdout, argc, argv );
        exit(0);
    }
    
    init_global_variables();
    int parse = parseInputArgs( argc, argv, &verbosity, dom, &goalNbRoots, &m, &increase, &check, &log2eps, &goal, &solver, &roundInput, b );
    
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if ( (type==PW_PARSE_FAILD) || (parse==0) ){
        clear_global_variables();
        flint_cleanup();
        exit(0);
    }
    
        if (verbosity>=level) {
        printf("roundInput : %ld\n", roundInput);
        printf("log2eps    : %ld\n", log2eps);
        printf("goal       : "); pwroots_fprint_goal(stdout, goal); printf("\n");
        printf("domain     : "); domain_print_short(dom); printf("\n"); 
        printf("check      : %d\n", check);
        printf("verbosity  : %d\n", verbosity);
        printf("domain     : "); domain_print_short(dom); printf("\n"); 
        printf("goalNbRoots: %ld\n", goalNbRoots);
    }
    
    if ((type==PW_PARSE_EXACT)&&(roundInput>0)) {
        type = PW_PARSE_BALLS;
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, (slong) roundInput );
    }

#ifdef PW_COCO_PROFILE
    PW_COCO_INIT_PROFILE
#endif

    acb_ptr roots = _acb_vec_init( length - 1 );
    slong * mults = (slong *) pwpoly_malloc ( (length-1)*sizeof(slong) );
    slong nbClust = -1;
    
    clock_t start_solve = clock();
    
    if (type==PW_PARSE_EXACT) {
        nbClust = solver_CoCo_solve_2fmpq_poly( roots, mults, 0, pfmpq_re, pfmpq_im, log2eps, goal, dom, goalNbRoots, verbosity );
    } else {
        for (slong i=0; i<length; i++) {
            mag_zero( arb_radref(acb_realref(pacb->coeffs+i)) );
            mag_zero( arb_radref(acb_imagref(pacb->coeffs+i)) );
        }
    
        nbClust = solver_CoCo_solve_exact_acb_poly( roots, mults, 0, pacb, log2eps, goal, dom, goalNbRoots, verbosity );
    }
    
    double clicks_in_solve = (clock() - start_solve);
    
    slong nbRoots = 0;
    for (slong i=0; i<nbClust; i++)
        nbRoots += mults[i];
    
    if (verbosity >= level) {
        printf(" time for approximating the roots: %lf seconds\n",
             clicks_in_solve/CLOCKS_PER_SEC) ;
        printf(" number of clusters              : %ld\n", nbClust);
        printf(" number of roots                 : %ld\n", nbRoots);
    }

#ifdef PW_COCO_PROFILE
    PW_COCO_PRINT_PROFILE
#endif

    FILE * outPltFile;
    char outPltFileName[] = "roots.plt\0";
    outPltFile = fopen (outPltFileName,"w");
//     pw_points_gnuplot( outPltFile, roots, nbClust );
    pw_roots_domain_gnuplot( outPltFile, roots, nbClust, dom );
    fclose (outPltFile);
    
    /* write the roots */
    FILE * outFile;
    char outFileName[] = "roots.txt\0";
    outFile = fopen (outFileName,"w");
    pwpoly_write_roots_mults( outFile, roots, mults, nbClust, 10 );
    if (outFile != NULL)
        fclose (outFile);
    
    /* check the output */
    if (check) {
        clock_t start_check = clock();
        int checkOK = 0;
        if (type==PW_PARSE_EXACT) {
            checkOK = pwpoly_checkOutput_solve_2fmpq_poly ( roots, mults, nbClust, pfmpq_re, pfmpq_im, log2eps, goal, dom, (slong) goalNbRoots );
        } else {
            checkOK = pwpoly_checkOutput_solve_exact_acb_poly ( roots, mults, nbClust, pacb, log2eps, goal, dom, (slong) goalNbRoots );
        }
        
        double clicks_in_check = (clock() - start_check);
        printf(" check output                    : %d\n", checkOK);
        printf(" time for checking output        : %lf seconds\n",
                 clicks_in_check/CLOCKS_PER_SEC) ;
    } else {
        if (verbosity>=level) {
            printf(" check output                    : NOT CHECKED\n");
        }
    }
    
    pwpoly_free(mults);
    _acb_vec_clear( roots, length - 1 );
    clear_global_variables();
    
    flint_cleanup();
    
    return 0;
}
