#include "pw_base.h"
#include "common/pw_common.h"
#include "covering/pw_covering.h"
#include "approximation/pw_approximation.h"
#include <stdio.h>

int slong_compare( const void * a, const void * b ) {
    return *(slong *)a<*(slong *)b;
}
slong acb_log_error_relative_to_tilde( arb_t ptildeval, int first, const acb_t eval, const acb_t point, arb_poly_t ptilde, slong prec ) {
    if (acb_is_exact(eval)) {
//         printf("evaluation of "); acb_printd(point, 10); printf(" is exact: "); acb_printd(eval, 10); printf("\n");
        return LONG_MIN;
    }
    
    arb_t pointabs;
    arb_init(pointabs);
    
    acb_t errors;
    acb_init(errors);
    
    acb_abs(pointabs, point, prec);
    if (first)
        arb_poly_evaluate_rectangular(ptildeval, ptilde, pointabs, prec);
//     printf(" ------ ptilde: "); arb_printd(ptildeval, 10); printf("\n");
    
    arb_get_rad_arb( acb_realref(errors), acb_realref(eval) );
    arb_get_rad_arb( acb_imagref(errors), acb_imagref(eval) );
    arb_max( acb_realref(errors), acb_realref(errors), acb_imagref(errors), prec );
    arb_div( acb_realref(errors), acb_realref(errors), ptildeval, prec );
    
//     printf(" ------ error relative to ptilde: "); arb_printd(acb_realref(errors), 10); printf("\n");
    arb_log_base_ui( acb_realref(errors), acb_realref(errors), 2, prec );
//     printf(" ------ log2 of error relative to ptilde: "); arb_printd(acb_realref(errors), 10); printf("\n");
    arb_get_ubound_arf( arb_midref( acb_realref(errors) ), acb_realref(errors), prec );
    slong res = arf_get_si( arb_midref( acb_realref(errors) ), ARF_RND_CEIL );
    
    acb_clear(errors);
    
    arb_clear(pointabs);
    
    return res;
}

acb_poly_t pacb, ppacb;
fmpq_poly_t pfmpq_re, pfmpq_im, ppfmpq_re, ppfmpq_im;
slong length;
int type;
int c;
fmpq_t scale, b;
int verbosity, m;

void init_global_variables(){
    acb_poly_init(pacb);
    acb_poly_init(ppacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    fmpq_poly_init(ppfmpq_re);
    fmpq_poly_init(ppfmpq_im);
    length = 0;
    c      = 16;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 7, 2);
    verbosity = 1;
    m = 16;
}

void clear_global_variables(){
    acb_poly_clear(pacb);
    acb_poly_clear(ppacb);
    fmpq_poly_clear(pfmpq_re);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(ppfmpq_re);
    fmpq_poly_clear(ppfmpq_im);
    fmpq_clear(scale);
    fmpq_clear(b);
}

int main(int argc, char **argv){
    
    if (argc<1) {
        printf(" !no input file! \n");
        return 0;
    }
    
    init_global_variables();
    
    if (argc>2)
        sscanf(argv[2], "%d", &m);
    printf("m: %d\n", m);
    
    int nbPointsPerSector = 10;
    if (argc>3)
        sscanf(argv[3], "%d", &nbPointsPerSector);
    printf("nbPointsPerSector: %d\n", nbPointsPerSector);
    
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if (type==PW_PARSE_FAILD){
        clear_global_variables();
        flint_cleanup();
        return 0;
    }

#ifdef PW_PROFILE
    PW_INIT_PROFILE
#endif

    pw_approximation_t app, appprime;
    slong prec;
    
    if (type==PW_PARSE_EXACT) {
        ulong working_m = pw_approximation_get_working_m( (ulong)m, length );
        prec = pw_approximation_get_working_prec(working_m);
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, prec);
//         pw_approximation_init_2fmpq_poly( app, pfmpq_re, pfmpq_im, (ulong)m, (ulong)c, scale, b);
        pw_approximation_init_acb_poly( app, pacb, (ulong)m, (ulong)c, scale, b);
//         prec = pw_approximation_precref(app);
//         acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, prec);
        fmpq_poly_derivative(ppfmpq_re, pfmpq_re);
        fmpq_poly_derivative(ppfmpq_im, pfmpq_im);
        acb_poly_set2_fmpq_poly( ppacb, ppfmpq_re, ppfmpq_im, prec);
//         pw_approximation_init_2fmpq_poly( appprime, ppfmpq_re, ppfmpq_im, (ulong)m, (ulong)c, scale, b);
        pw_approximation_init_acb_poly( appprime, ppacb, (ulong)m, (ulong)c, scale, b);
    } else {
        pw_approximation_init_acb_poly( app, pacb, (ulong)m, (ulong)c, scale, b);
        prec = pw_approximation_precref(app);
        acb_poly_derivative(ppacb, pacb, prec );
        pw_approximation_init_acb_poly( appprime, ppacb, (ulong)m, (ulong)c, scale, b);
    }
    
//     pw_approximation_print( app );
    
    pw_approximation_compute_approx_annulii( app, 0 );
    
//     pw_approximation_print( appprime );
    
    pw_approximation_compute_approx_annulii( appprime, 0 );
    
    arb_poly_t ptilde;
    arb_poly_init(ptilde);
    arb_poly_fit_length(ptilde, pacb->length);
    _arb_poly_set_length(ptilde, pacb->length);
    for (slong i = 0; i < pacb->length; i++)
        acb_abs( ptilde->coeffs + i, pacb->coeffs + i, prec );
    
    ulong N  = pw_approximation_Nref(app);
    
    /* put nbPointsPerSector point in each annulus/angular sector */
    ulong nbPoints=0;
    for (ulong n=0; n<N; n++)
        nbPoints += nbPointsPerSector*pw_approximation_nbsectref(app)[n];
    
    acb_ptr rootsOfUnits = _acb_vec_init(nbPointsPerSector);
    acb_ptr points = _acb_vec_init(nbPoints);
    
    _acb_vec_unit_roots(rootsOfUnits, nbPointsPerSector, nbPointsPerSector, prec);
    
    acb_t temp;
    acb_init(temp);
    
    arb_t s;
    arb_init(s);
    arb_set_d(s, 0.5);
    ulong indp = 0;
    for (ulong n=0; n<N; n++) {
        ulong K  = pw_approximation_nbsectref(app)[n];
        if (n==N-1) {
            for (int i=0; i< nbPointsPerSector; i++) {
                acb_div_arb( points+indp, rootsOfUnits+i, s, prec );
                acb_mul_arb(points+indp, points+indp, pw_approximation_annulii_radsref(app)+(n-1), prec);
                indp++;
            }
        } else if (K==1) {
            for (int i=0; i< nbPointsPerSector; i++) {
                acb_mul_arb( points+indp, rootsOfUnits+i, pw_approximation_app_gammasref(app)+n, prec );
                indp++;
            }
        } else {
            for (ulong k=0; k<K; k++){
                for (int i=0; i< nbPointsPerSector; i++) {
                    acb_mul_arb( temp, rootsOfUnits+i, s, prec );
                    _pw_covering_shift_back_point( points+indp, pw_approximation_coveringref(app), n, k, temp, prec );
                    indp++;
                }
            }
        }
    }
    
    acb_clear(temp);
    arb_clear(s);
    _acb_vec_clear(rootsOfUnits, nbPointsPerSector);
    
    int res = 1;
    acb_t valueACB, valuederACB, valuederderACB, valuePWE, valuederPWE, valuePWE2, valuederPWE2, valuederderPWE2;
    acb_init(valueACB);
    acb_init(valuederACB);
    acb_init(valuederderACB);
    acb_init(valuePWE);
    acb_init(valuederPWE);
    acb_init(valuePWE2);
    acb_init(valuederPWE2);
    acb_init(valuederderPWE2);
    
    int log2errorpweOK = 1;
    int log2errorpwe2OK = 1;
    int log2erroracbOK = 1;
    int valueOverlaps = 1;
    int valuederOverlaps = 1;
    int valuederderOverlaps = 1;
    
    arb_t dist, maxdist, vtilde, one;
    acb_t diff;
    arb_init(dist);
    arb_init(maxdist);
    arb_init(vtilde);
    acb_init(diff);
    arb_init(one);
    arb_one(one);
    arb_zero(maxdist);
    slong acb_min_rel_error = LONG_MAX;
    slong acb_max_rel_error = LONG_MIN;
    slong pwe_min_rel_error = LONG_MAX;
    slong pwe_max_rel_error = LONG_MIN;
    slong pwe2_min_rel_error = LONG_MAX;
    slong pwe2_max_rel_error = LONG_MIN;
    slong * acb_rel_errors = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * pwe_rel_errors = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
    slong * pwe2_rel_errors = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
        
    ulong n=0;
    for ( ; (n<nbPoints)&&res; n++){
        
//         mag_zero( arb_radref( acb_realref( points+n ) ) );
//         mag_zero( arb_radref( acb_imagref( points+n ) ) );
        
        acb_poly_evaluate2_rectangular(valueACB, valuederACB, pacb, points+n, prec);
        slong relerracb = acb_log_error_relative_to_tilde( vtilde, 1, valueACB, points+n, ptilde, prec );
        acb_min_rel_error = PWPOLY_MIN( acb_min_rel_error, relerracb );
        acb_max_rel_error = PWPOLY_MAX( acb_max_rel_error, relerracb );
        log2erroracbOK = log2erroracbOK && (relerracb <= -(slong)m);
        acb_rel_errors[n] = relerracb;
            
        pw_approximation_evaluate_acb( valuePWE, app, points+n, 0 );
        pw_approximation_evaluate_acb( valuederPWE, appprime, points+n, 0);
        slong relerrpwe = acb_log_error_relative_to_tilde( vtilde, 0, valuePWE, points+n, ptilde, prec );
        pwe_min_rel_error = PWPOLY_MIN( pwe_min_rel_error, relerrpwe );
        pwe_max_rel_error = PWPOLY_MAX( pwe_max_rel_error, relerrpwe );
        log2errorpweOK = log2errorpweOK && (relerrpwe <= -(slong)m);
        pwe_rel_errors[n] = relerrpwe;
        
        pw_approximation_evaluate2_acb( valuePWE2, valuederPWE2, app, points+n, 0 );
        slong relerrpwe2 = acb_log_error_relative_to_tilde( vtilde, 0, valuePWE2, points+n, ptilde, prec );
        pwe2_min_rel_error = PWPOLY_MIN( pwe2_min_rel_error, relerrpwe2 );
        pwe2_max_rel_error = PWPOLY_MAX( pwe2_max_rel_error, relerrpwe2 );
        log2errorpwe2OK = log2errorpwe2OK && (relerrpwe2 <= -(slong)m);
        pwe2_rel_errors[n] = relerrpwe2;
        
        valueOverlaps = acb_overlaps(valueACB, valuePWE) && acb_overlaps(valueACB, valuePWE2);
        valuederOverlaps = acb_overlaps(valuederACB, valuederPWE) && acb_overlaps(valuederACB, valuederPWE2);
        
        acb_poly_evaluate2_rectangular(valuederACB, valuederderACB, ppacb, points+n, prec);
        pw_approximation_evaluate2_acb( valuederPWE2, valuederderPWE2, appprime, points+n, 0 );
        
        valuederderOverlaps = acb_overlaps(valuederderACB, valuederderPWE2);
        
        acb_sub(diff, valuePWE, valueACB, prec);
        acb_abs(dist, diff, prec);
        arb_div(dist, dist, vtilde, prec);
        arb_max(maxdist, maxdist, dist, prec);
        
        acb_sub(diff, valuePWE2, valueACB, prec);
        acb_abs(dist, diff, prec);
        arb_div(dist, dist, vtilde, prec);
        arb_max(maxdist, maxdist, dist, prec);
        
        res = log2errorpweOK && log2errorpwe2OK && valueOverlaps && valuederOverlaps && valuederderOverlaps && arb_lt(maxdist, one);
//         res = valueOverlaps && valuederOverlaps && arb_lt(maxdist, one);
        
        if (!res) {
            printf(" result: %d, n= %lu/%lu\n", res, n, nbPoints);
            printf(" log2errorpweOK : %d\n", log2errorpweOK);
            printf(" log2errorpwe2OK: %d\n", log2errorpwe2OK);
            printf(" max dist center to center: "); arb_printd(maxdist, 10); printf("\n");
            printf(" values       overlaps: %d\n", valueOverlaps);
            printf(" valuesder    overlaps: %d\n", valuederOverlaps);
            printf(" valuesderder overlaps: %d\n", valuederderOverlaps);
            printf("\n");
            printf(" ACB eval prec %ld  : ", prec); acb_printd(valueACB, 10); printf("\n");
            printf(" PWE eval      : "); acb_printd(valuePWE, 10); printf("\n");
            printf(" ACB eval overlaps PWE eval : %d\n", acb_overlaps(valueACB, valuePWE) );
            printf(" PWE2 eval     : "); acb_printd(valuePWE2, 10); printf("\n");
            printf(" ACB eval overlaps PWE2 eval: %d\n", acb_overlaps(valueACB, valuePWE2) );
            printf("\n");
            printf(" acb evalder   : "); acb_printd(valuederACB, 10); printf("\n");
            printf(" acb evalderder: "); acb_printd(valuederderACB, 10); printf("\n");
            
            pw_approximation_evaluate_acb( valuePWE2, app, points+n, 4 );
            
            pw_approximation_evaluate2_acb( valuePWE2, valuederPWE2, app, points+n, 4 );
            
            pw_approximation_evaluate2_acb( valuederPWE2, valuederderPWE2, appprime, points+n, 4 );
        }
        
    }
    qsort (acb_rel_errors, nbPoints, sizeof(slong), slong_compare);
    qsort (pwe_rel_errors, nbPoints, sizeof(slong), slong_compare);
    qsort (pwe2_rel_errors, nbPoints, sizeof(slong), slong_compare);
    
    printf(" res: %d, n= %lu/%lu\n", res, n, nbPoints);
    printf(" log2erroracbOK : %d\n", log2erroracbOK);
    printf(" acb_min_rel_error: %ld, acb_max_rel_error: %ld, median value: %ld \n", acb_min_rel_error, acb_max_rel_error, acb_rel_errors[nbPoints/2]);
    printf(" log2errorpweOK : %d\n", log2errorpweOK);
    printf(" pwe_min_rel_error: %ld, pwe_max_rel_error: %ld, median value: %ld  \n", pwe_min_rel_error, pwe_max_rel_error, pwe_rel_errors[nbPoints/2]);
    printf(" log2errorpwe2OK: %d\n", log2errorpwe2OK);
    printf(" pwe2_min_rel_error: %ld, pwe2_max_rel_error: %ld, median value: %ld  \n", pwe2_min_rel_error, pwe2_max_rel_error, pwe2_rel_errors[nbPoints/2]);
    printf(" max dist center to center: "); arb_printd(maxdist, 10); printf("\n");
    printf(" values       overlaps: %d\n", valueOverlaps);
    printf(" valuesder    overlaps: %d\n", valuederOverlaps);
    printf(" valuesderder overlaps: %d\n", valuederderOverlaps);
    

    pwpoly_free( acb_rel_errors );
    pwpoly_free( pwe_rel_errors );
    pwpoly_free( pwe2_rel_errors );
    arb_clear(dist);
    arb_clear(vtilde);
    arb_clear(maxdist);
    acb_clear(diff);
    arb_clear(one);
    
    acb_clear(valueACB);
    acb_clear(valuederACB);
    acb_clear(valuederderACB);
    acb_clear(valuePWE);
    acb_clear(valuederPWE);
    acb_clear(valuePWE2);
    acb_clear(valuederPWE2);
    acb_clear(valuederderPWE2);
    
    FILE * covFile;
    char covFileName[] = "covering_with_points.plt\0";
    covFile = fopen (covFileName,"w");
    _pw_covering_with_points_gnuplot( covFile, pw_approximation_coveringref(app), points, nbPoints);
    fclose (covFile);
    
    _acb_vec_clear(points,    nbPoints);
    
    pw_approximation_clear(app);
    pw_approximation_clear(appprime);
    
#ifdef PW_PROFILE
    PW_PRINT_PROFILE
#endif    
    
    clear_global_variables();
    flint_cleanup();
    
    return 0;
}
