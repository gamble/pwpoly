#include <stdio.h>

#define _MPS_PRIVATE
#include <mps/mps.h>
// const static short int mps_complex_structures[] = { 0, 0, 0, 0, 1, 1, 1, 1, 0 };
// #define MPS_STRUCTURE_IS_COMPLEX(x)  (mps_complex_structures[(x)])
// const static short int mps_rational_structures[] = { 0, 1, 0, 0, 0, 1, 0, 0, 0 };
// #define MPS_STRUCTURE_IS_RATIONAL(x) (mps_rational_structures[(x)])
// const static short int mps_fp_structures[] = { 0, 0, 1, 0, 0, 0, 1, 0, 0 };
// #define MPS_STRUCTURE_IS_FP(x)       (mps_fp_structures[(x)])


int main(int argc, char **argv){
    
    if (argc<1) {
        printf(" !no input file! \n");
        return 0;
    }
        
    FILE * infile = fopen (argv[1], "r");
    
    if (!infile) {
        printf(" !Cannot open input file for read, aborting! \n");
        return 0;
    }
    
    mps_context * ctx = mps_context_new ();
    mps_polynomial * poly = mps_parse_stream (ctx, infile);
    
    if (!poly) {
        printf(" !Error while parsing the polynomial, aborting! \n");
        mps_context_free(ctx);
        fclose (infile);
        return 0;
    }
    
    int deg = poly->degree;
    /* Print input polynomial */
    mps_monomial_poly *p = MPS_POLYNOMIAL_CAST(mps_monomial_poly, poly);
    
    if (MPS_STRUCTURE_IS_RATIONAL( poly->structure )) {
        mpq_t coeff_re, coeff_im;
        mpq_init(coeff_re);
        mpq_init(coeff_im);
        printf("Input polynomial has rational coeffs\n");
        printf("degree\treal part\timaginary part\t\n");
        printf("=================================================================\n");
        for (int m = 0; m <= deg; m++) {
            mps_monomial_poly_get_coefficient_q (ctx, p, m, coeff_re, coeff_im);
            printf("%d\t", m);
            mpq_out_str( stdout, 10, coeff_re );
            printf("\t\t");
            mpq_out_str( stdout, 10, coeff_im );
            printf("\n");
        }
        printf("=================================================================\n\n");
        mpq_clear(coeff_re);
        mpq_clear(coeff_im);
    } else if (MPS_STRUCTURE_IS_FP( poly->structure )) {
        printf("Input polynomial has floating point coeffs of precision: %ld\n", mps_monomial_poly_get_precision(ctx, p));
        printf("Input polynomial has floating point coeffs of precision: %ld\n", p->prec);
        printf("degree\treal part\timaginary part\t\n");
        printf("=================================================================\n");
        for (int m = 0; m <= deg; m++) {
            gmp_printf("%Fe\t", mpc_Re(p->mfpc[m]));
            printf("\t\t");
            gmp_printf("%Fe\t", mpc_Im(p->mfpc[m]));
            printf("\n");
        }
        printf("=================================================================\n\n");
    }
    
    mpc_t *roots = NULL;  /* pairs of gmp's mpf_t, one for real part, one for imag part, defined in mps/mpc.h */
    rdpe_t *radii = NULL; /* a rdpe is a double (the mantissa) + a long (the exponent) , defined in mps/mt.h  */
  
    mps_context_set_input_poly (ctx, poly);
    /* Set the output precision */
    mps_context_set_output_prec (ctx, 53);
    mps_context_set_output_goal (ctx, MPS_OUTPUT_GOAL_APPROXIMATE);
    /* Set the algorithm */
    mps_context_select_algorithm(ctx, MPS_ALGORITHM_SECULAR_GA);
    
    /* Find the roots */
    mps_mpsolve (ctx);
    
    /* Read the roots from context */
    mps_context_get_roots_m (ctx, &roots, &radii);
    
    /* Print out roots */
    double err_max=0., err_d=0.;
    double root_re, root_im;
    printf("Roots:\t\t\t\t\tInclusion\n");
    printf("real part\t imaginary part\t\tabsolute radius\n");
    printf("=================================================================\n");
//     mpc_init2(err, mpc_get_prec (roots[0]));
    for (int m = 0; m < deg; m++) {
        err_d = rdpe_get_d (radii[m]);
        err_max = err_d > err_max ? err_d : err_max;
        root_re = mpf_get_d( mpc_Re(roots[m]) );
        root_im = mpf_get_d( mpc_Im(roots[m]) );
        printf("%.3e\t%.3e\t\t%.3e\n", root_re, root_im, err_d );
    }
    printf("=================================================================\n");
    printf("\t\t\t\tmax(Error) = %.3e\n",err_max);
    
    /* Free the allocated memory */
    mps_monomial_poly_free(ctx, poly);
    mps_context_free(ctx);
    free (roots);
    /* free(roots); */
    free(radii);
    
    fclose (infile);
    
    return EXIT_SUCCESS;
}
