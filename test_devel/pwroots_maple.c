#include "interface/pwpoly_mpz_cb.h"
#include "pw_mm.h"
#include <time.h>

pwmm_table_t default_pwmm_table;
/* malloc functions */
static __inline__ void * m_malloc_for_gmp (size_t size) { 
    return (void *) pwmm_malloc( (PWMM_SIZ) size, default_pwmm_table ); 
}
static __inline__ void * m_malloc_for_flint (size_t size) {  
    return (void *) pwmm_malloc( (PWMM_SIZ) size, default_pwmm_table );
}
/* realloc functions */
static __inline__ void * m_realloc_for_gmp (void * old_ptr, size_t oldsize, size_t new_size) { 
    return (void *) pwmm_realloc( (PWMM_PTR) old_ptr, (PWMM_SIZ) new_size, default_pwmm_table );
}
static __inline__ void * m_realloc_for_flint (void * old_ptr, size_t new_size) {
    return (void *) pwmm_realloc( (PWMM_PTR) old_ptr, (PWMM_SIZ) new_size, default_pwmm_table );
}
/* calloc functions */
static __inline__ void * m_calloc_for_flint (size_t num, size_t size) { 
    return (void *) pwmm_calloc( (PWMM_SIZ) num, (PWMM_SIZ) size, default_pwmm_table );
}
/* free functions */
static __inline__ void   m_free_for_gmp (void * ptr, size_t old_size) {
    pwmm_free( (PWMM_PTR) ptr, default_pwmm_table );
}
static __inline__ void   m_free_for_flint (void * ptr) { 
    pwmm_free( (PWMM_PTR) ptr, default_pwmm_table ); 
}

clock_t start_global;
/* interruption function */
int m_check_interrupt(){
//     double clicks = (clock() - start_global);
//     int ret = clicks/CLOCKS_PER_SEC > 1.5;
//     if (ret)
//         printf("\n\n Interrupt ! %lf \n\n", clicks/CLOCKS_PER_SEC); 
    int ret = 0;
//     printf("\n\n check user defined interrupt: %d \n\n", ret);
    return ret;
}

#ifndef slong
typedef unsigned long int slong
#endif

slong mpz_vec_set_fmpq_poly( mpz_ptr coeffs, const fmpq_poly_t src );

void mpz_vec_fprint( FILE * file, const mpz_ptr vec, slong len );
void mpz_vec_print( const mpz_ptr vec, slong len ){ mpz_vec_fprint(stdout, vec, len); }

void domain_get_mpz_tab( mpz_struct domain_num[], mpz_struct domain_den[], const domain_t dom );

void mpz_domain_fprint( FILE * file, const mpz_struct domain_num[], const mpz_struct domain_den[] );
void mpz_domain_print( const mpz_struct domain_num[], const mpz_struct domain_den[] ) { 
    mpz_domain_fprint(stdout, domain_num, domain_den);
}

// #ifdef PW_NO_INTERFACE
// void mpz_cb_gnuplot(FILE *file, const mpz_cb_t);
// void mpz_cb_vec_gnuplot(FILE *fpreamb, FILE *fdata, mpz_cb_srcptr l, const slong len);
// #endif

acb_poly_t pacb;
fmpq_poly_t pfmpq_re;
fmpq_poly_t pfmpq_im;
slong length;
int c;
fmpq_t scale, b;
int verbosity, increase, check, goal, solver;
slong m, log2eps, roundInput, goalNbRoots;
domain_t dom;

void init_global_variables(){
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    length = 0;
    c      = 2;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
//     fmpq_set_si(b, 1, 10);
    verbosity = 1;
    m = 10;
    increase = 1;
    check = 0;
    log2eps = -34;
    goal = PW_GOAL_ISO_AND_APP;
    solver = 63;
    roundInput = 0;
    domain_init(dom); /* by default set to C */
    goalNbRoots = -1; /* find all the roots */
}

void clear_global_variables(){
    domain_clear(dom);
    fmpq_clear(b);
    fmpq_clear(scale);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(pfmpq_re);
    acb_poly_clear(pacb);
}

int main(int argc, char* argv[]) {
    
    int level=1;
    
    pwmm_table_init( default_pwmm_table );
    mp_set_memory_functions(  m_malloc_for_gmp, m_realloc_for_gmp, m_free_for_gmp );
    __flint_set_memory_functions( m_malloc_for_flint, m_calloc_for_flint, m_realloc_for_flint, m_free_for_flint );
    
    
    init_global_variables();
    
    int parse = parseInputArgs( argc, argv, &verbosity, dom, &goalNbRoots, &m, &increase, &check, &log2eps, &goal, &solver, &roundInput, b );
    
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if ( (type==PW_PARSE_FAILD) || (parse==0) ){
        clear_global_variables();
        flint_cleanup();
        pwmm_cleanup(default_pwmm_table);
        exit(0);
    }
    
    if (verbosity>=level) {
//         printf("----------\n");
//         printf("-- Input:-\n");
//         printf("----------\n");
//         printf("roundInput : %ld\n", roundInput);
        printf("----------\n");
        printf("-- Goal :-\n");
        printf("----------\n");
        printf("goal       : "); pwroots_fprint_goal(stdout, goal); printf("\n");
        printf("log2eps    : %ld\n", log2eps);
        printf("domain     : "); domain_print_short(dom); printf("\n"); 
        printf("goalNbRoots: %ld\n", goalNbRoots);
        printf("----------\n");
        printf("-- Options:\n");
        printf("----------\n");
        printf("m          : %ld\n", m);
        printf("increase   : %d\n", increase);
        printf("----------\n");
        printf("-- Output: \n");
        printf("----------\n");
        printf("check      : %d\n", check);
        printf("verbosity  : %d\n", verbosity);
    }
    
    /* create input/output variables */
    mpz_ptr pmpz_re = (mpz_ptr) malloc ( length*sizeof(mpz_struct) );
    for (slong i=0; i<length; i++)
        mpz_init( pmpz_re+i );
    mpz_ptr pmpz_im = NULL;
    if ( pfmpq_im->length > 0 ) {
        pmpz_im = (mpz_ptr) malloc ( length*sizeof(mpz_struct) );
        for (slong i=0; i<length; i++)
            mpz_init( pmpz_im+i );
    }
    
    mpz_cb_ptr roots = (mpz_cb_ptr) malloc ( (length-1)*sizeof(mpz_cb_struct) );
    for (slong i=0; i<(length-1); i++)
        mpz_cb_init( roots+i );
    
    slong *reals = (slong *) malloc ( (length-1)*sizeof(slong) );
    slong nbreals=0;
    slong *mults = (slong *) malloc ( (length-1)*sizeof(slong) );
    
    mpz_struct dom_num[4], dom_den[4];
    for (slong i=0; i<4; i++) {
        mpz_init(dom_num+i);
        mpz_init(dom_den+i);
    }
    
    /* convert input poly in mpz_vec */
    slong len_re = mpz_vec_set_fmpq_poly( pmpz_re, pfmpq_re);
//     printf("Input pol as an mpz vec: \n");
//     mpz_vec_print(pmpz_re, length);
//     printf("\n\n");
    slong len_im = 0;
    if ( pfmpq_im->length > 0 ) {
        len_im = mpz_vec_set_fmpq_poly( pmpz_im, pfmpq_im);
    }
    /* convert input domain in mpz tables */
    domain_get_mpz_tab( dom_num, dom_den, dom );
//     printf("Input dom as mpz tabs  : \n");
//     mpz_domain_print( dom_num, dom_den );
//     printf("\n\n");
    
    start_global = clock();
    slong nbRoots=0;
    slong nbClusts=0;
    /* call interface function */
    if (pmpz_im==NULL) {
        if (PW_GOAL_MUST_REFINE(goal)){
            nbClusts = pwroots_refine_mpz_poly_mpz_cb ( roots, mults, reals, &nbreals,
                                                        pmpz_re, len_re, log2eps, dom_num, dom_den, goalNbRoots,
                                                        m_check_interrupt);
            for (slong i=0; i<nbClusts; i++)
                nbRoots += mults[i];
            printf(" nb of   clusters: %ld\n", nbClusts);
            printf(" nb of      roots: %ld\n", nbRoots);
            printf(" nb of real roots: %ld\n", nbreals);
            
        } else if (PW_GOAL_MUST_ISOLATE(goal)) {
            nbRoots = pwroots_isolate_mpz_poly_mpz_cb ( roots, reals, &nbreals,
                                                        pmpz_re, len_re, log2eps, dom_num, dom_den, goalNbRoots,
                                                        m_check_interrupt);
            for (slong i=0; i<nbRoots; i++)
                mults[i]=1;
            printf(" nb of      roots: %ld\n", nbRoots);
            printf(" nb of real roots: %ld\n", nbreals);
            nbClusts = nbRoots;
        } else {
            nbClusts = pwroots_approximate_mpz_poly_mpz_cb ( roots, mults, reals, &nbreals,
                                                            pmpz_re, len_re, log2eps, dom_num, dom_den, goalNbRoots,
                                                            m_check_interrupt);
            
            for (slong i=0; i<nbClusts; i++) {
                nbRoots += mults[i];
            }
            printf(" nb of   clusters: %ld\n", nbClusts);
            printf(" nb of      roots: %ld\n", nbRoots);
            printf(" nb of real roots: %ld\n", nbreals);
        }
    } else {
        if (PW_GOAL_MUST_REFINE(goal)){
            
            nbClusts = pwroots_refine_comp_mpz_poly_mpz_cb ( roots, mults,
                                                             pmpz_re, len_re, pmpz_im, len_im, 
                                                             log2eps, dom_num, dom_den, goalNbRoots,
                                                             m_check_interrupt);
            for (slong i=0; i<nbClusts; i++) {
                nbRoots += mults[i];
            }
            printf(" nb of   clusters: %ld\n", nbClusts);
            printf(" nb of      roots: %ld\n", nbRoots);
            
        } else if PW_GOAL_MUST_ISOLATE(goal) {
        
            nbRoots = pwroots_isolate_comp_mpz_poly_mpz_cb ( roots, 
                                                             pmpz_re, len_re, pmpz_im, len_im,
                                                             log2eps, dom_num, dom_den, goalNbRoots,
                                                             m_check_interrupt);
            for (slong i=0; i<nbRoots; i++)
                mults[i]=1;
            printf(" nb of      roots: %ld\n", nbRoots);
        } else {
            nbClusts = pwroots_approximate_comp_mpz_poly_mpz_cb ( roots, mults,
                                                                  pmpz_re, len_re, pmpz_im, len_im, 
                                                                  log2eps, dom_num, dom_den, goalNbRoots,
                                                                  m_check_interrupt);
            
            for (slong i=0; i<nbClusts; i++) {
                nbRoots += mults[i];
            }
            printf(" nb of   clusters: %ld\n", nbClusts);
            printf(" nb of      roots: %ld\n", nbRoots);
        }
    }
    
    acb_ptr roots_acb = _acb_vec_init(nbClusts);
    for (slong i=0; i<nbClusts; i++)
        mpz_cb_get_acb( roots_acb+i, roots + i );
    
    
    FILE * outFile = NULL;
    char outFileName[] = "roots.txt\0";
    outFile = fopen (outFileName,"w");
    pwpoly_write_roots_mults( outFile, roots_acb, mults, nbClusts, 10 );
    if (outFile != NULL)
        fclose (outFile);
    
    _acb_vec_clear(roots_acb, nbClusts);
    
    FILE * rootsFile;
    char rootsFileName[] = "roots.plt\0";
    rootsFile = fopen (rootsFileName,"w");
    mpz_cb_vec_gnuplot( rootsFile, rootsFile, roots, nbClusts );
    if (rootsFile != NULL)
        fclose (rootsFile);
    
    for (slong i=0; i<4; i++) {
        mpz_clear(dom_num+i);
        mpz_clear(dom_den+i);
    }
    free(mults);
    free(reals);
    for (slong i=0; i<(length-1); i++)
        mpz_cb_clear( roots+i );
    free(roots);
    for (slong i=0; i<length; i++)
        mpz_clear( pmpz_re+i );
    free(pmpz_re);
    if (pmpz_im!=NULL) {
        for (slong i=0; i<length; i++)
            mpz_clear( pmpz_im+i );
        free(pmpz_im);
    }
    clear_global_variables();
    flint_cleanup();
    
    pwmm_cleanup(default_pwmm_table);
    
    return 0;
}

slong mpz_vec_set_fmpq_poly( mpz_ptr coeffs, const fmpq_poly_t src ) {
    fmpz_t coeff;
    fmpz_init(coeff);
    slong len = fmpq_poly_length(src);
    for ( slong i=0; i<len; i++ ) {
        fmpz_mul(coeff, fmpq_poly_numref(src)+i, fmpq_poly_denref(src) );
        fmpz_get_mpz( coeffs+i, coeff );
    }
    fmpz_clear(coeff);
    return len;
}

void mpz_vec_fprint( FILE * file, const mpz_ptr vec, slong len ){
    fprintf( file, "length: %ld, elements:  ", len );
    for ( slong i=0; i<len; i++ ) {
        mpz_out_str( file, 10, vec+i );
        printf(" ");
    }
}

void domain_get_mpz_tab( mpz_struct domain_num[], mpz_struct domain_den[], const domain_t dom ) {
    if (domain_is_lre_finite( dom )){
        fmpz_get_mpz( domain_num+0, fmpq_numref( domain_lreref(dom) ) );
        fmpz_get_mpz( domain_den+0, fmpq_denref( domain_lreref(dom) ) );
    } else {
        mpz_set_ui(domain_num+0, 0);
        mpz_set_ui(domain_den+0, 0);
    }
    if (domain_is_ure_finite( dom )){
        fmpz_get_mpz( domain_num+1, fmpq_numref( domain_ureref(dom) ) );
        fmpz_get_mpz( domain_den+1, fmpq_denref( domain_ureref(dom) ) );
    } else {
        mpz_set_ui(domain_num+1, 0);
        mpz_set_ui(domain_den+1, 0);
    }
    if (domain_is_lim_finite( dom )){
        fmpz_get_mpz( domain_num+2, fmpq_numref( domain_limref(dom) ) );
        fmpz_get_mpz( domain_den+2, fmpq_denref( domain_limref(dom) ) );
    } else {
        mpz_set_ui(domain_num+2, 0);
        mpz_set_ui(domain_den+2, 0);
    }
    if (domain_is_uim_finite( dom )){
        fmpz_get_mpz( domain_num+3, fmpq_numref( domain_uimref(dom) ) );
        fmpz_get_mpz( domain_den+3, fmpq_denref( domain_uimref(dom) ) );
    } else {
        mpz_set_ui(domain_num+3, 0);
        mpz_set_ui(domain_den+3, 0);
    }
}

void mpz_domain_fprint( FILE * file, const mpz_struct domain_num[], const mpz_struct domain_den[] ) {
    domain_t dom;
    domain_init(dom);
    domain_set_mpz_tab( dom, domain_num, domain_den );
    domain_fprint( file, dom );
    domain_clear(dom);
}

// #ifdef PW_NO_INTERFACE
// void mpz_cb_gnuplot(FILE *file, const mpz_cb_t b){
//     fprintf(file, "%lf", mpz_2exp_mpz_get_d ( mpz_cb_rcmref(b), mpz_cb_rceref(b) ));
//     fprintf(file, "   ");
//     fprintf(file, "%lf", mpz_2exp_mpz_get_d ( mpz_cb_icmref(b), mpz_cb_iceref(b) ));
//     fprintf(file, "   ");
//     fprintf(file, "%lf", mpz_2exp_mpz_get_d ( mpz_cb_rrmref(b), mpz_cb_rreref(b) ));
//     fprintf(file, "   ");
//     fprintf(file, "%lf", mpz_2exp_mpz_get_d ( mpz_cb_irmref(b), mpz_cb_ireref(b) ));
// }
// 
// void mpz_cb_vec_gnuplot(FILE *fpreamb, FILE *fdata, mpz_cb_srcptr l, const slong len){
//     fprintf(fpreamb, "plot '-' title 'approximated roots' with xyerrorbars lc rgb \"#008080\" \n");
//     for(int i=0; i < len; i++) {
//         mpz_cb_gnuplot(fdata, l + i);
//         fprintf(fdata, "\n");
//     }
//     fprintf(fdata, "e\n");
//     fprintf(fdata, "pause mouse close\n");
// }
// 
// #endif
