#include "pw_base.h"
#include "pw_sector_list.h"

int main(int argc, char* argv[]) {
    
    pw_sector_list_t l;
    pw_sector_list_init(l);
    printf("empty list: ");
    pw_sector_list_print(l);
    printf("\n");
    
    printf("insert (2,1): ");
    pw_sector_list_insert_sorted_unique ( l, 2, 1, 4 );
    pw_sector_list_print(l);
    printf("\n");
    
    printf("insert (2,1) (already in): ");
    pw_sector_list_insert_sorted_unique ( l, 2, 1, 4 );
    pw_sector_list_print(l);
    printf("\n");
    
    printf("insert (1,0): ");
    pw_sector_list_insert_sorted_unique ( l, 1, 0, 4 );
    pw_sector_list_print(l);
    printf("\n");
    
    printf("insert (1,2): ");
    pw_sector_list_insert_sorted_unique ( l, 1, 2, 4 );
    pw_sector_list_print(l);
    printf("\n");
    
    printf("insert (1,1): ");
    pw_sector_list_insert_sorted_unique ( l, 1, 1, 4 );
    pw_sector_list_print(l);
    printf("\n");
    
    printf("insert (1,2): (already in)");
    pw_sector_list_insert_sorted_unique ( l, 1, 2, 4 );
    pw_sector_list_print(l);
    printf("\n");
    
    printf("insert (1,-1): (=(1,3))");
    pw_sector_list_insert_sorted_unique ( l, 1, -1, 4 );
    pw_sector_list_print(l);
    printf("\n");
    
    printf("insert (1,-1): (already in)");
    pw_sector_list_insert_sorted_unique ( l, 1, -1, 4 );
    pw_sector_list_print(l);
    printf("\n");
    
    printf("insert (1,0) (already in): ");
    pw_sector_list_insert_sorted_unique ( l, 1, 0, 4 );
    pw_sector_list_print(l);
    printf("\n");
    
    printf("insert (2,-1) : ");
    pw_sector_list_insert_sorted_unique ( l, 2, -1, 4 );
    pw_sector_list_print(l);
    printf("\n");
    
    printf("is (1,0)  (begin)         in list : %d\n", pw_sector_is_in_sorted_sector_list ( 1,  0, 4, l ));
    printf("is (1,-1) (last inserted) in list : %d\n", pw_sector_is_in_sorted_sector_list ( 1, -1, 4, l ));
    printf("is (2,1)  (last )         in list : %d\n", pw_sector_is_in_sorted_sector_list ( 2,  1, 4, l ));
    printf("is (1,-2) (=(1,2))        in list : %d\n", pw_sector_is_in_sorted_sector_list ( 1, -2, 4, l ));
    printf("is (0,0)                  in list : %d\n", pw_sector_is_in_sorted_sector_list ( 0,  0, 4, l ));
    printf("is (2,2)                  in list : %d\n", pw_sector_is_in_sorted_sector_list ( 2,  2, 4, l ));
    printf("is (3,1)                  in list : %d\n", pw_sector_is_in_sorted_sector_list ( 3,  1, 4, l ));
    
    pw_sector_list_clear(l);
    
    return 0;
}
