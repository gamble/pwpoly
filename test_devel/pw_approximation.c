#include "pw_base.h"
#include "common/pw_common.h"
#include "covering/pw_covering.h"
#include "approximation/pw_approximation.h"
#include <stdio.h>

int main(int argc, char **argv){
    
    if (argc<1) {
        printf(" !no input file! \n");
        return 0;
    }
    
    slong len=0;
    acb_poly_t pacb;
    fmpq_poly_t pfmpq_re;
    fmpq_poly_t pfmpq_im;
    
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    
    int type = parseInputFile( &len, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if (type==PW_PARSE_FAILD){
        acb_poly_clear(pacb);
        fmpq_poly_clear(pfmpq_re);
        fmpq_poly_clear(pfmpq_im);
        flint_cleanup();
        return 0;
    }
    
    slong m = 16;
    if (argc>2)
        sscanf(argv[2], "%ld", &m);
    printf("m: %ld\n", m);
    slong prec = PWPOLY_MAX( 2*m, PWPOLY_DEFAULT_PREC );
    
    if (type==PW_PARSE_EXACT) {
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, prec);
    } else if (type==PW_PARSE_BALLS) {
    }
    
    /* Print the converted polynomial */
//     printf("%ld-bit floating point approximation of input polynomial\n", prec);
//     printf("=================================================================\n");
//     acb_poly_printd(pacb, 10);
//     printf("\n");
//     printf("=================================================================\n");
    
    
//     int c=2;
//     int c=4;
    int c=16;
    fmpq_t a, b;
    fmpq_init(a);
    fmpq_init(b);
    fmpq_set_si(a, 3, 2);
//     fmpq_set_si(b, 2, 5);
//     fmpq_set_si(b, 1, 1); // for c=4 and m>=5
    fmpq_set_si(b, 7, 2); // for c=16 and m>=2
    
#ifdef PW_PROFILE
    PW_INIT_PROFILE
#endif
    
    pw_approximation_t app;
    pw_approximation_init_acb_poly( app, pacb, m, (ulong)c, a, b);
    
    pw_approximation_print_short( app );
    
    pw_approximation_compute_approx_annulus( app, 0, 1 );
    
    pw_approximation_compute_approx_annulus( app, 1, 1 );
    
    pw_approximation_compute_approx_annulii( app, 1 );
    
//     pw_approximation_print( app );
    pw_approximation_print_short( app );
    
    FILE * CHFile;
    char CHFileName[] = "CH.plt\0";
    CHFile = fopen (CHFileName,"w");
    _pw_covering_compute_annulii_from_fmpq_weights_gnuplot( CHFile, 1, pw_approximation_coveringref(app), pw_approximation_exponentsref(app) );
    fclose (CHFile);
    
    FILE * covFile;
    char covFileName[] = "covering.plt\0";
    covFile = fopen (covFileName,"w");
    _pw_covering_gnuplot( covFile, pw_approximation_coveringref(app));
    fclose (covFile);
    
    pw_approximation_clear(app);
    
#ifdef PW_PROFILE
    PW_PRINT_PROFILE
#endif    
    
    
    fmpq_clear(a);
    fmpq_clear(b);
    
    acb_poly_clear(pacb);
    fmpq_poly_clear(pfmpq_re);
    fmpq_poly_clear(pfmpq_im);
    flint_cleanup();
    
    return 0;
}
