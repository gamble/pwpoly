/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of PWPoly.

    PWPoly is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    PWPoly is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with PWPoly. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "pw_base.h"
#include "common/pw_common.h"
#include "covering/pw_covering.h"
#include "approximation/pw_approximation.h"
#include "solving/pw_solving.h"
#include "geometry/domain.h"
#include <stdio.h>
#include <time.h>

#include "pw_mm.h"

pwmm_table_t gmp_pwmm_table;
pwmm_table_t flint_pwmm_table;

/* malloc functions */
static __inline__ void * m_malloc (size_t size) { 
    return (void *) pwmm_malloc( (PWMM_SIZ) size, current_pwmm_table ); 
}

static __inline__ void * m_malloc_for_gmp (size_t size) { 
    return (void *) pwmm_malloc( (PWMM_SIZ) size, gmp_pwmm_table ); 
}

static __inline__ void * m_malloc_for_flint (size_t size) { 
//     return (void *) pwmm_malloc( (PWMM_SIZ) size, flint_pwmm_table ); 
    return (void *) pwmm_malloc( (PWMM_SIZ) size, gmp_pwmm_table );
}
/* realloc functions */
static __inline__ void * m_realloc (void * old_ptr, size_t new_size) {
    return (void *) pwmm_realloc( (PWMM_PTR) old_ptr, (PWMM_SIZ) new_size, current_pwmm_table );
}

static __inline__ void * m_realloc_for_gmp (void * old_ptr, size_t oldsize, size_t new_size) { 
    return (void *) pwmm_realloc( (PWMM_PTR) old_ptr, (PWMM_SIZ) new_size, gmp_pwmm_table );
}

static __inline__ void * m_realloc_for_flint (void * old_ptr, size_t new_size) {
//     return (void *) pwmm_realloc( (PWMM_PTR) old_ptr, (PWMM_SIZ) new_size, flint_pwmm_table );
    return (void *) pwmm_realloc( (PWMM_PTR) old_ptr, (PWMM_SIZ) new_size, gmp_pwmm_table );
}
/* calloc functions */
static __inline__ void * m_calloc (size_t num, size_t size) {
    return (void *) pwmm_calloc( (PWMM_SIZ) num, (PWMM_SIZ) size, current_pwmm_table );
}

static __inline__ void * m_calloc_for_flint (size_t num, size_t size) { 
//     return (void *) pwmm_calloc( (PWMM_SIZ) num, (PWMM_SIZ) size, flint_pwmm_table );
    return (void *) pwmm_calloc( (PWMM_SIZ) num, (PWMM_SIZ) size, gmp_pwmm_table );
}
/* free functions */
static __inline__ void   m_free (void * ptr) {
    pwmm_free( (PWMM_PTR) ptr, current_pwmm_table ); 
}

static __inline__ void   m_free_for_flint (void * ptr) { 
//     pwmm_free( (PWMM_PTR) ptr, flint_pwmm_table );
    pwmm_free( (PWMM_PTR) ptr, gmp_pwmm_table ); 
}

static __inline__ void   m_free_for_gmp (void * ptr, size_t old_size) {
    pwmm_free( (PWMM_PTR) ptr, gmp_pwmm_table );
}

static __inline__ void   m_cleanup(void) {
    pwmm_cleanup(current_pwmm_table);
}

acb_poly_t pacb;
fmpq_poly_t pfmpq_re;
fmpq_poly_t pfmpq_im;
slong length;
int c;
fmpq_t scale, b;
int verbosity, increase, check, goal, solver;
slong m, log2eps, roundInput, goalNbRoots;
domain_t dom;

void init_global_variables(){
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    length = 0;
    c      = 2;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
//     fmpq_set_si(b, 1, 10);
    verbosity = 1;
    m = 10;
    increase = 1;
    check = 0;
    log2eps = -53;
    goal = PW_GOAL_ISOLATE;
    solver = 63;
    roundInput = 0;
    domain_init(dom); /* by default set to C */
    goalNbRoots = -1; /* find all the roots */
}

void clear_global_variables(){
    domain_clear(dom);
    fmpq_clear(b);
    fmpq_clear(scale);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(pfmpq_re);
    acb_poly_clear(pacb);
}


int main(int argc, char* argv[]) {
    
    pwmm_table_init( default_pwmm_table );
    current_pwmm_table=default_pwmm_table;
    
    pwmm_table_init( flint_pwmm_table );
    pwmm_table_init( gmp_pwmm_table );
    
    mp_set_memory_functions(  m_malloc_for_gmp, m_realloc_for_gmp, m_free_for_gmp );
//     __flint_set_memory_functions( m_malloc, m_calloc, m_realloc, m_free );
    __flint_set_memory_functions( m_malloc_for_flint, m_calloc_for_flint, m_realloc_for_flint, m_free_for_flint );
//     __pwpoly_set_memory_functions( m_malloc, m_realloc, m_free, m_cleanup );
    
#ifdef PWMM_STATS
    PWMM_INIT_PROFILE
#endif
    
    int level = 1;
    
    if ( (argc<2) || (strcmp( argv[1], "-h" ) == 0) 
                  || (strcmp( argv[1], "--help" ) == 0) ) {
        pwroots_fprint_options( stdout, argc, argv );
        pwmm_cleanup(gmp_pwmm_table);
        pwmm_cleanup(flint_pwmm_table);
        pwmm_cleanup(default_pwmm_table);
        exit(0);
    }
    
    init_global_variables();
    int parse = parseInputArgs( argc, argv, &verbosity, dom, &goalNbRoots, &m, &increase, &check, &log2eps, &goal, &solver, &roundInput, b );
    
    /* type is either PW_PARSE_FAILD, PW_PARSE_EXACT for integer/rational polynomials */
    /*             or PW_PARSE_BALLS for acb poly                                     */
    /* length is the degree+1 of the input poly                                       */
    /* fills pacb if PW_PARSE_BALLS, pfmpq_re and pfmpq_im if PW_PARSE_EXACT          */
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if ( (type==PW_PARSE_FAILD) || (parse==0) ){
        clear_global_variables();
        flint_cleanup();
        pwmm_cleanup(gmp_pwmm_table);
        pwmm_cleanup(flint_pwmm_table);
        pwmm_cleanup(default_pwmm_table);
        exit(0);
    }
    
    if (verbosity>=level) {
        printf("m          : %ld\n", m);
        printf("increase   : %d\n", increase);
        printf("goal       : "); pwroots_fprint_goal(stdout, goal); printf("\n");
        printf("b          : "); fmpq_print(b); printf("\n");
        printf("solver     : %d\n", solver);
        printf("roundInput : %ld\n", roundInput);
        printf("check      : %d\n", check);
        printf("verbosity  : %d\n", verbosity);
        printf("domain     : "); domain_print_short(dom); printf("\n"); 
        printf("goalNbRoots: %ld\n", goalNbRoots);
    }
    
#ifdef PW_PROFILE
    PW_INIT_PROFILE
#endif

#ifdef PW_COCO_PROFILE
    PW_COCO_INIT_PROFILE
#endif

#ifdef PW_EA_PROFILE
    PW_EA_INIT_PROFILE
#endif
    
    if ((type==PW_PARSE_EXACT)&&(roundInput>0)) {
        type = PW_PARSE_BALLS;
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, roundInput );
        for (slong i=0; i<pacb->length; i++) {
            acb_get_mid( (pacb->coeffs) + i, (pacb->coeffs) + i );
        }
    }
    
    FILE * covrFile;
    char covrFileName[] = "covering_with_roots.plt\0";
    covrFile = fopen (covrFileName,"w");
    
    FILE * rootsFile;
    char rootsFileName[] = "roots.plt\0";
    rootsFile = fopen (rootsFileName,"w");
    
    clock_t start_isolate = clock();
    
    /* find the roots */
    acb_ptr roots = _acb_vec_init(length-1);
    slong * mults = (slong *) pwpoly_malloc ( (length-1)*sizeof(slong) );
    
    slong nbroots = 0, nbclusts =0;
    ulong wm = (ulong)m;
    if (type==PW_PARSE_EXACT){
        if (increase)
            nbclusts = pwpoly_solve_2fmpq_poly( roots, mults, &wm, pfmpq_re, pfmpq_im, goal, log2eps, dom, goalNbRoots, (ulong)c, scale, b, solver, verbosity, covrFile );
        else
            nbroots  = pwpoly_isolate_m_2fmpq_poly( roots, pfmpq_re, pfmpq_im, wm, (ulong)c, scale, b, dom, goalNbRoots, solver, verbosity, covrFile );
    } else {
        if (increase)
            nbclusts = pwpoly_solve_acb_poly( roots, mults, &wm, pacb, goal, log2eps, dom, goalNbRoots, (ulong)c, scale, b, solver, verbosity, covrFile );
        else
            nbroots  = pwpoly_isolate_m_acb_poly( roots, pacb, wm, (ulong)c, scale, b, dom, goalNbRoots, solver, verbosity, covrFile );
    }
    
    double clicks_in_isolate = (clock() - start_isolate);
    
    if (increase) {
        for (slong i=0; i<nbclusts; i++)
            nbroots += mults[i];
    } else {
        nbclusts = nbroots;
        for (slong i=0; i<length-1; i++)
            mults[i] = 1;
    }
        
    /* print solving info */
    if (verbosity>=level) {
        printf(" time for isolating the roots    : %lf seconds\n",
             clicks_in_isolate/CLOCKS_PER_SEC) ;
        printf(" total number of clusters        : %ld\n", nbclusts);
        printf(" total number of roots           : %ld\n", nbroots);
        printf(" final m                         : %lu\n", wm);   
    }

#ifdef PW_PROFILE
    PW_PRINT_PROFILE
#endif

#ifdef PW_COCO_PROFILE
    PW_COCO_PRINT_PROFILE
#endif

#ifdef PW_EA_PROFILE
    PW_EA_PRINT_PROFILE
#endif

#ifdef PWMM_STATS
    PWMM_PRINT_PROFILE
#endif

    pw_roots_domain_gnuplot( rootsFile, roots, nbclusts, dom );
    fclose (rootsFile);
    fclose (covrFile);

    /* check the output */
    if (check) {
        
        clock_t start_check = clock();
        int checkOK = 0;
          
        if (type==PW_PARSE_EXACT){
            checkOK = pwpoly_checkOutput_solve_2fmpq_poly ( roots, mults, nbclusts, pfmpq_re, pfmpq_im, log2eps, goal, dom, (slong) goalNbRoots );
        } else {
            checkOK = pwpoly_checkOutput_solve_exact_acb_poly ( roots, mults, nbclusts, pacb, log2eps, goal, dom, (slong) goalNbRoots );
        }

        double clicks_in_check = (clock() - start_check);
        printf(" check output                    : %d\n", checkOK);
        printf(" time for checking output        : %lf seconds\n",
                 clicks_in_check/CLOCKS_PER_SEC) ;
    } else {
        if (verbosity>=level) {
            printf(" check output                    : NOT CHECKED\n");
        }
    }
    
    pwpoly_free(mults);
    _acb_vec_clear(roots, length-1);
    clear_global_variables();
    
    flint_cleanup();
    m_cleanup();
    pwmm_table_clear( current_pwmm_table );
    pwmm_cleanup(gmp_pwmm_table);
    pwmm_cleanup(flint_pwmm_table);
    pwmm_table_clear( flint_pwmm_table );
    pwmm_table_clear( gmp_pwmm_table );
    
    return 0;
}

