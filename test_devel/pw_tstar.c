#include "pw_base.h"
#include "common/pw_common.h"
// #include "solvers/cache_poly.h"
#include "polynomial/pw_polynomial.h"
#include "solvers/tstar.h"

acb_poly_t pacb;
fmpq_poly_t pfmpq_re;
fmpq_poly_t pfmpq_im;
slong length;

void init_global_variables(){
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    length = 0;
}

void clear_global_variables(){
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(pfmpq_re);
    acb_poly_clear(pacb);
}

void getApprox(acb_poly_t dest, slong prec){
    acb_poly_set2_fmpq_poly(dest, pfmpq_im, pfmpq_re, prec);
}

void getApproxA(acb_poly_t dest, slong prec){
    acb_poly_set_round(dest, pacb, prec);
}

void double_poly_print( const double * coeffs, const double * abers, slong len ) {
    printf("[");
    for (slong i = 0; i < len; i++) {
        printf("(%f + %fj) +/- %e\n", coeffs[2*i], coeffs[2*i+1], abers[i] );
    }
    printf("]");
}

int main(int argc, char* argv[]) {
    
    if (argc<2) {
        printf("Usage: %s filename\n", argv[0]);
        exit(0);
    }
    
    init_global_variables();
    slong prec = PWPOLY_DEFAULT_PREC;
    
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if (type==PW_PARSE_FAILD){
        clear_global_variables();
        flint_cleanup();
        exit(0);
    }
    
    pw_polynomial_t poly;
    
    if (type==PW_PARSE_EXACT) {
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, prec);
    } else {
    }
    
    acb_poly_t fact, prod;
    acb_poly_init(fact);
    acb_poly_init(prod);
    acb_poly_fit_length(fact, 2);
    _acb_poly_set_length(fact, 2);
    acb_one( fact->coeffs + 1);
    acb_set_d_d( fact->coeffs + 0, -10., -10. );
    acb_poly_printd(fact, 10); printf("\n");
    acb_poly_mul( prod, pacb, fact, prec );
    acb_poly_set(pacb, prod);
    acb_poly_clear(prod);
    acb_poly_clear(fact);
    
    pw_polynomial_init_from_approx( poly, pacb, prec);
    printf("Cache initialized with 1 acb_poly of deg %ld, precision %ld\n", pacb->length-1, prec);
    
    acb_poly_srcptr papprox = pw_polynomial_cacheANDgetApproximation( poly, prec);
    
    printf("Poly approximated to precision %ld: \n", prec);
    acb_poly_printd(papprox, 20);
    printf("\n");
    
    acb_t center;
    acb_init(center);
    arb_t radius;
    arb_init(radius);
    acb_set_d_d(center, 10., 10.);
    arb_set_d(radius, 1e-5);
    slong nbroots = tstar_acb_arb( &prec, center, radius, poly, pacb->length-1, 0, 0, 0);
    
    printf("res of tstar test: %ld\n", nbroots );
    
    arb_clear(radius);
    acb_clear(center);
    
//     int fits = cache_poly_compute_double_approximation( poly );
//     
//     if (fits) {
//     
//         printf("Poly approximated to double precision floating point numbers: \n");
//         double_poly_print( cache_poly_Dcoeffref(poly), cache_poly_Dabersref(poly), cache_poly_lengthref(poly) );
//         printf("\n");
//         
//     } else {
//         printf("Poly does not fit in double precision floating points numbers!\n");
//     }
    
    pw_polynomial_clear(poly);
    
    clear_global_variables();
    
    flint_cleanup();
    
    return 0;
}
