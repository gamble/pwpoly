#include "pw_base.h"
#include "pw_list.h"

void int_clear(void * i){
//     hefpoly_free ( i );
}

void int_fprint(FILE * file, const void * i){
    fprintf(file, "%d", *( (int *) i) );
}
    
int int_isless(const void * d1, const void * d2){
    return ( *((int *) d1) <= *((int *) d2) );
}

int main(int argc, char* argv[]) {
    
    list_t l;
//     list_init(l, int_clear);
    list_init(l);
    
    list_fprint(stdout, l, int_fprint); printf("\n");
    
//     int *one   = (int *) hefpoly_malloc ( sizeof(int) );
//     int *two   = (int *) hefpoly_malloc ( sizeof(int) );
//     int *three = (int *) hefpoly_malloc ( sizeof(int) );
//     int *four  = (int *) hefpoly_malloc ( sizeof(int) );
//     int *five  = (int *) hefpoly_malloc ( sizeof(int) );
//     int *six   = (int *) hefpoly_malloc ( sizeof(int) );
    int one   = 1;
    int two   = 2;
    int three = 3;
    int four  = 4;
    int five  = 5;
    int six   = 6;
    
    list_push(l, &six);
    list_fprint(stdout, l, int_fprint); printf("\n");
    
    list_insert_sorted(l, &one, int_isless);
    list_fprint(stdout, l, int_fprint); printf("\n");
    
    list_insert_sorted(l, &three, int_isless);
    list_insert_sorted(l, &two, int_isless);
    list_insert_sorted(l, &four, int_isless);
    list_fprint(stdout, l, int_fprint); printf("\n");
    
    int * poped = list_pop(l);
    printf("poped element: %d\n", *poped);
    
    list_empty(l);
    
    list_t l2;
//     list_init(l2, int_clear);
    list_init(l2);
    
    list_push(l, &four);
    list_push(l, &five);
    
    list_push(l2, &one);
    list_push(l2, &two);
    
    printf("l before append:\n");
    list_fprint(stdout, l, int_fprint); printf("\n");
    printf("l2 before append:\n");
    list_fprint(stdout, l2, int_fprint); printf("\n");
    
    list_append(l, l2);
    printf("l after append:\n");
    list_fprint(stdout, l, int_fprint); printf("\n");
    printf("l2 after append:\n");
    list_fprint(stdout, l2, int_fprint); printf("\n");
    
    list_empty(l);
    list_empty(l2);
    
    flint_cleanup();
    return 0;
    
}
