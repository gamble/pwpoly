#include "pw_base.h"
#include "common/pw_common.h"
#include "covering/pw_covering.h"
#include "approximation/pw_approximation.h"
#include "solving/pw_solving.h"
#include <stdio.h>
#include <time.h>

acb_poly_t pacb;
fmpq_poly_t pfmpq_re;
fmpq_poly_t pfmpq_im;
slong length;
int type;
int c;
fmpq_t scale, b;
int verbosity, m, increase, check, log2eps, goal, solver, roundInput, goalNbRoots;
domain_t dom;

void init_global_variables(){
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    length = 0;
    c      = 2;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 2, 5);
    verbosity = 1;
    m = 30;
    increase = 1;
    check = 0;
    log2eps = 0;
    goal = PW_GOAL_ISOLATE;
    solver = 2;
    roundInput = 0;
    domain_init(dom); /* by default set to C */
    goalNbRoots = -1; /* find all the roots */
}

void clear_global_variables(){
    domain_clear(dom);
    fmpq_clear(b);
    fmpq_clear(scale);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(pfmpq_re);
    acb_poly_clear(pacb);
}

int be_poly_intersect_acb_poly( double * maxdist, double * bepoly, acb_srcptr acbpoly, slong len, slong prec ) {
    acb_t temp;
    acb_init(temp);
    arb_t dist, abs1, abs2;
    arb_init(dist);
    arb_init(abs1);
    arb_init(abs2);
//     *maxdist=0;
    int res = 1;
    for (slong i=0; (i<len)&&res; i++) {
        _be_get_acb( temp, bepoly[3*i], bepoly[3*i+1], bepoly[3*i+2] ); 
        
//         int de = _pwpoly_test_and_print_exception("be_poly_intersect_acb_poly 0 ", 1);
//         if (de) {
//             printf("i: %ld\n", i);
//         }
        
        res = res&&acb_overlaps(temp, acbpoly+i);
        
//         de = _pwpoly_test_and_print_exception("be_poly_intersect_acb_poly 1 ", 1);
//         if (de) {
//             printf("i: %ld\n", i);
//         }
        
        if (res==0) {
            printf("i: %ld / %ld\n", i, len-1);
            printf("%.16e, %.16e, %.16e\n", bepoly[3*i], bepoly[3*i+1], bepoly[3*i+2]);
            acb_printd(temp, 50); printf("\n");
            acb_printd(acbpoly+i, 50); printf("\n");
        }
        acb_abs(abs1, temp, prec);
        acb_abs(abs2, acbpoly+i, prec);
        arb_min(abs1, abs1, abs2, prec);
        acb_sub(temp, temp, acbpoly+i, prec);
        acb_abs(dist, temp, prec);
        arb_div(dist, dist, abs1, prec);
        
//         de = _pwpoly_test_and_print_exception("be_poly_intersect_acb_poly 2 ", 1);
//         if (de) {
//             printf("i: %ld\n", i);
//         }
        
        *maxdist = PWPOLY_MAX( *maxdist, arf_get_d( arb_midref(dist), ARF_RND_UP ) );
        if (res==0) {
            printf("rel dist: %.10e\n", arf_get_d( arb_midref(dist), ARF_RND_UP ) );
            printf("\n");
        }
        
//         de = _pwpoly_test_and_print_exception("be_poly_intersect_acb_poly 3 ", 1);
//         if (de) {
//             printf("i: %ld\n", i);
//             _be_get_acb( temp, bepoly[3*i], bepoly[3*i+1], bepoly[3*i+2] ); 
//             printf("conv: "); acb_printd(temp, 10); printf("\n");
//             printf("comp: "); acb_printd(acbpoly+i, 10); printf("\n");
//             acb_abs(abs1, temp, prec);
//             acb_abs(abs2, acbpoly+i, prec);
//             arb_min(abs1, abs1, abs2, prec);
//             acb_sub(temp, temp, acbpoly+i, prec);
//             acb_abs(dist, temp, prec);
//             printf("dist: "); arb_printd(dist, 10); printf("\n");
//             printf("abs1: "); arb_printd(abs1, 10); printf("\n");
//             arb_div(dist, dist, abs1, prec);
//             printf("dist: "); arb_printd(dist, 10); printf("\n");
//         }
        
    }
    
    arb_clear(abs2);
    arb_clear(abs1);
    arb_clear(dist);
    acb_clear(temp);
    
    return res;
}

int pw_solving_be_poly_oneDLGIteration_naive( double * dest, double * src, slong len, int verbose ) {
    
    int level = 4;
    int res = 1;
    /* save exception flags and re-init exceptions */
    fexcept_t except_save;
    fegetexceptflag (&except_save, FE_ALL_EXCEPT);
    feclearexcept (FE_ALL_EXCEPT);
    
    /* save rounding mode and set double rounding mode to NEAREST */
    int rounding_save = fegetround();
    int ressetnearest = fesetround (FE_TONEAREST);
    if (ressetnearest!=0) {
        res = PWPOLY_SET_ROUND_NEAREST_FAILED;
        if (verbose>=level)
            printf("pw_solving_be_poly_oneDLGIteration_naive: setting rounding to nearest failed!!!\n");
    }
    
    _be_poly_oneDLGIteration_naive( dest, src, len );
    
    int double_exception = _pwpoly_test_and_print_exception("pw_solving_be_poly_oneDLGIteration_naive: ", verbose);
    if (double_exception) {
        res = PWPOLY_DOUBLE_EXCEPTION;
        if (verbose>=level)
            printf("pw_solving_be_poly_oneDLGIteration_naive: double exception!!!\n");
    }
    
    /* restore exception flags and rounding mode */
    fesetexceptflag (&except_save, FE_ALL_EXCEPT);
    fesetround (rounding_save);
    
    return res;
}

int pwpoly_test_be_DLG(double *maxdist, int * nbdiff, int * nbexcept, const pw_approximation_t app, int verbose ){
    
    int level = 1;
    
    ulong N=pw_approximation_Nref(app);
    slong prec = pw_approximation_precref(app);
//     ulong m=pw_approximation_output__mref(app);
//     ulong lenInitial=pw_approximation_lenref(app);
    int realCoeffs = pw_approximation_realCoeffsref(app);
    
    acb_poly_t polyMiddle, polyMiddleDLG;
    acb_poly_init(polyMiddle);
    acb_poly_init(polyMiddleDLG);
    
    acb_t coeff;
    acb_init(coeff);
    
    int NG=0;
    
    int overlaps=1;
    *maxdist=0;
                
    /* loop on annulii */
    for (ulong n=0; (n<N)&&(overlaps==1); n++) {
        ulong K = pw_approximation_nbsectref(app)[n];
        if (verbose>=level) {
            printf("solving on %lu-th annulus: %lu sectors\n", n, K);
        }
        
        /* loop on sectors */
        ulong lastk = K-1;
        if ( realCoeffs && (K>1) )
            lastk = K/2;
        for (ulong k=0; (k<=lastk)&&(overlaps==1); k++) {
            /* get approximation */
            acb_srcptr coeffs;
            ulong lenapprox = pw_approximation_get_approx(&coeffs, app, n, k );
            _pwpoly_middle_almost_monic_acb_poly( polyMiddle, coeffs, lenapprox, prec );
            acb_poly_set(polyMiddleDLG, polyMiddle);
            
            double * poly = (double *) pwpoly_malloc ( 3*lenapprox*sizeof(double) );
            double * polyDLG = (double *) pwpoly_malloc ( 3*lenapprox*sizeof(double) );
            
            slong nbRootsPellet = TSTAR_CAN_NOT_DECIDE;
            if ((n>0) && (n<N-1) && (K>1)) {
                /* if the current annulus is divided in sectors, apply Pellet test to see if there are roots */

                int res = 1;
                /* try to convert acb poly in be poly */
                res = pwpoly_acb_poly_to_be_poly( poly, polyMiddle->coeffs, lenapprox, verbose );
                for (ulong j=0; j<lenapprox; j++) {
                    polyDLG[3*j]   = poly[3*j];
                    polyDLG[3*j+1] = poly[3*j+1];
                    polyDLG[3*j+2] = poly[3*j+2];
                }
                if (res!=1) {
                    (*nbexcept)++;
                    if (verbose>=level)
                        printf("pwpoly_test_be_DLG: converting acb poly to be poly failed: %d\n", res);
                }
                
                if (res>0) {
                    res = pw_solving_be_poly_oneDLGIteration_naive( polyDLG, poly, lenapprox, verbose );
                }
                
                if (res!=1) {
                    (*nbexcept)++;
                    if (verbose>=level)
                        printf("pwpoly_test_be_DLG: double exception while computing one DLG iteration!!!\n");
                }
                
                if (res>0) {
                    slong lprec = PWPOLY_MAX( PWPOLY_DEFAULT_PREC, prec/4);
                    _tstar_oneGraeffeIteration_inplace( polyMiddleDLG, lprec);
                    overlaps = overlaps&&be_poly_intersect_acb_poly(maxdist, polyDLG, polyMiddleDLG->coeffs, lenapprox, prec );
                    acb_poly_set(polyMiddleDLG, polyMiddle);
                    
                    if (overlaps==0) {
                        printf("n: %lu, k: %lu, DLG iterates do not overlaps!\n", n, k);
                        printf("initial poly: \n");
                        for (ulong i=0; i<lenapprox; i++)
                            printf("%.16e, %.16e, %.16e\n", poly[3*i], poly[3*i+1], poly[3*i+2]);
                        printf("\n");
                        printf("DLG poly: \n");
                        for (ulong i=0; i<lenapprox; i++)
                            printf("%.16e, %.16e, %.16e\n", polyDLG[3*i], polyDLG[3*i+1], polyDLG[3*i+2]);
                        printf("\n");
                        
                        slong lprec = PWPOLY_MAX( PWPOLY_DEFAULT_PREC, prec/4);
                        _tstar_oneGraeffeIteration_inplace( polyMiddleDLG, lprec);
                        printf("ACB DLG poly: \n");
                        for (ulong i=0; i<lenapprox; i++) {
                            acb_printd(polyMiddleDLG->coeffs+i, 10); 
                            printf("\n");
                        }
                        printf("\n");
                        acb_poly_set(polyMiddleDLG, polyMiddle);
                        
                        k=lastk;
                        n=N-1;
                    }
                }
                
                for (ulong j=0; j<lenapprox; j++) {
                    polyDLG[3*j]   = poly[3*j];
                    polyDLG[3*j+1] = poly[3*j+1];
                    polyDLG[3*j+2] = poly[3*j+2];
                }
                
                int NGbe = 0;
                slong nbRootsbePellet = -1;
                if (res>0) {
                    res = _pw_solving_tstar_unit_disc_be( polyDLG, lenapprox, &NGbe, -1, lenapprox, verbose);
                    if (res<=PWPOLY_DOUBLE_EXCEPTION) {
                        (*nbexcept)++;
                        if (verbose>=level)
                            printf("pwpoly_test_be_DLG: double exception while computing tstar!!!\n");
                    }
                    else {
                        nbRootsbePellet = res;
                        res = 1;
                    }
                }
                
                if (res>0) {
                    NG=0;
                    slong lprec = PWPOLY_MAX( PWPOLY_DEFAULT_PREC, prec/4);
                    nbRootsPellet = _pwpoly_tstar_unit_disc_fixed_prec( polyMiddleDLG, &NG, -1, lenapprox, lprec);
                    if ( (nbRootsPellet != nbRootsbePellet) || ( NG != NGbe ) ) {
                        (*nbdiff)++;
                        printf("n: %lu, k: %lu, tstar tests do not match!\n", n, k);
                        printf(" with arb: %ld, %d, with be: %ld, %d\n", nbRootsPellet, NG, nbRootsbePellet, NGbe );
                        k=lastk;
                        n=N-1;
                    }
                }
                
#ifdef PW_PROFILE
//     nb_pellet_tests+=1;
    if (nbRootsPellet!=TSTAR_NO_ROOT)
        nb_som_root+=1;
    if (nbRootsPellet<TSTAR_NO_ROOT)
        nb_und_root+=1;
    if (nbRootsPellet==1)
        nb_one_root+=1;
    if (nbRootsPellet==2)
        nb_two_root+=1;
    if (nbRootsPellet==3)
        nb_thr_root+=1;
#endif
            }
            pwpoly_free(polyDLG);
            pwpoly_free(poly);
        }
    }
    
    acb_clear(coeff);
    
    acb_poly_clear(polyMiddleDLG);
    acb_poly_clear(polyMiddle);
    
    
    return overlaps;
    
}

int main(int argc, char* argv[]) {
    
    int level = 1;
    
    if (argc<2) {
        pwroots_fprint_options( stdout, argc, argv );
        exit(0);
    }
    
    init_global_variables();
    parseInputArgs( argc, argv, &verbosity, dom, &goalNbRoots, &m, &increase, &check, &log2eps, &goal, &solver, &roundInput, b );
    slong prec = PWPOLY_MAX( 4*m, PWPOLY_DEFAULT_PREC );
    
    if (verbosity>=level) {
        printf("m        : %d\n", m);
        printf("b         : "); fmpq_print(b); printf("\n");
        printf("check    : %d\n", check);
        printf("verbosity: %d\n", verbosity);
    }
    
    type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if (type==PW_PARSE_FAILD){
        clear_global_variables();
        flint_cleanup();
        exit(0);
    }
    
    if (type==PW_PARSE_EXACT)
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, prec);
//     else if (type==PW_PARSE_BALLS) {
//     }
    
    for (slong i=0; i<length; i++) {
        mag_zero( arb_radref(acb_realref(pacb->coeffs+i)) );
        mag_zero( arb_radref(acb_imagref(pacb->coeffs+i)) );
    }
    
#ifdef PW_PROFILE
    PW_INIT_PROFILE
#endif

    /* compute approximation(s) */
    clock_t start_compApprox = clock();
    
    pw_approximation_t app;
    pw_approximation_init_acb_poly( app, pacb, (ulong)m, (ulong)c, scale, b);
    pw_approximation_compute_approx_annulii( app, 1 );

//     if (verbosity>=level) { 
//         pw_approximation_print_short( app );
//     }
    
    double clicks_in_compApprox = (clock() - start_compApprox);

    /* isolate the roots */
    clock_t start_solveApprox = clock();

    double maxdist;
    int nbFalse = 0;
    int nbExcept = 0;
    int res = pwpoly_test_be_DLG( &maxdist, &nbFalse, &nbExcept, app, verbosity );
    
    double clicks_in_solveApprox = (clock() - start_solveApprox);
//     double clicks_in_isolate = clicks_in_compApprox + clicks_in_solveApprox;
    
    /* print solving info */
    if (verbosity>=level) {
        printf(" time for computing approximation: %lf seconds\n",
                 clicks_in_compApprox/CLOCKS_PER_SEC) ;
        printf(" time for solving approximation  : %lf seconds\n",
             clicks_in_solveApprox/CLOCKS_PER_SEC) ;
        
        printf(" DLG's overlaps: %d\n", res);
        printf(" maxdist:        %.10e\n", maxdist);
        
        printf(" nb false: %d\n", nbFalse);
        printf(" nb excep: %d\n", nbExcept);
        
    }

#ifdef PW_PROFILE
    PW_PRINT_PROFILE
#endif

    pw_approximation_clear(app);
    clear_global_variables();
    flint_cleanup();
    
    return 0;
}
