#include "pw_base.h"
#include "common/pw_common.h"
#include "covering/pw_covering.h"
#include "approximation/pw_approximation.h"
#include <stdio.h>
#include <stdlib.h> /* for qsort */
#include <time.h>

int slong_compare( const void * a, const void * b ) {
    return *(slong *)a<*(slong *)b;
}
slong acb_log_error_relative_to_tilde( arb_t ptildeval, int first, const acb_t eval, const acb_t point, arb_poly_t ptilde, slong prec ) {
    if (acb_is_exact(eval)) {
//         printf("evaluation of "); acb_printd(point, 10); printf(" is exact: "); acb_printd(eval, 10); printf("\n");
        return LONG_MIN;
    }
    
    arb_t pointabs;
    arb_init(pointabs);
    
    acb_t errors;
    acb_init(errors);
    
    acb_abs(pointabs, point, prec);
    if (first)
        arb_poly_evaluate_rectangular(ptildeval, ptilde, pointabs, prec);
//     printf(" ------ ptilde: "); arb_printd(ptildeval, 10); printf("\n");
    
    arb_get_rad_arb( acb_realref(errors), acb_realref(eval) );
    arb_get_rad_arb( acb_imagref(errors), acb_imagref(eval) );
    arb_max( acb_realref(errors), acb_realref(errors), acb_imagref(errors), prec );
    arb_div( acb_realref(errors), acb_realref(errors), ptildeval, prec );
    
//     printf(" ------ error relative to ptilde: "); arb_printd(acb_realref(errors), 10); printf("\n");
    arb_log_base_ui( acb_realref(errors), acb_realref(errors), 2, prec );
//     printf(" ------ log2 of error relative to ptilde: "); arb_printd(acb_realref(errors), 10); printf("\n");
    arb_get_ubound_arf( arb_midref( acb_realref(errors) ), acb_realref(errors), prec );
    slong res = arf_get_si( arb_midref( acb_realref(errors) ), ARF_RND_CEIL );
    
    acb_clear(errors);
    
    arb_clear(pointabs);
    
    return res;
}

acb_poly_t pacb, ppacb;
fmpq_poly_t pfmpq_re, pfmpq_im, ppfmpq_re, ppfmpq_im;
slong length;
int type;
int c;
fmpq_t scale, b;
int verbosity, m, verify, nbPoints;

void init_global_variables(){
    acb_poly_init(pacb);
    acb_poly_init(ppacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    fmpq_poly_init(ppfmpq_re);
    fmpq_poly_init(ppfmpq_im);
    length = 0;
    c      = 16;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 7, 2);
    verbosity = 1;
    m = 16;
    verify=0;
    nbPoints=1;
}

void clear_global_variables(){
    acb_poly_clear(pacb);
    acb_poly_clear(ppacb);
    fmpq_poly_clear(pfmpq_re);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(ppfmpq_re);
    fmpq_poly_clear(ppfmpq_im);
    fmpq_clear(scale);
    fmpq_clear(b);
}

int main(int argc, char **argv){
    
    if (argc<1) {
        printf(" !no input file! \n");
        return 0;
    }
    
    init_global_variables();
    
    if (argc>2)
        sscanf(argv[2], "%d", &m);
    printf("m: %d\n", m);
    
    if (argc>3)
        sscanf(argv[3], "%d", &nbPoints);
    printf("nbPoints: %d\n", nbPoints);
    
    if (argc>4)
        sscanf(argv[4], "%d", &verify);
    printf("verify: %d\n", verify);
    
    slong precRoundInitPoly = 2*PWPOLY_DEFAULT_PREC;
    if (argc>5)
        sscanf(argv[5], "%ld", &precRoundInitPoly);
    printf("precRoundInitPoly: %ld\n", precRoundInitPoly);
    
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if (type==PW_PARSE_FAILD){
        clear_global_variables();
        flint_cleanup();
        return 0;
    }
    
    if (type==PW_PARSE_EXACT) {
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, precRoundInitPoly);
    } else if (type==PW_PARSE_BALLS) {
    }
    
    for (slong i=0; i<pacb->length; i++) {
        mag_zero( arb_radref(acb_realref(pacb->coeffs+i)) );
        mag_zero( arb_radref(acb_imagref(pacb->coeffs+i)) );
    }

        /* Print the converted polynomial */
//     printf("%ld-bit floating point approximation of input polynomial\n", prec);
//     printf("=================================================================\n");
//     acb_poly_printd(pacb, 10);
//     printf("\n");
//     printf("=================================================================\n");
    
    arb_poly_t ptilde;
    arb_poly_init(ptilde);
    arb_poly_fit_length(ptilde, pacb->length);
    _arb_poly_set_length(ptilde, pacb->length);
    for (slong i = 0; i < pacb->length; i++)
        acb_abs( ptilde->coeffs + i, pacb->coeffs + i, precRoundInitPoly );
    
#ifdef PW_PROFILE
    PW_INIT_PROFILE
#endif
    
    pw_approximation_t app;

    clock_t start_compApprox = clock();
    pw_approximation_init_acb_poly( app, pacb, m, (ulong)c, scale, b);
    pw_approximation_compute_approx_annulii( app, 0 );
    double clicks_in_compApprox = (clock() - start_compApprox);
    printf(" time for computing approximation: %lf seconds\n",
             clicks_in_compApprox/CLOCKS_PER_SEC) ;
//     pw_approximation_print( app );
    pw_approximation_print_short( app );
    
    flint_rand_t state;
    flint_randinit(state);
    
    acb_t point, value;
    acb_init(point);
    acb_init(value);
    fmpq_ptr pr, pi, vr, vi;
    pr = _fmpq_vec_init(nbPoints);
    pi = _fmpq_vec_init(nbPoints);
    vr = _fmpq_vec_init(nbPoints);
    vi = _fmpq_vec_init(nbPoints);
    
    acb_ptr points, valuesACB, valuesPWE;
    points    = _acb_vec_init(nbPoints);
    valuesACB = _acb_vec_init(nbPoints);
    valuesPWE = _acb_vec_init(nbPoints);
    
    ulong bits = 16;
    _fmpq_vec_randtest(pr, state, nbPoints, bits);
    _fmpq_vec_randtest(pi, state, nbPoints, bits);
    
    for (int n=0; n<nbPoints; n++) {
        arb_set_fmpq( acb_realref(points+n), pr+n, pw_approximation_precref(app) );
        arb_set_fmpq( acb_imagref(points+n), pi+n, pw_approximation_precref(app) );
        mag_zero( arb_radref(acb_realref(points+n)) );
        mag_zero( arb_radref(acb_imagref(points+n)) );
    }
             
    int log2len     = (int) ceil(1.5*log2( (double)pacb->length )); /* not correct if len is so big that the difference between 2 doubles is > 2 */
    slong precArb = 2*m + (slong)log2len;
    printf(" input precision for Arb: %ld\n", precArb);
             
    clock_t start_acbrectangular = clock();
    for (int n=0; n<nbPoints; n++)
        acb_poly_evaluate_rectangular(valuesACB+n, pacb, points+n, precArb);
    double clicks_in_acbrectangular = (clock() - start_acbrectangular);
    
    printf(" time for acb rectangular evaluation of a pol of degree %d at %d points at prec %ld: %lf seconds\n",
             (int)pacb->length-1, nbPoints, precArb, clicks_in_acbrectangular/CLOCKS_PER_SEC) ;
    
             
    clock_t start_pwe = clock();
    for (int n=0; n<nbPoints; n++) {
        pw_approximation_evaluate_acb( valuesPWE+n, app, points+n, 0 );
//         acb_t temp;
//         acb_init(temp);
//         _pw_approximation_evaluate2_acb( valuesPWE+n, temp, app, points+n, 0 );
//         acb_clear(temp);
    }
    double clicks_in_pwe = (clock() - start_pwe);
    
    printf(" time for pwe             evaluation of a pol of degree %d at %d points with m=%d: %lf seconds\n",
             (int)pacb->length-1, nbPoints, m, clicks_in_pwe/CLOCKS_PER_SEC) ;
    
    FILE * covFile;
    char covFileName[] = "covering_with_points.plt\0";
    covFile = fopen (covFileName,"w");
    _pw_covering_with_points_gnuplot( covFile, pw_approximation_coveringref(app), points, nbPoints);
    fclose (covFile);
            
    if (verify) {
        int log2errorpweOK = 1;
        int log2erroracbOK = 1;
        arb_t dist, maxdist, vtilde, one;
        acb_t diff;
        arb_init(dist);
        arb_init(maxdist);
        arb_init(vtilde);
        acb_init(diff);
        arb_init(one);
        arb_one(one);
        arb_zero(maxdist);
        slong acb_min_rel_error = LONG_MAX;
        slong acb_max_rel_error = LONG_MIN;
        slong pwe_min_rel_error = LONG_MAX;
        slong pwe_max_rel_error = LONG_MIN;
        slong * acb_rel_errors = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
        slong * pwe_rel_errors = (slong *) pwpoly_malloc ( nbPoints*sizeof(slong) );
        /* verify */
        for (int n=0; n<nbPoints; n++) {
            slong relerracb = acb_log_error_relative_to_tilde( vtilde, 1, valuesACB+n, points+n, ptilde, precRoundInitPoly );
            acb_min_rel_error = PWPOLY_MIN( acb_min_rel_error, relerracb );
            acb_max_rel_error = PWPOLY_MAX( acb_max_rel_error, relerracb );
            log2erroracbOK = log2erroracbOK && (relerracb <= -(slong)m);
            acb_rel_errors[n] = relerracb;
            
            slong relerrpwe = acb_log_error_relative_to_tilde( vtilde, 0, valuesPWE+n, points+n, ptilde, precRoundInitPoly );
            pwe_min_rel_error = PWPOLY_MIN( pwe_min_rel_error, relerrpwe );
            pwe_max_rel_error = PWPOLY_MAX( pwe_max_rel_error, relerrpwe );
            log2errorpweOK = log2errorpweOK && (relerrpwe <= -(slong)m);
            pwe_rel_errors[n] = relerrpwe;
            
            acb_poly_evaluate_rectangular(valuesACB+n, pacb, points+n, precRoundInitPoly);
            acb_sub(diff, valuesPWE+n, valuesACB+n, precRoundInitPoly);
            acb_abs(dist, diff, precRoundInitPoly);
            arb_div(dist, dist, vtilde, precRoundInitPoly);
            arb_max(maxdist, maxdist, dist, precRoundInitPoly);
            
            if ((relerrpwe > -(slong)m) || arb_ge(dist, one) ) {
                printf(" --- point                : "); acb_printd( points+n, 10 ); printf("\n");
                printf(" --- ACB eval rectangular : "); acb_printd( valuesACB+n, 10 ); printf("\n");
                printf(" --- PWE eval             : "); acb_printd( valuesPWE+n, 10 ); printf("\n");
                printf(" --- log2 of error        : %ld\n", relerrpwe );
                printf(" --- relative dist        : "); arb_printd( dist, 10); printf("\n");
                printf("\n");
                pw_approximation_evaluate_acb( valuesPWE+n, app, points+n, 4 );
                
            }
            
        }
        qsort (acb_rel_errors, nbPoints, sizeof(slong), slong_compare);
        qsort (pwe_rel_errors, nbPoints, sizeof(slong), slong_compare);
//         for (int n=0; n<nbPoints; n++)
//             printf("%ld, \n", acb_rel_errors[n] );
        
        
        printf(" log2erroracbOK: %d\n", log2erroracbOK);
        printf(" acb_min_rel_error: %ld, acb_max_rel_error: %ld, median value: %ld \n", acb_min_rel_error, acb_max_rel_error, acb_rel_errors[nbPoints/2]);
        printf(" log2errorpweOK: %d\n", log2errorpweOK);
        printf(" pwe_min_rel_error: %ld, pwe_max_rel_error: %ld, median value: %ld  \n", pwe_min_rel_error, pwe_max_rel_error, pwe_rel_errors[nbPoints/2]);
        printf(" max dist center to center: "); arb_printd(maxdist, 10); printf("\n");
        
        pwpoly_free( acb_rel_errors );
        pwpoly_free( pwe_rel_errors );
        arb_clear(dist);
        arb_clear(vtilde);
        arb_clear(maxdist);
        acb_clear(diff);
        arb_clear(one);
    }
    
//     pwpoly_free( isInACB );
//     pwpoly_free( isInPWE );
    
    _fmpq_vec_clear(pr, nbPoints);
    _fmpq_vec_clear(pi, nbPoints);
    _fmpq_vec_clear(vr, nbPoints);
    _fmpq_vec_clear(vi, nbPoints);
    
    _acb_vec_clear(points,    nbPoints);
    _acb_vec_clear(valuesACB, nbPoints);
    _acb_vec_clear(valuesPWE, nbPoints);
    
//     _mag_vec_clear(ratioErrors, nbPoints );
//     mag_clear(ratioErrorMean);
    
    flint_randclear(state);
    
    acb_clear(value);
    acb_clear(point);
    
    pw_approximation_clear(app);
    
#ifdef PW_PROFILE
    PW_PRINT_PROFILE
#endif    
    
    clear_global_variables();
    
    flint_cleanup();
    
    return 0;
}
