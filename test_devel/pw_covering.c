#include "pw_base.h"
#include "common/pw_common.h"
#include "covering/pw_covering.h"
#include "approximation/pw_approximation.h" /* for the function computing the mantissas and exponents */
#include <stdio.h>

int main(int argc, char **argv){
    
    if (argc<1) {
        printf(" !no input file! \n");
        return 0;
    }
    
    slong m = 16;
    if (argc>2)
        sscanf(argv[2], "%ld", &m);
    printf("m: %ld\n", m);
    slong prec = PWPOLY_MAX( 2*m, PWPOLY_DEFAULT_PREC );
        
    acb_poly_t pacb;
    fmpq_poly_t pfmpq_re;
    fmpq_poly_t pfmpq_im;
    
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    
    slong len=0;
    int type = parseInputFile( &len, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if (type==PW_PARSE_FAILD){
        acb_poly_clear(pacb);
        fmpq_poly_clear(pfmpq_re);
        fmpq_poly_clear(pfmpq_im);
        flint_cleanup();
        return 0;
    }
    
    if (type==PW_PARSE_EXACT) {
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, prec);
        
//         fmpq_t coeff;
//         fmpq_init(coeff);
//         printf("Input polynomial has rational coeffs\n");
//         printf("degree\treal part\timaginary part\t\n");
//         printf("=================================================================\n");
//         for (slong i=0; i<len; i++){
//             printf("%ld\t", i);
//             if (i<pfmpq_re->length)
//                 fmpq_poly_get_coeff_fmpq( coeff, pfmpq_re, i);
//             else
//                 fmpq_zero(coeff);
//             fmpq_print(coeff);
//             printf("\t\t");
//             if (i<pfmpq_im->length)
//                 fmpq_poly_get_coeff_fmpq( coeff, pfmpq_im, i);
//             else
//                 fmpq_zero(coeff);
//             fmpq_print(coeff);
//             printf("\n");
//         }
//         printf("=================================================================\n\n");
//         fmpq_clear(coeff);
    } else if (type==PW_PARSE_BALLS) {
//         len = pacb->length;
//         printf("Input polynomial has approximated coeffs\n");
//         printf("degree\treal part\timaginary part\t\n");
//         printf("=================================================================\n");
//         for (slong i=0; i < pacb->length; i++){
//             printf("%ld\t", i);
//             arb_printd( acb_realref( pacb->coeffs + i ), 10 );
//             printf("\t\t");
//             arb_printd( acb_imagref( pacb->coeffs + i ), 10 );
//             printf("\n");
//         }
//         printf("=================================================================\n\n");
    }
    

    /* Print the converted polynomial */
//     printf("%ld-bit floating point approximation of input polynomial\n", prec);
//     printf("=================================================================\n");
//     acb_poly_printd(pacb, 10);
//     printf("\n");
//     printf("=================================================================\n");
    
    fmpq_ptr weights   = _fmpq_vec_init( len );
    arb_ptr  mantissas = _arb_vec_init( len );
    arb_ptr  abscoeffs = _arb_vec_init( len );
    for (slong i = 0; i<pacb->length; i++)
        acb_abs( abscoeffs + i, (pacb->coeffs)+i, prec );
    
    _pw_approximation_compute_mantissas_exponents_arb_ptr( mantissas, weights, abscoeffs, len );
    
    /* Print the polynomial of weights */
//     printf("exponents of absolute values of approximated input polynomial\n");
//     printf("=================================================================\n");
//     printf("[ ");
//     for (slong i=0; i<len; i++) {
//         fmpq_print(weights+i); printf(" ");
//     }
//     printf("]\n");
//     printf("=================================================================\n");
    
    int c=2;
//     int c=16;
    fmpq_t a, b;
    fmpq_init(a);
    fmpq_init(b);
    fmpq_set_si(a, 3, 2);
    fmpq_set_si(b, 2, 5); // for c=2 and m>=4
//     fmpq_set_si(b, 7, 2); // for c=4 and m>=5
    
    pw_covering_t cov;
    pw_covering_init(cov, m, (ulong)len, (ulong)c, a, b);
    _pw_covering_compute_annulii_from_fmpq_weights( cov, weights );
    _pw_covering_print( cov );
    
    FILE * CHFile;
    char CHFileName[] = "CH.plt\0";
    CHFile = fopen (CHFileName,"w");
    _pw_covering_compute_annulii_from_fmpq_weights_gnuplot( CHFile, 1, cov, weights );
//     _pw_covering_compute_annulii_from_fmpq_weights_gnuplot( CHFile, pw_covering_Nref(cov)-1, cov, weights );
    fclose (CHFile);
    
    FILE * covFile;
    char covFileName[] = "covering.plt\0";
    covFile = fopen (covFileName,"w");
    _pw_covering_gnuplot( covFile, cov);
    fclose (covFile);
    
    _fmpq_vec_clear(weights, len);
    _arb_vec_clear(mantissas, len);
    _arb_vec_clear(abscoeffs, len);
    
    fmpq_clear(a);
    fmpq_clear(b);
    pw_covering_clear(cov);
    
    acb_poly_clear(pacb);
    fmpq_poly_clear(pfmpq_re);
    fmpq_poly_clear(pfmpq_im);
    flint_cleanup();
    
    return 0;
}
