#include "pw_base.h"
#include "common/pw_common.h"
#include <stdio.h>

int main(int argc, char **argv){
    
    if (argc<2) {
        printf(" !no input file! \n");
        return 0;
    }
    
    slong length = 0;
    acb_poly_t pacb;
    fmpq_poly_t pfmpq_re;
    fmpq_poly_t pfmpq_im;
    
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if (type==PW_PARSE_EXACT) {
        fmpq_t coeff;
        fmpq_init(coeff);
        printf("Input polynomial has rational coeffs\n");
        printf("degree\treal part\timaginary part\t\n");
        printf("=================================================================\n");
        for (slong i=0; i<length; i++){
            printf("%ld\t", i);
            if (i<pfmpq_re->length)
                fmpq_poly_get_coeff_fmpq( coeff, pfmpq_re, i);
            else
                fmpq_zero(coeff);
            fmpq_print(coeff);
            printf("\t\t");
            if (i<pfmpq_im->length)
                fmpq_poly_get_coeff_fmpq( coeff, pfmpq_im, i);
            else
                fmpq_zero(coeff);
            fmpq_print(coeff);
            printf("\n");
        }
        printf("=================================================================\n\n");
        fmpq_clear(coeff);
    } else if (type==PW_PARSE_BALLS) {
        printf("Input polynomial has approximated coeffs\n");
        printf("degree\treal part\timaginary part\t\n");
        printf("=================================================================\n");
        for (slong i=0; i < length; i++){
            printf("%ld\t", i);
            arb_printd( acb_realref( pacb->coeffs + i ), 10 );
            printf("\t\t");
            arb_printd( acb_imagref( pacb->coeffs + i ), 10 );
            printf("\n");
        }
        printf("=================================================================\n\n");
    }
    
    acb_poly_clear(pacb);
    fmpq_poly_clear(pfmpq_re);
    fmpq_poly_clear(pfmpq_im);
    
    flint_cleanup();
    
    return 0;
    
}
