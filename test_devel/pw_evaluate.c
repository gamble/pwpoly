#include "pw_base.h"
#include "common/pw_common.h"
#include "covering/pw_covering.h"
#include "approximation/pw_approximation.h"
#include <stdio.h>

slong acb_log_error_relative_to_tilde( const acb_t eval, const acb_t point, arb_poly_t ptilde, slong prec ) {
    arb_t pointabs, valueabs;
    arb_init(pointabs);
    arb_init(valueabs);
    
    acb_t errors;
    acb_init(errors);
    
    acb_abs(pointabs, point, prec);
    arb_poly_evaluate_rectangular(valueabs, ptilde, pointabs, prec);
    
    arb_get_rad_arb( acb_realref(errors), acb_realref(eval) );
    arb_get_rad_arb( acb_imagref(errors), acb_imagref(eval) );
    arb_max( acb_realref(errors), acb_realref(errors), acb_imagref(errors), prec );
    arb_div( acb_realref(errors), acb_realref(errors), valueabs, prec );
    
//     printf(" ------ error relative to ptilde: "); arb_printd(acb_realref(errors), 10); printf("\n");
    arb_log_base_ui( acb_realref(errors), acb_realref(errors), 2, prec );
//     printf(" ------ log2 of error relative to ptilde: "); arb_printd(acb_realref(errors), 10); printf("\n");
    arb_get_ubound_arf( arb_midref( acb_realref(errors) ), acb_realref(errors), prec );
    slong res = arf_get_si( arb_midref( acb_realref(errors) ), ARF_RND_CEIL );
    
    acb_clear(errors);
    
    arb_clear(pointabs);
    arb_clear(valueabs);
    
    return res;
}

acb_poly_t pacb, ppacb;
fmpq_poly_t pfmpq_re, pfmpq_im, ppfmpq_re, ppfmpq_im;
slong length;
int type;
int c;
fmpq_t scale, b;
int verbosity, m;

void init_global_variables(){
    acb_poly_init(pacb);
    acb_poly_init(ppacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    fmpq_poly_init(ppfmpq_re);
    fmpq_poly_init(ppfmpq_im);
    length = 0;
    c      = 16;
    fmpq_init(scale);
    fmpq_init(b);
    fmpq_set_si(scale, 3, 2);
    fmpq_set_si(b, 7, 2);
    verbosity = 1;
    m = 16;
}

void clear_global_variables(){
    acb_poly_clear(pacb);
    acb_poly_clear(ppacb);
    fmpq_poly_clear(pfmpq_re);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(ppfmpq_re);
    fmpq_poly_clear(ppfmpq_im);
    fmpq_clear(scale);
    fmpq_clear(b);
}

int main(int argc, char **argv){
    
    if (argc<1) {
        printf(" !no input file! \n");
        return 0;
    }
    
    init_global_variables();
    
    if (argc>2)
        sscanf(argv[2], "%d", &m);
    printf("m: %d\n", m);
    
    
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if (type==PW_PARSE_FAILD){
        clear_global_variables();
        flint_cleanup();
        return 0;
    }
    
#ifdef PW_PROFILE
    PW_INIT_PROFILE
#endif

    pw_approximation_t app, appprime;
    slong prec;
    
    if (type==PW_PARSE_EXACT) {
        ulong working_m = pw_approximation_get_working_m( (ulong)m, length );
        prec = pw_approximation_get_working_prec(working_m);
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, prec);
//         pw_approximation_init_2fmpq_poly( app, pfmpq_re, pfmpq_im, (ulong)m, (ulong)c, scale, b);
        pw_approximation_init_acb_poly( app, pacb, (ulong)m, (ulong)c, scale, b);
//         prec = pw_approximation_precref(app);
//         acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, prec);
        fmpq_poly_derivative(ppfmpq_re, pfmpq_re);
        fmpq_poly_derivative(ppfmpq_im, pfmpq_im);
        acb_poly_set2_fmpq_poly( ppacb, ppfmpq_re, ppfmpq_im, prec);
//         pw_approximation_init_2fmpq_poly( appprime, ppfmpq_re, ppfmpq_im, (ulong)m, (ulong)c, scale, b);
        pw_approximation_init_acb_poly( appprime, ppacb, (ulong)m, (ulong)c, scale, b);
        
    } else {
        pw_approximation_init_acb_poly( app, pacb, (ulong)m, (ulong)c, scale, b);
        prec = pw_approximation_precref(app);
        acb_poly_derivative(ppacb, pacb, prec );
        pw_approximation_init_acb_poly( appprime, ppacb, (ulong)m, (ulong)c, scale, b);
    }
    pw_approximation_compute_approx_annulii( app, 0 );
    pw_approximation_compute_approx_annulii( appprime, 0 );
    
    arb_poly_t ptilde;
    arb_poly_init(ptilde);
    arb_poly_fit_length(ptilde, pacb->length);
    _arb_poly_set_length(ptilde, pacb->length);
    for (slong i = 0; i < pacb->length; i++)
        acb_abs( ptilde->coeffs + i, pacb->coeffs + i, prec );
    
    acb_t point, value, valueder, valuePWE, valuederPWE, valueACB, valuederACB;
    acb_init(point);
    acb_init(value);
    acb_init(valueder);
    acb_init(valuePWE);
    acb_init(valuederPWE);
    acb_init(valueACB);
    acb_init(valuederACB);
//     acb_set_d_d(point, -1., -0.2);
    acb_set_d_d(point, -31.00000000, -1.151312890);
    acb_set_d_d(point, -31.00000000, 10);
    acb_set_d_d(point, -15.00000000, -1.151312890);
    acb_set_d_d(point,  31.00000000, 0.0);
    acb_set_d_d(point,  61.00000000, 0.0);
    acb_set_d_d(point,  3.00000000, 0.0);
//     arb_add_error_2exp_si( acb_realref(point), -200 );
//     arb_add_error_2exp_si( acb_imagref(point), -200 );
    
    printf("===========================================================\n");
    
    acb_poly_evaluate2_rectangular(valueACB, valuederACB, pacb, point, prec);
    
    slong relerrarb = acb_log_error_relative_to_tilde( valueACB, point, ptilde, prec );
    
    pw_approximation_evaluate_acb( valuePWE, app, point, 4 );
    pw_approximation_evaluate_acb( valuederPWE, appprime, point, 0);
    
    slong relerrpwe = acb_log_error_relative_to_tilde( valuePWE, point, ptilde, prec );
    
    pw_approximation_evaluate2_acb( value, valueder, app, point, 1 );
    
    printf("===========================================================\n");
    printf(" point                        : "); acb_printd( point, 10 ); printf("\n");
    printf(" --- value    ACB evaluate    : "); acb_printd( valueACB, 10 ); printf("\n");
    printf(" ------ relative error        : %ld\n",relerrarb); 
    printf(" --- value    PWE evaluate    : "); acb_printd( valuePWE, 10 ); printf("\n");
    printf(" ------ relative error        : %ld\n",relerrpwe); 
    printf(" --- value    PWE evaluate2   : "); acb_printd( value, 10 ); printf("\n");
    printf(" --- valueder ACB evaluate    : "); acb_printd( valuederACB, 10 ); printf("\n");
    printf(" --- valueder PWE evaluate    : "); acb_printd( valuederPWE, 10 ); printf("\n");
    printf(" --- valueder PWE evaluate2   : "); acb_printd( valueder, 10 ); printf("\n");
    printf("===========================================================\n");
    
    FILE * covFile;
    char covFileName[] = "covering.plt\0";
    covFile = fopen (covFileName,"w");
    _pw_covering_gnuplot( covFile, pw_approximation_coveringref(app));
    fclose (covFile);
    
    FILE * covPFile;
    char covPFileName[] = "covering_with_point.plt\0";
    covPFile = fopen (covPFileName,"w");
    _pw_covering_with_point_gnuplot( covPFile, pw_approximation_coveringref(app), point);
    fclose (covPFile);
    
    acb_clear(point);
    acb_clear(value);
    acb_clear(valueder);
    acb_clear(valuePWE);
    acb_clear(valuederPWE);
    acb_clear(valueACB);
    acb_clear(valuederACB);
    
#ifdef PW_PROFILE
    PW_PRINT_PROFILE
#endif    
    
    clear_global_variables();
    
    flint_cleanup();
    
    return 0;
}
