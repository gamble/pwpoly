#include "pw_base.h"
#include "common/pw_common.h"
#include <stdio.h>

void mps_monomial_poly_fprint( FILE * f, mps_monomial_poly *p, mps_context * ctx ) {
    
    mps_polynomial * poly = MPS_POLYNOMIAL_CAST(mps_polynomial, p);
    int deg = poly->degree;
    
    fprintf(f, "degree: %d, coeffs: [\n", deg);
    
    if (MPS_STRUCTURE_IS_RATIONAL( poly->structure )) {
        mpq_t coeff_re, coeff_im;
        mpq_init(coeff_re);
        mpq_init(coeff_im);
        printf("Input polynomial has rational coeffs\n");
        for (int i = 0; i <= deg; i++) {
            mps_monomial_poly_get_coefficient_q (ctx, p, i, coeff_re, coeff_im);
            printf("%d\t", i);
            mpq_out_str( stdout, 10, coeff_re );
            printf("\t\t");
            mpq_out_str( stdout, 10, coeff_im );
            printf("\n");
        }
        mpq_clear(coeff_re);
        mpq_clear(coeff_im);
    } else if (MPS_STRUCTURE_IS_FP( poly->structure )) {
        printf("Input polynomial has floating point coeffs of precision: %ld\n", mps_monomial_poly_get_precision(ctx, p));
        printf("Input polynomial has floating point coeffs of precision: %ld\n", p->prec);
        for (int i = 0; i <= deg; i++) {
            gmp_printf("%Fe\t", mpc_Re(p->mfpc[i]));
            printf("\t\t");
            gmp_printf("%Fe\t", mpc_Im(p->mfpc[i]));
            printf("\n");
        }
    }
    fprintf(f, "]" );
}

void mps_monomial_poly_print( mps_monomial_poly *src, mps_context * ctx ) {
    mps_monomial_poly_fprint( stdout, src, ctx );
}

int main(int argc, char **argv){
    
    if (argc<1) {
        printf(" !no input file! \n");
        return 0;
    }
        
    FILE * infile = fopen (argv[1], "r");
    
    if (!infile) {
        printf(" !Cannot open input file for read, aborting! \n");
        return 0;
    }
    
    mps_context * ctx = mps_context_new ();
    mps_polynomial * poly = mps_parse_stream (ctx, infile);
    
    if (!poly) {
        printf(" !Error while parsing the polynomial, aborting! \n");
        mps_context_free(ctx);
        return 0;
    }
    
    int deg = poly->degree;
    
    /* convert input polynomial */
    mps_monomial_poly *p = MPS_POLYNOMIAL_CAST(mps_monomial_poly, poly);
    
    printf("Input mps polynomial\n");
    printf("=================================================================\n");
    mps_monomial_poly_print( p, ctx );
    printf("\n");
    printf("=================================================================\n");
    
    slong prec = 2*PWPOLY_DEFAULT_PREC;
    acb_poly_t pApp;
    acb_poly_init(pApp);

    if (MPS_STRUCTURE_IS_RATIONAL( poly->structure )) {
        fmpq_poly_t p_re, p_im;
        fmpq_poly_init(p_re);
        fmpq_poly_init(p_im);
        
        pwpoly_mps_to_2_fmpq_poly( p_re, p_im, ctx, p, deg );
        printf("Input polynomial\n");
        printf("=================================================================\n");
        printf("Real part\n");
        fmpq_poly_print_pretty(p_re, "x");
        printf("\n");
        printf("Imaginary part\n");
        fmpq_poly_print_pretty(p_im, "x");
        printf("\n");
        printf("=================================================================\n");
        acb_poly_set2_fmpq_poly( pApp, p_re, p_im, prec);
        
        fmpq_poly_clear(p_re);
        fmpq_poly_clear(p_im);
        
    } else if (MPS_STRUCTURE_IS_FP( poly->structure )) {
        
        pwpoly_mps_to_acb_poly( pApp, ctx, p, deg, prec );
        printf("Input polynomial\n");
        printf("=================================================================\n");
        acb_poly_printd(pApp, 10);
        printf("\n");
        printf("=================================================================\n");
        
    }
    
//     mps_context * ctx = mps_context_new ();
    /* create and allocate a polynomial of degree deg */
    mps_monomial_poly * np = mps_monomial_poly_new (ctx, deg);
    /* fill the coefficients */
//     _pwpoly_acb_to_mps_poly( np, ctx, pApp->coeffs, pApp->length, prec );
    _pwpoly_acb_to_mps_poly_q( np, ctx, pApp->coeffs, pApp->length );
//     mpfr_t re, im;
//     mpfr_init2(re, (mpfr_prec_t)prec );
//     mpfr_init2(im, (mpfr_prec_t)prec );
//     mpc_t co;
//     mpc_init2(co, (unsigned long int)prec );
//     for (int i=0; i<=deg; i++) {
//         arf_get_mpfr( re, arb_midref(acb_realref(pApp->coeffs + i)), MPFR_RNDN );
//         arf_get_mpfr( im, arb_midref(acb_imagref(pApp->coeffs + i)), MPFR_RNDN );
//         mpfr_get_f ( mpc_Re( co ), re, MPFR_RNDN );
//         mpfr_get_f ( mpc_Im( co ), im, MPFR_RNDN );
//         mps_monomial_poly_set_coefficient_f (ctx, np, i, co);
//     }
//     mpfr_clear(re);
//     mpfr_clear(im);
//     mpc_clear(co);
    
    printf("Input polynomial converted in mps\n");
    printf("=================================================================\n");
    mps_monomial_poly_print( np, ctx );
    printf("\n");
    printf("=================================================================\n");
    
//     mps_context_free(ctx2);
    
    acb_poly_clear(pApp);
    
    /*cast it into  a poly */
    mps_polynomial * npoly = MPS_POLYNOMIAL_CAST(mps_polynomial, np);
    
    /* solve the polynomial */
//     mps_context_set_input_poly (ctx, poly);
    mps_context_set_input_poly (ctx, npoly);
    
    mpc_t *roots = NULL;  /* pairs of gmp's mpf_t, one for real part, one for imag part, defined in mps/mpc.h */
    rdpe_t *radii = NULL; /* a rdpe is a double (the mantissa) + a long (the exponent) , defined in mps/mt.h  */
  
    /* Set the output precision */
    mps_context_set_output_prec (ctx, 53);
    mps_context_set_output_goal (ctx, MPS_OUTPUT_GOAL_APPROXIMATE);
    /* Set the algorithm */
//     mps_context_select_algorithm(ctx, MPS_ALGORITHM_SECULAR_GA);
    mps_context_select_algorithm(ctx, MPS_ALGORITHM_STANDARD_MPSOLVE);
    
    
    /* Find the roots */
    mps_mpsolve (ctx);
    
    /* Read the roots from context */
    mps_context_get_roots_m (ctx, &roots, &radii);
    
    /* Print out roots */
    double err_max=0., err_d=0.;
    double root_re, root_im;
    printf("Roots:\t\t\t\t\tInclusion\n");
    printf("real part\t imaginary part\t\tabsolute radius\n");
    printf("=================================================================\n");
//     mpc_init2(err, mpc_get_prec (roots[0]));
    for (int m = 0; m < deg; m++) {
        err_d = rdpe_get_d (radii[m]);
        err_max = err_d > err_max ? err_d : err_max;
        root_re = mpf_get_d( mpc_Re(roots[m]) );
        root_im = mpf_get_d( mpc_Im(roots[m]) );
        printf("%.3e\t%.3e\t\t%.3e\n", root_re, root_im, err_d );
    }
    printf("=================================================================\n");
    printf("\t\t\t\tmax(Error) = %.3e\n",err_max);
    
    /* Free the allocated memory */
    for (int m = 0; m < deg; m++) {
        mpc_clear(roots[m]);
        rdpe_clear(radii[m]);
    }
    free (roots);
    free(radii);
    
    mps_monomial_poly_free (ctx, npoly);
    
    mps_monomial_poly_free(ctx, poly);
    mps_context_free(ctx);
    
    flint_cleanup();
    
    return EXIT_SUCCESS;
}
