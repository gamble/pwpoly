#include "pw_base.h"
#include "geometry/box.h"
#include "geometry/box_CoCo.h"

int main(int argc, char* argv[]) {
    
    box_t b;
    box_init(b);
    acb_t ba;
    acb_init(ba);
    
    box_set_si_si_si(b, 0, 0, 0);
    box_print(b); printf("\n");
    box_get_acb(ba, b);
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    
    box_set_si_si_si(b, 1, 0, 0);
    box_print(b); printf("\n");
    box_get_acb(ba, b);
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    
    box_set_si_si_si(b,-1, 0, 0);
    box_print(b); printf("\n");
    box_set_si_si_si(b, -1, -1, 0);
    box_print(b); printf("\n");
    box_set_si_si_si(b, -1, -1, -1);
    box_print(b); printf("\n");
    box_set_si_si_si(b, -1, 0, -1);
    box_print(b); printf("\n");
    
    
    box_set_si_si_si(b, -2, 1, 1);
    box_get_acb(ba, b);
    box_print(b); printf("\n");
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    
    box_set_si_si_si(b, 0, 0, 0);
    box_list_t lb1;
    box_list_init(lb1);
    box_quadrisect( lb1, b );
    printf("subboxes: "); box_list_print(lb1); printf("\n");
    
    box_list_iterator it = box_list_begin(lb1);
    while (it!=box_list_end()) {
        box_get_acb(ba, box_list_elmt(it));
        printf("acb box: "); acb_printd(ba, 10); printf("\n");
        it = box_list_next(it);
    }
    
    box_list_t lb2;
    box_list_init(lb2);
    box_list_quadrisect(lb2, lb1);
    printf("subboxes: "); box_list_print(lb2); printf("\n");
    
    it = box_list_begin(lb2);
    while (it!=box_list_end()) {
        box_get_acb(ba, box_list_elmt(it));
        printf("acb box: "); acb_printd(ba, 10); printf("\n");
        it = box_list_next(it);
    }
    
    box_list_clear(lb1);
    box_list_clear(lb2);
    
    box_t b1, b2;
    box_init(b1);
    box_init(b2);
    box_set_si_si_si(b1, -3, -4, -4);
    box_set_si_si_si(b2, -3, -3, -3);
    printf("b1: "); box_print(b1); printf("\n");
    box_get_acb(ba, b1);
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    printf("b2: "); box_print(b2); printf("\n");
    box_get_acb(ba, b2);
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    printf("b1 and b2 are 8 connected? %d\n", box_are_8connected ( b1, b2 ) );
    
    box_set_si_si_si(b1, -3, -4, -4);
    box_set_si_si_si(b2, -3, -2, -4);
    printf("b1: "); box_print(b1); printf("\n");
    box_get_acb(ba, b1);
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    printf("b2: "); box_print(b2); printf("\n");
    box_get_acb(ba, b2);
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    printf("b1 and b2 are 8 connected? %d\n", box_are_8connected ( b1, b2 ) );
    
    box_clear(b1);
    box_clear(b2);
    
    box_set_si_si_si(b, 0, 0, 0);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -1, 0, 0);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -2, 0, 0);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -2, 1, 1);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -3, 1, 1);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -3, 2, 2);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -3, 3, 3);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -3, 3, -4);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -3, -4, -4);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -3, -4, 3);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    
    box_set_si_si_si(b, -3, 2, 3);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -3, 3, -3);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -3, -4, -3);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    box_set_si_si_si(b, -3, -3, 3);
    printf("box "); box_print(b); printf(" is outside unit disc ? :%d\n", box_is_outside_unit_disc ( b ) );
    printf("box "); box_print(b); printf(" is inside  unit disc ? :%d\n", box_is_inside_unit_disc ( b ) );
    
    box_clear(b);
    acb_clear(ba);
    
    box_t bu, bE, bS, bW, bN;
    box_init(bu);
    box_init(bE);
    box_init(bS);
    box_init(bW);
    box_init(bN);
    box_set_si_si_si(bu, 0, 0, 0);
    box_set_si_si_si(bE, 0, 1, 0);
    box_set_si_si_si(bS, 0, 0, -1);
    box_set_si_si_si(bW, 0, -1, 0);
    box_set_si_si_si(bN, 0, 0, 1);
    printf("bE: "); box_print(bE); printf("\n");
    box_get_acb(ba, bE);
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    printf("bS: "); box_print(bS); printf("\n");
    box_get_acb(ba, bS);
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    printf("bW: "); box_print(bW); printf("\n");
    box_get_acb(ba, bW);
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    printf("bN: "); box_print(bN); printf("\n");
    box_get_acb(ba, bN);
    printf("acb box: "); acb_printd(ba, 10); printf("\n");
    
    printf("bE and bN are 8 connected? %d\n", box_are_8connected ( bE, bN ) );
    printf("bE and bW are 8 connected? %d\n", box_are_8connected ( bE, bW ) );
    
    box_clear(bu);
    box_clear(bE);
    box_clear(bS);
    box_clear(bW);
    box_clear(bN);
    
    acb_t tt, ttt;
    acb_init(tt);
    acb_init(ttt);
//     slong prec = 53;
    
    box_list_t bl;
    box_list_init(bl);
    printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
    acb_zero(tt);
    mag_set_d( arb_radref( acb_realref(tt) ), 0.45 );
    mag_set  ( arb_radref( acb_imagref(tt) ), arb_radref( acb_realref(tt) ) );
    printf("acb box tt: "); acb_printd(tt, 10); printf("\n");
    box_get_box_list_acb( bl, tt );
    printf("boxes: "); box_list_print(bl); printf("\n");
    box_CoCo_t bc;
    box_CoCo_init_box(bc, box_list_pop(bl));
    while ( ! box_list_is_empty(bl) )
        box_CoCo_insert_box(bc, box_list_pop(bl));
    box_CoCo_get_contBox(ttt, bc);
    printf("acb box ttt: "); acb_printd(ttt, 10); printf("\n");
    box_CoCo_clear(bc);
    printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
    acb_zero(tt);
    arb_set_d ( acb_realref(tt), 0.3 );
    mag_set_d( arb_radref( acb_realref(tt) ), 0.125 );
    mag_set  ( arb_radref( acb_imagref(tt) ), arb_radref( acb_realref(tt) ) );
    printf("acb box tt: "); acb_printd(tt, 10); printf("\n");
    box_get_box_list_acb( bl, tt );
    printf("boxes: "); box_list_print(bl); printf("\n");
    box_CoCo_init_box(bc, box_list_pop(bl));
    while ( ! box_list_is_empty(bl) )
        box_CoCo_insert_box(bc, box_list_pop(bl));
    box_CoCo_get_contBox(ttt, bc);
    printf("acb box ttt: "); acb_printd(ttt, 10); printf("\n");
    box_CoCo_clear(bc);
    printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
    acb_zero(tt);
    arb_set_d ( acb_realref(tt), 3. );
    mag_set_d( arb_radref( acb_realref(tt) ), 0.125 );
    mag_set  ( arb_radref( acb_imagref(tt) ), arb_radref( acb_realref(tt) ) );
    printf("acb box tt: "); acb_printd(tt, 10); printf("\n");
    box_get_box_list_acb( bl, tt );
    printf("boxes: "); box_list_print(bl); printf("\n");
    box_CoCo_init_box(bc, box_list_pop(bl));
    while ( ! box_list_is_empty(bl) )
        box_CoCo_insert_box(bc, box_list_pop(bl));
    box_CoCo_get_contBox(ttt, bc);
    printf("acb box ttt: "); acb_printd(ttt, 10); printf("\n");
    box_CoCo_clear(bc);
    printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
    acb_zero(tt);
    arb_set_d ( acb_realref(tt), 3. );
    mag_set_d( arb_radref( acb_realref(tt) ), 1. );
    mag_set  ( arb_radref( acb_imagref(tt) ), arb_radref( acb_realref(tt) ) );
    printf("acb box tt: "); acb_printd(tt, 10); printf("\n");
    box_get_box_list_acb( bl, tt );
    printf("boxes: "); box_list_print(bl); printf("\n");
    box_CoCo_init_box(bc, box_list_pop(bl));
    while ( ! box_list_is_empty(bl) )
        box_CoCo_insert_box(bc, box_list_pop(bl));
    box_CoCo_get_contBox(ttt, bc);
    printf("acb box ttt: "); acb_printd(ttt, 10); printf("\n");
    box_CoCo_clear(bc);
    printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
    acb_zero(tt);
    arb_set_d ( acb_realref(tt), 10 );
    mag_set_d( arb_radref( acb_realref(tt) ), 10 );
    mag_set  ( arb_radref( acb_imagref(tt) ), arb_radref( acb_realref(tt) ) );
    printf("acb box tt: "); acb_printd(tt, 10); printf("\n");
    box_get_box_list_acb( bl, tt );
    printf("boxes: "); box_list_print(bl); printf("\n");
    box_CoCo_init_box(bc, box_list_pop(bl));
    while ( ! box_list_is_empty(bl) )
        box_CoCo_insert_box(bc, box_list_pop(bl));
    box_CoCo_get_contBox(ttt, bc);
    printf("acb box ttt: "); acb_printd(ttt, 10); printf("\n");
    box_CoCo_clear(bc);
    printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
    
//     box_CoCo_clear(bc);
    box_list_clear(bl);
    acb_clear(tt);
    acb_clear(ttt);
    flint_cleanup();
    return 0;
    
}
