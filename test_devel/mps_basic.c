#include <mps/mps.h>
#include <stdio.h>

int main(int argc, char **argv){
    
    long int deg = 2;
    mps_monomial_poly *p;
    mps_context *ctx;
    mpc_t *roots = NULL;  /* pairs of gmp's mpf_t, one for real part, one for imag part, defined in mps/mpc.h */
    rdpe_t *radii = NULL; /* a rdpe is a double (the mantissa) + a long (the exponent) , defined in mps/mt.h  */
    
    ctx = mps_context_new ();
    p = mps_monomial_poly_new (ctx, deg);
    
    /* pol (x-1)*(x-2) = x^2 -3x + 2 */
    mps_monomial_poly_set_coefficient_int (ctx, p, 2, 1, 0);
    mps_monomial_poly_set_coefficient_int (ctx, p, 1, -3, 0);
    mps_monomial_poly_set_coefficient_int (ctx, p, 0, 2, 0);
    
    /* Set the input polynomial */
    mps_context_set_input_poly (ctx, MPS_POLYNOMIAL (p));
  
    /* Set the output precision */
    mps_context_set_output_prec (ctx, 53);
    mps_context_set_output_goal (ctx, MPS_OUTPUT_GOAL_APPROXIMATE);
    /* Set the algorithm */
    mps_context_select_algorithm(ctx, MPS_ALGORITHM_SECULAR_GA);
    
    /* Find the roots */
    mps_mpsolve (ctx);
    
    /* Read the roots from context */
    mps_context_get_roots_m (ctx, &roots, &radii);
    
    /* Print out roots */
    double err_max=0., err_d=0.;
    double root_re, root_im;
    printf("Roots:\t\t\t\t\tInclusion\n");
    printf("real part\t imaginary part\t\tabsolute radius\n");
    printf("=================================================================\n");
//     mpc_init2(err, mpc_get_prec (roots[0]));
    for (long int m = 0; m < deg; m++) {
        err_d = rdpe_get_d (radii[m]);
        err_max = err_d > err_max ? err_d : err_max;
        root_re = mpf_get_d( mpc_Re(roots[m]) );
        root_im = mpf_get_d( mpc_Im(roots[m]) );
        printf("%.3e\t%.3e\t\t%.3e\n", root_re, root_im, err_d );
    }
    printf("=================================================================\n");
    printf("\t\t\t\tmax(Error) = %.3e\n",err_max);
    
    /* Free the allocated memory */
    mps_monomial_poly_free(ctx, (mps_polynomial *)p);
    mps_context_free(ctx);
    free (roots);
    /* free(roots); */
    free(radii);
    
    return EXIT_SUCCESS;
}
