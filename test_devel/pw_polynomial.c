#include "pw_base.h"
// #include "common/pw_common.h"
#include "polynomial/pw_polynomial.h"

acb_poly_t pacb;
fmpq_poly_t pfmpq_re;
fmpq_poly_t pfmpq_im;
slong length;

void init_global_variables(){
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    length = 0;
}

void clear_global_variables(){
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(pfmpq_re);
    acb_poly_clear(pacb);
}

void getApprox(acb_poly_t dest, slong prec){
    acb_poly_set2_fmpq_poly(dest, pfmpq_re, pfmpq_im, prec);
}

void getApproxA(acb_poly_t dest, slong prec){
    acb_poly_set_round(dest, pacb, prec);
}

void double_poly_print( const double * coeffs, const double * abers, slong len ) {
    printf("[");
    for (slong i = 0; i < len; i++) {
        printf("(%f + %fj) +/- %e\n", coeffs[2*i], coeffs[2*i+1], abers[i] );
    }
    printf("]");
}

void fmpq_poly_ones( fmpq_poly_t dest, slong length ) {
    fmpq_poly_fit_length(dest, length);
    _fmpq_poly_set_length(dest, length);
    for(slong i=0; i<length; i++)
        fmpq_poly_set_coeff_si(dest, i, 1);
}

void acb_poly_set_middle_inplace( acb_poly_t dest ) {
    for (slong i=0; i<dest->length; i++) {
        acb_get_mid( dest->coeffs+i, dest->coeffs+i );
    }
} 

void acb_poly_add_error_double_inplace( acb_poly_t dest, double error ) {
    mag_t err;
    mag_init(err);
    mag_set_d(err, error);
    for (slong i=0; i<dest->length; i++) {
        acb_add_error_mag( dest->coeffs+i, err );
    }
    mag_clear(err);
}

void pw_polynomial_print( pw_polynomial_t poly ) {
    printf("   is_approx: %d, is_oracle: %d, is_exact: %d, is_exact_fmpq: %d, is_exact_arb: %d\n",
           pw_polynomial_is_approx(poly),
           pw_polynomial_is_oracle(poly),
           pw_polynomial_is_exact(poly),
           pw_polynomial_is_exact_fmpq(poly),
           pw_polynomial_is_exact_acb(poly) );
    printf("   degree: %ld, multiplicity of zero: %ld, max prec: %ld, prec of current approx: %ld\n",
           pw_polynomial_degree(poly),
           pw_polynomial_multZrref(poly),
           pw_polynomial_maxPreref(poly),
           pw_polynomial_appPreref(poly) );
    printf("   has real coeffs: %d\n", pw_polynomial_is_real(poly) );
    printf("   nb of approxs in cache: %d\n", pw_polynomial_SizePoref(poly) );
}

void test_pw_poly( const char * pchar ) {
    
    pw_polynomial_t poly;
    slong prec1 = PWPOLY_DEFAULT_PREC;
    slong prec2 = 2*PWPOLY_DEFAULT_PREC;
    
    pw_polynomial_init_from_exact_fmpq( poly, pfmpq_re, pfmpq_im );
    pw_polynomial_remove_root_zero(poly);
    if (pchar==NULL) {
        printf("=====================fmpq poly ");
        fmpq_poly_print_pretty(pfmpq_re, "x");
        printf(" + I(");
        fmpq_poly_print_pretty(pfmpq_im, "x");
        printf(")==============================\n");
    } else {
        printf("=====================fmpq poly %s=======================\n", pchar);
    }
    
    pw_polynomial_print( poly );
    printf("acb approx at prec %ld: \n", prec1);
    acb_poly_printd( pw_polynomial_getApproximation(poly, prec1), 10 );
    printf("\n");
    pw_polynomial_print( poly );
    printf("acb approx at prec %ld: \n", prec2);
    acb_poly_printd( pw_polynomial_getApproximation(poly, prec2), 10 );
    printf("\n");
    pw_polynomial_print( poly );
    printf("acb cache approx at prec %ld: \n", prec2);
    acb_poly_printd( pw_polynomial_cacheANDgetApproximation( poly, prec2), 10 );
    printf("\n");
    pw_polynomial_print( poly );
#ifdef PWPOLY_HAS_BEFFT
    printf( " fits in double: %d, double approximation: \n", pw_polynomial_compute_double_approximation(poly) );
    double_poly_print( pw_polynomial_Dcoeffref(poly), pw_polynomial_Dabersref(poly), pw_polynomial_degree(poly) + 1 );
#endif
    printf( "============================================================\n\n" );
    
    pw_polynomial_clear(poly);
    
    acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, PWPOLY_DEFAULT_PREC);
    acb_poly_set_middle_inplace( pacb );
    pw_polynomial_init_from_exact_acb( poly, pacb );
    pw_polynomial_remove_root_zero(poly);
    
    if (pchar==NULL) {
        printf("=====================exact acb poly ");
        fmpq_poly_print_pretty(pfmpq_re, "x");
        printf(" + I(");
        fmpq_poly_print_pretty(pfmpq_im, "x");
        printf(")==============================\n");
    } else {
        printf("=====================exact acb poly %s=======================\n", pchar);
    }
    
    pw_polynomial_print( poly );
    printf("acb approx at prec %ld: \n", prec1);
    acb_poly_printd( pw_polynomial_getApproximation(poly, prec1), 10 );
    printf("\n");
    pw_polynomial_print( poly );
    printf("acb approx at prec %ld: \n", prec2);
    acb_poly_printd( pw_polynomial_getApproximation(poly, prec2), 10 );
    printf("\n");
    pw_polynomial_print( poly );
    printf("acb cache approx at prec %ld: \n", prec2);
    acb_poly_printd( pw_polynomial_cacheANDgetApproximation( poly, prec2), 10 );
    printf("\n");
    pw_polynomial_print( poly );
#ifdef PWPOLY_HAS_BEFFT
    printf( " fits in double: %d, double approximation: \n", pw_polynomial_compute_double_approximation(poly) );
    double_poly_print( pw_polynomial_Dcoeffref(poly), pw_polynomial_Dabersref(poly), pw_polynomial_degree(poly) + 1 );
#endif
    printf( "============================================================\n\n" );
    
    pw_polynomial_clear(poly);
    
    acb_poly_add_error_double_inplace( pacb, 0.1 );
    pw_polynomial_init_from_approx( poly, pacb, PWPOLY_DEFAULT_PREC );
    pw_polynomial_remove_root_zero(poly);
    if (pchar==NULL) {
        printf("=====================approx acb poly ");
        fmpq_poly_print_pretty(pfmpq_re, "x");
        printf(" + I(");
        fmpq_poly_print_pretty(pfmpq_im, "x");
        printf(")==============================\n");
    } else {
        printf("=====================approx acb poly %s=======================\n", pchar);
    }
    
    pw_polynomial_print( poly );
    printf("acb approx at prec %ld: \n", prec1);
    acb_poly_printd( pw_polynomial_getApproximation(poly, prec1), 10 );
    printf("\n");
    pw_polynomial_print( poly );
    printf("acb approx at prec %ld: \n", prec2);
    acb_poly_printd( pw_polynomial_getApproximation(poly, prec2), 10 );
    printf("\n");
    pw_polynomial_print( poly );
    printf("acb cache approx at prec %ld: \n", prec2);
    acb_poly_printd( pw_polynomial_cacheANDgetApproximation( poly, prec2), 10 );
    printf("\n");
    pw_polynomial_print( poly );
#ifdef PWPOLY_HAS_BEFFT
    printf( " fits in double: %d, double approximation: \n", pw_polynomial_compute_double_approximation(poly) );
    double_poly_print( pw_polynomial_Dcoeffref(poly), pw_polynomial_Dabersref(poly), pw_polynomial_degree(poly) + 1 );
#endif
    printf( "============================================================\n\n" );
    
    pw_polynomial_clear(poly);
    
    pw_polynomial_init_from_oracle( poly, getApprox );
    pw_polynomial_remove_root_zero(poly);
    if (pchar==NULL) {
        printf("=====================oracle poly ");
        fmpq_poly_print_pretty(pfmpq_re, "x");
        printf(" + I(");
        fmpq_poly_print_pretty(pfmpq_im, "x");
        printf(")==============================\n");
    } else {
        printf("=====================oracle poly %s=======================\n", pchar);
    }
    
    pw_polynomial_print( poly );
    printf("acb approx at prec %ld: \n", prec1);
    acb_poly_printd( pw_polynomial_getApproximation(poly, prec1), 10 );
    printf("\n");
    pw_polynomial_print( poly );
    printf("acb approx at prec %ld: \n", prec2);
    acb_poly_printd( pw_polynomial_getApproximation(poly, prec2), 10 );
    printf("\n");
    pw_polynomial_print( poly );
    printf("acb cache approx at prec %ld: \n", prec2);
    acb_poly_printd( pw_polynomial_cacheANDgetApproximation( poly, prec2), 10 );
    printf("\n");
    pw_polynomial_print( poly );
#ifdef PWPOLY_HAS_BEFFT
    printf( " fits in double: %d, double approximation: \n", pw_polynomial_compute_double_approximation(poly) );
    double_poly_print( pw_polynomial_Dcoeffref(poly), pw_polynomial_Dabersref(poly), pw_polynomial_degree(poly) + 1 );
#endif
    printf( "\n============================================================\n\n" );
    
    pw_polynomial_clear(poly);
    
}

int main(int argc, char* argv[]) {
    
    init_global_variables();
    
    /* fmpq poly x^2 + x + 1 */
    fmpq_poly_ones( pfmpq_re, 3 );
    fmpq_poly_zero( pfmpq_im );
    test_pw_poly( "x^2 + x + 1" );
    
    printf("\n\n");
    /* fmpq poly x^2 + x + 1 + I(x^2 + x + 1) */
    fmpq_poly_ones( pfmpq_re, 3 );
    fmpq_poly_ones( pfmpq_im, 3 );
    test_pw_poly( NULL );
    
    printf("\n\n");
    /* fmpq poly x^2 + x + I(x^2) */
    fmpq_poly_ones( pfmpq_re, 3 );
    fmpq_poly_set_coeff_si(pfmpq_re, 0, 0);
    fmpq_poly_ones( pfmpq_im, 3 );
    fmpq_poly_set_coeff_si(pfmpq_im, 0, 0);
    fmpq_poly_set_coeff_si(pfmpq_im, 1, 0);
    test_pw_poly( NULL );
    
    printf("\n\n");
    /* fmpq poly x^2 + I(0) */
    fmpq_poly_ones( pfmpq_re, 3 );
    fmpq_poly_set_coeff_si(pfmpq_re, 0, 0);
    fmpq_poly_set_coeff_si(pfmpq_re, 1, 0);
    fmpq_poly_ones( pfmpq_im, 3 );
    fmpq_poly_set_coeff_si(pfmpq_im, 0, 0);
    fmpq_poly_set_coeff_si(pfmpq_im, 1, 0);
    fmpq_poly_set_coeff_si(pfmpq_im, 2, 0);
    test_pw_poly( NULL );

    clear_global_variables();
    
    flint_cleanup();
    
    return 0;
    
//     if (argc<2) {
//         printf("Usage: %s filename\n", argv[0]);
//         exit(0);
//     }
//     
//     init_global_variables();
//     slong prec = PWPOLY_DEFAULT_PREC;
//     
//     int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
//     
//     if (type==PW_PARSE_FAILD){
//         clear_global_variables();
//         flint_cleanup();
//         exit(0);
//     }
//     
//     cache_poly_t poly;
//     
//     if (type==PW_PARSE_EXACT) {
// //         acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, prec);
//         cache_poly_init_from_exact ( poly, pfmpq_re, pfmpq_im);
//         printf("Cache initialized with 2 fmpq_poly of deg %ld\n", length);
//     } else {
//         cache_poly_init_from_approx( poly, pacb->coeffs, length, prec);
//         printf("Cache initialized with 1 acb_poly of deg %ld, precision %ld\n", length, prec);
//     }
//     
//     acb_poly_ptr papprox = cache_poly_getApproximation( poly, prec);
//     
//     printf("Poly approximated to precision %ld: \n", prec);
//     acb_poly_printd(papprox, 20);
//     printf("\n");
//     
// #ifdef PWPOLY_HAS_BEFFT
//     int fits = cache_poly_compute_double_approximation( poly );
//     
//     if (fits) {
//     
//         printf("Poly approximated to double precision floating point numbers: \n");
//         double_poly_print( cache_poly_Dcoeffref(poly), cache_poly_Dabersref(poly), cache_poly_lengthref(poly) );
//         printf("\n");
//         
//     } else {
//         printf("Poly does not fit in double precision floating points numbers!\n");
//     }
// #endif
// 
//     cache_poly_clear(poly);
//     
//     clear_global_variables();
//     
//     flint_cleanup();
//     
//     return 0;
}
