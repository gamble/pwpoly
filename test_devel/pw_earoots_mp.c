#include "pw_base.h"
#include "common/pw_common.h"
#include "common/pw_gnuplot.h"
// #include "solving/pw_solving.h"
#include "solvers/pw_EA.h"
#include "pw_solving.h"
#include <time.h>

acb_poly_t pacb;
fmpq_poly_t pfmpq_re;
fmpq_poly_t pfmpq_im;
slong length;
int verbosity, increase, check, goal, solver;
slong m, log2eps, roundInput, goalNbRoots;
fmpq_t b; /* not used */
domain_t dom;

void init_global_variables(){
    acb_poly_init(pacb);
    fmpq_poly_init(pfmpq_re);
    fmpq_poly_init(pfmpq_im);
    length = 0;
    verbosity = 1;
    m = 500;
    increase = 1;
    check = 0;
    log2eps = -53;
    goal = PW_GOAL_ISOLATE;
    solver = 32;
    roundInput = 0;
    fmpq_init(b);
    domain_init(dom); /* by default set to C */
    goalNbRoots = -1; /* find all the roots */
}

void clear_global_variables(){
    domain_clear(dom);
    fmpq_clear(b);
    fmpq_poly_clear(pfmpq_im);
    fmpq_poly_clear(pfmpq_re);
    acb_poly_clear(pacb);
}

int main(int argc, char* argv[]) {
    
    int level = 1;
    
    if ( (argc<2) || (strcmp( argv[1], "-h" ) == 0) 
                  || (strcmp( argv[1], "--help" ) == 0) ) {
        pwroots_fprint_options( stdout, argc, argv );
        exit(0);
    }
    
    init_global_variables();
    int parse = parseInputArgs( argc, argv, &verbosity, dom, &goalNbRoots, &m, &increase, &check, &log2eps, &goal, &solver, &roundInput, b );
    
    /* type is either PW_PARSE_FAILD, PW_PARSE_EXACT for integer/rational polynomials */
    /*             or PW_PARSE_BALLS for acb poly                                     */
    /* length is the degree+1 of the input poly                                       */
    /* fills pacb if PW_PARSE_BALLS, pfmpq_re and pfmpq_im if PW_PARSE_EXACT          */
    int type = parseInputFile( &length, pacb, pfmpq_re, pfmpq_im, argv[1] );
    
    if ( (type==PW_PARSE_FAILD) || (parse==0) ){
        clear_global_variables();
        flint_cleanup();
        exit(0);
    }
    
    if (verbosity>=level) {
        printf("roundInput : %ld\n", roundInput);
        printf("log2eps    : %ld\n", log2eps);
        printf("goal       : "); pwroots_fprint_goal(stdout, goal); printf("\n");
        printf("domain     : "); domain_print_short(dom); printf("\n");
        printf("nb max itts: %ld\n", m);
        printf("check      : %d\n", check);
        printf("verbosity  : %d\n", verbosity);
        printf("goalNbRoots: %ld\n", goalNbRoots);
        
//         int testIncl = 0;
//         PW_EA_SET_INCLUSION(testIncl,PW_NATURAL_I);
//         printf(" testIncl after setting to PW_NATURAL_I (%d): %d\n", PW_NATURAL_I, testIncl );
//         printf(" PW_EA_IS_TAMED(testIncl): %d\n", PW_EA_IS_TAMED(testIncl));
//         PW_EA_SET_APPROXIMD(testIncl);
//         printf(" testIncl after setting to PW_EA_APPROXIMD (%d): %d\n", PW_EA_APPROXIMD, testIncl );
//         printf(" PW_EA_IS_TAMED(testIncl): %d\n", PW_EA_IS_TAMED(testIncl));
//         PW_EA_SET_INCLUSION(testIncl,PW_ISOLATING);
//         printf(" PW_EA_IS_TAMED(testIncl): %d\n", PW_EA_IS_TAMED(testIncl));
//         printf(" testIncl after setting to PW_ISOLATING (%d): %d\n", PW_ISOLATING, testIncl );
//         printf(" PW_EA_IS_TAMED(testIncl): %d\n", PW_EA_IS_TAMED(testIncl));
//         PW_EA_UNSET_APPROXIMD(testIncl);
//         printf(" testIncl after unsetting PW_EA_APPROXIMD (%d): %d\n", PW_EA_APPROXIMD, testIncl );
//         printf(" PW_EA_IS_TAMED(testIncl): %d\n", PW_EA_IS_TAMED(testIncl));
//         testIncl = 0;
//         printf("\n");
//         PW_EA_SET_INCLUSION(testIncl,PW_NATURAL_I);
//         printf(" testIncl after setting to PW_NATURAL_I (%d): %d\n", PW_NATURAL_I, testIncl );
//         PW_EA_SET_NEWTO_FAILED(testIncl);
//         printf(" testIncl after setting NEWTON FAILED: %d\n", testIncl );
//         printf(" PW_EA_IS_NATURAL_I(testIncl): %d\n", PW_EA_IS_NATURAL_I(testIncl));
//         printf(" PW_EA_NEWTO_FAILED(testIncl): %d\n", PW_EA_NEWTO_FAILED(testIncl));
//         printf(" PW_EA_NEWTO_SUCCEE(testIncl): %d\n", PW_EA_NEWTO_SUCCEE(testIncl));
//         PW_EA_SET_NEWTO_SUCCEE(testIncl);
//         printf(" testIncl after setting NEWTON SUCCEE: %d\n", testIncl );
//         printf(" PW_EA_IS_NATURAL_I(testIncl): %d\n", PW_EA_IS_NATURAL_I(testIncl));
//         printf(" PW_EA_NEWTO_FAILED(testIncl): %d\n", PW_EA_NEWTO_FAILED(testIncl));
//         printf(" PW_EA_NEWTO_SUCCEE(testIncl): %d\n", PW_EA_NEWTO_SUCCEE(testIncl));
        
    }
    
#ifdef PW_EA_PROFILE
    PW_EA_INIT_PROFILE
#endif
    
    if ((type==PW_PARSE_EXACT)&&(roundInput>0)) {
        type = PW_PARSE_BALLS;
        acb_poly_set2_fmpq_poly( pacb, pfmpq_re, pfmpq_im, roundInput );
    }
    
    /* result */
    acb_ptr roots = _acb_vec_init( length - 1 );
    slong * mults = (slong *) pwpoly_malloc ( (length - 1)*sizeof(slong) );
    slong nbclusts = 0;
    slong nbroots = 0;
    
    clock_t start_solve = clock();
    
    FILE * initPltFile = NULL;
    char initPltFileName[] = "init_points.plt\0";
    initPltFile = fopen (initPltFileName,"w");
    
    slong prec = PWPOLY_DEFAULT_PREC;
    slong res = 0;
    if (type==PW_PARSE_EXACT) {
        res = pw_EA_solve_2fmpq_polynomial ( roots, mults, &nbclusts, pfmpq_re, pfmpq_im, goal, log2eps, dom, goalNbRoots, &prec, m, verbosity, NULL);
    } else {
        for (slong i=0; i<length; i++) {
            mag_zero( arb_radref(acb_realref(pacb->coeffs+i)) );
            mag_zero( arb_radref(acb_imagref(pacb->coeffs+i)) );
        }
        res = pw_EA_solve_exact_acb_polynomial ( roots, mults, &nbclusts, pacb, goal, log2eps, dom, goalNbRoots, &prec, m, verbosity, initPltFile);
    }

    /* get nbroots */
    for (slong i=0; i< nbclusts; i++)
        nbroots+=mults[i];
    
    if ((res<0) || (res >= m)) {
        nbroots = 0;
        for (slong i=0; i< length-1; i++)
            acb_get_mid(roots+i, roots+i);
    }
    
    double clicks_in_solve = (clock() - start_solve);
    
    if (verbosity >= level) {
        printf(" res:     %ld\n", res) ;
        printf(" success: %ld\n", nbroots) ;
        printf(" time for isolating the roots: %lf seconds\n",
             clicks_in_solve/CLOCKS_PER_SEC) ;
    }
    
//     printf("solve: nb of clusters: %ld, nb of roots: %ld\n", nbclusts, nbroots);
    
    FILE * outPltFile;
    char outPltFileName[] = "roots.plt\0";
    outPltFile = fopen (outPltFileName,"w");
//     pw_points_gnuplot( outPltFile, roots, nbclusts );
//     pw_points_gnuplot( outPltFile, roots, length-1 );
    pw_roots_domain_gnuplot( outPltFile, roots, nbclusts, dom );
    fclose (outPltFile);
    
    if (initPltFile != NULL)
        fclose (initPltFile);
    
#ifdef PW_EA_PROFILE
    PW_EA_PRINT_PROFILE
#endif

    /* check the output */
    if (check) {
        clock_t start_check = clock();
        int checkOK = 0;
        if (type==PW_PARSE_EXACT){
            checkOK = pwpoly_checkOutput_solve_2fmpq_poly ( roots, mults, nbclusts, pfmpq_re, pfmpq_im, log2eps, goal, dom, (slong) goalNbRoots );
        } else {
            checkOK = pwpoly_checkOutput_solve_exact_acb_poly ( roots, mults, nbclusts, pacb, log2eps, goal, dom, (slong) goalNbRoots );
        }
        
        double clicks_in_check = (clock() - start_check);
        printf(" check output                    : %d\n", checkOK);
        printf(" time for checking output        : %lf seconds\n",
                 clicks_in_check/CLOCKS_PER_SEC) ;
    } else {
        if (verbosity>=level) {
            printf(" check output                    : NOT CHECKED\n");
        }
    }
    
    pwpoly_free(mults);
    _acb_vec_clear( roots, length - 1 );
    clear_global_variables();
    
    flint_cleanup();
    
    return 0;
}
