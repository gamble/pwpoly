# pwpoly

A C library for univariate polynomial root finding.

## This version

is NOT the prototype used to obtain the numerical results for our paper
"Fast evaluation and root finding for polynomials with floating-point coefficients"
submitted on February 7th, 2023.

If you want this prototype, checkout or download the tag Submission_February_7_23:

```
git checkout tags/Submission_February_7_23
```

## Installation, compilation and use of the C library

### Dependencies
- ```gcc``` (tested with 10.4.0)
- ```Make``` (tested with 4.3)
- ```CMake``` (version >= 3.9.4)
- ```gmp``` (tested with 6.2.1)
- ```mpfr``` (tested with 4.1.0)
- ```flint``` (tested with 2.8.5)
- ```arb``` (tested with 2.22.1)

### Cloning repository

```git clone https://gitlab.inria.fr/gamble/pwpoly```

### Compilation
Assume ```gmp```, ```mpfr```, ```flint```, ```arb``` are installed.
In the directory containing this file, in a terminal, run:

```make```

This will run cmake and compile the library and the executables.

The first time ```make``` is called, an internet connection must be available (see section Submodules below).

### Simple use

#### generate a file encoding a Mignotte polynomial with ```bin/genPolFile```:

```bin/genPolFile Mignotte 256 -b 8 -o Mignotte_256_8.pwr```

will generate a file ```Mignotte_256_8.pwr``` encoding the Mignotte polynomial
$p:=x^{256} -2(2^8 x -1)^2$

#### isolate the roots of $p$:

```bin/pwroots Mignotte_256_8.pwr```

This will generate two files: ```roots.txt``` and ```roots.plt```.

```roots.txt``` contains a list $B$ of isolating complex balls for the roots of $p$, 
i.e. each root of $p$ is in an element of $B$, each element of $B$ contains a unique root of $p$
and the elements of $B$ are pairwise disjoint.

The elements of $B$ with zero imaginary parts isolate a real root of $p$;
when $p$ has real coefficients, the real roots of $p$ are isolated in an element
of $B$ with an imaginary part which is zero.

In ```roots.txt```, each isolating ball is preceded by an integer giving the sums of multiplicities
of the roots in it; here it will always be $1$ since the roots are isolated.

Be aware that isolating the roots of a polynomial $p$ requires $p$ to be *square-free*, i.e.
there is no complex point $x$ so that $p(x)=p'(x)=0$.
Square-freeness is not checked in ```pwroots```, and isolating the roots of
a non square-free polynomial with ```pwroots``` leads to non-termination.

The roots can be plotted with:

```gnuplot roots.plt```

provided that ```gnuplot``` is installed on the system.

#### approximate the roots of $p$ up to precision $2^{-53}$:

```bin/pwroots Mignotte_256_8.pwr -g a -e -53```

Here $p$ is not required to be square-free!
This will compute $d'$ pairs $(b_i,m_i)$ satisfying:

- the $b_i$'s are pairwise disjoint and have width less than $2^{-53}$,
- $b_i$ and $3b_i$ both contain $m_i$ roots counted with multiplicities,
- the $m_i$'s sum up to $d$,

where $d$ is the degree of $p$ and $3b_i$ denotes the factor three centered dilation of $b_i$.

```roots.txt``` will contain $d'$ lines, each line being ```m_i  b_i```.

Of course $-53$ can be replaced by any $e<=0$.

See more options for ```bin/genPolFile``` and ```bin/pwroots``` with 
```bin/genPolFile -h``` and ```bin/pwroots -h```.

#### evaluate $p$ (and its derivative) at a list of points. 

Write the points in which $p$ has to be evaluated in a file, say ```points.txt``` which looks like

```
double 3
.0 .0
.5 .0
.5 .5
```

corresponding to the 3 points $0$, $0.5$ and $0.5 + 0.5i$, and do 

``` bin/pweval Mignotte_256_8.pwr points.txt -m 53 -2```

this will generate the two files ```values.txt``` and ```valuesDer.txt``` containing list of complex intervals
continaining respectively the list values of $p$ and the values of $p'$ at the list of points in input.

- ```-m 34``` means that the internal precision used for evaluation is *34* (replace *34* with any positive integer you like),
- ```-2``` means that both $p'$ and $p'$ are evaluated; if not given, only $p$ is evaluated and ```valuesDer.txt``` is not generated.

#### more about the use of internal precision
When internal precision is not specified, the default value *53* is used.

For $p(x)=p_0 + p_1x + ... + p_dx^d$, define $|p|(|x|)=|p_0| + |p_1||x| + ... + |p_d||x|^d$.
When using internal precision $u>0$, the output interval containing the value of $p(a)$ for a point $a$
will have width relative to $|p|(|a|)$ about $2^-u$. Increasing $u$ will increase this output relative width.

#### more about input point files
Input points can also be given as fmpq rational:

```
fmpq 3
0 0
1/2 0
1/2 1/2
```

or hexdump of acb balls:

```
acb 3
0 0 1 -35 0 0 1 -35
1 -1 1 -35 0 0 1 -35
1 -1 1 -35 1 -1 1 -35
```

See more options for ```bin/pweval``` with ```bin/pweval -h```.

## More details

### Submodules
```pwpoly``` has submodules for two of its dependences, namely:

- ```befft``` (https://gitlab.inria.fr/gamble/befft), set to track the main branch
- ```earoots``` (https://gitlab.inria.fr/gmoro/earoots), set to track the develop branch

and this is configured in the file ``` .gitmodules ```.

When cloning ```pwpoly```, the submodules are initialized but not pulled from repository.

The submodules are cloned in the targets of the ```Makefile```
if the repertories ```befft/``` and ```earoots/``` are not empty.
If you have reasons to think that ```befft``` of ```earoots``` have changed,
do 

```git submodule update --remote --init;```.

The first time the main targets of the ```Makefile``` are called, an internet connection must be available to clone the submodules.

## notes for future developpers

