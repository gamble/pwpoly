all: sbmod
	mkdir -p build
	cd build && cmake -DHAS_BEFFT=1 -DHAS_EAROOTS=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
allDebug: sbmod
	mkdir -p build
	cd build && cmake -DHAS_BEFFT=1 -DHAS_EAROOTS=1 -DCMAKE_BUILD_TYPE=Debug ../
	make -C build -j4 VERBOSE=1
	
allClang: sbmod
	mkdir -p build
	cd build && cmake  -DHAS_BEFFT=1 -DHAS_EAROOTS=1 -DCMAKE_C_COMPILER=clang -DCMAKE_BUILD_TYPE=Debug ../
	make -C build -j4 VERBOSE=1
	
onlyLib: sbmod
	mkdir -p build
	cd build && cmake  -DHAS_BEFFT=1 -DHAS_EAROOTS=1 -DONLY_LIB=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
simulate_maple: sbmod
	mkdir -p build
	cd build && cmake  -DHAS_BEFFT=1 -DHAS_EAROOTS=1 -DSIMULATE_MAPLE=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
forTime: sbmod
	mkdir -p build
	cd build && cmake  -DHAS_BEFFT=1 -DHAS_EAROOTS=1 -DSILENT=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1

withBefft: sbmod
	mkdir -p build
	cd build && cmake -DHAS_BEFFT=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
withEARoots: sbmod
	mkdir -p build
	cd build && cmake -DHAS_EAROOTS=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
withOnlyArb:
	mkdir -p build
	cd build && cmake -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
withOnlyArbBefft:
	mkdir -p build
	cd build && cmake -DHAS_BEFFT=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
withOnlyArbDebug:
	mkdir -p build
	cd build && cmake -DCMAKE_BUILD_TYPE=Debug ../
	make -C build -j4 VERBOSE=1
	
debug:
	mkdir -p build
	cd build && cmake -DCMAKE_BUILD_TYPE=Debug ../
	make -C build -j4 VERBOSE=1
	
clang:
	mkdir -p build
	cd build && cmake -D CMAKE_C_COMPILER=clang -DCMAKE_BUILD_TYPE=Debug ../
	make -C build -j4 VERBOSE=1
	
clean:
	rm -rf lib
	rm -rf bin
	rm -rf build
	rm -rf test_devel/*.o
	
sbmod:
	if [ ! -d "befft/src" ] || [ ! -d "earoots/src" ]; then \
	git submodule update --remote --init; \
	fi
